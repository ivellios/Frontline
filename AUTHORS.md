# CONTRIBUTOR LIST

## CORE TEAM
- Adanteh
- Gunther.S
- Stack/Overfl0
- Leon
- N3croo

## CONTRIBUTORS
- Cole (Gear configs)
- Alonso.E (Launcher)
- BaermitUmlaut (Original squad radar creator)
- Chairborne (Made the rallypoints)
- Audiocustoms (Tinitus sounds)

Plus AAW Team for making the Project Reality mod, which we use server-side
