 class CfgPatches {
	class WW2_Assets_c_Grenades {
		units[] = {};
		weapons[] = {"LIB_MolotovMuzzle"};
		requiredVersion = 0.1;
		requiredAddons[] = {
			"A3_Weapons_F",
			"A3_Data_F",
			"A3_Functions_F",
			"A3_characters_F"
		};
	};
};

class CfgWeapons {
	class GrenadeLauncher;
	class Throw: GrenadeLauncher {
		muzzles[] += {"LIB_MolotovMuzzle"};
		class ThrowMuzzle;
		class LIB_MolotovMuzzle: ThrowMuzzle {
			magazines[] = {"LIB_Molotov"};
		};
	};
};

class CfgFunctions {
	class LIB_GRENADE {
		tag = "LIB_Grenade";
		class Grenades {
			file = "\@molotov\Addons\Grenade\fnc";
			class throwInit { postInit = 1; };
			class throwHandler;
			class molotovThrown { recompile = 1; };
		};
	};
};

class CfgAmmo {
	class GrenadeHand_stone;
	class LIB_T_MolotovAmmo: GrenadeHand_stone {
		//simulation = "shotShell";
		effectsSmoke = "";
		explosionTime = 0;
		fuseDistance = 0;
		deflecting = 0;
		hit = 0;
		craterEffects = "NoCrater";
		timeToLive = 30;
		model = "\A3\Structures_F\Items\Food\BottlePlastic_V1_F.p3d";
		effectsMissile = "LIB_MolotovFire_trail";
		whistleDist = 10;
		whistleOnFire = 0;

		LIB_throwFunction = "LIB_Grenade_fnc_molotovThrown";
	};

	class Chemlight_red;
	class SmokeShell;
	class LIB_T_MolotovAmmo_fragment: SmokeShell {
		effectsSmoke = "LIB_Molotov_Effects";
		deflecting = 100;
		timeToLive = 60;
		fuseDistance = 0;
		hit = 0;
		explosionTime = 0;
	};

	class LIB_T_MolotovAmmo_fragment1: LIB_T_MolotovAmmo_fragment {
		deflecting = 0;
		timeToLive = 30;
		effectsSmoke = "LIB_Molotov_Effects1";
	};
};

class CfgMagazines {
	class HandGrenade;
	class LIB_Molotov: HandGrenade {
		author = "Adanteh";
		ammo = "LIB_T_MolotovAmmo";
		displayName = "Molotov Cocktail";
		displayNameShort = "Molotov";
		descriptionShort = "Type: Molotov Cocktail<br />Description: Spreads fire wherever it hits";
		scope = 2;
		mass = 11;
		initSpeed = 15;
		picture = "\@molotov\Addons\Grenade\data\m_molotov_ca.paa";
	};
};

class CfgCloudlets {
	class Default;
	class ObjectDestructionFire1Small;
	class ObjectDestructionRefractSmall;
	class LIB_MolotovRefract: ObjectDestructionRefractSmall {
		interval = 1.2;
		positionVar[] = {1.5, 0.5, 1.5};
		size[] = {4.8, 5.2};
	};

	 class ObjectDestructionSmokeSmallX;
	 class LIB_MolotovSmoke: ObjectDestructionSmokeSmallX {
		interval = 0.12;
		particleShape="warfxpe\ParticleEffects\Universal\smoke_01";
		blockAIVisibility = 0;
	 	moveVelocity[] = {0, 1, 0};
	 	weight = 0.053;
	 	lifeTime = 1;
	 	lifeTimeVar = 15;
		color[]=
		{
			{0.02,0.02,0.02,0},
			{0.02,0.02,0.02,1},
			{0.02,0.02,0.02,0.9},
			{0.02,0.02,0.02,0.8},
			{0.02,0.02,0.02,0.7},
			{0.02,0.02,0.02,0.6},
			{0.02,0.02,0.02,0.5},
			{0.02,0.02,0.02,0.4},
			{0.02,0.02,0.02,0.3},
			{0.02,0.02,0.02,0.2},
			{0.02,0.02,0.02,0.1},
			{0.02,0.02,0.02,0}
		};
	 	size[] = {0.8, 1.8};
	 };

	class ObjectDestructionSmoke2X;
	class LIB_MolotovSmoke1: ObjectDestructionSmoke2X {
		interval = 0.7;
		particleShape="warfxpe\ParticleEffects\Universal\smoke_01";
		blockAIVisibility = 0;
		moveVelocity[] = {0, 1, 0};
		weight = 0.051;//weight = 0.05;
		lifeTime = 10;
		lifeTimeVar = 3;
		circleRadius = 1;
		color[]=
		{
			{0.02,0.02,0.02,0},
			{0.02,0.02,0.02,0.6},
			{0.02,0.02,0.02,0.5},
			{0.02,0.02,0.02,0.4},
			{0.02,0.02,0.02,0.3},
			{0.02,0.02,0.02,0.2},
			{0.02,0.02,0.02,0.2},
			{0.02,0.02,0.02,0.1},
			{0.02,0.02,0.02,0.1},
			{0.02,0.02,0.02,0.05},
			{0.02,0.02,0.02,0.01},
			{0.02,0.02,0.02,0}
		};
		size[] = {1.8, 3.8, 8};
	};


	class LIB_MolotovFire: ObjectDestructionFire1Small {
		
		interval = 0.13;
		particleShape="warfxpe\ParticleEffects\Universal\largefire_01";
		blockAIVisibility = 0;
		damageType = "Fire";
		damageTime = 0.03;
		color[] = {{1, 1, 1, -1}};
		constantDistance = 0;
		coreDistance = 0.9;
		coreIntensity = 1.05;
		circleRadius = 0.6;
		lifeTime = 1.5;
		lifeTimeVar = 2;
		onTimerScript = "";
		beforeDestroyScript = "";
		onSurface = false;
		bounceOnSurface = 0.8;
		bounceOnSurfaceVar = 0.2;
		animationSpeed[] = {0.4};//animationSpeed[] = {1.5};
		animationSpeedCoef = 1;
		moveVelocity[] = {0,0,0};
		moveVelocityVar[] = {0.1, 0.5, 0.1};
		positionVar[] = {0.15, 0, 0.15};
		size[] = {2.1, 1.4, 0.5};
		sizeCoef = 1;
		weight = 0.048;//weight = 0.045;
		volume = 0.04;
	
	};
	//sparkles
	class Molotov_Fire : Default {
        interval = 0.6;
        circleRadius = 0;
        circleVelocity[] = {0, 0, 0};
        particleFSNtieth = 16;
        particleFSIndex = 10;
        particleFSFrameCount = 32;
        particleFSLoop = 1;
        angleVar = 0.1;
        particleShape = "\A3\data_f\ParticleEffects\Universal\Universal";
        animationName = "";
        particleType = "Billboard";
        timerPeriod = 1;
        lifeTime = 4.2;
        moveVelocity[] = {0, 1.5, 0};
        rotationVelocity = 0;
        weight = 0.045;
        volume = 0.04;
        rubbing = 0.1;
        size[] = {0.2, 0.02};
        color[] = {{1, 1, 1, -1}};
        animationSpeed[] = {0.5, 1};
        randomDirectionPeriod = 0;
        randomDirectionIntensity = 0;
        onTimerScript = "";
        beforeDestroyScript = "";
        lifeTimeVar = 0.3;
        positionVar[] = {0.1, 0.3, 0.1};
        moveVelocityVar[] = {0.3, 0.5, 0.3};
        rotationVelocityVar = 10;
        sizeVar = 0.5;
        colorVar[] = {0.1, 0.1, 0.1, 0};
        randomDirectionPeriodVar = 0;
        randomDirectionIntensityVar = 0;
        coreIntensity = 3.5;
        coreDistance = 1.2;
        damageTime = 0.01;
        constantDistance = 1.5;
        damageType = "Fire";
    };
	
	class LIB_MolotovFire1: Molotov_Fire {
		interval = 0.1;
        lifeTime = 3.2;
        weight = 1.276;
        size[] = {0.1, 0.02};
        volume = 1;
        rubbing = 0;
        coreIntensity = 2;
        coreDistance = 1;
	};
	

	class LIB_MolotovFire_trail: ObjectDestructionFire1Small {
		particleShape="warfxpe\ParticleEffects\Universal\largefire_01";
		blockAIVisibility = 0;
		damageType = "";
		damageTime = 0;
		coreDistance = 0;
	 	coreIntensity = 0;
	 	circleRadius = 0;
	 	lifeTime = 1.5;
	 	lifeTimeVar =1;
	 	onTimerScript = "";
	 	beforeDestroyScript = "";
		onSurface = false;
	 	bounceOnSurface = 0.1;
	 	bounceOnSurfaceVar = 0.2;
	 	animationSpeed[] = {1};
	 	animationSpeedCoef = 1;
		moveVelocity[] = {0,0,0};
	 	size[] = {0.9, 0};
	 	sizeCoef = 1;
	 	weight = 0.065;			             //weight of particle (kg)
    	volume = 0.05;
	 };

	 class AutoCannonFired;
	 class LIB_MolotovSmoke_trail: AutoCannonFired {
		particleShape="warfxpe\ParticleEffects\Universal\smoke_01";
		blockAIVisibility = 0;
	 	circleRadius = 0;
	 	lifeTime = 1.5;
	 	lifeTimeVar =1;
	 	onTimerScript = "";
	 	beforeDestroyScript = "";
	 	onSurface = false;
	 	bounceOnSurface = 0.1;
	 	bounceOnSurfaceVar = 0.2;
	 	animationSpeed[] = {1};
	 	animationSpeedCoef = 1;
	 	moveVelocity[] = {0,0,0};
	 	size[] = {0.9, 0};
	 	sizeCoef = 1;
	 	weight = 0.065;			             //weight of particle (kg)
		volume = 0.05;
	};
};

class LIB_Molotov_Effects {
	class Smoke1 {
		intensity = 1;
		interval = 1;
		position[] = {0, 0, 0};
		simulation = "particles";
		type = "LIB_MolotovSmoke1";
	};
	class Fire1 {
		intensity = 1;
		interval = 1;
		position[] = {0, 0, 0};
		simulation = "particles";
		type = "LIB_MolotovFire";
	};
	class Fire2 {
		intensity = 1;
		interval = 1;
		position[] = {0, 0, 0};
		simulation = "particles";
		type = "LIB_MolotovFire1";
	};
	class Refract1 {
		intensity = 1;
		interval = 1;
		position[] = {0, 0, 0};
		simulation = "particles";
		type = "LIB_MolotovRefract";
	};
	class Light1 {
        simulation = "light";
        type = "FiredLightMed";
        position[] = {0, 0, 0};
        intensity = 0;
        interval = 1;
        lifeTime = 30;
    };
};

class LIB_Molotov_Effects1 {
	class Smoke1 {
		intensity = 0.5;
		interval = 1;
		position[] = {0, 0, 0};
		simulation = "particles";
		type = "LIB_MolotovSmoke1";
	};
	class Fire1 {
		intensity = 1;
		interval = 1;
		position[] = {0, 0, 0};
		simulation = "particles";
		type = "LIB_MolotovFire1";
	};
};
