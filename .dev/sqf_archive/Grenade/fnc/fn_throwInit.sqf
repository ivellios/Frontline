/*
    Function:       LIB_Grenade_fnc_throwInit
    Author:         Adanteh
    Description:    Handles adding event handler on init
*/

[] spawn {
	waitUntil {!(isNull player)};
	waitUntil {player == player};
	player addEventHandler ["fired", LIB_Grenade_fnc_throwHandler];
};
