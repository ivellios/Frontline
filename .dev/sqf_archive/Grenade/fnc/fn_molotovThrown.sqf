/*
    Function:       LIB_Grenade_fnc_molotovThrown
    Author:         Adanteh
    Description:    Handles molotov explosion and fire
*/

_this spawn {
	params ["", "", "", "", "", "", "_projectile"];

	private _posOriginal = [];
	waitUntil {
		if (alive _projectile) then {
			_posOriginal = getPos _projectile;
			false
		} else {
			true
		};
	};

	if ((count _posOriginal) == 0) exitWith {};
	// -- Big long fire
	for "_i" from 0 to (6) do {
		private _ugol = (random 360);
		private _speed = (random 5) min 3;
		private _height = 1 + (random 8);
		private _pos = +(_posOriginal apply { (_x + (random 0.1 - 0.05)) });
		_pos set [2, (_pos select 2) + 0.1];

		private _bullet = "LIB_T_MolotovAmmo_fragment" createVehicle _pos;
		_bullet setVelocity [(_speed*(sin _ugol)), (_speed*(cos _ugol)), (random _height)];
		_bullet spawn {
			sleep (5 max (random 45));
			deleteVehicle _this;
		};
	};

	// -- Small shorter fire
	for "_i" from 0 to (5 max (ceil(random 15))) do {
		private _ugol = (random 360);
		private _speed = (random 6) min 5;
		private _height = 2 + (random 10);
		private _pos = +(_posOriginal apply { (_x + random 0.1 - 0.05) });
		_pos set [2, (_pos select 2) + 0.1];

		private _bullet = "LIB_T_MolotovAmmo_fragment" createVehicle _pos;
		_bullet setVelocity [(_speed*(sin _ugol)), (_speed*(cos _ugol)), (random _height)];
		_bullet spawn {
			sleep (5 max (random 45));
			deleteVehicle _this;
		};
	};
};
