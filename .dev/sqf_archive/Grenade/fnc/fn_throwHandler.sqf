/*
    Function:       LIB_Grenade_fnc_throwHandler
    Author:         Adanteh
    Description:    Handles throw weapon effects, call function connected to ammo type
*/

params ["_object", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile"];
call {
    if (_weapon == "throw") exitWith {
        private _grenadeFunction = getText (configFile/"cfgAmmo"/_ammo/"LIB_throwFunction");
		if (_grenadeFunction != "") exitWith {
			_this call (missionNamespace getVariable [_grenadeFunction, {}]);
		};
    };
};
