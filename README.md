# README - Frontline #

Frontline is an up and coming PvP teamwork orientated mod which currently is in testing phase, it draws inspiration from the Project Reality mod, Squad and Tactical Battlefield.
Frontline is a full overhaul mod which encourages the highest levels of team-play whilst not being strictly a mil-sim mod.

Custom features are including, but not limited to:
* Role and Group system
* Modified TFAR version
* Suppression
* Squad Radar
* Spotting System
* Medical/Revive module
* Weapon Rebalance (Reworked recoil, balanced scopes)
* Particle Overhaul (Better smoke, with less FPS impact)
* Rally points and Forward Outposts
* 3D Interaction (Better door opening, ladder climbing)

http://discord.frontline-mod.com


# WUT IS THIS? #
This is mainly a project meant to have a well balanced PvP mod with mechanics that work together well.
A lot of mods seem to just piece together lots of bloated with broken SQF to get something functional,
with a codebase that's absolutely horrendous and mechanics that are in there for no other reason than 'Why not?'
We do try to provide a full overhaul experience, rather than just looking like a regular gamemode someone pieced together in A3 editor.
It is also based around public TEAMPLAY-based gameplay. If you're looking for a 360noscope thing where you might as well play solo, this is not it.
If you're looking for the ultimate milsim experience with organized events and super realistic mechanics, this is also not it.


It's not the epitome of absolutely amazing SQF and there's definitely parts that could use a cleanup,
and the quantity of the code in it could be frightening, but it should be easy enough to only focus on some stuff,
without needing to worry about the entire code base. Some stuff might be done in illogical ways,
it might not all be terribly consistent, but I try to do what I could.


If there's a serious interest for people to help, help can be given to set it up,
but don't expect hours of our time. First give the development setup below a try.
Also check out the CONTRIBUTING.md file for a list of tasks


# DEVELOPMENT #
### How to set up dev environment? ###
Check out *Tools\Readme.md* for better info
This is purely for creating a dev version of the mod, for local and MP testing. To make an actual release version check out [The Releast List page instead](release-list)

----------


### How does it all work? ###
Both Clib and everything in Frontline/server is **only** present on the server.
* **Clib** is the basic framework that loads modules and sends the code to clients, plus it has library fuctions like the event system, perFrame and lots more
* **Frontline**

**The *server* pbo does not get distributed. This one is only used in the server mod. To test missions and so on you will need the server mod**

This means that everything that requires client-side changes, Like config changes, pictures, sounds or things along these lines) then it will need to be a local module instead. In some cases, like only needing a simple UI config you can drop it Client folder instead, however for modules that really work fully independent **and** need serious client changes, make a local module out of it instead. As of right now (2017-03-05) two examples of local modules are:
* Suppression: Has special client-side scripts to trigger suppression on explosions, plus sounds
* Medical System: Has extra configs, pictures, sounds and more

Unless you really don't want people to take a look at the code, generally speaking you can just make it a local module and be save with it.
