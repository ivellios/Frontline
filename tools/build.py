#!/usr/bin/env python3

import os
import sys
import subprocess

from common.colorsLog import color, print_error, print_green, print_magenta, print_blue, print_yellow, print_grey
######## GLOBALS #########
MAINPREFIX = "pr"
PREFIX = "FRL_"
##########################

def mod_time(path):
    if not os.path.isdir(path):
        return os.path.getmtime(path)
    maxi = os.path.getmtime(path)
    for p in os.listdir(path):
        maxi = max(mod_time(os.path.join(path, p)), maxi)
    return maxi


def check_for_changes(filepaththingie, module):
    if not os.path.exists(os.path.join(filepaththingie, "{}{}.pbo".format(PREFIX,module))):
        return True
    return mod_time(os.path.join(filepaththingie, module)) > mod_time(os.path.join(filepaththingie, "{}{}.pbo".format(PREFIX,module)))

def check_for_obsolete_pbos(filepaththingie, file):
    module = file[len(PREFIX):-4]
    if not os.path.exists(os.path.join(filepaththingie, module)):
        return True
    return False

def build_da_pbo(filepaththingie,prefix):

    global made, failed, skipped, removed

    print_green("\n# Building from {}".format(filepaththingie))
    os.chdir(filepaththingie)
    for p in os.listdir(filepaththingie):
        path = os.path.join(filepaththingie, p)
        if not os.path.isdir(path):
            continue
        if p[0] == ".":
            continue
        if not check_for_changes(filepaththingie, p):
            skipped += 1
            print_grey("  Skipping {}.".format(p))
            continue

        nopackfile = os.path.join(path, "$NOPACK$")
        if os.path.isfile(nopackfile):
            print_grey("  No pack {}.".format(p))
            if os.path.isfile("{}{}.pbo".format(prefix,p)):
                os.remove("{}{}.pbo".format(prefix,p))
            continue

        print("# Making {} ...".format(p))

        try:
            subprocess.check_output([
                "makepbo",
                "-NUP",
                p,
                "{}{}.pbo".format(prefix,p)
            ], stderr=subprocess.STDOUT)
        except:
            failed += 1
            print_error("  Failed to make {}.".format(p))
        else:
            made += 1
            print("  Successfully made {}.".format(p))

def main():
    print_yellow("""
  ###################
  # Frontline Build #
  ###################
""")


    scriptpath = os.path.realpath(__file__)
    projectpath = os.path.dirname(os.path.dirname(scriptpath))
    addonspath = os.path.join(projectpath, "addons")

    global made, failed, skipped, removed
    made = 0
    failed = 0
    skipped = 0
    removed = 0

    build_da_pbo(addonspath, "FRL_")
    build_da_pbo(os.path.join(projectpath, "servermods\\Clib\\addons"), "")

    print_green("\n# Done.")
    print("  Made {}, skipped {}, removed {}, failed to make {}.".format(made, skipped, removed, failed))

    # repl = input("Press ANY key to exit")

if __name__ == "__main__":
    sys.exit(main())
