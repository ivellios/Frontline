#!/usr/bin/env python3

#######################
#  Frontline Setup Script  #
#######################

import sys
req_version = (3,5)
cur_version = sys.version_info
if cur_version < req_version:
   print("You're using an incorrect Python version.\nPlease get the latest 3+ version on python.org/downloads.")
   input("\nPress enter to exit ...")
   sys.exit(0)

import os
import shutil
import platform
import subprocess
import winreg
import ctypes

from common.colorsLog import color, print_error, print_green, print_magenta, print_blue, print_yellow, print_grey

######## GLOBALS #########
MAINDIR = "pr"
TCDIR = "tc"
PROJECTDIR = "frl"
##########################

def create_link(link_name, target_element):
    print('Creating link: {} => {}'.format(link_name, target_element))
    return subprocess.call(["cmd", "/c", "mklink", "/D", "/J", link_name, target_element])

def install_tools():
    import installMikero
    installMikero.install()

def main():
    FULLDIR = "{}\\{}".format(MAINDIR,PROJECTDIR)
    print_yellow("""
  ###########################################
  # Frontline Development Environment Setup #
  ###########################################

  This script will create your Frontline dev environment for you.
  This script will create two hard links on your system, both pointing to your Frontline project folder:\n""")
    print("[Arma 3]\\{}               => Frontline project".format(FULLDIR))
    print("[Arma 3]\\{}\\Clib              => Clib Addons".format(TCDIR))
    print("[Arma 3]\\Serverconfig         => Frontline serverconfig")
    print("[Missions]\\Frontline_dev      => Frontline dev missions")
    print("\n")

    try:
        reg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
        key = winreg.OpenKey(reg,
                r"SOFTWARE\Wow6432Node\bohemia interactive\arma 3")
        armapath = winreg.EnumValue(key,1)[1]
    except:
        print_error("Failed to determine Arma 3 Path.")
        return 1

    from ctypes.wintypes import MAX_PATH
    dll = ctypes.windll.shell32
    buf = ctypes.create_unicode_buffer(MAX_PATH + 1)
    if dll.SHGetSpecialFolderPathW(None, buf, 0x0005, False):

        try:
            # Get the normal user name
            regmission = winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER)
            keymission = winreg.OpenKey(regmission,
                    r"SOFTWARE\bohemia interactive\arma 3")
            armauser = winreg.EnumValue(keymission,0)[1]
            missionpath = os.path.join(buf.value, 'Arma 3 - Other Profiles', armauser, 'missions')
        except:
            missionpath = os.path.join(buf.value, 'Arma 3', 'missions')

    scriptpath = os.path.realpath(__file__)
    projectpath = os.path.dirname(os.path.dirname(scriptpath))

    print_green("# Detected Paths:")
    print("  Arma 3 Path:   {}".format(armapath))
    print("  Missions Path: {}".format(missionpath))
    print("  Project Path:  {}".format(projectpath))

    repl = input("\nAre these correct? (y/n): ")
    if repl.lower() != "y":
        return 3

    print_green("\n# Creating links ...")

    try:
        if not os.path.exists(os.path.join(armapath, MAINDIR)):
            os.mkdir(os.path.join(armapath, MAINDIR))

        if not os.path.exists(os.path.join(armapath, TCDIR)):
            os.mkdir(os.path.join(armapath, TCDIR))

        if not os.path.exists(os.path.join(armapath, 'serverconfig')):
            os.mkdir(os.path.join(armapath, 'serverconfig'))

        create_link(os.path.join(armapath, MAINDIR, PROJECTDIR), projectpath)
        create_link(os.path.join(armapath, 'serverconfig', 'Frontline'), os.path.join(projectpath, 'serverconfig', 'Frontline'))
        create_link(os.path.join(missionpath, 'Frontline_Dev'), os.path.join(projectpath, 'missions_dev'))

        create_link(os.path.join(armapath, TCDIR, 'Clib'), os.path.join(projectpath, 'servermods\clib'))
        create_link(os.path.join(projectpath, 'addons', 'radio'), os.path.join(projectpath, 'modules', 'frontline_radio', 'addons', 'radio'))

    except:
        raise
        print_error("Something went wrong during the link creation. Please finish the setup manually.")
        return 6

    print_green("# Links created successfully.")

    install_tools()

    return 0


if __name__ == "__main__":
    exitcode = main()

    if exitcode > 0:
        print_error("\nSomething went wrong during the setup. Make sure you run this script as administrator.")
    else:
        print("\nSetup successfully completed.")

    input("\nPress enter to exit ...")
    sys.exit(exitcode)
