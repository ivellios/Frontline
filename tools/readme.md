# The 'I have no idea what im doing' basics
You need python for all these scripts. Either get [@Pythia](https://github.com/overfl0/Pythia/releases) and use the packaged in Python version,
I recommend this because you'll need it either way for testing DFL gamemode

Open up a CMD window in your frontline repo tools folder (Shift+right click in empty space within explorer and do Open Command Window / Open Powershell Window)
Alternatively you can just open up CMD and use `cd <path to repo>\tools`


## Setup
```
python .\setup.py
```
After this in your arma folder you should have `tc\clib` and `pr\frl` present
After you build this is what you want to launch as mod. Use something like Arma3Sync for dev launching
Otherwise use a shortcut wtih `-mod=\tc\Clib;\pr\frl;`

## Building
This is for local testing copies only

```
python .\build.py
```

This will build any updated PBOs for Clib and Frontline. This is what you can use to run a server
**Keep in mind that by default the IFA_- and RHS_compat aren't build, delete the $NOPACK$ file in their folders**
to make sure these get build. Keep in mind if you add new files you will need to rebuild and that your game isnt running

** In case you want to go full devmode with faster spawning types, function recompiling and role restriction bypasses,
go to `addons\main\ISDEV.hpp` and uncomment the text. DON'T COMMIT THIS **



## Testing
* Get [Arma3Sync](ftp://www.sonsofexiled.fr/ArmA3/ArmA3Sync/releases) (You can launch manually, but Arma3Sync is recommended)
* Make a new addon group (Right click > Add Group) in the right pane on Addons tab
* Add *tc\clib* and *pr\frl* into that group
* Under launcher options make sure *File Patching* and *Show Script errors* are enabled
* Press Start Game
* Go Into Editor, Load Mission and pick one from the Frontline_dev subfolder

** To enable function recompiling (Changing functions without restarting) you need to uncomment #define ISDEV in two files:
`addons\main\ISDEV.hpp` and `servermods\CLib\addons\Clib\CLib_macros.hpp`
** To bypass squad limitations, spawn timer and a lot more uncomment `#define DEBUFULL` in `addons\main\ISDEV.hpp`

#### File patching ####
You can use *-filePatching* as startup parameter for A3 to reload PBO contents on mission restart. Only files that already exist within a PBO exist can be accessed. If you create a new file within an addon you need to repack the PBO with *build.py* for the file to be available even when using file patching. Config files are only compiled on game start, so if you make changes in a config file you need to restart the game to have any effect (You don't need to repack it if you use file patching)

** I HIGHLY RECOMMEND USING FILE PATCHING**, because it saves you a ton of time constantly restarting and rebuilding the mod. If you used setup.py (Which you should have) then all the symlinks are in place and the only thing you need to do to use file patching, is launch with the -filePatching option enabled


## Making
This is for release copies, both server and client. You will need A3 Tools installed for this and P drive mounted

```
python .\make.py
```

This will create @Clib_server, @Frontline and @Frontline_server in the release folder.
In case of serious updates make sure you create a new signature (DSUtils in A3 tools, put the path on `<frontline-repo>\key_private\Frontline.biprivatekey` and press N)
Generally speaking you should just be using the deploy script instead, because that'll automatically upload it

** MAKE SURE ISDEV is turned off. `addons\main\ISDEV.hpp` and `servermods\clib\addons\clib\clib_macros.hpp` **




## Deploy

##### First deployment
You first need to create an SSH keypair. If you don't have one already, run the following:
```
python .\create_key.py
```

This will create a new keypair and store them in `C:\users\<username>\.ssh` on windows an in `~/.ssh` on Linux.
Once you have those SSH keys, contact someone with access to the game server and give him your **public** key contents
(`id_rsa.pub`). After they place it on the server, you're good to go!

##### Regular deployment

The following command will:
1. Build Frontline from scratch (using `make.py`).
2. Push all the changes to both RHS and IFA server. This is important because both of IFA and RHS compat files that are
created in the process.
3. Update the Dynamic Frontline on the server side
4. Restart the server
5. Update the launcher

```
python .\deploy.py rhs ifa
```

To deploy to all 3 servers available, run:
```
python .\deploy.py rhs ifa dev
```

If you want to check for additional options, run:
```
python .\deploy.py --help
```

For deployment to dev server for testing purposes
```
python .\deploy.py dev --noworkshop
```
