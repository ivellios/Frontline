import argparse
import os
import shutil
import subprocess
import time

from datetime import datetime

import sys

from common import steam
from common.deployment import Deployment
from common.colorsLog import print_blue, print_error
from common.launcher_deployment import perform_update, DummyMessageQueue
from common.makeTorrent.maketorrent import mktorrent
from common.rsync import rsync
from common.settings import *
from common.workshop_deployment import BISPublisher

FRONTLINE_DIRS_WITH_DEV =[
    '@Frontline',
    '@Frontline_compat_IFA',
    '@Frontline_compat_RHS',
]

FRONTLINE_OTHER_DIRS = [
    '@Clib',
    '@Frontline_server',
]

FRONTLINE_DIRS = (
    FRONTLINE_OTHER_DIRS +
    FRONTLINE_DIRS_WITH_DEV +
    [i + '_dev' for i in FRONTLINE_DIRS_WITH_DEV]  # Dirs with _dev appended
)


def ensure_dir_deleted(path):
    try:
        shutil.rmtree(path)
    except FileNotFoundError:
        pass


def get_release_dir(*path):
    return os.path.join(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'release')), *path)


def get_tools_dir(path):
    return os.path.join(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'tools')), path)


def build():
    dirs_to_delete = FRONTLINE_DIRS
    for directory in dirs_to_delete:
        ensure_dir_deleted(get_release_dir(directory))

    subprocess.check_call(['python', get_tools_dir('make.py'), '--ci', '--newkey'])
    for directory in FRONTLINE_DIRS_WITH_DEV:
        shutil.copytree(get_release_dir(directory), get_release_dir(directory + '_dev'))


class FrontlineDeployment(Deployment):
    def before_deploy(self):
        for directory in FRONTLINE_DIRS:
            if not os.path.isdir(get_release_dir(directory)):
                raise RuntimeError('Directory {} is missing from the release directory, aborting deploy!'.format(directory))

    def deploy_to_server(self, server):
        dev_suffix = '_dev' if server.get('dev', False) else ''

        self.rsync(get_release_dir('@Frontline_server') + os.path.sep,
                   "{}@{}:'{}/@Frontline_server/'".format(USERNAME, HOST, server['cygwin_path']))
        self.rsync(os.path.join(get_release_dir('@Frontline_server'), 'keys', 'Frontline.bikey'),
                   "{}@{}:'{}/Keys/'".format(USERNAME, HOST, server['cygwin_path']))
        self.rsync(get_release_dir('@Frontline_compat_IFA{}'.format(dev_suffix)) + os.path.sep,
                   "{}@{}:'{}/@Frontline_compat_IFA{}/'".format(USERNAME, HOST, server['cygwin_path'], dev_suffix))
        self.rsync(get_release_dir('@Frontline_compat_RHS{}'.format(dev_suffix)) + os.path.sep,
                   "{}@{}:'{}/@Frontline_compat_RHS{}/'".format(USERNAME, HOST, server['cygwin_path'], dev_suffix))
        self.rsync(get_release_dir('@Clib') + os.path.sep,
                   "{}@{}:'{}/@Clib_server/'".format(USERNAME, HOST, server['cygwin_path']))


def create_timestamp(epoch):
    """
    Create a timestamp that looks like this:
    YYYY-MM-DD_Epoch
    """
    return datetime.fromtimestamp(int(epoch)).strftime('%Y-%m-%d_') + str(int(epoch))


class FakeMod(object):
    def __init__(self, name):
        self.foldername = name


def create_torrents(regular_server, dev_server):
    torrents = []
    # mod, local_file_path, file_name, timestamp

    torrent_names = []
    if regular_server:
        torrent_names.extend(FRONTLINE_DIRS_WITH_DEV)

    if dev_server:
        torrent_names.extend(i + '_dev' for i in FRONTLINE_DIRS_WITH_DEV)

    for name in torrent_names:
        print('Creating torrent for: {}...'.format(name))

        timestamp = create_timestamp(time.time())
        file_name = '{}-{}.torrent'.format(name, timestamp)
        with open(file_name, 'wb') as f:
            mktorrent(path=get_release_dir(name),
                      outfile=f,
                      tracker=TORRENT_TRACKER_URLS,
                      webseeds=TORRENT_WEB_SEEDS,
            )
        torrents.append((FakeMod(name), f.name, file_name, timestamp))

    return torrents


def callback(mods_created):
    print('Calling callback!')
    for mod, local_file_path, file_name, timestamp in mods_created:
        print('Pushing {}'.format(mod.foldername))
        source_dir = get_release_dir(mod.foldername) + os.path.sep
        destination = "{}@{}:'{}/{}/'".format(
            LAUNCHER_SERVER_USERNAME,
            LAUNCHER_SERVER_HOST,
            LAUNCHER_SERVER_MODS_PATH,
            mod.foldername,
        )
        rsync(source_dir, destination, key_path=PRIVATE_KEY, permissions='Du=rwx,Dgo=rx,Fu=rw,Fog=r')


def remove_torrents(mods_created):
    for mod, local_file_path, file_name, timestamp in mods_created:
        os.unlink(local_file_path)


def publish_to_launcher(regular_server, dev_server):
    print_blue('====================================================================')
    print_blue('Deploying to: launcher server')
    print_blue('====================================================================')
    torrents = create_torrents(regular_server, dev_server)
    perform_update(DummyMessageQueue(), torrents, callback)
    remove_torrents(torrents)


def publish_to_workshop(release_dir, regular_server, dev_server):
    print_blue('====================================================================')
    print_blue('Deploying to: Steam Workshop')
    print_blue('====================================================================')
    steam.ensure_user_logged(STEAM_LOGIN, STEAM_PASSWORD)
    publisher = BISPublisher(release_dir)
    if regular_server:
        retval = publisher.publish('@Frontline', WORKSHOP_FRONTLINE_ID)
        retval = publisher.publish('@Frontline_compat_IFA', WORKSHOP_FRONTLINE_COMPAT_IFA_ID)
        retval = publisher.publish('@Frontline_compat_RHS', WORKSHOP_FRONTLINE_COMPAT_RHS_ID)

    if dev_server:
        # TODO: set up the workshop IDs for dev versions of the mods
        # retval = publisher.publish('@Frontline', WORKSHOP_FRONTLINE_ID)
        # retval = publisher.publish('@Frontline_compat_IFA', WORKSHOP_FRONTLINE_COMPAT_IFA_ID)
        # retval = publisher.publish('@Frontline_compat_RHS', WORKSHOP_FRONTLINE_COMPAT_RHS_ID)
        pass

    steam.shutdown()


def main(args):
    if not args.forcedev:
        dev_server = 'dev' in args.server
        regular_server = any(server != 'dev' for server in args.server)
    else:
        dev_server = True
        regular_server = False

    if not args.nodeploy and not args.noworkshop:
        if not STEAM_PASSWORD:
            print_error('You need to set STEAM_PASSWORD in settings_local.py or run with --noworkshop!')
            sys.exit(1)

    if not args.nobuild:
        build()

    if not args.nodeploy:
        frontline_deployment = FrontlineDeployment(HOST, USERNAME, PRIVATE_KEY)
        frontline_deployment.deploy([SETTINGS[server] for server in args.server])

    if not args.noworkshop:
        publish_to_workshop(get_release_dir(), regular_server, dev_server)

    if not args.nolauncher:
        publish_to_launcher(regular_server, dev_server)

    print_blue('====================================================================')
    print_blue('Deployment done!')
    print_blue('====================================================================')


def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg # open(arg, 'r')  # return an open file handle


if __name__ == '__main__':
    default_private_key = os.path.join(os.path.expanduser('~'), '.ssh', 'id_rsa')

    parser = argparse.ArgumentParser(description='Deployment script for the dev server')
    parser.add_argument('-n', '--nobuild', help='Don\'t build Frontline using make.py', action='store_true')
    parser.add_argument('--nodeploy', help='Don\'t deploy Frontline to the server', action='store_true')
    parser.add_argument('--nolauncher', help='Don\'t deploy Frontline to the launcher', action='store_true')
    parser.add_argument('--noworkshop', help='Don\'t deploy Frontline to the workshop', action='store_true')
    parser.add_argument('--forcedev', help='Force creation of _dev mod version', action='store_true')
    # parser.add_argument('--key', help='Private ssh key to establish an ssh connection',
    #                     type=lambda x: is_valid_file(parser, x),
    #                     default=default_private_key)
    parser.add_argument('server', help='On which server to deploy', choices=SETTINGS.keys(), nargs='+')
    args = parser.parse_args()
    # PRIVATE_KEY = args.key
    PRIVATE_KEY = default_private_key


    main(args)
