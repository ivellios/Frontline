#!/usr/bin/env python3

import sys

import os
import os.path
import shutil
import errno
import glob
import subprocess
import configparser
import re

from common.bis_tools import GetBISTools
from common.colorsLog import color, print_error, print_green, print_magenta, print_yellow, print_grey

###############################################################################

######## GLOBALS #########
release_dir = ""
make_root_parent = ""
key_name = "Frontline"
key = ""
importantFiles = ["mod.cpp", "meta.cpp", "CONTRIBUTING.md", "LICENSE.md", "AUTHORS.md", "mod.paa", "frontline_radio.ts3_plugin"]
importantServerFiles = ["mod.cpp", "meta.cpp", "CONTRIBUTING.md", "LICENSE.md", "AUTHORS.md", "mod.paa"]
##########################


def purge(dir, pattern, friendlyPattern="files"):
    print_green("Deleting {} files from directory: {}".format(friendlyPattern,dir))
    for f in os.listdir(dir):
        if re.search(pattern, f):
            os.remove(os.path.join(dir, f))

def mod_time(path):
    if not os.path.isdir(path):
        return os.path.getmtime(path)
    maxi = os.path.getmtime(path)
    for p in os.listdir(path):
        maxi = max(mod_time(os.path.join(path, p)), maxi)
    return maxi


def check_for_changes(addonspath, module):
    if not os.path.exists(os.path.join(addonspath, "{}{}.pbo".format(pbo_name_prefix,module))):
        return True
    return mod_time(os.path.join(addonspath, module)) > mod_time(os.path.join(addonspath, "{}{}.pbo".format(pbo_name_prefix,module)))

def check_for_obsolete_pbos(addonspath, file):
    module = file[len(pbo_name_prefix):-4]
    if not os.path.exists(os.path.join(addonspath, module)):
        return True
    return False


def ensure_directory_deleted(path):
    """Try deleting the directory and don't fail if the directory doesn't exist."""
    try:
        shutil.rmtree(path)

    except OSError as ex:
        if ex.errno != errno.ENOENT:  # File does not exist
            raise


def move_file(src_name, dest_name):
    try:
        os.makedirs(os.path.dirname(dest_name))

    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise

    os.rename(src_name, dest_name)


def extract_compat_mods(release_dir, project, addonsdir):
    """@Frontline\addons\frl_ifa_compat.pbo => @Frontline_compat_IFA\Addons\frl_ifa_compat.pbo"""

    if not project == "@Frontline":
        return

    compat_mods_data = {
        '{}_compat_IFA': [(
            os.path.join('addons', 'frl_ifa_compat.pbo'),
            os.path.join('addons', 'frl_ifa_compat.pbo.Frontline.bisign'),
        ), (
            os.path.join('ifa_compat', 'meta.cpp'),
            os.path.join('ifa_compat', 'mod.cpp'),
        )],
        '{}_compat_RHS': [(
            os.path.join('addons', 'frl_rhs_compat.pbo'),
            os.path.join('addons', 'frl_rhs_compat.pbo.Frontline.bisign'),
        ), (
            os.path.join('rhs_compat', 'meta.cpp'),
            os.path.join('rhs_compat', 'mod.cpp'),
        )],
    }

    print_green('\nExtracting compat mods from the release...')
    src_dir = os.path.join(release_dir, project)

    for compat_mod_name, compat_mod_files in compat_mods_data.items():
        compat_pbos, compat_meta = compat_mod_files
        print(compat_mod_name.format(project))

        compat_dest_dir = compat_mod_name.format(os.path.join(release_dir, project))
        print_green('Purging {}'.format(compat_dest_dir))
        ensure_directory_deleted(compat_dest_dir)

        for compat_file in compat_pbos:
            src_file = os.path.join(src_dir, compat_file)
            dst_file = os.path.join(compat_dest_dir, compat_file)
            print_grey('Move file => {}'.format(dst_file))
            move_file(src_file, dst_file)


        for compat_metafile in compat_meta:
            src_file = os.path.join(addonsdir, compat_metafile)
            dst_file = os.path.join(compat_dest_dir, os.path.basename(compat_metafile))
            print_grey('Copying file => {}'.format(dst_file))
            shutil.copyfile(src_file, dst_file)


class BuildMod():
    def __init__(self, configName):
        cfg = configparser.ConfigParser();

        try:
            cfg.read(os.path.join(make_root_parent, "tools", 'make.cfg'))
            self.buildthis = cfg.get(configName, "build", fallback="True") == "True"
            self.project = cfg.get(configName, "project", fallback="@"+configName)
            self.prefix = cfg.get(configName, "pbo_name_prefix", fallback="")
            self.projectpath = cfg.get(configName, "projectpath", fallback="addons")
            self.mode = cfg.get(configName, "mode", fallback="both")
            self.keypath = cfg.get(configName, "keypath", fallback="")
            self.binarize = cfg.get(configName, "binarize", fallback="True")
            self.signfiles = cfg.get(configName, "binarize", fallback="True")
        except:
            print_error("Problem loading make.cfg for project {}".format(configName))
            raise

        self.key = os.path.join(make_root_parent, self.keypath)
        self.newkey = False
        # print_yellow("{} - {} - {} - {}".format(self.project, self.prefix, self.projectpath, self.mode))

    def create_signing_key(self, bisTools):
        if not os.path.isfile(self.privatekey) or self.newkey:
            print_yellow("Key Path: {}".format(self.key))
            private_key_path = os.path.dirname(self.key)
            key_name = os.path.splitext(os.path.basename(self.privatekey))[0]

            print_yellow("Key Path: {}".format(private_key_path))
            print_yellow("Key name: {}".format(key_name))
            try:
                os.makedirs(private_key_path)
            except:
                pass
            curDir = os.getcwd()
            os.chdir(private_key_path)
            ret = subprocess.call([bisTools.dscreatekey, key_name])
            os.chdir(curDir)
            if ret == 0:
                print_green("Created: {}".format(os.path.join(private_key_path, key_name + ".biprivatekey")))
            else:
                print_error("Failed to create key!")
        else:
            print_green("\nNOTE: Using key {}".format(self.privatekey))

    def copy_important_files(self, source_dir, destination_dir):
        originalDir = os.getcwd()

        print_green("\nSearching for important files in {}".format(self.sourcepath))
        # Copy importantFiles
        try:
            print_magenta("Source_dir: {}".format(self.sourcepath))
            print_magenta("Destination_dir: {}".format(self.releasepath))
            print_magenta("Project: {}".format(self.project))
            if self.project == "@Frontline":
                extra_data = {
                    self.releasepath: importantFiles,
                    self.releasepathserver: importantServerFiles,
                }

                for targetpath, targetmodefiles in extra_data.items():
                    for file in targetmodefiles:
                        filePath = os.path.join(make_root_parent, file)
                        # Take only file name for destination path (to put it into root of release dir)
                        if "\\" in file:
                            count = file.count("\\")
                            file = file.split("\\", count)[-1]
                        print_grey("Copying file => {}".format(os.path.join(self.sourcepath, file)))
                        shutil.copyfile(os.path.join(self.sourcepath, filePath), os.path.join(targetpath, file))

        except:
            print_error("COPYING IMPORTANT FILES.")
            raise

        #copy all extension dlls
        try:
            if self.project == "@Frontline":
                os.chdir(make_root_parent)
                print_green("\nSearching for DLLs in {}".format(os.getcwd()))
                filenames = glob.glob("*.dll")

                if not filenames:
                    print ("Empty SET")


                for targetpath in {self.releasepath, self.releasepathserver}:
                    for dll in filenames:
                        print_grey("Copying dll => {}".format(os.path.join(self.sourcepath, dll)))
                        if os.path.isfile(dll):
                            shutil.copyfile(os.path.join(source_dir, dll), os.path.join(targetpath, dll))
        except:
            print_error("COPYING DLL FILES.")
            raise
        finally:
            os.chdir(originalDir)

    def build_a_mod(self, bisTools):
        if not self.buildthis:
            print_magenta("  Skipping build for {}\n".format(self.project))
            return

        self.releasepath = os.path.join(release_dir, self.project)
        self.sourcepath = os.path.join(make_root_parent, self.projectpath)
        self.privatekey = "{}.biprivatekey".format(self.key)
        self.create_signing_key(bisTools)

        print_yellow("Building project {} from {}".format(self.project, self.sourcepath))

        if os.path.isdir(self.releasepath):
            shutil.rmtree(self.releasepath)
        os.makedirs(os.path.join(self.releasepath, "addons"))
        os.makedirs(os.path.join(self.releasepath, "keys"))

        # Copy the key
        biKeyNameAbrev = os.path.basename(self.key).split("-")[0]
        shutil.copyfile(self.key + ".bikey", os.path.join(self.releasepath, "keys", "{}.bikey".format(biKeyNameAbrev)))

        # If we want both a client and server version, create a folder with _server append to copy all required PBOs
        if self.mode == "both":
            self.releasepathserver = os.path.join(release_dir, self.project + "_server")
            if os.path.isdir(self.releasepathserver):
                shutil.rmtree(self.releasepathserver)
            os.makedirs(os.path.join(self.releasepathserver, "addons"))
            os.makedirs(os.path.join(self.releasepathserver, "keys"))
            shutil.copyfile(self.key + ".bikey", os.path.join(self.releasepathserver, "keys", "{}.bikey".format(biKeyNameAbrev)))


        self.copy_important_files(make_root_parent, self.releasepath)
        os.chdir(self.sourcepath)
        for module in os.listdir(self.sourcepath):
            path = os.path.join(self.sourcepath, module)
            if not os.path.isdir(path):
                continue
            if module.startswith("."):
                continue
            if not os.path.isfile(os.path.join(path, "config.cpp")):
                continue

            modulepath = os.path.join(self.sourcepath, module)
            if self.mode == "server":
                # If building a server mod, exclude any files with a $NOSERVER$ file inside the folder
                if os.path.isfile(os.path.join(modulepath, "$NOSERVER$")):
                    continue
            if self.mode == "client":
                # If building a client mod, exclude any files with a $NOCLIENT$ file inside the folder
                if os.path.isfile(os.path.join(modulepath, "$NOCLIENT$")):
                    continue

            self.build_a_pbo(bisTools, module, modulepath)

        extract_compat_mods(release_dir, self.project, self.sourcepath)

    def build_a_pbo(self, bisTools, module, modulepath):

        if testmode and module not in ('ifa_compat', 'rhs_compat', 'bluehud', 'spotting'):
            print_magenta("  Skipping build for {} due to testmode\n".format(module))
            return 0

        print_green("\nMaking {} - {}".format(self.project, module + "-"*max(1, (60-len(module)))))

        # Build the module into a pbo
        # Detect LICENSE.MD and if so, copy it to the addons folder and later format into same name as the PBO
        destination = os.path.join(self.releasepath, "addons")
        if os.path.isfile(os.path.join(modulepath, "LICENSE.md")):
            do_add_license = True
            print("LICENSE file found in module, will copy.")
        else:
            do_add_license = False

        # Detect $NOBIN$ and do not binarize if found.
        do_binarize = False
        if self.binarize == "True":
            if not os.path.isfile(os.path.join(modulepath, "$NOBIN$")):
                do_binarize = True

        # Detect $NOBIN$ and do not binarize if found.
        do_sign = False
        if self.signfiles == "True":
            if not os.path.isfile(os.path.join(modulepath, "$NOSIGN$")):
                do_sign = True

        # Detect $NOBIN$ and do not binarize if found.
        temp_path = "-temp="+(os.path.join(bisTools.workdrive, "temp", self.prefix))
        if os.path.isfile(os.path.join(modulepath, "$PBOPREFIX$")):
            do_prefix = True
            prefix_file = open(os.path.join(modulepath, "$PBOPREFIX$"), 'r')
            prefix_path = prefix_file.readlines()[0]
            temp_path = "-temp="+(os.path.join(bisTools.workdrive, "temp", prefix_path))
        else:
            do_prefix = False

        try:
            # Call AddonBuilder
            os.chdir(bisTools.workdrive)
            project_path = "-project="+self.sourcepath
            include_path = "-include="+(os.path.join(make_root_parent, "tools", "include.txt"))
            exclude_path = "-exclude="+(os.path.join(make_root_parent, "tools", "exclude.lst"))

            files = []
            cmd = [bisTools.addonbuilder, modulepath, destination, "-clear", project_path, temp_path, include_path]
            # cmd = [bisTools.addonbuilder, modulepath, destination, "-clear", project_path, include_path]
            if not do_binarize:
                print_grey("  Skipping binarizing for this PBO, uses $NOBIN$ file or setting in make.cfg")
                cmd.append("-packonly")

            if do_sign:
                cmd.append("-sign="+self.privatekey)
            else:
                print_grey("  Skipping signature for this PBO, uses $NOSIGN$ file or setting in make.cfg")

            if do_prefix:
                cmd.append("-prefix="+prefix_path)
                print_grey("  Building with prefix {}".format(prefix_path))

            print_yellow(cmd)
            previousDirectory = os.getcwd()
            os.chdir(bisTools.tools_path)

            try:
                ret = subprocess.check_call(cmd)
            except:
                print_error("Module not successfully built. Check your {}\temp\{}_packing.log for more info.".format(bisTools.workdrive, module))

            os.chdir(previousDirectory)
            color("reset")

            # If there is a license file in the folder, copy it next to the PBO with a similar name
            if do_add_license:
                try:
                    license_filename = "{}_{}_LICENSE.md".format(self.prefix, module)
                    file_src = os.path.join(modulepath, "LICENSE.md")
                    file_dst = os.path.join(destination, license_filename)
                    shutil.copyfile(file_src, file_dst)
                    files.append(file_dst)

                except:
                    print_error("Could not copy license file.")
                    raise


            # Prettyprefix rename the PBO if requested.
            if self.prefix:
                pbonameprefixed = os.path.join(destination, "{}_{}.pbo".format(self.prefix, module))
                try:
                    os.rename(os.path.join(destination, "{}.pbo".format(module)), pbonameprefixed)
                    if do_sign:
                        os.rename(os.path.join(destination, "{}.pbo.Frontline.bisign".format(module)), os.path.join(destination, "{}_{}.pbo.Frontline.bisign".format(self.prefix, module)))
                        files.append(os.path.join(destination, "{}_{}.pbo.Frontline.bisign".format(self.prefix, module)))

                    files.append(pbonameprefixed)

                except:
                    print_error("Could not rename built PBO with prefix.")
                    raise
            else:
                pbonameprefixed = os.path.join(destination, "{}.pbo".format(module))

            # If buildling both a client and server version, copy to the _server appended release folder
            if ret == 0:
                if self.mode == "both":
                    if not os.path.isfile(os.path.join(modulepath, "$NOSERVER$")):
                        for file in files:
                            shutil.copy2(file, os.path.join(self.releasepathserver, "addons"))
                    if os.path.isfile(os.path.join(modulepath, "$NOCLIENT$")):
                        for file in files:
                            os.remove(file)

        except:
            print_error("Could not run Addon Builder.")
            raise

        # Back to the root
        os.chdir(make_root_parent)


def main(argv):
    global release_dir
    global make_root_parent
    global key_name
    global testmode

    if '--ci' in argv:
        ci_build = True
    else:
        ci_build = False

    if '--nobuild' in argv:
        build = False
    else:
        build = True

    if '--test' in argv:
        testmode = True
    else:
        testmode = False


    if '--newkey' in argv:
        newkey = True
    else:
        newkey = False

    message = '{} Build'.format('Frontline')
    print('  ##{}##\n'
          '  # {} #\n'
          '  ##{}##\n'.format('#' * len(message), message, '#' * len(message)))

    make_release_zip = True

    # Get the directory the make script is in.
    make_root_parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    print_grey("Basepath: {}".format(make_root_parent))
    release_dir = os.path.join(make_root_parent, "release")
    os.chdir(make_root_parent)

    # Find the tools we need.
    bisTools = GetBISTools()
    bisTools.find_bi_tools()

    build_frl = BuildMod("Frontline")
    build_frl.newkey = newkey

    if build:
        build_frl.build_a_mod(bisTools)

    build_clib = BuildMod("Clib")
    build_frl.newkey = newkey

    if build:
        build_clib.build_a_mod(bisTools)

    print("\n# Done.")
    if not ci_build:
        repl = input("Press ANY key to exit")


if __name__ == "__main__":
    sys.exit(main(sys.argv))
