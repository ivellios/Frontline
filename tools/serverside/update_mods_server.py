
ARMA_WORKSHOP_ID = 107410
STEAM_ACCOUNTNAME = "steamaccountname"
STEAM_PASSWORD = "enterpasswordpls"
STEAMCMD_PATH = "C:\\Users\\armaadmin\\Desktop\\SteamCMD\\steamcmd.exe"

"""Usage

Open up CMD in 'serverside' folder and use:
python -m update_mods_server rhs

"""


import os
import sys
import subprocess
import json
import argparse
import shutil
import glob

from imports.colorsLog import color, print_error, print_green, print_magenta, print_blue, print_yellow, print_grey

scriptpath = os.path.realpath(__file__)
dirpath = os.path.dirname(scriptpath)

with open(os.path.join(dirpath,'modlist.json')) as json_file:
    SERVER_LIST = json.load(json_file)

def update_mod(modname, modid, path):
    print_magenta("\nUpdating mod {} [{}]".format(modname, modid))

    modpath = os.path.join(path, "@{}".format(modname))
    steamcmd_params = (
        STEAMCMD_PATH,
        "+login {} {}".format(STEAM_ACCOUNTNAME, STEAM_PASSWORD),
        "+workshop_download_item {} {}".format(ARMA_WORKSHOP_ID, modid),
        "+quit"
    )

    try:
        copy_keys(modpath, path)
        _exitcode = subprocess.check_call(steamcmd_params)
    except subprocess.CalledProcessError:
        raise SteamcmdException("Steamcmd was unable to run")

def copy_keys(modpath, serverdir):
    dst_dir = os.path.join(serverdir, "keys")
    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir)

    for bikey in glob.iglob(os.path.join(modpath, "**", "*.bikey")):
        file_ = os.path.basename(bikey)
        print_yellow("Copying keys: {}".format(file_))
        dst_file = os.path.join(dst_dir, file_)
        if os.path.exists(dst_file):
            os.remove(dst_file)
        shutil.copy2(bikey, dst_dir)

def server_toggle(servicename, enable=False):
    if enable:
        print_green("\nStarting {}".format(servicename))
        subprocess.call(('net start "FireDaemon Service: {}"'.format(servicename)))
    else:
        print_green("\nStopping {}".format(servicename))
        subprocess.call(('net stop "FireDaemon Service: {}"'.format(servicename)))

def main(args):
    # First turn of all servers, in case of shared mods
    for server in args.server:
        server_toggle(SERVER_LIST[server]['servicename'], False)

    for server in args.server:
        serverdata = SERVER_LIST[server]
        path = serverdata['path']
        mods = serverdata['mods']
        print_green("\nUpdating {} in {}".format(server, path))

        for modname, modid in mods.items():
            update_mod(modname, modid, path)

    # Turn all servers back on
    for server in args.server:
        server_toggle(SERVER_LIST[server]['servicename'], True)
    return 0

if __name__ == "__main__":
    if STEAM_ACCOUNTNAME == "steamaccountname":
        print_error("Please enter a steam account name and password at the top of this file, needed to access workshop")
        input("\nPress enter to exit ... ")
        sys.exit(0)

    parser = argparse.ArgumentParser(description="Updates workshop mods for server")
    parser.add_argument('server', help="Which server to update", choices=SERVER_LIST.keys(), nargs="+")
    args = parser.parse_args()


    exitcode = main(args)

    if exitcode > 0:
        print_error("\nSomething went wrong during the setup. Make sure you run this script as administrator.")
    else:
        print("\nSetup successfully completed.")

    input("\nPress enter to exit ...")
    sys.exit(exitcode)
