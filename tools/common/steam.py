import subprocess
import time

from . import admin
from .pip_install import import_or_install
from .registry import readkey_all

psutil = import_or_install('psutil')

class SteamNotInstalled(Exception):
    pass


_steam_registry_path = r'Software\Valve\Steam'
_active_user_registry_path = r'Software\Valve\Steam\ActiveProcess\ActiveUser'
_active_process_registry_path = r'Software\Valve\Steam\ActiveProcess'


def program_running(executable_name):
    """Return if a process running on the system matches the given names."""

    for process in psutil.process_iter():
        try:
            name = process.name()
            if name.casefold() == executable_name.casefold():
                return True

        except psutil.Error:
            continue

    return False


def get_steam_exe_path():
    """Return the path to the steam executable.

    Raises SteamNotInstalled if steam is not installed."""

    try:
        return readkey_all(_steam_registry_path, 'SteamExe')  # SteamPath

    except Exception:
        raise SteamNotInstalled()


def shutdown():
    steam = get_steam_exe_path()
    print('Shutting down Steam...')
    Popen_with_optional_elevation([steam, '-shutdown'])

    while(is_steam_running()):
        print('Waiting...')
        time.sleep(1)


def login(username, password):
    steam = get_steam_exe_path()
    print('Logging into Steam as {}...'.format(username))
    Popen_with_optional_elevation([steam, '-login', username, password])


def is_steam_running():
    return readkey_all(_active_process_registry_path, 'pid') != 0 or program_running('steam.exe')


def get_active_user():
    return readkey_all(_active_process_registry_path, 'ActiveUser')


def ensure_user_logged(username, password):
    if is_steam_running():
        print('Steam is running.')
        shutdown()

    login(username, password)

    while(True):
        active_user = get_active_user()
        if active_user:
            print('Logged as userid: {}'.format(active_user))
            break

        print('Waiting...')
        time.sleep(1)

    print('Logged in!')


def Popen_with_optional_elevation(program_args, shell=False):
    """Run the given program with the given parameters.
    In case the program requires elevation, run an UAC prompt if on Windows.
    Returns a Popen or Popen compatible object.
    """

    try:
        return subprocess.Popen(program_args, shell=shell)

    except WindowsError as ex:
        if ex.winerror != 740:  # The requested operation requires elevation
            raise

        # Try running as admin
        handle = admin.run_admin(program_args[0], program_args[1:])

        if not handle:
            raise Exception('Error while running executable!')

        return handle
