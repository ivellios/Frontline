import errno
import json
import sys
import time
from collections import OrderedDict

from common import remote


###############################################################################
# Dummy stuff to allow reuse Launcher's code verbatim
###############################################################################

class DummyLogger(object):
    def error(self, text):
        print(text)
    def info(self, text):
        print(text)
    def debug(self, text):
        print(text)

Logger = DummyLogger()


class DummyMessageQueue(object):
    def progress(self, *args):
        pass
    def reject(self, *args):
        pass

from . import settings

host = settings.LAUNCHER_SERVER_HOST
username = settings.LAUNCHER_SERVER_USERNAME
password = settings.LAUNCHER_SERVER_PASSWORD
port = settings.LAUNCHER_SERVER_PORT
metadata_path = settings.LAUNCHER_SERVER_METADATA_PATH
metadata_file = settings.LAUNCHER_SERVER_METADATA_FILE
server_delay = settings.LAUNCHER_SERVER_DELAY
torrents_path = settings.LAUNCHER_SERVER_TORRENTS_PATH
private_key = settings.LAUNCHER_SSH_PRIVATE_KEY


###############################################################################


def update_metadata_json(metadata_json_orig, mods_created):
    """Modify the metadata_json given as input to use the updated mods.
    Return the modified metadata_json contents.
    """

    tree = json.loads(metadata_json_orig, object_pairs_hook=lambda x : OrderedDict(x))

    for mod, _, _, timestamp in mods_created:
        # Perform the global mods update
        for mod_leaf in tree.get('mods', []):
            if mod_leaf['foldername'] == mod.foldername:
                mod_leaf['torrent-timestamp'] = timestamp

        # Perform the launcher update
        if 'launcher' in tree:
            if tree['launcher']['foldername'] == mod.foldername:
                tree['launcher']['torrent-timestamp'] = timestamp

                # Get the new mod version
                tree['launcher']['version'] = get_new_launcher_version(mod)

        # Perform the per-server mods update
        # Note: update the hidden servers, if present
        for server in tree.get('servers', []) + tree.get('hidden_servers', []):
            for mod_leaf in server.get('mods', []):
                if mod_leaf['foldername'] == mod.foldername:
                    mod_leaf['torrent-timestamp'] = timestamp

    metadata_json_modified = json.dumps(tree, indent=4, separators=(',', ': '))
    return metadata_json_modified


def perform_update(message_queue, mods_created, callback=None):
    """Connect to the remote server, remove old, unused torrents, push newly
    created torrents and update metadata.json to use them.
    """

    Logger.info('perform_update: Starting the torrents remote update process...')
    message_queue.progress({'msg': 'Connecting to the server...'}, 1)

    with remote.RemoteConnection(host=host,
                                 username=username,
                                 password=password,
                                 port=port,
                                 private_key=private_key,
    ) as connection:

        # Fetch metadata.json
        message_queue.progress({'msg': 'Fetching metadata.json...'}, 1)

        metadata_json_path = remote.join(metadata_path, metadata_file)
        try:
            metadata_json = connection.read_file(metadata_json_path)
            if sys.version_info >= (3,):
                metadata_json = metadata_json.decode('utf-8')
        except IOError as ex:
            if ex.errno != errno.ENOENT:
                raise

            message_queue.reject({
                'msg': 'Could not fetch file from the server:\n{}'.format(metadata_json_path)})

        Logger.info('perform_update: Got metadata.json:\n{}'.format(metadata_json))

        metadata_json_updated = update_metadata_json(metadata_json, mods_created)

        # Delete old torrents
        message_queue.progress({'msg': 'Deleting old torrents...'}, 1)
        Logger.info('perform_update: Deleting old torrents...')
        for mod, local_file_path, file_name, _ in mods_created:
            for remote_file_name in connection.list_files(torrents_path):
                if not remote_file_name.endswith('.torrent'):
                    continue

                if not remote_file_name.startswith(mod.foldername):
                    continue

                if len(remote_file_name) != len(file_name):
                    continue

                # Got the file[s] to remove
                file_path = remote.join(torrents_path, remote_file_name)
                connection.remove_file(file_path)

        # Sleep custom amount of time
        if server_delay:
            message_queue.progress({'msg': 'Waiting {} seconds...'.format(server_delay)}, 1)
            Logger.info('perform_update: Sleeping {} seconds...'.format(server_delay))
            time.sleep(server_delay)

        if callback is not None:
            callback(mods_created)

        # Push new torrents
        message_queue.progress({'msg': 'Pushing new torrents...'}, 1)
        Logger.info('perform_update: Pushing new torrents...')
        for mod, local_file_path, file_name, _ in mods_created:
            remote_torrent_path = remote.join(torrents_path, file_name)
            connection.put_file(local_file_path, remote_torrent_path)

        message_queue.progress({'msg': 'Updating modified metadata.json...'}, 1)
        Logger.info('perform_update: Updated metadata.json:\n{}'.format(metadata_json_updated))

        # Push modified metadata.json
        connection.save_file(metadata_json_path, metadata_json_updated)

        message_queue.progress({'msg': 'Updating the mods is done!'}, 1)
