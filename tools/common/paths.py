import os
import platform


def get_local_user_directory(*relative):
    """Return the local user directory. Optionally append <relative> at the end.
    This means AppData\Local on windows.
    """

    local_app_data = os.environ.get('LOCALAPPDATA')  # C:\Users\user\AppData\Local
    if local_app_data:
        # TODO: Check if this is still needed for Python 3
        # local_app_data = unicode_helpers.fs_to_u(local_app_data)
        pass

    else:
        # No LOCALAPPDATA variable? Try to build it yourself based on the user home
        home = os.path.expanduser('~')

        if platform.system() == 'Linux':
            local_app_data = os.path.join(home, '.local', 'share')
        else:
            local_app_data = os.path.join(home, 'AppData', 'Local')

    directory = os.path.join(local_app_data, *relative)
    return directory


def check_path(path):
    if '"' in path or '^' in path:
        raise RuntimeError('This path looks fishy: {}'.format(path))


def convert_to_cygwin(path):
    check_path(path)
    if path[1:3] == ':\\':
        path = '/cygdrive/{}{}'.format(path[0].lower(), path[2:])

    return path.replace('\\', '/')

def convert_to_windows(path):
    check_path(path)

    # Bare drive
    if len(path) == len('/cygdrive/c') and path.startswith('/cygdrive/'):
        return '{}:\\'.format(path[10])

    # Single letter drive + real path
    if len(path) >= len('/cygdrive/c/') and path.startswith('/cygdrive/') and path[11] == '/':
        path = '{}:/{}'.format(path[10], path[12:])

    return path.replace('/', '\\')
