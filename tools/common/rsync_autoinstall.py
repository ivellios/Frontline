import os
import platform
import subprocess
import urllib.request
from io import BytesIO
from zipfile import ZipFile

from . import paths

#RSYNC_URL = 'https://itefix.net/dl/cwRsync_5.5.0_x86_Free.zip'  # This does not seem to be avalable anymore :(
RSYNC_URL = 'https://github.com/overfl0/Bulletproof-Arma-Launcher/releases/download/1.13.0/cwRsync_5.5.0_x86_Free.zip'

def fetch_rsync_installation(url):
    response = urllib.request.urlopen(url)
    code = response.getcode()
    if code != 200:
        raise RuntimeError('Receiver HTTP error code {} {} while trying to download rsync.'.format(
            code, response.reason))

    data = response.read()
    return data


def get_rsync_ssh_paths():
    def run(args):
        return subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)

    try:
        run(['rsync', '--version'])
        run(['ssh', '-V'])

        return 'rsync', 'ssh'
    except:
        # rsync or ssh not in default path, checking appdata
        if platform.system() == 'Linux':
            raise RuntimeError('Install rsync and/or ssh!')

        rsync_path = paths.get_local_user_directory('cwrsync', 'bin', 'rsync')
        ssh_path = paths.get_local_user_directory('cwrsync', 'bin', 'ssh')

        try:
            run([rsync_path, '--version'])
            run([ssh_path, '-V'])

            return rsync_path, ssh_path

        except:
            return None, None


def ensure_rsync_and_ssh_are_installed():
    if get_rsync_ssh_paths() != (None, None):
        return

    print('Downloading cwRsync from: {}'.format(RSYNC_URL))
    zip_data = fetch_rsync_installation(RSYNC_URL)
    print('Extracting cwRsync')
    extract_cwrsync(zip_data, paths.get_local_user_directory('cwrsync'))

    if get_rsync_ssh_paths() == (None, None):
        raise RuntimeError('Something went wrong while installing rsync and ssh')


def extract_cwrsync(zip_data, directory):
    os.makedirs(directory, exist_ok=True)

    zip = ZipFile(BytesIO(zip_data), 'r')
    for name in zip.namelist():
        if name.startswith('cwrsync'):
            raise RuntimeError('The file doesn\'t seem to contain cwRsync')

    for name in zip.namelist():
        _, file_path = name.split('/', 1)

        # Empty base
        if not file_path:
            continue

        # Directory entry
        if file_path.endswith('/'):
            os.makedirs(os.path.join(directory, file_path), exist_ok=True)
            continue

        # File entry
        print(file_path)
        file_data = zip.read(name)
        file_path = os.path.realpath(os.path.join(directory, file_path))

        # Check if file path does not contain ".." for example
        if not file_path.startswith(os.path.realpath(directory) + os.path.sep):
            raise RuntimeError('Suspicious zip file! Do not extract!!!')

        with open(file_path, 'wb') as f:
            f.write(file_data)

# print(get_rsync_ssh_paths())
#
# data = open('test.zip', 'rb').read()
# extract_cwrsync(data, 'badum_tsss')

# ensure_rsync_and_ssh_are_installed()
