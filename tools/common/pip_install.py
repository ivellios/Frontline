import importlib
import logging

try:
    from pip import main as pipmain
except ImportError:
    from pip._internal import main as pipmain  # pip v10+


def install(package):
    pipmain(['install', package])
    logging.shutdown()
    importlib.reload(logging)


def import_or_install(package, to_install=None):
    try:
        return importlib.import_module(package)
    except ImportError:
        print('{} not found. Trying to install from pip...'.format(package))
        if to_install:
            install(to_install)
        else:
            install(package)

        return importlib.import_module(package)
