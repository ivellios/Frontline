import os

PRIVATE_KEY = None
USERNAME = 'Administrator'
HOST = 'new.frontline-mod.com'

SETTINGS = {
    'vanilla': {
        'cygwin_path': '/cygdrive/c/Frontline/armaservers/arma 3 - vanilla',
        'service_name': 'arma - frontline_vanilla',
    },
    'dev': {
        'cygwin_path': '/cygdrive/c/Frontline/armaservers/arma 3 - dev',
        'service_name': 'arma - frontline_dev',
    },
    'rhs': {
        'cygwin_path': '/cygdrive/c/Frontline/armaservers/arma 3 - frontline_rhs',
        'service_name': 'arma - frontline_rhs',
    },
    'ifa': {
        'cygwin_path': '/cygdrive/c/Frontline/armaservers/arma 3 - ifa',
        'service_name': 'arma - frontline_ifa',
    },
    'uns': {
        'cygwin_path': '/cygdrive/c/Frontline/armaservers/arma 3 - unsung',
        'service_name': 'arma - frontline_uns',
    },
}

TORRENT_TRACKER_URLS = ['http://launcher.frontline-mod.com:6969/announce']
TORRENT_WEB_SEEDS = ['http://launcher.frontline-mod.com/mods']
LAUNCHER_SERVER_METADATA_PATH = 'metadata_auth'
LAUNCHER_SERVER_METADATA_FILE = 'metadata.json'
LAUNCHER_SERVER_TORRENTS_PATH = 'www/torrents'
LAUNCHER_SERVER_MODS_PATH = 'www/mods'
LAUNCHER_SERVER_HOST = 'launcher.frontline-mod.com'
LAUNCHER_SERVER_PORT = 22
LAUNCHER_SERVER_DELAY = 6
LAUNCHER_SSH_PRIVATE_KEY = os.path.join(os.path.expanduser('~'), '.ssh', 'id_rsa')
LAUNCHER_SERVER_USERNAME = 'frontline'
LAUNCHER_SERVER_PASSWORD = None  # Do NOT set this here! use sensible_settings.py instead!

WORKSHOP_FRONTLINE_ID = 882328821
WORKSHOP_FRONTLINE_COMPAT_IFA_ID = 902486769
WORKSHOP_FRONTLINE_COMPAT_RHS_ID = 902488043

STEAM_LOGIN = 'frontlineA3'
STEAM_PASSWORD = None

if STEAM_PASSWORD or LAUNCHER_SERVER_PASSWORD:
    raise Exception('STOP! This file is versionned in git! Don\'t store passwords here! Declare them in settings_local.py')

try:
    from .settings_local import *
except ImportError:
    pass
