import winreg


def _readkey(super_key_handle, key_path, value_name):
    flags = winreg.KEY_READ
    flags32 = flags | winreg.KEY_WOW64_32KEY
    flags64 = flags | winreg.KEY_WOW64_64KEY

    try:
        key = winreg.OpenKey(super_key_handle, key_path, 0, flags32)
    except FileNotFoundError:
        key = winreg.OpenKey(super_key_handle, key_path, 0, flags64)

    (value, valuetype) = winreg.QueryValueEx(key, value_name)
    key.Close()
    return value


def readkey_all(key_path, value_name):
    try:
        return _readkey(winreg.HKEY_LOCAL_MACHINE, key_path, value_name)
    except FileNotFoundError:
        return _readkey(winreg.HKEY_CURRENT_USER, key_path, value_name)
