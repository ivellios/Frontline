import os
import subprocess
import sys

from .bis_tools import GetBISTools
from .colorsLog import print_error, print_green, print_yellow


class BISPublisher():
    """Pushes an update to workshop"""
    def __init__(self, releasepathbase):
        bisTools = GetBISTools()
        bisTools.find_bi_tools()
        self.publisher = bisTools.publisher
        self.releasepath = releasepathbase

    def publish(self, foldername, modid):
        releasepath = os.path.join(self.releasepath, foldername)
        if not os.path.isdir(releasepath):
            print_error("Couldn't publish. Mod path not valid: {}".format(releasepath))
            return 1

        print_green("\nPublishing mod on workshop")
        changenotes = os.path.join(releasepath, "CHANGELOG.md")
        retval = subprocess.call([self.publisher, "update", "/id:{}".format(modid), "/changeNote:{}".format('Fixes everything'), "/path:{}".format(releasepath), "/nologo", "/nosummary"])
        if retval == 0:
            print_yellow("Publishing mod finished")
        else:
            print_error("Publishing mod failed. Error code: {}".format(retval))
            sys.exit(1)
        return retval
