from __future__ import unicode_literals

# TODO: Implement this for linux as well
import pywintypes
import sys
import win32con
import win32event
import win32process

from win32com.shell import shellcon
from win32com.shell.shell import ShellExecuteEx


class AdminTask(object):
    def __init__(self, process):
        """Constructor. Pass the process handle as parameter."""

        self.process_handle = process['hProcess']
        self.returncode = None

    def wait(self):
        """Wait for the process to terminate.
        Return the process exit code.
        """

        return self.poll(timeout=win32event.INFINITE)

    def poll(self, timeout=0):
        """Check if the process has terminated.
        Return the process exit code if it has terminated or None otherwise.
        """

        obj = win32event.WaitForSingleObject(self.process_handle, timeout)
        if obj == win32event.WAIT_TIMEOUT:
            return None

        self.returncode = win32process.GetExitCodeProcess(self.process_handle)

        return self.returncode


def run_admin(executable, args):
    """Run executable as an administrator.
    Return an object that simulates a subset of subprocess.Popen class or None
    if the user chose to cancel elevating the executable rights.
    """

    params = ' '.join('"{}"'.format(arg) for arg in args)

    try:
        process = ShellExecuteEx(nShow=win32con.SW_SHOWNORMAL,
                                 fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                                 lpVerb='runas',
                                 lpFile=executable,
                                 lpParameters=params)

    except pywintypes.error as ex:
        if ex.winerror == 1223:
            return None  # The operation was canceled by the user.
        raise

    return AdminTask(process)
