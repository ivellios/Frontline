import os
import sys

def pywin32_workaround():
    python_path = os.path.dirname(sys.executable)
    pywin32_path = os.path.join(python_path, 'Lib', 'site-packages', 'pywin32_system32')
    print(pywin32_path)
    os.environ['PATH'] += ';{}'.format(pywin32_path)

pywin32_workaround()
from common.pip_install import import_or_install
pywin32 = import_or_install('win32', to_install='pywin32')

try:
    import win32security  # Needs pywin32_workaround() to be called first!!!
except ImportError:
    print('\nImporting win32security failed.')
    print('This may happen the first time after installing pywin32.')
    print('Just re-run this script again.')

    sys.exit(1)


def set_file_ssh_permissions(filename):
    """
    By 'Antoine Martin <antoine@openance.com> under the MIT license
    """
    import win32api
    import win32security
    import ntsecuritycon as con

    WinBuiltinUsersSid = 27  # not exported by win32security. according to WELL_KNOWN_SID_TYPE enumeration from http://msdn.microsoft.com/en-us/library/windows/desktop/aa379650%28v=vs.85%29.aspx
    users = win32security.CreateWellKnownSid(WinBuiltinUsersSid)
    WinBuiltinAdministratorsSid = 26  # not exported by win32security. according to WELL_KNOWN_SID_TYPE enumeration from http://msdn.microsoft.com/en-us/library/windows/desktop/aa379650%28v=vs.85%29.aspx
    admins = win32security.CreateWellKnownSid(WinBuiltinAdministratorsSid)
    user, domain, type = win32security.LookupAccountName("", win32api.GetUserName())

    sd = win32security.GetFileSecurity(filename, win32security.DACL_SECURITY_INFORMATION)

    dacl = win32security.ACL()
    dacl.AddAccessAllowedAce(win32security.ACL_REVISION, con.FILE_ALL_ACCESS, user)
    #dacl.AddAccessAllowedAce(win32security.ACL_REVISION, 0, users)
    #dacl.AddAccessAllowedAce(win32security.ACL_REVISION, 0, admins)
    sd.SetSecurityDescriptorDacl(1, dacl, 0)
    win32security.SetFileSecurity(filename, win32security.DACL_SECURITY_INFORMATION, sd)
