import os
import stat
import time

from common.remote import RemoteConnection
from .colorsLog import print_blue
from .pip_install import import_or_install
from .rsync import rsync

SERVER_STOP_TIMEOUT = 30  # How much time the script will wait for the server to stop before erroring
SERVICE_PREFIX_NAME = 'DaemonMaster_'  # The service can be named 'arma - frontline_rhs' for example
paramiko = import_or_install('paramiko')

class Deployment(object):
    def __init__(self, host, username, private_key):
        self.host = host
        self.username = username
        self.private_key = private_key
        self.connection = None

    def rsync(self, source, destination):
        return rsync(source, destination, key_path=self.private_key)

    def exec_command(self, cmd, valid_errcodes=None, verbose=True):
        if valid_errcodes is None:
            valid_errcodes = []

        if verbose:
            print('Executing {}'.format(cmd))

        stdin, stdout, stderr = self.connection.client.exec_command(cmd)
        return_code = stdout.channel.recv_exit_status()

        if return_code and return_code not in valid_errcodes:
            print('Return code:', return_code)
            print(stdout.read())
            print(stderr.read())
            raise RuntimeError(cmd)

        return return_code, stdout, stderr

    def stop_server(self, name, timeout=SERVER_STOP_TIMEOUT):
        self.exec_command('net stop "{}{}"'.format(SERVICE_PREFIX_NAME, name), valid_errcodes=[2])
        time_start = time.time()

        while (time.time() <= time_start + timeout):
            if not self.is_server_running(name):
                return

            time.sleep(1)

        # Timed out
        raise RuntimeError('Could not shut down the server after {} seconds'.format(timeout))

    def start_server(self, name):
        self.exec_command('net start "{}{}"'.format(SERVICE_PREFIX_NAME, name))

    def is_server_running(self, name):
        return_code, stdout, stderr = self.exec_command('net start', verbose=False)

        for line in stdout:
            if line.strip().startswith('{}{}'.format(SERVICE_PREFIX_NAME, name)):
                return True
        return False

    def before_deploy(self):
        """Override this method in your subclass."""
        pass

    def deploy(self, servers):
        self.before_deploy()

        print('Connecting...')
        with RemoteConnection(host=self.host, username=self.username, private_key=self.private_key) as connection:
            self.connection = connection
            # getpass.getpass('Password for {}:')

            for server in servers:
                print_blue('====================================================================')
                print_blue('Deploying to: {}'.format(server['service_name']))
                print_blue('====================================================================')
                print()

                self.stop_server(server['service_name'])
                self.deploy_to_server(server)
                self.start_server(server['service_name'])

    def restart(self, servers):
        with RemoteConnection(host=self.host, username=self.username, private_key=self.private_key) as connection:
            self.connection = connection

            for server in servers:
                self.stop_server(server['service_name'])
                self.start_server(server['service_name'])
