import subprocess

from .paths import check_path, convert_to_cygwin
from . import rsync_autoinstall

rsync_autoinstall.ensure_rsync_and_ssh_are_installed()
rsync_path, ssh_path = rsync_autoinstall.get_rsync_ssh_paths()

check_path(rsync_path)
check_path(ssh_path)


def rsync(source, destination, key_path=None, permissions=None):
    chmod = '--chmod={}'.format(permissions if permissions else 'ug=rwX')
    # '--chmod=ugo=rwX'
    args = [rsync_path, '-avt', chmod, '--progress', '--inplace', '--delete']
    if key_path:
        args.extend(['-e', '"{}" -i {} -o "StrictHostKeyChecking no"'.format(ssh_path, convert_to_cygwin(key_path))])

    args.extend([convert_to_cygwin(source), destination])
    print('Calling rsync {} {}'.format(source, destination))
    print(args)
    subprocess.check_call(args)
