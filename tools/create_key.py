import os
import subprocess

from common import acl_tools
from common import rsync_autoinstall
from common.colorsLog import print_error, print_green, print_magenta, print_blue, print_yellow, print_grey, print_red


def recreate_file(filename):
    """
    Stupid worklaround for files created by ssh-keygen.
    For some reason they are not accepted by ssh when used with rsync.
    You have to delete the file and create it manually from python for it to have the right permissions.
    """
    with open(filename, 'rb') as f:
        private_key = f.read()

    os.unlink(filename)

    with open(filename, 'wb') as f:
        f.write(private_key)

    os.chmod(filename, 0o700)

# ssh-keygen -t rsa -b 4096 -f id_rsa -P ''

rsync_autoinstall.ensure_rsync_and_ssh_are_installed()
rsync_path, ssh_path = rsync_autoinstall.get_rsync_ssh_paths()

ssh_keygen = os.path.join(os.path.dirname(ssh_path), 'ssh-keygen')

os.makedirs(os.path.join(os.path.expanduser('~'), '.ssh'), mode=0o700, exist_ok=True)
private_file = os.path.join(os.path.expanduser('~'), '.ssh', 'id_rsa')
subprocess.check_call([ssh_keygen, '-t', 'rsa', '-b', '4096', '-f', private_file, '-P', ''])
recreate_file(private_file)
acl_tools.set_file_ssh_permissions(private_file)

print('\n\n')
print('#' * 80)
print('The keys have been created.')
print_red('Your private key: {} - KEEP IT SECURE!!! (used by deploy.py)'.format(private_file))
print('Your public key: {}.pub - add it to .ssh/authorized_keys on the server.'.format(private_file))
print('#' * 80)

print_yellow('\n\nYour public key contents:')
print_yellow('-' * 80)
print(open(private_file + '.pub', 'r').read(), end='')
print_yellow('-' * 80)
