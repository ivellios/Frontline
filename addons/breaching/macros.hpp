#define MODULE Breaching

// -- Global defines -- //
#include "..\main\macros_local.hpp"

#define __MAGTYPE "FRL_BreachingCharge_Wpn"
#define __AMMOTYPE "FRL_BreachingCharge_Ammo"

#define __MAGTYPEEXPL "FRL_ExplosiveCharge_Wpn"
#define __AMMOTYPEEXPL "FRL_ExplosiveCharge_Ammo"
