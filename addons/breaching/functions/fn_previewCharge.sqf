#include "macros.hpp"
/*
 *	File: fn_previewFO.sqf
 *	Author: Adanteh
 *	Handles the previewing of construction / FO placement
 *
 *	Example:
 *	[player] call FUNC(previewFO);
 */

// -- Having sights up automatically raises weapon again, so if someone has his sights up, make sure we put those down first -- //
if (!weaponLowered CLib_Player) then {
	if (local CLib_Player) then {
		if (cameraView == "GUNNER") then { CLib_Player switchCamera "INTERNAL" };
	};
	CLib_Player action ["WeaponOnBack", CLib_Player];
};

private _objPosition = CLib_Player modelToWorldVisual [0, 3, 0];
private _ammo = switch (GVAR(explosiveType)) do {
	case __MAGTYPEEXPL: {__AMMOTYPEEXPL};
	case __MAGTYPE: {__AMMOTYPE};
	default {__AMMOTYPEEXPL};
};
private _previewObject = _ammo createVehicleLocal [0, 0, 0];

GVAR(previewObject) = _previewObject;
GVAR(placementMode) = true;
GVAR(lastVector) = [0.561786,0.827281,-0.00162336];

[{
	(_this select 0) params ["_previewObject"];

	if !(GVAR(placementMode)) exitWith {
		[_this select 1] call MFUNC(removePerFrameHandler);
	};

	private _eyePos = (eyePos CLib_Player);
	private _cameraPosClose = (positionCameraToWorld [0,0,0.2]);
	private _cameraPosFar = (positionCameraToWorld [0,0,1.8]);
	_ins = lineIntersectsSurfaces [
		AGLtoASL (_cameraPosClose),
		AGLtoASL (_cameraPosFar),
		_previewObject,
		CLib_Player,
		true,
		1,
		"GEOM",
		"VIEW"
	];

	_previewObject setDir (getDirVisual CLib_Player);

	private _validPlacement = true;

	if ({!(_x select 2 isKindOf "Man")} count _ins == 0) exitWith {
		GVAR(validPlacement) = false;
		GVAR(validPlacementReason) = "Need to attach to something";
		_previewObject setPosATL (ASLtoATL (AGLToASL (positionCameraToWorld [0,0,0.8])));
		_previewObject setVectorUp GVAR(lastVector);
	};

	_objPosition = ASLToATL (_ins select 0 select 0);
	if (_objPosition select 2 < 0) then { _objPosition set [2, 0]; };
	_previewObject setPosATL _objPosition;
	_previewObject setVectorUp (_ins select 0 select 1);
	GVAR(lastVector) = (_ins select 0 select 1);

	// hintSilent str((_ins select 0 select 1));

	GVAR(attachedObject) = _ins select 0 select 2;
	GVAR(attachedExtra) = _ins select 0 select 3;
	GVAR(validPlacement) = _validPlacement;
}, 0, [_previewObject]] call MFUNC(addPerFramehandler);
