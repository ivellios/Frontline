/*
    Function:       FRL_Breaching_fnc_satchelDetonated
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

// ["Land_Trench_01_forest_F","Wall_F","Wall","NonStrategic","Building","Static","All"]

params ["_charge"];

private _pos = getposATL _charge;
private _nearbyObjects = _pos nearObjects ["Static",5];
_nearbyObjects append (_pos nearObjects ["StaticWeapon",5]);

private _roots = [];
private _fxPos = [];

private _destroy = {
    _fxPos append [getposATL _this];
    _this setdamage 1;
    deleteVehicle _this;
};


{

    private _object = _x;
    private _attached = attachedObjects _object;
    private _attachedTo = attachedTo _object;

    if (count _attached > 0) then {
        _attached = _attached - [_charge];
        {
            deleteVehicle _x;
        } foreach _attached;
        _object call _destroy;

    } else {

        if !(isNull _attachedTo) then {
            if !( _attachedTo in _roots || {_attachedTo isKindOf "Static"} ) then {
                _roots append [_attachedTo];
            };
        } else {
            // -- todo make a bit of a check, dont wanna simply delete the zargabad mosque here
            private _bb = boundingBoxReal _object;
            _bb params ["_p1", "_p2"];
            private _maxHeight = abs ((_p2 select 2) - (_p1 select 2));
            if (_maxHeight < 2 || {_object iskindof "Strategic"} ) then {
                _object call _destroy;
            };
        };

    };

} foreach _nearbyObjects;

{

    private _object = _x;
    private _attached = attachedObjects _object;

    _fxPos append [getposATL _object];
    {
        deleteVehicle _x;
    } foreach _attached;
    _object call _destroy

} foreach _roots;

/* TODO: make serverside */
{
    private _fx1 = "#particlesource" createVehicleLocal _x;
    _fx1 setParticleClass "DirtBulletImpact5_120mm";
    private _fx2 = "#particlesource" createVehicleLocal _x;
    _fx2 setParticleClass "DirtBulletImpact1_120mm";

    [{
        {deleteVehicle _x} foreach _this;
    },0.5,[_fx1,_fx2]] call CFUNC(wait);
} foreach _fxPos;
