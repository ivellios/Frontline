/*
    Function:       FRL_Breaching_fnc_postInit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

if (hasInterface) then {


// in vs count vs findif
    ["<t color='#FFAA00'>Place Breaching Charge</t>", CLib_Player, 0, {
    	[QGVAR(canPlaceCharge), {
            ( (items CLib_Player findIf { _x ==  __MAGTYPE }) >= 0 )
    	}, [], 3] call CFUNC(cachedCall);
    }, {
        ["preview", __MAGTYPE] call FUNC(placeCharge);
    }, ["showWindow", false]] call CFUNC(addAction);

    ["<t color='#FFAA00'>Place Explosive Charge</t>", CLib_Player, 0, {
    	[QGVAR(canPlaceExpl), {
    		( (items CLib_Player findIf { _x ==  __MAGTYPEEXPL }) >= 0 )
    	}, [], 3] call CFUNC(cachedCall);
    }, {
        ["preview", __MAGTYPEEXPL] call FUNC(placeCharge);
    }, ["showWindow", false]] call CFUNC(addAction);


    ["<t color='#FF0000'>Set Off Charges</t>", CLib_Player, 0, {
    	((count GVAR(detonators)) > 0)
    }, {
    	["use"] call FUNC(detonator);
    }, ["showWindow", false]] call CFUNC(addAction);

};
