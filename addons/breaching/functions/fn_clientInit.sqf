/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *
 *	Example:
 *	[] call FUNC(clientInit);
 */

#include "macros.hpp"

GVAR(placingExplosive) = false;
GVAR(detonators) = [];
GVAR(detonatorIndex) = 0;
GVAR(fuseTime) = 15;

// -- Remove all the old explosives after you've respawned -- //
["Killed", {
	{
		_x params ["_index", "_charge"];
		deleteVehicle _x;
		nil;
	} count GVAR(detonators);
	GVAR(detonators) = [];
}] call CFUNC(addEventHandler);

["UnconsciousnessChanged", {
	private _incapped = (_this select 0);
	if (_incapped && GVAR(placingExplosive)) then {
		["cancel"] call FUNC(placeCharge);
	};
}] call CFUNC(addEventHandler);
