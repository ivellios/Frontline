/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Loads parameters and applies server weather settings globally
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call EFUNC(main,serverInit);
 */

#include "macros.hpp"

params ["_mode", ["_args", []]];

switch (toLower _mode) do {
	case "add": {
		_args params ["_charge", ["_attached", objNull], ["_delete", false]];
		GVAR(detonators) pushBack [GVAR(detonatorIndex), _charge, _attached, _delete];
		GVAR(detonatorIndex) = GVAR(detonatorIndex) + 1;
	};

	case "use": {
		{
			_x params ["_index", "_charge", ["_attached", objNull], ["_delete", false]];
			if !(isNull _charge) then {
				_charge call FUNC(satchelDetonated);
				_charge setDamage 1;
				if !(isNull _attached) then {
					_attached setDamage 1;
					if (_delete) then {
						deleteVehicle _attached;
					};
				};
			};
			nil;
		} count GVAR(detonators);
		GVAR(detonators) = [];
	};

	case "delete": {
		_args params ["_index"];
		private _leftover = [];
		{
			if ((_x select 0) != _index) then {
				_leftover pushBack _x;
			};
		} count GVAR(detonators);
		GVAR(detonators) = _leftover;
	};
};
