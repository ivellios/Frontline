/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Loads parameters and applies server weather settings globally
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call EFUNC(main,serverInit);
 */

#define __INCLUDE_DIK
#include "macros.hpp"

params ["_mode", "_args"];

switch (toLower _mode) do {
	case "preview": {
		_args params ["_explosiveType"];
		GVAR(placingExplosive) = true;
		GVAR(validPlacement) = false;
		GVAR(validPlacementReason) = "";
		GVAR(previewObject) = objNull;
		GVAR(attachedObject) = objNull;
		GVAR(explosiveType) = _explosiveType;

		switch (GVAR(explosiveType)) do {
			case __MAGTYPE: {
				[QMVAR(showControlsHUD), [
					["To place breaching charge with remote detonator", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { ["place",-1] call FUNC(placeCharge) }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
					["To place breaching charge with " + str GVAR(fuseTime) + " fuse", [TEXTURE(Ctrl.paa),"\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { ["place"] call FUNC(placeCharge) }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
					["To cancel", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\rmb_ca.paa"], { ["cancel"] call FUNC(placeCharge) }, true, [1]]
				]] call CFUNC(localEvent);
			};
			case __MAGTYPEEXPL: {
				[QMVAR(showControlsHUD), [
					["To place explosive charge with remote detonator", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { ["place",-1] call FUNC(placeCharge) }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
					["To place explosive charge with " + str GVAR(fuseTime) + " fuse", [TEXTURE(Ctrl.paa),"\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { ["place"] call FUNC(placeCharge) }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
					//["To change the fusetime", [TEXTURE(Mouse_roll.paa)], {}, true, [2], { true }],
					["To cancel", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\rmb_ca.paa"], { ["cancel"] call FUNC(placeCharge) }, true, [1]]
				]] call CFUNC(localEvent);
			};
			default {
				[QMVAR(showControlsHUD), [
					["To place the charge with remote detonator", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { ["place",-1] call FUNC(placeCharge) }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
					["To cancel", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\rmb_ca.paa"], { ["cancel"] call FUNC(placeCharge) }, true, [1]]
				]] call CFUNC(localEvent);
			};
		};



		// -- Having sights up automatically raises weapon again, so if someone has his sights up, make sure we put those down first -- //
		[true] call MFUNC(disableMouse);
		call FUNC(previewCharge);
	};

	case "place": {
		if !(GVAR(validPlacement)) exitWith { };

		private _removeHUD = true;

	    GVAR(placementMode) = false;
	    private _dir = getDirVisual GVAR(previewObject);
	    private _pos = getPosATLVisual GVAR(previewObject);
	    private _vec = vectorUpVisual GVAR(previewObject);
	    private _type = typeOf GVAR(previewObject);
	    deleteVehicle GVAR(previewObject);

	    _charge = createVehicle [_type, _pos, [], 0, "CAN_COLLIDE"];
	    _charge setDir _dir;
	    _charge setPosATL _pos;

	    CLib_Player playActionNow "PutDown";
	    private _args = [_charge];
		switch (GVAR(explosiveType)) do {
			case __MAGTYPE: {
				if (typeOf GVAR(attachedObject) == "") then {
				  _args pushBack GVAR(attachedObject);
				} else {
					if (GVAR(attachedObject) isKindOf "Wall") then {
						_args pushBack GVAR(attachedObject);
					} else {
						// -- not sexy probably refractoring along the way
						if (GVAR(attachedObject) isKindOf "Land_fort_rampart") exitWith {
							_args append [GVAR(attachedObject), true];
						};
						if (GVAR(attachedObject) isKindOf "HBarrier_base_F") then {
							if (GVAR(attachedObject) isKindOf "Land_HBarrierTower_F") exitWith { };
							if (GVAR(attachedObject) isKindOf "Land_HBarrierWall_corner_F") exitWith { };
							if (GVAR(attachedObject) isKindOf "Land_HBarrierWall_corridor_F") exitWith { };
							if (GVAR(attachedObject) isKindOf "Land_HBarrierWall4_F") exitWith { };
							if (GVAR(attachedObject) isKindOf "Land_HBarrierWall6_F") exitWith { };
							_args append [GVAR(attachedObject), true];
						};
					};
				};
			};
			default {
				_args pushBack GVAR(attachedObject);
				if (typeOf GVAR(attachedObject) != "") then {
					_charge attachto [GVAR(attachedObject), GVAR(attachedObject) worldToModel _pos];
				};
			};
		};

		_charge setVectorUp _vec;
		if ([DIK_LCONTROL] call MFUNC(keyPressed) && GVAR(explosiveType) in [__MAGTYPE, __MAGTYPEEXPL]) then {
			["showNotification", ["Set to explode in " + str GVAR(fuseTime) + "!", "nope"]] call CFUNC(localEvent);
			[{
				_this call FUNC(satchelDetonated);
				_this setdamage 1;
			}, GVAR(fuseTime), _charge] call CFUNC(wait);
		} else {
			["add", _args] call FUNC(detonator);
		};

	    CLib_Player removeItem GVAR(explosiveType);

	    if (_removeHUD) then {
	    	["cancel"] call FUNC(placeCharge);
	    };
	};

	case "cancel": {
		if !(isNull GVAR(previewObject)) then {
			deleteVehicle GVAR(previewObject);
			GVAR(previewObject) = objNull;
		};

		GVAR(placementMode) = false;
		[QMVAR(hideControlsHUD)] call CFUNC(localEvent);
		["forceWalk", QGVAR(forceWalk), false] call CFUNC(setStatusEffect);
		[false] call MFUNC(disableMouse);
		GVAR(placingExplosive) = false;
	};
};
