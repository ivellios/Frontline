## Contents

Includes 2:1 ratio screenshots of the map (Taken from editor) so you have some sort of clue which map to choose as an admin.
This is only required because mission screenshots need to be packed in a mod if you want to see them on lobby.
Pictures inside missions don't get loaded in
