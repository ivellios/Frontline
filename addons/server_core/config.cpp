#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class CfgFunctions {
	class ADDON {
	    tag = QUOTE(ADDON);
	    class MODULE {
	        file = "\pr\frl\addons\server_core";
	        class replaceFunctions {
	            preStart = 1;
	            preInit = 1;
	        };
	    };
	};
};
