#include "macros.hpp"
/*
 *	File: fn_replaceFunctions.sqf
 *	Author: Adanteh
 *	This drops functions into parsingNamespace as early as possible, before AAW does anything. This is needed to make sure compileFinal doesn't block our replacement functions
 */

#define SCRIPTHEADER "\
private _fnc_scriptNameParent = if (isNil '_fnc_scriptName') then {\
    '%1'\
} else {\
    _fnc_scriptName\
};\
private _fnc_scriptName = '%1';\
scriptName _fnc_scriptName;\
scopeName (_fnc_scriptName + '_Main');\
%2\
\
"

private _debug = "";

{
	_x params ["_functionVarName", "_functionPath"];
	diag_log format ["[{WARNING} FRL ReplaceFunction] Replacement function used '%1'", _functionVarName];

    private _fncCode = if (isNil "_cachedFunction") then {
        private _header = format [SCRIPTHEADER, _functionVarName, _debug];
        private _funcString = _header + preprocessFileLineNumbers _functionPath;

        compileFinal _funcString;
    } else { _cachedFunction };

	{
	    _x setVariable [_functionVarName, _fncCode];
	    nil
	} count [missionNamespace, uiNamespace, parsingNamespace];

} forEach [

];
