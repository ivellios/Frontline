// -- Module defines -- //
#define MODULE Compat

// -- Global defines -- //
#include "..\main\macros_local.hpp"

#define COMPATCONFIG(var1) TRIPLE(PREFIX,MODULE,var1)
