/*
    Function:       FRL_Compat_fnc_noExplEHCode
    Author:         N3croo
    Description:    prevent silly vehicle explosion when crashing or taking .50cal fire
*/
#include "macros.hpp"

_this params ["_vehicle", "", "_damage", "", "_ammo", "", "", "_hitpoint"];

if (!local _vehicle || damage _vehicle >= 1) exitWith {};

_hitpoint = toLower _hitpoint;

if (_hitpoint in ["hithull", "hitfuel", "hitbody" ,""] ) then {

    private _isExplosiveAmmo = false;
    if (_ammo != "") then {
        _isExplosiveAmmo = GVAR(namespace) getVariable [_ammo, -1];
        if (_isExplosiveAmmo isEqualTo -1) then {
            _isExplosiveAmmo = ((getNumber (configFile >> "CfgAmmo" >> _ammo >> "explosive")) >= 0.6);
            GVAR(namespace) setVariable [_ammo, _isExplosiveAmmo];
        };
    };
    if !(_isExplosiveAmmo) then {
        _damage min 0.89
    };
};
