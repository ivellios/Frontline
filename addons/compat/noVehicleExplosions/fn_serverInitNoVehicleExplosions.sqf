/*
    Function:       FRL_Compat_fnc_serverInitnoVehicleExplosions
    Author:         N3croo
    Description:    adds an eventhandler for clients that attaches the script to check for damage on their vehicles
*/
#include "macros.hpp"

["vehicleInit", {

    (_this select 0) params ["_vehicle"];

    if (_vehicle isKindOf "LandVehicle") then {
        private _EH = _vehicle addEventHandler ["HandleDamage",{_this call FUNC(noExplEHCode)}];
        _vehicle setVariable ["NoExplEH",_EH];

        ["Local",{
            (_this select 0) params ["_vehicle","_local"];
            if (_local) then {
                private _EH = _vehicle addEventHandler ["HandleDamage",{_this call FUNC(noExplEHCode)}];
                _vehicle setVariable ["NoExplEH",_EH];
            } else {
                private _EH = _vehicle getVariable ["NoExplEH",-1];
                _vehicle removeeventhandler ["HandleDamage",_EH];
            };
        },[_vehicle]] call CFUNC(addEventHandler);
    };

}] call CFUNC(addEventHandler);
