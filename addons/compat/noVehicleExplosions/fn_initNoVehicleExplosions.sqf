/*
    Function:       FRL_Compat_fnc_initnoVehicleExplosions
    Author:         N3croo
    Description:    creates a namespace to store the isexplosive ammo in
*/
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
