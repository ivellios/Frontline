/*
    Function:       FRL_Compat_fnc_clientInitnoVehicleExplosions
    Author:         N3croo
    Description:    adds an eventhandler for clients that attaches the script to check for damage on their vehicles
*/
#include "macros.hpp"

GVAR(lastVehicle) = ObjNull;

["vehicleChanged", {
	(_this select 0) params ["_vehicle"];

    //removing to prevent double or leftover assigment
	private _lastVeh = GVAR(lastVehicle);
    if !(isNull _lastVeh) then {
        _lastVeh removeEventhandler  ["Local", _lastVeh getVariable ["NoExplLocalEH", -1] ];
        _lastVeh removeEventhandler  ["HandleDamage", _lastVeh getVariable ["NoExplEH", -1] ];
		_lastVeh setVariable ["NoExplEH",nil];
		_lastVeh setVariable ["NoExplLocalEH",nil];
        GVAR(lastVehicle) = ObjNull;
    };

	if (!(_vehicle isKindOf "CaManBase") && _vehicle isKindOf "LandVehicle") then {
		GVAR(lastVehicle) = _vehicle;
		if (local _vehicle) then {
        	private _EH = _vehicle addEventHandler ["HandleDamage",{_this call FUNC(noExplEHCode)}];
			_vehicle setVariable ["NoExplEH",_EH];
        };
		private _localEH = _vehicle addEventHandler ["Local",{
			_this params ["_vehicle","_local"];
			if (_local) then {
				private _EH = _vehicle addEventHandler ["HandleDamage",{_this call FUNC(noExplEHCode)}];
				_vehicle setVariable ["NoExplEH",_EH];
			};
		}];
		_vehicle setVariable ["NoExplLocalEH",_EH];
	};

}] call CFUNC(addEventHandler);
