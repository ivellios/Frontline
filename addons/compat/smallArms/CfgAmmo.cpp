#include "..\macros.hpp"

class CfgAmmo{
    class Default;

    /*
    class ShellCore: Default{};
    class ShellBase: ShellCore{};
    */

    class BulletCore: Default{};
    class BulletBase: BulletCore{};

    class B_762x51_Ball: BulletBase {
        hit = 16;
    };
    class B_762x54_Ball: B_762x51_Ball {};
};
