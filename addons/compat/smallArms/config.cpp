#include "..\macros.hpp"

#include "CfgAmmo.cpp"

class CfgPatches {
    class COMPATCONFIG(SmallArms) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};
