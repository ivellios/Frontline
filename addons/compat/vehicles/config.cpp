#include "..\macros.hpp"

#define soldierStructuralArmor 0.5

class CfgPatches {
    class COMPATCONFIG(vehicles) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class CfgVehicles {
    class All;
    class AllVehicles : All {};
    class Land : AllVehicles {};
    class LandVehicle : Land {

    };
    class StaticWeapon : LandVehicle {
        simulation = "tank";
    };

    class Car: LandVehicle {};
    class Car_F: Car {};
    class Wheeled_APC_F: Car_F {
        class AnimationSources;
    };

    class Tank: LandVehicle {};
    class Tank_F: Tank {
        class AnimationSources;
    };

    // APC tracked
    class APC_Tracked_01_base_F: Tank_F {
        class AnimationSources: AnimationSources {};
    };
    class APC_Tracked_03_base_F: Tank_F {
        class AnimationSources: AnimationSources {};
    };
    class APC_Tracked_02_base_F: Tank_F {
        class AnimationSources: AnimationSources {};
    };

    // APC wheeled
    class APC_Wheeled_01_base_F: Wheeled_APC_F {
        armor = 480;
        class AnimationSources: AnimationSources {};
    };
    class APC_Wheeled_02_base_F: Wheeled_APC_F {
        // https://spicerparts.com/calculators/horsepower-torque-calculator
        // peakTorque = 2037.5; how the fuck does a 600hp engine make over 2k NM of trq
        //  580 horsepower and 1,960 Nm of torque.
        brakeDistance = 8;
        enginePower = 400;
        engineMOI = 1.5;
        peakTorque = 1350;  // 400hp engine in the real vehicle
        switchtime = 0.15;
        terrainCoef = 2.25; // 1.5
        //thrustDelay = 0.75;
        torqueCurve[] = {{0,0},{"(500/2300)",0.5},{"(1200/2300)",1},{"(1500/2300)",1},{"(1800/2300)",0.8},{"(2000/2300)",0.66},{"(2300/2300)",0.5},{"(4700/2300)","(0/2260)"}};
        turnCoef = 2.5; // 3
        class AnimationSources: AnimationSources {};
    };
    class APC_Wheeled_02_base_v2_F: APC_Wheeled_02_base_F {
        armor = 400;        // used to be 270 and explode anytime sth looked that way
        class AnimationSources: AnimationSources {};
    };
    class APC_Wheeled_03_base_F: Wheeled_APC_F {
        class AnimationSources: AnimationSources {};
    };

    // NATO
    class B_APC_Wheeled_01_base_F: APC_Wheeled_01_base_F {
        class AnimationSources: AnimationSources {
            class showCamonetCannon;
            class showCamonetTurret;
            class showCamonetHull;
            class showSLATTurret;
            class showSLATHull;
        };
    };
    class B_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_base_F {
        class AnimationSources: AnimationSources {
            class showCamonetCannon;
            class showCamonetTurret;
            class showCamonetHull;
            class showSLATTurret;
            class showSLATHull;
        };
    };
    class B_APC_Wheeled_01_base_FRL: B_APC_Wheeled_01_cannon_F {
        displayName = "AMV-7 Marshall (slat armor)";
        class AnimationSources: AnimationSources {
            class showCamonetCannon: showCamonetCannon {
                initPhase = 1;
            };
            class showCamonetTurret: showCamonetTurret {
                initPhase = 1;
            };
            class showCamonetHull: showCamonetHull {
                initPhase = 1;
            };
            class showSLATTurret: showSLATTurret {
                initPhase = 1;
            };
            class showSLATHull: showSLATHull {
                initPhase = 1;
            };
        };
    };

    class B_APC_Tracked_01_base_F: APC_Tracked_01_base_F {
        class AnimationSources: AnimationSources {};
    };
    class B_APC_Tracked_01_CRV_F: B_APC_Tracked_01_base_F {
        class AnimationSources: AnimationSources {
            class showCamonetHull;
            class showWheels;
            class showAmmobox;
        };
    };

    class B_APC_Tracked_01_CRV_FRL: B_APC_Tracked_01_CRV_F {
        displayName = "CRV-6e Bobcat (camonet)";
        class AnimationSources: AnimationSources {
            class showCamonetHull: showCamonetHull {
                initPhase = 1;
            };
            class showWheels: showWheels{
                initPhase = 1;
            };
            class showAmmobox: showAmmobox {
                initPhase = 1;
            };
        };
    };

    class B_APC_Tracked_01_rcws_F: B_APC_Tracked_01_base_F {
        class AnimationSources: AnimationSources {
            class showCamonetHull {};
        };
    };
    class B_APC_Tracked_01_rcws_FRL: B_APC_Tracked_01_rcws_F {
        displayName = "IFV-6c Panther (camonet)";
        class AnimationSources: AnimationSources {
            class showCamonetHull: showCamonetHull {
                initPhase = 1;
            };
        };
    };
    class MBT_01_base_F: Tank_F {
        animationList[] = {"showCamonetCannon",1,"showCamonetPlates1",1,"showCamonetPlates2",1,"showCamonetTurret",1,"showCamonetHull",1};
        class AnimationSources : AnimationSources {
            class showCamonetCannon {
                initPhase = 1;
            };
            class showCamonetHull {
                initPhase = 1;
            };
            class showCamonetTurret {
                initPhase = 1;
            };
            class showCamonetPlates1 {
                initPhase = 1;
            };
            class showCamonetPlates2 {
                initPhase = 1;
            };
        };
    };
    class B_MBT_01_base_F: MBT_01_base_F {
        class AnimationSources : AnimationSources {};
    };
    class B_MBT_01_cannon_F: B_MBT_01_base_F {
        class AnimationSources : AnimationSources {};
    };
    // okay bis why are none of the animations inherited
    class B_MBT_01_TUSK_F: B_MBT_01_cannon_F {
        animationList[] = {"showCamonetCannon",1,"showCamonetPlates1",1,"showCamonetPlates2",1,"showCamonetTurret",1,"showCamonetHull",1};
        class AnimationSources : AnimationSources {
            class showCamonetCannon {
                initPhase = 1;
            };
            class showCamonetHull {
                initPhase = 1;
            };
            class showCamonetTurret {
                initPhase = 1;
            };
            class showCamonetPlates1 {
                initPhase = 1;
            };
            class showCamonetPlates2 {
                initPhase = 1;
            };
        };
    };

    // GREF
    class I_APC_tracked_03_base_F: APC_Tracked_03_base_F {
        class AnimationSources: AnimationSources {};
    };
    class I_APC_tracked_03_cannon_F: I_APC_tracked_03_base_F {
        class AnimationSources: AnimationSources {
            class showCamonetTurret;
            class showCamonetHull;
            class showSLATTurret;
            class showSLATHull;
        };
    };
    class I_APC_tracked_03_cannon_FRL: I_APC_tracked_03_cannon_F {
        displayName = "FV-720 Mora (slat armor)";
        class AnimationSources: AnimationSources {
            class showCamonetTurret: showCamonetTurret {
                initPhase = 1;
            };
            class showCamonetHull: showCamonetHull {
                // initPhase = 1;
            };
            class showSLATTurret: showSLATTurret {
                initPhase = 1;
            };
            class showSLATHull: showSLATHull {
                initPhase = 1;
            };
        };
    };

    class I_APC_Wheeled_03_base_F: APC_Wheeled_03_base_F {
        class AnimationSources: AnimationSources {};
    };
    class I_APC_Wheeled_03_cannon_F: I_APC_Wheeled_03_base_F {
        class AnimationSources: AnimationSources {
            class showSLATHull;
            class showCamonetHull;
        };
    };
    class I_APC_Wheeled_03_cannon_FRL: I_APC_Wheeled_03_cannon_F {
        displayName = "AFV-4 Gorgon (slat armor)";
        class AnimationSources: AnimationSources {
            class showSLATHull: showSLATHull {
                initPhase = 1;
            };
            class showCamonetHull: showCamonetHull {
                initPhase = 1;
            };
        };
    };
    class MBT_03_base_F: Tank_F {
        animationList[] = {"showCamonetHull",1,"showCamonetTurret",1,"showCamonetCannon",1,"showCamonetCannon1",1};
        class AnimationSources: AnimationSources {
            class showCamonetCannon {
                initPhase = 1;
            };
            class showCamonetCannon1 {
                initPhase = 1;
            };
            class showCamonetHull {
                initPhase = 1;
            };
            class showCamonetTurret {
                initPhase = 1;
            };
        };
    };

    // CSAT - fuck bis
    class O_APC_Wheeled_02_rcws_v2_F: APC_Wheeled_02_base_v2_F {
        class AnimationSources: AnimationSources {
            // oh yeah, those arent inherited even tho they exist in parent variant gj bis
            class showSLATHull;
            class showCamonetHull;
            class showCanisters;
        };
    };
    class O_APC_Wheeled_02_rcws_v2_FRL: O_APC_Wheeled_02_rcws_v2_F {
        // okay ... no idea why but this fixes it
        animationList[] = {"showBags",0,"showCanisters",1,"showTools",0,"showCamonetHull",0,"showSLATHull",1};
        displayName = "MSE-3 Marid (slat armor)";
        class AnimationSources: AnimationSources {
            class showSLATHull: showSLATHull {
                initPhase = 1;
            };
            class showCanisters: showCanisters {
                initPhase = 1;
            };
            class showCamonetHull: showCamonetHull {
                //initPhase = 1;
            };
        };
    };

    class O_APC_Tracked_02_base_F: APC_Tracked_02_base_F {
        class AnimationSources: AnimationSources {
            class showSLATHull;
            class showCamonetHull;
            class showTracks;
        };
    };
    class O_APC_Tracked_02_cannon_F: O_APC_Tracked_02_base_F {
        class AnimationSources: AnimationSources {
            class showSLATHull;
            class showCamonetHull;
            class showTracks;
        };
    };
    class O_APC_Tracked_02_cannon_FRL: O_APC_Tracked_02_cannon_F {
        animationList[] = {"showTracks",1,"showCamonetHull",1,"showBags",0,"showSLATHull",1};
        displayName = "BTR-K Kamysh (slat armor)";
        class AnimationSources: AnimationSources {
            class showTracks: showTracks {
                initPhase = 1;
            };
            class showSLATHull: showSLATHull {
                initPhase = 1;
            };
            class showCamonetHull: showCamonetHull {
                initPhase = 1;
            };
        };
    };
    class MBT_02_base_F: Tank_F {
        animationList[] = {"showCamonetHull",1,"showCamonetCannon",1,"showCamonetTurret",1,"showLog",1};
        class AnimationSources : AnimationSources {
            class showCamonetCannon {
                initPhase = 1;
            };
            class showCamonetHull {
                initPhase = 1;
            };
            class showCamonetTurret {
                initPhase = 1;
            };
        };
    };
    class B_Soldier_04_F;
    class B_helipilot_F: B_Soldier_04_F {
        linkedItems[] = {"V_TacVest_blk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles"};
        respawnLinkedItems[] = {"V_TacVest_blk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles"};
    };
    class B_helicrew_F: B_helipilot_F {
        linkedItems[] = {"V_TacVest_blk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles"};
        respawnLinkedItems[] = {"V_TacVest_blk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles"};
    };
    class I_Soldier_03_F;
    class I_helipilot_F: I_Soldier_03_F {
        linkedItems[] = {"H_StrawHat","V_TacVest_oli","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_INDEP"};
        respawnLinkedItems[] = {"H_StrawHat","V_TacVest_oli","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_INDEP"};
    };
    class I_helicrew_F: I_helipilot_F {
        linkedItems[] = {"H_StrawHat","V_TacVest_oli","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_INDEP"};
        respawnLinkedItems[] = {"H_StrawHat","V_TacVest_oli","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_INDEP"};
    };
    class O_Soldier_02_F;
    class O_helipilot_F: O_Soldier_02_F {
        linkedItems[] = {"V_TacVest_khk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_OPFOR"};
        respawnLinkedItems[] = {"V_TacVest_khk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_OPFOR"};
    };
    class O_helicrew_F: O_helipilot_F {
        linkedItems[] = {"V_TacVest_khk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_OPFOR"};
        respawnLinkedItems[] = {"V_TacVest_khk","H_StrawHat","ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles_OPFOR"};
    };


    // "H_StrawHat"
};
