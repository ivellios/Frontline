/*
    Function:       FRL_Compat_fnc_bombFuses
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_replacement", "", "", "", "", "", "", "_projectile"];

if ( isNil QGVAR(FusedBombs) ) then {

    GVAR(FusedBombs) = [[_projectile, getPosATL _projectile, _replacement]];

    [{
        private _bombArray = GVAR(FusedBombs);
        {
            private _subarray = _x;
            _subarray params ["_bomb", "_pos", "_replacement"];
            if (alive _bomb) then {
                _subarray set [1, getPosATL _bomb];
                _bombArray set [_foreachIndex, _subarray];
            } else {
                private _bomb = createVehicle [_replacement, _pos, [], 0, "NONE"];
                _bomb setVectorUp [0, 0, -1];
                _bombArray deleteAt _foreachIndex;
            };
        } forEach _bombArray;
        GVAR(FusedBombs) = _bombArray;

        if (count _bombArray == 0) then {
            GVAR(FusedBombs) = nil;
            [_this select 1] call MFUNC(removePerFrameHandler);

        };
    }, 0, [] ] call MFUNC(addPerFramehandler);

} else {

    GVAR(FusedBombs) = GVAR(FusedBombs) + [[_projectile, getPosATL _projectile, _replacement]];

};
