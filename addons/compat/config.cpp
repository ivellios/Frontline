#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            dependencies[] = {"Main"};
            path = "pr\frl\addons\compat";

            // -- Add this
            class NoVehicleExplosions {
                class noExplEHCode;
	            class clientInitNoVehicleExplosions;
	            class serverInitNoVehicleExplosions;
                class initNoVehicleExplosions;
            };
            class fuses {
                class bombFuses;
            };
        };
    };
};

// -- Keep empty. -- //
