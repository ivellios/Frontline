#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Main"};
            path = PATHTO(functions);

            class clientInit;
            class createOverlay;
            class handleStatic;
        };
    };

    class cfgUAV {
        maxHeight = 150;
        maxDistance = 1000;
        fadeoutDistance = 250;
        overlayRefreshRate = 3;
    };
};

#include "ui\ui.hpp"

class RscControlsGroup;
class RscIngameUI {
    class RscUnitInfo;
    class RscOptics_AV_pilot: RscUnitInfo {
        class DriverOpticsGroup: RscControlsGroup {
            onLoad = "uiNamespace setVariable ['frl_uav_rscoverlaytarget', _this select 0]; uiNamespace setVariable ['frl_uav_rscoverlaytarget_driver', _this select 0]";
        };
    };
    class RscOptics_AV_driverNoWeapon: RscUnitInfo {
        class DriverOpticsGroup: RscControlsGroup {
            onLoad = "uiNamespace setVariable ['frl_uav_rscoverlaytarget', _this select 0]; uiNamespace setVariable ['frl_uav_rscoverlaytarget_driver', _this select 0]";
        };
    };

    class RscOptics_UAV_gunner: RscUnitInfo {
        class CA_IGUI_elements_group: RscControlsGroup {
            onLoad = "uiNamespace setVariable ['frl_uav_rscoverlaytarget', _this select 0]; uiNamespace setVariable ['frl_uav_rscoverlaytarget_gunner', _this select 0]";
        };
    };
};
