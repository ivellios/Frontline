/*
    Function:       FRL_UAV_fnc_createOverlay
    Author:         Adanteh
    Description:    Creates the static overlay for the UAV
*/
#include "macros.hpp"
#define CTRL(var1) (_groupCtrl controlsGroupCtrl var1)

if (GVAR(staticLevel) > 0) then {
    GVAR(overlayShown) = true;

    {
        private _groupCtrl = _x;
        ctrlPosition _groupCtrl params ["", "", "_width", "_height"];

        GVAR(overlayIndex) = round (GVAR(staticLevel) * 8);
        for "_i" from 1 to 8 do {
            if (_i <= GVAR(overlayIndex)) then {
                if (time > (CTRL(_i) getVariable ["updateAt", -1])) then {
                    CTRL(_i) ctrlSetText selectRandom GVAR(overlayArray);
                    CTRL(_i) setVariable ["updateAt", time + random (1 - GVAR(staticLevel) / 2)];
                    private _position = [
                        0 - (random (_width * 0.25)),
                        0 - (random (_height * 0.25)),
                        _width*1.5 + (random (_width * 0.5)),
                        _height*1.5 + (random (_height * 0.5))
                    ];
                    CTRL(_i) ctrlSetPosition _position;
                    CTRL(_i) ctrlCommit 0;
                };
            } else {
                CTRL(_i) ctrlSetText "";
                CTRL(_i) ctrlSetPosition [0, 0, 0, 0];
                CTRL(_i) ctrlCommit 0;
            };
        };

    } forEach (_this select { !isNull _x });

} else {
    if !(GVAR(overlayShown)) exitWith { };
    GVAR(overlayShown) = false;
    {
        private _groupCtrl = _x;
        for "_i" from 1 to 8 do {
            CTRL(_i) ctrlSetText "";
            CTRL(_i) ctrlSetPosition [0, 0, 0, 0];
            CTRL(_i) ctrlCommit 0;
        };
    } forEach (_this select { !isNull _x });
};
