/*
    Function:       FRL_UAV_fnc_clientInit
    Author:         Adanteh
Description:    A Function That Does stuff
*/
#include "macros.hpp"

[QADDON, configFile >> "FRL" >> "cfgUAV"] call MFUNC(cfgSettingLoad);
[QADDON, missionConfigFile >> "FRL" >> "cfgUAV"] call MFUNC(cfgSettingLoad);

// -- Get settings for the currently used UAV type
GVAR(mapUAVOpened) = false;
GVAR(uavRole) = "";
GVAR(uavSettings) = [];
GVAR(staticLevel) = 0;
GVAR(overlayIndex) = 0;
GVAR(overlayShown) = false;
GVAR(overlayArray) = [
    PATHTO(data\noise_02.paa),
    PATHTO(data\noise_03.paa),
    PATHTO(data\noise_04.paa),
    PATHTO(data\noise_05.paa),
    PATHTO(data\noise_06.paa),
    PATHTO(data\noise_07.paa),
    PATHTO(data\noise_08.paa),
    PATHTO(data\noise_09.paa),
    PATHTO(data\noise_10.paa),
    PATHTO(data\noise_11.paa),
    PATHTO(data\noise_12.paa)
];

["getConnectedUAVChanged", {
    (_this select 0) params ["_newUAV", "_oldUAV"];
    if !(isNull _newUAV) then {
        GVAR(uavSettings) = [];
    };
}] call CFUNC(addEventHandler);

[{ _this call FUNC(handleStatic) }, 0] call MFUNC(addPerFrameHandler);

[{
    private ["_map", "_data"];
    _map = (findDisplay 160) displayCtrl 1401;
    _data = !(isNull _map);
	if !(_data isEqualTo GVAR(mapUAVOpened)) then {
		GVAR(mapUAVOpened) = _data;
		["mapUAVToggled", [(["onunload", "onload"] select _data)]] call CFUNC(localEvent);
        if (_data) then {
            [_map] call CFUNC(registerMapControl);
            ["registerMapClick", [_map]] call CFUNC(localEvent);
        };
	};
}, 0] call MFUNC(addPerFrameHandler);
