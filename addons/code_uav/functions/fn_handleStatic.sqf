/*
    Function:       FRL_UAV_fnc_handleStatic
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"
#define DISP (uiNamespace getVariable [QGVAR(RscOverlay), controlNull])


if (isNull (getConnectedUAV Clib_Player)) then {
    GVAR(staticLevel) = 0;
} else {
    if (GVAR(mapUAVOpened) || { cameraOn isEqualTo (getConnectedUAV Clib_Player) }) then {
        private _distanceMax = [QGVAR(maxDistance), 500] call MFUNC(cfgSetting);
        private _fadeoutDistance = [QGVAR(fadeoutDistance), _distanceMax / 5] call MFUNC(cfgSetting);
        private _distance = Clib_Player distance (getConnectedUAV Clib_Player);
        //private _maxHeight = [QGVAR(maxHeight), 150] call MFUNC(cfgSetting);

        GVAR(staticLevel) = (1 - ((_distanceMax - _distance) / _fadeoutDistance)) min 1;
        private _baseGroup = uiNamespace getVariable [QGVAR(RscOverlayTarget), controlNull];

        // -- Workaround because this shit is insanely broken. None of the rscunitinfo works properly with onload when switching roles.
        // -- It loads in Pilot when you enter the turret, unless you enter turret directly from scroll menu in which it case it won't load in pilot
        private _uavRole = (uavControl getConnectedUAV Clib_player) select 1;
        if (GVAR(uavRole) != _uavRole) then {
            GVAR(uavRole) = _uavRole;
        };
        _baseGroup = uiNamespace getVariable [format [QGVAR(rscoverlayTarget_%1), _uavRole], _baseGroup];
        if !(isNull _baseGroup) then {
            if (isNull (_baseGroup getVariable [QGVAR(RscOverlay), controlNull])) then {
                [[_fnc_scriptNameShort, _baseGroup, ctrlIDC _baseGroup], "red"] call MFUNC(debugMessage);
                private _ctrl = (uiNamespace getVariable "RscUnitInfo") ctrlCreate [QGVAR(CtrlOverlayGroup), 1402, _baseGroup];
                (ctrlPosition _baseGroup) params ["", "", "_width", "_height"];
                (_ctrl controlsGroupCtrl 101) ctrlSetText "a3\drones_f\weapons_f_gamma\reticle\data\UAV_Optics_Gunner_CA.paa";
                {
                    _x ctrlSetPosition [0, 0, _width, _height];
                    _x ctrlCommit 0;
                } forEach [_ctrl, _ctrl controlsGroupCtrl 101];
                _baseGroup setVariable [QGVAR(RscOverlay), _ctrl];
            };
        };
    } else {
        GVAR(staticLevel) = 0;
        GVAR(uavRole) = "";
    };
};


[
    uiNamespace getVariable [format [QGVAR(rscoverlayTarget_%1), GVAR(uavRole)], controlNull],
    (findDisplay 160) displayCtrl 1402,
    (findDisplay 160) displayCtrl 1403
] call FUNC(createOverlay);
