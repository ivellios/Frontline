#define TEXTURELAYERS class textureLayer_0 : RscPicture {\
  idc = 1;\
  x = 0;\
  y = 0;\
  w = 0;\
  h = 0;\
  text = "";\
  fade = 0;\
};\
class textureLayer_2: textureLayer_0 { idc = 2; };\
class textureLayer_3: textureLayer_0 { idc = 3; };\
class textureLayer_4: textureLayer_0 { idc = 4; };\
class textureLayer_5: textureLayer_0 { idc = 5; };\
class textureLayer_6: textureLayer_0 { idc = 6; };\
class textureLayer_7: textureLayer_0 { idc = 7; };\
class textureLayer_8: textureLayer_0 { idc = 8; };

class RscPicture;
class RscMapControl;
class RscControlsGroupNoScrollbars;
class ScrollBar;

class RscTitles {
    #include "RscOverlay.hpp"
};

#include "CtrlOverlayGroup.hpp"
#include "RscDisplayAVTerminal.hpp"
