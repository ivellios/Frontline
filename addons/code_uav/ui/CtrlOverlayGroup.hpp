class GVAR(CtrlOverlayGroup): RscControlsGroupNoScrollbars {
    idc = 1402;
    x = 0;
    y = 0;
    w = safeZoneW;
    h = safeZoneH;

    class Controls {
        TEXTURELAYERS

        class BaseTexture: RscPicture {
            idc = 101;
            x = 0;
            y = 0;
            w = safeZoneW;
            h = safeZoneH;
            text = "";
        };
    };
};
