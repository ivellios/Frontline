class GVAR(RscOverlay) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_UAV_RscOverlay', _this select 0];";
    onUnLoad = "";
    fadeIn = 0.1;
    fadeOut = 0.1;

    class Controls {
        class Group: RscControlsGroupNoScrollbars {
            idc = 1402;
            x = safeZoneX;
            y = safeZoneY;
            w = safeZoneW;
            h = safeZoneH;

            class Controls {
                TEXTURELAYERS
            };
        };
    };
};
