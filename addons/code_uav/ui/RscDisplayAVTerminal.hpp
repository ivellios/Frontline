class RscDisplayAVTerminal {
    class ControlsBackground {
        class CA_Map: RscMapControl {
            idc = 1401;
        };
    };
    class Controls {
        class AVT_PIP1;
        class AVT_PIP1_OverlayGroup: AVT_PIP1 {
            idc = 1402;
            type = 15;
            shadow = 0;
            style = 16;
            class VScrollbar: ScrollBar { width = 0; };
            class HScrollbar: ScrollBar { height = 0; };
            class Controls {
                TEXTURELAYERS
            };
        };
        class AVT_PIP2;
        class AVT_PIP2_OverlayGroup: AVT_PIP2 {
            idc = 1403;
            type = 15;
            shadow = 0;
            style = 16;
            class VScrollbar: ScrollBar { width = 0; };
            class HScrollbar: ScrollBar { height = 0; };
            class Controls {
                TEXTURELAYERS
            };
        };
    };
};
