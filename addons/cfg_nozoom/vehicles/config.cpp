class CfgPatches {
    class FRL_NoZoomVehicles {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.70;
        requiredAddons[] = {
            "A3_Data_F_Loadorder",
            "A3_Data_F_Curator_Loadorder",
            "A3_Data_F_Kart_Loadorder",
            "A3_Data_F_Bootcamp_Loadorder",
            "A3_Data_F_Heli_Loadorder",
            "A3_Data_F_Mark_Loadorder",
            "A3_Data_F_Exp_A_Loadorder",
            "A3_Data_F_Exp_B_Loadorder",
            "A3_Data_F_Exp_Loadorder",
            "A3_Data_F_Jets_Loadorder",
            "A3_Data_F_Argo_Loadorder",
            "A3_Data_F_Patrol_Loadorder",
            "A3_Data_F_Orange_Loadorder",
            "A3_Data_F_Tacops_Loadorder"
        };
    };
};

// 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
class RCWSOptics {
    initFov = 0.4375;
    minFov = 0.06; // 0.034
    maxFov = 0.4375;

};
class Optics_Armored {
    class Wide: RCWSOptics {
        initFov = "(36 / 120)";
        minFov = "(36 / 120)";
        maxFov = "(36 / 120)";

    };
    class Medium: Wide {
        initFov = 0.075; /// 0.07
        minFov = 0.075; /// 0.07
        maxFov = 0.075; /// 0.07

    };
    class Narrow: Medium {
        initFov = 0.042; // 0.28
        minFov = 0.042; // 0.28
        maxFov = 0.042; // 0.28

    };
    initFov = 0;
    minFov = 0;
    maxFov = 0;

};
class Optics_Gunner_MBT_01: Optics_Armored {
    class Wide: Wide {
    };
    class Medium: Medium {
    };
    class Narrow: Narrow {
    };
};
class Optics_Gunner_MBT_02: Optics_Armored {
    class Wide: Wide {
    };
    class Medium: Medium {
    };
    class Narrow: Narrow {
    };
};

class CfgVehicles {
    class All;
    class AllVehicles: All {
        class NewTurret {
        };
        class ViewOptics;
    };
    class Land: AllVehicles {
    };
    class LandVehicle: Land {
    };
    class Car: LandVehicle {
    };
    class Tank: LandVehicle {
    };
    class Tank_F: Tank {
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
    };
    class MBT_01_base_F: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class OpticsIn: Optics_Gunner_MBT_01 {
                    class Wide: Wide {
                        initFov = "(36 / 120)";
                        minFov = "(36 / 120)";
                        maxFov = "(36 / 120)";

                    };
                    class Medium: Medium {
                        initFov = 0.075; /// 0.07
                        minFov = 0.075; /// 0.07
                        maxFov = 0.075; /// 0.07

                    };
                    class Narrow: Narrow {
                        initFov = 0.042; // 0.28
                        minFov = 0.042; // 0.28
                        maxFov = 0.042; // 0.28

                    };
                    initFov = 0;
                    minFov = 0;
                    maxFov = 0;

                };
            };
        };
    };
    class MBT_02_base_F: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class OpticsIn: Optics_Gunner_MBT_02 {
                    class Wide: Wide {
                        initFov = "(36 / 120)";
                        minFov = "(36 / 120)";
                        maxFov = "(36 / 120)";

                    };
                    class Medium: Medium {
                        initFov = 0.075; /// 0.07
                        minFov = 0.075; /// 0.07
                        maxFov = 0.075; /// 0.07

                    };
                    class Narrow: Narrow {
                        initFov = 0.042; // 0.28
                        minFov = 0.042; // 0.28
                        maxFov = 0.042; // 0.28

                    };
                    initFov = 0;
                    minFov = 0;
                    maxFov = 0;

                };
            };
        };
    };
    class APC_Tracked_01_base_F: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: RCWSOptics {
                    initFov = 0.4375;
                    minFov = 0.06; // 0.034
                    maxFov = 0.4375;

                };
            };
        };
    };
    class APC_Tracked_02_base_F: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class OpticsIn: Optics_Gunner_MBT_02 {
                    class Wide: Wide {
                        initFov = "(36 / 120)";
                        minFov = "(36 / 120)";
                        maxFov = "(36 / 120)";

                    };
                    class Medium: Medium {
                        initFov = 0.075; /// 0.07
                        minFov = 0.075; /// 0.07
                        maxFov = 0.075; /// 0.07

                    };
                    class Narrow: Narrow {
                        initFov = 0.042; // 0.28
                        minFov = 0.042; // 0.28
                        maxFov = 0.042; // 0.28

                    };
                    initFov = 0;
                    minFov = 0;
                    maxFov = 0;

                };
            };
        };
    };
    class Car_F: Car {
        class NewTurret: NewTurret {
        };
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
    };
    class MRAP_01_base_F: Car_F {
    };
    class MRAP_02_base_F: Car_F {
    };
    class Wheeled_APC_F: Car_F {
        class NewTurret: NewTurret {
        };
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
    };
    class APC_Wheeled_01_base_F: Wheeled_APC_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: RCWSOptics {
                    initFov = 0.4375;
                    minFov = 0.06; // 0.034
                    maxFov = 0.4375;

                };
            };
        };
    };
    class APC_Wheeled_02_base_F: Wheeled_APC_F {
        class ViewOptics;
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: RCWSOptics {
                };
            };
        };
    };
    class MRAP_01_gmg_base_F: MRAP_01_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: RCWSOptics {
                    initFov = 0.4375;
                    minFov = 0.1; // 0.034
                    maxFov = 0.4375;

                };
            };
        };
    };
    class MRAP_02_hmg_base_F: MRAP_02_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: RCWSOptics {
                    initFov = 0.4375;
                    minFov = 0.1; // 0.034
                    maxFov = 0.4375;

                };
            };
        };
    };
    class APC_Wheeled_02_base_v2_F: APC_Wheeled_02_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    initFov = 0.31;
                    minFov = 0.06; // 0.034
                    maxFov = 0.31;

                };
            };
        };
    };

    // Statics
    class StaticWeapon: LandVehicle {
        class Turrets {
            class MainTurret: NewTurret {
                class ViewOptics {
                    initFov = 0.75;
                    minFov = 0.375;
                    maxFov = 0.75;

                };
            };
        };
    };
    class StaticMGWeapon: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class HMG_01_base_F: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    initFov = 0.25; //0.117;
                    minFov = 0.06;//0.029;
                    maxFov = 0.25; //0.117;

                };
            };
        };
    };
    class AA_01_base_F: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    initFov = 0.117; //0.08333;
                    minFov = 0.06; //0.04167;
                    maxFov = 0.117; //0.08333;

                };
            };
        };
    };
    class AT_01_base_F: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    initFov = 0.117; //0.08333;
                    minFov = 0.04167;
                    maxFov = 0.117; //0.08333;

                };
            };
        };
    };
};
