//#include "macros.hpp"

class CfgPatches {
    class FRL_NoZoom {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.70;
        requiredAddons[] = {
            "A3_Data_F_Loadorder",
            "A3_Data_F_Curator_Loadorder",
            "A3_Data_F_Kart_Loadorder",
            "A3_Data_F_Bootcamp_Loadorder",
            "A3_Data_F_Heli_Loadorder",
            "A3_Data_F_Mark_Loadorder",
            "A3_Data_F_Exp_A_Loadorder",
            "A3_Data_F_Exp_B_Loadorder",
            "A3_Data_F_Exp_Loadorder",
            "A3_Data_F_Jets_Loadorder",
            "A3_Data_F_Argo_Loadorder",
            "A3_Data_F_Patrol_Loadorder",
            "A3_Data_F_Orange_Loadorder",
            "A3_Data_F_Tacops_Loadorder",
            "FRL_Weapons"
        };
    };
};

#define __ZOOMMIN 0.75
#define __ZOOMMAX 0.375
#define __ZOOMINIT 0.75

class RCWSOptics {};
class CfgWeapons {
    class Default {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class PistolCore: Default {
    };
    class RifleCore: Default {
    };
    class GrenadeLauncher: Default {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class UGL_F: GrenadeLauncher {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class Rifle: RifleCore {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class Pistol: PistolCore {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class Pistol_Base_F: Pistol {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class Put: Default {
        class PutMuzzle: Default {
            opticsZoomMin = __ZOOMMAX;
            opticsZoomMax = __ZOOMMIN;
            opticsZoomInit = __ZOOMINIT;

        };
    };

    // CUSTOM, CAN'T OVERRIDE DEFAULT, SO NEED TO DO IT HERE SEEING IT'S WHAT MOST INHERITS FROM
    class ItemCore: Default {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;
    };

    class InventoryItem_Base_F {
    };
    class InventoryOpticsItem_Base_F: InventoryItem_Base_F {
    };
    class optic_Arco: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ARCO2collimator {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_Hamr: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class Hamr2Collimator {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_Aco: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_ACO_grn: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_Aco_smg: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_ACO_grn_smg: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_Holosight: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_Holosight_smg: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_SOS: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class Iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_MRCO: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class MRCOcq {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_Nightstalker: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class NCTALKEP {
                };
                class Iron: NCTALKEP {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_DMS: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class Snip {
                };
                class Iron: Snip {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_Yorris: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_MRD: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_AMS_base: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class Iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_KHS_base: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class Iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
};
class CfgVehicles {
    class All {
        class ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class ViewCargo {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class ViewOptics;
    };
    class AllVehicles: All {
        class NewTurret {
            class ViewGunner: ViewOptics {
                minFov = __ZOOMMAX;
                maxFov = __ZOOMMIN;

            };
            class ViewOptics {
            };
        };
        class ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class ViewCargo {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class ViewOptics {
        };
    };
    class Land: AllVehicles {
    };
    class LandVehicle: Land {
    };
    class Car: LandVehicle {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Motorcycle: LandVehicle {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Tank: LandVehicle {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Man: Land {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Air: AllVehicles {
    };
    class Helicopter: Air {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
        class ViewOptics: ViewOptics {
        };
    };
    class Plane: Air {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class ViewOptics: ViewOptics {
        };
    };
    class Ship: AllVehicles {
        class Turrets {
        };
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class ViewOptics;
    };
    class Truck: Car {
        minFov = __ZOOMMAX;
        maxFov = __ZOOMMIN;

    };
    class StaticWeapon: LandVehicle {
        class Turrets {
            class MainTurret: NewTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class UAV: Plane {
    };
    class CAManBase: Man {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Plane_Base_F: Plane {
        class Turrets {
            class CopilotTurret: NewTurret {
            };
        };
    };
    class Car_F: Car {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class NewTurret: NewTurret {
        };
        class Turrets {
            class MainTurret: NewTurret {
                class ViewOptics: ViewOptics {
                };
            };
        };
    };
    class Helicopter_Base_F: Helicopter {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class Helicopter_Base_H: Helicopter_Base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
            class CopilotTurret: MainTurret {
            };
        };
    };
    class Heli_Light_01_base_F: Helicopter_Base_H {
        class ViewOptics: ViewOptics {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Heli_Attack_01_base_F: Helicopter_Base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class Heli_Attack_02_base_F: Helicopter_Base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class Heli_Transport_01_base_F: Helicopter_Base_H {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class Ship_F: Ship {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Boat_F: Ship_F {
    };
    class Boat_Armed_01_base_F: Boat_F {
        class Turrets: Turrets {
            class FrontTurret: NewTurret {
                class ViewOptics: RCWSOptics {
                };
            };
            class RearTurret: FrontTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class Offroad_01_base_F: Car_F {
    };
    class Offroad_01_military_base_F: Offroad_01_base_F {
    };
    class Offroad_01_armed_base_F: Offroad_01_military_base_F {
        class Turrets: Turrets {
            class M2_Turret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class MRAP_03_base_F: Car_F {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Heli_light_03_base_F: Helicopter_Base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class UAV_01_base_F: Helicopter_Base_F {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class Viewoptics: ViewOptics {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class PilotCamera {
            class OpticsIn {
                class Wide {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class UAV_02_base_F: UAV {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class Viewoptics: ViewOptics {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class Heli_Transport_03_base_F: Helicopter_Base_H {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class Heli_Transport_04_base_F: Helicopter_Base_H {
        class Turrets: Turrets {
            class CopilotTurret: CopilotTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class LSV_01_base_F: Car_F {
        class Turrets: Turrets {
        };
    };
    class LSV_01_armed_base_F: LSV_01_base_F {
        class Turrets: Turrets {
            class TopTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
            class CodRiverTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class LSV_02_base_F: Car_F {
        class Turrets: Turrets {
        };
    };
    class LSV_02_armed_base_F: LSV_02_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class VTOL_Base_F: Plane_Base_F {
        class Turrets: Turrets {
        };
    };
    class VTOL_01_base_F: VTOL_Base_F {
        class Turrets: Turrets {
            class CopilotTurret: CopilotTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class VTOL_01_armed_base_F: VTOL_01_base_F {
        class Turrets: Turrets {
            class GunnerTurret_01: NewTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class VTOL_02_base_F: VTOL_Base_F {
        class Turrets {
            class GunnerTurret: NewTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class UAV_05_Base_F: UAV {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class Viewoptics: ViewOptics {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class UAV_06_base_F: Helicopter_Base_F {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
};
