/*
    Function:       FRL_Statics_fnc_getRearmCost
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_staticWeapon"];

switch (true) do {
   case (_staticWeapon isKindOf "rhs_SPG9_base"): { 200 };   //-- listed as StaticMGWeapon soo nope
   case (_staticWeapon isKindOf "StaticMGWeapon"): { 100 };
   case (_staticWeapon isKindOf "StaticMortar"): { 400 };
   case (_staticWeapon isKindOf "StaticAAWeapon"): { 200 };  // -- manpads
   case (_staticWeapon isKindOf "StaticATWeapon"): { 200 };  // -- AT guns and ATGMs
   case (_staticWeapon isKindOf "LIB_FlaK_36_base"): { 200 };
   case (_staticWeapon isKindOf "RHS_ZU23_base"): { 500 };
   case (_staticWeapon isKindOf "staticCannon"): { 200 };    // -- all AAguns really
   default { 150 };
};
