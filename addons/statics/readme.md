## Contents

Static weapon system. Gets rid of the backpack oogabooga and makes it a 'skill' locked to certain Roles, much like RPs.
Uses our 3d interact system for rearming, packing up and moving. Main advantage is better, faster and more accurate placement that doesn't suck, plus the ability to use backpacks for other things instead.
