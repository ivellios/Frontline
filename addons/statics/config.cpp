#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {"frl_staticammo_bag", "frl_staticfortify_bag"};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\statics";

            class canPlaceStatic;
            class convertMagsRemaining;
            class fortifyStatic;
            class fortifyStaticCondition;
            class getRearmCost;
            class moveStatic;
            class moveStaticCondition;
            class moveStaticEnd;
            class packupStatic;
            class packupStaticCondition;
            class placeStatic;
            class rearmStatic;
            class clientInit;
            class clientInitMenu;
            class init;
            class serverInit;
        };
    };
};

class cfgVehicles {
  class Weapon_Bag_Base;
  class frl_staticammo_bag : Weapon_Bag_Base {
    scope = 2;
    displayName = "Static Weapon Ammo";
    isStaticAmmo = 1;
    model = "\A3\weapons_f\Ammoboxes\bags\Backpack_Gorod";
    editorCategory = "EdCat_Equipment";
    editorSubcategory = "EdSubcat_DismantledWeapons";
    picture = "\A3\Weapons_F\Ammoboxes\Bags\data\UI\icon_B_C_Small_mcamo.paa";
    mass = 100;
  };

  class frl_staticfortify_bag : Weapon_Bag_Base {
    scope = 2;
    displayName = "Static Fortication Supplies";
    isStaticAmmo = 0;
    model = "\A3\weapons_f\Ammoboxes\bags\Backpack_Gorod";
    editorCategory = "EdCat_Equipment";
    editorSubcategory = "EdSubcat_DismantledWeapons";
    picture = "\A3\Weapons_F\Ammoboxes\Bags\data\UI\icon_B_C_Small_mcamo.paa";
    mass = 100;
  };

};
