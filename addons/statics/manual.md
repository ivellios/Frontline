## Manual Contents ##
- You can rearm static weapons with the Static Weapon Ammo backpack (Generic one for all statics)
- You need the Team Leader in static weapon team to deploy it, plus one extra squadmate nearby
- You can pack up the static at any time, but this won't restore ammo
- After placing a static weapon down, there's a 20 minute cooldown till you can place your next one
- You can only have one static weapon per squad. Placing a new one will ask if you want to delete the old one
- After respawn the leader can put a new static weapon down
