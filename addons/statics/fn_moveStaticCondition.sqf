/*
    Function:       FRL_Statics_fnc_moveStaticCondition
    Author:         Adanteh
    Description:    Condition to check if you can move a static weapon
*/
#include "macros.hpp"

params ["_caller", "_staticWeapon"];
if (count crew _staticWeapon > 0) exitWith {
    ["showNotification", ["You can't move an occupied static weapon", "none"]] call CFUNC(localEvent);
    false;
};

if (_staticWeapon iskindof "StaticCannon") exitWith {
    ["showNotification", ["Cannons are restricted to pushing only", "none"]] call CFUNC(localEvent);
    false;
};

if !(_staticWeapon getVariable [QSVAR(canMove), true]) exitWith {
    ["showNotification", ["This static is locked and can't be moved", "none"]] call CFUNC(localEvent);
    false;
};

true
