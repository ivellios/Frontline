/*
    Function:       FRL_Statics_fnc_fortifyStaticCondition
    Author:         Adanteh
    Description:    Checks if you can build the dirt mound around static
*/
#include "macros.hpp"

params ["_caller", "_staticWeapon"];
private _posATL = getPosATL _staticWeapon;
if ((_posATL select 2) > 0.5) exitWith {
    ["showNotification", ["You can only fortify on ground level", "nope"]] call CFUNC(localEvent);
    false;
};

private _backpackType = backpack _caller;
if !(_backpackType isKindOf ["frl_staticfortify_bag", (configFile >> "cfgVehicles")]) exitWith {
    ["showNotification", ["You aren't carrying a fortification backpack", "nope"]] call CFUNC(localEvent);
    false;
};

true;
