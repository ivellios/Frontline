/*
    Function:       FRL_Statics_fnc_canPlaceStatic
    Author:         Adanteh
    Description:    Condition for placing a static
*/
#include "macros.hpp"

private _group = group Clib_Player;

// -- Squad type has a static weapon type
private _playerGroupType = _group getVariable [QMVAR(gType), ""];
private _staticClass = EGVAR(rolesspawns,squadNamespace) getVariable [(format ["%1_staticWeapon", _playerGroupType]), ""];
if (_staticClass == "") exitWith { };

// -- Realtime checks
private _position = getPosATL CLib_Player;
private _canPlace = ["placeStaticRealtime", [CLib_Player, _position], true] call MFUNC(checkConditions);

// -- Cooldown for placement
if ((_group getVariable [QGVAR(cooldown), -9999]) > serverTime) exitWith { false };

true;
