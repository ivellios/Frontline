/*
    Function:       FRL_Statics_fnc_clientInit
    Author:         Adanteh
    Description:    Adds static weapon system. Allows deploying one, like RP, replacing old ones, moving and rearming
*/
#include "macros.hpp"

[QUOTE(ADDON), configFile >> "FRL" >> "cfgStatics"] call MFUNC(cfgSettingLoad);
GVAR(movingMode) = false;
GVAR(staticWeapon) = objNull;
GVAR(placeIntersect) = objNull;

["MissionStarted", {
    ["Deploy Static Weapon", CLib_Player, 0, {
        [QGVAR(canPlaceStatic), FUNC(canPlaceStatic), [], 5, QGVAR(clearStatic)] call CFUNC(cachedCall);
    }, {
        QGVAR(clearStatic) call CFUNC(localEvent);
        [false] call FUNC(placeStatic);
    }, ["showWindow", false]] call CFUNC(addAction);
}] call CFUNC(addEventHandler);

// PLACEMENT CONDITIONS
// -- Check for if this kit has the ability to place it
["placeStaticOnCall", {
    params ["_caller", "_pos"];
    private _canPlace = (({ _x == "Static"} count (_caller getVariable [QSVAR(rAbilities), []]) > 0));
    if !(_canPlace) then {
        ["showNotification", ["Only team leader can place static weapons"]] call CFUNC(localEvent);
    };
    _canPlace;
}] call MFUNC(addCondition);

// -- Need player nearby
["placeStaticOnCall", {
    params ["_caller", "_pos"];
    private _nearPlayerToBuild = [QGVAR(nearPlayerToBuild), -1] call MFUNC(cfgSetting);
    #ifdef DEBUGFULL
        private _nearPlayerToBuild = -1;
    #endif
    private _canPlace = (_nearPlayerToBuild <= 1 || {
        private _buildRadius = [QGVAR(nearPlayerToBuildRadius), 10] call MFUNC(cfgSetting);
        private _nearUnits = [_caller, _buildRadius] call MFUNC(getNearUnits);
        private _count = {((group _x) == _group && !([_x] call MFUNC(isUnconscious))) } count _nearUnits;  // -- This excludes player, so do +1
        (_count >= _nearPlayerToBuild)
    });
    if !(_canPlace) then {
        ["showNotification", [format ["Need %1 squad members nearby to place", _nearPlayerToBuild]]] call CFUNC(localEvent);
    };
    _canPlace;
}] call MFUNC(addCondition);

// -- Reset the timer when the person that has the 'static' ability respawns
["Killed", {
    (_this select 0) params ["_unit"];
	private _isSet = (group _unit) getVariable QGVAR(cooldown);
	if !(isNil "_isSet") then {
		(group _unit) setVariable [QGVAR(cooldown), nil, true];
	};
}] call CFUNC(addEventHandler);
