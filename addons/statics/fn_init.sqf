/*
    Function:       FRL_Statics_fnc_init
    Author:         Adanteh
    Description:    Adds static weapon system. Allows deploying one, like RP, replacing old ones, moving and rearming
*/
#include "macros.hpp"

[QGVAR(addAmmo), {
    (_this select 0) params ["_staticWeapon"];
    _staticWeapon setVehicleAmmoDef 1;
}] call CFUNC(addEventHandler);

["staticDeployed", {
    (_this select 0) params ["_staticWeapon", "_caller"];

    private _side = side group _caller;
    private _varname = format [QGVAR(%1), GVAR(staticIndex)];
    GVAR(staticIndex) = GVAR(staticIndex) + 1;
    publicVariable QGVAR(staticIndex);

    ["vehicleInit", [_staticWeapon, _side, _varname]] call CFUNC(globalEvent);
}] call CFUNC(addEventHandler);
