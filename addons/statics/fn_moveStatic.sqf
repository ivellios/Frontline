/*
    Function:       FRL_Statics_fnc_moveStatic
    Author:         Adanteh
    Description:    Adds moving of static weapon
*/
#include "macros.hpp"

params ["_caller", "_staticWeapon"];

GVAR(movingMode) = true;
GVAR(validPlacement) = false;
GVAR(validPlacementReason) = "";
GVAR(staticMoving) = _staticWeapon;

[QMVAR(showControlsHUD), [
    ["To drop the weapon", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\rmb_ca.paa"], { [GVAR(staticMoving)] call FUNC(moveStaticEnd) }, true, [1], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }]
]] call CFUNC(localEvent);

_staticWeapon enableSimulation false; // -- Do this instantly local, so we don't need to wait for server
_staticWeapon allowDamage false;
_staticWeapon disableCollisionWith Clib_Player;
_staticWeapon lock true;
_staticWeapon setVariable [QMVAR(Used), true, true];

["enableSimulation", [_staticWeapon, false]] call CFUNC(serverEvent);
detach _staticWeapon;

[{
	(_this select 0) params ["_staticWeapon"];

	if !(GVAR(movingMode)) exitWith {
		[_this select 1] call MFUNC(removePerFrameHandler);
	};

    if (!alive Clib_Player || { [Clib_Player] call MFUNC(isUnconscious) }) exitWith {
        [GVAR(staticMoving), true] call FUNC(moveStaticEnd);
    };

    private _offset = [0, 2] select (cameraView == "EXTERNAL");
    private _cameraPosClose = (positionCameraToWorld [0,-0.3,1.5 + _offset]);
    private _cameraPosFar = (positionCameraToWorld [0,-0.3,4 + _offset]);
    _ins = lineIntersectsSurfaces [
        AGLtoASL (_cameraPosClose),
        AGLtoASL (_cameraPosFar),
        _staticWeapon,
        Clib_Player,
        true,
        1,
        "GEOM",
        "VIEW"
    ];

    private _validPlacement = true;
    private "_vectorUp";
    if (count _ins > 0) then {
        //GVAR(placeIntersect) = (_ins select 0 select 2);
        _objPosition = ASLToATL (_ins select 0 select 0);
        _objPosition set [2, ((_objPosition select 2) + 0.1) max 0.05];
        _vectorUp = if (isNull (_ins select 0 select 2)) then {
            surfaceNormal _objPosition;
        } else {
            (_ins select 0 select 1);
        };

        if (_vectorUp select 2 <= 0.75) exitWith { _validPlacement = false; };
        _staticWeapon setPosATL _objPosition;
    } else {
        _staticWeapon setPosATL (positionCameraToWorld [0,-0.3,3 + _offset]);
        _vectorUp = (surfaceNormal _cameraPosClose);
        //GVAR(placeIntersect) = objNull;
    };

    if (_validPlacement) then {
        _staticWeapon setDir (getDirVisual CLib_Player);
        _staticWeapon setVectorUp _vectorUp;
    } else {
        GVAR(validPlacementReason) = "Too steep";
    };

    // -- Validation starts here -- //
    if (true) then {
        if (count (lineIntersectsSurfaces [
            AGLtoASL (_staticWeapon modelToWorldVisual [0, 0, 0.5]),
            AGLtoASL _cameraPosClose,
            _staticWeapon,
            CLib_Player,
            true
        ]) > 0) exitWith {
            _validPlacement = false;
            GVAR(validPlacementReason) = "Need LOS";
        };

        // -- Back top -- //
        if (count (lineIntersectsSurfaces [
            AGLtoASL (_staticWeapon modelToWorldVisual [-0.4, 0.3, 0.5]),
            AGLtoASL (_staticWeapon modelToWorldVisual [0.4, 0.3, 0.5]),
            _staticWeapon,
            objNull,
            true
        ]) > 0) exitWith {
            _validPlacement = false;
            GVAR(validPlacementReason) = "Invalid Location";
        };

        // -- Front top -- //
        if (count (lineIntersectsSurfaces [
            AGLtoASL (_staticWeapon modelToWorldVisual [-0.4, -0.3, 0.5]),
            AGLtoASL (_staticWeapon modelToWorldVisual [0.4, -0.3, 0.5]),
            _staticWeapon,
            objNull,
            true
        ]) > 0) exitWith {
            _validPlacement = false;
            GVAR(validPlacementReason) = "Invalid Location";
        };
    };

    GVAR(validPlacement) = _validPlacement;
}, 0, [_staticWeapon]] call MFUNC(addPerFramehandler);

true;
