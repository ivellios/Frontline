/*
    Function:       FRL_Statics_fnc_rearmStatic
    Author:         Adanteh
    Description:    Rearms a static weapon
*/
#include "macros.hpp"

params ["_caller", "_staticWeapon"];
private _backpackType = backpack Clib_Player;

private _spawnpoint = _staticWeapon getVariable ["spawnpoint", objNull];
if (isnull _spawnpoint) then {
    if !(_backpackType isKindOf ["frl_staticammo_bag", (configFile >> "cfgVehicles")]) exitWith {
        ["showNotification", ["You aren't carrying the required ammo", "nope"]] call CFUNC(localEvent);
        RETURN(true);
    };
    removeBackpack Clib_Player;
} else {
    private _suppliesRequired = [_staticWeapon] call FUNC(getRearmCost);
    private _supplies = _spawnpoint getvariable ["supplies", 0];
    if (_supplies < _suppliesRequired) exitWith {
        ["showNotification", [format ["You need %1 supply in the FO to rearm", _suppliesRequired], "nope"]] call CFUNC(localEvent);
        RETURN(true);
    };
    _spawnpoint setVariable ["supplies", _supplies - _suppliesRequired, true];
};

[QGVAR(addAmmo), _staticWeapon, [_staticWeapon]] call CFUNC(targetEvent);
["showNotification", ["Finished rearming", "ok"]] call CFUNC(localEvent);

true
