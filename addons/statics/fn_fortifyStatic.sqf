/*
    Function:       FRL_Statics_fnc_fortifyStatic
    Author:         Adanteh
    Description:    Builds a dirt mound around a static weapon
*/
#include "macros.hpp"

params ["_caller", "_staticWeapon"];
private _posATL = getPosATL _staticWeapon;
private _fortification = createVehicle ["Fort_EnvelopeSmall", _posATL, [], 0, "CAN_COLLIDE"];

_posATL set [2, (_posATL select 2) + 0.1];
_fortification setPosATL _posATL;
_fortification setDir (getDir _staticWeapon);
_fortification setVectorUp (surfaceNormal _posATL);

removeBackpack _caller;

true;
