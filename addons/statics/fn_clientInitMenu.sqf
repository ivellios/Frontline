/*
    Function:       FRL_Statics_fnc_clientInitMenu
    Author:         Adanteh
    Description:    Adds F interact menu options, and timer in top left
*/
#include "macros.hpp"

["StaticWeapon", [
    "",
    "Move",
    { _this call FUNC(moveStatic) },
    { _this call FUNC(moveStaticCondition) }
]] call EFUNC(interactthreed,menuOptionAdd);

["StaticWeapon", [
    "",
    "Rearm",
    { _this call FUNC(rearmStatic) },
    { true }
]] call EFUNC(interactthreed,menuOptionAdd);

["StaticWeapon", [
    "",
    "Pack up",
    { _this call FUNC(packupStatic) },
    { _this call FUNC(packupStaticCondition) }
]] call EFUNC(interactthreed,menuOptionAdd);

["StaticWeapon", [
    "",
    "Fortify",
    { _this call FUNC(fortifyStatic) },
    { _this call FUNC(fortifyStaticCondition) }
]] call EFUNC(interactthreed,menuOptionAdd);

// -- Show the cooldown bar in top left if your group has a cooldown set
[{
    private _cooldownFinishedAt = (group Clib_Player) getVariable [QGVAR(cooldown), 0];
    if (_cooldownFinishedAt > serverTime) then {
        private _cooldownTime = [QGVAR(cooldown), 20*60] call MFUNC(cfgSetting);
        ["progressHudShow", [QGVAR(cooldownBar),
        {
            (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\statics.paa");
            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.27, 0.42, 0.95, 0.6];
        },
        {
            params ["_cooldownFinishedAt", "_cooldownTime"];
            private _timeLeft = (_cooldownFinishedAt - serverTime) max 0;
            private _percentage = (0 max (1 - (_timeLeft / _cooldownTime))) min 1;
            (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
            (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Static Cooldown %1", ([_timeLeft, "MM:SS", false] call BIS_fnc_secondsToString)]);
        },
        [_cooldownFinishedAt, _cooldownTime]]] call CFUNC(localEvent);
    } else {
        ["progressHudHide", [QGVAR(cooldownBar)]] call CFUNC(localEvent);
    };
}, 5] call MFUNC(addPerFramehandler);
