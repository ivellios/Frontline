/*
    Function:       FRL_Statics_fnc_moveStaticEnd
    Author:         Adanteh
    Description:    Stops moving the static weapon and drops it in the current stop
    Example:        [frl_statics_staticMoving] call FRL_Statics_fnc_moveStaticEnd
*/
#include "macros.hpp"

params ["_staticWeapon", ["_force", false]];
if (!(GVAR(validPlacement)) && !_force) exitWith { };
GVAR(movingMode) = false;

[QMVAR(hideControlsHUD)] call CFUNC(localEvent);
["staticMoved", [_staticWeapon, Clib_Player]] call CFUNC(serverEvent);

/*if !(isNull GVAR(placeIntersect)) then {
    private _relativeModelPos = GVAR(placeIntersect) worldToModelVisual (getPosATLVisual _staticWeapon);
    _staticWeapon attachTo [GVAR(placeIntersect), _relativeModelPos];
};*/

_staticWeapon enableSimulation true; // -- Do this instantly local, so we don't need to wait for server
_staticWeapon allowDamage true;
_staticWeapon enableCollisionWith Clib_Player;
_staticWeapon lock false;
_staticWeapon setVariable [QMVAR(Used), nil, true];
["enableSimulation", [_staticWeapon, true]] call CFUNC(serverEvent);
