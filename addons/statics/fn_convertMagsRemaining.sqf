/*
    Function:       FRL_Statics_fnc_convertMagsRemaining
    Author:         N3croo
    Description:    A Function That Does stuff
    [magazinesAmmo cursorObject, "verify"] call FRL_Statics_fnc_convertMagsRemaining
*/
#include "macros.hpp"

params ["_magazines", "_mode"];

switch (_mode) do {
    case "compress": {
        private _j = 0;
        private _run = true;
        while {true} do {
            for [{_i = _j}, {_i < count _magazines}, {_i = _i + 1}] do {
                private _entry = _magazines select _i;
                private _duplicateCount = {_entry isEqualTo _x} count _magazines;
                if (_duplicateCount > 1) exitWith {
                    // since we can run into duplicates beyond the pointer we have to go over it again
                    _j = _i + 1; // to save some time, we skip what we alread checked
                    _magazines set [_i, _entry + [_duplicateCount]];
                    _magazines = _magazines - [_entry];
                };
            };
            if (_i >= count _magazines) exitWith {};
        };
    };
    case "uncompress": {
        {
            if (count _x > 2) then {
                private _duplicateCount = _x select 2;
                _magazines = _magazines - [_x];
                _x resize 2;
                for "_i" from 1 to _duplicateCount do {
                    _magazines pushBack _x;
                };
            };
        } forEach _magazines;
    };
    case "verify": {
        // since the order can be different and wen dont care for that...
        _magazines = [_magazines, "compress"] call FUNC(convertMagsRemaining);
        _magazines = [_magazines, "uncompress"] call FUNC(convertMagsRemaining);

        private _staticNew = cursorObject;
        _staticNew setVehicleAmmo 0;
        systemChat str count _magazines;
        {
            _x params ["_magazine", "_ammoCount"];
            _staticNew addMagazine [_magazine, _ammoCount];
        } forEach _magazines;
    };
};

_magazines;
