/*
    Function:       FRL_Statics_fnc_packupStatic
    Author:         Adanteh
    Description:    Disassembles the static weapon (Allows redeploying somewhere else)
*/
#include "macros.hpp"

params ["_caller", "_staticWeapon"];
private _magsRemaining = magazinesAmmo _staticWeapon;
private _group = group _caller;

_magsRemaining = [_magsRemaining, "compress"] call FUNC(convertMagsRemaining);

_group setVariable [QGVAR(ammoLeft), _magsRemaining, true];
_group setVariable [QGVAR(cooldown), nil, true];
deleteVehicle _staticWeapon;

true;
