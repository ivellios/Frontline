/*
    Function:       FRL_Statics_fnc_placeStatic
    Author:         Adanteh
    Description:    Places a static weapon
*/
#include "macros.hpp"

params [["_forceReplace", false]];

if (!_forceReplace && {
    // -- Do non-real time calls. If these conditions fail they will show a notification themselves. -- //
    // -- Action menu option will be shown if these return false for better understanding of the rally point placement conditions -- //
    private _canPlace = ["placeStaticOnCall", [CLib_Player, getPosATL Clib_Player], true] call MFUNC(checkConditions);
    !_canPlace;
}) exitWith { false };

private _group = group Clib_Player;
private _currentStatic = _group getVariable [QGVAR(currentStatic), objNull];
private _exit = false;
if !(isNull _currentStatic) then {
    if (_forceReplace) then {
        if (count (crew _currentStatic) > 0) then {
            moveOut (crew _currentStatic select 0);
        };
        deleteVehicle _currentStatic;
    } else {
        // -- If you already have a static weapon, ask if you want to remove it first
        [[Clib_Player, true, "Would you like to replace your current static weapon?", {
            params ["_unit", "", "_answer"];
            switch (_answer) do {
                case 1: {
                    [true] call FUNC(placeStatic);
                };
            };
        }, 5]] call EFUNC(voting,voteHUD);
        _exit = true;
    };
};

if (_exit) exitWith { };
private _playerGroupType = _group getVariable [QMVAR(gType), ""];
private _staticClass = EGVAR(rolesspawns,squadNamespace) getVariable [(format ["%1_staticWeapon", _playerGroupType]), ""];
private _staticNew = createVehicle [_staticClass, Clib_Player modelToWorldVisual [0, 1, 0], [], 0, "CAN_COLLIDE"];
_staticNew setVariable ["side", playerSide, true];
_group setVariable [QGVAR(cooldown), serverTime + ([QGVAR(cooldown), 20*60] call MFUNC(cfgSetting)), true];
_group setVariable [QGVAR(currentStatic), _staticNew, true];

["staticDeployed", [_staticNew, Clib_Player]] call CFUNC(localEvent);

[Clib_Player, _staticNew] call FUNC(moveStatic);

// -- If the static is redeployed after being packed up, reset the ammo to the old state
if !(_forceReplace) then {
    private _oldAmmo = _group getVariable [QGVAR(ammoLeft), []];

    if (count _oldAmmo >= 0) then {
        _oldAmmo = [_oldAmmo, "uncompress"] call FUNC(convertMagsRemaining);
        _staticNew setVehicleAmmo 0;
        private _i = 0;
        {
            _i = _i + 1;
            _x params ["_magazine", "_ammoCount"];
            _staticNew addMagazine [_magazine, _ammoCount];
        } forEach _oldAmmo;
    };
};
