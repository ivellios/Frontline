/*
    Function:       FRL_Statics_fnc_serverInit
    Author:         Adanteh
    Description:    Adds static weapon system. Allows deploying one, like RP, replacing old ones, moving and rearming
*/
#include "macros.hpp"

GVAR(staticIndex) = 0; // Used for varname assigning
publicVariable QGVAR(staticIndex);

// -- Update the possition of the clutter cutter after moving the static weapon (If one doesnt exist yet, create it)
["staticMoved", {
	(_this select 0) params ["_staticWeapon", "_caller"];
	private _posATL = getPosATL _staticWeapon;
	private _cluttercutter = _staticWeapon getVariable ["ClutterCutter", objNull];
	if (_posATL select 2 < 0.5) then { // -- Only create clutter cutters on terrain level
		if !(isNull _cluttercutter) then { // setPos ON THESE THINGS DOESN"T WORK. ALWAYS DELETE AND MAKE A NEW ONE
			deleteVehicle _cluttercutter;
		};
		private _cluttercutter = createVehicle ["Land_ClutterCutter_large_F", (ASLToATL (_staticWeapon modelToWorldVisualWorld [0, 2.5, 0])), [], 0, "CAN_COLLIDE"];
		_staticWeapon setVariable ["ClutterCutter", _cluttercutter];
	} else {
		if !(isNull _cluttercutter) then {
			deleteVehicle _cluttercutter;
		};
	};
}] call CFUNC(addEventHandler);
