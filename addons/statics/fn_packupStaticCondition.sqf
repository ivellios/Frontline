/*
    Function:       FRL_Statics_fnc_packupStaticCondition
    Author:         Adanteh
    Description:    Disassembles the static weapon condition
*/
#include "macros.hpp"

params ["_caller", "_staticWeapon"];
private _currentStatic = (group _caller) getVariable [QGVAR(currentStatic), objNull];
if (_currentStatic != _staticWeapon) exitWith {
    ["showNotification", ["You can only pack up your own statics", "nope"]] call CFUNC(localEvent);
    false;
};

if !(alive _staticWeapon) exitWith {
    ["showNotification", ["You can't pack up destroyed statics", "nope"]] call CFUNC(localEvent);
    false;
};

if (count (crew _staticWeapon) > 0) exitWith {
    ["showNotification", ["You can't pack up statics with occupants", "nope"]] call CFUNC(localEvent);
    false
};

true;
