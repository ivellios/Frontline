#define MODULE InteractThreeD

// -- Global defines -- //
#include "..\main\macros_local.hpp"


#define __CACHE_TIME 2
#define __RANGE_CACHE 10
#define __MARGIN 1
#define __RANGE_SHOW 6
#define __RANGE_INTERACT 2

#define __ANIMSPEED 0.2
#define CTRL(var1) ((uiNamespace getVariable [QGVAR(menu), displayNull]) displayCtrl var1)
