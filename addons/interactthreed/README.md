## Contents

This PBO includes the F interact system.
It allows opening doors and climbing letters, with a 3D indicated icon.

The important thing is that all data is only ever retrieved once, and cached as much as possible. This means that if you walk towards a building it'll only ever retrieve the possible actions for that building once and caches it all

It also supports a menu for vehicles, plus adding scripted actions for some vehicles. Main advantage is one combined action for getting in Back, plus being able to quickly double tap F to enter ANY back seat, instead of placing down explosive charges

### Old Description
Some info on this system and how all data is handled.
Whenever you come close to a building type that isn't processed yet (Come nearby a similar building in the past)
we check all its config values to detect all doors. We save the model positions of all door handles under the building type, so we can reuse this when we get close to a similar building
Additionally on the actual building object we save the world positions to the building itself, so we can use those when we come close to the same building again.

Seeing the position of opened and closed door is not the same, we save the general position and check those based on distance.
If we come within the distance, we recheck the actual current position of the action position using the saved name.
