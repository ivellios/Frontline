/*
    Function:       FRL_InteractThreeD_fnc_addInteractObject
    Author:         Adanteh
    Description:    Adds an object to be interactive
*/
#include "macros.hpp"

if (isNil QGVAR(interactObjects)) then { GVAR(interactObjects) = []; };
params ["_object", "_offset", "_actionText", "_action", ["_actionType", "generic"]];
GVAR(interactObjects) pushBack [_object, _offset, _actionText, _action, _actionType];
