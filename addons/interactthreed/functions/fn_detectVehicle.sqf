/*
    Function:       FRL_InteractThreeD_fnc_detectVehicle
    Author:         Adanteh
    Description:    Detects if we're aiming at a vehicle close to our front
    Example:        call FRL_InteractThreeD_fnc_detectVehicle
*/
#include "macros.hpp"
#define __RANGE 3.5

if !(isNull (objectParent Clib_Player)) exitWith { locationNull }; // -- In vehicle
if !(alive Clib_Player) exitWith { locationNull }; // -- DED

private _start = AGLtoASL (Clib_Player modelToWorldVisual (Clib_Player selectionPosition "pilot"));
private _end = (_start vectorAdd (getCameraViewDirection Clib_Player vectorMultiply __RANGE));
private _intersections = lineIntersectsSurfaces [_start, _end, cameraOn, objNull, true, 1, "GEOM"];
if (_intersections isEqualTo []) exitWith { locationNull; };

private _object = _intersections select 0 select 2;
if (isNull _object) exitWith { locationNull; };

_object = _object getVariable [QSVAR(proxy), _object]; // Linked object
private _options = [_object] call FUNC(menuOptions);
if (_options isEqualTo []) exitWith { locationNull };

// -- Generic object lock (Someone is carrying?)
if (_object getVariable [QMVAR(Used), false]) exitWith { locationNull };

_object;
