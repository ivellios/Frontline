/*
    Function:       FRL_InteractThreeD_fnc_findDoors
    Author:         Adanteh
    Description:    Retrieves all door positions for a buildling type and saves them for future usage
    Example:        [cursorobject] call FRL_InteractThreeD_fnc_findDoors
*/
#include "macros.hpp"

params ["_building"];
private _buildingType = toLower (typeOf _building);
private _cachedIndex = GVAR(interactNamespace) getVariable _buildingType;

if !(isNil "_cachedIndex") exitWith {
	_cachedIndex
};

// -- Check all user actions if they are related to doors. Save the actions usihng the same selectionPostion and combine the code for easier usage
private _animationConfig = (configFile >> "CfgVehicles" >> _buildingType >> "UserActions");
private _doors = [];
private _userActions = [_animationConfig, 1] call bis_fnc_returnChildren;
// -- We need to use this weird function, some configs (like IFA ones) have fucked up inheritance and normal config commands wont return what we need

{
	private _actionCfg = _x;
	private _actionName = toLower (getText (_actionCfg >> "displayName"));
	private _className = toLower (configName _actionCfg);
	private _seperateDoors = [];
    //testvar pushBack _actionName;
	if ((_actionName find "open" != -1) || { (_className find "open" != -1) }) then {
		if (toLower (configName _x) find "locked" == -1) then {

			// -- Find all the other actions using this same thing -- //
			private _memoryPoint = getText (_actionCfg >> "position");
			private _condition = format ["(getText (_x >> 'position') == '%1')", _memoryPoint];
			private _combinedAction = "";
			{
				private _condition = (getText (_X >> "condition"));
				private _action = (getText (_X >> "statement"));
				_combinedAction = [_combinedAction, _condition, _action] call FUNC(combineCode);
				nil;
			} count (configProperties [_animationConfig, _condition, true]);

			_combinedAction = compile _combinedAction;

			// -- Try to get the door handle position -- //
			private _splitPosition = _memoryPoint splitString "_";
			private _trySelection = format ["%1_Handle_%2_Axis", (_splitPosition select 0), (_splitPosition select 1)];
			private _doorPos = _building selectionPosition _trySelection;
			private _realTime = true;
			if (_doorPos isEqualTo [0, 0, 0]) then {
				_doorPos = _building selectionPosition _memoryPoint;
			} else {
				_memoryPoint = _trySelection;
				_realTime = true;
			};

			_doors pushBack ["\A3\Ui_f\data\IGUI\Cfg\Actions\open_door_ca.paa", _memoryPoint, _doorPos, _combinedAction, _realTime, "DOOR"];
		};
	};
	nil;
} count _userActions;

_doors append ([_building, _buildingType] call FUNC(findLadders));

// -- Save even when there's 0 doors, so we know we don't need to recheck this building -- //
GVAR(interactNamespace) setVariable [_buildingType, _doors];
_doors;
