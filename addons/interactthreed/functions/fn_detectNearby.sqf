/*
    Function:       FRL_InteractThreeD_fnc_detectNearby
    Author:         Adanteh
    Description:    Detects nearby objects, doors, ladders and so on. Checks the functions and figures out which action is closest on screen
*/
#include "macros.hpp"

if !(alive CLib_Player) exitWith { GVAR(activeAction) = []; };
if !(isNull (findDisplay 49)) exitWith { GVAR(activeAction) = []; }; // Esc menu open
if !(isNull (objectParent Clib_Player)) exitWith { GVAR(activeAction) = []; }; // Esc menu open
if (isNull (findDisplay 46)) exitWith { /*GVAR(activeAction) = [];*/ }; // Mission display closed

private _unitPos = (CLib_Player modelToWorldVisual (CLib_Player selectionPosition "spine2"));
private _anyActive = false;

//GVAR(allActions) = [];
//GVAR(screenPos) = [];
{
	_x params ["_building", "_icon", "_memoryPoint", "_doorPosReal", "_action", "_realTime", "_iconText", ["_range", __RANGE_INTERACT], ["_rangeShow", __RANGE_SHOW], ["_margin", __MARGIN]];

	if (_realTime) then {
		if (_memoryPoint isEqualType "") then {
			_doorPosReal = (_building modelToWorldVisual (_building selectionPosition _memoryPoint));
		} else {
			_doorPosReal = (_building modelToWorldVisual _memoryPoint);
		};
	};
	private _screenPos = worldToScreen _doorPosReal;
	if (count _screenPos > 0) then {
		private _realDistance = (_doorPosReal distance _unitPos) max 0.4;
		if (_realDistance <= _rangeShow) then {
			private _active = false;
			if (_realDistance <= _range) then {
				if ((_screenPos distance2D [0.5, 0.5]) <= (_margin / _realDistance)) then {
					_active = true;
					_anyActive = true;
					GVAR(activeAction) = _x;
				};
			};
			[_icon, _doorPosReal, _realDistance, _active, _iconText] call FUNC(drawIconReal);
		};
	};
	nil;
} count ([QGVAR(nearInteractCache), {
	private _nearest = nearestObjects [CLib_Player, ["House", "Wall_F"], 40];
	private _nearInteract = [];
	{
		private _building = _x;
		private _doors = [_x] call FUNC(findDoors);
		{
			_x params ["_icon", "_memoryPoint", "_doorPos", "_action", "_realTime", "_iconText"];
			private _doorPosReal = _building modelToWorldVisual _doorPos;
			if ((_doorPosReal distance CLib_Player) <= __RANGE_CACHE) then {
				_nearInteract pushBack [_building, _icon, _memoryPoint, _doorPosReal, _action, _realTime, _iconText];
			};
			nil;
		} count _doors;
		nil;
	} count _nearest;

	GVAR(interactObjects) = GVAR(interactObjects) - [objNull];
	{
		_x params ["_object", "_offset", "_actionText", "_action", "_actionType"];
		private _actionPosReal = _object modelToWorldVisual _offset;
		if (isNull _object) then {
			GVAR(interactObjects) deleteAt (GVAR(interactObjects) find _x);
		} else {
			if ((_actionPosReal distance CLib_Player) <= __RANGE_CACHE) then {
				_nearInteract pushBack [_object, "", _actionType, _actionPosReal, _action, false, _actionText];
			};
		};
		nil;
	} count GVAR(interactObjects);

    _nearInteract

}, [], __CACHE_TIME] call CFUNC(cachedCall));

if !(_anyActive) then {
	GVAR(activeAction) = [];
};
