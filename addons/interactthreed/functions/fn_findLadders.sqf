/*
    Function:       FRL_InteractThreeD_fnc_findLadders
    Author:         Adanteh
    Description:    Gets ladders config and creates action code
*/
#include "macros.hpp"

params ["_building", "_buildingType"];
private _ladders = getArray (configFile >> "CfgVehicles" >> _buildingType >> "ladders");
private _allLadders = [];
{
	_x params ["_ladderBottom", "_ladderTop"];

	_actionBottom = compile (format ["CLib_Player action ['ladderUp', _building, %1, 0]", _forEachIndex]);
	_actionTop = compile (format ["CLib_Player action ['ladderDown', _building, %1, 1]", _forEachIndex]);
	_allLadders pushBack ["\A3\ui_f\data\igui\cfg\actions\ladderup_ca.paa", _ladderBottom, (_building selectionPosition _ladderBottom) vectorAdd [0, 0, 0.75], _actionBottom, false, "LADDER"];
	_allLadders pushBack ["\A3\ui_f\data\igui\cfg\actions\ladderdown_ca.paa", _ladderTop, (_building selectionPosition _ladderTop) vectorAdd [0, 0, 0.5], _actionTop, false, "LADDER"];
} forEach _ladders;
_allLadders;
