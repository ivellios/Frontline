/*
    Function:       FRL_InteractThreeD_fnc_removeInteractObject
    Author:         Adanteh
    Description:    Adds an object to be interactive
*/
#include "macros.hpp"

params ["_objectToRemove", ["_typeToRemove", ""]];
private _objects = +GVAR(interactObjects);
{
    private _object = _x param [0, objNull];
    private _type = _x param [4, ""];
    if (_objectToRemove isEqualTo _object) then {
        if ((_typeToRemove == "") || { _type == _typeToRemove }) then {
            GVAR(interactObjects) deleteAt (GVAR(interactObjects) find _x);
        };
    };
} forEach _objects;
