/*
    Function:       FRL_InteractThreeD_fnc_canEnterSeat
    Author:         Adanteh
    Description:    Condition to check if we can enter a seat. (Call before assigining seat)
    Example:        [Player, cursorObject, "driver"] call FRL_InteractThreeD_fnc_canEnterSeat
*/
#include "macros.hpp"

params ["_unit", "_vehicle", "_seatType", ["_seatPath", [-1]]];

// -- Checked stacked conditions (Correct side, dunno what else)
private _canEnter = ["canEnterVehicle", [_unit, _vehicle, false], true] call MFUNC(checkConditions);
if !(_canEnter) exitWith { false };

// -- Check side
private _side = _vehicle getVariable ["side", sideLogic];
if ((_side != sideLogic) && { _side != (side group _unit) } && {!(_vehicle isKindOf "StaticWeapon")}) exitWith {
    ["You are not allowed to use enemy vehicles", "nope"] call MFUNC(notificationShow);
    false
};

// -- If 'any' seat is given, check only if there's an empty seat in general. plus side requirements
if (_seatType isEqualTo "ANY") exitWith {
    true
};

// -- Check crewman requirements for this vehicle
#ifndef DEBUGFULL
private _crewSeatsAvailable = ["crewSeatAvailable", [_unit, _vehicle], true] call MFUNC(checkConditions);
if (!_crewSeatsAvailable && { toLower _seatType in ["commander", "gunner", "driver"] }) exitWith {
    ["You need a crewman kit to enter this seat", ICON(content,clear), 'orange'] call MFUNC(notificationShow);
    false
};
#endif

private _canEnter = switch (toLower _seatType) do {
    case "commander": { _vehicle emptyPositions "Commander" > 0 };
    case "gunner": { _vehicle emptyPositions "Gunner" > 0 };
    case "cargo": {
        if (_seatPath isEqualTo [-1]) then {
             _vehicle emptyPositions "Cargo" > 0
        } else {
            private _openSeat = (fullCrew [_vehicle, "cargo", true]) select (((_x select 2) isEqualTo _seatPath) && {isNull (_x select 0)});
            (count _openSeat > 0);
        };
    };
    case "turret": {
        if (_seatPath isEqualTo [-1]) then {
            (count ((fullCrew [_vehicle, "turret", true]) select {isNull (_x select 0)}) > 0);
        } else {
            private _openSeat = (fullCrew [_vehicle, "turret", true]) select (((_x select 2) isEqualTo _seatPath) && {isNull (_x select 0)});
            (count _openSeat > 0);
        };
    };
    case "driver": { _vehicle emptyPositions "Driver" > 0 };
    default { false };
};

if !(_canEnter) exitWith {
    ["You can't enter this seat", "nope"] call MFUNC(notificationShow);
    false;
};

true;
