/*
    Function:       FRL_InteractThreeD_fnc_clientInit
    Author:         Adanteh
    Description:    Inits the 3D interact system
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(icon) = (MEDIAPATH + "interact\3d_key_f.paa");
if (isNil QGVAR(interactNamespace)) then { GVAR(interactNamespace) = false call CFUNC(createNamespace); };
if (isNil QGVAR(interactObjects)) then { GVAR(interactObjects) = []; };

GVAR(activeAction) = [];
GVAR(releaseNeeded) = "-";
GVAR(menuOpen) = false;
GVAR(menuTarget) = objNull;

["General", "Interact3D", ["Interact", "Uses the currently focussed 3D action"], {
	if (GVAR(activeAction) isEqualTo []) exitWith { false };

	GVAR(activeAction) params ["_building", "", "_memoryPoint", "", "_action"];
	// -- This make sure if a call is done, it doesn't repeat itself till we let go of the button first (Or the action is run on another door!)
	if (GVAR(releaseNeeded) == _memoryPoint) exitWith { true };
	GVAR(releaseNeeded) = _memoryPoint;
	if (GVAR(menuOpen)) then {
		//[nil, 2] call FUNC(menuUseAction);
	} else {
        private _dikButton = _key;
		[_building] call _action;
	};

	true;
}, { GVAR(releaseNeeded) = "-"; false }, [DIK_F, [nil, nil, nil]], false, false] call MFUNC(addKeybind);
[{ call FUNC(detectNearby) }, 0, []] call MFUNC(addPerFramehandler); // -- 3D F button


// -- Detect if you're looking at a vehicle
[{
    private _target = call FUNC(detectVehicle); // -- Check if you're looking at vehicle
    if !(_target isEqualTo GVAR(menuTarget)) then {
        ["vehicleInteractChanged", [_target, GVAR(menuTarget)]] call CFUNC(localEvent);
        GVAR(menuTarget) = _target;

        if (isNull _target) then {
            call FUNC(menuClose);
        } else {
            [_target] call FUNC(menuOpen);
        };
    }
}, 0.1, []] call MFUNC(addPerFramehandler);
