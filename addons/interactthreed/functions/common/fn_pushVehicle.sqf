/*
    Function:       FRL_InteractThreeD_fnc_canEnterSeat
    Author:         Adanteh
    Description:    Condition to check if we can enter a seat. (Call before assigining seat)
    Example:        [Player, cursorObject, "driver"] call FRL_InteractThreeD_fnc_canEnterSeat
*/
#include "macros.hpp"

params ["_caller", "_vehicle"];

if ((count (crew _vehicle select { alive _x })) > 0) exitWith {
    ["You can't push vehicles with occupants", "nope"] call MFUNC(notificationShow);
    true;
};

private _isBoat = (_vehicle isKindOf "Ship" || _vehicle isKindOf "Ship_F");
["setVelocity", _vehicle, [_vehicle, [(sin (direction _caller)) * 3, (cos (direction _caller)) * 3, 0.5], _isBoat]] call CFUNC(targetEvent);

true;
