/*
    Function:       FRL_InteractThreeD_fnc_getInAnyTurret
    Author:         Adanteh
    Description:    Puts you in the first open turret (No alt syntax normally available for this. Thank BIS command consistency)
    Example:        [Player, cursorObject] call FRL_InteractThreeD_fnc_getInAnyTurret
*/
#include "macros.hpp"

params ["_caller", "_vehicle"];

call {
    scopeName "turretLoop";
    private _turretSeats = fullCrew [_vehicle, "turret", true];
    {
        _x params ["_unit", "", "", "_turretPath", "_personTurret"];
        if (isNull (_x select 0)) then {
            _caller action ["GetInTurret", _vehicle, _turretPath];
            breakTo "turretLoop";
        };
    } forEach _turretSeats;
};

true;

actrion
