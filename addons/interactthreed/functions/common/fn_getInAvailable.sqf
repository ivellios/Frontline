/*
    Function:       FRL_InteractThreeD_fnc_getInAvailable
    Author:         Adanteh
    Description:    Puts a unit in a vehicle seat availabe to him (In whatever seat is possible)
*/
#include "macros.hpp"

params ["_unit", "_vehicle", ["_instant", false]];

if (unitIsUAV _vehicle) exitWith { false };

private _crewSeatsAvailable = ["crewSeatAvailable", [_unit, _vehicle], true] call MFUNC(checkConditions);
private _seats = [["Cargo"], ["Driver", "Gunner", "Commander", "Cargo"]] select _crewSeatsAvailable;

//[[_fnc_scriptNameShort, _this, _crewSeatsAvailable], "purple"] call MFUNC(debugMessage);
testvar = _vehicle;
{
    private _desiredRole = _x;
    {
        _x params ["_occupant", "_role", "_cargoIndex", "_turretPath"];
        //[[_fnc_scriptNameShort, _x, _desiredRole], "cyan"] call MFUNC(debugMessage);
        if ((isNull _occupant) || {!alive _occupant}) then { // -- Filter out occupied seats
            private _effectiveRole = toLower _role;

            // -- Workaround for vehicles that have no driver at all (Fucking lol, applies to static weapons mostly)
            if ((_effectiveRole == "driver") && {getNumber (configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "hasDriver") isEqualTo 0}) exitWith { }; // Ignoring Non Driver (static weapons)
            if (_effectiveRole == "turret") then {
                _effectiveRole = ["cargo", "gunner"] select (_cargoIndex < 0);
            };
            if (_effectiveRole != _desiredRole) exitWith { };

            if !(_turretPath isEqualTo []) then {
                ["debugMessage", [[0, _fnc_scriptNameShort, _effectiveRole, _turretPath], "purple"]] call CFUNC(localEvent);
                if (_instant) then {
                    _unit moveInTurret [_vehicle, _turretPath];
                } else {
                    _unit action ["GetInTurret", _vehicle, _turretPath];
                };
            } else {
                if (_cargoIndex > -1) then {
                    // GetInCargo expects the index of the seat in the "cargo" array from fullCrew
                    // See description: https://community.bistudio.com/wiki/fullCrew
                    private _cargoActionIndex = -1;
                    {
                        if ((_x select 2) == _cargoIndex) exitWith {_cargoActionIndex = _forEachIndex};
                    } forEach (fullCrew [_vehicle, "cargo", true]);

                    ["debugMessage", [[1, _fnc_scriptNameShort, _effectiveRole, _cargoIndex, _cargoActionIndex], "purple"]] call CFUNC(localEvent);
                    if (_cargoActionIndex > -1) then {
                        if (_instant) then {
                            _unit moveInCargo [_vehicle, _cargoActionIndex];
                        } else {
                            _unit action ["GetInCargo", _vehicle, _cargoActionIndex];
                        };
                    };

                } else {

                    ["debugMessage", [[2, _fnc_scriptNameShort, _effectiveRole, _role, _cargoIndex, _turretPath], "purple"]] call CFUNC(localEvent);

                    if (_instant) then {
                        switch (_effectiveRole) do {
                            case "driver": { _unit moveInDriver _vehicle; };
                            case "gunner": { _unit moveInGunner _vehicle; };
                            case "commander": { _unit moveInCommander _vehicle; };
                        };
                    } else {
                        _unit action ["GetIn" + _role, _vehicle];
                    };

                };
            };
            RETURN(true);
        };
    } forEach (fullCrew [_vehicle, "", true]);
} forEach _seats;


["No available seat to enter", "nope"] call MFUNC(notificationShow);
false
