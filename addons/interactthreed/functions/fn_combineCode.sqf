/*
    Function:       FRL_InteractThreeD_fnc_combineCode
    Author:         Adanteh
    Description:    Combines code of multiple actions into one, and makes sure the 'this' entries get replaced by _building
					This makes it possible to read the userAction entries and call them with normal paramaters
*/
#include "macros.hpp"

params ["_current", "_condition", "_action"];

// -- Workaround for broken BIS_fnc_Door 1.68, replace by our own function
// https://feedback.bistudio.com/T124191    in 1.72 this is still broken
private _thisDoor = _action find "BIS_fnc_Door";
while  { _thisDoor != -1 } do {
	_action = ((_action select [0, _thisDoor]) + QFUNC(door) + (_action select [(_thisDoor + 12), count _action - 1]));
	_thisDoor = _action find "BIS_fnc_Door";
};

private _thisAction = _action find "this";
while  { _thisAction != -1 } do {
	_action = ((_action select [0, _thisAction]) + "_building" + (_action select [(_thisAction + 4), count _action - 1]));
	_thisAction = _action find "this";
};

private _thisCondition = _condition find "this";
while  { _thisCondition != -1 } do {
	_condition = ((_condition select [0, _thisCondition]) + "_building" + (_condition select [(_thisCondition + 4), count _condition - 1]));
	_thisCondition = _condition find "this";
};

_current + (format ["if (%1) exitWith { %2 };", _condition, _action]);
