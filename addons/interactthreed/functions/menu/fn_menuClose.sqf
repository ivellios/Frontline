/*
    Function:       FRL_InteractThreeD_fnc_menuClose
    Author:         Adanteh
    Description:    Closes the menu with an animation and resets all vars
*/
#include "macros.hpp"

//[[_fnc_scriptNameShort], "lime"] call MFUNC(debugMessage);

if !(GVAR(menuOpen)) exitWith { };

GVAR(menuOpen) = false;
GVAR(menuOptionsActive) = [];

//(findDisplay 46) displayRemoveEventHandler ["MouseZChanged", GVAR(menuScrollEH)];
(findDisplay 46) displayRemoveEventHandler ["KeyDown", GVAR(menuButtonDownEH)];
(findDisplay 46) displayRemoveEventHandler ["KeyUp", GVAR(menuButtonUpEH)];
GVAR(menuButtonDownEH) = -1;
GVAR(menuButtonUpEH) = -1;

private _menuGroup = CTRL(1);
private _ctrlPos = ctrlPosition _menuGroup;
_ctrlPos set [2, 0];
_menuGroup ctrlSetPosition _ctrlpos;
_menuGroup ctrlCommit __ANIMSPEED;
QGVAR(menuLayer) cutText ["", "Plain"];
