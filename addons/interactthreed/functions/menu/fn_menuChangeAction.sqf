/*
    Function:       FRL_InteractThreeD_fnc_menuChangeAction
    Author:         Adanteh
    Description:    Changes selected action through scrolling
*/
#include "macros.hpp"

// ## DISABLED
params ["", "_direction"];
private _scrollList = CTRL(2);
private _newSelection = if (_direction < 0) then {
    (lbSize _scrollList - 1) min (lbCursel _scrollList + 1);
} else {
    0 max (lbCursel _scrollList - 1);
};
(GVAR(menuOptionsActive) param [_newSelection, {}]) params ["_action", "_condition"];

_scrollList lbSetCurSel _newSelection;
