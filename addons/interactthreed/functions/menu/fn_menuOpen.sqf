/*
    Function:       FRL_InteractThreeD_fnc_menuOpen
    Author:         Adanteh
    Description:    Opens in menu in the bottom right to use number keys for basic actions
*/
#include "macros.hpp"

params ["_target", ["_menuLevel", ""]];

//[[_fnc_scriptNameShort, _this], "lime"] call MFUNC(debugMessage);

private _menuOptions = [_target, _menuLevel] call FUNC(menuOptions);
if (_menuOptions isEqualTo []) exitWith { };

if !(GVAR(menuOpen)) then {
    GVAR(menuOpen) = true;
    QGVAR(menuLayer) cutRsc [QGVAR(menu), "Plain", 1, false];
};

private _menuGroup = CTRL(1);
private _scrollList = CTRL(2);
private _background = CTRL(3);
// -- Add all the options
lbClear _scrollList;
GVAR(menuOptionsActive) = [];

{
    _x params ["_icon", "_name", "_action", "_condition", ["_enabled", true], ["_menuLevel", ""]];
    if (_name isEqualType {}) then { _name = call _name };
    if (_enabled isEqualType {}) then { _enabled = call _enabled };

    private _index = _scrollList lbAdd _name;
    _scrollList lbSetData [_index, str _forEachIndex];
    _scrollList lbSetColor [_index, [[1, 1, 1, 0.25], [1, 1, 1, 1]] select _enabled];
    GVAR(menuOptionsActive) pushBack [_action, _condition, _enabled];
} forEach _menuOptions;
_scrollList lbSetCurSel 0;

// -- Animate the menu
private _menuHeight = GRIDY(1) * (count _menuOptions);
private _controlPosition = ctrlPosition _menuGroup;
_controlPosition set [0, safeZoneX + safeZoneW - (_controlPosition select 2) - GRIDX(2)];
_controlPosition set [1, safeZoneY + safeZoneH - _menuHeight - GRIDY(4)];
_controlPosition set [3, _menuHeight];
_scrollList ctrlSetPosition [0, 0, GRIDX(10), _menuHeight];
_scrollList ctrlCommit 0;
_menuGroup ctrlSetPosition _controlPosition;
_menuGroup ctrlCommit 0;
_background ctrlSetBackgroundColor [0, 0, 0, 0];

//GVAR(menuScrollEH) = (findDisplay 46) displayAddEventHandler ["MouseZChanged", { _this call FUNC(menuChangeAction)}];
if (GVAR(menuButtonDownEH) == -1) then {
    GVAR(menuButtonDownEH) = (findDisplay 46) displayAddEventHandler ["KeyDown", { ['down', _this] call FUNC(menuUseActionKey) }];
    GVAR(menuButtonUpEH) = (findDisplay 46) displayAddEventHandler ["KeyUp", { ['up', _this] call FUNC(menuUseActionKey) }];
};
