/*
    Function:       FRL_InteractThreeD_fnc_clientInitMenu
    Author:         Adanteh
    Description:    Init's the 3D scroll menu
*/
#include "macros.hpp"


GVAR(menuButtonDownEH) = -1;
GVAR(menuButtonUpEH) = -1;
GVAR(menuUpReturn) = false;
GVAR(menuSubLevel) = -1;
