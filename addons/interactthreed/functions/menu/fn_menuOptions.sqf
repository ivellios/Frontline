/*
    Function:       FRL_InteractThreeD_fnc_menuOptions
    Author:         Adanteh
    Description:    Retrieves options for given object
    Example:        [cursorObject] call FRL_InteractThreeD_fnc_menuOptions
*/
#include "macros.hpp"

params [["_vehicle", objNull, [objNull]], ["_menuLevel", ""], ["_noCache", false]];

private _vehicleType = typeOf _vehicle;
private _isStaticWeapon = _vehicle isKindOf "StaticWeapon";
private _menuOptions = GVAR(interactNamespace) getVariable (_vehicleType + "_menuOptions");

private _hasSeats = _vehicle getVariable QGVAR(hasSeats);
if (isNil "_hasSeats") then {
    private _allSeats = fullCrew [_vehicle, "", true];
    _hasSeats = (count _allSeats > 0);
    _vehicle setVariable [QGVAR(hasSeats), _hasSeats, false];
};

#ifdef DEBUGFULL
    _noCache = true;
#endif
if (_noCache || (isNil "_menuOptions")) then {

    _menuOptions = [];
    private _allSeats = fullCrew [_vehicle, "", true];

    if (count _allSeats > 0) then {
        if (unitIsUAV _vehicle) exitWith { }; // -- lol

        _menuOptions pushBack [
            "",
            "Get in",
            { [_this select 0, _this select 1] call FUNC(getInAvailable) },
            { [_this select 0, _this select 1, "ANY"] call FUNC(canEnterSeat) }
        ];

        if !(_isStaticWeapon) then {
            {
                _x params ["_role", "_picture"];
                private _roleName = _role;

                private _seatExists = ({ _x select 1 == _role } count _allSeats) > 0;
                if (_role == "driver") then { // Workaround for some things having a driverSeat, although there isn't actually one
                    _seatExists = getNumber (configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "hasDriver") > 0;
                    if (_vehicle isKindOf "Air") then { _roleName = "pilot"; };
                };

                // -- Format the action for get in directly
                private _actionCode = compile format ["(_this select 0) action ['getIn%1', (_this select 1)]; true", _role];
                private _conditionCode = if (_seatExists) then {
                    compile format ["[_this select 0, _this select 1, '%2'] call %1;", QFUNC(canEnterSeat), _role]
                } else {
                    { false };
                };


                // -- Don't call someone controlling an airvehicle a driver

                _menuOptions pushBack [
                    _picture,
                    "Get in " + _roleName,
                    _actionCode,
                    _conditionCode,
                    _seatExists
                ];
            } forEach [
                ["driver", (MEDIAPATH + "roles\crewman.paa")],
                ["gunner", ""],
                ["commander", ""]
            ];
        };
    };

    if (count _menuOptions > 0) then {
        if !(_isStaticWeapon) then {
            _menuOptions pushBack [
                "",
                "Drag out wounded",
                { _this call EFUNC(medical,dragOut) },
                { true }
            ];

            _menuOptions pushBack [
                "",
                "Push",
                { _this call FUNC(pushVehicle) },
                { true }
            ];

            _menuOptions pushBack [
                "",
                "Repair",
                { ['start', _this] call EFUNC(maintenance,repairLightAction) },
                { ['condition', _this] call EFUNC(maintenance,repairLightAction) }
            ];

            // Add check if this vehicle needs crewman
            private _needsCrewman = [_vehicleType] call EFUNC(vehicles,getCrewmanRequired);
            private _conditionCode = [{ false }, { true }] select _needsCrewman;
            _menuOptions pushBack [
                (MEDIAPATH + "roles\crewman.paa"),
                "Equip Crewman",
                { _this call EFUNC(rolesspawns,roleEquipCrewman) },
                [{ false }, { true }] select _needsCrewman,
                _needsCrewman
            ];


            _menuOptions pushBack [
                "",
                "Advanced",
                { [_this select 1, "advanced"] call FUNC(menuOpen) },
                { true },
                true
            ];
        };
    };

    // -- Cache the options, so we can access them faster
    GVAR(interactNamespace) setVariable [(_vehicleType + "_menuOptions"), _menuOptions];
};

private _options = +_menuOptions;
private _customCategory = _vehicle getVariable [QSVAR(interactMenuCategory), ""];
if (_customCategory isEqualTo "") then {
    if (_isStaticWeapon) then {
        if (isNull (_vehicle getVariable ["spawnpoint", objNull])) then {
            _customCategory = "StaticWeapon";
        } else {
            _customCategory = "FortificationWeapon";
        };
    } else {
        if (_hasSeats) then {
            _customCategory = "Vehicle";
        };
    };
};

if (_customCategory != "") then {
    private _suboptions = +(GVAR(interactNamespace) getVariable [_customCategory + "_category", []]);
    _options append _suboptions;
};

// -- Get only the options belonging to this menu level
_options = _options select { _menuLevel == (_x param [5, ""]) };


// -- Add number hotkeys to the first 10 options
for "_i" from 0 to (9 min (count _options - 1)) do {
    private _number = [_i + 1, 0] select (_i == 9);
    private _text = ((_options select _i) select 1);
    if (_text isEqualType {}) then { _text = call _text };
    (_options select _i) set [1, format ["[%1] %2", _number, _text]];
};

_options;
