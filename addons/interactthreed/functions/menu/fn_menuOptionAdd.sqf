/*
    Function:       FRL_InteractThreeD_fnc_menuOptionAdd
    Author:         Adanteh
    Description:    Adds an option for given isKindOf vehicle type
    Example:        ["StaticWeapon", ['', 'rearm', "hint 'hi'", "true"] call FRL_InteractThreeD_fnc_menuOptionAdd
*/
#include "macros.hpp"

params ["_category", "_option"];
_option params [
    ["_picture", "", [""]],
    ["_text", "Default Action", ["", {}]],
    ["_actionCode", { true }, [{}, ""]],
    ["_conditionCode", { true }, [{}, ""]],
    ["_enabled", true, [true, {}]],
    ["_menuLevel", ""]
];

_actionCode = _actionCode call MFUNC(parseToCode);
_conditionCode = _conditionCode call MFUNC(parseToCode);

if (isNil QGVAR(interactNamespace)) then {
    GVAR(interactNamespace) = false call CFUNC(createNamespace);
};

private _currentActions = GVAR(interactNamespace) getVariable [_category + "_category", []];
_currentActions pushBack [_picture, _text, _actionCode, _conditionCode, _enabled, _menuLevel];
GVAR(interactNamespace) setVariable [_category + "_category", _currentActions];
