/*
    Function:       FRL_InteractThreeD_fnc_menuUseActionKey
    Author:         Adanteh
    Description:    Uses menu action from number key (F is first option)
*/
#include "macros.hpp"

params ["_mode", "_args"];
_args params ["", "_dikButton"];

//[[_fnc_scriptNameShort, _this], "red"] call MFUNC(debugMessage);

// -- KEYDOWN
private _return = false;
if (_mode == "down") then {
    if (_dikButton == 1) exitWith { // -- 1 is the Esc key
        //[] call FUNC(menuClose);
        _return = false;
    };

    // -- Don't retrigger, till we let go of key first
    private _numberKey = _dikButton - 1; // -- DIK_1 is button 2, so remove 1 to get actual number key
    if (_numberKey <= 10) exitWith {
        if (GVAR(menuUpReturn)) exitWith { _return = true; };

        private _actionSelected = (GVAR(menuOptionsActive) param [_numberKey - 1, []]);
        if (_actionSelected isEqualTo []) exitWith { false; };

        _actionSelected params ["_action", "_condition", ["_enabled", true]];
        if !(_enabled) exitWith { };

        private _condition = [Clib_Player, GVAR(menuTarget), { GVAR(menuTarget) }] call _condition;
        if !(_condition) exitWith { _return = true; };
        private _succes = [Clib_Player, GVAR(menuTarget), { GVAR(menuTarget) }] call _action;

        GVAR(menuUpReturn) = true;
        _return = true;
    };
} else {
    // -- KEYUP
    // -- If it's set to return true (Key is handled, then do a one time true return ignoring other keybinds)
    if (GVAR(menuUpReturn) && (_dikButton <= 11)) then {
        GVAR(menuUpReturn) = false;
        _return = true;
    } else {
        _return = false;
    };
};

_return
