/*
    Function:       FRL_InteractThreeD_fnc_drawIconReal
    Author:         Adanteh
    Description:    Fairly basic wsrapped for DrawIcon3D. Only used if there's an action within the viewport
*/
#include "macros.hpp"

params ["_icon", "_doorPosReal", "_distance", "_active", "_iconText"];
private _size = (4.5 / (_distance max 1.4));
private _color = if (_active) then {
	[1, 1, 1, 1];
} else {
	[0.8, 0.8, 0.8, (1.5 / (_distance max 1)) - 0.2];
};

drawIcon3D [GVAR(icon), _color, _doorPosReal, _size, _size, 0, _iconText, 0, (0.0325 * _size), FONTBOLD, "center", false];
