#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Main"};

            class addInteractObject;
            class clientInit;
            class combineCode;
            class detectNearby;
            class detectVehicle;
            class door;
            class drawIconReal;
            class findDoors;
            class findLadders;
            class removeInteractObject;

            class menu {
                class clientInitMenu;
                class menuChangeAction;
                class menuClose;
                class menuOpen;
                class menuOptionAdd;
                class menuOptions;
                class menuUseActionKey;
            };

            class condition {
                class canEnterSeat;
            };

            class common {
                class pushVehicle;
                class getInAnyTurret;
                class getInSeat;
                class getInAvailable;
            };
        };
    };
};
