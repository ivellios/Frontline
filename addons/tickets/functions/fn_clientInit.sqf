/*
    Function:       FRL_Tickets_fnc_init
    Author:         Adanteh
    Description:    Inits the ticket system
*/
#include "macros.hpp"
#define CTRL(var1,var2) ((_dialog displayCtrl var1) controlsGroupCtrl var2)

private _tracks = [];
{
    if (getNumber (_x >> "duration") > 30) then {
        if !(getArray (_x >> "sound") isEqualTo []) then {
            _tracks pushBack configName _x;
        };
    };
} forEach ("true" configClasses (configFile >> "CfgMusic"));
GVAR(availableTracks) = _tracks;


["fadeHud", {
    (_this select 0) params ["_fade", "_commit"];
    if (isNull (uiNamespace getVariable [QSVAR(Tickets), displayNull])) exitWith { };
    ((uiNamespace getVariable QSVAR(Tickets)) displayCtrl 1000) ctrlSetFade _fade;
    ((uiNamespace getVariable QSVAR(Tickets)) displayCtrl 1000) ctrlCommit _commit;
}] call CFUNC(addEventHandler);

["playEndMusic", {
    playMusic (selectRandom (GVAR(availableTracks)));
    addMusicEventHandler ["MusicStop", {
        playMusic (selectRandom (GVAR(availableTracks)));
    }];
}] call CFUNC(addEventHandler);

[{

    QSVAR(Tickets) cutRsc [QSVAR(Tickets),"PLAIN", 0, false];
    private _dialog = uiNamespace getVariable QSVAR(Tickets);
    {
        private _idBase = (_forEachIndex + 1) * 10;
        CTRL(_idBase,2011) ctrlSetText ([_x, "flag"] call MFUNC(getSideData));
        CTRL(_idBase,2013) ctrlSetText str 0;
        CTRL(_idBase,2015) ctrlSetTextColor ([_x, "color"] call MFUNC(getSideData));
        CTRL(_idBase,2015) progressSetPosition 0;
    } forEach (call MFUNC(getSides));

    ["ticketsChanged", {

        private _dialog = uiNamespace getVariable QSVAR(Tickets);

        {
            private _idBase = (_forEachIndex + 1) * 10;
            CTRL(_idBase,2013) ctrlSetText str floor _x;
            CTRL(_idBase,2015) progressSetPosition (_x / GVAR(ticketsToWin));
        } forEach GVAR(tickets);

        if (GVAR(deactivateTicketSystem)) exitWith { };

        private _higestTickets = selectMax GVAR(tickets);
        if (isNil QGVAR(musicPlay) && {_higestTickets >= (GVAR(ticketsToWin) - GVAR(musicStartTickets))}) then {
            "playEndMusic" call CFUNC(localEvent);
            GVAR(musicPlay) = true;
        };
    }] call CFUNC(addEventHandler);

},{ !isNil QGVAR(tickets) }] call CFUNC(waitUntil);

['add', ["admin", 'Admin Settings', 'Misc', [
    'Bleed Modifier',
    'Modifier for the bleed',
    QMVAR(CtrlTextBoxButton),
    { GVAR(bleedModifer) },
    { [Clib_Player] call MFUNC(isAdmin); },
    {
        if (parseNumber _value != 0) then {
            GVAR(bleedModifer) = parseNumber _value;
            publicVariable QGVAR(bleedModifer);
        };
        false;
    }, "SET"
]]] call MFUNC(settingsWindow);
