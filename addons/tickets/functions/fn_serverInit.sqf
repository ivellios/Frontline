/*
    Function:       FRL_Tickets_fnc_serverInit
    Author:         Adanteh
    Description:    Inits the ticket system
*/
#include "macros.hpp"


["adjustTicket", {
    (_this select 0) call FUNC(adjustTicket);
}] call CFUNC(addEventHandler);


[{
    private _tickets = (call MFUNC(getSides)) apply { GVAR(startTickets) };
    GVAR(tickets) = _tickets;
    publicVariable QGVAR(tickets);

    if (GVAR(playerTicketValue) != 0) then {

        ["KilledPlayer", { (_this select 0) call FUNC(handlePlayerDeath); }] call CFUNC(addEventHandler);

        // -- Handle ticket loss on disconnect when incapped
        addMissionEventHandler ["HandleDisconnect", {
            params ["_killedUnit"];
            private _isIncapped = [_killedUnit] call MFUNC(isUnconscious);
            if (_isIncapped) then {
                [_killedUnit] call FUNC(handlePlayerDeath);
            };
        }];

    };

    // -- Event for whenever a team has 0 tickets left -- //
    ["ticketsChanged", {
        if (selectMax GVAR(tickets) >= GVAR(ticketsToWin)) then {
            if (GVAR(deactivateTicketSystem)) exitWith { };
            GVAR(deactivateTicketSystem) = true;
            publicVariable QGVAR(deactivateTicketSystem);
            ["victoryPointsReached"] call CFUNC(globalEvent);
        };
    }] call CFUNC(addEventHandler);

}, {!(call MFUNC(getSides) isEqualTo [])}] call CFUNC(waitUntil);
