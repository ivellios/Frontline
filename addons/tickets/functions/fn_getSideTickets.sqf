/*
    Function:       FRL_Tickets_fnc_getSideTickets
    Author:         Adanteh
    Description:    Gets tickets for given side
*/
#include "macros.hpp"

params ["_side"];
private _sideIndex = [_side] call MFUNC(getSideIndex);
GVAR(tickets) param [_sideIndex, 0];
