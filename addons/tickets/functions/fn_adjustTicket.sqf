/*
    Function:       FRL_Tickets_fnc_adjustTicket
    Author:         Adanteh
    Description:    A Function That Does stuff
    Ecample:        [50, west] call FRL_Tickets_fnc_adjustTicket
*/
#include "macros.hpp"

params ["_amount", "_side", ["_modify", false]];
private _sideIndex = [_side] call MFUNC(getSideIndex);

if (_modify) then { // -- Things like bleed we wanna adjust by a playernymber modifier
    _amount = _amount * GVAR(bleedModifer) * (0.015 * (count allPlayers) + 1);
};
private _tickets = ((GVAR(tickets) param [_sideIndex, 0]) + _amount) max 0;
GVAR(tickets) set [_sideIndex, _tickets];
publicVariable QGVAR(tickets);

"ticketsChanged" call CFUNC(globalEvent);
