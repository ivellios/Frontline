/*
    Function:       FRL_Tickets_fnc_handlePlayerDeath
    Author:         Adanteh
    Description:    Handles unit death
*/
#include "macros.hpp"

params ["_unit"];

private _adjustTicket = ["handlePlayerTicket", [_unit], true] call MFUNC(checkConditions);
if (_adjustTicket) then {
    [GVAR(playerTicketValue) * -1, side group _unit] call FUNC(adjustTicket);
};
