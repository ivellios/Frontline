/*
    Function:       FRL_Tickets_fnc_init
    Author:         Adanteh
    Description:    Inits the ticket system
*/
#include "macros.hpp"

[QGVAR(Settings), configFile >> "FRL" >> "cfgTickets"] call MFUNC(cfgSettingLoad);
[QGVAR(Settings), missionConfigFile >> "FRL" >> "cfgTickets"] call MFUNC(cfgSettingLoad);

GVAR(startTickets) = 0;
GVAR(deactivateTicketSystem) = false;
GVAR(playerTicketValue) = [QGVAR(Settings_playerValue), 1] call MFUNC(cfgSetting);
GVAR(musicStartTickets) = [QGVAR(Settings_musicStart), 10] call MFUNC(cfgSetting);
GVAR(bleedModifer) = [QGVAR(Settings_bleedModifier), 1] call MFUNC(cfgSetting);
if (isNil QGVAR(ticketsToWin)) then { // -- In case it's changed by the server for JIP
    GVAR(ticketsToWin) = [QGVAR(Settings_tickets), 100] call MFUNC(cfgSetting);
};
