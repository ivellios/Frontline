#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Main"};

            class adjustTicket;
            class clientInit;
            class getSideTickets;
            class handlePlayerDeath;
            class init;
            class serverInit;
        };
    };
};
