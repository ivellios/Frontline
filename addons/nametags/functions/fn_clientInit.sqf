/*
    Function:       FRL_Nametags_fnc_clientInit
    Author:         Adanteh
    Description:    Adds settings, vars and keybind for earplugs
*/
#define __INCLUDE_DIK
#include "macros.hpp"


[QGVAR(Settings), configFile >> "FRL" >> "cfgNametags"] call MFUNC(cfgSettingLoad);
[QGVAR(Settings), missionConfigFile >> "FRL" >> "cfgNametags"] call MFUNC(cfgSettingLoad);
GVAR(baseRange) = [QGVAR(Settings_baseRange), 7] call MFUNC(cfgSetting);
GVAR(maxRange) = [QGVAR(Settings_maxFocusRange), 100] call MFUNC(cfgSetting);
GVAR(aimMargin) = [QGVAR(Settings_aimMargin), 5] call MFUNC(cfgSetting);

["missionStarted", {
    // The draw3D event triggers on each frame if the client window has focus.
    [{ _this call FUNC(drawTags) }, 2] call MFUNC(addPerFramehandler);
}] call CFUNC(addEventHandler);
