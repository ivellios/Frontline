/*
    Function:       FRL_Nametags_fnc_drawTags
    Author:         Adanteh
    Description:    Main function to draw nametags. Will draw all tags when close, further nametags will draw if looking near them
*/
#include "macros.hpp"
#define __MARGIN 1.8

if (!alive Clib_Player || !isNull (findDisplay 49) || dialog) exitWith {};

// Cycle through all nearby players and display their nameTag.
private _icons = [];
private _playerGroup = group Clib_Player;
private _nearby = [QGVAR(nearby), {
    ([positionCameraToWorld [0, 0, 0], GVAR(maxRange)] call MFUNC(getNearUnits)) select { (!(_x isEqualTo Clib_Player) && alive _x && playerSide getFriend (side group _x) >= 0.6) };
}, [], 3] call CFUNC(cachedCall);

{

    // The position of the nameTag is above the head.
    private _color = [[1, 1, 1, 1], [0.333,0.8,0.333, 1]] select (group _x == _playerGroup);
    private _text = _x call CFUNC(name);
    private _icon = _x getVariable [QSVAR(rKitIcon), ""];

    _icons pushBack ["ICON", _icon, _color, [_x, "Head"], 1, 1, 0, _text, 2, 0.05, "RobotoCondensed", "center", false, {
        private _cameraPosASL = AGLToASL (_cameraPosition);
        private _facePositionAGL = ASLtoAGL eyePos (_position select 0);
        private _facePositionASL = AGLToASL _facePositionAGL;

        // -- Don't show behind objects

        if (!((lineIntersectsSurfaces [_cameraPosASL, _facePositionASL, CLib_Player, (_position select 0)]) isEqualTo [])) exitWith {false};


        // -- If within nametag range, show it always, otherwise only show nametag if we're looking at a unit directly
        private _show = false;
        private _distance = _cameraPosASL distance _facePositionASL;
        private "_alpha";
        if (_distance < GVAR(baseRange)) then {
            _alpha = 1;
            _show = true;
        } else {
            // -- If wit
            private _screenPos = worldToScreen _facePositionAGL;
            if (_screenPos isEqualTo []) exitWith { };
            private _screenDistance = (_screenPos distance2D [0.5, 0.5]);
            private _margin = (GVAR(aimMargin) * _fov / (_distance / 4));
            if (_screenDistance <= _margin) then {
                _show = true;
                _alpha = 1.2 - (1.2 / _margin * _screenDistance);
                //[[_fnc_scriptNameShort, _margin, _screenDistance, _alpha], "lime", 0.1] call MFUNC(debugMessage);
            };
        };

        if !(_show) exitWith {
            false
        };

        _size = 1.25 * (_fov min 1.25 / 2);
        _position = (_position select 0) modelToWorldVisual ((_position select 0) selectionPosition (_position select 1)) vectorAdd [0, 0, 0.7 * ((6 + _distance) / 15 / (_fov * 1.5))];
        _width = _size * _width;
        _height = _size * _height;
        _textSize = _size * _textSize;
        _color set [3, _alpha];
        //_position set [2, _offset];

        true
    }];

    nil
} count _nearby;

[QGVAR(Icons),_icons] call CFUNC(add3dGraphics);

//[[_fnc_scriptNameShort], "lime"] call MFUNC(debugMessage);
