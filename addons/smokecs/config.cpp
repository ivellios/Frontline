class CfgPatches {
	class Blastcore_SmokeCS {
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]={"FRL_Main"};
	};
};

class CfgAmmo {
	class GrenadeHand;
	class FlareCore;
	class SmokeShell: GrenadeHand
	{
		SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1",0.225893,1,170};//SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1",0.125893,1,70};
		SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2",0.225893,1,170};//SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2",0.125893,1,70};
		grenadeBurningSound[] = {"SmokeShellSoundLoop1",0.5,"SmokeShellSoundLoop2",0.5};
		timeToLive = 90;
	};
	class G_40mm_Smoke: SmokeShell
	{
		simulation = "shotSmoke";
	};
	class FlareBase: FlareCore
	{
		timeToLive = 125;
	};
};

class cfgWeapons {
    class Bulletbase;
	class SmokeLauncherAmmo: BulletBase
	{
		initTime = 0;
		timeToLive = 0.001;
		hit = 0.01;
		indirectHit = 0;
		indirectHitRange = 0;
		explosive = 1;
		muzzleEffect = "";
		explosionEffects="SmokeCS";
		simulation="shotRocket";
	};
};

class CfgCloudlets
{
	class Default
	{
		interval="0.5 * interval + 0.5";
		circleRadius=0;
		circleVelocity[]={0,0,0};
		particleShape="\A3\Data_f\cl_basic.p3d";
		particleFSNtieth=1;
		particleFSIndex=0;
		particleFSFrameCount=1;
		particleFSLoop=1;
		angle=0;
		angleVar=0;
		animationName="";
		particleType="Billboard";
		timerPeriod=1;
		lifeTime=1;
		moveVelocity[]={0,0,0};
		rotationVelocity=0;
		weight=1;
		volume=1;
		rubbing=0.050000001;
		size[]={1,1};
		color[]=
		{
			{1,1,1,1}
		};
		animationSpeed[]={1};
		randomDirectionPeriod=0;
		randomDirectionIntensity=0;
		onTimerScript="";
		beforeDestroyScript="";
		position[]={0,0,0};
		lifeTimeVar=0;
		positionVar[]={0,0,0};
		positionVarConst[]={0,0,0};
		MoveVelocityVar[]={0,0,0};
		MoveVelocityVarConst[]={0,0,0};
		rotationVelocityVar=0;
		sizeVar=0;
		colorVar[]={0,0,0,0};
		randomDirectionPeriodVar=0;
		randomDirectionIntensityVar=0;
	};

	class Blastcore_CS1: Default
	{
		interval=0.001;
		circleRadius=0;
		circleVelocity[]={0,0,0};
		particleShape="warfxpe\ParticleEffects\Universal\Explosion_09";
		particleFSNtieth=1;
		particleFSIndex=0;
		particleFSFrameCount=1;
		particleFSLoop=0;
		angleVar=360;
		animationName="";
		particleType="Billboard";
		timerPeriod=1;
		lifeTime=0.07;
		moveVelocity[]={0,0,0};
		rotationVelocity=0;
		weight=10.1;
		volume=7.9000001;
		rubbing=0.0099999998;
		size[]={1.5};
		color[]=
		{
			{1,1,1,-1},
			{1,1,1,0}
		};
		animationSpeed[]={2};
		randomDirectionPeriod=0;
		randomDirectionIntensity=0;
		onTimerScript="";
		beforeDestroyScript="";
		lifeTimeVar=0;
		positionVar[]={25,0,25};
		MoveVelocityVar[]={0,0,0};
		rotationVelocityVar=0;
		sizeVar=0;
		colorVar[]={0,0,0,0};
		randomDirectionPeriodVar=0;
		randomDirectionIntensityVar=0;
		blockAIVisibility=1;
        destroyOnWaterSurface = 1;
		emissiveColor[]=
		{
			{1000,1000,1000,1}
		};
	}
	//fix BTR smokes plz
	class RHS_3D17TrailEffect : Default {
        circleRadius = 0;
        circleVelocity[] = {0, 0, 0};
        particleShape = "\A3\data_f\ParticleEffects\Universal\Universal";
        particleFSNtieth = 16;
        particleFSIndex = 12;
        particleFSFrameCount = 8;
        particleFSLoop = 1;
        angleVar = 1;
        animationName = "";
        particleType = "Billboard";
        timerPeriod = 1;
        moveVelocity[] = {0, 0, 0};
        sizeCoef = 1;
        colorCoef[] = {1, 1, 1, 1};
        animationSpeed[] = {1};
        animationSpeedCoef = 1;
        onTimerScript = "";
        beforeDestroyScript = "";
        MoveVelocityVar[] = {0, 0, 0};
        interval = 0.3;
        emissiveColor[] = {{0.5, 0.5, 0.5, 0}, {0, 0, 0, 0}};
        colorVar[] = {0, 0, 0, 0};
        lifeTime = 28.5;
        lifeTimeVar = 0.25;
        weight = 1.277;
        volume = "0.99 + randomValue / 40";
        rubbing = 0.0075;
        size[] = {2.8125, 9.625, 12.875};
        sizeVar = 0.5;
        color[] = {{0.6, 0.6, 0.6, 0.7}, {0.5, 0.5, 0.5, 0.8}, {0.5, 0.5, 0.5, 0.9}, {0.5, 0.5, 0.5, 0.7}, {0.5, 0.5, 0.5, 0.5}, {0.8, 0.8, 0.8, 0.09}, {1, 1, 1, 0}};
        rotationVelocity = 0.75;
        rotationVelocityVar = 0.25;
        randomDirectionPeriod = 1.25;
        randomDirectionIntensity = 0.25;
        randomDirectionIntensityVar = 0.1;
        position[] = {0, 0, 0};
        positionVar[] = {0.1, 0.1, 0.1};
        blockAIVisibility = 0;
    };
    class RHS_3D17TrailEffect1 : RHS_3D17TrailEffect {
        color[] = {{0.6, 0.6, 0.6, 0.7}, {0.5, 0.5, 0.5, 0.8}, {0.5, 0.5, 0.5, 0.9}, {0.5, 0.5, 0.5, 0.7}, {0.5, 0.5, 0.5, 0.5}, {0.8, 0.8, 0.8, 0.09}, {1, 1, 1, 0}};
        size[] = {2.875, 9.75, 12.25};
        interval = 0.15;
    };
    class RHS_3D17TrailEffect2 : RHS_3D17TrailEffect {
        color[] = {{0.6, 0.6, 0.6, 0.7}, {0.5, 0.5, 0.5, 0.8}, {0.5, 0.5, 0.5, 0.9}, {0.5, 0.5, 0.5, 0.7}, {0.5, 0.5, 0.5, 0.5}, {0.8, 0.8, 0.8, 0.09}, {1, 1, 1, 0}};
        size[] = {2.25, 5.5, 7.5};
        interval = 0.075;
    };
    class RHS_3D17TrailEffectLong : RHS_3D17TrailEffect {
        particleShape = "\A3\data_f\cl_basic.p3d";
        particleFSNtieth = 1;
        particleFSIndex = 0;
        particleFSFrameCount = 1;
        particleFSLoop = 0;
        animationName = "";
        particleType = "Billboard";
        timerPeriod = 1;
        animationSpeed[] = {0};
        circleRadius = 0;
        circleVelocity[] = {0, 0, 0};
        interval = 0.1;
        color[] = {{0.8, 0.8, 0.8, 0.2}, {0.6, 0.6, 0.6, 1}, {0.6, 0.6, 0.6, 1}, {0.6, 0.6, 0.6, 0.9}, {0.6, 0.6, 0.6, 0.8}, {0.6, 0.6, 0.6, 0.6}};
        emissiveColor[] = {{0.5, 0.5, 0.5, 0}, {0, 0, 0, 0}};
        lifeTime = 88;
        lifeTimeVar = 1;
        size[] = {5.5, 12.8, 13.879.65, 17.625};//{4.5, 7.875, 14.625};
        sizeVar = 0.5;
        weight = 1.273;
        volume = "0.985 + randomValue / 60";//0.99 + randomValue / 70
		rubbing = 0.00000001 ;
        rotationVelocity = 1.25;
        rotationVelocityVar = 1;
        randomDirectionPeriod = 1.5;
        randomDirectionIntensity = 0.15;
        bounceOnSurface = 0.3;
        bounceOnSurfaceVar = 0;
        blockAIVisibility = 0;
    };
    class RHSUSF_CM_L8A3_Smoke_Tail_Base : Default {
		rubbing = 0.00000001 ;
    };
    class RHSUSF_CM_L8A3_Smoke_Tail_Big_0 : RHSUSF_CM_L8A3_Smoke_Tail_Base {
        particleShape = "warfxpe\ParticleEffects\Universal\smoke_02";
        particleFSNtieth = 1;
        particleFSIndex = 0;
        particleFSFrameCount = 1;
        particleFSLoop = 0;
        animationName = "";
        particleType = "Billboard";
        timerPeriod = 1;
        animationSpeed[] = {0};
        circleRadius = 0;
        circleVelocity[] = {0, 0, 0};
        lifeTime = 98;
        lifeTimeVar = 1;
        weight = 1.273;
        volume = "0.985 + randomValue / 60";
        rubbing = 0.00000001 ;
        rotationVelocity = 1.5;
        rotationVelocityVar = 1.5;
        randomDirectionPeriod = 0.4;
        randomDirectionIntensity = 0.1;
        bounceOnSurface = 0.3;
        bounceOnSurfaceVar = 0;
        blockAIVisibility = 0;
        color[] = {{0.8, 0.8, 0.8, 0.2}, {0.6, 0.6, 0.6, 1}, {0.6, 0.6, 0.6, 1}, {0.6, 0.6, 0.6, 1}, {0.6, 0.6, 0.6, 0.9}, {0.6, 0.6, 0.6, 0.8}, {0.6, 0.6, 0.6, 0.6}};
        size[] = {6.5, 18.875, 19.625, 20, 21, 22.5, 23, 23.1};
        interval = 0.38;
    };
    class RHSUSF_CM_L8A3_Smoke_Tail_Big_1 : RHSUSF_CM_L8A3_Smoke_Tail_Big_0 {
        color[] = {{0.6, 0.6, 0.6, 0.7}, {0.5, 0.5, 0.5, 0.8}, {0.5, 0.5, 0.5, 0.9}, {0.5, 0.5, 0.5, 0.7}, {0.5, 0.5, 0.5, 0.5}, {0.8, 0.8, 0.8, 0.09}, {1, 1, 1, 0}};
        size[] = {3, 5.25, 9.75};
        interval = 0.2;
    };
    class RHSUSF_CM_L8A3_Smoke_Tail_Big_2 : RHSUSF_CM_L8A3_Smoke_Tail_Big_0 {
        color[] = {{0.6, 0.6, 0.6, 0.7}, {0.5, 0.5, 0.5, 0.8}, {0.5, 0.5, 0.5, 0.9}, {0.5, 0.5, 0.5, 0.7}, {0.5, 0.5, 0.5, 0.5}, {0.8, 0.8, 0.8, 0.09}, {1, 1, 1, 0}};
        size[] = {3, 5.25, 9.75};
        interval = 0.1;
    };
	class SmokeVehicleCS: Default
	{
		interval=0.00079999998;
		circleRadius=0;
		circleVelocity[]={0,0,0};
		particleShape="warfxpe\ParticleEffects\Universal\smoke_02";
		particleFSNtieth=1;
		particleFSIndex=0;
		particleFSFrameCount=1;
		particleFSLoop=1;
		angleVar=360;
		animationName="";
		particleType="Billboard";
		timerPeriod=1;
		lifeTime=20;
		moveVelocity[]={0,0,0};
		rotationVelocity=0;
		weight=10.1;
		volume=7.9000001;
		rubbing=1;
		size[]={12,20};
		color[]=
		{
			{1,1,1,1},
			{1,1,1,0}
		};
		animationSpeed[]={0.2};
		randomDirectionPeriod=0.02;
		randomDirectionIntensity=0.02;
		onTimerScript="";
		beforeDestroyScript="";
		lifeTimeVar=0;
		positionVar[]={30,0.050000001,30};
		MoveVelocityVar[]={1,1,1};
		rotationVelocityVar=10;
		sizeVar=0;
		colorVar[]={0,0,0,0};
		randomDirectionPeriodVar=0;
		randomDirectionIntensityVar=0;
		blockAIVisibility=1;
        destroyOnWaterSurface = 1;
	};
	class SmokeShellWhiteSmall: Default
	{
		animationSpeedCoef=1;
		colorCoef[]=
		{
			"colorR",
			"colorG",
			"colorB",
			1.8
		};
		sizeCoef=1;
		position[]={0,0,0};
		interval=0.2;
		circleRadius=0;
		circleVelocity[]={0,0,0};
		particleShape="warfxpe\ParticleEffects\Universal\smoke_02";
		particleFSNtieth=1;
		particleFSIndex=0;
		particleFSFrameCount=1;
		particleFSLoop=0;
		angleVar=360;
		animationName="";
		particleType="Billboard";
		timerPeriod=1;
		lifeTime=26;
		moveVelocity[]={0,0.30000001,0};
		rotationVelocity=1;
		weight=10.05;
		volume=7.9000001;
		rubbing=0.00000001;
		size[]={0.2,6,16};
		color[]=
		{
			{0.6,0.6,0.6,0.2},
			{0.6,0.6,0.6,0.25},
			{0.6,0.6,0.6,0.25},
			{0.6,0.6,0.6,0.22},
			{0.6,0.6,0.6,0.18},
			{0.6,0.6,0.6,0.1},
			{0.6,0.6,0.6,0.0}
		};
		animationSpeed[]={1000};
		randomDirectionPeriod=0.30000001;
		randomDirectionIntensity=0.15000001;
		onTimerScript="";
		beforeDestroyScript="";
		lifeTimeVar=2;
		positionVar[]={0,0,0};
		MoveVelocityVar[]={1.2,0.34999999,1.2};
		rotationVelocityVar=25;
		sizeVar=0.5;
		colorVar[]={0,0,0,0.050000001};
		randomDirectionPeriodVar=0;
		randomDirectionIntensityVar=0;
		blockAIVisibility=1;
	};
	class SmokeShellWhite: Default
	{
		animationSpeedCoef=1;
		colorCoef[]=
		{
			"colorR",
			"colorG",
			"colorB",
			"colorA"
		};
		sizeCoef=1;
		position[]={0,0,0};
		interval=0.035;
		circleRadius=0;
		circleVelocity[]={0,0,0};
		particleShape="warfxpe\ParticleEffects\Universal\smoke_01";
		particleFSNtieth=1;
		particleFSIndex=0;
		particleFSFrameCount=1;
		particleFSLoop=1;
		angleVar=360;
		animationName="";
		particleType="Billboard";
		timerPeriod=1;
		lifeTime=24;//14
		moveVelocity[]={0,0,0};
		rotationVelocity=0;//0
		weight=10.097;
		volume=7.9000001;
		rubbing=0.005;//
		size[]={0.01,3.5,6};
		color[]= { { 0.1, 0.1, 0.1, 0.8 }, { 0.25, 0.25, 0.25, 0.5 }, { 0.5, 0.5, 0.5, 0.1 }, { 0.9, 1, 0.83, 0 } };
		/*{
			{1,1,1,1},
			{1,1,1,0}
		};*/
		animationSpeed[]={1};
		randomDirectionPeriod=0.4;
		randomDirectionIntensity=0.1;
		onTimerScript="";
		beforeDestroyScript="";
		lifeTimeVar=0.5;
		positionVar[]={0,0,0};
		MoveVelocityVar[]={0.1,0.05,0.1};
		rotationVelocityVar=20;//20
		sizeVar=0.5;
		colorVar[]={0,0,0,0.34999999};
		randomDirectionPeriodVar=0;
		randomDirectionIntensityVar=0;
		blockAIVisibility=1;
	};
	class SmokeShellWhiteLong: Default
	{
		animationSpeedCoef=1;
		colorCoef[]=
		{
			"colorR",
			"colorG",
			"colorB",
			"colorA"
		};
		sizeCoef=1;
		position[]={0,0,0};
		interval=0.02;//0.02
		circleRadius=0;
		circleVelocity[]={0,0,0};
		particleShape="warfxpe\ParticleEffects\Universal\smoke_02";
		particleFSNtieth=1;
		particleFSIndex=0;
		particleFSFrameCount=1;
		particleFSLoop=1;
		angleVar=360;
		animationName="";
		particleType="Billboard";
		timerPeriod=1;
		lifeTime=30;
		moveVelocity[]={0,0.25,0};
		rotationVelocity=0;//0
		weight=10.08;
		volume=7.9;
		rubbing=0.3;
		size[]={2,5};
		color[]= { { 0.1, 0.1, 0.1, 0.8 }, { 0.25, 0.25, 0.25, 0.5 }, { 0.5, 0.5, 0.5, 0.1 }, { 0.9, 1, 0.83, 0 } };
		/*{
			{1,1,1,0},
			{1,1,1,0.7},
			{1,1,1,0}
		};*/
		animationSpeed[]={1};
		randomDirectionPeriod=0;
		randomDirectionIntensity=0;
		onTimerScript="";
		beforeDestroyScript="";
		lifeTimeVar=0.5;
		positionVar[]={0,0,0};
		MoveVelocityVar[]={1,0.5,1};
		rotationVelocityVar=25;//25
		sizeVar=0.5;
		colorVar[]={0,0,0,0};
		randomDirectionPeriodVar=0;
		randomDirectionIntensityVar=0;
		blockAIVisibility=1;
	};
	class SmokeShellWhite2: SmokeShellWhite
	{
		particleFSNtieth=1;
		particleFSIndex=0;
		particleFSFrameCount=1;
		particleFSLoop=0;
	};
	//helo dust
	class HDust1: Default
    {
        size[] = {6,8};
        color[] = {{1,1,0.8,0.62},{1,1,0.8,0.6},{1,1,0.8,0.45},{1,1,0.8,0.61},{1,1,0.8,0.035},{1,1,0.8,0.051},{1,1,0.8,0}};
        colorCoef[] = {1,1,1,"0.7*((density*1.7) interpolate [0,0.6,0,0.6])"};
    };
    class HDust1M: Default
    {
        size[] = {6,9};
        color[] = {{1,1,0.8,0.62},{1,1,0.8,0.6},{1,1,0.8,0.45},{1,1,0.8,0.61},{1,1,0.8,0.035},{1,1,0.8,0.051},{1,1,0.8,0}};
        colorCoef[] = {1,1,1,"0.65*((density*1.7) interpolate [0,0.6,0,0.6])"};
    };
    class HDust1L: Default
    {
        size[] = {6,10};
        color[] = {{1,1,0.8,0.62},{1,1,0.8,0.6},{1,1,0.8,0.45},{1,1,0.8,0.61},{1,1,0.8,0.035},{1,1,0.8,0.051},{1,1,0.8,0}};
        colorCoef[] = {1,1,1,"0.65*((density*1.7) interpolate [0,0.6,0,0.7])"};
    };
    class HDust1Long: Default
    {
        size[] = {15,15,15,15,15,20,20,20,20,25,25,25,25,25,25,25,25,25,25,25,25,25};
        color[] = {{1,1,0.8,4},{1,1,0.8,4.6},{1,1,0.8,3.5},{1,1,0.8,2.6},{1,1,0.8,1.8},{1,1,0.8,1.1},{1,1,0.8,0.62}};
        colorCoef[] = {1,1,1,"0.1*((density*1.7) interpolate [0,0.6,0,0.7])"};
    };
    class HDust1LongM: Default
    {
        size[] = {15,15,15,15,15,20,20,20,20,25,25,25,25,25,25,25,25,25,25,25,25,25};
        color[] = {{1,1,0.8,4},{1,1,0.8,4.6},{1,1,0.8,3.5},{1,1,0.8,2.6},{1,1,0.8,1.8},{1,1,0.8,1.1},{1,1,0.8,0.62}};
        colorCoef[] = {1,1,1,"0.1*((density*1.7) interpolate [0,0.6,0,0.7])"};
    };
};

class SmokeShellWhite
{
	class SmokeShellWhite
	{
		simulation="particles";
		type="SmokeShellWhite";
		position[]={0,0,0};
		intensity=1;
		interval=1;
	};
	class SmokeShellWhite2
	{
		simulation="particles";
		type="SmokeShellWhiteLong";
		position[]={0,0,0};
		intensity=1;
		interval=1;
	};
};

class SmokeShellWhiteSmall
{
	class SmokeShellWhite
	{
		simulation="particles";
		type="SmokeShellWhiteSmall";
		position[]={0,0,0};
		intensity=1;
		interval=1;
		smokeGenMinDist=0.1;
		smokeGenMaxDist=2000;
		smokeSizeCoef=0;
		smokeIntervalCoef=0;
	};
	class SmokeShellWhite2
	{
		simulation="particles";
		type="SmokeShellWhiteLong";
		position[]={0,0,0};
		intensity=1;
		interval=1;
		smokeGenMinDist=0.1;
		smokeGenMaxDist=2000;
		smokeSizeCoef=0;
		smokeIntervalCoef=0;
	};
};

class SmokeCS
{
	class SmokeVehicle
	{
		simulation="particles";
		type="Blastcore_CS1";
		position[]={0,0,0};
		intensity=1;
		interval=1;
		lifeTime=0.3;
	};
	class SmokeVehicleCS1
	{
		simulation="particles";
		type="SmokeVehicleCS";
		position[]={0,0,0};
		intensity=1;
		interval=1;
		lifeTime=0.3;
	};
};