class Modules {

    class MODULE {
        defaultLoad = 1;
        class clientInitChat;
        class postInitRemoteValue;

        class MapInteract {
            class clientInitMap;
            class registerMapClick;
        };

        class HoldAction {
            class clientInitHoldAction;
            class holdActionEnd;
            class holdActionCallback;
            class holdActionParams;
            class holdActionStart;
        };

        class Library {
            class localInit;
            class codeToString;
            class parseToBool;
            class parseToCode;
            class ppEffectCreate;

            class Arrays {
                class arrayMove;
                class arrayPeek;
                class arrayGetKeys;
                class arrayToMatchParams;
            };

            class Conditions {
                class addCondition;
                class checkConditions;
            };

            class Config {
                class cfgSetting;
                class cfgSettingLoad;
                class cfgSettingLoadSpecial;
                class cfgSettingSet;
                class getConfigValue;
                class initCfgSetting;
            };

            class Debug {
                class clientInitDebug;
                class debugMessage;
                class diagLog;
                class exportZoomCfg;
                class isEditorPreview;
            };

            class perFrame {
                class addPerFrameHandler;
                class initPerFrameHandler;
                class removePerFrameHandler;
            };

            class Dictionary {
                class configToDictionary;
                class dictionaryGet;
            };

            class Events {
                class addEventHandler;
                class localEvent;
            };

            class Localization {
                class localize;
                class localizeMissingLog;
            };

            class Map {
                class getNearestName;
                class posIsInTerrain;
            };

            class Markers {
                class createObjectMarker;
                class markerExists;
            };

            class Math {
                class calcEdenAngles;
                class calcPitchBankYaw;
                class calcPolygonCoords;
                class calcRelativePosition;
                class calcVectorDirAndUp;
                class getRelDir;
                class getTerrainVector;
                class rotateAroundPoint;
                class setPitchBankYaw;

                class Matrix {
                    class matrixMultiply;
                    class matrixTranspose;
                    class RPY2DCM;
                    class DCM2RPY;
                    class vectorDirAndUp2DCM;
                    class DCM2vectorDirAndUp;
                    class getDCM;

                    class pbyToVector;
                    class vectorToPby;
                };
            };

            class Namespace {
                class createNamespace;
                class deleteNamespace;
                class forEachVariable;
                class namespaceName;
                class resetNamespace;
            };

            class Objects {
                class getGroupPosition;
                class getObjectsInArea;
                class getObjectScale;
                class isTerrainObject;
            };

            class Strings {
                class replaceInString;
                class removeSpaces;
                class customMsg;
            };

            class UI {
                class colorFromName;
                class colorHexToRGBA;
                class forEachTreeview;
                class getCtrlChildren;
                class getCtrlData;
                class getCtrlParentTop;
                class getCtrlPositionReal;
                class setCtrlPosInScreen;
                class uiGetCfgSize;
                class uiPosInPos;
            };

            class Units {
                class getDeathAnim;
                class disableMouse;
                class getNearUnits;
            };
        };

        class loading {
            class startLoadingScreen;
            class endLoadingScreen;
        };

        class Keybinds {
            class addKeybind;
            class addKeybindSetting;
            class changeKeybind;
            class clientInitKeybinds;
            class getKeybindText;
            class getKeyName;
            class keybindCheck;
            class keyPressed;
            class modifierPressed;
        };

        class Settings {
            class clientInitSettings;
            class settingsNumberEditScroll;
            class settingsAddControl;
            class settingsWindow;
            class settingsCategoryExists;

            class Weather {
                class initWeather;
                class weatherCommit;
                class weatherPreview;
                class weatherSetDate;
                class weatherSetFog;
                class weatherSetOvercast;
                class weatherSetRain;
                class weatherSetWind;
                class weatherStopRain;
            };
        };

        class Support {
            class airdropObject;
        };

        class Notifications {
            class clientInitNotifications;
            class mapTooltip;
            class notificationShow;
            class notificationHide;
            class notificationParse;
            class tooltipParse;
            class tooltipShow;
        };

        class UI {
            class popupDialog;
        };
    };

    class FrlCollection {
        dependencies[] = {
            "Main",
            "RolesSpawns",
            "Earplugs",
            "Bluehud",
            "Radio",
            "Medical",
            "Suppress",
            "Spotting",
            "Voting",
            "Weather",
            "InteractThreeD",
            "Missionselection",
            "Statics",
            "Maintenance",
            "Scoring",
            "Recoil",
            "WindowBreak",
            "Fortifications",
            "Compat",
            "AI",
            "Commander",
            "Prep"
        };
    };
    class FrlCollectionNoRadio {
        dependencies[] = {
            "Main",
            "RolesSpawns",
            "Earplugs",
            "Bluehud",
            "Medical",
            "Suppress",
            "Spotting",
            "Voting",
            "Weather",
            "InteractThreeD",
            "Missionselection",
            "Statics",
            "Maintenance",
            "Scoring",
            "Recoil",
            "WindowBreak",
            "Fortifications",
            "Compat",
            "AI",
            "Commander",
            "Prep"
        };
    };
};
