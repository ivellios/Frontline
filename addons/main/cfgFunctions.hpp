class Generic {
    file = "\pr\frl\addons\main\fnc";
    class clearProfileCode {
        preStart = 1;
        preInit = 1;
    };

    class EndMission;
};

class GUI {
    file = "\pr\frl\addons\main\fnc";
    class showNotification;
    class blockingMessage;
    class blurBackground;
    class blackWhiteBackground;
    class blockingDialog;
};

class htmlLoad {
    file = "\pr\frl\addons\main\fnc";
    class LoadWebInfo;
};

class Version {
    file = "\pr\frl\addons\main\fnc";
    class versionCheck {
	    preInit = 1;
	    postInit = 1;
	    preStart = 1;
	    header = -1;
    };

};

// -- Add more functions through modular system, instead of here.
#define RECOMPILE recompile = 1;
#ifndef RECOMPILE
    #define RECOMPILE
#endif

class Compiling {
    file = "\pr\frl\addons\main\compile";
    class compile { RECOMPILE };
    class moduleImport { RECOMPILE };
    class moduleLoad {
        RECOMPILE
        prestart = 1;
        preinit = 1;
    };
    class moduleQueue { RECOMPILE };
    class moduleQueueExecute { RECOMPILE };
    class recompileModuleBox;
};
