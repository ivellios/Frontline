/*
    Function:       FRL_Main_fnc_airdropObject
    Author:         Adanteh
    Description:    Aidrop an object at given position or from a given vehicle

    Virtual airdrop example:
        ["ReammoBox_F", AGLtoASL (player modelToWorld [0, 76, 150]), playerSide] spawn FRL_Main_fnc_airdropObject;
*/
#include "macros.hpp"

if !(canSuspend) exitWith { _this call FUNC(airdropObject) };

params [["_object", objNull, [objNull, ""]], ["_source", objNull, [[], objNull]], ["_side", sideUnknown]];

private ["_objectType", "_objectNew"];
if (_object isEqualType objNull) then {
    _objectType = typeOf _object;
    _objectNew = false;
};
if (_object isEqualType "") then {
    _objectType = _object;
    _object = objNull;
    _objectNew = true;
};

/*---------------------------------------------------------------------------
	Compatibility for non-animated objects (? Whatever that even means)
	if animated is false, the object cannot be repositioned mid-air with parachute, instead it will be on the ground
	Some items don't work well being attached under parachute, so for those special cases we use a replacement item
---------------------------------------------------------------------------*/

private _animated = ((getNumber (configFile >> "cfgVehicles" >> _objectType >> "animated")) != 0);

if (isNull _object) then {
    if !(isClass (configFile >> "cfgVehicles" >> _objectType)) then {
        [[_fnc_scriptNameShort, "MISSING CLASS", _objectType], "red", 15, -1] call FUNC(debugMessage);
        _objectType = "ReammoBox_F";
    };
	_object = createVehicle [_objectType, [0, 0, 0], [], 0, "NONE"];
};

/*---------------------------------------------------------------------------
	Create multiple parachutes for larger objects
	ICE_function\vehicle\fn_objectTypeExtended. Case sensitive checks!
---------------------------------------------------------------------------*/
private _objectTypeExtended = ([_objectType] call EFUNC(vehicles,getVehicleType)) apply { toLower _x };
_objectTypeExtended params ["_objectCategory", "_objectTypeExtended"];

private _extraParaArray = [];
private _parachuteClass = "B_parachute_02_F";
private _extendTime = 0.5;
if (true) then {
	if (_objectTypeExtended in [
            "tank",
            "wheeledapc",
            "trackedapc",
            "truck",
            "armored_car",
            "wheeledifv",
            "ifv",
            "apc",
            "tank_amphibious"
    ]) exitWith {
		_parachuteClass = "B_parachute_02_F";
		_extendTime = 3;
		_extraParaArray = [[0.5,0.4,0.6],[-0.5,0.4,0.6],[0.5,-0.4,0.6],[-0.5,-0.4,0.6]];
	};

	if (_objectTypeExtended in ["mrap", "armedmrap", "ship", "car", "armed_car", "lcm", "lcvp"]) exitWith {
		_parachuteClass = "B_parachute_02_F";
		_extendTime = 2;
		_extraParaArray = [[0.5,0.4,0.6],[-0.5,0.4,0.6]];
	};

	if (_objectTypeExtended in ["motorcycle", "boat", "largeammobox", "cargobox"]) exitWith {
		_extendTime = 1.5;
		_parachuteClass = "B_parachute_02_F";
	};
};



_objectPara = _object;
/*---------------------------------------------------------------------------
	Check if item is animated: Animated items automatically drop. Non animated items hover in mid-air.
---------------------------------------------------------------------------*/
if (!_animated) then {
	if (_objectType isKindOf "thingX" && (_objectTypeExtended != "LargeAmmoBox")) exitWith { _animated = true };
};

/*---------------------------------------------------------------------------
	Position is given: Start airdrop from given position and height.
---------------------------------------------------------------------------*/
_objectDir = 0;
_objectPos = [0, 0, 0];
_objectVel = [0, 0, -2];
_dropHeight = 0;
if (_source isEqualType []) then {
	_objectPos = _source;
	_objectDir = (random 360);
	_objectVel = [(random 5) / 10, (random 5) / 10, (-1*(random 4))];
	_dropHeight = _objectPos select 2;
};

/*---------------------------------------------------------------------------
	Object is given: Start airdrop from given vehicle (And make airdrop with vehicle's vectorspeed)
---------------------------------------------------------------------------*/
if (_source isEqualType objNull) then {
	_bboxSource = boundingBox _source;
	_offsetRear = ((_bboxSource select 0 select 1) min (_bboxSource select 1 select 1)) - 3; // negative value
	/*---------------------------------------------------------------------------
		modelToWorld doesn't give height above buildings, so using sin / cos instead
		We're using getPos to retrieve height above terrain / sea / buildings, for damage calcutions,
		however all positions are set with setPosASL instead.
	---------------------------------------------------------------------------*/
	_objectDir = getDir _source;
	_objectPos = getPosASL _source;
	_objectPos set [0, (_objectPos select 0) + (sin(_objectDir) * _offsetRear)];
	_objectPos set [1, (_objectPos select 1) + (cos(_objectDir) * _offsetRear)];
	_objectPos set [2, (((_objectPos select 2) - 2) max 0)];
	_dropHeight = ((((getPos _source) select 2) - 2) max 0);
	_objectVel = velocity _source;
	// Invert from vehicle direction -- //
	_objectVel = [
		(_objectVel select 0) * 0.25,
		(_objectVel select 1) * 0.25,
		(_objectVel select 2) - 4];
};


/*---------------------------------------------------------------------------
	Check height. If it's under a certain height, we want to apply damage to the object
	If it's at very low height, we don't want to attach a parachute but let the object fall isntead
---------------------------------------------------------------------------*/
_noParachute = false;
_addedDamage = 0;

_dropHeightTenth = _dropHeight / 10;
_addedDamage = (0 max (-0.3*(_dropHeightTenth-3)^2/(_dropHeightTenth+2)+1.25)) min 1;
if (true) then {
	if (_dropHeight < 30) exitWith {
		_noParachute = true;
		//_addedDamage = 1;
	};
/*
	if (_dropHeight < 70) exitWith {
		_addedDamage = 0.75;

	};

	if (_dropHeight < 80) exitWith {
		_addedDamage = 0.50;
	};

	if (_dropHeight < 100) exitWith {
		_addedDamage = 0.25;
	};

	if (_dropHeight < 120) exitWith {
		_addeDamage = 0.12;
	};*/
};

_parachutes = [];
if (true) then {
	/*---------------------------------------------------------------------------
		Animated items will drop by themself
	---------------------------------------------------------------------------*/
	_objectDir = _objectDir - 180;
	_objectPara setDir _objectDir;
	_objectPara setPosASL _objectPos;
	_objectPara setVelocity _objectVel;

	if (_noParachute && _animated) exitWith { };

	if (_noParachute) exitWith {
		private _fall = 0.005;
		private _time = time + 20;
		private _height = (getPosASL _objectPara) select 2;
		while {((_height > 2) && (time > _time))} do	{
			_fall = (_fall * 1.01);
			_height = (_height - _fall) max 0;
			_pos = getPosASL _objectPara;
			_pos set [2, _height];
			_objectPara setPosASL _pos;
			sleep 0.05;
		};
	};

	/*---------------------------------------------------------------------------
		If animated item, let it drop a while before we create an open prachute
	---------------------------------------------------------------------------*/
	if (_animated) then {
		sleep 1;
		_objectPos = getPosASL _objectPara;
		_objectDir = getDir _objectPara;
		_objectVel = velocity _objectPara;
	} else {
		/*---------------------------------------------------------------------------
			Lower item for a bit
		---------------------------------------------------------------------------*/
		_height = (getPosASL _objectPara) select 2;
		_fall   = 0.05;
		_time = time + 1;
		while {_time > time} do	{
			_fall = (_fall * 1.01);
			_height = (_height - _fall) max 0;
			_pos = getPosASL _objectPara;
			_pos set [2, _height];
			_objectPara setPosASL _pos;
			sleep 0.01;
		};
		_objectPos = getPosASL _objectPara;
	};

	/*---------------------------------------------------------------------------
		Create the parachutes
	---------------------------------------------------------------------------*/
	_parachute = createVehicle [_parachuteClass, [0,0,0], [], 0, "NONE"];
	_parachutes = [_parachute];
	{
		_p = createVehicle [_parachuteClass, [0,0,0], [], 0, "NONE"];
		_parachutes set [count _parachutes, _p];
		_p attachTo [_parachute, [0,0,0]];
		_p setVectorUp _x;
	} count _extraParaArray;
	_parachute setDir _objectDir;
	_parachute setPosASL _objectPos;
	_parachute setVelocity _objectVel;
	_objectPara attachTo [_parachute, [0,0,-(abs ((boundingbox _objectPara select 1) select 2) + 0.25)]];
	_time = time + 0.25;

    [[_fnc_scriptNameShort, _object, _objectTypeExtended, _parachute, _objectPos], "lime"] call FUNC(debugMessage);

	while { time > _time } do {
		_parachute setVelocity _objectVel;
		sleep 0.05;
	};
	/*---------------------------------------------------------------------------
		If we ejected a virtual item, make sure we run the init for it.
		If the object is getting destroyed instantly because of incorrect height, don't waste resources on this
	---------------------------------------------------------------------------*/
	if (_addedDamage < 1) then {
		if (_objectNew) then {
            ["vehicleInit", [_object, _side, str _object]] call CFUNC(globalEvent);
		};
	};
};

/*---------------------------------------------------------------------------
	Detach object when it's close to the ground and delete the parachutes
---------------------------------------------------------------------------*/
0 = [_object, _objectPara, _parachutes, _animated, _addedDamage] spawn {
    params ["_object", "_objectPara", "_parachutes", "_animated", "_addedDamage"];
	sleep 0.25;

	/*---------------------------------------------------------------------------
		getPos doesn't play nice with ammo boxes, because it'll run right above the ammo box as 0
	---------------------------------------------------------------------------*/
	waitUntil {
		sleep 0.25;
		_height = ((getPos _objectPara) select 2);
		(_height < 4);
		//((getPos _objectPara) select 2) < 4) || ({(if (count _parachutes > 0) then { _posPara = getPos (_parachutes select 0) select 2 < 4; } else { false })});
		/*
		_pos = getPosATL _objectPara;
		if (surfaceIsWater _pos) then {
			_pos = getPosASL _objectPara;
		};*/
	};

	//sleep 0.25;

	if (_addedDamage > 0) then {
		_object setDamage (damage _object + _addedDamage);
	};

	/*---------------------------------------------------------------------------
		Make sure the object drops on the terrain properly
	---------------------------------------------------------------------------*/
	_objectVel = velocity _objectPara;
	detach _object;
	_object setVelocity _objectVel;
	_objectPos = getPosASL _objectPara;
	_smokePos =+ _objectPos;

	/*---------------------------------------------------------------------------
		If these items aren't the same, it means we were using a placeholder object while dropping
	---------------------------------------------------------------------------*/
	if (_object != _objectPara) then {
		deleteVehicle _objectPara;
	};

/*	if (!_animated) then {
		_objectPos set [2, 0];
	};*/
	if (!_animated) then {
		/*---------------------------------------------------------------------------
			Use combined height to make sure object gets to the the proper level (Above buildings, terrain and sea)
		---------------------------------------------------------------------------*/
		_objectHeight = (getPos _object) select 2;
		_objectPos set [2, (_objectPos select 2) - _objectHeight];
		_object setPosASL _objectPos;


	};

	/*---------------------------------------------------------------------------
		Only create smoke + chemlight if parachutes were used
	---------------------------------------------------------------------------*/
	if (count _parachutes > 0) then {
		_smoke = createVehicle ['SmokeShellGreen', _smokePos, [], 0, "NONE"];
		_smoke setPosASL _smokePos;

		/*---------------------------------------------------------------------------
			Only create chemlight at night
		---------------------------------------------------------------------------*/
		if (sunOrMoon < 0.3) then {
			_chem = createVehicle ['Chemlight_green', _smokePos, [], 0, "NONE"];
			_chem setPosASL _smokePos;
		};

		{
			detach _x;
			_x disableCollisionWith _object;
		} count _parachutes;
		_time = time + 2.5;
		waitUntil {time > _time};
		playSound3D [
			"a3\sounds_f\weapons\Flare_Gun\flaregun_1_shoot.wss",
			_object
		];
		{
			if (!isNull _x) then { deleteVehicle _x; };
		} count _parachutes;
	};


};
