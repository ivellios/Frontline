/*
    Function:       FRL_Main_fnc_postInitRemoteValue
    Author:         Adanteh
    Description:    Retrieves a value from a remote to this client only. Used to get data that's only present in server
*/
#include "macros.hpp"

GVAR(remoteValueNamespace) = call CFUNC(createNamespace);
GVAR(clientID) = clientOwner;
if (isMultiplayer) then {
    (format [QGVAR(getValue_%1), GVAR(clientID)]) addPublicVariableEventHandler {
        [QGVAR(receiveRemoteValue), (_this select 1)] call CFUNC(localEvent);
    };

    /*
        Description: Call code on the server and send the return value back to the client
        Example: ["requestRemoteValue", ['test', { serverTime }, { hint str _this; }]] call Clib_fnc_localEvent;
    */
    ["requestRemoteValue", {
        (_this select 0) params ["_tag", "_codeValue", ["_codeReturn", {}], ["_target", 2]];
        if (_codeReturn isEqualTo {}) then {
            _codeReturn = compile format ["%1 = _this;", _tag];
        };
        GVAR(remoteValueNamespace) setVariable [(_tag + "_code"), _codeReturn];

        [QGVAR(getRemoteValue), _target, [GVAR(clientID), _tag, _codeValue]] call CFUNC(targetEvent);
    }] call CFUNC(addEventHandler);


    // -- Receive the request for value, execute the code and send it back to the client
    if (isServer) then {
        [QGVAR(getRemoteValue), {
            (_this select 0) params ["_targetID", "_tag", "_codeValue"];
            private _value = call _codeValue;
            private _varName = format [QGVAR(getValue_%1), _targetID];
            missionNamespace setVariable [_varName, [_tag, _value]];
            _targetID publicVariableClient _varName;
        }] call CFUNC(addEventHandler);
    };


    // -- Receive the value from the server and execute the return code
    [QGVAR(receiveRemoteValue), {
        (_this select 0) params ["_tag", "_value"];
        GVAR(remoteValueNamespace) setVariable [(_tag + "_value"), _value];
        _value call (GVAR(remoteValueNamespace) getVariable [(_tag + "_code"), {}]);
    }] call CFUNC(addEventHandler);

} else {

    ["requestRemoteValue", {
        (_this select 0) params ["_tag", "_codeValue", ["_codeReturn", {}]];
        if (_codeReturn isEqualTo {}) then {
            _codeReturn = compile format ["%1 = _this;", _tag];
        };
        (call _codeValue) call _codeReturn;
    }] call CFUNC(addEventHandler);
};
