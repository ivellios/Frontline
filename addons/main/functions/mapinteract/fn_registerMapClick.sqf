/*
    Function:       FRL_Main_fnc_registerMapClick
    Author:         Adanteh
    Description:    Adds mapclick data to a map control
*/
#include "macros.hpp"

params ["_mapCtrl"];

private _registered = _mapCtrl getVariable [QGVAR(registeredMapClick), false];
if (_registered) exitWith { };


_mapCtrl ctrlAddEventHandler ["mouseButtonDown", {
    params ["_ctrl", "_button", "_posX", "_posY"];
    private _buttonString = ["RMB", "LMB"] select (_button == 0);
    uiNamespace setVariable [(_buttonString + "_pos"), [_posx,_posy]];
    uiNamespace setVariable [(_buttonString + "_hold"), true];
    uiNamespace setVariable [(_buttonString + "_time"), diag_tickTime];
}];

_mapCtrl ctrlAddEventHandler ["mouseButtonUp", {
    params ["_mapCtrl", "_button", "_posX", "_posY"];
    if (_button == 1) then {
        // -- Make sure mouse wasn't hold for quite a long time
        if ((diag_tickTime - (uiNamespace getVariable ["RMB_time", 0])) < 0.3) then {
            // -- Make sure mouse hasn't moved too much (dragging map)
            if (([_posX, _posY] distance (uiNamespace getVariable ["RMB_pos", [0,0]])) < 0.05) then {
                ["map.RightClick.short", [_mapCtrl, _posX, _posY]] call CFUNC(localEvent);
            };
        };
    };
    if (_button == 0) then {
        if ((diag_tickTime - (uiNamespace getVariable ["LMB_time", 0])) < 0.3) then {
            if (([_posX, _posY] distance (uiNamespace getVariable ["LMB_pos", [0,0]])) < 0.05) then {
                ["map.LeftClick.short", [_mapCtrl, _posX, _posY]] call CFUNC(localEvent);
            };
        };
    };
    private _buttonString = ["RMB", "LMB"] select (_button == 0);
    uiNamespace setVariable [(_buttonString + "_hold"), false];
}];
