/*
    Function:       FRL_Main_fnc_clientInitMap
    Author:         Adanteh
    Description:    Map interaction basics
*/
#include "macros.hpp"

[{
	uinamespace setVariable ["_display",finddisplay 12];
	uinamespace setVariable ["_map", (findDisplay 12) displayCtrl 51];

	[(uinamespace getVariable "_map")] call FUNC(registerMapClick);
}, {!(isNull ((findDisplay 12) displayCtrl 51))}] call CFUNC(waitUntil);

// -- This needs a rework of all the m ap interact menus. Can't properly open dialog on top of other dialogs :D:D:D:D:D:D: REEEEEEEEEEE
if (false) then {
    ["registerMapClick", {
        (_this select 0) params ["_control"];
        [_control] call FUNC(registerMapClick);
    }] call CFUNC(addEventHandler);

};
