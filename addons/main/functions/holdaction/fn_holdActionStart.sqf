/*
    Function:       FRL_Main_fnc_actionHoldStart
    Author:         Adanteh
    Description:    Starts a progress action
*/
#include "macros.hpp"

params call FUNC(holdActionParams);

// -- Check start code, if not a true return exit it
private _return = _args call ([_codeStart] call FUNC(parseToCode));
if !(_return) exitWith { };

private _handle = missionNamespace getVariable [QGVAR(haEH), -1];
if (_handle != -1) exitWith { };
    //[_handle] call FUNC(removePerFrameHandler);
    // -- Exit existing actions??
//};


// -- Show the UI (Or hide if it it's shown and we don't want that)
if (_showProgress) then {
    if (isNull DISP) then {
        QSVAR(CtrlHoldActionDisplay) cutRsc [QSVAR(CtrlHoldActionDisplay), "PLAIN", 0, false];
    };
    CTRL(2) ctrlSetText _actionIcon;
} else {
    if !(isNull DISP) then {
        QSVAR(CtrlHoldActionDisplay) cutText ["", "PLAIN"];
    };
};

// -- Figure out if this is a resumed action, if so, lower the time required
private _savedProgress = 0;
{
    _x params ["_actionNameSaved", "_argsSaved", "_progressLeft"];
    if (_actionName isEqualTo _actionNameSaved) then {
        if (_args isEqualTo _argsSaved) then {
            _savedProgress = _progressLeft;
            breakTo (_fnc_scriptName + "_main");
        };
    };
} forEach GVAR(haProgressStack);


// -- Start the progress, callback checking and updating of progress
GVAR(haStartTime) = time - _savedProgress;
GVAR(haData) = [call FUNC(holdActionParams), _this] call FUNC(arrayToMatchParams);
GVAR(haEH) = [{
    _this call FUNC(holdActionCallback);
}, 0] call FUNC(addPerFrameHandler);

// -- If a dik key is given as input, we want to exit the action if this key is released
if (_dikButton != -1) then {
    if (GVAR(haKeyUpEH) == -1) then {
        GVAR(haKeyUpEH) = (findDisplay 46) displayAddEventHandler ["KeyUp", { _this call FUNC(holdActionEnd) }];
    };
};
