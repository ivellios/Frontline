/*
    Function:       FRL_Main_fnc_actionHoldCallback
    Author:         Adanteh
    Description:    While-holding action thingie
*/
#include "macros.hpp"

params ["", "_handle"];

GVAR(haData) params (call FUNC(holdActionParams));

private _exit = false;
private _progressPercentage = (time - GVAR(haStartTime)) / _duration;

call {
    // -- Exit var, usually set by button up. Call the end code with last var being 1 to indicate exitVar exit
    if (GVAR(haExit)) exitWith {
        [_args, 1] call _codeEnd;
        _exit = true;
    };

    // -- Check the callback. If return isn't true, then exit with code 2
    if !(_args call _codeCallback) exitWith {
        [_args, 2] call _codeEnd;
        _exit = true;
    };

    // -- Check the progress, if progress is over 1, exit with a completed action
    if (_progressPercentage >= 1) exitWith {
        [_args, 0] call _codeEnd;
        _exit = true;
    };

    // -- Update the progress bar
    if (_showProgress) then {
        private _progressPath = format ["\A3\Ui_f\data\IGUI\Cfg\HoldActions\progress\progress_%1_ca.paa", floor (_progressPercentage * 24)];
        CTRL(1) ctrlSetText _progressPath;
    };
};

// -- Exit, reset loop variable, remove the loop and close the UI
if (_exit) then {
    GVAR(haEH) = -1;
    GVAR(haExit) = false;
    [_handle] call FUNC(removePerFrameHandler);
    QSVAR(CtrlHoldActionDisplay) cutText ["", "Plain"];

    if (GVAR(haKeyUpEH) != -1) then {
        (findDisplay 46) displayRemoveEventHandler ["KeyUp", GVAR(haKeyUpEH)];
        GVAR(haKeyUpEH) = -1;
    };

    // -- Add to the loop taking care of progress lowering
    if (_saveProgress) then {
        if (_progressPercentage < 1) then {
            {
                if (_actionName == (_x select 0)) then {
                    if (_args isEqualTo (_x select 1)) then {
                        GVAR(haProgressStack) set [_forEachIndex, [_actionName, _args, _progressPercentage * _duration, time + _gracePeriod]];
                        breakTo (_fnc_scriptName + "_Main");
                    };
                };
            } forEach GVAR(haProgressStack);
            GVAR(haProgressStack) pushBack [_actionName, _args, _progressPercentage * _duration, time + _gracePeriod];
        } else {
            // -- Remove saved progress if it's completed
            {
                if (_actionName == (_x select 0)) then {
                    if (_args isEqualTo (_x select 1)) then {
                        GVAR(haProgressStack) set [_forEachIndex, [_actionName, _args, 0, 0]];
                        breakTo (_fnc_scriptName + "_Main");
                    };
                };
            } forEach GVAR(haProgressStack);
        };
    };
};
