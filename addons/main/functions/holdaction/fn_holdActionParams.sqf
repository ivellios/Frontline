/*
    Function:       FRL_Main_fnc_holdActionParams
    Author:         Adanteh
    Description:    Params array for hold action
*/
#include "macros.hpp"

[
    ["_actionName", ""], // some ID referring to action (Used to share progress)
    ["_actionIcon", ""], // Path of icon to show in middle of bar
    ["_duration", 2.5], // How long the action should take to complete
    ["_args", []], // Arguments to call code with (And need to be same to resume progress)
    ["_codeStart", {}, [{}]], // Code to call at start of action
    ["_codeCallback", {}, [{}]], // Code to call each frame
    ["_codeEnd", {}, [{}]], // Code to end with (Called with [_args, EXIT], where EXIT is 0 for success, 1 for key exit and 2 for callback exit)
    ["_showProgress", true], // Show the progress bar/circle
    ["_saveProgress", false], // Saves progress if you do an early exit
    ["_gracePeriod", 1], // Period to skip lowering of progress (If you put it on 2, you can interupt an action for 2 seconds before you start losing progress)
    ["_dikButton", -1] // -- If a dik key is given, we will end the action if this key is released
];
