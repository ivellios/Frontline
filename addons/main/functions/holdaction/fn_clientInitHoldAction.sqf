/*
    Function:       FRL_Main_fnc_clientInitHoldAction
    Author:         Adanteh
    Description:    Prepares var for hold actions
*/
#include "macros.hpp"

GVAR(haIndex) = 0;
GVAR(haData) = [];
GVAR(haProgressStack) = [];
GVAR(haStartTime) = 0;
GVAR(haExit) = false;
GVAR(haKeyUpEH) = -1;


// -- This takes care of lowering progress over time, some actions we want to save progress
[{
    {
        _x params ["_actionNameSaved", "_argsSaved", "_progressLeft", "_gracePeriod"];
        if (time > _gracePeriod) then {

            _x set [2, _progressLeft - 0.1];
            if (_x select 2 < 0) then { // -- If 0 progress left, remove this entry
                GVAR(haProgressStack) deleteAt (GVAR(haProgressStack) find _x);
            };
        };
    } forEach GVAR(haProgressStack);
}, 0.1] call FUNC(addPerFrameHandler);
