/*
    Function:       FRL_Main_fnc_holdActionEnd
    Author:         Adanteh
    Description:    Ends a progress action
*/
#include "macros.hpp"

params ["", "_dikButton"];

private _dikToCheck = GVAR(haData) param [10, -1];
if (_dikToCheck == _dikButton) then {
    GVAR(haExit) = true;
};

nil
