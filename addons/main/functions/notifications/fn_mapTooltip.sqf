/*
    Function:       FRL_Main_fnc_mapTooltip
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

if (!(isNil QGVAR(mapTooltipCD)) && {GVAR(mapTooltipCD) > time} ) exitWith {};
GVAR(mapTooltipCD) = time + 300;

private _txt = "";
private _icon = "";
private _color = "";

switch (true) do {
    case (leader CLib_Player isEqualTo CLib_Player): {
        if !(currentWaypoint (group CLib_Player) == 0) then {
            _icon = "\pr\frl\addons\main\icons\alert\warning.paa";
            _color = "white";
            _txt = "<br/> Set a Squad movemarker to show your men where to go... or shoot<br/>click Ctrl LMB to set a marker";
        };
    };

    default {
        _icon = "\pr\frl\addons\main\icons\alert\warning.paa";
        _color = "white";
        _txt = "<br/>If you need to forward enemy positions or make requests press RMB on the map!";
    };
};

if (_txt != "") then {
    [_txt, _icon, _color] call MFUNC(tooltipShow);
};
