/*
    Function:       FRL_Main_fnc_notificationParse
    Author:         Adanteh
    Description:    Shows a basic notification on top of the screen
*/
#include "macros.hpp"

params ["_message", ["_image", ""], ["_color", "red"]];

if !(_message isEqualType "") then {
    _message = str _message;
};

// -- Retrieve a picture.
private _defaultColor = "";
if !(_image isEqualTo "") then {
    _image = [_image] call {
        params ["_image"];

        // -- If array is given, get a picture from our material selection.
        if (_image isEqualType []) exitWith {
            format [ICON(%1,%2), _image select 0, _image select 1];
        };

        // -- If it's a class use that, else use a shorthand for a picture
        private _notifcationCfg = configFile >> QUOTE(PREFIX) >> "Notifications" >> _image;
        if (isText (_notifcationCfg >> "path")) exitWith {
            getText (_notifcationCfg >> "path");
        };

        // -- If it's a link to a .paa picture use that
        if ((_image select [count _image - 4, 4]) == ".paa") exitWith {
            _image
        };

        // -- If it's neither use one of the preset ones
        switch (toLower _image) do {
            case "warning": { _defaultColor = "orange"; ICON(alert,warning) };
            case "error": { _defaultColor = "red"; ICON(alert,error_outline) };
            case "ok": { _defaultColor = "green"; ICON(action,check_circle) };
            case "file": { _defaultColor = "cyan"; ICON(content,save) };
            case "nope": { _defaultColor = "orange", ICON(content,clear) };
            case "": { "" };
            default { ICON(action,info) };
        };
    };
};

// -- Retrieve a color (This is for the picture, so only bother if there is actually a picture)
_color = [_color] call {
    params ["_color"];
    if (_color isEqualType []) exitWith { _color };

    if (_color isEqualTo "") then { _color = _defaultColor };
    _color = [_color] call FUNC(colorFromName);
    if (_color select [0, 1] == "#") exitWith {
        [_color] call FUNC(colorHexToRGBA);
    };

    [];
};

[_message, _image, _color];
