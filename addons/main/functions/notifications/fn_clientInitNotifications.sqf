/*
    Function:       FRL_Main_fnc_clientInitNotificiations
    Author:         Adanteh
    Description:    Inits notification system, uses to show nice stacked notifications with fancy colors on top of screen
*/
#include "macros.hpp"

GVAR(notificationStack) = [];
GVAR(tooltipStack) = [];

disableSerialization;

["showNotification", {
    (_this select 0) call FUNC(notificationShow);
}] call CFUNC(addEventHandler);

["showTooltip", {
    (_this select 0) call FUNC(tooltipShow);
}] call CFUNC(addEventHandler);

[{
    private _otherNotifications =+ (uiNamespace getVariable [QGVAR(notificationStack), []]);
    {
        _x params ["_ctrl", "_timeout"];
        if (diag_tickTime > _timeout) then {
            _otherNotifications deleteAt (_otherNotifications find _x);
            [_ctrl] call FUNC(notificationHide);
        };
    } forEach _otherNotifications;
    uiNamespace setVariable [QGVAR(notificationStack), _otherNotifications];

    private _otherTooltips =+ (uiNamespace getVariable [QGVAR(tooltipStack), []]);
    {
        _x params ["_ctrl", "_timeout"];
        if (diag_tickTime > _timeout) then {
            _otherTooltips deleteAt (_otherTooltips find _x);
            [_ctrl] call FUNC(notificationHide);
        };
    } forEach _otherTooltips;
    uiNamespace setVariable [QGVAR(tooltipStack), _otherTooltips];

}] call MFUNC(addPerFramehandler);

addMissionEventHandler ["Map", {
    params ["_mapIsOpened", "_mapIsForced"];
    if (_mapIsOpened && !_mapIsForced) then {
        call FUNC(mapTooltip);
    };
}];
