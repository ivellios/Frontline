/*
    Function:       FRL_Main_fnc_tooltipShow
    Author:         Adanteh/N3croo
    Description:    Shows a basic tooltip on side of the screen
    Example:        ["You pressed a button", "ok", "green", 15] call FRL_Main_fnc_tooltipShow
*/
#define __INCLUDE_IDC
#include "macros.hpp"
#define SUBCTRL(x) (_ctrlTooltip controlsGroupCtrl x)

if (missionNamespace getVariable [QGVAR(tooltipDisabled), false]) exitWith { };

disableSerialization;
params ["_message", ["_image", "##"], ["_color", ""], ["_displayTime", __TTLENGTH]];

private _args = [_message, _image, _color] call FUNC(tooltipParse);
_args params ["_message", "_image", "_color"];

// -- This makes sure we cut the Rsc again if our top layer isn't handled as top layer already,
// this is required because we don't have z-layer. Also remove map display (idd = 12), because that one is always on top (Even if it's not open)
// it doesn't actually overlay though, because that would make sense and this is arma
private _topDisplay = [(allDisplays - [findDisplay 12]) select { !(_x getVariable [QSVAR(noOverlay), false]) }] call MFUNC(arrayPeek);

// -- Create notification
private _ctrlTooltip = _topDisplay ctrlCreate [QSVAR(CtrlTooltip), -1, controlNull];

// -- Set the data to the notification
_message = format ["<t valign='middle'>%1</t>", _message];
//[[_fnc_scriptNameShort, _message, _color], "grey", 2] call FUNC(debugMessage);

SUBCTRL(12) ctrlSetStructuredText parseText _message;
SUBCTRL(11) ctrlSetText _image;

/* TODO check clr of that darn text */
if !(_color isEqualTo []) then {
    SUBCTRL(11) ctrlSetTextColor _color;
};
SUBCTRL(11) ctrlCommit 0;

// -- Collapse open the notification
private _ctrlPos = ctrlPosition _ctrlTooltip;
private _notificationClass = configFile >> QSVAR(CtrlTooltip);
private _hSize = [_notificationClass >> "hExtended"] call FUNC(uiGetCfgSize);
private _ySpacing = [_notificationClass >> "ySpacing"] call FUNC(uiGetCfgSize);
private _basePosY = _ctrlPos select 1;
_ctrlPos set [3, _hSize];
_ctrlTooltip ctrlSetPosition _ctrlPos;
_ctrlTooltip ctrlCommit __COMMITTIME;

// -- Move all notifications below it downwards a little
private _otherNotifications =+ (uiNamespace getVariable [QGVAR(tooltipStack), []]);
private _otherCount = count _otherNotifications - 1;
{
    _x params ["_ctrl"];
    private _ctrlPos = ctrlPosition _ctrl;
    _ctrlPos set [1, _basePosY + _hSize * (_otherCount - _forEachIndex + 1) + ((_otherCount - _forEachIndex) * _ySpacing)];
    _ctrlPos set [3, _hSize]; // -- Keep extend animation for multiple notifaciotn opening at same time
    _ctrl ctrlSetPosition _ctrlPos;
    _ctrl ctrlCommit __COMMITTIME;
} forEach _otherNotifications;

_otherNotifications pushBack [_ctrlTooltip, diag_tickTime + _displayTime];
uiNamespace setVariable [QGVAR(tooltipStack), _otherNotifications];
