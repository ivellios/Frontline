/*
    Function:       FRL_Main_fnc_notificationShow
    Author:         Adanteh
    Description:    Shows a basic notification on top of the screen
    Example:        ["You pressed a button", "ok", "green"] call FRL_Main_fnc_notificationShow
*/
#define __INCLUDE_IDC
#include "macros.hpp"
#define SUBCTRL(x) (_ctrlNotification controlsGroupCtrl x)

if (missionNamespace getVariable [QGVAR(notificationDisabled), false]) exitWith { };

disableSerialization;
params ["_message", ["_image", "##"], ["_color", ""]];

private _args = [_message, _image, _color] call FUNC(notificationParse);
_args params ["_message", "_image", "_color"];

// -- This makes sure we cut the Rsc again if our top layer isn't handled as top layer already,
// this is required because we don't have z-layer. Also remove map display (idd = 12), because that one is always on top (Even if it's not open)
// it doesn't actually overlay though, because that would make sense and this is arma
private _topDisplay = [(allDisplays - [findDisplay 12]) select { !(_x getVariable [QSVAR(noOverlay), false]) }] call MFUNC(arrayPeek);

// -- Create notification
private _ctrlNotification = _topDisplay ctrlCreate [QSVAR(CtrlNotification), -1, controlNull];

// -- Set the data to the notification
_message = format ["<t valign='middle'>%1</t>", _message];
//[[_fnc_scriptNameShort, _message, _color], "grey", 2] call FUNC(debugMessage);

SUBCTRL(12) ctrlSetStructuredText parseText _message;
SUBCTRL(11) ctrlSetText _image;
if !(_color isEqualTo []) then {
    SUBCTRL(11) ctrlSetTextColor _color;
};
SUBCTRL(11) ctrlCommit 0;

// -- Collapse open the notification
private _ctrlPos = ctrlPosition _ctrlNotification;
private _notificationClass = configFile >> QSVAR(CtrlNotification);
private _hSize = [_notificationClass >> "hExtended"] call FUNC(uiGetCfgSize);
private _ySpacing = [_notificationClass >> "ySpacing"] call FUNC(uiGetCfgSize);
private _basePosY = _ctrlPos select 1;
_ctrlPos set [3, _hSize];
_ctrlNotification ctrlSetPosition _ctrlPos;
_ctrlNotification ctrlCommit __COMMITTIME;

// -- Move all notifications below it downwards a little
private _otherNotifications =+ (uiNamespace getVariable [QGVAR(notificationStack), []]);
private _otherCount = count _otherNotifications - 1;
{
    _x params ["_ctrl"];
    private _ctrlPos = ctrlPosition _ctrl;
    _ctrlPos set [1, _basePosY + _hSize * (_otherCount - _forEachIndex + 1) + ((_otherCount - _forEachIndex) * _ySpacing)];
    _ctrlPos set [3, _hSize]; // -- Keep extend animation for multiple notifaciotn opening at same time
    _ctrl ctrlSetPosition _ctrlPos;
    _ctrl ctrlCommit __COMMITTIME;
} forEach _otherNotifications;

_otherNotifications pushBack [_ctrlNotification, diag_tickTime + __LENGTH];
uiNamespace setVariable [QGVAR(notificationStack), _otherNotifications];
