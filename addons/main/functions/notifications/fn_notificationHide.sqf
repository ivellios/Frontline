/*
    Function:       FRL_Main_fnc_notificationHide
    Author:         Adanteh
    Description:    Shows a basic notification on top of the screen
*/
#include "macros.hpp"

disableSerialization;

params ["_ctrlNotification"];

// -- Collapse close the notification
private _ctrlPos = ctrlPosition _ctrlNotification;
_ctrlPos set [3, 0];
_ctrlNotification ctrlSetPosition _ctrlPos;
_ctrlNotification ctrlCommit __COMMITTIME;
[_ctrlNotification] spawn {
    disableSerialization;
    uiSleep __COMMITTIME;
    ctrlDelete (_this select 0);
};
