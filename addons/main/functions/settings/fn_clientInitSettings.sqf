/*
    Function:       FRL_Main_fnc_clientInitSettings
    Author:         Adanteh
    Description:    Adds basic settings
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

["showSettings", {
    (_this select 0) params ["_category"];
    ["show", _category] call FUNC(settingsWindow);
}] call CFUNC(addEventHandler);

// -- This allows you to change numbers by scrolling/dragging
// THIS WAS MEANT TO ALLOW YOU CHANGING WITHOUT CLICKING, BUT MOUSEEXIT DOESN'T FUCKING WORK SO IMPOSSIBRU
// I HAD MOUSEPOS CHECK TO FIX THAT, BUT MOUSEENTER EVEN ONLY FUCKING WORKS ONE TIME REEEE FIX THIS SHITJKLDFAJSLK
// (Not salty)

['ctrl.numberEdit.setFocus', {
    (_this select 0) params ["_args"];
    _args params ["_control"];
    private _display = ctrlParent _control;
    _display setVariable [QGVAR(MouseZNumberControl), [_control]];
    _control ctrlSetBackgroundColor ((_control getVariable QGVAR(colors)) select 1);

    private _zChangeEvent = _display getVariable [QGVAR(MouseZNumber), -1];
    if (_zChangeEvent == -1) then {
        _zChangeEvent = _display displayAddEventHandler ["MouseZChanged", {
            _this call FUNC(settingsNumberEditScroll);
        } call MFUNC(codeToString)];
        _display setVariable [QGVAR(MouseZNumber), _zChangeEvent];
    };

    // -- Thanks BIS: OnMouseExit doesn't work for these text boxes, so instead check if mouse position is within the control position every frame
    // -- This workaround works btw, just won't detect mouseIn after this
    /*
    private _mouseExitDetect = _control getVariable [QGVAR(MouseExitDetect), -1];
    if (_mouseExitDetect == -1) then {
        private _realPos = [_control] call CFUNC(getCtrlPositionReal);
        _mouseExitDetect = [{
            (_this select 0) params ["_realPos", "_control"];
            if (isNull _control) exitWith {
                [_this select 1] call MFUNC(removePerFrameHandler);
            };

            if !([getMousePosition, _realPos] call CFUNC(uiPosInPos)) then {
                [_this select 1] call MFUNC(removePerFrameHandler);
                _control setVariable [QGVAR(MouseExitDetect), -1];
                ['ctrl.numberEdit.killFocus', [_control]] call CFUNC(localEvent);
            };
        }, 0, [_realPos, _control]] call MFUNC(addPerFramehandler);
    };*/
}] call CFUNC(addEventHandler);

['ctrl.numberEdit.killFocus', {
    (_this select 0) params ["_args"];
    _args params ["_control"];

    private _display = ctrlParent _control;
    _display setVariable [QGVAR(MouseZNumberControl), nil];
    _control ctrlSetBackgroundColor ((_control getVariable QGVAR(colors)) select 0);
}] call CFUNC(addEventHandler);

// -- Highlight//unhighlight while active
['ctrl.textEdit.setFocus', {
    (_this select 0) params ["_args"];
    _args params ["_control"];
    _control ctrlSetBackgroundColor ((_control getVariable QGVAR(colors)) select 1);
}] call CFUNC(addEventHandler);

// -- Highlight//unhighlight while active
['ctrl.textEdit.killFocus', {
    (_this select 0) params ["_args"];
    _args params ["_control"];
    _control ctrlSetBackgroundColor ((_control getVariable QGVAR(colors)) select 0);
}] call CFUNC(addEventHandler);
