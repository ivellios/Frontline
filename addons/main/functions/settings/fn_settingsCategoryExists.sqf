/*
    Function:       FRL_Main_fnc_settingsCategoryExists
    Author:         Adanteh
    Description:    Checks if given category tag has any settings to it
    Example:        ['frl_main_keybinds'] call FRL_Main_fnc_settingsCategoryExists
*/
#include "macros.hpp"


if (isNil QGVAR(settingsNamespace)) exitWith {
    false;
};

params ["_tag"];
!isNull (GVAR(settingsNamespace) getVariable [_tag, locationNull]);
