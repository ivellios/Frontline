/*
    Function:       FRL_Main_fnc_postInitWeather
    Author:         Adanteh
    Description:    Inits all the weather settings, plus makes the rain stop on high overcast
*/
#include "macros.hpp"

// -- Stops the rain from autostarting on high overcast settings -- //
[{ call FUNC(weatherStopRain) }, 5, []] call MFUNC(addPerFramehandler);

// -- Add all the settings -- //
if (hasInterface) then {
	['add', ['weather', "Weather Settings", 'Weather', [
		'Time',
		'Sets the time of day',
		QMVAR(CtrlSlider),
		{ (date select 3) },
		{ true; },
		(format ["['date', [(_this select 1)]] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 24], [0.05, 1]]
	]]] call FUNC(settingsWindow);

	// -- Volume levels -- //
	['add', ['weather', "Weather Settings", 'Weather', [
		'Overcast',
		'Set the amount of overcast',
		QMVAR(CtrlSlider),
		{ overcast },
		{ true; },
		(format ["['overcast', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call FUNC(settingsWindow);

	['add', ['weather', "Weather Settings", 'Weather', [
		'Rain',
		'Set the amount of rain',
		QMVAR(CtrlSlider),
		{ rain },
		{ true; },
		(format ["['rain', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call FUNC(settingsWindow);

	['add', ["weather", 'Weather Settings', 'Weather', [
		'Fog',
		'Set the amount of fog',
		QMVAR(CtrlSlider),
		{ fog },
		{ true; },
		(format ["['fog', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call FUNC(settingsWindow);

	['add', ['weather', "Weather Settings", 'Weather', [
		'Wind',
		'Set the amount of wind',
		QMVAR(CtrlSlider),
		{ windStr },
		{ true; },
		(format ["['wind', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call FUNC(settingsWindow);

	['add', ['weather', "Weather Settings", 'Weather', [
		'Broadcast',
		'Broadcasts all the changed weather settings',
		QMVAR(CtrlButton),
		true,
		{ true; },
		(format ["[false, []] call %1; false;", QFUNC(weatherCommit)]),
		["BROADCAST"]
	]]] call FUNC(settingsWindow);
};
