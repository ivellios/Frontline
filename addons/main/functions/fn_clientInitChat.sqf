/*
    Function:       FRL_Main_fnc_clientInitChat
    Author:         Adanteh
    Description:    Creates chat events for global chat usage
*/
#include "macros.hpp"

// -- None of these commands work for some reason. Only systemchat does
{
    _x params ["_tag", "_call"];

    [_tag, compile (({
        (_this select 0) params [['_message', '', [[], '']], ['_speaker', player]];
        if (_message isEqualType []) then {
        	// -- Localize all the format params -- //
        	for "_i" from 1 to (count _message - 1) do {
        		if (isLocalized (_message select _i)) then {
        			_message set [_i, localize _x];
        		};
        	};
        	_message = format _message;
        } else {
        	if (isLocalized _message) then {
        		_message = localize _message;
        	};
        };

    } call FUNC(codeToString)) + _call)] call CFUNC(addEventHandler);

} forEach [
    ["hint", "hint _message;"],
    ["hintSilent", "hint _message;"],
    ["systemChat", "systemChat _message;"],
    ["sideChat", "_speaker sideChat _message;"],
    ["globalChat", "_speaker globalChat _message;"],
    ["commandChat", "_speaker commandChat _message;"],
    ["groupChat", "_speaker groupChat _message;"],
    ["vehicleChat", "_speaker vehicleChat _message;"]
];
