/*
	Function:       FRL_Main_fnc_initPerFrameHandler
	Author:         Based on CBA/ACE/Clib
	Description:    Inits perframe handle (Simplified version)
*/
#include "macros.hpp"

GVAR(perFrameHandleArray) = [];
GVAR(PFHhandles) = [];
GVAR(lastTickTime) = diag_tickTime;

addMissionEventHandler ["EachFrame", {
	if (getClientState == "GAME FINISHED") exitWith {
		removeMissionEventHandler ["EachFrame",_thisEventHandler];
	};

	{
		_x params ["_function","_delay","_delta","","_args","_handle"];
		if (diag_tickTime > _delta) then {
			_x set [2,_delta + _delay];
			[_args,_handle] call _function;
		};
		nil;
	} count GVAR(perFrameHandleArray);
}];

addMissionEventHandler ["Loaded", {
	{
		_x set [2, (_x select 2) - GVAR(lastTickTime) + diag_tickTime];
		nil;
	} count GVAR(perFrameHandleArray);
	GVAR(lastTickTime) = diag_tickTime;
}];
