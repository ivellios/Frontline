/*
    Function:       FRL_Main_fnc_removePerFrameHandler
    Author:         Based on CBA/ACE/Clib
    Description:    Removes perFrame handler
*/
#include "macros.hpp"

params [["_handle", -1, [0]]];

if (_handle < 0 || {_handle >= count GVAR(PFHhandles)}) exitWith { };

GVAR(perFrameHandleArray) deleteAt (GVAR(PFHhandles) select _handle);
GVAR(PFHhandles) set [_handle, nil];

{
    _x params ["", "", "", "", "", "_handle"];
    GVAR(PFHhandles) set [_handle, _forEachIndex];
} forEach GVAR(perFrameHandleArray);
