/*
    Function:       FRL_Main_fnc_configToDictionary
    Author:         Adanteh
    Description:    Reads through a given config tree, all its entries and subentries for both config and missionconfig and makes a dictionary out of them
        Keep in mind this is a little slower than pseudo-namespace dictionaries, but in our case having a traditional return syntax is important,
        because we want to pass it directly to python through Pythia usage

    Example:        [["FRL", "CfgDynamicFrontline"], true] call FRL_Main_fnc_configToDictionary;
    Output:         [["key1", 1], ["key2", ["key2a", 2], ["key2b", 3]]]
*/
#include "macros.hpp"

params [["_configTree", [], [[], ""]], ["_dictionary", []], ["_includeMission", true]];

if (_configTree isEqualTo "") then {
    _configTree = _configTree splitString ".";
};

// -- Recurse check for a config and all its children
private _fnc_recurse = {
    params ["_cfg"];

    private _key = toLower (configName _x);

    private _keyIndex = -1;
    { if ((_x select 0) == _key) exitWith { _keyIndex = _forEachIndex } } forEach _subset;
    if (_keyIndex == -1) then { _keyIndex = _subset pushBack [_key, []]; };
    private _hashCurrent = _subset select _keyIndex;

    private _value = call {
    	if (isNumber _cfg) exitWith { getNumber _cfg };
    	if (isText _cfg) exitWith { getText _cfg };
    	if (isArray _cfg) exitWith { getArray _cfg };
        if (isClass _cfg) exitWith {
            {
                private _subset = _hashCurrent select 1;
                private _return = [_x] call _fnc_recurse;
            } forEach (configProperties [_cfg, "true", true]);
            nil
        };
    	nil;
    };

    if !(isNil "_value") then {
        _hashCurrent set [1, _value];
    };
};

{
    private _cfg = _x;
    { _cfg = _cfg >> _x; nil; } count _configTree;
    {
        private _subset = _dictionary;

        // -- If key already exists, use this to add/change values in. If not just use an empty array
        [_x] call _fnc_recurse;
    } forEach (configProperties [_cfg, "true", true]);
} forEach (if (_includeMission) then { [configFile, missionConfigFile] } else { [configFile] });

_dictionary
