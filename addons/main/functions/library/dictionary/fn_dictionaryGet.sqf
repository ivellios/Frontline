/*
    Function:       FRL_Main_fnc_dictionaryGet
    Author:         Adanteh
    Description:    Gets a value from a dictionary. This is slower than 'good' SQF dictionaries, but we want the syntax to be the same as python

    Usage 1:        Simple usage, returns first value
        Example:        [[["fruits", ["apple", 10], ["pear", 10]], ["vegetables", ["brocolli", 2]]], "vegetables"] call FRL_Main_fnc_dictionaryGet;
        Return:         ["brocolli", 2]]

    Usage 2:        Go into found subset and return subvalues out of that
        Example:        [[["fruits", ["apple", 10], ["pear", 10]], ["vegetables", ["brocolli", 2]]], ["vegetables"."brocolli"]] call FRL_Main_fnc_dictionaryGet;
        Return:         2
*/
#include "macros.hpp"

params [["_dictionary", []], ["_key", "", ["", []]], ["_defaultValue", "KeyException"]];

if (_key isEqualType "") then {
    _key = [_key];
};

call {
    scopeName "dictionaryGet";
    private _value = 0;
    while { count _key > 0 } do {
        private _keyCurrent = _key deleteAt 0;
        private _found = false;
        {
            if ((_x select 0) == _keyCurrent) exitWith {
                _value = (_x select 1);
                _found = true;
            };
        } forEach _dictionary;

        if !(_found) exitWith {
            _defaultValue breakOut "dictionaryGet";
        };

        _dictionary = _value;
    };

    _value;
};
