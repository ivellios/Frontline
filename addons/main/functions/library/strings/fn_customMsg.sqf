/*
    Function:       FRL_Main_fnc_custoMsg
    Author:         Adanteh
    Description:    Shows special chat message (Just a wrapper now, still needs to be implemented)
*/
#include "macros.hpp"

params ["_text"];
if (_text isEqualType []) then {
	_text = format _text;
};

systemChat _text;
