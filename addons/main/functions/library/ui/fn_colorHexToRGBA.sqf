/*
    Function:       FRL_Main_fnc_colorHexToRGBA
    Author:         Commy2 in A3 discord
    Description:    Converts hex color code "#FFFF00" to arma RGBA (0 to 1 values)
    Example:        [_color] call FRL_Main_fnc_colorHexToRGBA;
*/
#include "macros.hpp"

params [["_hexString", "#FFFFFF"], ["_opacity", 1]];

_color = [
    _color select [1,2],
    _color select [3,2],
    _color select [5,2]
];

private _color = _color apply {call compile format ["0x%1", _x] / 255};
_color pushBack _opacity;
_color;
