/*
    Function:       FRL_Main_fnc_getCtrlData
    Author:         Adanteh
    Description:    Gets all values, data, elements, variables and so belonging to a UI control
*/
#include "macros.hpp"

params ["_control"];

private _allVariables = allVariables _control;
private _allVariablesSaved = [];
{
    _allVariablesSaved pushBack [_x, _control getVariable _x];
} forEach _allVariables;
