/*
    Function:       FRL_Main_fnc_forEachTreeview
    Author:         Adanteh
    Description:    Does a block of code for each item in treeview
*/
#include "macros.hpp"

params [["_treeView", controlNull], ["_code", {}], ["_pathEntry", []]];

_code = _code call FUNC(parseToCode);
private _return = [];
private _fnc_recursive = {
    params [["_path", []]];
    for "_entryIndex" from 0 to ((_treeView tvCount _path) - 1) do {
        private _pathEntry = _path + [_entryIndex];
        [_pathEntry, _treeView] call _code;
        [_pathEntry] call _fnc_recursive;
    };
};

[_pathEntry, _treeView] call _code;
[_pathEntry] call _fnc_recursive;
_return;
