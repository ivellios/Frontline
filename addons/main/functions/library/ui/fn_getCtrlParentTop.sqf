/*
    Function:       FRL_Main_fnc_getCtrlParentTop
    Author:         Adanteh
    Description:    Gets topmost ctrlParentControlsGroup for given _control (Useful for nested ones)
*/

params ["_control"];
private _controlParent = ctrlParentControlsGroup _control;
while { !isNull (ctrlParentControlsGroup _controlParent) } do {
    _controlParent = (ctrlParentControlsGroup _controlParent);
};

_controlParent
