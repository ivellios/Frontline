/*
    Function:       FRL_Main_fnc_setCtrlPosInScreen
    Author:         Adanteh
    Description:    Sets control position to screen coordinate, but makes sure the element fully fits in.
                    This means that if you give a position with less room to the bottom and right than the control requires, it'll offset it
*/
#include "macros.hpp"

params ["_control", "_position", ["_flip", false]];

private _ctrlPos = ctrlPosition _control;
_ctrlPos set [0, _position select 0];
_ctrlPos set [1, _position select 1];

// -- Prevent the window from going below the screen (Move it up if that's the case) -- //
if (((_ctrlPos select 1) + (_ctrlPos select 3)) > (safeZoneY + safeZoneH)) then {
    if (_flip) then { // -- Flipping instead of aligning to a side, will just flip the element around completely
        _ctrlPos set [1, (_ctrlPos select 1) - (_ctrlPos select 3)];
    } else {
        _ctrlPos set [1, (safeZoneY + safeZoneH) - (_ctrlPos select 3)];
    };
};

// -- Prevent the window from going to the right-side of screen (Move to left if that's the case)  -- //
if (((_ctrlPos select 0) + (_ctrlPos select 2)) > (safeZoneX + safeZoneW)) then {
    if (_flip) then { // -- Flipping instead of aligning to a side, will just flip the element around completely
        _ctrlPos set [0, (_ctrlPos select 0) - (_ctrlPos select 2)];
    } else {
        _ctrlPos set [0, (safeZoneX + safeZoneW) - (_ctrlPos select 2)];
    };
};

_control ctrlSetPosition _ctrlPos;
_control ctrlCommit 0;
_ctrlPos;
