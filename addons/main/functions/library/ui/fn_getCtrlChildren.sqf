/*
    Function:       FRL_Main_fnc_getCtrlChildren
    Author:         Adanteh
    Description:    Gets all children of given control
*/
#include "macros.hpp"

params ["_control"];
private _allControls = allControls (ctrlParent _control);
(_allControls select { (ctrlParentControlsGroup _x) isEqualTo _control });
