/*
    Function:       FRL_Main_fnc_arrayToMatchParams
    Author:         Adanteh
    Description:    Adjusts an array based on param style array, takes values of given array, then only keeps the values pressent in params and will use default for missing ones
    Example:        [[['_size', 0], ['_xPos', 0], ['_yPos', 1]], [3]] call FRL_Main_fnc_arrayToMatchParams;
    Return:         [3, 0, 1]
*/
#include "macros.hpp"

params ["_keys", "_array"];

private _return = [];
_array params _keys;
{
    _x params ["_key", "_valueDefault", "_acceptedTypes"];
    private _value = (call compile _key);
    if (isNil "_value") then { _value = _valueDefault };
    if (isNil "_acceptedTypes") then {
        if !(_value isEqualTypeAny _acceptedTypes) then {
            if (_acceptedTypes isEqualTo [{}]) then { // -- Parse to code if that's our expected return
                _value = [_value] call FUNC(parseToCode);
            } else {
                _value = _valueDefault;
            };
        };
    };
    _return pushBack _value;
} forEach _keys;

_return;
