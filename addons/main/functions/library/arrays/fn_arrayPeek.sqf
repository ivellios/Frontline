/*
    Function:       FRL_Main_fnc_arrayPeek
    Author:         Adanteh
    Description:    Selects last element in an array
*/

params ["_array"];
(_array select ((count _array) - 1));
