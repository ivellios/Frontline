/*
    Function:       FRL_Main_fnc_arrayGetKeys
    Author:         Adanteh
    Description:    This will go through an array and return the first value of each element. Used to get only the variable names from params for example (In lowercase)
*/
#include "macros.hpp"

params [["_array", []]];
(_array apply { toLower (_x param [0, ""]); });
