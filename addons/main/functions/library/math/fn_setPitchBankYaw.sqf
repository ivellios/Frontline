/*
    Function:       FRL_Main_fnc_setPitchBankYaw
    Author:         Adanteh
    Description:    Sets pitch bank yaw on an object (Actuall vectorDirAndUp). I can't into vectors
    Returns:        New pitch,bank,yaw (Return and set aren't exactly the same)
    Eample:         [cursorObject, [100, 0, 100]] call FRL_Main_fnc_setPitchBankYaw
*/
#include "macros.hpp"

params ["_object", "_pitchBankYaw"];

private _vectorDirAndUp = [_pitchBankYaw] call FUNC(pbyToVector);
_object setVectorDirAndUp _vectorDirAndUp;
[_vectorDirAndUp] call FUNC(vectorToPby);
