/*
    Function:       ADA_Main_fnc_pbyToVector
    Author:         Adanteh
    Description:    Converts pitch,bank,yaw rotation into vectorDirAndUp
*/
#include "macros.hpp"

params ["_pitchBankYaw"];
private _dcm = [_pitchBankYaw select 1, _pitchBankYaw select 0, _pitchBankYaw select 2] call FUNC(RPY2DCM);
[_dcm] call FUNC(DCM2VectorDirAndUp);
