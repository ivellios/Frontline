/*
    Function:       ADA_Main_fnc_vectorToPby
    Author:         Adanteh
    Description:    Converts vectorDirAndUp into PitchBankYaw
*/
#include "macros.hpp"

params ["_vectorDirAndUp"];

private _dcm = _vectorDirAndUp call CFUNC(vectorDirAndUp2DCM);
private _rpy = [_dcm] call CFUNC(DCM2RPY);
[_rpy select 1, _rpy select 0, _rpy select 2]
