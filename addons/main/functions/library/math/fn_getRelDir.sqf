/*
    Function:       FRL_Main_fnc_getRelDir
    Author:         Mondkalb
    Description:    Gets relative direction based on position instead of position like A3 command
*/
#include "macros.hpp"

params ["_pos1", "_pos2"];
if (count _pos1 == 2) then {_pos1 set [2,0]};
if (count _pos2 == 2) then {_pos2 set [2,0]};

private _v = _pos1 vectorFromTo _pos2;
private _d = (_v select 0) atan2 (_v select 1);

if (_d < 0) then {
    _d = _d + 360;
};

_d
