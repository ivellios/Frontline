/*
    Function:       FRL_Main_fnc_calcVectorDirAndUp
    Author:         Adanteh
    Description:    Calculates vector dir and up from pitch bank yaw
*/
#include "macros.hpp"

params ["_pitch", "_bank", "_yaw"];
_yaw = (360 - _yaw) - 360;
private _dirX = 0;
private _dirY = 1;
private _dirZ = 0;
private _upX = 0;
private _upY = 0;
private _upZ = 1;

if (_pitch != 0) then {
    _dirY = cos _pitch;
    _dirZ = sin _pitch;
    _upY = -sin _pitch;
    _upZ = cos _pitch;
};
if (_bank != 0) then {
    _dirX = _dirZ * sin _bank;
    _dirZ = _dirZ * cos _bank;
    _upX = _upZ * sin _bank;
    _upZ = _upZ * cos _bank;
};
if (_yaw != 0) then {
    _dirXTemp = _dirX;
    _dirX = (_dirXTemp* cos _yaw) - (_dirY * sin _yaw);
    _dirY = (_dirY * cos _yaw) + (_dirXTemp * sin _yaw);
    _upXTemp = _upX;
    _upX = (_upXTemp * cos _yaw) - (_upY * sin _yaw);
    _upY = (_upY * cos _yaw) + (_upXTemp * sin _yaw);
};
[[_dirX,_dirY,_dirZ], [_upX,_upY,_upZ]];
