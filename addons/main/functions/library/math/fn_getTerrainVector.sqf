/*
    Function:       FRL_Main_fnc_getTerrainVector
    Author:         Adanteh
    Description:    Gets vector dir and up for a terrain. Can be used to align objects to terrain
*/
#include "macros.hpp"

params [["_position", [0, 0, 0]], ["_direction", 0]];

private _vSurface = surfaceNormal _position;
private _vAngle = [sin _direction, +cos _direction, 0] vectorCrossProduct _vSurface;
private _vDir = _vSurface vectorCrossProduct _vAngle;
[_vDir, _vSurface]
