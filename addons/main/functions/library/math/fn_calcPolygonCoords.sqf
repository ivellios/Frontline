/*
    Function:       FRL_Main_fnc_calcPolygonCoords
    Author:         Adanteh
    Description:    This gets the bottom left corner and top right corner, plus dimensions of a polygon.
                    It takes leftmost point and bottommost point
*/
#include "macros.hpp"

params ["_corners"];
private _originBottomLeft = [999999, 999999, 0]; // Marks the min X and min Y coordinate (Can be outside the AO!)
private _originTopRight = [0, 0, 0]; // Marks the min X and min Y coordinate (Can be outside the AO!)
{
    _x params ["_xPosCorner", "_yPosCorner"];
    if (_xPosCorner < (_originBottomLeft select 0)) then {
        _originBottomLeft set [0, _xPosCorner];
    };
    if (_yPosCorner < (_originBottomLeft select 1)) then {
        _originBottomLeft set [1, _yPosCorner];
    };
    if (_xPosCorner > (_originTopRight select 0)) then {
        _originTopRight set [0, _xPosCorner];
    };
    if (_yPosCorner > (_originTopRight select 1)) then {
        _originTopRight set [1, _yPosCorner];
    };
} forEach _corners;

private _fullWidth = (_originTopRight select 0) - (_originBottomLeft select 0);
private _fullHeight = (_originTopRight select 1) - (_originBottomLeft select 1);
//[[_originBottomLeft, _originTopRight, _fullWidth, _fullHeight], 'lime', 10] call FUNC(debugMessage);
[_originBottomLeft, _originTopRight, _fullWidth, _fullHeight];
