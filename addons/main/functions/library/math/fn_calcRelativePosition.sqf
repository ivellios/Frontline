/*
    Function:       FRL_Main_fnc_calcRelativePosition
    Author:         Adanteh
    Description:    Calculates a relative position, based on direction
    This is basically a way to do modelToWorld, that will works when objects are pitch/banked
*/
#include "macros.hpp"

params [
    ["_position", [0, 0, 0], [[], objNull]],
    ["_offset", [1, 0, 0]],
    ["_angle", 0]
];

//[[_fnc_scriptNameShort, _position, _offset, _angle], "lime"] call MFUNC(debugMessage);
if (_position isEqualType objNull) then {
    _position = getPosWorld _position;
};

_position = +_position;
_position params ["_x", "_y", "_z"];
_offset params ["_xOff", "_yOff", "_zOff"];

_position set [0, _x + (_xOff * cos (180 - _angle)) + (_yOff * sin (180 - _angle))];
_position set [1, _y + (_xOff * sin _angle) + (_yOff * cos _angle)];
_position set [2, _z + _zOff];
_position;
