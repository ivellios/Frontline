/*
    Function:       FRL_Main_fnc_cfgSettingLoadSpecial
    Author:         Adanteh
    Description:    Caches a config tree to a namespace, processed the cfg entries in a specific way that's common to us (Based on some keywords)
*/
#include "macros.hpp"

params ["_prefix", "_cfg", ["_namespace", GVAR(cfgSettingNamespace)], ["_inherit", true]];

if !(isClass _cfg) exitWith {};

{
    if (isClass _x) then {
        [format ["%1_%2", _prefix, configName _x], _x, _namespace, _inherit] call FUNC(cfgSettingLoadSpecial);
    } else {
        private _configName = configName _x;
        private _value = switch (true) do {
            case (isArray _x): {getArray _x};
            case (isNumber _x): {getNumber _x};
            case (isText _x): {getText _x};
            default { 0 };
        };

        _value = switch (toLower _configName) do {
            case "displayname": { // -- If no name is given, use the classname (replacing underscore by spaces)
                if (_value isEqualTo "") then {
                    _value = [_configName, "_", " "] call FUNC(replaceInString);
                };
                if (isLocalized _value) then {
                    _value = localize _displayname;
                };
                _value;
            };
            case "function": { [_value] call FUNC(parseToCode) };
            case "condition": { [_value] call FUNC(parseToCode) };
            case "conditionshow": { [_value] call FUNC(parseToCode) };
            default { _value };
        };
        _namespace setVariable [format ["%1_%2", _prefix, _configName], _value];

    };
    nil
} count (configProperties [_cfg, "true", _inherit]);
