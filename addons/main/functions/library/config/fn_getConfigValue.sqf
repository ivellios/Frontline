/*
    Function:       FRL_Main_fnc_getConfigValue
    Author:         Adanteh
    Description:    This returns a config entry, with a possible preference for mission
    Example:        [["FRL", "Roles", "SomeEntry"], nil, true] call FRL_Main_fnc_getConfigValue;
*/
#include "macros.hpp"

params [["_configPath", []], ["_typesAllowed", [[0,"", []]], [[]]], ["_useMission", true], "_returnDefault"];

private "_return";
{
    private _cfg = _x;
    {
        _cfg = _cfg >> _x;
        nil;
    } count _configPath;
    _return = switch ( true ) do {
    	case (isNumber _cfg): {getNumber _cfg};
    	case (isText _cfg): {getText _cfg};
    	case (isArray _cfg): {getArray _cfg;};
    	default {nil};
    };
    if !(isNil "_return") then {
        if !(_return isEqualTypeAny _typesAllowed) then {
            _return = nil;
        } else {
            _return breakOut (_fnc_scriptName + "_Main");
        };
    };
} forEach (if (_useMission) then { [missionConfigFile, configFile] } else { [configFile] });

if (isNil "_return") exitWith {
    _returnDefault
};

_return
