/*
    Function:       FRL_Main_fnc_cfgSettingLoad
    Author:         Adanteh
    Description:    Caches a config tree to a namespace
*/
#include "macros.hpp"

params ["_prefix", "_cfg", ["_namespace", GVAR(cfgSettingNamespace)]];

if !(isClass _cfg) exitWith {};

{
    if (isClass _x) then {
        [format ["%1_%2", _prefix, configName _x], _x] call FUNC(cfgSettingLoad);
    } else {
        _namespace setVariable [format ["%1_%2", _prefix, configName _x], switch (true) do {
            case (isArray _x): {getArray _x};
            case (isNumber _x): {getNumber _x};
            case (isText _x): {getText _x};
        }];

    };
    nil
} count (configProperties [_cfg, "true"]);
