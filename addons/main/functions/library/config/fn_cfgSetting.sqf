/*
    Function:       FRL_Main_fnc_cfgSetting
    Author:         Adanteh
    Description:    Gets value from namespace
*/
#include "macros.hpp"

params ["_key", "_default", ["_namespace", GVAR(cfgSettingNamespace)]];
_namespace getVariable [_key, _default];
