/*
    Function:       FRL_Main_fnc_cfgSettingSet
    Author:         Adanteh
    Description:    Replaces given setting tag
*/
#include "macros.hpp"

params ["_key", "_value", ["_namespace", GVAR(cfgSettingNamespace)]];
_namespace setVariable [_key, _value];
