/*
    Function:       FRL_Main_fnc_posIsInTerrain
    Author:         Adanteh
    Description:    Checks if given position is within the terrain
    Example:        [[-10, 0]] call FRL_Main_fnc_posIsInTerrain
*/
#include "macros.hpp"

params ["_pos", ["_offset", 0]];

if ((_pos select 0) < (0 + _offset)) exitWith { false };
if ((_pos select 1) < (0 + _offset)) exitWith { false };

if ((_pos select 0) > (worldSize - _offset)) exitWith { false };
if ((_pos select 1) > (worldSize - _offset)) exitWith { false };

true
