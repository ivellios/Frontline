/*
    Function:       FRL_Main_fnc_getNearestName
    Author:         Adanteh
    Description:    Gets nearest named location
    Example:        [getPos player] call FRL_Main_fnc_getNearestName
*/
#include "macros.hpp"

params ["_pos", ["_range", 800]];

// -- Cache config types
private _allLocationTypes = uiNamespace getVariable QGVAR(locationCfg);
if (isNil "_allLocationTypes") then {
    _allLocationTypes = ("true" configClasses (configFile >> "CfgLocationTypes")) apply { configName _x };
    uiNamespace setVariable [QGVAR(locationCfg), _allLocationTypes];
};

// -- Check for config types
{
    if (text _x != "") then {
        private _text = format [localize (switch (round ((locationPosition _x getDir _pos) % 360 / 45)) do {
        		default {"STR_A3_BIS_fnc_locationDescription_n"};
        		case 1: {"STR_A3_BIS_fnc_locationDescription_ne"};
        		case 2: {"STR_A3_BIS_fnc_locationDescription_e"};
        		case 3: {"STR_A3_BIS_fnc_locationDescription_se"};
        		case 4: {"STR_A3_BIS_fnc_locationDescription_s"};
        		case 5: {"STR_A3_BIS_fnc_locationDescription_sw"};
        		case 6: {"STR_A3_BIS_fnc_locationDescription_w"};
        		case 7: {"STR_A3_BIS_fnc_locationDescription_nw"};
        	}),
        	text _x
        ];
        _text breakOut (_fnc_scriptName + "_Main");
    };
} count (nearestLocations [_pos, _allLocationTypes, _range]);

// -- Check custom placed ones
private _customMarkers = [];
{
    _x params ["_mName", "_mPos"];
    private _dis = _mName distance2D _pos;
    if (_dis <= _range) then {
        _customMarkers pushBack [_dis, _mName];
    };
    nil
} count (missionNamespace getVariable [QGVAR(markerLocations), []]);
if !(_customMarkers isEqualTo []) then {
    _customMarkers sort true;
    (markerText ((_customMarkers select 0) select 1)) breakOut (_fnc_scriptName + "_Main");
};


// -- Return mapgrid
"Position " + mapGridPosition _pos;
