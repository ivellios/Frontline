/*
    Function:       FRL_Main_fnc_namespaceName
    Author:         Adanteh
    Description:    Gets a stringified name for the current namespace
*/
#include "macros.hpp"

params ["_namespace"];

if (_namespace isEqualTo missionNamespace) exitWith { "missionNamespace" };
if (_namespace isEqualTo uiNamespace) exitWith { "uiNamespace" };
if (_namespace isEqualTo parsingNamespace) exitWith { "parsingNamespace" };
if (_namespace isEqualTo profileNamespace) exitWith { "profileNamespace" };
if (_namespace isEqualType objNull) exitWith { "objectNamespace" };
if (_namespace isEqualType locationNull) exitWith { "locationNamespace" };

"Unknown Namespace";
