/*
    Function:       FRL_Main_fnc_deleteNamespace
    Author:         Adanteh
    Description:    Deletes a namespace
*/

params [["_namespace", locationNull]];
if (_namespace isEqualType locationNull) then {
    deleteLocation _namespace;
} else {
    if (_namespace isEqualType objNull) then {
        deleteVehicle _namespace;
    };
};
