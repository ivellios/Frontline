/*
    Function:       FRL_Main_fnc_forEachVariable
    Author:         Adanteh
    Description:    Does code for each variable of a namespace
    Example:        [player, { hint str [_key, _value] }] call FRL_Main_fnc_forEachVariable
*/
#include "macros.hpp"

params ["_namespace", "_code"];
_code = _code call FUNC(parseToCode);

{
    private _key = _x;
    if !(isNil "_key") then {
        private _value = _namespace getVariable _key;
        [_key, _value] call _code;
    };

    nil;
} count (allVariables _namespace);
