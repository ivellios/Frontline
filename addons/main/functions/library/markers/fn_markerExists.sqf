/*
    Function:       FRL_Main_fnc_markerExists
    Author:         Adanteh
    Description:    Checks if given marker name exist
*/
#include "macros.hpp"

params ["_marker"];

if !(getMarkerColor _marker isEqualTo "") exitWith { true };
if !(getMarkerPos _marker isEqualTo [0, 0, 0]) exitWith { true };

false
