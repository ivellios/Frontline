/*
    Function:       FRL_Main_fnc_createObjectMarker
    Author:         Adanteh
    Description:    Adds a (local) marker based on given object size
*/
#include "macros.hpp"

params [
    ["_object", objNull],
    ["_color", "ColorGrey"],
    ["_shape", "RECTANGLE"],
    ["_brush", "SolidFull"],
    ["_alpha", 1]
];

private _markerName = [str _object, netID _object] select isMultiplayer;

// -- Get marker data -- //
private _boundingBox = boundingBoxReal _object;
private _w = ((_boundingBox select 1 select 0) - (_boundingBox select 0 select 0))/2;
private _h = ((_boundingBox select 1 select 1) - (_boundingBox select 0 select 1))/2;
private _objectPos = getPosATL _object;

// -- Create marker and adjust settings -- //
private _marker = createMarkerLocal [_markerName, _objectPos];
_marker setMarkerShapeLocal _shape;
_marker setMarkerSizeLocal [_w, _h];
_marker setMarkerColorLocal _color;
_marker setMarkerDirLocal (getDir _object);
_marker setMarkerBrushLocal _brush;
_marker setMarkerAlphaLocal _alpha;
_marker;
