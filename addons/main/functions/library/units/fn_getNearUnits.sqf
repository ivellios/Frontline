/*
    Function:       FRL_Main_fnc_getNearUnits
    Author:         Adanteh
    Description:    Gets nearby units, position is object or getPosAGL(??)
*/
#include "macros.hpp"

params ["_pos", "_range", ["_excludeCrew", false]];
private _return = +(allUnits inAreaArray [_pos, _range, _range, 0, false, _range]);
if (_excludeCrew) then {
    _return = _return select { isNull objectParent _x };
};
_return
