/*
    Function:       FRL_Main_fnc_disableMouse
    Author:         Adanteh
    Description:    Disables the mouse clicking (Doesn't always work because BIS)
*/
#include "macros.hpp"

params ["_disable", ["_target", player]];

if (_disable) then {
	private _actions = [];
	_actions pushBack (_target addAction ["", {}, "", 0, false, true, "DefaultAction"]);
	_actions pushBack (_target addAction ["", {}, "", 0, false, true, "ZoomTemp"]); // Figure out a way to block this too. Doesn't work right now
	GVAR(mouseBlockActions) = _actions;
} else {
	private _actionIDs = missionNamespace getVariable [QGVAR(mouseBlockActions), []];
	if !(_actionIDs isEqualTo []) then {
		{ _target removeAction _x; nil; } count _actionIDs;
	};
};
