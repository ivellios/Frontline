/*
    Function:       FRL_Main_fnc_getDeathAnim
    Author:         Adanteh
    Description:    Gets death animation for given vehicle
*/
#include "macros.hpp"

params ["_unit"];

private _animState = animationState _unit;
private _unitAnimCfg = configFile >> "CfgMovesMaleSdr" >> "States" >> _animState;

// exit if dead unit is already in the death Anim
if (getNumber (_unitAnimCfg >> "terminal") isEqualTo 1) exitWith {_animState};

if !(isNull objectParent _unit) then {
    private _interpolateTo = getArray (_unitAnimCfg >> "interpolateTo");
    if (!(_interpolateTo isEqualTo [])) exitWith {
        _interpolateTo select 0
    };
    "Unconscious"
} else {
    getText (configFile >> "CfgMovesBasic" >> "Actions" >> getText (_unitAnimCfg >> "actions") >> "die")
};
