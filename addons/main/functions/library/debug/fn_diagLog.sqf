/*
    Function:       FRL_Main_fnc_diagLog
    Author:         Adanteh
*/
#include "macros.hpp"

diag_log format ["[%2 LOG] %1", _this, QUOTE(PREFIX)];
