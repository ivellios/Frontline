/*
    Function:       FRL_Main_fnc_exportZoomCfg
    Author:         Adanteh
    Description:    Exports all config values that have zoom value entries
    Example:        call FRL_Main_fnc_exportZoomCfg;
*/
#include "..\macros.hpp"
#define NAMESPACE _tempNamespace
#define INDENT(var1) private _indent = []; _indent resize var1; _indent = ((_indent apply { toString [9] }) joinString "")

if (isMultiplayer) exitWith {
    [[_fnc_scriptNameShort, "Use this script in singeplayer only!"], "red"] call MFUNC(debugMessage);
    [format ["%1<br />Only use this in singleplayer", _fnc_scriptNameShort], "error"] call MFUNC(notificationShow);
};


private NAMESPACE = createLocation ["fakeTown", [-200, -200, 300], 0, 0];
private _fnc_traverseAllClasses = {
    params ["_cfg"];
    diag_log [_fnc_scriptNameShort, configName _cfg];
    private _entries = configProperties [_cfg, "true", false];
    private _entryNames = _entries apply { toLower configName _x };
    if (call _fnc_filterMain) then {
        if (call _filter) then {
            _hasZoomConfig = true;
            _configHierarchy pushBack (configHierarchy _cfg);
            NAMESPACE setvariable [format ["%1_hasValues", str _cfg], true];
            (configHierarchy _cfg) apply { NAMESPACE setVariable [format ["%1_brackets", str _x], true] };
        };
    };

    {
        [_x] call _fnc_traverseAllClasses;

    } forEach ([_cfg, 5] call BIS_fnc_returnChildren);
};

// -- Write every class in the hierarchy as required
private _fnc_buildHierarchy = {
    params ["_hierarchy", ["_inherit", false]];

    for "_i" from (count _hierarchy - 1) to 0 step -1 do {
        private _parentClass = _hierarchy select _i;
        private _inheritsFrom = inheritsFrom _parentClass;
        private _inheritText = "";
        if !(isNull _inheritsFrom) then {
            _inheritText = format [": %1", configName _inheritsFrom];
            NAMESPACE setVariable [format ["%1_brackets", str _inheritsFrom], true];
        };

        NAMESPACE setVariable [str _parentclass, [_inheritText, _dumpInfo]];

        if !(isNull _inheritsFrom) then {
            private _inheritHierarchy = configHierarchy _inheritsFrom;
            [_inheritHierarchy, false] call _fnc_buildHierarchy;

            _inheritHierarchy deleteAt (count _inheritHierarchy - 1);
            _inheritHierarchy apply { NAMESPACE setVariable [format ["%1_brackets", str _x], true]; };

            // -- Weird inheritance that we can't figure out by script commands, so trace inheritance and figure out where class is first defined
            // -- This is when inheritsFrom returns a class N up the three, instead of the first inheriting parent class
            private _classToCheck = configName _inheritsFrom;
            private _mark = false;
            diag_log format ["%1 inherits from %2 - '%3'", _parentClass, _inheritsFrom, _classToCheck];

            private _subHierarchy = configHierarchy _parentClass;
            for "_j" from (count _subHierarchy - 2) to 0 step -1 do {
                private _class = _subHierarchy select _j;
                private _inheritsFrom = inheritsFrom _class;
                diag_log format ["    Checking: %1 - '%2'", configName _class, _inheritsFrom];
                if !(isNull _inheritsFrom) then {
                    private _children = [_inheritsFrom, 0] call BIS_fnc_returnChildren;
                    //private _children = "isClass _x" configClasses _inheritsFrom;
                    {
                        diag_log format ["        Child: %1", configName _x];
                        if (configName _x isEqualTo _classToCheck) then {
                            diag_log format ["---- %1 found within %2", _classToCheck, _inheritsFrom];
                            (configHierarchy _inheritsFrom) apply { NAMESPACE setVariable [format ["%1_brackets", str _x], true] };
                            private _inheritNested = (inheritsFrom _x);
                            private _inheritsFromText = ["", format [": %1", configName (inheritsFrom _x)]] select !(isNull _inheritNested);
                            NAMESPACE setVariable [str _x, [_inheritsFromText, _dumpInfo]];


                            // -- tracing full inheritance back even if no values are given, to make sure inherited subclasses are not missing
                            if !(isNull _inheritNested) then {
                                [configHierarchy _inheritNested] call _fnc_buildHierarchy;
                            };

                            //[configHierarchy _x] call _fnc_buildHierarchy;
                        };
                    } forEach _children;
                };
            };
        };
    };
};

private _fnc_writeText = {
    params ["_classes"];
    {
        private _data = NAMESPACE getVariable (str _x);
        diag_log text format ["class %1    - %2, %3", (configName _x), !isNil "_data"];

        if !(isNil "_data") then {
            private  _depth = _depth + 1;
            INDENT(_depth);
            private _hasValues = !isNil { NAMESPACE getVariable (format ["%1_hasValues", str _x]) };
            private _brackets = !isNil { NAMESPACE getVariable (format ["%1_brackets", str _x]) };
            _data params ["_inheritText", "_dumpInfo"];
            diag_log text format ["-- Writing %1 - %2", (configName _x), _brackets];

            if (_brackets) then {
                _allText pushBack format ["%1class %2%3 {", _indent, configName _x, _inheritText];
                //private _subclasses = [_x, 0] call BIS_fnc_returnChildren;
                private _subclasses = "isClass _x" configClasses _x;
                [_subclasses] call _fnc_writeText;
                if (_hasValues) then {
                    _allText pushBack format ["%1", [_x] call _dumpInfo];
                };
                _allText pushBack format ["%1};", _indent];
            } else {
                _allText pushBack format ["%1class %2;", _indent, configName _x, _inheritText];
            };
        };
    } forEach _classes;
};


// -- Each config class must have these entries to show up
private _fnc_filter = {
    (getNumber (_cfg >> "opticsZoomMin") in [0.25, 0.275, 0.34, 0.375]) &&
    (getNumber (_cfg >> "opticsZoomMax") in [1.25, 1, 1.1, 1.125]) &&
    (getNumber (_cfg >> "opticsZoomInit") == 0.75)
};

// -- Each config class must have these entries to show up
private _fnc_filter3 = {
    (getNumber (_cfg >> "opticsZoomInit") == 0.75)
};

// -- Each class will we written with this data
private _fnc_dumpInfo = {
    private _info = "";
    INDENT(_depth + 1);
    {
        _info = _info + format ["%1%3%2", _indent, _linebreak, _x];
    } forEach ['opticsZoomMin = __ZOOMMAX;', 'opticsZoomMax = __ZOOMMIN;', 'opticsZoomInit = __ZOOMINIT;'];
    _info
};

// -- Each class will we written with this data
private _fnc_dumpInfo3 = {
    params ["_cfg"];
    private _info = "";
    INDENT(_depth + 1);
    {
        private _value = getNumber (_cfg >> _x);
        _info = _info + format ["%1%3 = %4;%2", _indent, _linebreak, _x, _value];
    } forEach ['opticsZoomMin', 'opticsZoomMax', 'opticsZoomInit'];
    _info
};

private _fnc_filter1 = {
    (getNumber (_cfg >> "minFov") in [0.25, 0.6]) &&
    (getNumber (_cfg >> "maxFov") in [1.25, 0.85, 1.4, 1.1])
};

// -- Each class will we written with this data
private _fnc_dumpInfo1 = {
    private _info = "";
    INDENT(_depth + 1);
    {
        _info = _info + format ["%1%3%2", _indent, _linebreak, _x];
    } forEach ['minFov = __ZOOMMAX;', 'maxFov = __ZOOMMIN;'];
    _info
};

// -- Each class will we written with this data
private _fnc_dumpInfo4 = {
    params ["_cfg"];
    private _info = "";
    INDENT(_depth + 1);
    {
        private _value = if (isText (_cfg >> _x)) then {
            format ['"%1"', getText (_cfg >> _x)];
        } else {
            getNumber (_cfg >> _x)
        };
        _info = _info + format ["%1%4 = %3;%2", _indent, _linebreak, _value, _x];
    } forEach ['initFov', 'minFov', 'maxFov'];
    _info
};

private _generalvar = 0;
private _allText = [];
private _linebreak = toString [13,10];
private _return = [];
private _hierarchyArray = [];
private _fnc_inheritTree = {
    params ["_cfg"];
    diag_log text format ["-- Inherit %1", (configName _cfg)];
    private _inherit = inheritsFrom _cfg;
    if !(isNull _inherit) then {
        _inheritTree pushBackUnique _inherit;
        [_inherit] call _fnc_inheritTree;
    };
};

{
    _x params ["_startCfg", "_fnc_filterMain", "_filter", "_dumpInfo"];
    private _baseCfg = "true" configClasses _startCfg;
    private _baseReturn = [];

    private _inheritTree = [];
    [inheritsFrom _startCfg] call _fnc_inheritTree;
    _baseCfg = _baseCfg + _inheritTree;
    _baseCfg pushBack _startCfg;

    diag_log text format ["-- Start class %1", _startCfg];
    diag_log text format ["-- Start class count %1, %2", count _baseCfg, count _inheritTree];

    for "_i" from 0 to (count _baseCfg - 1) do {
        private _configHierarchy = [];
        private _hasZoomConfig = false;
        private _cfg = _baseCfg select _i;
        [_cfg] call _fnc_traverseAllClasses;
        if (_hasZoomConfig) then {
            _return pushBack [(configName _cfg), _configHierarchy];
            _baseReturn pushBack [(configName _cfg), _configHierarchy];
        };
    };

    {
        _x params ["_name", "_hierarchy"];
        {
            [_x] call _fnc_buildHierarchy;
        } forEach _hierarchy;
    } forEach _baseReturn;


} forEach [
    //[(configFile >> "cfgWeapons"), { toLower _mainFilter in _entryNames }, _fnc_filter3, _fnc_dumpInfo3]
    //[(configfile >> "CfgVehicles" >> "B_MBT_01_cannon_F" >> "Turrets" >> "MainTurret" >> "OpticsIn"), { true }, { true }, _fnc_dumpInfo4],
    //[(configfile >> "CfgVehicles" >> "O_MBT_02_cannon_F" >> "Turrets" >> "MainTurret" >> "OpticsIn"), { true }, { true }, _fnc_dumpInfo4],
    //[(configfile >> "CfgVehicles" >> "O_APC_Tracked_02_cannon_F" >> "Turrets" >> "MainTurret" >> "OpticsIn"), { true }, { true }, _fnc_dumpInfo4],
    //[(configfile >> "CfgVehicles" >> "B_MBT_01_TUSK_F" >> "Turrets" >> "MainTurret" >> "OpticsIn"), { true }, { true }, _fnc_dumpInfo4],
    //[(configfile >> "CfgVehicles" >> "B_APC_Tracked_01_rcws_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4],
    //[(configfile >> "CfgVehicles" >> "B_APC_Wheeled_01_cannon_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4],
    //[(configfile >> "CfgVehicles" >> "O_MRAP_02_hmg_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4],
    //[(configfile >> "CfgVehicles" >> "O_APC_Wheeled_02_rcws_v2_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4],
    [(configfile >> "CfgVehicles" >> "O_HMG_01_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4],
    [(configfile >> "CfgVehicles" >> "O_GMG_01_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4],
    [(configfile >> "CfgVehicles" >> "B_T_Static_AT_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4],
    [(configfile >> "CfgVehicles" >> "B_T_Static_AA_F" >> "Turrets" >> "MainTurret" >> "ViewOptics"), { true }, { true }, _fnc_dumpInfo4]
    /*[(configFile >> "cfgVehicles"), "minFov", _fnc_filter1, _fnc_dumpInfo1]*/
];

private _depth = 0;
["isClass _x" configClasses configFile] call _fnc_writeText;

copyToClipboard (_allText joinString _lineBreak);

if (isMultiplayer) then {
    hint "This script doesn't copy to clipboard in MP, run in Singleplayer";
};

allVariables NAMESPACE;
