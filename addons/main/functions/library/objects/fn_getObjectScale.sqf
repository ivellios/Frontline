/*
    Function:       FRL_Main_fnc_getObjectScale
    Author:         Adanteh
    Description:    Gets the scale of current terrain object
*/
#include "macros.hpp"
#define __TESTPOS [-2500, -2500, 2500]

params [["_object", objNull]];
if (isNull _object) exitWith { 1 };

private _corners = (boundingBoxReal _object);
private _distanceObject = (_object modelToWorld (_corners select 0)) distance (_object modelToWorld (_corners select 1));

private _objectModelPath = (getModelInfo _object) select 1;
private _testObject = createSimpleObject [_objectModelPath, __TESTPOS];
private _corners = (boundingBoxReal _testObject);
private _distanceReal = (_testObject modelToWorld (_corners select 0)) distance (_testObject modelToWorld (_corners select 1));

// -- This value could be cached with _objectModelPath if you want, seeing it won't change
deleteVehicle _testObject;

(_distanceObject / _distanceReal)
