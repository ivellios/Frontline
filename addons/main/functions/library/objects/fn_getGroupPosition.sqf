/*
    Function:       FRL_Main_fnc_getGroupPosition
    Author:         Adanteh
    Description:    Gets group position (Average position between all alive units in the group),
    Returns:        Array POSATL
*/
#include "macros.hpp"

params ["_group"];

private _units = units _group select { alive _x };
// -- avoid divide by zero error eh?
if (count _units == 0) exitWith {
    [0,0,0]
};
private _averagePosition = [0, 0, 0];
_units apply { _averagePosition = _averagePosition vectorAdd (getPosATL _x ) };
(_averagePosition vectorMultiply (1 / (count _units)));
