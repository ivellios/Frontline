/*
    Function:       FRL_Main_fnc_getObjectsInArea
    Author:         Adanteh
    Description:    Gets all the objects in a given area (With optional filter)
*/
#include "macros.hpp"

params ["_corners", ["_source", []], ["_angle", 0]];

_startPos = +(_corners select 0);
_currentPos = +(_corners select 1);
private _posStart = [_startPos, -_angle] call FUNC(rotateAroundPoint);
private _posCurrent = [_currentPos, -_angle] call FUNC(rotateAroundPoint);
(_posCurrent vectorDiff _posStart) params ["_xDimension", "_yDimension"];
private _center = (_startPos vectorAdd _currentPos) vectorMultiply 0.5;

// -- Filter objects within the area
+(_source select { _x inArea [_center, _xDimension / 2, _yDimension / 2, _angle, true]; });
