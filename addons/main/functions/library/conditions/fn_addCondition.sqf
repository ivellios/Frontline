/*
    Function:       FRL_Main_fnc_addCondition
    Author:         Adanteh
    Description:    Adds a stacked condition under given tag.
                    This allows you to add a bunch of conditions from different modules,
                    For example FO placement condition based on different gamemodes
*/
#include "macros.hpp"

params ["_condition", "_code"];

private _conditionName = format [QGVAR(stackedCondition_%1), toLower _condition];
private _currentConditions = missionNamespace getVariable [_conditionName, []];
private _conditionID = _currentConditions pushBack (_code call FUNC(parseToCode));
missionNamespace setVariable [_conditionName, _currentConditions, false];

_conditionID;
