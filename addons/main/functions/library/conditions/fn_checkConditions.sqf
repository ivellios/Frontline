/*
    Function:       FRL_Main_fnc_checkConditions
    Author:         Adanteh
    Description:    Checks stacked conditions. If return is different from expected return, it'll exit
					Allows you add extra conditions to certain systems while keeping things modular,
					for example the FO placement restrictions can be placed in the gamemode
					This function will CHECK a given condition, use FRL_Main_fnc_addConditions

	Example:		["foPlaceRealtime", [_args], true] call FRL_Main_fnc_checkConditions
*/
#include "macros.hpp"

params ["_condition", ["_args", []], ["_expect", true]];

private _conditions = missionNamespace getVariable [(format [QGVAR(stackedCondition_%1), _condition]), []];

//[[_fnc_scriptNameShort, _args], "lime"] call MFUNC(debugMessage);

{
	if !((_args call _x) isEqualTo _expect) exitWith {
		_expect = !_expect;
		nil;
	};
	nil;
} count _conditions;

_expect;
