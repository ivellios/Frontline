/*
    Function:       FRL_Main_fnc_localEvent
    Author:         Adanteh
    Description:    Calls a local event
*/
#include "macros.hpp"

params [["_key", ""], ["_args", []]];

if (isNil QGVAR(eventNamespace)) exitWith {
    GVAR(eventNamespace) = false call FUNC(createNamespace);
};

#ifdef DEBUGFULL
    [format ["<t size='0.3' shadow='1'>%1</t>", [_fnc_scriptNameShort, _key, _args] joinString " "], "white", 3] call MFUNC(debugMessage);
#endif

private _currentCode = GVAR(eventNamespace) getVariable [_key, []];

{
    private _eventIndex = _forEachIndex;
    ([_args, []]) call _x;
} forEach _currentCode;
