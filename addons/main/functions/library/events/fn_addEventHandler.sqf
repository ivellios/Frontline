/*
    Function:       FRL_Main_fnc_addEventHandler
    Author:         Adanteh
    Description:    Creates a scripted event
    Example:        ["someEvent", { hint 'someEvent was triggered' }] call FRL_Main_fnc_addEventHandler;
*/
#include "macros.hpp"

params [["_key", ""], ["_code", { true }, ["", {}]]];

if (isNil QGVAR(eventNamespace)) then {
    GVAR(eventNamespace) = false call FUNC(createNamespace);
};

private _currentCode = GVAR(eventNamespace) getVariable [_key, []];
_currentCode pushBack ([_code] call FUNC(parseToCode));
GVAR(eventNamespace) setVariable [_key, _currentCode];
