/*
    Function:       FRL_Main_fnc_changeKeybind
    Author:         Adanteh
    Description:    Changes keybind
*/
#include "macros.hpp"

params ["_category", "_tag", "_newKeybind"];

private _varName = [_category, _tag] joinString ".";

private _keybindIndex = (GVAR(keybindArray) select 3) find (toLower _varName);
[[_fnc_scriptNameShort, _varname, _keybindIndex], "olive"] call MFUNC(debugMessage);
if (_keybindIndex == -1) exitWith { };

(GVAR(keybindArray) select 0) set [_keybindIndex, _newKeybind];

private _profileSettingName = [QUOTE(PREFIX), QUOTE(MODULE), "keybind", _varName] joinString ".";
profileNamespace setVariable [_profileSettingName, _newKeybind];
saveProfileNamespace;
