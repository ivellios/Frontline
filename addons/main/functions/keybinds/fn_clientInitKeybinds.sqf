/*
    Function:       FRL_Main_fnc_clientInitKeybinds
    Author:         Adanteh
    Description:    Creates the event handlers for keybinding system
*/
#include "macros.hpp"

if (isNil QGVAR(keybindArray)) then { GVAR(keybindArray) = [[], [], [], [], []] };
GVAR(keybindNamespace) = false call CFUNC(createNamespace);

[] spawn {
	waitUntil { !isNull (findDisplay 46) };
	sleep 5;
	waitUntil { !isNull (findDisplay 46) };
    sleep 5;

	disableSerialization;
	private _display = (findDisplay 46);
	{
		_x params ["_event", "_code"];
		private _ehHandle = uiNamespace getVariable [QGVAR(keybind) + _event, -1];
		if (_ehHandle != -1) then {
			_display displayRemoveEventHandler [_event, _ehHandle];
		};
		uiNamespace setVariable [QGVAR(keybind) + _event, (_display displayAddEventHandler [_event, _code])];
	} forEach [
		["keyDown", (format ["[true, _this, %2] call %1;", QFUNC(keybindCheck), ctrlIDD _display])],
		["keyUp", (format ["[false, _this, %2] call %1;", QFUNC(keybindCheck), ctrlIDD _display])]
	];
};

// -- Event to add keybind system to a display
["addKeybinds.display", {
	(_this select 0) params ["_display"];
	{
		_x params ["_event", "_code"];
		private _ehHandle = _display getVariable [QGVAR(keybind) + _event, -1];
		if (_ehHandle != -1) then {
			_display displayRemoveEventHandler [_event, _ehHandle];
		};
		_display setVariable [QGVAR(keybind) + _event, (_display displayAddEventHandler [_event, _code])];
	} forEach [
		["keyDown", (format ["[true, _this, %2] call %1;", QFUNC(keybindCheck), ctrlIDD _display])],
		["keyUp", (format ["[false, _this, %2] call %1;", QFUNC(keybindCheck), ctrlIDD _display])]
	];
}] call CFUNC(addEventHandler);

// -- Event to add keybind system to a control
["addKeybinds.control", {
	(_this select 0) params ["_control"];
	{
		_x params ["_event", "_code"];
		private _ehHandle = _control getVariable [QGVAR(keybind) + _event, -1];
		//_control ctrlRemoveAllEventHandlers _event;
		if (_ehHandle != -1) then {
			_control ctrlRemoveEventHandler [_event, _ehHandle];
		};
		_control setVariable [QGVAR(keybind) + _event, (_control ctrlAddEventHandler [_event, _code])];
	} forEach [
		["keyDown", (format ["[true, _this, %2] call %1;", QFUNC(keybindCheck), ctrlIDC _control])],
		["keyUp", (format ["[false, _this, %2] call %1;", QFUNC(keybindCheck), ctrlIDC _control])]
	];
}] call CFUNC(addEventHandler);


['add', ["gui", 'Frontline Settings', 'Debug', [
    'Fix Keybinds',
    "Readds the keybind handlers",
    QMVAR(CtrlButton),
    true,
    { true; },
    { ["addKeybinds.display", [findDisplay 46]] call CFUNC(localEvent) },
    ["FIX KEYBINDS"]
]]] call MFUNC(settingsWindow);
