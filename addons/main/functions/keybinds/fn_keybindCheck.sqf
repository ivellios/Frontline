/*
    Function:       FRL_Main_fnc_keybindCheck
    Author:         Adanteh
    Description:    Checks connected code for given keycombo press or release
*/
#include "macros.hpp"

params ["_keydown", "_args", "_displayIDD"];
_args params ["", "_key", "_shift", "_ctrl", "_alt"];

_this call FUNC(modifierPressed);

//[format ["%1: [%3 - %4] %2", _fnc_scriptNameShort, [_key, [_shift, _ctrl, _alt]], ["down", "up"] select !_keydown, _displayIDD], "red", 5, 999] call FUNC(debugMessage);

if (_keydown) then {
    GVAR(keybindNamespace) setVariable [format ["%1_pressed", _key], time + 0.5];
} else {
    GVAR(keybindNamespace) setVariable [format ["%1_pressed", _key], -1];
};
private _return = false;
private _index = 0;
{
	// -- This allows us to use nil modifier, which ignores it -- //
	(_x select 1) params [["_shiftCheck", _shift], ["_ctrlCheck", _ctrl], ["_altCheck", _alt]];
	if (_keydown) then {
		if (([_x select 0, [_shiftCheck, _ctrlCheck, _altCheck]]) isEqualTo [_key, [_shift, _ctrl, _alt]]) then {
			if !(call (GVAR(keybindArray select 4) select _index)) exitWith { }; // -- General condition
			_return = call (((GVAR(keybindArray) select 1) select _index) select 0);
		};
	} else {
		// -- Sometimes we don't want to check if the same modifiers are still held when releasing key. -- //
		if ((GVAR(keybindArray) select 2) select _index) then {
			if (([_x select 0, [_shiftCheck, _ctrlCheck, _altCheck]]) isEqualTo [_key, [_shift, _ctrl, _alt]]) then {
				if !(call (GVAR(keybindArray select 4) select _index)) exitWith { }; // -- General condition
				_return = call (((GVAR(keybindArray) select 1) select _index) select 1);
			};
		} else {
			if ((_x select 0) isEqualTo _key) then {
				if !(call (GVAR(keybindArray select 4) select _index)) exitWith { }; // -- General condition
				_return = call (((GVAR(keybindArray) select 1) select _index) select 1);
			};
		};
	};
	_index = _index + 1;
	if (_return) exitWith { };
	nil;
} count (GVAR(keybindArray) select 0);

_return
