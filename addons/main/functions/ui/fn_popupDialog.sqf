/*
    Function:       FRL_Main_fnc_popupDialog
    Author:         Adanteh
    Description:    Shows a popup window

	Parameter(s):
		#0 STRING - _title: Title to show on top of window
		#1 STRING - _caption: Text to show in window (Structured text)
		#2 ARRAY of STRING - _buttons: Array for 3 buttons
			0 _buttonName: STRING
			1 _toolTip: STRING
			2 _actionCode: STRING
        #3 ARRAY - Position to display the popup, if not given center of screen

	Example:
		[displayNull, "Welcome", "How are you doing today?",
			[["Good", "Yay", "systemChat 'Thats good to hear!'; closeDialog 1;"],
			[],
			["Bad", "Boo", "systemChat 'Het komt wel goed schatje'; closeDialog 1;"]]
		] call FRL_Main_fnc_showPopup;
*/
#define __INCLUDE_IDCS
#include "macros.hpp"
#define CTRL(x) (_popupCtrl controlsGroupCtrl (x))

disableSerialization;
params [
    ["_parent", (findDisplay 46)], // -- Parent display / dialog
    ["_title", ""], // -- Title in the title bar of the popup
    ["_text", ""], // -- Structured text to show in the box
    ["_options", []], // -- array of buttons [[_text ,_tooltip, _action]]
    ["_type", QSVAR(CtrlPopupDialog)], // -- Classname for dialog
    ["_screenPos", []] // -- Location of screen to show the dialog
];

[[_fnc_scriptNameShort, _this], "lime", 10] call MFUNC(debugMessage);
if (isNull _parent) then {
    _parent = findDisplay 46;
};

if (_parent isEqualTo (findDisplay 46)) then {
	createDialog QGVAR(emptyDialog);
    _parent = findDisplay 116;
};
private _popupCtrl = _parent ctrlCreate [_type, -1];
private _popupPos = ctrlPosition _popupCtrl;

// -- If screenpos argument is given, change location of popup on screen
if !(_screenPos isEqualTo []) then {
    _popupPos set [0, _screenPos param [0, 0.5]];
    _popupPos set [1, _screenPos param [1, 0.5]];
};

if (_text isEqualType "") then {
	_text = parseText _text;
};

// -- Update the text and adjust to text height
private _textCtrl = CTRL(__IDC_ELEMENT_3);
private _textPos = ctrlPosition _textCtrl;
_textCtrl ctrlSetStructuredText _text;
_textPos set [3, ctrlTextHeight _textCtrl];
_textCtrl ctrlSetPosition _textPos;
_textCtrl ctrlcommit 0;

[[_fnc_scriptNameShort, _parent, _popupCtrl, _popupPos], "purple", 10] call MFUNC(debugMessage);

// -- Adust control group size (This uses the H value, which assumes a text height of 0, so just add height and we know the required Y size)
private _heightWithoutText = [(configFile >> _type >> "h")] call MFUNC(uiGetCfgSize);
_popupPos set [3, _heightWithoutText + (_textPos select 3) + (_textPos select 1)];
_popupCtrl ctrlsetPosition _popupPos;
_popupCtrl ctrlCommit 0;

// -- Update the background
private _backgroundCtrl = CTRL(__IDC_ELEMENT_2);
private _backgroundPos = ctrlPosition _backgroundCtrl;
_backgroundPos set [3, (_popupPos select 3) - (_backgroundPos select 1)];
_backgroundCtrl ctrlSetPosition _backgroundPos;
_backgroundCtrl ctrlcommit 0;

// -- Reposition the buttons below the now adjusted text height
private _buttonsCtrl = CTRL(__IDC_ELEMENT_4);
private _buttonsPos = ctrlPosition _buttonsCtrl;
_buttonsPos set [1, (_backGroundPos select 1) + (_backgroundPos select 3) - __GUI_GRIDY(1)];
_buttonsCtrl ctrlSetPosition _buttonsPos;
_buttonsCtrl ctrlcommit 0;



// -- Shows bottom options
if !(_options isEqualTo []) then {
	if (count _options > 3) then {
		_options resize 3;
	};

    private _codeDefault = [
        { ctrlDelete ([(_this select 0)] call MFUNC(getCtrlParentTop)); true },
        { true },
        { ctrlDelete ([(_this select 0)] call MFUNC(getCtrlParentTop)); true }
    ];

	{
		_x params [["_buttonText", ""], ["_buttonTooltip", ""], ["_buttonAction", _codeDefault select _forEachIndex]];
		private _buttonCtrl = CTRL(__IDC_BUTTON_1 + _forEachIndex);
		if (_buttonText == "") then {
			_buttonCtrl ctrlEnable false;
			_buttonCtrl ctrlShow false;
		} else {
			private _buttonActionParsed = [([_buttonAction] call MFUNC(parseToCode))] call MFUNC(codeToString);
			_buttonCtrl ctrlSetText _buttonText;
			_buttonCtrl ctrlShow true;
			_buttonCtrl ctrlEnable true;
			_buttonCtrl ctrlSetTooltip _buttonTooltip;
			_buttonCtrl ctrlSetEventHandler ["ButtonClick", _buttonActionParsed];
		};
	} forEach _options;
};

// -- Title
CTRL(__IDC_ELEMENT_1) ctrlSetText _title;
_popupCtrl;
