class CfgMarkerBrushes {
	// -- Type 1: 0%/10% rows
	class GVAR(FShaded1) {
		drawBorder = 1;
		name = "Forward shaded 1";
		scope = 1;
		texture = "\pr\frl\addons\main\markers\fshaded1_ca.paa";
	};

	class GVAR(BShaded1): GVAR(FShaded1) {
		name = "Backward shaded 1";
		texture = "\pr\frl\addons\main\markers\bshaded1_ca.paa";
	};
	class GVAR(VShaded1): GVAR(FShaded1) {
		name = "Vertical shaded 1";
		texture = "\pr\frl\addons\main\markers\vshaded1_ca.paa";
	};
	class GVAR(HShaded1): GVAR(FShaded1) {
		name = "Horizontal shaded 1";
		texture = "\pr\frl\addons\main\markers\hshaded1_ca.paa";
	};

	// -- Type 2: 10%/20% rows
	class GVAR(FShaded2) {
		drawBorder = 1;
		name = "Forward shaded 2";
		scope = 1;
		texture = "\pr\frl\addons\main\markers\fshaded2_ca.paa";
	};
	class GVAR(BShaded2): GVAR(FShaded2) {
		name = "Backward shaded 2";
		texture = "\pr\frl\addons\main\markers\bshaded2_ca.paa";
	};
	class GVAR(VShaded2): GVAR(FShaded2) {
		name = "Vertical shaded 2";
		texture = "\pr\frl\addons\main\markers\vshaded2_ca.paa";
	};
	class GVAR(HShaded2): GVAR(FShaded2) {
		name = "Horizontal shaded 2";
		texture = "\pr\frl\addons\main\markers\hshaded2_ca.paa";
	};

	class GVAR(DShaded2): GVAR(FShaded2) {
		name = "Diagnoal checkered Shaded";
		texture = "\pr\frl\addons\main\markers\dshaded2_ca.paa";
	};

	class GVAR(IslandShade1): GVAR(FShaded2) {
		drawBorder = 0;
		name = "Backward shade island covered";
		texture = "\pr\frl\addons\main\markers\island_shade_1.paa";
	};
	class GVAR(IslandShade2): GVAR(IslandShade1) {
		name = "Backward shade island bordered";
		texture = "\pr\frl\addons\main\markers\island_shade_2.paa";
	};
	class GVAR(IslandShade3): GVAR(IslandShade1) {
		name = "Backward shade island";
		texture = "\pr\frl\addons\main\markers\island_shade_3.paa";
	};
};

class cfgMarkerColors {
	class ColorGreen;
	class FRL_ColorGreen: ColorGreen {
		displayName = "Green (Bright)";
		color[] = {0, 0.8, 0, 1};
		scope = 1;
	};
};
