class CfgVehicles {
    class B_Soldier_base_F;
    class FRL_B_Soldier_F: B_Soldier_base_F {
      displayName = "Soldier";
      editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\I_soldier_F.jpg";
        //displayName = "$STR_a3_cfgvehicles_b_soldier_f0";
      fsmDanger = "";
      fsmFormation = "";
      scope = 2;
    	uniformClass = "";
    	weapons[] = {"Throw", "Put"};
    	respawnWeapons[] = {"Throw", "Put"};
    	Items[] = {};
    	RespawnItems[] = {};
    	magazines[] = {};
    	respawnMagazines[] = {};
    	linkedItems[] = {};
    	respawnLinkedItems[] = {};
    };

    class I_Soldier_base_F;
   	class FRL_I_Soldier_F: I_Soldier_base_F {
      displayName = "Soldier";
      editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\I_soldier_F.jpg";
      //displayName = "$STR_a3_cfgvehicles_b_soldier_f0";
      fsmDanger = "";
      fsmFormation = "";
      scope = 2;
    	uniformClass = "";
    	weapons[] = {"Throw", "Put"};
    	respawnWeapons[] = {"Throw", "Put"};
    	Items[] = {};
    	RespawnItems[] = {};
    	magazines[] = {};
    	respawnMagazines[] = {};
    	linkedItems[] = {};
    	respawnLinkedItems[] = {};
    };

    class O_Soldier_base_F;
   	class FRL_O_Soldier_F: O_Soldier_base_F {
      displayName = "Soldier";
      editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\I_soldier_F.jpg";
      //displayName = "$STR_a3_cfgvehicles_b_soldier_f0";
      fsmDanger = "";
      fsmFormation = "";
      scope = 2;
    	uniformClass = "";
    	weapons[] = {"Throw", "Put"};
    	respawnWeapons[] = {"Throw", "Put"};
    	Items[] = {};
    	RespawnItems[] = {};
    	magazines[] = {};
    	respawnMagazines[] = {};
    	linkedItems[] = {};
    	respawnLinkedItems[] = {};
    };
};
