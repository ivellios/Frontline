

class CfgDifficultyPresets {
	class Veteran {
		class Options;
	};

	class FRL_Frontline: Veteran {
		displayName = "Frontline - Default";
		optionDescription = "Recommended settings for Frontline mod.";
		optionPicture = "\A3\Ui_f\data\Logos\arma3_white_ca.paa";
		class Options: Options {
			// -- Simulation
			reducedDamage = 0;  // Reduced damage

			// -- Situational Awareness
			groupIndicators = 0; // Hexagons over group members
			friendlyTags = 0; // Rifleman (Adanteh) on friendly
			enemyTags = 0; // Rifleman (Gunther.S) on enemy
			detectedMines = 2; // Red triangle on mine detected
			commands = 2; // Waypoints?
			waypoints = 0; // Show waypoint in 3D (Shift click as well?)

			// -- Personal Awareness
			weaponInfo = 1; // Weapon hud
			stanceIndicator = 1;
			staminaBar = 1;
			weaponCrosshair = 0; // Crosshair
			visionAid = 0; // Spidey-sense (Dots on vehicles and side of screen)

			// -- View
			thirdPersonView = 0;
			cameraShake = 1;

			// -- Multiplayer
			scoreTable = 0; // P keybind scoredboard
			vonID = 1; // Name tag for voice
			deathMessages = 0;  // Killed by

			// -- Misc
			mapContent = 0; // Map markers for friendlies and enemies (Lol)
			autoReport = 0;  // Automatic reporting (Rifleman at grid 0202010 20km away 1 pixel while hiding in a bush spotted!)
			multipleSaves = 0; // Saves lel nope

			squadRadar = 0; // We got our own thing
		};
	};

	defaultPreset = "FRL_Frontline";
	myArmorCoef = 1;
	groupArmorCoef = 1;
	fadeDistanceStart = 20;
	fadeDistanceSpan = 10;
	recoilCoef = 1;
	visionAidCoef = 0;
	divingLimitMultiplier = 1;
	animSpeedCoef = 0;
	cancelThreshold = 0;
	showCadetHints = 1;
	showCadetWP = 1;
};
