#include "inc\resincl.hpp"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GRIDS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PYN 108
#define PX(X) ((X)/PYN*safeZoneH/(4/3))
#define PY(Y) ((Y)/PYN*safeZoneH)

#define pixelScale  0.50
#define GRID_W (pixelW * pixelGridNoUIScale * pixelScale)
#define GRID_H (pixelH * pixelGridNoUIScale * pixelScale)
#define CENTER_X    ((getResolution select 2) * 0.5 * pixelW)
#define CENTER_Y    ((getResolution select 3) * 0.5 * pixelH)
#define GRIDX(var1)         (var1 * (((safezoneW / safezoneH) min 1.2) / 40))
#define GRIDY(var1)         (var1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DIMENSIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// -- Generic
#define FRL_HEADER_HEIGHT PY(3)
#define FRL_HEADER_FONT FONTBOLT_ALT
#define FRL_BUTTON_HEIGHT GRIDY(1.5)
#define FRL_BUTTON_WIDTH GRIDX(14)
#define FRL_BUTTON_SPACING GRIDY(0.2)
#define FRL_BUTTON_DIALOG_WIDTH GRIDX(10)

// -- The dropdown menu for squad type selection
#define _SQUADSELECTOR_WIDTH_CONDITION GRIDX(9)
#define _SQUADSELECTOR_WIDTH_TYPE GRIDX(13)
#define _SQUADSELECTOR_ROWHEIGHT GRIDY(3)
#define _SQUADSELECTOR_SPACING_X GRIDX(0.2)
#define _SQUADSELECTOR_SPACING_Y GRIDY(0.2)


// -- What is this?
#define __H_HEADER 0
#define __H_CONTENTS PY(60)
#define __W PX(32)
#define __W_SETTING PX(28)
#define __H_SETTING PY(4)
#define __H (__H_HEADER + __H_CONTENTS)
#define __Y safeZoneY + 1.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + PY(1)
#define __X (safeZoneX + safeZoneW) - __W - PX(1)
#define __PADDING 1.5

// -- Mid screen notifcation dimensions
#define FRL_MAIN_NOTIFICATION_W (GRID_W * 80)
#define FRL_MAIN_NOTIFICATION_W_MAX (GRID_W * 180)
#define FRL_MAIN_NOTIFICATION_H (GRID_H * 7)
#define FRL_MAIN_NOTIFICATION_TEXTW 1.8

// -- Role Presets
#define PRESET_TAB_HEIGHT PY(2.5)
#define PRESET_WIDTH PX(40)
#define PRESET_HEIGHT PY(21)
#define PRESET_ITEM_SIZE 4
#define PRESET_ITEM_SPACING 0.2
#define PRESET_WEAPON_HEIGHT (2 * PY(PRESET_ITEM_SIZE) + PY(PRESET_ITEM_SPACING))
#define PRESET_WEAPON_WIDTH (2 * PX(PRESET_ITEM_SIZE) + PX(PRESET_ITEM_SPACING))
#define PRESET_ITEM_WIDTH PX(PRESET_ITEM_SIZE)
#define PRESET_ITEM_HEIGHT PY(PRESET_ITEM_SIZE)
#define PRESET_ITEM_SPACING_X PX(PRESET_ITEM_SPACING)
#define PRESET_ITEM_SPACING_Y PY(PRESET_ITEM_SPACING)
#define PRESET_ITEM_POS_X(var1) (PRESET_WIDTH - (var1 * PRESET_ITEM_WIDTH) - (var1 - 1) * PRESET_ITEM_SPACING_X)

#define WINDOW_W    120
#define WINDOW_H    180
#define WINDOW_HAbs (safezoneH min (WINDOW_H * GRID_H))
#define WINDOW_PREVIEW_W    WINDOW_W
#define WINDOW_PREVIEW_H    65
#define WINDOW_PREVIEW_HAbs (safezoneH min (WINDOW_PREVIEW_H * GRID_H))
#define ATTRIBUTE_TITLE_W   48
#define ATTRIBUTE_CONTENT_W 82
#define ATTRIBUTE_CONTENT_H 5
#define DIALOG_HEADER_H 10

// -- Progress bar hud top left
#define SIZE_WIDTH_PROGRESS 		GRIDX(9)
#define SIZE_HEIGHT_PROGRESS 		GRIDY(1.3)

#define SETTINGS_WINDOW_W PX(80)
#define SETTINGS_WINDOW_H PY(60)
#define SETTINGS_WINDOW_X (CENTER_X - SETTINGS_WINDOW_W * 0.5)
#define SETTINGS_WINDOW_Y (CENTER_Y - SETTINGS_WINDOW_H * 0.5)
#define SETTINGS_WINDOW_SPACING_X PX(1)
#define SETTINGS_WINDOW_CATEGORY_H PY(10)
#define SETTINGS_WINDOW_LINE_H PY(3)
#define SETTINGS_WINDOW_TEXT_W PX(12)
#define SETTINGS_WINDOW_SETTING_W (SETTINGS_WINDOW_W - 2*SETTINGS_WINDOW_TEXT_W)
#define SETTINGS_WINDOW_BUTTON_W PX(20)
#define SETTINGS_WINDOW_BUTTON_WIDE PX(40)
#define SETTINGS_WINDOW_LABEL_W PX(2)
#define SETTINGS_WINDOW_VALUE_W PX(10)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COLORS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define FRL_HEADER_FONT_COLOR {0.995, 0.714, 0.208, 1}
#define FRL_HEADER_BG_COLOR {0, 0, 0, 1}
#define FRL_WINDOW_BG_COLOR {"33/255", "32/255", "38/255", "1"}

#define COLOR_TEXT 			{1, 1, 1, 1}
#define COLOR_BAR     {"33/255", "32/255", "38/255", "1"}
#define COLOR_ACCENT        {0.995, 0.714, 0.208, 1}
#define COLOR_BACKGROUND    {"33/255", "32/255", "38/255", "1"}
#define COLOR_BACKGROUND_BOX    {0, 0, 0, 0.5}
#define COLOR_BACKGROUND_PROGRESS    {"54/255", "52/255", "62/255", "1"}

#define COLOR_BUTTON_BG {0, 0, 0, 0.5}
#define COLOR_BUTTON_BG_FOCUS {0.2, 0.2, 0.2, 0.5}
#define COLOR_BUTTON_BG_ACCENT {0.995, 0.714, 0.208, 1}
#define COLOR_BUTTON_BG_ACCENT_FOCUS {"254/255", "200/255", "80/255", "1"}
#define COLOR_BUTTON_TEXT {0.749, 0.749, 0.749, 1}
#define COLOR_BUTTON_TEXT_ACCENT {0, 0, 0, 1}
#define COLOR_BUTTON_TEXT_DISABLED {0.25, 0.25, 0.25, 1}

#define __COLOR_TITLE_TEXT {1, 1, 1, 1}
#define __COLOR_BACKGROUND_SIDEBAR {46/255, 46/255, 46/255, 1}
#define __COLOR_BACKGROUND_CONTENT {43/255, 43/255, 43/255, 1}
#define __COLOR_BACKGROUND_ALT {88/255, 88/255, 88/255, 0.25}
#define __COLOR_BACKGROUND_HEADER {0.078, 0.078, 0.078, 1}
#define __COLOR_BACKGROUND_RESIZE {1, 0.2, 0, 0.5}
#define __COLOR_SELECT_ACCENT {0.957, 0.612, 0.110, 1}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonts
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define SIZEEX_PURISTA(SIZEPX)		__EVAL([SIZEPX,1.8,[6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34,35,37,46]] call _fnc_sizeEx)
#define SIZEEX_ETELKA(SIZEPX)		__EVAL([SIZEPX,1.55,[6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34,35,37,46]] call _fnc_sizeEx)
#define SIZEEX_TAHOMA(SIZEPX)		__EVAL([SIZEPX,1.6,[16]] call _fnc_sizeEx)
#define SIZEEX_LUCIDA(SIZEPX)		__EVAL([SIZEPX,1.6,[8,11]] call _fnc_sizeEx)

#define SIZEEX_XS	3.0
#define SIZEEX_S	4.0
#define SIZEEX_M	4.5
#define SIZEEX_L	5.0
#define SIZEEX_XL	6.0

#define SIZE_XS	3.5
#define SIZE_S	4
#define SIZE_M	5
#define SIZE_L	5
#define SIZE_XL	6


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonts
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define FONT_NORMAL         "RobotoCondensedLight"
#define FONT_SEMIBOLD           "RobotoCondensed"
#define FONT_BOLD           "RobotoCondensedBold"
#define FONT_THIN           "RobotoCondensedLight"
#define FONT2_NORMAL            "PuristaMedium"
#define FONT2_BOLD          "PuristaSemiBold"
#define FONT2_THIN          "PuristaLight"
#define FONT_MONO           "EtelkaMonospacePro"
#define FONT_NARROW         "EtelkaNarrowMediumPro"
#define FONT_CODE           "LucidaConsoleB"
#define FONT_SYSTEM         "TahomaB"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Color
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define COLOR_TEXT_RGB	1,	1,	1
#define COLOR_TEXT_RGBA	COLOR_TEXT_RGB, 1

#define COLOR_BACKGROUND_RGB	0.2,	0.2,	0.2
#define COLOR_BACKGROUND_RGBA	COLOR_BACKGROUND_RGB, 1

#define COLOR_TAB_RGB	0.1,	0.1,	0.1
#define COLOR_TAB_RGBA	COLOR_TAB_RGB, 1

#define COLOR_OVERLAY_RGB	0,	0,	0
#define COLOR_OVERLAY_RGBA	COLOR_OVERLAY_RGB, 0.5

//#define COLOR_ACTIVE_RGB	1,	0.5,	0
//#define COLOR_ACTIVE_RGBA	COLOR_ACTIVE_RGB, 1

#define COLOR_ACTIVE_RGB_R	"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])"
#define COLOR_ACTIVE_RGB_G	"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])"
#define COLOR_ACTIVE_RGB_B	"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])"
#define COLOR_ACTIVE_RGB_A	1
#define COLOR_ACTIVE_RGB	COLOR_ACTIVE_RGB_R,	COLOR_ACTIVE_RGB_G,	COLOR_ACTIVE_RGB_B
#define COLOR_ACTIVE_RGBA	COLOR_ACTIVE_RGB,COLOR_ACTIVE_RGB_A

#define COLOR_HIGHLIGHT_RGB	0,	1,	1
#define COLOR_HIGHLIGHT_RGBA	COLOR_HIGHLIGHT_RGB, 1

#define COLOR_POSX_RGB	0.77,	0.18,	0.1
#define COLOR_POSX_RGBA	COLOR_POSX_RGB, 1

#define COLOR_POSY_RGB	0.58,	0.82,	0.22
#define COLOR_POSY_RGBA	COLOR_POSY_RGB, 1

#define COLOR_POSZ_RGB	0.26,	0.52,	0.92
#define COLOR_POSZ_RGBA	COLOR_POSZ_RGB, 1

#define COLOR_NOTE_DEFAULT_RGB	0.12549,	0.505882,	0.313726
#define COLOR_NOTE_DEFAULT_RGBA	COLOR_NOTE_DEFAULT_RGB,	0.9

#define COLOR_NOTE_WARNING_RGB	0.8,		0.35,		0
#define COLOR_NOTE_WARNING_RGBA	COLOR_NOTE_WARNING_RGB,	0.9

#define COLOR_NOTE_ERROR_RGB	0.815686,	0.266667,	0.215686
#define COLOR_NOTE_ERROR_RGBA	COLOR_NOTE_ERROR_RGB,	0.9

//--- BLUFOR
#define COLOR_WEST_R		"(profilenamespace getvariable ['Map_BLUFOR_R',0])"
#define COLOR_WEST_G		"(profilenamespace getvariable ['Map_BLUFOR_G',1])"
#define COLOR_WEST_B		"(profilenamespace getvariable ['Map_BLUFOR_B',1])"
#define COLOR_WEST_A		"(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"
#define COLOR_WEST_RGB		COLOR_WEST_R,	COLOR_WEST_G,	COLOR_WEST_B
#define COLOR_WEST_RGBA		COLOR_WEST_RGB,COLOR_WEST_A

//--- OPFOR
#define COLOR_EAST_R		"(profilenamespace getvariable ['Map_OPFOR_R',0])"
#define COLOR_EAST_G		"(profilenamespace getvariable ['Map_OPFOR_G',1])"
#define COLOR_EAST_B		"(profilenamespace getvariable ['Map_OPFOR_B',1])"
#define COLOR_EAST_A		"(profilenamespace getvariable ['Map_OPFOR_A',0.8])"
#define COLOR_EAST_RGB		COLOR_EAST_R,	COLOR_EAST_G,	COLOR_EAST_B
#define COLOR_EAST_RGBA		COLOR_EAST_RGB,COLOR_EAST_A

//--- Independent
#define COLOR_GUER_R		"(profilenamespace getvariable ['Map_Independent_R',0])"
#define COLOR_GUER_G		"(profilenamespace getvariable ['Map_Independent_G',1])"
#define COLOR_GUER_B		"(profilenamespace getvariable ['Map_Independent_B',1])"
#define COLOR_GUER_A		"(profilenamespace getvariable ['Map_Independent_A',0.8])"
#define COLOR_GUER_RGB		COLOR_GUER_R,	COLOR_GUER_G,	COLOR_GUER_B
#define COLOR_GUER_RGBA		COLOR_GUER_RGB,COLOR_GUER_A

//--- Civilian
#define COLOR_CIV_R		"(profilenamespace getvariable ['Map_Civilian_R',0])"
#define COLOR_CIV_G		"(profilenamespace getvariable ['Map_Civilian_G',1])"
#define COLOR_CIV_B		"(profilenamespace getvariable ['Map_Civilian_B',1])"
#define COLOR_CIV_A		"(profilenamespace getvariable ['Map_Civilian_A',0.8])"
#define COLOR_CIV_RGB		COLOR_CIV_R,	COLOR_CIV_G,	COLOR_CIV_B
#define COLOR_CIV_RGBA		COLOR_CIV_RGB,COLOR_CIV_A

//--- Unknown
#define COLOR_EMPTY_R		"(profilenamespace getvariable ['Map_Unknown_R',0])"
#define COLOR_EMPTY_G		"(profilenamespace getvariable ['Map_Unknown_G',1])"
#define COLOR_EMPTY_B		"(profilenamespace getvariable ['Map_Unknown_B',1])"
#define COLOR_EMPTY_A		"(profilenamespace getvariable ['Map_Unknown_A',0.8])"
#define COLOR_EMPTY_RGB		COLOR_EMPTY_R,	COLOR_EMPTY_G,	COLOR_EMPTY_B
#define COLOR_EMPTY_RGBA	COLOR_EMPTY_RGB,COLOR_EMPTY_A

//--- Ambient
#define COLOR_AMBIENT_RGB	0,	1,	0.5
#define COLOR_AMBIENT_RGBA	COLOR_AMBIENT_RGB,1

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CHECKBOX_TEXTURE(UNCHECKED,CHECKED)\
	textureChecked = CHECKED;\
	textureUnchecked = UNCHECKED;\
	textureFocusedChecked = CHECKED;\
	textureFocusedUnchecked = UNCHECKED;\
	textureHoverChecked = CHECKED;\
	textureHoverUnchecked = UNCHECKED;\
	texturePressedChecked = CHECKED;\
	texturePressedUnchecked = UNCHECKED;\
	textureDisabledChecked = CHECKED;\
	textureDisabledUnchecked = UNCHECKED;

///////////////////////////////////////////////////////////////
// Convert RGB or RGBA color to hexadecimal
// Examples:
// 	RGBATOHEX(1,0,1,1)
// 	returns "#ffff00ff"
//
// 	RGBTOHEX(1,0,1)
// 	returns "#ff00ff"
///////////////////////////////////////////////////////////////
#define RGBATOHEX(COLOR_R,COLOR_G,COLOR_B,COLOR_A)	__EVAL([COLOR_R,COLOR_G,COLOR_B,COLOR_A] call _fnc_colorRGBAtoHEX)
#define RGBTOHEX(COLOR_R,COLOR_G,COLOR_B)		__EVAL([COLOR_R,COLOR_G,COLOR_B] call _fnc_colorRGBAtoHEX)
#define VARTOHEX					__EVAL(_color call _fnc_colorRGBAtoHEX)


///////////////////////////////////////////////////////////////
// Convert hexadecimal color to RGB or RGBA
// Example:
// 	HEXTORGBA("#ffff00ff")
// 	returns 1,0,1,1
//
// 	HEXTORGB("#ff00ff")
// 	returns 1,0,1
///////////////////////////////////////////////////////////////
#define HEXTORGBA(COLOR_RGBA)\
	__EVAL(COLOR_RGBA call _fnc_colorHEXtoRGBA_2),\
	__EVAL(COLOR_RGBA call _fnc_colorHEXtoRGBA_4),\
	__EVAL(COLOR_RGBA call _fnc_colorHEXtoRGBA_6),\
	__EVAL(COLOR_RGBA call _fnc_colorHEXtoRGBA_0)

#define HEXTORGB(COLOR_RGB)\
	__EVAL(COLOR_RGB call _fnc_colorHEXtoRGBA_0),\
	__EVAL(COLOR_RGB call _fnc_colorHEXtoRGBA_2),\
	__EVAL(COLOR_RGB call _fnc_colorHEXtoRGBA_4)


///////////////////////////////////////////////////////////////
// Convert RGBA in range [0-255] to [0-1] used by the game config
// Example:
// 	RGBA255to1(255,0,255,255)
// 	returns 1,0,1,1
///////////////////////////////////////////////////////////////
#define RGBA255to1(COLOR_R,COLOR_G,COLOR_B,COLOR_A)\
	__EVAL(COLOR_R * 0.00392157),\
	__EVAL(COLOR_G * 0.00392157),\
	__EVAL(COLOR_B * 0.00392157),\
	__EVAL(COLOR_A * 0.00392157)


///////////////////////////////////////////////////////////////
/// Scripts
///////////////////////////////////////////////////////////////

#define INIT_DISPLAY_FUNCTION	(uinamespace getvariable 'BIS_fnc_initDisplay')
#define INIT_DISPLAY_INTERNAL	scriptIsInternal = 1;

//--- Code executed on each display where the macro is used. Scripts are pre-compiled at the game start by BIS_fnc_initDisplays
#define INIT_DISPLAY(NAME,PATH) \
	scriptName = ##NAME##;\
	scriptPath = ##PATH##;\
	onLoad = ["onLoad",_this,#NAME,'##PATH##'] call INIT_DISPLAY_FUNCTION; \
	onUnload = ["onUnload",_this,#NAME,'##PATH##'] call INIT_DISPLAY_FUNCTION;

//--- Special version of the macro for initial loading screen which initializes Functions
#define INIT_DISPLAY_START(NAME,PATH) \
	scriptName = ##NAME##;\
	scriptPath = ##PATH##;\
	onLoad = "[2] call compile preprocessfilelinenumbers gettext (configfile >> 'CfgFunctions' >> 'init'); ['onLoad',_this,'RscDisplayLoading','Loading'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";\
	onUnload = ["onUnload",_this,#NAME,'##PATH##'] call INIT_DISPLAY_FUNCTION;

#define INIT_CONTROL(NAME,PATH) \
	scriptName = ##NAME##;\
	scriptPath = ##PATH##;\
	onLoad = ["onLoad",_this,#NAME,'##PATH##',false] call INIT_DISPLAY_FUNCTION; \
	onUnload = ["onUnload",_this,#NAME,'##PATH##',false] call INIT_DISPLAY_FUNCTION;
