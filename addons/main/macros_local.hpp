#include "modInfo.hpp"

// -- Frontline specific -- //
#define VAR(x) private x
#define MEDIAPATH QUOTE(PATH\PREFIX\SUBPREFIX\client\ui\media\)

#define DOUBLE(var1,var2) var1##_##var2
#define TRIPLE(var1,var2,var3) DOUBLE(var1,DOUBLE(var2,var3))
#define QUOTE(var) #var

#define BASEPATH pr\frl\addons
#define ICONPATH BASEPATH\main\icons
#define PATHTO(var1) QUOTE(BASEPATH\MODULE\var1)
#define ICON(varCategory, varName) QUOTE(ICONPATH\varCategory\varName.paa)
#define TEXTURE(varName) QUOTE(BASEPATH\MODULE\data\varName)
#define MTEXTURE(varName) QUOTE(BASEPATH\Main\data\varName)

#ifndef MODULE
    #define MODULE Core
#endif

#define ADDON DOUBLE(PREFIX,MODULE)
#define ADDONNAME QUOTE(ObjectPlacement - MODULE)
#define QADDON QUOTE(ADDON)

// -- Module macros
#define EFUNC(var1,var2) TRIPLE(PREFIX,var1,DOUBLE(fnc,var2))
#define QEFUNC(var1,var2) QUOTE(EFUNC(var1,var2))
#define EGVAR(var1,var2) TRIPLE(PREFIX,var1,var2)
#define QEGVAR(var1,var2) QUOTE(EGVAR(var1,var2))

// Module vars
#define FUNC(var1) EFUNC(MODULE,var1)
#define QFUNC(var1) QUOTE(FUNC(var1))
#define DFUNC(var1) EFUNC(MODULE,var1)
#define GVAR(var1) EGVAR(MODULE,var1)
#define QGVAR(var1) QEGVAR(MODULE,var1)

// -- Core (library) variables
#define CVAR(var1) DOUBLE(Clib,var1)
#define QCVAR(var1) QUOTE(CVAR(var1))
#define CFUNC(var1) TRIPLE(Clib,fnc,var1)
#define QCFUNC(var1) QUOTE(CFUNC(var1))

// -- Main variables
#define MVAR(var1) EGVAR(Main,var1)
#define QMVAR(var1) QEGVAR(Main,var1)
#define MFUNC(var1) EFUNC(Main,var1)
#define QMFUNC(var1) QUOTE(MFUNC(var1))

// -- Short variables (No module name, use for easy subclasses)
#define SVAR(var1) DOUBLE(PREFIX,var1)
#define QSVAR(var1) QUOTE(SVAR(var1))

#define RANDOMOFFSET(var1,var2) (var1 - (var2 / 2) + (random var2))

// -- Localization -- //
#define LSTRING(var1) localize QUOTE(TRIPLE(STR,ADDON,var1))
#define ELSTRING(var1,var2) QUOTE(TRIPLE(STR,DOUBLE(PREFIX,var1),var2))
#define CSTRING(var1) QUOTE(TRIPLE($STR,ADDON,var1))
#define ECSTRING(var1,var2) QUOTE(TRIPLE($STR,DOUBLE(PREFIX,var1),var2))
#define LSTR(var1) QUOTE(TRIPLE($STR,ADDON,var1))

#include "macros_ui.hpp"

#ifdef __INCLUDE_DIK
    #include "functions\keybinds\definedikcodes.hpp"
#endif

#ifdef __INCLUDE_IDCS
    #include "ui\macros_ui2.hpp"
    #include "ui\macros_settings.hpp"
#endif

// -- Script macros
#define MODULELOADED(var1) ((toLower QUOTE(var1)) in (uiNamespace getVariable [QCVAR(loadedModules), []]))
#define LOCALIZE(var1) [var1, QUOTE(PREFIX), QUOTE(LANGUAGE)] call MFUNC(localize)
#define RETURN(var1) var1 breakOut (_fnc_scriptName + "_main")
#define ISFRIEND(var1) (side group var1 getFriend playerSide > 0.6)
