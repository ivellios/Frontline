/*
    Function:       FRL_main_fnc_blurBackground
    Author:         Adanteh
    Description:    Adds a stacked Blurred background
*/

#include "macros.hpp"

if (!hasInterface) exitWith {};

params ["_id", ["_show", false], ["_commit", 0.25], ["_strength", 0.9], ["_blackWhite", true]];

if (_show isEqualType 0) then {
    _show = _show == 1;
};

if (isNil QGVAR(blurCollection)) then {
    GVAR(blurCollection) = [];
};

if (_show) then {
    //diag_log ["[FRL] BLUR #1"];
    private _new = (GVAR(blurCollection) pushBackUnique _id) != -1;
    if (isNil QGVAR(blurCollectionPP)) then {
        //diag_log ["[FRL] BLUR #2"];
        GVAR(blurCollectionPP) = ppEffectCreate ["DynamicBlur", 102];
        GVAR(blurCollectionPP) ppEffectEnable true;
        if !(isNil "Clib_fnc_localEvent") then {
            ["fadeHUD", [0.7, _commit]] call Clib_fnc_localEvent;
        };
    };
    GVAR(blurCollectionPP) ppEffectAdjust [_strength];
    GVAR(blurCollectionPP) ppEffectCommit _commit;
} else {
    GVAR(blurCollection) = GVAR(blurCollection) - [_id];
    //diag_log ["[FRL] BLUR #3"];
    if (GVAR(blurCollection) isEqualTo []) then {
        //diag_log ["[FRL] BLUR #4"];
        if (!isNil QGVAR(blurCollectionPP)) then {
            GVAR(blurCollectionPP) ppEffectAdjust [0];
            GVAR(blurCollectionPP) ppEffectCommit _commit;
            //diag_log ["[FRL] BLUR #5"];
            if !(isNil "Clib_fnc_localEvent") then {
                ["fadeHUD", [0, _commit]] call Clib_fnc_localEvent;
            };
            _commit spawn {
                uiSleep _this;
                //diag_log ["[FRL] BLUR #6"];
                if !(GVAR(blurCollection) isEqualTo []) exitWith { };
                ppEffectDestroy GVAR(blurCollectionPP);
                GVAR(blurCollectionPP) = nil;
            };
        };
    };
};

if (_blackWhite) then {
    [_id, _show, _commit] call FUNC(blackWhiteBackground);
};
