/*
    Function:       FRL_Main_fnc_EndMission
    Author:         Adanteh
    Description:    A passthrough function for BIS_fnc_endMission, but sets some aditional vars for scripted subtitles
*/
#include "macros.hpp"

#define IDC_RSCDISPLAYDEBRIEFING_TITLE			20600
#define IDC_RSCDISPLAYDEBRIEFING_SUBTITLE		20601
#define IDC_RSCDISPLAYDEBRIEFING_DESCRIPTIONTEXT	20693
#define DISP_DEBRIEFING (uiNamespace getVariable ["RscDisplayDebriefing", displayNull])

params ["_template", "_winner", "_subtitle", ["_description", ""]];

private _params = [nil, _subtitle, _description];
uiNamespace setVariable ["endScreenParams", _params];
[_template, _winner] call BIS_fnc_endMission;

private _display = uiNamespace getVariable [QEGVAR(RespawnUI,respawnDisplay), displayNull];
if !(isNull _display) then {
    _display closeDisplay 2;
};

["EndMission"] call CFUNC(localEvent);

// -- This is a silly workaround for not being allowed to override the normal debriefing script
// -- Overwriting the paths to make sure your script gets loaded in does absolutely nothign somehow
[{
    params ["_template"];
    private _endTemplate = [["CfgDebriefing", _template]] call bis_fnc_loadClass;
    private _scriptedTemplate = uiNamespace getVariable ["endScreenParams", []];
    _scriptedTemplate params [
        ["_titleText", gettext (_endTemplate >> "title")],
        ["_subtitleText", gettext (_endTemplate >> "subtitle")],
        ["_descriptionText", gettext (_endTemplate >> "description")]
    ];

    private _display = (uiNamespace getVariable ["RscDisplayDebriefing", displayNull]);
    _title = _display displayctrl IDC_RSCDISPLAYDEBRIEFING_TITLE;
    _subtitle = _display displayctrl IDC_RSCDISPLAYDEBRIEFING_SUBTITLE;
    _description =_display displayctrl IDC_RSCDISPLAYDEBRIEFING_DESCRIPTIONTEXT;


    _title ctrlSetText toUpper _titleText;
    _subtitle ctrlsettext toupper _subtitleText;
    _description ctrlsetstructuredtext parsetext _descriptionText;

}, {
    (ctrlText (DISP_DEBRIEFING displayCtrl IDC_RSCDISPLAYDEBRIEFING_TITLE) != "")
}, [_template]] call CFUNC(waitUntil);
