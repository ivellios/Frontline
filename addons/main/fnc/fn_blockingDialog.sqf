/*
    Function:       FRL_main_fnc_blockingDialog
    Author:         Adanteh
    Description:    Adds a little invisble dialog that prevents you from shooting/driving
    Example:        [true] call FRL_main_fnc_blockingDialog;
*/

#include "macros.hpp"
#define DISP (uiNamespace getVariable [QGVAR(blockingDialog), displayNull])
params ["_toggle"];

if (_toggle) then {
    if (!isNull DISP) exitWith { };

    createDialog QGVAR(blockingDialog);
    private _dialog = DISP;

    _dialog displayAddEventHandler ["KeyDown", {
        params ["", "_button"];
        if (_button in actionKeys "ShowMap") then {
            DISP closeDisplay 0;
            openMap true;
        };
        false;
    }];

    GVAR(blockingDialogPFH) = [{
        if (isNull DISP && {!visibleMap && (isNull findDisplay 49) && (isNull findDisplay 312) && (isNull findDisplay 632)}) then {
            [GVAR(blockingDialogPFH)] call MFUNC(removePerFrameHandler);
            [true] call FUNC(blockingDialog);
        };
    }, 0, []] call MFUNC(addPerFramehandler);

} else {
    DISP closeDisplay 0;
    [GVAR(blockingDialogPFH)] call MFUNC(removePerFrameHandler);
};
