/*
    Function:       FRL_main_fnc_blackWhiteBackground
    Author:         Adanteh
    Description:    Adds a stacked BW background
*/

#include "macros.hpp"

if (!hasInterface) exitWith {};

params ["_id", ["_show", false], ["_commit", 0.25]];

if (_show isEqualType 0) then {
    _show = _show == 1;
};

if (isNil QGVAR(bwCollection)) then {
    GVAR(bwCollection) = [];
};

if (_show) then {
    private _new = (GVAR(bwCollection) pushBackUnique _id) != -1;
    if (isNil QGVAR(bwCollectionPP)) then {
        private _data = [0.8,0.5,0,[1, 1, 1,0],[1,1,1,0],[0.75,0.25,0,0.1]];
        GVAR(bwCollectionPP) = ppEffectCreate ["ColorCorrections", 2220+6];
        GVAR(bwCollectionPP) ppEffectAdjust _data;
        GVAR(bwCollectionPP) ppEffectCommit _commit;
        GVAR(bwCollectionPP) ppEffectEnable true;
    };
} else {
    GVAR(bwCollection) = GVAR(bwCollection) - [_id];
    if (GVAR(bwCollection) isEqualTo []) then {
        if (!isNil QGVAR(bwCollectionPP)) then {
            GVAR(bwCollectionPP) ppEffectAdjust [1,1,0,[0,0,0,0],[1,1,1,1],[0,0,0,0]]; // Default
            GVAR(bwCollectionPP) ppEffectCommit _commit;
            _commit spawn {
                uiSleep _this;
                if !(GVAR(bwCollection) isEqualTo []) exitWith { };
                ppEffectDestroy GVAR(bwCollectionPP);
                GVAR(bwCollectionPP) = nil;
            };
        };
    };
};
