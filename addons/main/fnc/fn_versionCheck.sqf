#include "macros.hpp"

/*
 *	File: fn_versionCheck.sqf
 *	Author: Adanteh
 *	Do important version checks for the required mods.
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(versionCheck);
 */

//with uiNamespace do {

disableSerialization;

params [["_mode", "prestart"], ["_args", []]];

diag_log ["[FRL VersionCheck] ", _this];

private _return = true;

switch (toLower _mode) do {
	private _isServerMod = getNumber (configFile >> "CfgPatches" >> "FRL_Server" >> "isServer") >= 1;

	// -- Check the version of client and compare it to server -- //
	case "prestart": {
		private _modVersion = getText (configFile >> "CfgPatches" >> "FRL_Main" >> "VersionStr");
		if (_modVersion == "") then { "1.0"; };
		diag_log ["[FRL VersionCheck] Version: ", _modVersion];
		uiNamespace setVariable [QGVAR(modVersion), _modVersion];
	};

	case "menu": {
		// -- Load current mod version from the amazing world-wide web -- //
		_isServerMod = false;
		if (_isServerMod) then {
			(_args select 0) ctrlShow false;
		} else {
			private _file = "versioncheck.php?";
			private _input = format ["version=%1", (uiNamespace getVariable [QGVAR(modVersion), ""])];
			private _url = toLower format ["http://frontline-mod.com/ctrlhtml/%1%2", _file, _input];
			(_args select 0) htmlLoad _url;
		};
	};

	case "apexcheck": { // -- Do a html request with our state of Apex, so we know which % of players has Apex
		private _hasApex = ["no", "yes"] select (395180 in (getDLCs 1));
		(_args select 0) htmlLoad (toLower format ["http://launcher.frontline-mod.com/apex/%1.html", _hasApex]);
	};

	// -- Check version when connecting to the server
	case "preinit": {
		if (is3den) exitWith { };
		if (isServer) then {
			private _version = getText (configFile >> "CfgPatches" >> "FRL_Main" >> "VersionStr");
			if (_version == "") then { _version = "1.0"; };
			diag_log ["[FRL VersionCheck] Setting server version to: ", _version];
			missionNamespace setVariable [QGVAR(versionSrv), _version];
			[] spawn { publicVariable QGVAR(versionSrv); }; // -- publicVariable doesn't work in preInit unless you spawn it.
		};
	};

	case "postinit": {
		if (is3den) exitWith { };
		if !(isServer) then {
			if (isNil QGVAR(versionSrv)) exitWith {
				_return = true;
			};

			_versionLocal = uiNamespace getVariable [QGVAR(modVersion), "1.0"];
			if !(_versionLocal isEqualTo GVAR(versionSrv)) then {
				["updaterequired", [_versionLocal, GVAR(versionSrv)]] call FUNC(versionCheck);
				_return = false;
			} else {
				_return = true;
			};
		};
	};

	case "updaterequired": {
		_args spawn {
			disableserialization;
			waitUntil { ! isNull ([] call bis_fnc_displayMission); };
			params ["_versionLocal", "_versionServer"];
			_msg = parseText format ["<t>Version mismatch: <br/><br/>[%1] - Server<br/>[%2] - You<br/><br/><br/>Grab the latest version of the mod on discord! Without the latest version you can't play.</t>", _versionServer, _versionLocal, "http://www.frontline-mod.com/dl"];
			private _cancel = [_msg, "WRONG MOD VERSION", false, "EXIT", ([] call bis_fnc_displayMission), false, true] call bis_fnc_guiMessage;

			([] call bis_fnc_displayMission) closeDisplay 2;
		};

		// -- Auto-close in case user is a slowpoke with clicking buton -- //
		private _nil = [] spawn {
			disableserialization;
			uiSleep 25; if !(isNull ([] call bis_fnc_displayMission)) then { ([] call bis_fnc_displayMission) closeDisplay 2; };
		};
	};
};

_return;

//};
