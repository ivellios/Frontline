/*
    Function:       FRL_Main_fnc_loadWebInfo
    Author:         Adanteh
    Description:    This will create a hidden html control and parse the text
	Example:		["load", (findDisplay 12), "df.php?", "version=01", false] call FRL_Main_fnc_loadWebInfo
*/
#include "macros.hpp"

params ["_mode", ["_args", []], ["_silent", true]];

diag_log ["[FRL] LoadWebInfo", _this];

switch (toLower _mode) do {
	case "load": {
		_args params ["_display", "_file", ["_input", ""], ["_cache", false], ["_processCode", {}]];

		private _url = toLower format ["http://frontline-mod.com/ctrlhtml/%1%2", _file, _input];
		private _html = _display ctrlCreate ["RscHTML", -1];
		_html ctrlSetBackgroundColor [0,0,0,0.8];
		_html ctrlSetPosition [safeZoneX + (safeZoneW / 2) - 0.15, safeZoneY + (safeZoneH / 5), 0.3, 0.1];
		_html ctrlCommit 0;
		_html htmlLoad _url;
	};
};
