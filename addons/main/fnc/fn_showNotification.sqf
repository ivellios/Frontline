#include "macros.hpp"

/*
 *	File: fn_showNotification.sqf
 *	Author: Adanteh
 *	Shows a small notification on midscreen. Used to give more clear feedback on actions the player uses directly, without the need to move eyes to corner of screen
 * 	Should be used conservatively and only for things that the player has an active role in using
 *
 *	Example:
 *	["show", ["Hello!", 3, nil, { alive player }]] call FUNC(showNotification);
 */

// -----------
#define CTRL(x) ((uiNamespace getVariable QGVAR(NotificationMid)) displayCtrl x)
#define __TIMESHOWN 7
#define __SOUND "defaultNotification"

params ["_mode", ["_params", []]];

private _return = false;
switch (toLower _mode) do {
	case "init": {
		GVAR(notificationInit) = true;
		GVAR(notificationQueue) = [];
		GVAR(notificationIndex) = 0;
		GVAR(notificationActive) = false;
	};

	// -- TODO: Add a category option. Adding a new option with same category as an existing one, will replace it.
	case "show": {
		_params params ["_text", ["_timeShown", __TIMESHOWN], ["_sound", __SOUND], ["_condition", { alive player }]];
		if (isNil QGVAR(notificationInit)) then {
			["init"] call FUNC(showNotification);
		};

		if (isNull (uiNamespace getVariable [QGVAR(NotificationMid), displayNull])) then {
			QGVAR(NotificationMid) cutRsc [QGVAR(NotificationMid), "PLAIN", 0, true];
		};
		GVAR(notificationQueue) pushBack [GVAR(notificationIndex), _text, _timeShown, _sound, _condition];
		GVAR(notificationIndex) = GVAR(notificationIndex) + 1;
		_return = GVAR(notificationIndex);
		if !(GVAR(notificationActive)) then {
			GVAR(notificationActive) = true;
			["loop"] call FUNC(showNotification);
		};
	};

	case "loop": {
		[] spawn {
			while { count GVAR(notificationQueue) > 0 } do {
				(GVAR(notificationQueue) deleteAt 0) params ["_id", "_text", "_timeShown", "_sound", "_condition"];

				// -- Run some code to see if we should show this queued notification -- //
				if (call _condition) then {
					private _currentPos = ctrlPosition CTRL(1);
					private _textW = (GRID_W * (count _text * FRL_MAIN_NOTIFICATION_TEXTW)) + (GRID_W * 10);
					CTRL(1) ctrlSetPosition [CENTER_X - (_textW / 2), _currentPos select 1, _textW, 0];
					CTRL(3) ctrlSetPosition [0, 0, _textW, FRL_MAIN_NOTIFICATION_H];
					CTRL(1) ctrlCommit 0;
					CTRL(3) ctrlCommit 0;
					CTRL(1) ctrlSetPosition [CENTER_X - (_textW / 2), _currentPos select 1, _textW, FRL_MAIN_NOTIFICATION_H];
					CTRL(1) ctrlCommit 0.1;
					CTRL(3) ctrlSetText _text;
					playSound _sound;

					sleep _timeShown;

					CTRL(1) ctrlSetPosition [CENTER_X - (_textW / 2), _currentPos select 1, _textW, 0];
					CTRL(1) ctrlCommit 0.1;

					sleep 0.1;
				};
			};

			GVAR(notificationActive) = false;
			QGVAR(NotificationMid) cutText ["", "PLAIN"];
		};
	};
};

_return;
