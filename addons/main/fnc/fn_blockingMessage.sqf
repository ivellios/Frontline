/*
    Function:       FRL_main_fnc_blockingMessage
    Author:         Adanteh
    Description:    Shows full black screen with a message in the middle
    Example:        ['test', true, "Testing the message<br />Second line!"] call FRL_main_fnc_blockingMessage
*/

#include "macros.hpp"
#define CTRL(var1) ((uiNamespace getVariable QGVAR(blockingMessage)) displayCtrl var1)

if (!hasInterface) exitWith {};

params ["_id", ["_show", false], ["_text", ""], ["_timeout", -1]];

if (_show) then {
    private _layerID = (QGVAR(blockingMessage) + _id);
    _layerID cutRsc [QGVAR(blockingMessage), "BLACK", 0, true];
    CTRL(1) ctrlSetStructuredText parseText _text;
    if (_timeout != -1) then {
        [_layerID, _timeout] spawn {
            params ["_layerID", "_timeout"];
            sleep _timeout;
            _layerID cutText ["", "Plain"];
        };
    };
} else {
    (QGVAR(blockingMessage) + _id) cutText ["", "Plain"];
};
