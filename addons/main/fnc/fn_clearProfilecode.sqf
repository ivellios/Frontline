/*
 *	File: fn_clearProfilecode.sqf
 *	Author: Adanteh
 *	This is some code to clear out profileNamespace entries that get call compiled by BIS, which allow for full code execution.
 *
 */

private _save = false;
{
	if ((profileNamespace getVariable [_x, 0]) isEqualType "") then {
		profileNamespace setVariable [_x, 0.4];
		_save = true;
	};
} forEach ['igui_bcg_rgb_a','igui_bcg_rgb_r','igui_bcg_rgb_g','igui_bcg_rgb_b','igui_grid_mission_x','igui_grid_mission_y','igui_grid_mission_w','igui_grid_mission_h'];

if (_save) then {
	saveProfileNamespace;
};
