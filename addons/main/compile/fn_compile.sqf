/*
    Function:       FRL_Main_fnc_compile
    Author:         Adanteh
    Description:    Compiles local module function, also see if it needs queing for automagic execution
    Example:        ["\pr\frl\addons\medical\treatment\ui\fn_updateInteractUI.sqf", "frl_medical_fnc_updateInteractUI"] call FRL_Main_fnc_compile
*/
#include "macros.hpp"

params [["_functionPath", "", [""]], ["_functionVarName", "", [""]], ["_module", ""], ["_allowImport", true], ["_functionName", ""]];

#define SCRIPTHEADER "\
private _fnc_scriptNameParent = if (isNil '_fnc_scriptName') then {\
    '%1'\
} else {\
    _fnc_scriptName\
};\
private _fnc_scriptName = '%1';\
private _fnc_scriptNameShort = '%2';\
scriptName _fnc_scriptName;\
scopeName (_fnc_scriptName + '_Main');\
\
"

private _header = format [SCRIPTHEADER, _functionVarName, _functionName];


private _cached = false;
#ifndef ISDEV
    private _cachedFunction = parsingNamespace getVariable _functionVarName;
    private _fncCode = if (isNil "_cachedFunction") then {
        private _header = format [SCRIPTHEADER, _functionVarName];
        private _funcString = _header + preprocessFileLineNumbers _functionPath;
        diag_log text format ["[%1] Compiling: %2 - %3", QUOTE(PREFIX), _functionVarName, diag_tickTime];
        compileFinal _funcString;
    } else {
        _cached = true;
        _cachedFunction
    };
#else
    private _funcString = _header + preprocessFileLineNumbers _functionPath;
    diag_log text format ["[%1] Compiling: %2 - %3", QUOTE(PREFIX), _functionVarName, diag_tickTime];
    private _fncCode = compile _funcString;
#endif

{
    if (!_cached || { isNil { _x getVariable _functionVarName }}) then {
        _x setVariable [_functionVarName, _fncCode];
    };
    nil
} count [missionNamespace, uiNamespace, parsingNamespace];

// -- Queue for automatic execution based on filenames
[_functionVarName, _module] call (uiNamespace getVariable QFUNC(moduleQueue));

// -- This is used to allow create extra varnames for accessing this function under a different prefix
if (_allowImport) then {
    private _allFunctions = uiNamespace getVariable [QGVAR(compiled), []];
    _allFunctions pushBackUnique (toLower _functionVarName);
    uiNamespace setVariable [QGVAR(compiled), _allFunctions];
};
