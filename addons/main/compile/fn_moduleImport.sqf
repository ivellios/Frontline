/*
    Function:       FRL_Main_fnc_moduleImport
    Author:         Adanteh
    Description:    Gets all functions starting with given prefix, then makes them available under another prefix (Aka ADA_toolBounding_fnc_whatever >> ADA_object_fnc_whatever)
    Example:        ["ADA_toolBounding", "ADA_Object"] call FRL_Main_fnc_moduleImport
*/
#include "macros.hpp"

params ["_importFrom", "_importTo"];

_importFrom = toLower _importFrom;
_importTo = toLower _importTo;

private _imported = [];
private _allCompiled = uiNamespace getVariable [QGVAR(compiled), []];

// -- Go through all compiled functions
{
    if ((_x find _importFrom) == 0) then {
        _code = compile format ["_this call %1", _x];
        _functionName = _x select [count _importFrom, count _x - 1]; // -- Select the part '_fnc_whatever'
        _target = _importTo + _functionName; // -- Add the target prefix
        _imported pushBack _x;
        {
            _x setVariable [_target, _code];
        } count [missionNamespace, uiNamespace, parsingNamespace];
        diag_log text format ["[%1] Mapping: %2 to %3 - %4", QUOTE(PREFIX), _x, _target, diag_tickTime];
    };
} forEach _allCompiled;

// -- Return the function names we've imported
_imported;
