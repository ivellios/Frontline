/*
    Function:       FRL_Main_fnc_recompileModuleBox
    Author:         Adanteh
    Description:    Executes the onLoad code for box on Esc menu that recompiles selected module
*/
#include "macros.hpp"

disableSerialization;
params ["_mode", "_args"];

//systemChat str ["Recompile", _this, _fnc_scriptName];

#ifdef ISDEV

switch (toLower _mode) do {
    // -- UI element is loaded (Esc menu opened)
    case "load": {
        _args params ["_dropDown"];

        if (isNil QGVAR(recompileModuleSelected)) then {
            ["init"] call (missionNamespace getVariable _fnc_scriptName);
        };

        GVAR(recompileModuleList) = [];
        private _modulesLoaded = + (missionNamespace getVariable [QGVAR(loadedModules), []]);
        _modulesLoaded append (parsingNamespace getVariable ["clib_core_allModuleNamesCached", []]);
        if (_modulesLoaded isEqualTo []) exitWith { };

        _modulesLoaded sort true;

        lbClear _dropDown;
        //_dropDown ctrlSetEventHandler ['LBSelChanged', format ["systemChat 'test';"]];
        //_dropDown ctrlSetEventHandler ['MouseButtonClick', format ["systemChat 'click';"]];

        // -- Add the options
        // -- if an existing option was last selected, reselect that so you don't need to constantly reselect on menu reopen
        private _oldSelectionIndex = -1;
        {
            private _lbIndex = _dropDown lbAdd _x;
            _dropDown lbSetData [_lbIndex, _x];
            GVAR(recompileModuleList) pushBack _x;
            if (GVAR(recompileModuleSelected) == _x) then {
                _oldSelectionIndex = _forEachIndex;
            };
        } forEach _modulesLoaded;
        if (_oldSelectionIndex != -1) then {
            _dropDown lbsetCurSel _oldSelectionIndex;
        };
    };

    case "init": {
        GVAR(recompileModuleSelected) = profileNamespace getVariable [QGVAR(recompileModuleSelected), "-"];
    };

    // -- Selection for the dropdown box is changed
    case "select": {
        _args params ["", "_newSelection"];
        GVAR(recompileModuleSelected) = GVAR(recompileModuleList) select _newSelection;
        profileNamespace setVariable [QGVAR(recompileModuleSelected), GVAR(recompileModuleSelected)];
        saveProfileNamespace;
    };

    // -- Recompile button is pressed
    case "recompile": {
        [GVAR(recompileModuleSelected), true] call FUNC(moduleLoad);
    };
};

#endif
