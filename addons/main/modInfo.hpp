#define MOD server

#define PATH pr
#define PREFIX FRL
#define SUBPREFIX addons
#define FUNCTIONPREFIX FRL

#define AUTHOR_STR  "Adanteh"
#define AUTHOR_ARR  {"Adanteh"}
#define AUTHOR_URL  "http://www.frontline-mod.com"

#define FONTLIGHT   "RobotoCondensedLight"
#define FONTMED     "RobotoCondensed"
#define FONTBOLD    "RobotoCondensedBold"
#define FONTREG     FONTMED
#define FONTLIGHT_ALT   "NexaLight"
#define FONTMED_ALT     "RobotoCondensed"
#define FONTBOLD_ALT    "NexaBold"

#define REQUIRED_VERSION 1.80

#define MAJOR 1
#define MINOR 3
#define PATCHLVL 34
#define BUILD 0

#define VERSION MAJOR.MINOR.PATCHLVL.BUILD
#define VERSION_AR MAJOR,MINOR,PATCHLVL,BUILD

#include "ISDEV.hpp"
