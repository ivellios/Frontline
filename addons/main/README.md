## Contents

Includes local very basic core functions, that don't require anything of the server,
plus some generic config changes that need to happen client-side.
