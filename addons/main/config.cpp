#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {
            "A3_Data_F_Loadorder",
            "A3_Data_F_Curator_Loadorder",
            "A3_Data_F_Kart_Loadorder",
            "A3_Data_F_Bootcamp_Loadorder",
            "A3_Data_F_Heli_Loadorder",
            "A3_Data_F_Mark_Loadorder",
            "A3_Data_F_Exp_A_Loadorder",
            "A3_Data_F_Exp_B_Loadorder",
            "A3_Data_F_Exp_Loadorder",
            "A3_Data_F_Jets_Loadorder",
            "A3_Data_F_Argo_Loadorder",
            "A3_Data_F_Patrol_Loadorder",
            "A3_Data_F_Orange_Loadorder",
            "A3_Data_F_Tacops_Loadorder",
            "FRL_Server"
        };

        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class CfgFunctions {
    class ADDON {
        tag = "FRL_Main";
        #include "cfgFunctions.hpp"
    };
};

#include "CfgDifficultyPresets.hpp"
#include "CfgVoice.hpp"
#include "CfgMarkers.hpp"
#include "CfgVehicles.hpp"

class CfgWeapons {
    class ItemCore;
    class FRL_ItemCore: ItemCore {
        type = 4096;
        detectRange = -1;
        simulation = "ItemMineDetector";
    };
};

class cfgCommands {
   allowedHTMLLoadURIs[] += {
      "http://frontline-mod.com/ctrlhtml/*",
      "http://launcher.frontline-mod.com/*"
   };
};

class FRL {
    #include "localModules.hpp"
};

#include "ui\ui.hpp"

class CfgScriptPaths {
    FRL_MainUI = "pr\frl\addons\main\ui\scripts\";
};


/*
class CfgVideoOptions {
    class PPRadialBlur {
        minValue = 1;
    };
    class OverallSettings {
        class DefaultSettings;
        class VeryLow: DefaultSettings {
            ppRadialBlur = 1;
        };
    };
};
*/

class CfgDebriefing {
    class FRL_WINNER {
        title = "Victory";
        description = "Your team won!";
        subtitle = ".";
        pictureBackground = "";
        picture = "";
        pictureColor[] = {0.0,0.3,0.6,1};
    };
    class FRL_LOOSER {
        title = "Defeat";
        description = "Your team lost!";
        subtitle = ".";
        pictureBackground = "";
        picture = "";
        pictureColor[] = {0.0,0.3,0.6,1};
    };
};
