#include "macros_ui2.hpp"

class RscStructuredText;
class ctrlControlsGroupNoHScrollbars;
class ctrlControlsGroupNoScrollbars;
class ctrlControlsGroup;
class ctrlStaticBackground;
class ctrlStaticPictureKeepAspect;
class ctrlStaticPicture;
class ctrlStatic;
class ctrlStaticTitle;
class ctrlStructuredText;
class ctrlButton;
class ctrlCheckbox;
class ctrlXSliderH;
class ctrlEdit;
class ctrlCombo;
class ctrlToolbox;
class GVAR(RscStructuredText): RscStructuredText {
  x = 0;
	y = 0;
	h = 0.035;
	w = 0.1;
	text = "";
	size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[] = {1,1,1,1};
	shadow = 1;
	class Attributes {
        font = "RobotoCondensed";
        color = "#ffffff";
        colorLink = "#D09B43";
        align = "left";
        shadow = 2;
        shadowColor = "#222222";
	};
};

#include "customclasses.hpp"

#include "controls\default.hpp"
#include "controls\settingsCategory.hpp"
#include "controls\button.hpp"
#include "controls\checkbox.hpp"
#include "controls\slider.hpp"
#include "controls\textboxButton.hpp"
#include "controls\valueset.hpp"
#include "controls\playerselector.hpp"
#include "controls\combo.hpp"
#include "controls\keybind.hpp"
#include "controls\toolbox.hpp"
#include "controls\CtrlNumberEdit.hpp"
#include "controls\CtrlTextEdit.hpp"

#include "RscBlockingDialog.hpp"
#include "RscEmptyDialog.hpp"

#include "settingswindow.hpp"
#include "settingspopup.hpp"

#include "notifications\CtrlNotification.hpp"
#include "notifications\CtrlNotificationGroup.hpp"
#include "notifications\CtrlTooltip.hpp"

#include "popups\CtrlPopupDialog.hpp"
#include "popups\CtrlPopupDialogTextEdit.hpp"
class RscTitles {
  // -- THIS DOESN'T WORK. Tried everything, but old script path is still loaded in
  // #include "ui\RscMissionEnd.hpp"
  #include "RscBlockingMessage.hpp"
  #include "notifications\CtrlNotificationDisplay.hpp"
  #include "holdaction\CtrlHoldActionDisplay.hpp"
};
