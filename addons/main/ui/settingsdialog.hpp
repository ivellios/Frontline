#include "controls\default.hpp"
#include "controls\settingsCategory.hpp"
#include "controls\button.hpp"
#include "controls\checkbox.hpp"
#include "controls\slider.hpp"
#include "controls\textboxButton.hpp"
#include "controls\valueset.hpp"
#include "controls\playerselector.hpp"
#include "controls\keybind.hpp"

class GVAR(window) {
    idd = 115;
    enableSimulation = 1;
    enableDisplay = 1;
    movingEnable = 1;

    onLoad = "['settingsDialog', true, 0.5, 1.5] call FRL_Main_fnc_blurBackground; uiNamespace setVariable ['FRL_Main_window', _this select 0]; ['load'] call FRL_Main_fnc_settingsWindow;";
    onUnload = "['settingsDialog', false] call FRL_Main_fnc_blurBackground; ['unload'] call FRL_Main_fnc_settingsWindow;";

    class ControlsBackground {
        class Background: ctrlStatic {
            x = safeZoneX;
            y = safeZoneY;
            w = safeZoneW;
            h = safeZoneH;
            colorBackground[] = {0.1,0.1,0.1,0};
            fade = 1;
            onLoad = "(_this select 0) ctrlSetBackgroundColor [0.1,0.1,0.1,0.3]; (_this select 0) ctrlSetFade 0; (_this select 0) ctrlCommit 0.7;";
            onUnload = "";
        };
    };

    class Controls {
      class MainWindow: ctrlControlsGroupNoHScrollbars {
        x = __GUI_SETTING_X;
        y = __GUI_SETTING_Y;
        w = __GUI_SETTING_W;
        h = __GUI_SETTING_H;
        class Controls {
          class Title: GVAR(DialogTitle) {
              idc = 4000;
              x = 0;
              y = 0;
              w = __GUI_SETTING_W;
              h = __GUI_SETTING_HEADER;
              moving = 1;
              text = "SETTINGS";
          };

          class Background: ctrlStaticBackground {
              colorBackground[] = __COLOR_BACKGROUND_BOX;
              x = 0;
              y = __GUI_SETTING_HEADER;
              w = __GUI_SETTING_W;
              h = __GUI_SETTING_H - __GUI_SETTING_HEADER;
          };

          class AttributeCategories: ctrlControlsGroupNoHScrollbars {
            idc = 4001;
            x = 0;
            y = __GUI_SETTING_HEADER + __GUI_SETTING_SPACING_X;
            w = __GUI_SETTING_W;
            h = __GUI_SETTING_H - __GUI_SETTING_HEADER - 2*__GUI_SETTING_SPACING_X - __GUI_SETTING_LINE_H;
            class Controls {
              //--- Categories will be placed here using AttributeCategory template
            };

          };
          class ButtonOK: GVAR(ButtonAccent) {
            default = 1;
            idc = 1;
            text = "OK";
            y = __GUI_SETTING_H - __GUI_SETTING_LINE_H;
            onButtonDown = "ctrlsetfocus (_this select 0);"; //--- Some values are saved only when they lose focus (e.g., slider's numerical value)
          };
        };
      };
    };
};
