class GVAR(CtrlTextEdit): GVAR(CtrlSetting) {
    colorMouseoverScroll[] = {__COLOR_EDIT_BG, __COLOR_BUTTON_BG_ACCENT}; // -- Used for settingsNumberEditScroll

	class Controls: Controls {
		class Title: Title {};

		class Value: ctrlEdit {
			idc = 100;
			tooltip = "";
            text = "0";
            font = __MONO;
            x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
			w = __GUI_SETTING_BUTTON_WIDE;
			h = __GUI_SETTING_LINE_H;
            colorBackground[] = __COLOR_EDIT_BG;
            colorText[] = {0, 0, 0, 1};
		};
	};
};
