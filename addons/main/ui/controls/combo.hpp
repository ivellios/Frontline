class GVAR(CtrlCombo): GVAR(CtrlSetting) {
	class Controls: Controls {
		class Title: Title {};
		class Button: ctrlCombo {
			idc = 101;
			font = __FONT;
            x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
			w = __GUI_SETTING_BUTTON_WIDE;
			h = __GUI_SETTING_LINE_H;
		};
	};
};
