class GVAR(CtrlCheckbox): GVAR(CtrlSetting) {
	class Controls: Controls {
		class Title: Title{};
		class Value: ctrlCheckbox {
			idc = 100;
			textureChecked = __GUI_SETTING_CHECKED;
			textureUnchecked = __GUI_SETTING_UNCHECKED;
			textureFocusedChecked = __GUI_SETTING_CHECKED;
			textureFocusedUnchecked = __GUI_SETTING_UNCHECKED;
			textureHoverChecked = __GUI_SETTING_CHECKED;
			textureHoverUnchecked = __GUI_SETTING_UNCHECKED;
			texturePressedChecked = __GUI_SETTING_CHECKED;
			texturePressedUnchecked = __GUI_SETTING_UNCHECKED;
			textureDisabledChecked = __GUI_SETTING_CHECKED;
			textureDisabledUnchecked = __GUI_SETTING_UNCHECKED;
			colorBackground[] = {0, 0, 0, 0};
			x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
			w = __GUI_SETTING_CHECKBOX_W;
			h = __GUI_SETTING_LINE_H;
		};
	};
};
