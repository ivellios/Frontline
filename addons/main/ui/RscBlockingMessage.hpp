
#define _BLOCKING_WIDTH PX(40)
#define _BLOCKING_HEIGHT PY(40)
class GVAR(blockingMessage) {
	idd = -1;
	duration = 1e11;
	name = QGVAR(blockingMessage);
	onLoad = "uiNamespace setVariable [""frl_main_blockingMessage"", _this select 0]";

	class ControlsBackground {
    class MessageArea : RscStructuredText {
      idc = 1;
      text = "";
      x = 0.5 - _BLOCKING_WIDTH/2;
      y = 0.5 - _BLOCKING_HEIGHT/2;
      w = _BLOCKING_WIDTH;
      h = _BLOCKING_HEIGHT;
    };
	};
};
