
class GVAR(blockingDialog) {
	idd = -1;
	duration = 1e11;
	name = QGVAR(blockingDialog);
	onLoad = "uiNamespace setVariable [""frl_main_blockingDialog"", _this select 0]";
	movingEnable = 0;

  class controlsBackground {
    class Background {
        idc = -1;
        moving = 0;
        font = "TahomaB";
        text = "";
        sizeEx = 0;
        lineSpacing = 0;
        access = 0;
        type = 0;
        style = 0;
        size = 1;
        colorBackground[] = {0, 0, 0, 0};//0.5
        colorText[] = {0, 0, 0, 0};
        x = "safezoneX";
        y = "safezoneY";
        w = "safezoneW";
        h = "safezoneH";
    };
  };
};
