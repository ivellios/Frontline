class SVAR(CtrlPopupDialogTextEdit): SVAR(CtrlPopupDialog) {
    x = CENTER_X - __GUI_GRIDX(6.25);
    y = safeZoneY + safeZoneH * 0.3;
    w = __GUI_GRIDX(12.5);
    h = __GUI_GRIDY(2.75); // -- This is the height if text height were to be 0

    class Controls {
        class Title: ctrlStaticTitle {
            idc = __IDC_ELEMENT_1;
            sizeEx = __DEFAULT;
            shadow = 0;
            x = 0;
            y = 0;
            w = __GUI_GRIDX(12.5);
            h = __GUI_GRIDY(1);
            moving = 1;
            text = "Hello";
            colorBackground[] = __COLOR_BACKGROUND_HEADER;
            colorText[] = __COLOR_TITLE_TEXT;
        };

        class Background: ctrlStaticBackground {
            idc = __IDC_ELEMENT_2;
            x = 0;
            y = __GUI_GRIDY(1);
            w = __GUI_GRIDX(12.5);
            h = __GUI_GRIDY(1.25);
        };

        class TextEdit: ctrlEdit {
            idc = __IDC_ELEMENT_5;
            x = __GUI_GRIDX(1.25);
            y = __GUI_GRIDY(1.25);
            w = __GUI_GRIDX(10);
            h = __GUI_GRIDY(1);
            colorBackground[] = __COLOR_BACKGROUND_SIDEBAR;
            colorBorder[] = {0, 0, 0, 0};
        };

        class TextField: ctrlStructuredText {
            idc = __IDC_ELEMENT_3;
            x = 0;
            y = __GUI_GRIDX(3.75);
            w = __GUI_GRIDX(12.5);
            h = __GUI_GRIDY(0);
            class Attributes {
                shadow = 0;
                font = __FONT;
                size = 0.8;
            };
        };

        class ButtonControlsGroup: ctrlControlsGroupNoScrollbars {
            idc = __IDC_ELEMENT_4;
            x = 0;
            y = __GUI_GRIDY(1.25);
            w = __GUI_GRIDX(12.5);
            h = __GUI_GRIDY(1);
            class controls {
                class Button1BG: ctrlStaticBackground {
                    x = 0;
                    y = 0;
                    w = __GUI_GRIDX(4);
                    h = __GUI_GRIDY(1);
                    colorBackground[] = __COLOR_BACKGROUND_CONTENT;
                };

                class Button1: ctrlButton {
                    default = 0;
                    idc = __IDC_BUTTON_1;
                    x = 0;
                    y = 0;
                    w = __GUI_GRIDX(4);
                    h = __GUI_GRIDY(1);
                    shortcuts[] = {"0x00050000 + 2"};
                    text = "$STR_DISP_OK";
                    OnButtonClick = "closeDialog 1;";
                    style = 0xC0;
                    colorBackground[] = __COLOR_BUTTON_BG;
                    colorBackgroundDisabled[] = __COLOR_BUTTON_BG;
                    colorBackgroundActive[] = __COLOR_BUTTON_BG_FOCUS;
                    colorFocused[] = __COLOR_BUTTON_BG_FOCUS;
                    colorText[] = __COLOR_BUTTON_TEXT;
                    colorDisabled[] = __COLOR_BUTTON_TEXT_DISABLED;
                    font = __FONT;
                    offsetX = 0;
                    offsetY = 0;
                    offsetPressedX = 0;
                    offsetPressedY = 0;
                };

                class Button2BG: Button1BG {
                    x = __GUI_GRIDX(4.25);
                };

                class Button2: Button1 {
                    default = 0;
                    idc = __IDC_BUTTON_2;
                    x = __GUI_GRIDX(4.25);
                    w = __GUI_GRIDX(4);
                    text = "$STR_A3_RscDisplayOptionsLayout_ButtonSave";
                    OnButtonClick = "closeDialog 0;";
                };

                class Button3BG: Button1BG {
                    x = __GUI_GRIDX(8.5);
                };

                class Button3: Button1 {
                    default = 1;
                    idc = __IDC_BUTTON_3;
                    x = __GUI_GRIDX(8.5);
                    w = __GUI_GRIDX(4);
                    shortcuts[] = {"0x00050000 + 1"};
                    text = "$STR_DISP_CANCEL";
                    OnButtonClick = "closeDialog 2;";
                };
            };
        };
    };
};
