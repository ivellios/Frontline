// -- Settings window
#define __GUI_SETTING_W __GUI_GRIDX(50)
#define __GUI_SETTING_H __GUI_GRIDY(30)
#define __GUI_SETTING_HEADER __GUI_GRIDx(1.25)
#define __GUI_SETTING_X (CENTER_X - __GUI_SETTING_W * 0.5)
#define __GUI_SETTING_Y (CENTER_Y - __GUI_SETTING_H * 0.5)
#define __GUI_SETTING_BUTTON_W __GUI_GRIDX(4)
#define __GUI_SETTING_BUTTON_WIDE __GUI_GRIDX(12)
#define __GUI_SETTING_CATEGORY_H __GUI_GRIDY(2)
#define __GUI_SETTING_SPACING_X __GUI_GRIDX(1)
#define __GUI_SETTING_SPACING_Y __GUI_GRIDY(0.25)
#define __GUI_SETTING_TEXT_W __GUI_GRIDX(7.5)
#define __GUI_SETTING_TEXT_H __GUI_GRIDY(1)
#define __GUI_SETTING_TEXT_TITLE_H __GUI_GRIDY(1.5)
#define __GUI_SETTING_SETTING_W (__GUI_SETTING_W - 2*__GUI_SETTING_TEXT_W)
#define __GUI_SETTING_CHECKBOX_W __GUI_GRIDX(6)
#define __GUI_SETTING_LINE_H __GUI_GRIDY(1.5)
#define __GUI_SETTING_LABEL_W __GUI_GRIDX(2)

/* -- Base color
#define __COLOR_BUTTON_BG {0.275, 0.275, 0.275, 1}
#define __COLOR_BUTTON_BG_FOCUS {0.2, 0.2, 0.2, 0.5}
#define __COLOR_BUTTON_BG_ACCENT {0.957, 0.612, 0.110, 1}
#define __COLOR_BUTTON_BG_ACCENT_FOCUS {"254/255", "200/255", "80/255", "1"}
#define __COLOR_BUTTON_TEXT {0.749, 0.749, 0.749, 1}
#define __COLOR_BUTTON_TEXT_ACCENT {0, 0, 0, 1}
#define __COLOR_BUTTON_TEXT_DISABLED {0.25, 0.25, 0.25, 1}
*/

#define __COLOR_BUTTON_BG {0, 0, 0, 0.5}
#define __COLOR_BUTTON_BG_FOCUS {0.2, 0.2, 0.2, 0.5}
#define __COLOR_BUTTON_BG_ACCENT {0.995, 0.714, 0.208, 1}
#define __COLOR_BUTTON_BG_ACCENT_FOCUS {"254/255", "200/255", "80/255", "1"}
#define __COLOR_BUTTON_TEXT {0.749, 0.749, 0.749, 1}
#define __COLOR_BUTTON_TEXT_ACCENT {0, 0, 0, 1}
#define __COLOR_BUTTON_TEXT_DISABLED {0.25, 0.25, 0.25, 1}
#define __COLOR_LIST_BG {0.250, 0.270, 0.316, 1}
#define __COLOR_EDIT_BG {0.560, 0.596, 0.635, 1}
#define __COLOR_BACKGROUND_BOX {0, 0, 0, 0.5}

#define __COLOR_SETTING_TEXT {1, 1, 1, 1}
#define __COLOR_SETTING_TEXT_ACCENT {0.996, 0.784, 0.314, 1}

#define __COLOR_TOOLBUTTON {0.5, 0.5, 0.5, 0.7}
#define __COLOR_TOOLBUTTON_HOVERBG {0.5, 0.5, 0.5, 0.1}
#define __COLOR_TOOLBUTTON_HIGHLIGHT {1, 1, 1, 1}

/*
#define __GUI_SETTING_CHECKED ICON(toggle,check_box)
#define __GUI_SETTING_UNCHECKED ICON(toggle,check_box_outline_blank)
*/
#define __GUI_SETTING_CHECKED "\pr\frl\addons\interface\img\checkbox\checkbox_checked.paa"
#define __GUI_SETTING_UNCHECKED "\pr\frl\addons\interface\img\checkbox\checkbox_unchecked.paa"

#define __FONTLIGHT_ALT "NexaLight"
#define __FONTMED_ALT "RobotoCondensed"
#define __FONTBOLD_ALT "NexaBold"
#define __FONT_TITLE "NexaBold"
