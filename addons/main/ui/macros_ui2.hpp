#define GUI_GRID_X	(0.028)
#define GUI_GRID_Y	(0.043)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

#define __GRID_X (0.032)
#define __GRID_Y (0.040)
#define __PADDING_X (0.01)
#define __PADDING_Y (0.01)

// MessageBox styles
#define MB_BUTTON_OK      1
#define MB_BUTTON_CANCEL  2
#define MB_BUTTON_USER    4

#define __LARGE 0.04
#define __SMALL 0.02
#define __DEFAULT 0.03
#define __FONT "RobotoCondensed"
#define __FONT_LIGHT "RobotoCondensedLight"
#define __MONO "RobotoCondensed"

#define pixelScale 0.5
#define GRID_W (pixelW * pixelGrid * pixelScale)
#define GRID_H (pixelH * pixelGrid * pixelScale)
#define CENTER_X	((getResolution select 2) * 0.5 * pixelW)
#define CENTER_Y	((getResolution select 3) * 0.5 * pixelH)

#define __GUI_GRIDX(var1) ((GRID_W * 4) * var1)
#define __GUI_GRIDY(var1) ((GRID_H * 4) * var1)

// -- Toolbar
#define GRID_TOOLBAR_X(var1) (var1 * (5 * GRID_W))
#define GRID_TOOLBAR_Y(var1) (var1 * (5 * GRID_W))

#define GRID_TOGGLES_X(var1) (var1 * (5 * GRID_W))
#define GRID_TOGGLES_Y(var1) (var1 * (5 * GRID_W))

// Variables
#define __GUI_VIEW_IDC 14001
#define __GUI_MAP_IDC 14002
#define __GUI_WINDOW (uiNamespace getVariable ['ADA_Interface', displayNull])
#define __GUI_VIEWPORT (__GUI_WINDOW displayCtrl __GUI_VIEW_IDC)
#define __GUI_MAP (__GUI_WINDOW displayCtrl __GUI_MAP_IDC)

// SIZES
#define __GUI_PANE_W (80 * GRID_W)
#define __GUI_PANE_HEADER_H __GUI_GRIDY(1.25)
#define __GUI_PANE_BUTTON_H __GUI_GRIDY(1.25)
#define __GUI_PANE_BUTTON_W __GUI_GRIDX(1.25)
#define __GUI_PANE_BUTTONWIDE_W __GUI_GRIDX(4)
#define __GUI_PANE_CONTENT_X (2.5 * GRID_W)
#define __GUI_PANE_CONTENT_Y (2.5 * GRID_H)
#define __GUI_PANE_CONTENT_W (__GUI_PANE_W - (2 * __GUI_PANE_CONTENT_X))
#define __GUI_PANE_RESIZE_W __GUI_GRIDX(1)
#define __GUI_PANE_RESIZE_H __GUI_GRIDY(1)

#define __GUI_MENUBAR_H (safeZoneH * 0.02)
#define __GUI_TOGGLES_H (GRID_TOGGLES_Y(2.2))

// IDCs
#define __IDC_TOOLBARBUTTON 161001
#define __IDC_TOOLBAR 162000
#define __IDC_TOOLBARGROUP 162001
#define __IDC_TOOLBARSETTINGSGROUP 162002
#define __IDC_TOOLBARFOCUS 162003

#define __IDC_TOGGLESBUTTON 151001
#define __IDC_TOGGLES 152000
#define __IDC_TOGGLESGROUP 152001
#define __IDC_TOGGLESSETTINGSGROUP 152002
#define __IDC_TOGGLESFOCUS 152003
#define __IDC_TOGGLESBG 152004

#define __IDC_PANE_HEADER 10
#define __IDC_PANE_HEADER_BG 11
#define __IDC_PANE_HEADER_TOGGLE 12
#define __IDC_PANE_HEADER_TEXT 13
#define __IDC_PANE_HEADER_HANDLE 14
#define __IDC_PANE_HEADER_CLOSE 15
#define __IDC_PANE_HEADER_HELP 16
#define __IDC_PANE_CONTENT 20
#define __IDC_PANE_CONTENT_BG 21
#define __IDC_PANE_CONTENT_RESIZE 22

#define __IDC_PANE_BASEIDC 10000
#define __IDC_PANE_IDC 100
#define __IDC_SIDEBAR_BASEIDC 100000
#define __IDC_SIDEBAR_BG 200000

#define __IDC_COREGROUP 40
#define __IDC_OVERLAYGROUP 41
#define __IDC_RESIZER 42

// IDC.
#define __IDC_BUTTON_1 170902
#define __IDC_BUTTON_2 170903
#define __IDC_BUTTON_3 170904
#define __IDC_BUTTON_4 170905
#define __IDC_BUTTON_5 170906
#define __IDC_BUTTON_6 170907
#define __IDC_ELEMENT_1 172001
#define __IDC_ELEMENT_2 172002
#define __IDC_ELEMENT_3 172003
#define __IDC_ELEMENT_4 172004
#define __IDC_ELEMENT_5 172005
#define __IDC_ELEMENT_6 172006

// COLORS
#define __COLOR_TITLE_TEXT {1, 1, 1, 1}
#define __COLOR_BACKGROUND_SIDEBAR {46/255, 46/255, 46/255, 1}
#define __COLOR_BACKGROUND_CONTENT {43/255, 43/255, 43/255, 1}
#define __COLOR_BACKGROUND_ALT {88/255, 88/255, 88/255, 0.25}
#define __COLOR_BACKGROUND_HEADER {0.078, 0.078, 0.078, 1}
#define __COLOR_BACKGROUND_RESIZE {1, 0.2, 0, 0.5}
#define __COLOR_SELECT_ACCENT {0.957, 0.612, 0.110, 1}
