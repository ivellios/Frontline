class GVAR(SettingsWindow) {
    idd = 115;
    enableSimulation = 1;
    enableDisplay = 1;
    movingEnable = 1;

    onLoad = "uiNamespace setVariable ['FRL_Main_SettingsWindow', _this select 0]; ['load'] call FRL_Main_fnc_settingsWindow;";
    onUnload = "['unload'] call FRL_Main_fnc_settingsWindow;";

    class ControlsBackground {};

    class Controls {
      class MainWindow: ctrlControlsGroupNoHScrollbars {
        idc = 116;
        x = __GUI_SETTING_X;
        y = __GUI_SETTING_Y;
        w = __GUI_SETTING_W;
        h = __GUI_SETTING_H;
        class Controls {
          class Title: GVAR(DialogTitle) {
              idc = 4000;
              x = 0;
              y = 0;
              w = __GUI_SETTING_W;
              h = __GUI_PANE_HEADER_H;
              moving = 1;
              text = "SETTINGS";
          };

          class Background: ctrlStaticBackground {
              colorBackground[] = __COLOR_BACKGROUND_SIDEBAR;
              x = 0;
              y = __GUI_PANE_HEADER_H;
              w = __GUI_SETTING_W;
              h = __GUI_SETTING_H - __GUI_PANE_HEADER_H;
          };

          class AttributeCategories: ctrlControlsGroupNoHScrollbars {
            idc = 4001;
            x = 0;
            y = __GUI_PANE_HEADER_H + __GUI_SETTING_SPACING_X;
            w = __GUI_SETTING_W;
            h = __GUI_SETTING_H - __GUI_PANE_HEADER_H - 2*__GUI_SETTING_SPACING_X - __GUI_SETTING_LINE_H;
            class Controls {
              //--- Categories will be placed here using AttributeCategory template
            };

          };
          class ButtonOK: GVAR(ButtonAccent) {
                default = 1;
                idc = 1;
                text = "OK";
                y = __GUI_SETTING_H - __GUI_SETTING_LINE_H;
                onButtonDown = "ctrlsetfocus (_this select 0);"; //--- Some values are saved only when they lose focus (e.g., slider's numerical value)
          };
        };
      };
    };
};
