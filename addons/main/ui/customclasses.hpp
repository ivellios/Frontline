class GVAR(ButtonMain): ctrlButton {
  style = 0xC0;
  colorBackground[] = __COLOR_BUTTON_BG;
  colorBackgroundDisabled[] = __COLOR_BUTTON_BG;
  colorBackgroundActive[] = __COLOR_BUTTON_BG_FOCUS;
  colorFocused[] = __COLOR_BUTTON_BG_FOCUS;
  colorText[] = __COLOR_BUTTON_TEXT;
  colorDisabled[] = __COLOR_BUTTON_TEXT_DISABLED;
  font = __FONTLIGHT_ALT;
  w = __GUI_SETTING_BUTTON_W;
  h = __GUI_SETTING_LINE_H;
  offsetX = 0;
  offsetY = 0;
  offsetPressedX = 0;
  offsetPressedY = 0;
};

class GVAR(ButtonAccent): GVAR(ButtonMain) {
  colorBackground[] = __COLOR_BUTTON_BG_ACCENT;
  colorBackgroundDisabled[] = __COLOR_BUTTON_BG_ACCENT;
  colorBackgroundActive[] = __COLOR_BUTTON_BG_ACCENT_FOCUS;
  colorFocused[] = __COLOR_BUTTON_BG_ACCENT_FOCUS;
  colorText[] = __COLOR_BUTTON_TEXT_ACCENT;
  colorDisabled[] = __COLOR_BUTTON_TEXT_DISABLED;
};

class GVAR(DialogTitle): ctrlStaticTitle {
  colorBackground[] = __COLOR_BACKGROUND_HEADER;
  colorText[] = __COLOR_SETTING_TEXT_ACCENT;
  font = __FONT_TITLE;
  sizeEx = 5 * GRID_H;
};
