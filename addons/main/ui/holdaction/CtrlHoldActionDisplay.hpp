#define __WIDTH (GRID_W * 20)

class SVAR(CtrlHoldActionDisplay) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_CtrlHoldActionDisplay', _this select 0];";
    fadeIn = 0;
    fadeOut = 0;
    class Controls {
        class ProgressPicture: ctrlStaticPictureKeepAspect {
            idc = 1;
            x = safeZoneX + (safeZoneW * 0.5) - (__WIDTH / 2);
            y = (safeZoneY + (safeZoneH * 0.75));
            w = __WIDTH;
            h = GRID_H * 20;
            text = "\a3\ui_f\data\IGUI\Cfg\HoldActions\progress\progress_0_ca.paa";
            //colorText = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"};
        };
        class MiddlePicture: ProgressPicture {
            x = safeZoneX + (safeZoneW * 0.5) - ((GRID_W * 10) / 2);
            y = (safeZoneY + (safeZoneH * 0.75) + (GRID_H * 10 / 2));
            w = GRID_W * 10;
            h = GRID_H * 10;
            idc = 2;
            text = "";
        };
    };
};
