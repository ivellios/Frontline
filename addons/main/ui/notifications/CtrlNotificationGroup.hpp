#define __WIDTH (GRID_W * 100)

class SVAR(CtrlNotificationGroup) : ctrlControlsGroupNoScrollbars {
    idc = 1;
    x = safeZoneX + (safeZoneW * 0.5) - (__WIDTH / 2);
    y = (safeZoneY + (safeZoneH * 0.08));
    w = __WIDTH;
    h = safeZoneH * 0.92;

    class Controls { };
};
