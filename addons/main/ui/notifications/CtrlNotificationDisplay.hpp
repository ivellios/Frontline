#define __WIDTH (GRID_W * 100)

class SVAR(CtrlNotificationDisplay) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_CtrlNotificationDisplay', _this select 0];";
    fadeIn = 0;
    fadeOut = 0;
    class Controls {
        class CtrlGroup : ctrlControlsGroupNoScrollbars {
            idc = 1;
            x = safeZoneX + (safeZoneW * 0.5) - (__WIDTH / 2);
            y = (safeZoneY + (safeZoneH * 0.08));
            w = __WIDTH;
            h = safeZoneH * 0.92;

            class Controls { };
        };
    };
};
