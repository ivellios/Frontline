#define __WIDTH (GRID_W * 80)
#define __HEIGHT (GRID_H * 22)

class SVAR(CtrlTooltip): ctrlControlsGroupNoScrollbars {
    idd = -1;
    x = safeZoneX + (safeZoneW * 0.85) - (__WIDTH / 2);
    y = (safeZoneY + (safeZoneH * 0.66));
    w = __WIDTH;
    h = 0;
    hExtended = __HEIGHT + (GRID_H * 1);
    ySpacing = 0;

    class Controls {
        class Background: ctrlStaticBackground {
            idc = 10;
            x = 0;
            y = 0;
            w = __WIDTH;
            h = __HEIGHT;
            colorBackground[] = {0.2, 0.2, 0.2, 0.35};
            //colorBackground[] = __COLOR_BACKGROUND_HEADER;
        };

        class Image: ctrlStaticPictureKeepAspect {
            text = ICON(action,info_outline);
            idc = 11;
            x = (GRID_W * 2.5);
            y = (GRID_H * 4);
            w = (GRID_W * 7.5);
            h = (GRID_H * 7.5);
        };

        class Text : ctrlStructuredText {
            idc = 12;
            x = (GRID_W * 12.5);
            y = 0;
            w = __WIDTH - (GRID_W * 12.5);
            h = __HEIGHT;

            class Attributes {
                size = 1;
                align = "left";
                valign = "middle";
            };
        };
    };
};
