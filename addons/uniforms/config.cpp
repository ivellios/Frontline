#include "macros.hpp"

class CfgPatches {
	class ADDON {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
		requiredAddons[] = {"FRL_Main"};
	};
};

// i case there is a crotch armor
#define Vest_Armorvalue 15
#define Vest_Armorvalues class ItemInfo: ItemInfo {\
	class HitpointsProtectionInfo {\
		class Chest {\
			hitpointName = "HitChest";\
			armor = Vest_Armorvalue;\
			passThrough = 0.35;\
		};\
		class Diaphragm {\
			hitpointName = "HitDiaphragm";\
			armor = Vest_Armorvalue;\
			passThrough = 0.35;\
		};\
		class Abdomen {\
			hitpointName = "HitAbdomen";\
			armor = Vest_Armorvalue;\
			passThrough = 0.35;\
		};\
		class Body {\
			hitpointName = "HitBody";\
			passThrough = 0.35;\
		};\
	};\
};\


#define sideEast    0
#define sideWest    1
#define sideResistance  2
#define sideCivilian    3

class CfgFactionClasses {
    class FRL_Faction_CHN {
        displayName = "China (PLA)";
        armyNameFull = "People's Liberation Army";
        priority = 3;
        side = sideEast;
        icon = "\pr\frl\addons\uniforms\factions\Cfgfactionclasses_chn_pla.paa";
        flag = "\pr\frl\addons\uniforms\factions\flag_china_co.paa";
    };
};

class cfgVehicles {
    class B_Soldier_F;
    class O_Soldier_FRL_CHN_07Woodland: B_Soldier_F {
        author = "Adanteh (Frontline)";
        faction = "FRL_Faction_CHN";
        side = sideEast;
        backpack = "";
        hiddenSelections[] = {"Camo"};
        model = "\A3\Characters_F_Beta\INDEP\ia_soldier_01.p3d";
        identityTypes[] = {"LanguageCHI_F","Head_Asian"};

        _generalMacro = "O_Soldier_FRL_CHN_07Woodland";
        displayName = "Rifleman (Type 07 Woodland)";
        hiddenSelectionsTextures[] = {"\pr\frl\addons\uniforms\Data\clothes_IND_chn_07Woodland.paa"};
        uniformClass = "O_CombatUniform_FRL_chn_07Woodland";

        weapons[] = {"arifle_CTAR_blk_F","hgun_Rook40_F","Throw","Put"};
        magazines[] = {"30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_F","30Rnd_580x42_Mag_F","16Rnd_9x21_Mag","SmokeShell","SmokeShellGreen","Chemlight_green","Chemlight_green","HandGrenade","HandGrenade","16Rnd_9x21_Mag","16Rnd_9x21_Mag"};
        linkedItems[] = {"V_TacVest_chn_07universal","H_HelmetIA_chn_07Woodland","","ItemMap","ItemCompass","ItemWatch","ItemRadio"};
    };

    class O_Soldier_FRL_CHN_07Universal: O_Soldier_FRL_CHN_07Woodland {
        _generalMacro = "O_Soldier_FRL_CHN_07Universal";
        displayName = "Rifleman (Type 07 Universal)";
        hiddenSelectionsTextures[] = {"\pr\frl\addons\uniforms\Data\clothes_IND_chn_07universal.paa"};
        uniformClass = "O_CombatUniform_FRL_chn_07universal";
        linkedItems[] = {"V_TacVest_chn_07universal","H_HelmetIA_chn_07universal","","ItemMap","ItemCompass","ItemWatch","ItemRadio"};
    };

    class O_Soldier_FRL_CHN_99Woodland: O_Soldier_FRL_CHN_07Woodland  {
        _generalMacro = "O_Soldier_FRL_CHN_99Woodland";
        displayName = "Rifleman (Type 99 Woodland)";
        hiddenSelectionsTextures[] = {"\pr\frl\addons\uniforms\Data\clothes_IND_chn_99Woodland.paa"};
        uniformClass = "O_CombatUniform_FRL_chn_99Woodland";
        linkedItems[] = {"V_PlateCarrierIA1_dgtl","H_HelmetIA_chn_99Woodland","","ItemMap","ItemCompass","ItemWatch","ItemRadio"};
    };
	// -- cyka blyat uniforms, getting rid of CSAT armor
	// copied HitPoints from B_Soldier_F
	// ["O_Soldier_base_F","SoldierEB","CAManBase","Man","Land","AllVehicles","All"]
	class All;
	class AllVehicles: All {};
	class Land: AllVehicles {};
	class Man: Land {};
	class CAManBase: Man {};
	class SoldierEB: CAManBase {};
	class O_Soldier_base_F: SoldierEB {};
	class O_Soldier_F: O_Soldier_base_F {
		class HitPoints {
			class HitFace {
				armor = 1;
				explosionShielding = 0.1;
				material = -1;
				minimalHit = 0.01;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
			};
			class HitNeck: HitFace {
				explosionShielding = 0.5;
				name = "neck";
				radius = 0.1;
			};
			class HitHead: HitNeck {
				depends = "HitFace max HitNeck";
				name = "head";
				radius = 0.2;
			};
			class HitPelvis: HitHead {
				armor = 6;
				depends = "0";
				explosionShielding = 1;
				name = "pelvis";
				radius = 0.24;
				visual = "injury_body";
			};
			class HitAbdomen: HitPelvis {
				armor = 1;
				name = "spine1";
				radius = 0.16;
			};
			class HitDiaphragm: HitAbdomen {
				explosionShielding = 6;
				name = "spine2";
				radius = 0.18;
			};
			class HitChest: HitDiaphragm {
				explosionShielding = 6;
				name = "spine3";
				radius = 0.18;
			};
			class HitBody: HitChest {
				armor = 1000;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
				name = "body";
				passThrough = 1;
				radius = 0;
			};
			class HitArms: HitBody {
				armor = 3;
				depends = "0";
				explosionShielding = 1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				visual = "injury_hands";
			};
			class HitHands: HitArms {
				name = "hands";
				visual = "injury_hands";
			};
			class HitLegs: HitHands {
				name = "legs";
				radius = 0.14;
				visual = "injury_legs";
			};
			class Incapacitated: HitLegs {
				armor = 1000;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
				minimalHit = 0;
				name = "body";
				passThrough = 1;
				radius = 0;
				visual = "";
			};
		};
	};
};

class cfgWeapons {
    class InventoryItem_Base_F;
    class ItemCore;
    class UniformItem: InventoryItem_Base_F {
        type = 801;
    };

    class Uniform_Base: ItemCore {
        scope = 0;
        allowedSlots[] = {901};
        picture = "\pr\frl\addons\uniforms\Data\Icons\clothes_IND_NFP_Green.paa";
        class ItemInfo: UniformItem
        {
            uniformModel = "-";
            uniformClass = "B_Soldier_F";
            containerClass = "Supply0";
            mass = 0;
        };
    };

    class VestItem: InventoryItem_Base_F { };

    class U_I_CombatUniform;
    class O_CombatUniform_FRL_chn_07Woodland: U_I_CombatUniform  {
        scope = 2;
        displayName = "Type 07 Fatiques (Woodland)";
        picture = "\pr\frl\addons\uniforms\Data\Icons\clothes_IND_chn_07Woodland.paa";
        class ItemInfo: UniformItem {
            uniformModel = "-";
            uniformClass = "O_Soldier_FRL_CHN_07Woodland";
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class O_CombatUniform_FRL_chn_07universal: O_CombatUniform_FRL_chn_07Woodland {
        displayName = "Type 07 Fatiques (Universal)";
        picture = "\pr\frl\addons\uniforms\Data\Icons\clothes_IND_chn_07universal.paa";
        class ItemInfo: UniformItem {
            uniformModel = "-";
            uniformClass = "O_Soldier_FRL_CHN_07universal";
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class O_CombatUniform_FRL_chn_99Woodland: O_CombatUniform_FRL_chn_07Woodland {
        displayName = "Type 99 Fatiques (Woodland)";
        picture = "\pr\frl\addons\uniforms\Data\Icons\clothes_IND_chn_99Woodland.paa";
        class ItemInfo: UniformItem {
            uniformModel = "-";
            uniformClass = "O_Soldier_FRL_CHN_99Woodland";
            containerClass = "Supply40";
            mass = 40;
        };
    };

    // -- HELMETS -- //
    class H_HelmetIA;
    class H_HelmetIA_chn_07Woodland: H_HelmetIA {
        picture = "\pr\frl\addons\uniforms\Data\Icons\helmet_IND_chn_07Woodland.paa";
        displayName = "MICH (Type 07 Woodland)";
        hiddenSelectionsTextures[] = {"\pr\frl\addons\uniforms\Data\helmet\helmet_IND_chn_07Woodland.paa"};
    };
    class H_HelmetIA_chn_07Universal: H_HelmetIA {
        picture = "\pr\frl\addons\uniforms\Data\Icons\helmet_IND_chn_07universal.paa";
        displayName = "MICH (Type 07 Universal)";
        hiddenSelectionsTextures[] = {"\pr\frl\addons\uniforms\Data\helmet\helmet_IND_chn_07universal.paa"};
    };
    class H_HelmetIA_chn_99Woodland: H_HelmetIA_chn_07Woodland {
        picture = "\pr\frl\addons\uniforms\Data\Icons\helmet_IND_chn_99Woodland.paa";
        displayName = "MICH (Type 99 Woodland)";
        hiddenSelectionsTextures[] = {"\pr\frl\addons\uniforms\Data\helmet\helmet_IND_chn_99Woodland.paa"};
    };

    class Vest_NoCamo_Base: ItemCore {
        class ItemInfo: VestItem { };
    }
    class Vest_Camo_Base: ItemCore {
        class ItemInfo: VestItem { };
    };

    class V_TacVest_blk_POLICE: Vest_Camo_Base {
        class ItemInfo: ItemInfo { };
    };

    class V_TacVest_chn_07universal: V_TacVest_blk_POLICE {
        author = "Adanteh (Frontline)";
        _generalMacro = "V_TacVest_chn_07universal";
        displayName = "Tactical Vest (07 Universal)";
        hiddenSelectionsTextures[] = {"\pr\frl\addons\uniforms\Data\vests\v_tacvest_chn_07universal.paa"};
		Vest_Armorvalues
    };

	class V_PlateCarrier1_rgr: Vest_NoCamo_Base {};
	class V_FRL_PlateCarrier1_rgr: V_PlateCarrier1_rgr {
		Vest_Armorvalues
	};
	class V_PlateCarrier2_rgr: V_PlateCarrier1_rgr {};
	class V_FRL_PlateCarrier2_rgr: V_PlateCarrier2_rgr {
		Vest_Armorvalues
	};
	class V_PlateCarrier2_rgr_noflag_F: V_FRL_PlateCarrier2_rgr {};
	class V_PlateCarrier1_rgr_noflag_FRL: V_PlateCarrier2_rgr_noflag_F {
		Vest_Armorvalues;
	};
	class V_PlateCarrierIA1_dgtl: Vest_NoCamo_Base {};
	class V_FRL_PlateCarrierIA1_dgtl: V_PlateCarrierIA1_dgtl {
		Vest_Armorvalues
	};
	class V_TacVest_khk: Vest_Camo_Base {};
	class V_FRL_TacVest_khk: V_TacVest_khk {
		Vest_Armorvalues
	};
	// class V_PlateCarrierIA1_dgtl: V_PlateCarrierIA1_dgtl {};
    // class V_PlateCarrierIA1_dgtl: Vest_NoCamo_Base {
    //     class ItemInfo: VestItem { };
    // }
    // class V_PlateCarrierIA1_chn_07universal: V_PlateCarrierIA1_dgtl
    // {
    //     author = "Adanteh (Frontline)";
    //     _generalMacro = "V_PlateCarrierIA1_chn_07universal";
    //     scope = 2;
    //     displayName = "GA Carrier Lite (07 Universal)";
    //     picture = "\A3\characters_f_Beta\Data\UI\icon_V_I_Vest_01_ca.paa";
    // };

    // class V_PlateCarrierIA2_dgtl: V_PlateCarrierIA1_dgtl {
    //     class ItemInfo: VestItem { }
    // };

    // class V_PlateCarrierIA2_chn_07universal: V_PlateCarrierIA2_dgtl {
    //     author = "Adanteh (Frontline)";
    //     _generalMacro = "V_PlateCarrierIA2_chn_07universal";
    //     scope = 2;
    //     displayName = "GA Carrier Rig (07 Universal)";
    //     picture = "\A3\characters_f_Beta\Data\UI\icon_V_I_Vest_01_ca.paa";
    // };
};
