/*
    Function:       FRL_Suppress_fnc_vehicleHasWeapons
    Author:         Adanteh
    Description:    Checks if the vehicle has weapons that we should add suppression for
*/
#include "macros.hpp"

params ["_vehicle"];
private _weapons = ((weapons _vehicle) - ["CarHorn", "TruckHorn", "TruckHorn2", "TruckHorn3", "rhsusf_weap_M259", "rhs_weap_mastersafe", "rhsusf_weap_CMFlareLauncher"]);
private _hasWeapons = (count _weapons) > 0;
iF (_hasWeapons) then {
    // -- Exempt vehicles with only miniguns and rocket launchers
    private _nonIgnoredWeapons = [];
    {
        private _ignoreWeapon = call {
             if (_x isKindOf ["M134_minigun", (configFile >> "CfgWeapons")]) exitWitH { true };
             if (_x isKindOf ["RocketPods", (configFile >> "CfgWeapons")]) exitWitH { true };
             false;
        };
        if !(_ignoreWeapon) then {
            _nonIgnoredWeapons pushBack _x;
        };
        nil;
    } count _weapons;
    _hasWeapons = count _nonIgnoredWeapons > 0;
};
_hasWeapons
