/*
    Function:       FRL_Suppress_fnc_addSupression
    Author:         Adanteh
    Description:    Adds suppression

    *	0: Amount of suppression to add <SCALAR>
    *	1: The person firing (You can also give the projectile here for things like mortar) <OBJECT>
*/
#include "macros.hpp"

params ["_suppression", ["_shooter", objNull, [objNull]]];

private _shooterPos = AGLToASL (_shooter modelToWorld (_shooter selectionPosition "Pilot"));
_suppression = _suppression / ([_shooterPos, _shooter] call FUNC(getCover));

// -- Update effects instantly -- //
GVAR(Amount) = ((GVAR(Amount) + _suppression) min GVAR(Setting_maxSuppression));
GVAR(ppBlur) ppEffectEnable true;
GVAR(ppBlur) ppEffectAdjust [(GVAR(Amount) * (GVAR(Setting_blurFactor) / 100))];
GVAR(ppBlur) ppEffectCommit 0.05;

GVAR(ppColor) ppEffectEnable true;
GVAR(ppColor) ppEffectAdjust [1, (1 + GVAR(Amount) / 600 * 1.2), (0 - GVAR(Amount) / 15000 * 1.7), [0.0, 0.0, 0.0, 0.0], [1, 1, 1, 1],  [0, 0, 0, 0.0]];
GVAR(ppColor) ppEffectCommit 0.05;

// -- Time when suppression starts going down again -- //
private _timeValue = (_suppression * (GVAR(Setting_timeFactor) / 10));
GVAR(downTime) = ((GVAR(downTime) max time) + _timeValue min (time + GVAR(Setting_maxTimeStartFade)));

// -- How long and intense to shake camera -- //
addCamShake [(GVAR(Setting_shakeFactor) * (_suppression / 3)), (_timeValue * 2), (GVAR(Setting_shakeFactor) * (_suppression / 3) / 4)];

true;
ppEffectC
