#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

#include "cfgSounds.hpp"

class FRL {
    class Modules {
        class Suppress {
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\suppress";
            class addSuppression;
            class clientInit;
            class firedEH;
            class getCover;
            class handleEffects;
            class particleSuppress;
            class startSystem;
            class vehicleHasWeapons;
        };
    };
};
