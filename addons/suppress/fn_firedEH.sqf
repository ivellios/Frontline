/*
    Function:       FRL_Suppress_fnc_firedEH
    Author:         Adanteh
    Description:    Fired EH for suppression system
*/
#include "macros.hpp"

private _shooter = _this param [0, objNull];
private _bullet = _this param [6, objNull];

// TODO Optimiize this like craycray. Quickest and most reliable exiit first
private _playerVehicle = vehicle Clib_Player;
if !(_playerVehicle == _shooter) then {
	private _applicable = (_playerVehicle == Clib_Player);
	if (_applicable || { isTurnedOut Clib_Player } || {(({_playerVehicle isKindOf _x} count GVAR(Setting_openVehicleTypes)) > 0)}) then {
		private _shooterRange = _shooter distance Clib_Player;
		if (_shooterRange < GVAR(Setting_maxDist)) then {
			private ["_detectRange"];
			private _minDist = if ((side effectiveCommander (vehicle _shooter) isEqualTo playerSide)) then {
				_detectRange = GVAR(Setting_detectRadius) / 2.5;
				(_shooterRange > (GVAR(Setting_minDist) * 6));
			} else {
				_detectRange = GVAR(Setting_detectRadius);
				(_shooterRange > (GVAR(Setting_minDist)));
			};

			if (_minDist) then {
				// -- See if bullet is flying in the same direction as we are from the bullet -- //
				private _relDir = abs ((_bullet getDir Clib_Player) - getDir _bullet);
				// -- Bullet should actually be travelling in direction of Clib_Player -- //
				if ((_relDir < 7.5) || {(_relDir > 352.5)}) then {
					GVAR(Bullets) pushBack [_bullet, _shooter, _detectRange];
				};
				//_hit = getNumber(configFile >> "CfgAmmo" >> (_args select 4) >> "hit");
			};
		};
	};
};

true
