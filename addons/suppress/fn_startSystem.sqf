/*
    Function:       FRL_Suppress_fnc_startSystem
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

setCamShakeParams  [0.001, 0.015, 0.015, 0.8, true];
enableCamShake true;

GVAR(ppColor) = ["colorCorrections", 2501] call MFUNC(ppEffectCreate);
GVAR(ppBlur) = ["dynamicBlur", 416, [0], 0.3] call MFUNC(ppEffectCreate);
//GVAR(ppBlurRadial) = ["radialBlur", 110] call MFUNC(ppEffectCreate);

GVAR(soundStartedAt) = 0;
GVAR(soundLoopTill) = 0;
GVAR(soundStarted) = false;
GVAR(soundPlayNextAt) = 0;
GVAR(soundFadeStrength) = 0;

GVAR(amount) = 0;
GVAR(downTime) = 0;
GVAR(bullets) = [];

// -- Add fired EH to all players -- //
{
    if (alive _x) then {
        _handle = _x addEventhandler ["fired", format ["_this spawn %1;", QFUNC(firedEH)]];
    };
    nil;
} count (allUnits - [CLib_Player]);

// -- Add fired EH to all vehicles -- //
{
    if (alive _x) then {
        if !(_x getVariable [QGVAR(handle), false]) then {
            _x setVariable [QGVAR(handle), true];
            if ([_x] call FUNC(vehicleHasWeapons)) then {
                _x addEventhandler ["fired", format ["_this spawn %1;", QFUNC(firedEH)]];
            };
        };
    };
    nil;
} count ((entities "LandVehicle") + (entities "Air") + (entities "Ship"));

// -- Apply EH when a unit is created -- //
["unitCreated", {
    params ["_unit"];
    if ((_unit getVariable [QGVAR(handle), -1]) isEqualTo -1) then {
        if (_unit isEqualTo player) exitWith { };

        _handle = _unit addEventhandler ["fired", format ["_this spawn %1;", QFUNC(firedEH)]];
        _unit setVariable [QGVAR(handle), _handle];
    };
}] call CFUNC(addEventHandler);


// -- Handles the effects -- //
[{ _this call FUNC(handleEffects) }, __c_effectinterval, []] call MFUNC(addPerFramehandler);


// -- Handles the bullets -- //
// TODO Optimize this
[{
    private _deleted = 0;
    {
        _x params ["_bullet", "", "", ["_closest", 20.5]];
        if !(alive _bullet) then {
            GVAR(bullets) deleteAt (_forEachIndex - _deleted);
            _deleted = _deleted + 1;
            if (_closest != 20.5) then {
                [25 / (_closest + 2), (_x param [1, objNull])] call FUNC(addSuppression);
            };
        } else {
            private _distance = _bullet distance CLib_Player;
            if (_distance < _closest) then {
                _x set [3, _distance];
            } else {
                // -- If bullet is moving away again, delete it -- //
                if (_closest < 20.5) then {
                    GVAR(bullets) deleteAt (_forEachIndex - _deleted);
                    _deleted = _deleted + 1;
                    if (_closest < (_x param [2, 10])) then {
                        [25 / (_closest + 2), (_x param [1, objNull])] call FUNC(addSuppression);
                    };
                };
            };
        };
        nil;
    } forEach GVAR(bullets);
}, 0, []] call MFUNC(addPerFramehandler);
