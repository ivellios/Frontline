/*
    Function:       FRL_Suppress_fnc_getCover
    Author:         Adanteh
    Description:    Gets a cover modifier for player
*/
#include "macros.hpp"

params ["_shooterPos", ["_shooter", objNull]];

private _coverValues = 1;
private _lowCover = 1;

{
	if (count _x > 0) then {
		_coverValues = _coverValues + 3 ^ 1.65;
	} else {
		_lowCover = _lowcover + 2;
	};
	nil;
} count [
	(lineIntersectsSurfaces [AGLtoASL (CLib_Player modelToWorld ((CLib_Player selectionPosition "Neck") vectorAdd [0, 0.35, 0.5])), _shooterPos, _shooter, objNull, true, 1, "IFIRE", "NONE"]), // -- Head
	(lineIntersectsSurfaces [AGLtoASL (CLib_Player modelToWorld ((CLib_Player selectionPosition "LeftShoulder") vectorAdd [-0.5, 0, 0])), _shooterPos, _shooter, objNull, true, 1, "IFIRE", "NONE"]), // -- Left Shoulder
	(lineIntersectsSurfaces [AGLtoASL (CLib_Player modelToWorld ((CLib_Player selectionPosition "RightShoulder") vectorAdd [0.5, 0, 0])), _shooterPos, _shooter, objNull, true, 1, "IFIRE", "NONE"]) // -- Right shoulder
];

_coverValues = ((_coverValues / 2 / _lowCover) max 1);
_coverValues;
