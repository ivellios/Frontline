/*
    Function:       FRL_Supress_fnc_handleEffects
    Author:         Adanteh
    Description:    Handles the strength of effects
*/
#include "macros.hpp"

#define __FADETIME 6
if (GVAR(soundFadeStrength) > 0) then {
    GVAR(soundFadeStrength) = (GVAR(soundFadeStrength) - 0.020) max 0; // -- Max 10 seconds ever
    __c_effectinterval fadeSound (1 - (GVAR(soundFadeStrength) * 2)) max 0.01;
    __c_effectinterval fadeSpeech (GVAR(soundFadeStrength) / 4);

    if (GVAR(soundFadeStrength) >= 0.03) then {
        if (time > GVAR(soundPlayNextAt)) then {
            GVAR(soundPlayNextAt) = time + 4;
            playSound [QGVAR(loopRumble), true];
        };
    };
};


if (GVAR(amount) > 0) then {
    // -- Add extra sway factor, to compensate for extra precission from resting / different stances
    private _swayFactor = (getAnimAimPrecision player);
    if (isWeaponRested player) then {
        _swayFactor = _swayFactor / 0.4;
    } else {
        if (isWeaponDeployed player) then {
            _swayFactor = _swayFactor / 0.25;
        };
    };

    GVAR(ppBlur) ppEffectEnable true;
    GVAR(ppColor) ppEffectEnable true;
    CLib_Player setCustomAimCoef (GVAR(amount) * (GVAR(Setting_swayFactor)) * _swayFactor);

    if (GVAR(downTime) < time) then { // -- Lower suppression -- //
        GVAR(amount) = GVAR(amount) - GVAR(Setting_bleed);

        // -- Simulates increased light sensitivy from pupil dilation -- //
        GVAR(ppColor) ppEffectAdjust [1, (1 + GVAR(amount) / 600 * 1.2), (0 - GVAR(amount) / 15000 * 1.7), [0.0, 0.0, 0.0, 0.0], [1, 1, 1, 1],  [0, 0, 0, 0.0]];
        GVAR(ppColor) ppEffectCommit __c_effectinterval;
        GVAR(ppBlur) ppEffectAdjust [(GVAR(amount) * (GVAR(Setting_blurFactor) / 100))];
        GVAR(ppBlur) ppEffectCommit __c_effectinterval;

        if (GVAR(amount) <= 0) then {
            CLib_Player setCustomAimCoef 1;
            GVAR(amount) = 0;
            GVAR(ppBlur) ppEffectEnable false;
            GVAR(ppColor) ppEffectEnable false;
        };
    };
};
