/*
    Function:       FRL_Suppress_fnc_particleSuppress
    Author:         Adanteh
    Description:    This handles destruction script inputs from particle effects and translates it into a decent particle suppression
*/
#include "macros.hpp"

if (isNil QGVAR(enabled)) exitWith { };

params ["_maxSuppress", "_maxRange", "_fallOffCoef", "_position", ["_tinitus", true]];

private _distance = _position distance CLib_Player;
if (_distance > _maxRange) exitWith { false };

_position set [2, (_position select 2 + 0.1)];
private _suppression = (_maxSuppress / ((_distance / _fallOffCoef) max 1)) / ([AGLtoASL _position] call FUNC(getCover));

// -- Update effects instantly -- //
GVAR(amount) = ((GVAR(amount) + _suppression) min GVAR(Setting_maxSuppression));
GVAR(ppBlur) ppEffectEnable true;
GVAR(ppBlur) ppEffectAdjust [(GVAR(amount) * (GVAR(Setting_blurFactor) / 100))];
GVAR(ppBlur) ppEffectCommit 0.02;

GVAR(ppColor) ppEffectEnable true;
GVAR(ppColor) ppEffectAdjust [1, (1 + GVAR(amount) / 600 * 1.2), (0 - GVAR(amount) / 15000 * 1.7), [0.0, 0.0, 0.0, 0.0], [1, 1, 1, 1],  [0, 0, 0, 0.0]];
GVAR(ppColor) ppEffectCommit 0.02;

// -- Time when suppression starts going down again -- //
private _timeValue = (_suppression * (GVAR(Setting_timeFactor) / 15));
GVAR(downTime) = ((GVAR(downTime) max time) + _timeValue min (time + GVAR(Setting_maxTimeStartFadeExp)));

// -- How long and intense to shake camera -- //
private _shakeStrength = _suppression min 20;
addCamShake [(GVAR(Setting_shakeFactor) * (_shakeStrength / 6)), _timeValue min 2, GVAR(Setting_shakeFactor) * (_shakeStrength / 5)];

if (_tinitus) then {
	if (GVAR(soundLoopTill) - time <= 0) then {
		GVAR(soundPlayNextAt) = time + 4;
		playSound [QGVAR(loopRumble), true];
	};
	GVAR(soundLoopTill) = GVAR(soundLoopTill) max (time + (_timeValue min 8));
	GVAR(soundFadeStrength) = (GVAR(soundFadeStrength) + (GVAR(amount) / 150)) min 0.99;
	0.01 fadeSound (1 - GVAR(soundFadeStrength));
	0.01 fadeSpeech (GVAR(soundFadeStrength) / 4);
};
