Hyper-realistic blur when someone shoots at your face.
Prevents people peeking out from behind a tree and making a couple quick headshots even though there's 5 guys shooting at him