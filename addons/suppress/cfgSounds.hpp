class CfgSounds {
	class GVAR(fadein) {
		sound[] = {"\pr\frl\addons\suppress\sounds\tinitus_fade_in_1s.wss", 1, 1};
		name = QGVAR(fadeIn);
		titles[] = {0, ""};
	};

	class GVAR(fadeinRumble) {
		sound[] = {"\pr\frl\addons\suppress\sounds\tinitus_fade_in_1sec.wss", 1, 1};
		name = QGVAR(fadeInRumble);
		titles[] = {0, ""};
	};

	class GVAR(fadeout) {
		sound[] = {"\pr\frl\addons\suppress\sounds\tinitus_fade_out_1s.wss", 1, 1};
		name = QGVAR(fadeout);
		titles[] = {0, ""};
	};

	class GVAR(loop) {
		sound[] = {"\pr\frl\addons\suppress\sounds\tinitus_loop_2s.wss", 1, 1};
		name = QGVAR(loop);
		titles[] = {0, ""};
	};

	class GVAR(loopRumble) {
		sound[] = {"\pr\frl\addons\suppress\sounds\tinitus_loop_4sec.wss", 1, 1};
		name = QGVAR(loopRumble);
		titles[] = {0, ""};
	};

	class GVAR(fullRumble) {
		sound[] = {"\pr\frl\addons\suppress\sounds\tinitus_full_5sec.wss", 1, 1};
		name = QGVAR(fullRumble);
		titles[] = {0, ""};
	};
};
