#include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Add fire eventhandler for all units and vehicles in a loop (Check if this can be done better)
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call FUNC(clientInit);
 */


if (!(getMissionConfigValue [QGVAR(enabled), true])) exitWith { };

GVAR(enabled) = true;

GVAR(Setting_maxDist) = getMissionConfigValue [QGVAR(maxistance), 800]; // bullets fired from further than this don't suppress (m)
GVAR(Setting_minDist) = getMissionConfigValue [QGVAR(minDistance), 10]; // bullets fired from less than this don't suppress (m) (Makes sure you don't annoy each other with friendly suppressing on close range and makes sure the effect isn't as bad in CQB)
GVAR(Setting_detectRadius) = getMissionConfigValue [QGVAR(detectionDistance), 11]; // detection radius around each CLib_Player (m). If a bullet passes around a sphere of this value in meters, it gets counted towards suppression
GVAR(Setting_maxSuppression) = getMissionConfigValue [QGVAR(maxSuppression), 200]; // Max suppression amount. Extra suppression stops being added once this value is reached. In practice this caps the max amount of blur
GVAR(Setting_bleed) = getMissionConfigValue [QGVAR(suppressionBleed), 4]; // Amount of suppression to reduce per interval (0.25)
GVAR(Setting_maxTimeStartFade) = getMissionConfigValue [QGVAR(timeFadeMax), 5];  // Max time BEFORE suppression starts reducing (NOT TIME TO FADE!!!).
GVAR(Setting_maxTimeStartFadeExp) = getMissionConfigValue [QGVAR(timeFadeMaxExp), 10];  // Max time BEFORE suppression starts reducing (NOT TIME TO FADE!!!).
GVAR(Setting_timeFactor) = getMissionConfigValue [QGVAR(timeFactor), 12];  // Extra recovery time added per weighted suppression value
GVAR(Setting_shakeFactor) = getMissionConfigValue [QGVAR(shakeFactor), 0.6];  // Intensity of camera shake per weighted suppresson value
GVAR(Setting_swayFactor) = getMissionConfigValue [QGVAR(shakeFactor), 0.13];  // Intensity of camera shake per weighted suppresson value
GVAR(Setting_blurFactor) = getMissionConfigValue [QGVAR(blurFactor), 0.5];  // Intensity of blur per suppression value
GVAR(Setting_openVehicleTypes) = getMissionConfigValue [QGVAR(openVehicleTypes), ["StaticWeapon", "Motorcycle"]];  // Vehicle types in which suppression should still be applied

["vehicleInit", {
	(_this select 0) params ["_vehicle"];
	if !(_vehicle getVariable [QGVAR(handle), false]) then {
		_vehicle setVariable [QGVAR(handle), true];
		if ([_vehicle] call FUNC(vehicleHasWeapons)) then {
			_vehicle addEventhandler ["fired", format ["_this spawn %1;", QFUNC(firedEH)]];
		};
	};
}] call CFUNC(addEventHandler);

// -- Only run this once display 46 is available -- //
["missionStarted", { _this call FUNC(startSystem) }] call CFUNC(addEventHandler);
