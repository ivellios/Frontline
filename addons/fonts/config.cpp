#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class CfgFontFamilies {
  #include "font_nexa.hpp"
  /*
  #include "font_kelsonSansRU.hpp"
  #include "font_muller.hpp"
  #include "font_casper.hpp"
  #include "font_bebas.hpp"
  #include "font_nexa.hpp"
  */
};
