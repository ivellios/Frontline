/*
 *    File: fn_treatmentResult.sqf
 *    Author: Adanteh
 *    Shows message based on type of return
 *
 *    0: Finished treatment <BOOLEAN>
 *
 *    Returns:
 *    None
 *
 *    Example:
 *    [true] call FUNC(treatmentResult);
 */


#include "macros.hpp"

(_this select 0) params ["_medic", "_action", "_finished", "_patient"];
private _isPatient = Clib_Player isEqualTo _patient;
VAR(_result) = "";
if (_finished) then {
    _result = "healthy";
    switch (toLower _action) do {
        case "cpr";
        case "drag": {
            _result = "";
        };
        case "morphine": {
            if !([_patient] call FUNC(isInPain)) then { _result = "injectWrong"; };
            if ([_patient] call FUNC(isUnconscious)) then { _result = "needTreatment"; };
            if (damage _patient > 0.01) then { _result = "needTreatment"; };
        };
        case "adrenaline": {
            if ([_patient] call FUNC(isDead)) exitWith { _result = "heDead"; };
            if ([_patient] call FUNC(isBleeding)) exitWith { _result = "injectwrong"; };
            if ([_patient] call FUNC(isInPain)) then { _result = "needTreatment"; };
            if !([_patient] call FUNC(isUnconscious)) then { _result = "injectWrong"; };
            if (damage _patient > 0.01) then { _result = "needTreatment"; };
        };
        case "fielddressing": {
            if (damage _patient > 0.01) then {
                _result = "needTreatment";
                if (damage _patient <= ([QGVAR(Settings_healthIncreaseCap), 0.25] call MFUNC(cfgSetting))) then {
                    if !([_medic] call FUNC(isMedic)) then {
                        _result = "needMedic";
                    };
                };
            } else {
                if ([_patient] call FUNC(isUnconscious)) exitWith { _result = "needTreatment"; };
                if ([_patient] call FUNC(isInPain)) exitWith { _result = "needTreatment"; };
            };
        };
    };
} else {
    if ([_patient] call FUNC(isDead)) then {
        _result = "hedead";
    };
};
switch (toLower _result) do {
    case "": { };
    case "hedead": {
        if !(_isPatient) then {
           ["show", [LSTRING(PersonDied), 3]] call MFUNC(showNotification); //"The person you were treating has died during treatment"
        };
    };
    case "cancelled": {
        if (_isPatient) then {
           ["show", [LSTRING(TreatCancel), 3]] call MFUNC(showNotification); // "The Treatment was cancelled"
        } else {
           ["show", [LSTRING(ICancelTreat), 3]] call MFUNC(showNotification); // "I cancelled your treatment"
        };
    };
    case "needtreatment": {
        if (_isPatient) then {
            ["show", [LSTRING(YouNeedTreat), 3]] call MFUNC(showNotification); // "You are still in need of further treatment."
        } else {
            ["show", [LSTRING(HeNeedTreat), 3]] call MFUNC(showNotification); // "He is still in need of further treatment."
        };
    };
    case "needmedic": {
        if (_isPatient) then {
            ["show", [LSTRING(YouNeedMedic), 3]] call MFUNC(showNotification); // "You require a qualified medic for final treatment of wounds."
        } else {
           ["show", [LSTRING(HeNeedMedic), 3]] call MFUNC(showNotification); // "He requires a qualified medic for final treatment of wounds.""
        };
    };
    case "injectwrong": {
        if (_isPatient) then {
            ["show", [LSTRING(NoBenefit), 3]] call MFUNC(showNotification); // "The injector had no benefit."
        } else {
            ["show", [LSTRING(WhyYouInject), 3]] call MFUNC(showNotification); // "What did you inject me for?!"
        };
    };
    case "healthy": {
        if (_isPatient) then {
            ["show", [LSTRING(YouFine), 3]] call MFUNC(showNotification); // "You appear fine now."
        } else {
            ["show", [LSTRING(HeFine), 3]] call MFUNC(showNotification); // "He appears fine now."
        };
    };
    case "finished": {
        if !(_isPatient) then {
            ["show", [LSTRING(IFinishTreat), 3]] call MFUNC(showNotification); // "I finished your treatment."
        };
    };
};