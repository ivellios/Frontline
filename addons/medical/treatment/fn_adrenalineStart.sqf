/*
    Function:       FRL_Medical_fnc_adrenalineStart
    Author:         Adanteh
    Description:    Starts adrenaline treatment
*/
#include "macros.hpp"

private _actionDuration = if ([_medic] call FUNC(isMedic)) then {
	[QGVAR(Settings_treatmentTimeAdrenalineMedic), 6] call MFUNC(cfgSetting);
} else {
	[QGVAR(Settings_treatmentTimeAdrenaline), 10] call MFUNC(cfgSetting);
};

_target setVariable [QGVAR(treatmentAmount), 1 / _actionDuration, true];
_target setVariable [QGVAR(treatmentProgress), _savedProgress, true];
