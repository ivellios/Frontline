/*
    Function:       FRL_Medical_fnc_dragEnd
    Author:         Adanteh
    Description:    Drag end function
*/
#include "macros.hpp"

private _isPatient = Clib_Player isEqualTo _target;

private _log = ["[#FRL DRAG End]", _this, _isPatient];
LOG(_log)

if (_isPatient) then {
    if (alive _target) then {
        ["switchMove", [_target, "unconsciousrevivedefault"]] call CFUNC(globalEvent);
        detach _target;
        _target setVariable [QGVAR(draggedBy), nil, true];
    };
} else {
    // -- Validation in case target has disappeared somehow -- //
    if (isNull _target) then {
        {
            if (_x isKindOf "CaManBase") then {
                detach _x;
            };
            nil;
        } count (attachedObjects CLib_Player);
    } else {
        detach _target;
    };

    _target setVariable [QGVAR(draggedBy), nil, true]; // broadcast
    if (alive CLib_Player) then {
        private _animation = switch (CLib_Player getVariable [QGVAR(dragEndAnim), ""]) do {
            case "pistol": { "AcinPknlMstpSnonWpstDnon_AmovPknlMstpSrasWpstDnon"; };
            case "rifle": { "AcinPknlMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon" };
            default { "AcinPknlMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon" };
        };
        CLib_Player setVariable [QGVAR(dragEndAnim), nil];
        ["switchMove", [CLib_Player, _animation]] call CFUNC(globalEvent);
    };
};
