/*
    Function:       FRL_Medical_fnc_diagnoseStart
    Author:         Adanteh
    Description:    Starts diagnosing. Will check which medical are required and highlight said action
*/
#include "macros.hpp"

private _actionDuration = if ([_medic] call FUNC(isMedic)) then {
	[QGVAR(Settings_treatmentTimeDiagnoseMedic), 1] call MFUNC(cfgSetting);
} else {
	[QGVAR(Settings_treatmentTimeDiagnose), 3] call MFUNC(cfgSetting);
};

_target setVariable [QGVAR(treatmentAmount), 1 / _actionDuration, true];
_target setVariable [QGVAR(treatmentProgress), _savedProgress, true];
