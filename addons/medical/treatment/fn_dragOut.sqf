/*
    Function:       FRL_Medical_fnc_dragOut;
    Author:         Adanteh
    Description:    Allows dragging a wounded unit out of a vehicle
    Example:        [Clib_Player] call FRL_Medical_fnc_dragOut;
*/
#include "macros.hpp"

params ["_unit", "_vehicle"];

private _log = ["DragoutWounded", _this];

// -- Check if there is anyone to drag out
private _crewIncapped = (crew _vehicle) select { [_x] call FUNC(isUnconscious) };
if (_crewIncapped isEqualTo []) exitWith {
    ["No unconscious units inside", "nope"] call MFUNC(notificationShow);
    true;
};

private _targetUnit = _crewIncapped param [0, objNull];

// -- Find the proper dragging settings
private _actionIndex = (GVAR(medicalOptions) select 6) find "Drag";
if (_actionIndex == -1) exitWith { false };

GVAR(treatmentInProgress) = true;
GVAR(treatmentCurrent) = _actionIndex;
GVAR(treatmentCurrentButton) = (GVAR(medicalOptions) select 0) select _actionIndex;
GVAR(medicalTarget) = _targetUnit;

private _log = ["action", _actionIndex];

// -- Start the code
private _actionCode = (GVAR(medicalOptions) select 2) select _actionIndex;
private _actionReturn = [_unit, _targetUnit, _actionIndex] call _actionCode;

true;
