/*
    Function:       FRL_Medical_fnc_clientInitTreatments
    Author:         Adanteh
    Description:    Prepares the medical actions possible, inits vars
*/
#include "macros.hpp"

["CAManBase"] call EFUNC(main,addInteractType);

GVAR(medicalOptions) = [[], [], [], [], [], [], [], [], []];
GVAR(usingTargetItem) = false;

private _treatmentOptions = "(isClass _x) && {getNumber (_x >> 'scope') >= 2}" configClasses (configFile >> "FRL" >> "MedicalTreatments");
{
    private _treatmentCfg = _x;
    private _treatmentIndex = (GVAR(medicalOptions) select 0) pushBack (getNumber (_treatmentCfg >> "dikKey"));

    (GVAR(medicalOptions) select 1) set [_treatmentIndex, getText (_treatmentCfg >> "displayName")];
    (GVAR(medicalOptions) select 5) set [_treatmentIndex, getNumber (_treatmentCfg >> "allowSelf") > 0];
    (GVAR(medicalOptions) select 6) set [_treatmentIndex, configName _treatmentCfg];
    (GVAR(medicalOptions) select 7) set [_treatmentIndex, getNumber (_treatmentCfg >> "needsMedikit") > 0];
    {
        _x params ["_index", "_entryName"];
        private _codeEntry = getText (_treatmentCfg >> _entryName);
        private _code = missionNamespace getVariable _codeEntry;
        if (isNil "_code") then {
            _code = compile _codeEntry;
        };
        (GVAR(medicalOptions) select _index) set [_treatmentIndex, _code];
    } forEach [[2, "actionCode"], [3, "conditionCode"], [4, "returnCode"], [8, "conditionShow"]];
	nil;
} count _treatmentOptions;

[QGVAR(treatmentStart), { _this call FUNC(treatmentStartLocal) }] call CFUNC(addEventHandler); // -- Local to patient
[QGVAR(treatmentReceived), { _this call FUNC(treatmentEndLocal) }] call CFUNC(addEventHandler); // -- Local to patient


// -- End medic action when you get killer or go incap
["Killed", {
    (_this select 0) params ["_unit"];
    if (_unit isEqualTo Clib_Player) then {
        [false] call FUNC(stopTreatment);
    };
}] call CFUNC(addEventHandler);

["UnconsciousnessChanged", {
    private _unconscious = (_this select 0);
    if (_unconscious) then {
        [false] call FUNC(stopTreatment);
    };
}] call CFUNC(addEventHandler);

["currentWeaponChanged", {
    [false] call FUNC(stopTreatment);
}] call CFUNC(addEventHandler);
