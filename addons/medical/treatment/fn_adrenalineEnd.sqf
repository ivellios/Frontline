/*
    Function:       FRL_Medical_fnc_adrenalineEnd
    Author:         Adanteh
    Description:    Adrenaline end function
*/
#include "macros.hpp"

if !([_target] call FUNC(isBleeding)) then {
    [_target, false] call FUNC(setUnconscious);
};
