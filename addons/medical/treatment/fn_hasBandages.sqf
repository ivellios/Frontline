/*
    Function:       FRL_medical_fnc_hasBandages
    Author:         N3croo
    Description:    returns if unit has Bandages
*/
#include "macros.hpp"

params ["_unit"];

// TODO, make this use target items
private _itemRequired = getText (configFile >> "FRL" >> "MedicalTreatments" >> "bandage" >> "itemRequired");

private _hasItem = if (_itemRequired != "") then {

    if (({ _itemRequired == _x } count (items _unit)) > 0) exitWith { // -- You have the item
        true
    };

    false;
} else {
    true
};

_hasItem;
