/*
    Function:       FRL_Medical_fnc_bandageStart
    Author:         Adanteh
    Description:    Starts bandaging treatment
*/
#include "macros.hpp"

private _bandageDuration = if ([_medic] call FUNC(isMedic)) then {
	[QGVAR(Settings_treatmentTimeBandageMedic), 6] call MFUNC(cfgSetting);
} else {
	[QGVAR(Settings_treatmentTimeBandage), 10] call MFUNC(cfgSetting);
};

_target setVariable [QGVAR(treatmentAmount), 1 / _bandageDuration, true];
_target setVariable [QGVAR(treatmentProgress), _savedProgress, true];
