/*
    Function:       FRL_Medical_fnc_treatmentStart
    Author:         Adanteh
    Description:    Shared treatment starting
*/
#include "macros.hpp"

params ["_actionSlug", "_args"];
_args params ["_medic", "_target", "_actionIndex"];

if (_actionSlug == "") exitWith { };

private _log = ["Treatmentstart ", _actionSlug];
LOG(_log)

GVAR(progressTarget) = _target;

private _instantAction = getNumber (configFile >> "FRL" >> "MedicalTreatments" >> _actionSlug >> "instantAction") > 0;
private _savedProgress = 0;
if !(_instantAction) then {

	_target setVariable [QGVAR(treatmentStartTime), time]; // -- Used to track progress
	// -- Load saved progress. Only add progress param if the treatment type is the same -- //
	private _oldProgress = _target getVariable [QGVAR(treatmentProgressSaved), 0];
	if (_oldProgress > 0) then {
		if ((_target getVariable [QGVAR(treatmentSaved), ""]) isEqualTo _actionSlug) then {
			_savedProgress = _oldProgress;
		};
	};
};

[_actionSlug, true] call FUNC(treatmentGesture);

private _knownTreatment = switch (toLower _actionSlug) do {
    case "diagnose": {
        call FUNC(diagnoseStart);
		true;
    };

    case "bandage": {
        call FUNC(bandageStart);
		true;
    };

    case "adrenaline": {
        call FUNC(adrenalineStart);
		true;
    };

    case "morphine": {
        call FUNC(morphineStart);
		true;
    };

    case "drag": {
        [_medic, _target] call FUNC(dragStart);
		true;
    };

    case "cpr": {
		call FUNC(cprStart);
		true;
    };
    default { false };
};

[QGVAR(treatmentStart), _target, [_medic, _target, _actionSlug]] call CFUNC(targetEvent);

if !(_instantAction) then {
	[_actionSlug] call FUNC(showProgressbar);
};

true;
