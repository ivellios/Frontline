/*
    Function:       FRL_Medical_fnc_treatmentEnd
    Author:         Adanteh
    Description:    Shared treatment end function
    Locality:       Medic
*/
#include "macros.hpp"

params ["_actionSlug", "_args"];
_args params ["_medic", "_target", "_actionIndex", "_finished"];

private _log = ["TreatmentEnd", _actionSlug, _this];
LOG(_log)

[_actionSlug, false] call FUNC(treatmentGesture);

if (_actionSlug == "") exitWith { };
if (_actionSlug == "drag") then {
    _finished = true;
    _target = missionNamespace getVariable [QGVAR(progressTarget), _target];
};

if (_finished) then {
    switch (toLower _actionSlug) do {
        case "diagnose": {
            [false] call FUNC(diagnoseEnd);
        };

        case "bandage";
        case "adrenaline";
        case "morphine": {

        };

        case "drag": {
            call FUNC(dragEnd);
        };

        case "cpr": { };
        default { };
    };


    ["TreatmentDone", [_medic, _actionSlug]] call CFUNC(serverEvent);
    private _itemRequired = getText (configFile >> "FRL" >> "MedicalTreatments" >> _actionSlug >> "itemRequired");
    if (_itemRequired != "") then {
        if (GVAR(usingTargetItem)) then {
            _target removeItem _itemRequired;
        } else {
            _medic removeItem _itemRequired;
        };
    };
};

[QGVAR(treatmentReceived), _target, [_medic, _target, _actionSlug, _finished]] call CFUNC(targetEvent);


if (_finished) exitWith { true };

private _keepProgress = (getNumber (configFile >> "FRL" >> "MedicalTreatments" >> _actionSlug >> "keepProgress") >= 1);
if !(_keepProgress) exitWith { };
[_target, _actionSlug] call FUNC(treatmentKeepProgress);

true;
