/*
    Function:       FRL_Medical_fnc_diagnoseEnd
    Author:         Adanteh
    Description:    Starts diagnosing. Will check which medical are required and highlight said action
*/
#include "macros.hpp"

params [["_silent", false]];

private _treatmentOrder = [];
if ([_target] call FUNC(isBleeding)) then {
    _treatmentOrder pushBack "bandage";
};

if ([_target] call FUNC(isUnconscious)) then {
    _treatmentOrder pushBack "adrenaline";
};

if ([_target] call FUNC(isInPain)) then {
    _treatmentOrder pushBack "morphine";
};

if (damage _target > 0.01) then {
    _treatmentOrder pushBackUnique "bandage";
};

GVAR(diagnoseCache) = [_target, _treatmentOrder];
call FUNC(diagnoseHighlight);

if (count _treatmentOrder == 0) exitWith {
    if !(_silent) then {
        if (_target isEqualTo Clib_Player) then {
            ["show", ["You don't need any treatment", 3]] call MFUNC(showNotification);
        } else {
            ["show", [format ["%1 doesn't need any treatment", [_target] call CFUNC(name)], 3]] call MFUNC(showNotification);            
        };
    };
    true
};



true;
