/*
    Function:       FRL_Medical_fnc_bandageEnd
    Author:         Adanteh
    Description:    Bandage end function
*/
#include "macros.hpp"

private ["_healthIncrease", "_healthIncreaseCap"];
if ([_medic] call FUNC(isMedic)) then {
    _healthIncrease = [QGVAR(Settings_healthIncreaseMedic), 0.5] call MFUNC(cfgSetting);
    _healthIncreaseCap = 0;
} else {
    _healthIncrease = [QGVAR(Settings_healthIncrease), 0.25] call MFUNC(cfgSetting);
    _healthIncreaseCap = [QGVAR(Settings_healthIncreaseCap), 0.25] call MFUNC(cfgSetting);
};

_damageNew = (damage _target) min (((damage _target) - _healthIncrease) max _healthIncreaseCap);
[_target, false] call FUNC(setBleeding);
[_target, _damageNew] call FUNC(setDamage);
