/*
    Function:       FRL_Medical_fnc_dragStart
    Author:         Adanteh
    Description:    Starts the dragging
*/
#include "macros.hpp"

params ["_medic", "_target"];

private _isPatient = Clib_Player isEqualTo _target;

private _log = ["[#FRL DRAG Start]", _this, _isPatient];
LOG(_log)

if (_isPatient) then {

	if ((vehicle _target) != _target) then {
		// -- Moving people out vehicles while unconscious is 100% fucked and creates some fucked up situations, so setUnconscious false
		_target setUnconscious false;
		moveOut _target;
		_target setUnconscious true;
	};
	 
	[{
		params ["_target", "_medic"];
		_target enableSimulation true;
		["enableSimulation", [_target, true]] call CFUNC(serverEvent);
		["switchMove", [_target, "AinjPpneMrunSnonWnonDb_grab"]] call CFUNC(globalEvent);
		_target attachTo [_medic, [0.12, 0.95, 0]];
		_target setDir 180;

	}, [_target, _medic]] call CFUNC(execNextFrame);

} else {

	private _weapon = "";
	private _animation = switch (currentWeapon _medic) do {
		case (primaryWeapon _medic): { _weapon = "rifle"; "frl_drag_rfl_stp"; };
		case (handgunWeapon _medic): { _weapon = "pistol"; "frl_drag_pst_stp"; };
		case (secondaryWeapon _medic): { _weapon = "launcher"; "frl_drag_lnr_stp"; };
		default { _weapon = "none"; "frl_drag_non_stp" };
	};

	_target enableSimulation true;
	_target setVariable [QGVAR(draggedBy), _medic, true];
	_medic setVariable [QGVAR(dragEndAnim), _weapon];
	["switchMove", [_medic, _animation]] call CFUNC(globalEvent);
};

private _log = ["drag: ", simulationEnabled _target];
LOG(_log)
