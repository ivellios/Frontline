/*
    Function:       FRL_Medical_fnc_morphineStart
    Author:         Adanteh
    Description:    Handles starting morphine treatment
*/
#include "macros.hpp"

private _actionDuration = if ([_medic] call FUNC(isMedic)) then {
	[QGVAR(Settings_treatmentTimeMorphineMedic), 6] call MFUNC(cfgSetting);
} else {
	[QGVAR(Settings_treatmentTimeMorphine), 10] call MFUNC(cfgSetting);
};

_target setVariable [QGVAR(treatmentAmount), 1 / _actionDuration, true];
_target setVariable [QGVAR(treatmentProgress), _savedProgress, true];
