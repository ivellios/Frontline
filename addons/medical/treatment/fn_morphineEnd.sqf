/*
    Function:       FRL_Medical_fnc_morphineEnd
    Author:         Adanteh
    Description:    Morphine end function
*/
#include "macros.hpp"

[_target, false] call FUNC(setPain);
