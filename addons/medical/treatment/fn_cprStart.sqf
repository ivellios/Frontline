/*
    Function:       FRL_Medical_fnc_cprStart
    Author:         Adanteh
    Description:    Starts CPR treatment
*/
#include "macros.hpp"

private ["_tickEffect", "_treatmentDuration"];

private _tickInterval = [QGVAR(Settings_treatmentTickCPR), 1] call MFUNC(cfgSetting);
if ([_medic] call FUNC(isMedic)) then {
	_treatmentDuration = [QGVAR(Settings_treatmentTimeCPRMedic), 10] call MFUNC(cfgSetting);
	_tickEffect = [QGVAR(Settings_treatmentEffectCPRMedic), 13] call MFUNC(cfgSetting);
} else {
	_treatmentDuration = [QGVAR(Settings_treatmentTimeCPR), 10] call MFUNC(cfgSetting);
	_tickEffect = [QGVAR(Settings_treatmentEffectCPR), 8] call MFUNC(cfgSetting);
};

_target setVariable [QGVAR(treatmentAmount), 1 / _treatmentDuration, true];
_target setVariable [QGVAR(treatmentProgress), 0, true];

[{
	params ["_params", "_id"];
	_params params ["_medic", "_target", "_tickEffect", "_tickInterval"];

	if !(GVAR(treatmentInProgress)) exitWith {
		[_id] call MFUNC(removePerFrameHandler);
	};

	// -- Cancel treatment when person already died -- //
	if ([_target] call FUNC(isDead)) exitWith {
		[_id] call MFUNC(removePerFrameHandler);
	};

	// -- Cancel treatment when person is no longer incapped (Revived by someone else) -- //
	if !([_target] call FUNC(isUnconscious)) exitWith {
		[_id] call MFUNC(removePerFrameHandler);
	};

	// -- Timer system is only used if player is still bleeding
	if !([_target] call FUNC(isBleeding)) exitWith {
		[_id] call MFUNC(removePerFrameHandler);
	};

	// -- Only update every X seconds. This script is run every frame -- //
	if (time >= (_medic getVariable [QGVAR(nextTick), 0])) then {
		private _bleedoutTime = (_target getVariable [QGVAR(bleedoutTime), 0]) + _tickEffect;
		_target setVariable [QGVAR(bleedoutTime), _bleedoutTime, true];
		_medic setVariable [QGVAR(nextTick), time + _tickInterval, false];

		// -- Progress callback. This will increase bleedout time on patient client -- //
		// -- Update respawn counter with new bleedout time -- //
		missionNamespace setVariable ["RscRespawnCounter_Custom", _bleedoutTime - serverTime];
	};
}, 0, [_medic, _target, _tickEffect, _tickInterval]] call MFUNC(addPerFramehandler);
