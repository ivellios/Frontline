 /*
    Function:       FRL_Medical_fnc_treatmentStartLocal
    Author:         Adanteh
    Description:    Treatment started receiving
*/
#include "macros.hpp"

(_this select 0) params ["_medic", "_target", "_actionSlug"];

switch (toLower _actionSlug) do {
    case "diagnose": { }; // -- No Local
    case "bandage": { };
    case "adrenaline": { };
    case "morphine": { };

    case "drag": {
        [_medic, _target] call FUNC(dragStart);
    };

    case "cpr": { };

    default { };
};
