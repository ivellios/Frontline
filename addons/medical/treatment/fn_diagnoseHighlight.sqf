/*
    Function:       FRL_Medical_fnc_diagnoseHighlight
    Author:         Adanteh
    Description:    Highlights the required diagnose option
*/
#include "macros.hpp"

#define __HIGHLIGHT [1, 1, 1, 1]
#define __NORMAL [1, 1, 1, 0.5]


private _firstTreatment = (GVAR(diagnoseCache) select 1) param [0, "#"];

private _optionControls = + (uiNamespace getVariable [QGVAR(optionIDCS), []]);
for "_i" from 0 to (count _optionControls - 1) do {
    private _optionControl = (_optionControls select _i);
    if (_optionControl getVariable ["actionName", "-"] == _firstTreatment) then {
        (_optionControl controlsGroupCtrl 101) ctrlSetTextColor __HIGHLIGHT;
        (_optionControl controlsGroupCtrl 102) ctrlSetTextColor __HIGHLIGHT;
    } else {
        (_optionControl controlsGroupCtrl 101) ctrlSetTextColor __NORMAL;
        (_optionControl controlsGroupCtrl 102) ctrlSetTextColor __NORMAL;
    };
};

true;
