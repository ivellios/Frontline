/*
    Function:       FRL_Medical_fnc_treatmentEndLocal
    Author:         Adanteh
    Description:    Treatment succesfully received. Triggered at the end
    Locality:       Patient
*/
#include "macros.hpp"

(_this select 0) params ["_medic", "_target", "_actionSlug", "_finished"];

if !(_finished) exitWith { };

switch (toLower _actionSlug) do {
    case "diagnose": { }; // -- No Local

    case "bandage": {
        call FUNC(bandageEnd);
    };

    case "adrenaline": {
        call FUNC(adrenalineEnd);
    };

    case "morphine": {
        call FUNC(morphineEnd);
    };

    case "drag": {
        call FUNC(dragEnd);
    };
    
    case "cpr": { }; // -- No Local

    default { };
};

["MedicalEffect", [_actionSlug]] call CFUNC(localEvent);
