/*
    Function:       FRL_Medical_fnc_updateInteractUI
    Author:         Adanteh
    Description:    Updates the interact UI
*/
#include "macros.hpp"

private _optionsToShow = [];

// -- Check which options should be available
private _medikitSelect = (Clib_Player getVariable [QMVAR(fakeWeaponName), ""]) == "FRL_Medikit";
if (isNull GVAR(medicalTarget)) then { GVAR(medicalTarget) = Clib_Player };
private _selfTreatment = (GVAR(medicalTarget) isEqualTo Clib_Player);
private _enemyPlayer = (side group GVAR(medicalTarget)) != playerSide;

if !(_enemyPlayer) then {
    {
        call {
            if (_selfTreatment && { !((GVAR(medicalOptions) select 5) select _forEachIndex) }) exitWith { };
            if (!_medikitSelect && { ((GVAR(medicalOptions) select 7) select _forEachIndex) }) exitWith { };
            if !([Clib_Player, GVAR(medicalTarget)] call ((GVAR(medicalOptions) select 8) select _forEachIndex)) exitWith { };


            private _keybind = [(GVAR(medicalOptions) select 0) select _forEachIndex] call BIS_fnc_keyCode;
            private _keybindPicture = format ["%1%2\%3.paa", MEDIAPATH, "keybinds", _keybind];
            _optionsToShow pushBack [_x, _forEachIndex, _keybindPicture];
        };
    } forEach (GVAR(medicalOptions) select 1);
};

// -- If no options are present, remove the HUD
if (count _optionsToShow == 0) exitWith { QGVAR(interactUI) cutFadeOut 0.2; };

// -- If options are available, make sure the HUD shows and add the options to it
_display = (uiNamespace getVariable [QGVAR(interactUI), displayNull]);
if (isNull _display) then {
    QGVAR(interactUI) cutRsc [QGVAR(interactUI), "PLAIN", 0];
    _display = uiNamespace getVariable [QGVAR(interactUI), displayNull];
};

// -- Update the indicator on who you are meddicing
private _targetText = if (_selfTreatment) then {
    "Treating Yourself"
} else {
    format ["Treating %1", GVAR(medicalTarget) call CFUNC(name)];
};
(_display displayCtrl 2) ctrlSetText _targetText;

// -- Using control groups created on the fly because normal list boxes look shit and lack options
// -- Might as well set the code up for control groups right away and not have to deal with redoing it later on
private _listBox = (_display displayCtrl 3);
{
    ctrlDelete _x;
} forEach (uiNamespace getVariable [QGVAR(optionIDCS), []]);

private _controls = [];
{
    _x params ["_actionName", "_actionIndex", "_actionKeybindPicture"];
    private _control = _display ctrlCreate [QGVAR(treatmentOption), (1000 + _forEachIndex), _listBox];
    (_control controlsGroupCtrl 101) ctrlSetText _actionName;
    (_control controlsGroupCtrl 102) ctrlSetText _actionKeybindPicture;
    _control ctrlSetPosition [0, GRIDY(1.5) * _forEachIndex];
    _control ctrlCommit 0;
    _control setVariable ["actionName", _actionName];
    _controls pushBack _control;
} forEach _optionsToShow;

uiNamespace setVariable [QGVAR(optionIDCS), _controls];

// -- Auto diagnose for medics
if ([Clib_Player] call FUNC(isMedic)) then {
    [true] call FUNC(diagnoseEnd);
};

// -- Highlight diagnose options if this target was already diagnosed, without rediagnosing someone else
// -- This helps in that if you lose sight of the patient for a second, you don't need to diagnose right away again
if (GVAR(medicalTarget) isEqualTo (GVAR(diagnoseCache) select 0)) then {
    call FUNC(diagnoseHighlight);
};
