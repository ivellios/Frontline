/*
    Function:       FRL_Medical_fnc_stopTreatment
    Author:         Adanteh
    Description:    Shared function to stop treatment, close progress bar (if opened) and reset vars
*/
#include "macros.hpp"

params [["_finished", false]];

//systemChat format ["stopTreatment: '%1'", _this];
if (GVAR(treatmentCurrent) == -1) exitWith { };
private _endCode = (GVAR(medicalOptions) select 4) select GVAR(treatmentCurrent);
private _endReturn = [Clib_Player, GVAR(medicalTarget), -1, _finished] call _endCode;
GVAR(treatmentCurrentButton) = -1;
GVAR(treatmentCurrent) = -1;
GVAR(treatmentInProgress) = false;

if !(isNull (uiNamespace getVariable [QGVAR(progressBar), displayNull])) then {
    QGVAR(progressBar) cutFadeOut 0;
};
