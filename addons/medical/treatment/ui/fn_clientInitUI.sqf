/*
    Function:       FRL_Medical_fnc_clientInitUI
    Author:         Adanteh
    Description:    Initializes the UI / Interact for the medical system.
*/
#include "macros.hpp"

GVAR(treatmentInProgress) = false;
GVAR(treatmentCurrentButton) = -1;
GVAR(treatmentCurrent) = -1;

GVAR(medicalTarget) = objNull;
GVAR(medikitSelected) = false;

GVAR(diagnoseCache) = [objNull, []];
GVAR(buttonHold) = -1;

["missionStarted", {
    (findDisplay 46) displayAddEventHandler ["KeyDown", {
        params ["", "_button"];

        // -- Cancel treatment when you press Esc
        if (GVAR(treatmentInProgress)) exitWith {
            if (_button == 1) then {
                [false] call FUNC(stopTreatment);
            }; // -- Esc
            false;
        };

        // -- Check if keybind matches a treatment option
        private _actionIndex = (GVAR(medicalOptions) select 0) find _button;
        if (_actionIndex == -1) exitWith { false };

        if (GVAR(buttonHold) == _button) exitWith { true };
        GVAR(buttonHold) = _button;

        // -- Check if self treatment is allowed
        if ((GVAR(medicalTarget) isEqualTo Clib_Player) && { !((GVAR(medicalOptions) select 5) select _actionIndex) }) exitWith { false };

        private _conditionCode = (GVAR(medicalOptions) select 3) select _actionIndex;
        private _conditionReturn = [Clib_Player, GVAR(medicalTarget), _actionIndex] call _conditionCode;
        if !(_conditionReturn) exitWith { false };

        GVAR(treatmentInProgress) = true;
        GVAR(treatmentCurrent) = _actionIndex;
        GVAR(treatmentCurrentButton) = _button;
        private _actionCode = (GVAR(medicalOptions) select 2) select _actionIndex;
        private _actionReturn = [Clib_Player, GVAR(medicalTarget), _actionIndex] call _actionCode;
        _actionReturn

    }];

    (findDisplay 46) displayAddEventHandler ["KeyUp", {
        params ["", "_button"];

        if (GVAR(buttonHold) == _button) then {
            GVAR(buttonHold) = -1;
        };

        if (GVAR(treatmentCurrentButton) == _button) exitWith {
            [false] call FUNC(stopTreatment);
            true
        };

        false
    }];
}] call CFUNC(addEventHandler);

// -- This is a detection of players in middle of screen. Works way better than cursorObject/target
["interactTargetChanged", {
    (_this select 0) params ["_target"];
    if (_target isKindOf "CAManBase") then {
        if (GVAR(medicalTarget) != _target) then {
            GVAR(medicalTarget) = _target;
            private _isFriendly = (side group _target == side group CLib_Player);

            if ( [_target] call FUNC(isUnconscious)) then {
                if (_isFriendly) then {
                    ["revive"] call FUNC(showHints);
                };
            } else {
                if (_isFriendly && [_target] call FUNC(isBleeding)) then {
                    ["bleedingTgt"] call FUNC(showHints);
                };
            };
        };

    } else {
        GVAR(medicalTarget) = Clib_Player;
    };
    call FUNC(updateInteractUI);
}] call CFUNC(addEventHandler);

// -- Loop it just in case
[{
    if (isNull GVAR(medicalTarget) || { GVAR(medicalTarget) distance Clib_Player > 10 }) then {
        GVAR(medicalTarget) = Clib_Player;
    };

    [] call FUNC(updateInteractUI);
}, 0.5] call MFUNC(addPerFramehandler);
