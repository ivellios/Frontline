/*
    Function:       FRL_Medical_fnc_showProgressBar
    Author:         Adanteh
    Description:    Shows the treatment progress bar
*/
#include "macros.hpp"

params ["_actionSlug", "_treatingSelf"];

private _display = uiNamespace getVariable [QGVAR(progressBar), displayNull];
if (isNull _display) then {
    QGVAR(progressBar) cutRsc [QGVAR(progressBar), "PLAIN", 0.1, false];
    _display = uiNamespace getVariable [QGVAR(progressBar), displayNull];
};

(_display displayCtrl 1) progressSetPosition 0;
(_display displayCtrl 10) ctrlSetText (getText (configFile >> "FRL" >> "MedicalTreatments" >> _actionSlug >> "verbName"));

[{
    params ["_params", "_id"];
    _params params ["_display", "_target"];

    if (GVAR(treatmentCurrent == -1)) exitWith {
        [_id] call MFUNC(removePerFrameHandler);
    };

    if (([_target] call FUNC(isDead)) || {([CLib_Player] call FUNC(isUnconscious))} || dialog) then {
        [false] call FUNC(stopTreatment);
    };

    // -- Update medical progress
    private _treatmentAmount = _target getVariable [QGVAR(treatmentAmount), 0];
    private _treatmentProgress = _target getVariable [QGVAR(treatmentProgress), 0];
    private _treatmentStartTime = _target getVariable [QGVAR(treatmentStartTime), time];
    _treatmentProgress = _treatmentProgress + (time - _treatmentStartTime) * _treatmentAmount;

    if (_treatmentProgress >= 1) then {
        [true] call FUNC(stopTreatment);
    };

    (_display displayCtrl 1) progressSetPosition _treatmentProgress;
}, 0, [_display, _target]] call MFUNC(addPerFramehandler);
