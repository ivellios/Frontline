/*
    Function:       FRL_Medical_fnc_treatmentGesture
    Author:         Adanteh
    Description:    Plays (or ends) a gesture belonging to the given treatment option
*/
#include "macros.hpp"

params ["_actionSlug", ["_toggle", false]];

if (_toggle) then {
	// -- Determine which animation to play and which animation to end in (Based on current stance) -- //
	["forceWalk","Medical", true] call CFUNC(setStatusEffect);

	private _gesture = getText (configFile >> "FRL" >> "MedicalTreatments" >> _actionSlug >> "gestureUse");
	if (_gesture == "") exitWith { };

	GVAR(gestureHandle) = [{
		CLib_Player playActionNow (_this select 0);
	}, 3.5, _gesture] call MFUNC(addPerFramehandler);

} else {
	[GVAR(gestureHandle)] call MFUNC(removePerFrameHandler);
	["forceWalk","Medical", false] call CFUNC(setStatusEffect);
};
