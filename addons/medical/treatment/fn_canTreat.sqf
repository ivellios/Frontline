/*
    Function:       FRL_Medical_fnc_canTreat
    Author:         Adanteh
    Description:    General treatment condition
*/
#include "macros.hpp"

params ["_actionSlug", "_args"];
_args params ["_caller", "_target", "_index"];

private _medikitSelect = (_caller getVariable [QMVAR(fakeWeaponName), ""]) == "FRL_Medikit";
if (!_medikitSelect && { ((GVAR(medicalOptions) select 7) select _index)}) exitWith {
    false;
};

// systemChat str ["#1"];

if ([_caller] call FUNC(isUnconscious)) exitWith {
    false;
};

if (!(_target isKindOf "CAManBase") || {([_target] call FUNC(isDead))}) exitWith {
    false;
};

//systemChat str ["#2"];


if !(isNull (_target getVariable [QGVAR(draggedBy), objNull])) exitWith {
    false;
};

//systemChat str ["#3"];


if ((side group _target) getFriend playerSide < 0.6) exitWith {
    false;
};

if ((tolower _actionSlug in ["cpr", "drag"]) && {!([_target] call FUNC(isUnconscious))}) exitWith {
    false;
};

// TODO, make this use target items
private _itemRequired = getText (configFile >> "FRL" >> "MedicalTreatments" >> _actionSlug >> "itemRequired");
private _hasItem = if (_itemRequired != "") then {

    if (({ _itemRequired == _x } count (items _caller)) > 0) exitWith { // -- You have the item
        GVAR(usingTargetItem) = false;
        true
    };

    if ((_caller != _target) && { (({ _itemRequired == _x } count (items _target)) > 0) }) exitWith { // -- The patient has the item you need
        GVAR(usingTargetItem) = true;
        true;
    };

    false;
} else {
    true
};

if !(_hasItem) exitWith {
    private _itemName = getText (configFile >> "CfgWeapons" >> _itemRequired >> "displayName");
    ["show", [format ["You don't have a %1 left", _itemName], 1]] call MFUNC(showNotification);
    false;
};

//systemChat str ["#4"];

true;
