/*
    Function:       FRL_Medical_fnc_treatmentKeepProgress
    Author:         Adanteh
    Description:    Handles saving the progress of unfinished medical treatment, so you can interrupt and continue where you left off, with gradual lowering of said progress
*/
#include "macros.hpp"

params ["_target", "_actionSlug"];

private _treatmentAmount = _target getVariable [QGVAR(treatmentAmount), 0];
private _treatmentProgress = _target getVariable [QGVAR(treatmentProgress), 0];
private _treatmentStartTime = _target getVariable [QGVAR(treatmentStartTime), time];
_treatmentProgress = _treatmentProgress + (time - _treatmentStartTime) * _treatmentAmount;

_target setVariable [QGVAR(treatmentEndTime), time];
_target setVariable [QGVAR(treatmentSaved), _actionSlug];
_target setVariable [QGVAR(treatmentProgressSaved), _treatmentProgress];

#define __TIMEOUTTIME 3
#define __LOWERTIME 10

// -- Cancel dropping if treatment status changed already -- //
[{
	(_this select 0) params ["_target"];

	// -- General timeout for progress starts dropping -- //
	private _treatmentEndTime = _target getVariable [QGVAR(treatmentEndTime), time];
	if ([_target] call FUNC(isDead)) exitWith { };
	if ((time - _treatmentEndTime) >= __TIMEOUTTIME) then {
		private _treatmentProgress = _target getVariable [QGVAR(treatmentProgressSaved), 0];
		_treatmentProgress = _treatmentProgress - ((1/__LOWERTIME) * 0.25);

		if (_treatmentProgress <= 0) then {
			_target setVariable [QGVAR(treatmentProgressSaved), nil];
			_target setVariable [QGVAR(treatmentEndTime), nil];
			_target setVariable [QGVAR(treatmentSaved), nil];
			[(_this select 1)] call MFUNC(removePerFrameHandler);
		} else {
			_target setVariable [QGVAR(treatmentProgressSaved), _treatmentProgress];
		};
	};

}, 0.25, [_target]] call MFUNC(addPerFramehandler);
