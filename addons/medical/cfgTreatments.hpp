class MedicalTreatments {

    class Diagnose {
        scope = 0;
        displayName = "Diagnose";
        verbName = "Diagnosing";
        gestureUse = "";
        itemRequired = "";
        conditionCode = "['diagnose', _this] call frl_medical_fnc_canTreat";
        conditionShow = "!([(_this select 0)] call frl_medical_fnc_isUnconscious);";
        actionCode = "['diagnose', _this] call frl_medical_fnc_treatmentStart";
        returnCode = "['diagnose', _this] call frpl_medical_fnc_treatmentEnd";
        dikKey = DIK_F; // F
        allowSelf = 1;
        keepProgress = 1;
        needsMedikit = 1;
    };

    class Bandage: Diagnose {
        scope = 2;
        displayName = "Bandage";
        verbName = "Bandaging";
        itemRequired = "FRL_fieldDressing";
        gestureUse = "HandSignalFreeze";
        conditionCode = "['bandage', _this] call frl_medical_fnc_canTreat";
        actionCode = "['bandage', _this] call frl_medical_fnc_treatmentStart";
        returnCode = "['bandage', _this] call frl_medical_fnc_treatmentEnd";
        dikKey = DIK_R; //R
        needsMedikit = 1;
    };

    class Adrenaline: Bandage {
        displayName = "Adrenaline";
        verbName = "Injecting Adrenaline";
        itemRequired = "FRL_epinephrine";
        gestureUse = "HandSignalFreeze";
        conditionCode = "['adrenaline', _this] call frl_medical_fnc_canTreat";
        actionCode = "['adrenaline', _this] call frl_medical_fnc_treatmentStart";
        returnCode = "['adrenaline', _this] call frl_medical_fnc_treatmentEnd";
        dikKey = DIK_T; // T
        needsMedikit = 1;
  };

    class Morphine: Adrenaline {
        scope = 0;
        displayName = "Morphine";
        verbName = "Injecting Morphine";
        itemRequired = "FRL_morphine";
        gestureUse = "HandSignalFreeze";
        conditionCode = "['morphine', _this] call frl_medical_fnc_canTreat";
        actionCode = "['morphine', _this] call frl_medical_fnc_treatmentStart";
        returnCode = "['morphine', _this] call frl_medical_fnc_treatmentEnd";
        dikKey = DIK_Y; // Y
        needsMedikit = 1;
    };

    class Drag: Diagnose {
        scope = 2;
        displayName = "Drag";
        verbName = "Dragging";
        itemRequired = "";
        allowSelf = 0;
        keepProgress = 0;
        instantAction = 1;
        conditionCode = "['drag', _this] call frl_medical_fnc_canTreat";
        conditionShow = "!([(_this select 0)] call frl_medical_fnc_isUnconscious) && { ([(_this select 1)] call frl_medical_fnc_isUnconscious); }";
        actionCode = "['drag', _this] call frl_medical_fnc_treatmentStart";
        returnCode = "['drag', _this] call frl_medical_fnc_treatmentEnd";
        dikKey = DIK_E; // E
        needsMedikit = 0;
    };

    class CPR: Drag {
        scope = 0;
        displayName = "CPR";
        verbName = "Applying CPR";
        instantAction = 0;
        conditionCode = "['cpr', _this] call frl_medical_fnc_canTreat";
        actionCode = "['cpr', _this] call frl_medical_fnc_treatmentStart";
        returnCode = "['cpr', _this] call frl_medical_fnc_treatmentEnd";
        dikKey = DIK_Q; // Q
        needsMedikit = 0;
    };
};
