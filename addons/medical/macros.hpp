// -- Module defines -- //
#define MODULE Medical

// -- Global defines -- //
#include "..\main\macros_local.hpp"



//#define DEBUGFULL
#ifdef DEBUGFULL
  #define LOG(var1) diag_log (var1);\
  systemChat str (var1);\
  hintSilent str (var1);
#else
  #define LOG(var1)
#endif
