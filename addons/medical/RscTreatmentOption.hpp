#define SIZE_WIDTH 			GRIDX(8)

class GVAR(treatmentOption): RscControlsGroupNoScrollbars {
  x = 0;
  y = 0;
  w = SIZE_WIDTH;
  h = GRIDY(1.5);
  class Controls {
    class TreatmentLab: RscText {
      idc = 101;
      size = GRIDY(1);
      sizeEx = GRIDY(1);
      x = GRIDX(1.25);
      w = SIZE_WIDTH - GRIDX(1.25);
      y = GRIDX(0);
      h = GRIDY(1.25);

      font = FONTLIGHT;
      text = "";
      colorText[] = {1, 1, 1, 1};
      colorBackground[] = {0, 0, 0, 0};
      colorShadow[] = {0, 0, 0, 0.5};
      shadow = 1;
    };

    class TreatmentPicture: RscPictureKeepAspect {
      idc = 102;
      x = 0;
      y = GRIDX(0.25);
      h = GRIDY(1.25);
      w = GRIDX(1.25);
      text = "";
    };
  };
};
