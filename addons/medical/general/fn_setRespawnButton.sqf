/*
    Function:       FRL_Medical_fnc_setRespawnButton
    Author:         Adanteh
    Description:    Adjust the respawn button action to properly respawn, plus prevents clicking respawn button for a while
*/
#include "macros.hpp"

disableSerialization;

(_this select 0) params [
	["_mode", "", [""]],
	["_params", [displayNull]]
];

VAR(_return) = true;
switch (toLower _mode) do {
	// -- Overwrite the default button behaviour -- //
	case "onload": {
        if !(isMultiplayer) exitWith { };

		VAR(_display) = _params select 0;
		VAR(_buttonRespawn) = _display displayctrl ([104, 1010] select isMultiplayer);
		VAR(_event) = format ["[['buttonrespawn', _this]] spawn %1; true;", _fnc_scriptName]; // Need spawn because of guiMessage function
		_buttonRespawn ctrlSeteventhandler ["buttonclick", _event];


		if ([CLib_Player] call FUNC(isUnconscious)) then {
			if (missionNamespace getVariable [QEGVAR(respawnUI,debugSpawn), false]) exitWith { };
			_buttonRespawn ctrlEnable false;
			[_display, _buttonRespawn] spawn {
				disableSerialization;
				params ["_display", "_buttonRespawn"];
				while { !(isNull _display) } do {
					private _incappedSince = ((CLib_Player getVariable [QGVAR(bleedoutInfo), []]) param [2, -1]);
					#ifdef ISDEV
						private _waitTime = 5;
					#endif
					#ifndef ISDEV
						private _waitTime = ([QEGVAR(RespawnUI,RespawnSettings_minimumIncapTime), 45] call MFUNC(cfgSetting));
					#endif
					private _timeTillRespawn = _waitTime - (serverTime - _incappedSince);
					if (_timeTillRespawn <= 0) exitWith { };
					_buttonRespawn ctrlEnable false;
					_buttonRespawn ctrlSetText format ["RESPAWN (%1s)", ceil _timeTillRespawn];
					_buttonRespawn ctrlSetTooltip format ["Wait %1 seconds before you can respawn", ceil _timeTillRespawn];
				};
				_buttonRespawn ctrlEnable true;
				_buttonRespawn ctrlSetText "RESPAWN";
				_buttonRespawn ctrlSetTooltip "";
			};
		};

	};

	case "buttonrespawn": {
		VAR(_display) = ctrlparent (_params select 0);
		VAR(_respawn) = [localize "str_a3_rscdisplaympinterrupt_respawnprompt",nil,localize "STR_DISP_INT_RESPAWN",true,_display,true] call bis_fnc_guiMessage;
		VAR(_unit) = player;
		if (_respawn) then {
			if (alive _unit) then {
				missionNamespace setVariable ["RscDisplayMPInterrupt_respawnTime", time];
				["setDead", [_unit, _unit]] call CFUNC(localEvent);
			};
			_display closedisplay 2;
		};
		_return = true;
	};
};

_return
