/*
    Function:       FRL_Medical_fnc_clientInit
    Author:         Adanteh
    Description:    Main clientInit for medical system
*/
#include "macros.hpp"

GVAR(instaKill) = false;

["HandleDamage", FUNC(ehDamage)] call CFUNC(addEventHandler);
["missionStarted", {
    ["Killed", FUNC(setDead)] call CFUNC(addEventHandler);
    ["escMenu", FUNC(setRespawnButton)] call CFUNC(addEventHandler);
}] call CFUNC(addEventHandler);


MVAR(ignoreVariables) append [QGVAR(draggedBy), QGVAR(bleedoutInfo)];

// -- Vanilla healing compatability
private _code = compile format ["[""%1"", _this] call %2", "HandleHeal", QCFUNC(localEvent)];
private _index = CLib_Player addEventHandler ["HandleHeal", _code];
["playerChanged", {
    params ["_data", "_params"];
    _data params ["_currentPlayer", "_oldPlayer"];
    _params params ["_name", "_code", "_index"];

    _oldPlayer removeEventHandler [_name, _index];
    _currentPlayer removeEventHandler [_name, _index];
    _params set [2, _currentPlayer addEventHandler [_name, _code]];
}, ["HandleHeal", _code, _index]] call CFUNC(addEventHandler);

["HandleHeal", {
    (_this select 0) params ["_patient", "_healer"];
	private _damage = damage _patient;
	if (_patient == _healer) then {
        [{
            params ["_patient", "_damage"];
            if (damage _patient < _damage) then {
                [_patient, false] call FUNC(setBleeding);
                [_patient, damage _patient] call FUNC(setDamage);
            };
        },
        {
            params ["_patient", "_damage"];
            damage _patient != _damage
        }, [_patient, _damage]] call CFUNC(waitUntil);
	};
}] call CFUNC(addEventHandler);
