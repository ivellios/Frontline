/*
    Function:       FRL_Medical_fnc_clientInitIcons
    Author:         Adanteh
    Description:    Inits the 3D icons for medical, indicating wounded palyers
*/
#include "macros.hpp"
#define __TAG_DIST 25

// -- Add on role assigned role -- //
GVAR(tags_handle) = -1;
[QSVAR(roleChanged), {
    (_this select 0) params ["_role"];

    // -- If new role of unit has Medic capabilities, start the drawing of the UI -- //
    if ([_role, "medic"] call EFUNC(rolesspawns,roleHasAbility)) then {
        // -- Only add the handle if it's not running yet -- //
       if (GVAR(tags_handle) == -1) then {
            GVAR(tags_handle) = addMissionEventHandler ["Draw3D", {
                if (!(alive CLib_Player) || !(isNull (findDisplay 49)) || (dialog)) exitWith { };
                // -- GO through list and draw icon -- //
                {
                    // -- Check if unit is withinview -- //
                    _x params ["_unit", "_icon"];
                    VAR(_tagPositionAGL) = _unit modelToWorldVisual (_unit selectionPosition "spine2");
                    if !((worldToScreen _tagPositionAGL) isEqualTo []) then {
                        drawIcon3D [_icon, [1,0.3,0.3,1], _tagPositionAGL,  1, 1, 0, "", 2, 0.15 * 1, "RobotoCondensed", "center", false];
                    };
                    nil
                } count ([QGVAR(cache_tags), {
                    VAR(_nearby) = (_this select 0) nearObjects ["CaManBase", __TAG_DIST];
                    VAR(_return) = [];
                    {
                        if !(_x isEqualTo CLib_Player) then {
                            // -- Same side only -- //
                            if (side group _x == playerSide) then {
                                private _unit = _x;
                                private _icon = call {
                                    if ([_unit] call FUNC(isDead)) exitWith { "\pr\frl\addons\medical\ui\hud\dead.paa" };
                                    if ([_unit] call FUNC(isUnconscious)) exitWith { "\pr\frl\addons\medical\ui\hud\incap.paa" };
                                    if ([_unit] call FUNC(isBleeding)) exitWith { "\pr\frl\addons\medical\ui\hud\bleeding.paa" };
                                    if ([_unit] call FUNC(isInPain)) exitWith { "\pr\frl\addons\medical\ui\hud\pain.paa" };
                                    if (damage _unit > 0.01) exitWith { "\pr\frl\addons\medical\ui\hud\injured.paa" };
                                    "";
                                };

                                // -- Get the icon in the following priority -- //
                                if (_icon != "") then {
                                    _return pushBack [_unit, _icon];
                                };
                            };
                        };
                        nil
                    } count _nearby;
                    _return;
                }, [Clib_Player], 2] call CFUNC(cachedCall));
            }];
       };
    } else {
        // -- If new role doesn't have Medic capabilities and the UI is currently running, stop it -- //
        if (GVAR(tags_handle) != -1) then {
            removeMissionEventHandler ["Draw3D", GVAR(tags_handle)];
            GVAR(tags_handle) = -1;
        };
    };
}] call CFUNC(addEventHandler);
