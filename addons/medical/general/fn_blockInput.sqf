/*
    Function:       FRL_Medical_fnc_blockInput
    Author:         Adanteh
    Description:    Blocks some input actions that cause issues when incapped.
*/
#include "macros.hpp"

params ["_toggle"];

if (_toggle) then {
	if (isNil QGVAR(blockInput)) then {
		GVAR(blockInput) = (findDisplay 46) displayAddEventHandler ["KeyDown", {
		    params ["", "_key"];

		    if (_key in actionKeys "Throw") exitWith { true };
		    if (_key in actionKeys "DeployWeaponAuto") exitWith { true };
		    if (_key in actionKeys "CarForward") exitWith { true };
		    if (_key in actionKeys "CarFastForward") exitWith { true };
		    false;
		}];
	};
} else {
	if !(isNil QGVAR(blockInput)) then {
		(findDisplay 46) displayRemoveEventHandler ["KeyDown", GVAR(blockInput)];
		GVAR(blockInput) = nil;
	};
};
