/*
    Function:       FRL_Medical_fnc_ehDamage
    Author:         Adanteh
    Description:    Damage handler, prevents damage when incapped
*/
#include "macros.hpp"

(_this select 0) params ["_unit", "_selection", "_damage", "_source", "_projectile", "_index"];

if (!(local _unit) || !(alive _unit) || (_unit != player)) exitWith {};
if (!(isNull objectParent _unit) && {!alive objectParent _unit}) exitWith {
    _CLib_EventReturn = 1;
    1
};

if (_index == -1) then {
    [_unit, _source, (_damage - (damage _unit))] call FUNC(ehHit);
};

if (_index <= 7 && !(_unit getVariable ["Unconscious", false])) then {
    if (_damage >= 1) then {
        if ([_unit] call FUNC(reviveTime) select 0) then {
            [_unit, true, _source] call FUNC(setUnconscious);
            _damage = _damage min 0.95;
        } else {
            _damage = 1;
        };
    };
} else {
    _damage = _damage min 0.95;
};

_CLib_EventReturn = _damage;
_damage;
