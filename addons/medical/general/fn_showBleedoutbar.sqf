/*
    Function:       FRL_Medical_fnc_showBleedoutBar
    Author:         Adanteh
    Description:    Shows bleedout bar with keybind to respawn
*/
#define __INCLUDE_DIK
#include "macros.hpp"

#define DISPBAR (uiNamespace getVariable ['FRL_Medical_bleedoutBar', displayNull])
#define HOLDTIME 1.5

params [["_toggle", true]];

if (_toggle) then {
    private _display = DISPBAR;
    if (isNull _display) then {
        QGVAR(bleedoutBar) cutRsc [QGVAR(bleedoutBar), "PLAIN", 0.1, true];
        _display = DISPBAR;
    };

    private _baseText = format ["<img size='1.25' image='%1%2\%3.paa'/>   Hold to respawn", MEDIAPATH, "keybinds", "space"];

    GVAR(spaceHoldStart) = -1;
    private _handle = missionNamespace getVariable [QGVAR(bleedoutPFH), -1];
    if (_handle == -1) then {

        GVAR(bleedoutPFH) = [{
            (_this select 0) params ["_baseText"];
            private _display = DISPBAR;



            // -- Grey out the space bar text if you can't respawn yet
            private _timeIncapped = serverTime - ((CLib_Player getVariable [QGVAR(bleedoutInfo), []]) param [2, -1]);
            private _timeTillRespawn = ([QEGVAR(RespawnUI,RespawnSettings_minimumIncapTime), 45] call MFUNC(cfgSetting)) - _timeIncapped;

            if (_timeTillRespawn <= 0) then {
                (_display displayCtrl 10) ctrlSetTextColor [1, 1, 1, 1];
                (_display displayCtrl 10) ctrlSetStructuredText parseText _baseText;
            } else {
                (_display displayCtrl 10) ctrlSetTextColor [1, 1, 1, 0.5];
                (_display displayCtrl 10) ctrlSetStructuredText parseText format ["%1 (Allowed in %2s)", _baseText, ceil _timeTillRespawn];
            };


            // -- Set the text within the bar to show your bleedout time if bleeding, else just 'not bleeding'
            private _text = if ([Clib_Player] call FUNC(isBleeding)) then {
                private _bleedoutAt = (CLib_Player getVariable [QGVAR(bleedoutTime), -1]);
                (format ["%1 Till death", ([_bleedoutAt - serverTime, "MM:SS"] call bis_fnc_secondsToString)])
            } else {
                "Not bleeding";
            };

            (_display displayCtrl 3) ctrlShow true;
            (_display displayCtrl 3) ctrlSetText _text;

            if ((_timeTillRespawn <= 0) && { [DIK_SPACE] call MFUNC(keyPressed) }) then {
                if (GVAR(spaceHoldStart) == -1) then {
                    GVAR(spaceHoldStart) = time;
                };

                private _progressHold = ((time - GVAR(spaceHoldStart)) * (1 / HOLDTIME));
                (_display displayCtrl 1) ctrlShow true;
                (_display displayCtrl 1) progressSetPosition _progressHold;
                if (_progressHold >= 1) then {
                    ["setDead", [Clib_Player, Clib_Player]] call CFUNC(localEvent);
                };
            } else {
                (_display displayCtrl 1) ctrlShow false;
                GVAR(spaceHoldStart) = -1;
            };

        }, 0, [_baseText]] call MFUNC(addPerFramehandler);
    };

} else {
    [missionNamespace getVariable [QGVAR(bleedoutPFH), -1]] call MFUNC(removePerFrameHandler);
    QGVAR(bleedoutBar) cutFadeOut 0;
    GVAR(spaceHoldStart) = -1;
    GVAR(bleedoutPFH) = -1
};
