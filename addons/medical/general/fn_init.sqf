/*
    Function:       FRL_Medical_fnc_init
    Author:         Adanteh
    Description:    Shared server and client init for medical system. Adds stacked conditions and loads general settings (Run before all other inits)
*/
#include "macros.hpp"

[QGVAR(Settings), configFile >> "FRL" >> "CfgMedical"] call MFUNC(cfgSettingLoad);
if (isNil QGVAR(debugMode)) then { GVAR(debugMode) = false; };

// --Disallow incapped units from capping. Use stacked condition -- //
["isUnconscious", FUNC(isUnconscious)] call MFUNC(addCondition);
["isAlive", FUNC(isAlive)] call MFUNC(addCondition);
["isDead", FUNC(isDead)] call MFUNC(addCondition);
["isMedic", FUNC(isMedic)] call MFUNC(addCondition);
["canCapture", { !(_this call MFUNC(isUnconscious)) }] call MFUNC(addCondition);

["setDead", FUNC(setDead)] call CFUNC(addEventHandler);
