/*
    Function:       FRL_Medical_fnc_saveIncapBy
    Author:         Adanteh
    Description:    Saves the full crew of the vehicle causing damage, so it can be used to award score (These days _instigator in ehDamage can be used, but we prefer rewarding full crew)
*/
#include "macros.hpp"

params ["_unit", "_source"];

// -- Allows resetting the saved killers on respawn and similar -- //
if (isNull _source) exitWith { _unit setVariable [QGVAR(killedBy), []]; [] };

// -- If a player blows up it's own vehicle don't penalize anyone else for it -- //
if (_source == (vehicle _unit)) exitWith {  _unit setVariable [QGVAR(killedBy), [_unit]]; [_unit] };

// -- If soldier save the soldier -- //
if (_source isKindOf "CaManBase") exitWith { _unit setVariable [QGVAR(killedBy), [_source]]; [_source] };

private _sourceUnits = [];
// -- If not soldier check crew of source -- //
{
	_x params ["_unit", "_role"];
	if (toLower _role in ["commander", "gunner", "turret", "driver"]) then {
		_sourceUnits pushBack _unit;
	};
	nil
} count (fullCrew _source);
_unit setVariable [QGVAR(killedBy), _sourceUnits];
_sourceUnits;
