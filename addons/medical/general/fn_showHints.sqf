/*
    Function:       FRL_Medical_fnc_showHints
    Author:         N3croo
    Description:    A Function That Does stuff
     ["nomedic"] call FRL_Medical_fnc_showHints;
*/
#include "macros.hpp"

// -- no need to tell dead ppl to revive or bandage
if (!(alive CLib_Player) || !(isNil QGVAR(bleedoutInfo)) || [CLib_Player] call FUNC(isUnconscious)) exitWith {};

params ["_hintName"];

_hintName = tolower _hintName;

if (_hintName in ["revive", "bleedingtgt", "selfbleed"] &&
    {!(isNil QGVAR(medicaTooltipCD)) && GVAR(medicaTooltipCD) > time}
) exitWith {};

GVAR(medicaTooltipCD) = time + 30;

private _txt = "";
private _icon = "";
private _color = "";

private _exit = false;

switch (_hintName) do {
    case "selfbleed": {
        if (
            [Clib_Player] call FUNC(hasBandages) &&
            {currentWeapon Clib_Player isEqualTo "FRL_FakeHandgunWeapon"}
        ) exitWith {_exit = true};
        _icon = "\pr\frl\addons\medical\ui\hud\bleeding.paa";
        _color = "green";
        // "<t align='center'>You are bleeding, to stop the bleeding:</t>
        // <t shadow='1'>Text with shadow</t>
        // -- shadows dont do anything
        _txt = "<br/>You are bleeding, to stop the bleeding:<br/>Select your medikit and apply a bandage.";
        if !( [CLib_Player] call FUNC(isMedic) ) then {
            [ { ["nomedic"] call FUNC(showHints) }, 6] call CFUNC(wait);
        };
    };
    case "nomedic": {
        _icon = "\pr\frl\addons\main\icons\alert\warning.paa";
        _color = "white";
        _txt = "<br/>Consecutive bandages are of limited value<br/>only a medic can fully restore health";
    };
    case "revive": {
        _icon = "\pr\frl\addons\medical\ui\hud\incap.paa";
        _color = "green";
        _txt = "To revive a teammate:<br/><br/>Open the medical menu<br/>apply 1x bandage then inject adrenaline";
        if !( [CLib_Player] call FUNC(isMedic) ) then {
            [ { ["nomedic"] call FUNC(showHints) }, 5] call CFUNC(wait);
        };

    };
    case "bleedingtgt": {
        _icon = "\pr\frl\addons\medical\ui\hud\bleeding.paa";
        _color = "green";
        _txt = "<br/>The teammate you are looking at is bleeding<br/><br/>Select your medikit and apply a bandage.";
        if !( [CLib_Player] call FUNC(isMedic) ) then {
            [ { ["nomedic"] call FUNC(showHints) }, 6] call CFUNC(wait);
        };
    };
    default {};
};

if (_exit) exitWith{};

[_txt, _icon, _color] call MFUNC(tooltipShow);
