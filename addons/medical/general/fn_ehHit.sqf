/*
    Function:       FRL_Medical_fnc_ehHit
    Author:         Adanteh
    Description:    Hit handler for medical system. Checks overal damage received from a hit and causes effects
    [player,player, 0.11] call FRL_Medical_fnc_ehHit
*/
#include "macros.hpp"

params ["_unit", "_shooter", "_damage"];

if (!(local _unit) || !(alive _unit) || (_unit != player)) exitWith { 0 };
if !(_unit getVariable ["Unconscious", false]) then {
    if !(_unit getVariable [QGVAR(bleeding), false]) then {
        if (_damage > 0.1) then {

            [_unit, true] call FUNC(setBleeding);
			private _hitSoundFile = selectRandom [
				"A3\sounds_f\characters\human-sfx\P01\Low_hit_1.wss",
				"A3\sounds_f\characters\human-sfx\P01\Low_hit_2.wss",
				"A3\sounds_f\characters\human-sfx\P01\Low_hit_3.wss",
				"A3\sounds_f\characters\human-sfx\P01\Low_hit_4.wss",
				"A3\sounds_f\characters\human-sfx\P01\Low_hit_5.wss",
				"A3\sounds_f\characters\human-sfx\P01\Mid_hit_1.wss",
				"A3\sounds_f\characters\human-sfx\P01\Mid_hit_2.wss",
				"A3\sounds_f\characters\human-sfx\P01\Mid_hit_3.wss",
				"A3\sounds_f\characters\human-sfx\P01\Mid_hit_4.wss",
				"A3\sounds_f\characters\human-sfx\P01\Mid_hit_5.wss",
				"A3\sounds_f\characters\human-sfx\P09\Hit_Max_1.wss",
				"A3\sounds_f\characters\human-sfx\P09\Hit_Max_2.wss",
				"A3\sounds_f\characters\human-sfx\P09\Hit_Max_3.wss",
				"A3\sounds_f\characters\human-sfx\P09\Hit_Max_4.wss",
				"A3\sounds_f\characters\human-sfx\P09\Hit_Max_5.wss"
			];
			playSound3D [_hitSoundFile, _unit, false, getPos _unit, 10, 1, 70];
        };
    };

    if !(_unit getVariable [QGVAR(pain), false]) then {
        if (_damage > 0.2 && {random 100 > 75}) then {
            [_unit, true] call FUNC(setPain);
        };
    };

    [_unit, _damage] call FUNC(setHitEffects);
};

_damage;
