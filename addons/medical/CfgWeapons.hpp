class CfgWeapons {
    class ItemCore;
    class InventoryItem_Base_F;
    class InventoryFirstAidKitItem_Base_F;
    class MedikitItem;

    // ITEMS
    class FirstAidKit: ItemCore {
        //type = 0;
        class ItemInfo: InventoryFirstAidKitItem_Base_F {
            mass = 4;
        };
    };
    class Medikit: ItemCore {
        //type = 0;
        class ItemInfo: MedikitItem {
            mass = 60;
        };
    };
    class FRL_ItemCore;
    class FRL_fieldDressing: FRL_ItemCore {
        scope = 2;
        model = "\A3\Structures_F_EPA\Items\Medical\Bandage_F";
        picture = "\pr\frl\addons\medical\ui\items\field_dressing.paa";
        displayName = CSTRING(BandTitle);
        descriptionShort = "Containing a bandage with sterile padding wad and fastening clip. It is used to treat bleeding.";
        descriptionUse = CSTRING(BandTitle);
        class ItemInfo: InventoryItem_Base_F {
            mass = 1;
        };
    };
    class FRL_morphine: FRL_fieldDressing {
        displayName = CSTRING(MorphTitle);
        picture = "\pr\frl\addons\medical\ui\items\morphine.paa";
        descriptionShort = "Auto-Injector: It is used to treat severe pain or agony.";
        model = "\A3\weapons_F\ammo\mag_univ.p3d";
        descriptionUse = CSTRING(MorphTitle);
    };
    class FRL_epinephrine: FRL_Morphine {
        displayName = CSTRING(AdrenTitle);
        picture = "\pr\frl\addons\medical\ui\items\adrenaline.paa";
        descriptionShort = "Auto-Injector: (also known as epinephrine). It is used to induce consciousness.";
        model = "\A3\weapons_F\ammo\mag_univ.p3d";
        descriptionUse = CSTRING(AdrenTitle);
    };

    class Rifle;
    class Rifle_Base_F: Rifle {
        class WeaponSlotsInfo;
    };



    class FRL_FakePrimaryWeapon: Rifle_Base_F {
        scope = 1;
        scopeCurator = 1;
        scopeArsenal = 1;
        displayName = "";
        model = "\A3\Weapons_f\empty";
        picture = "\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_gun_gs.paa";
        magazines[] = {};
        discreteDistance[] = {};
        discreteDistanceInitIndex = 0;

        class WeaponSlotsInfo: WeaponSlotsInfo {
            mass = 0;
        };
    };

    class PistolCore;
    class Pistol: PistolCore {
        class WeaponSlotsInfo;
    };
    class Pistol_Base_F: Pistol {
        class WeaponSlotsInfo: WeaponSlotsInfo {};
    };

    class FRL_FakeHandgunWeapon: Pistol_Base_F {
        scope = 1;
        scopeCurator = 1;
        scopeArsenal = 1;
        displayName = "";
        model = "\A3\Weapons_f\empty";
        picture = "\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_hgun_gs.paa";
        magazines[] = {};
        discreteDistance[] = {};
        discreteDistanceInitIndex = 0;

        class WeaponSlotsInfo: WeaponSlotsInfo {
            mass = 0;
        };
    };
};
