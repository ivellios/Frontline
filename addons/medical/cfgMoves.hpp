class CfgMovesBasic {
	class Default;
	class AgonyBase: Default {};
	class InjuredMovedBase: Default {};
    class Actions {
		class NoActions;
		class FRL_drag_non_Actions : NoActions {
			stop = "FRL_drag_non_stp";
			default = "FRL_drag_non_stp";
			stopRelaxed = "FRL_drag_non_stp";

			WalkF = "FRL_drag_non_stp";
			WalkLF = "";
			WalkRF = "";
			WalkL = "";
			WalkR = "";
			WalkLB = "";
			WalkRB = "";
			WalkB = "FRL_drag_non_go";

			PlayerWalkF = "FRL_drag_non_stp";
			PlayerWalkLF = "";
			PlayerWalkRF = "";
			PlayerWalkL = "";
			PlayerWalkR = "";
			PlayerWalkLB = "";
			PlayerWalkRB = "";
			PlayerWalkB = "FRL_drag_non_go";

			SlowF = "FRL_drag_non_stp";
			SlowLF = "";
			SlowRF = "";
			SlowL = "";
			SlowR = "";
			SlowLB = "";
			SlowRB = "";
			SlowB = "FRL_drag_non_go";

			PlayerSlowF = "FRL_drag_non_stp";
			PlayerSlowLF = "";
			PlayerSlowRF = "";
			PlayerSlowL = "";
			PlayerSlowR = "";
			PlayerSlowLB = "";
			PlayerSlowRB = "";
			PlayerSlowB = "FRL_drag_non_go";

			FastF = "FRL_drag_non_stp";
			FastLF = "";
			FastRF = "";
			FastL = "";
			FastR = "";
			FastLB = "";
			FastRB = "";
			FastB = "FRL_drag_non_go";

			PlayerFastF = "FRL_drag_non_stp";
			PlayerFastLF = "";
			PlayerFastRF = "";
			PlayerFastL = "";
			PlayerFastR = "";
			PlayerFastLB = "";
			PlayerFastRB = "";
			PlayerFastB = "FRL_drag_non_go";

			TactF = "FRL_drag_non_stp";
			TactLF = "";
			TactRF = "";
			TactL = "";
			TactR = "";
			TactLB = "";
			TactRB = "";
			TactB = "FRL_drag_non_go";

			PlayerTactF = "FRL_drag_non_stp";
			PlayerTactLF = "";
			PlayerTactRF = "";
			PlayerTactL = "";
			PlayerTactR = "";
			PlayerTactLB = "";
			PlayerTactRB = "";
			PlayerTactB = "FRL_drag_non_go";

			up = "FRL_drag_non_stp";
			down = "FRL_drag_non_stp";

			upDegree = "ManPosNoWeapon";
		};

		class FRL_drag_pst_Actions : NoActions {
			stop = "FRL_drag_pst_stp";
			default = "FRL_drag_pst_stp";
			stopRelaxed = "FRL_drag_pst_stp";

			WalkF = "FRL_drag_pst_stp";
			WalkLF = "";
			WalkRF = "";
			WalkL = "";
			WalkR = "";
			WalkLB = "";
			WalkRB = "";
			WalkB = "FRL_drag_pst_go";

			PlayerWalkF = "FRL_drag_pst_stp";
			PlayerWalkLF = "";
			PlayerWalkRF = "";
			PlayerWalkL = "";
			PlayerWalkR = "";
			PlayerWalkLB = "";
			PlayerWalkRB = "";
			PlayerWalkB = "FRL_drag_pst_go";

			SlowF = "FRL_drag_pst_stp";
			SlowLF = "";
			SlowRF = "";
			SlowL = "";
			SlowR = "";
			SlowLB = "";
			SlowRB = "";
			SlowB = "FRL_drag_pst_go";

			PlayerSlowF = "FRL_drag_pst_stp";
			PlayerSlowLF = "";
			PlayerSlowRF = "";
			PlayerSlowL = "";
			PlayerSlowR = "";
			PlayerSlowLB = "";
			PlayerSlowRB = "";
			PlayerSlowB = "FRL_drag_pst_go";

			FastF = "FRL_drag_pst_stp";
			FastLF = "";
			FastRF = "";
			FastL = "";
			FastR = "";
			FastLB = "";
			FastRB = "";
			FastB = "FRL_drag_pst_go";

			PlayerFastF = "FRL_drag_pst_stp";
			PlayerFastLF = "";
			PlayerFastRF = "";
			PlayerFastL = "";
			PlayerFastR = "";
			PlayerFastLB = "";
			PlayerFastRB = "";
			PlayerFastB = "FRL_drag_pst_go";

			TactF = "FRL_drag_pst_stp";
			TactLF = "";
			TactRF = "";
			TactL = "";
			TactR = "";
			TactLB = "";
			TactRB = "";
			TactB = "FRL_drag_pst_go";

			PlayerTactF = "FRL_drag_pst_stp";
			PlayerTactLF = "";
			PlayerTactRF = "";
			PlayerTactL = "";
			PlayerTactR = "";
			PlayerTactLB = "";
			PlayerTactRB = "";
			PlayerTactB = "FRL_drag_pst_go";

			up = "FRL_drag_pst_stp";
			down = "FRL_drag_pst_stp";

			upDegree = "ManPosHandGunStand";
		};
		class FRL_drag_rfl_Actions : NoActions {
			stop = "FRL_drag_rfl_stp";
			default = "FRL_drag_rfl_stp";
			stopRelaxed = "FRL_drag_rfl_stp";

			WalkF = "FRL_drag_rfl_stp";
			WalkLF = "";
			WalkRF = "";
			WalkL = "";
			WalkR = "";
			WalkLB = "";
			WalkRB = "";
			WalkB = "FRL_drag_rfl_go";

			PlayerWalkF = "FRL_drag_rfl_stp";
			PlayerWalkLF = "";
			PlayerWalkRF = "";
			PlayerWalkL = "";
			PlayerWalkR = "";
			PlayerWalkLB = "";
			PlayerWalkRB = "";
			PlayerWalkB = "FRL_drag_rfl_go";

			SlowF = "FRL_drag_rfl_stp";
			SlowLF = "";
			SlowRF = "";
			SlowL = "";
			SlowR = "";
			SlowLB = "";
			SlowRB = "";
			SlowB = "FRL_drag_rfl_go";

			PlayerSlowF = "FRL_drag_rfl_stp";
			PlayerSlowLF = "";
			PlayerSlowRF = "";
			PlayerSlowL = "";
			PlayerSlowR = "";
			PlayerSlowLB = "";
			PlayerSlowRB = "";
			PlayerSlowB = "FRL_drag_rfl_go";

			FastF = "FRL_drag_rfl_stp";
			FastLF = "";
			FastRF = "";
			FastL = "";
			FastR = "";
			FastLB = "";
			FastRB = "";
			FastB = "FRL_drag_rfl_go";

			PlayerFastF = "FRL_drag_rfl_stp";
			PlayerFastLF = "";
			PlayerFastRF = "";
			PlayerFastL = "";
			PlayerFastR = "";
			PlayerFastLB = "";
			PlayerFastRB = "";
			PlayerFastB = "FRL_drag_rfl_go";

			TactF = "FRL_drag_rfl_stp";
			TactLF = "";
			TactRF = "";
			TactL = "";
			TactR = "";
			TactLB = "";
			TactRB = "";
			TactB = "FRL_drag_rfl_go";

			PlayerTactF = "FRL_drag_rfl_stp";
			PlayerTactLF = "";
			PlayerTactRF = "";
			PlayerTactL = "";
			PlayerTactR = "";
			PlayerTactLB = "";
			PlayerTactRB = "";
			PlayerTactB = "FRL_drag_rfl_go";

			up = "FRL_drag_rfl_stp";
			down = "FRL_drag_rfl_stp";

			upDegree = "ManPosCombat";
		};
		class FRL_drag_lnr_Actions : NoActions {
			stop = "FRL_drag_lnr_stp";
			default = "FRL_drag_lnr_stp";
			stopRelaxed = "FRL_drag_lnr_stp";

			WalkF = "FRL_drag_lnr_stp";
			WalkLF = "";
			WalkRF = "";
			WalkL = "";
			WalkR = "";
			WalkLB = "";
			WalkRB = "";
			WalkB = "FRL_drag_lnr_go";

			PlayerWalkF = "FRL_drag_lnr_stp";
			PlayerWalkLF = "";
			PlayerWalkRF = "";
			PlayerWalkL = "";
			PlayerWalkR = "";
			PlayerWalkLB = "";
			PlayerWalkRB = "";
			PlayerWalkB = "FRL_drag_lnr_go";

			SlowF = "FRL_drag_lnr_stp";
			SlowLF = "";
			SlowRF = "";
			SlowL = "";
			SlowR = "";
			SlowLB = "";
			SlowRB = "";
			SlowB = "FRL_drag_lnr_go";

			PlayerSlowF = "FRL_drag_lnr_stp";
			PlayerSlowLF = "";
			PlayerSlowRF = "";
			PlayerSlowL = "";
			PlayerSlowR = "";
			PlayerSlowLB = "";
			PlayerSlowRB = "";
			PlayerSlowB = "FRL_drag_lnr_go";

			FastF = "FRL_drag_lnr_stp";
			FastLF = "";
			FastRF = "";
			FastL = "";
			FastR = "";
			FastLB = "";
			FastRB = "";
			FastB = "FRL_drag_lnr_go";

			PlayerFastF = "FRL_drag_lnr_stp";
			PlayerFastLF = "";
			PlayerFastRF = "";
			PlayerFastL = "";
			PlayerFastR = "";
			PlayerFastLB = "";
			PlayerFastRB = "";
			PlayerFastB = "FRL_drag_lnr_go";

			TactF = "FRL_drag_lnr_stp";
			TactLF = "";
			TactRF = "";
			TactL = "";
			TactR = "";
			TactLB = "";
			TactRB = "";
			TactB = "FRL_drag_lnr_go";

			PlayerTactF = "FRL_drag_lnr_stp";
			PlayerTactLF = "";
			PlayerTactRF = "";
			PlayerTactL = "";
			PlayerTactR = "";
			PlayerTactLB = "";
			PlayerTactRB = "";
			PlayerTactB = "FRL_drag_lnr_go";

			up = "FRL_drag_lnr_stp";
			down = "FRL_drag_lnr_stp";

			upDegree = "ManPosWeapon";
		};
	};
};

class CfgMovesMaleSdr: CfgMovesBasic {
	class Default;
	class AgonyBase: Default {};
	class InjuredMovedBase: Default {};
	class States {
		class AmovPercMstpSnonWnonDnon;
		class FRL_drag_rfl: AmovPercMstpSnonWnonDnon {
		   weaponLowered = 1;
		   disableWeapons = 1;
		   enableOptics = 0;
		   disableWeaponsLong = 0;
		   actions = "FRL_drag_rfl_Actions";
		   connectTo[] = {};
		   InterpolateTo[] = {};
		};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		class AcinPknlMwlkSnonWnonDb;
		class FRL_drag_go: AcinPknlMwlkSnonWnonDb {
			ConnectTo[] = {};
			InterpolateTo[] = {"Unconscious",0.01};
			speed = 1.3;
		};
		class AcinPknlMstpSrasWrflDnon;
		class FRL_drag_stp: AcinPknlMstpSrasWrflDnon {
			ConnectTo[] = {};
			InterpolateTo[] = {"Unconscious",0.01};
			speed = 0.10;
		};
		class FRL_drag_non_stp: FRL_drag_stp {
		   weaponLowered = 1;
		   disableWeapons = 1;
		   enableOptics = 0;
		   disableWeaponsLong = 1;
		   actions = "FRL_drag_non_Actions";
		   connectTo[] = {"FRL_drag_non_stp", 0.02};
		   InterpolateTo[] = {"FRL_drag_non_stp", 0.02, "FRL_drag_non_go", 0.02, "Unconscious",0.01};
		};
		class FRL_drag_non_go: FRL_drag_go {
		   weaponLowered = 1;
		   disableWeapons = 0;
		   enableOptics = 0;
		   disableWeaponsLong = 0;
		   actions = "FRL_drag_non_Actions";
		   connectTo[] = {"FRL_drag_non_stp", 0.02, "FRL_drag_non_go", 0.02};
		   InterpolateTo[] = {"FRL_drag_non_stp", 0.02, "FRL_drag_non_go", 0.02, "Unconscious",0.01};
		};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		class FRL_drag_pst_stp: FRL_drag_stp {
		   weaponLowered = 1;
		   disableWeapons = 1;
		   enableOptics = 0;
		   disableWeaponsLong = 1;
		   actions = "FRL_drag_pst_Actions";
		   connectTo[] = {"FRL_drag_pst_stp", 0.02};
		   InterpolateTo[] = {"FRL_drag_pst_stp", 0.02, "FRL_drag_pst_go", 0.02, "Unconscious",0.01};
		};
		class FRL_drag_pst_go: FRL_drag_go {
		   weaponLowered = 1;
		   disableWeapons = 0;
		   enableOptics = 0;
		   disableWeaponsLong = 0;
		   actions = "FRL_drag_pst_Actions";
		   connectTo[] = {"FRL_drag_pst_stp", 0.02, "FRL_drag_pst_go", 0.02};
		   InterpolateTo[] = {"FRL_drag_pst_stp", 0.02, "FRL_drag_pst_go", 0.02, "Unconscious",0.01};
		};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		class FRL_drag_lnr_stp: FRL_drag_stp {
		   weaponLowered = 1;
		   disableWeapons = 1;
		   enableOptics = 0;
		   disableWeaponsLong = 1;
		   actions = "FRL_drag_lnr_Actions";
		   connectTo[] = {"FRL_drag_lnr_stp", 0.02};
		   InterpolateTo[] = {"FRL_drag_lnr_stp", 0.02, "FRL_drag_lnr_go", 0.02, "Unconscious",0.01};
		};
		class FRL_drag_lnr_go: FRL_drag_go {
		   weaponLowered = 1;
		   disableWeapons = 1;
		   enableOptics = 0;
		   disableWeaponsLong = 1;
		   actions = "FRL_drag_lnr_Actions";
		   connectTo[] = {"FRL_drag_lnr_stp", 0.02, "FRL_drag_lnr_go", 0.02};
		   InterpolateTo[] = {"FRL_drag_lnr_stp", 0.02, "FRL_drag_lnr_go", 0.02, "Unconscious",0.01};
		};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		class FRL_drag_rfl_stp: FRL_drag_stp {
		   weaponLowered = 1;
		   disableWeapons = 1;
		   enableOptics = 0;
		   disableWeaponsLong = 1;
		   actions = "FRL_drag_rfl_Actions";
		   connectTo[] = {"FRL_drag_rfl_stp", 0.02};
		   InterpolateTo[] = {"FRL_drag_rfl_stp", 0.02, "FRL_drag_rfl_go", 0.02, "Unconscious",0.01};
		};

		class FRL_drag_rfl_go: FRL_drag_go {
		   weaponLowered = 1;
		   disableWeapons = 0;
		   enableOptics = 0;
		   disableWeaponsLong = 0;
		   actions = "FRL_drag_rfl_Actions";
		   connectTo[] = {"FRL_drag_rfl_stp", 0.02, "FRL_drag_rfl_go", 0.02};
		   InterpolateTo[] = {"FRL_drag_rfl_stp", 0.02, "FRL_drag_rfl_go", 0.02, "Unconscious",0.01};
		};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Carrying anim (for caller)
		class AcinPknlMstpSrasWrflDnon_AcinPercMrunSrasWrflDnon: InjuredMovedBase {
			speed = -7.5;
		};

		class AinjPfalMstpSnonWnonDnon_carried_Up: AgonyBase {
			speed = -7.5;
		};

		class AinjPfalMstpSnonWrflDnon_carried_Up: AgonyBase {
			canReload = 1;
			speed = -7.5;
			onLandBeg = 0;
			showWeaponAim = 0;
			static = 1;
			weaponLowered = 1;
		};
		class AcinPercMrunSrasWrflDf_AmovPercMstpSlowWrflDnon: InjuredMovedBase {
			speed = -4;
		};

		class AinjPfalMstpSnonWrflDnon_carried_Down: AgonyBase {
			speed = -4;
		};
	};
};
