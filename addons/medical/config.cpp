#define __INCLUDE_DIK
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

#include "CfgSounds.hpp"
#include "CfgWeapons.hpp"
#include "CfgMoves.hpp"
#include "CfgVehicles.hpp"

class RscControlsGroupNoScrollbars;
class RscPicture;
class RscPictureKeepAspect;
class RscBackground;
class RscProgress;
class RscStructuredText;
class RscListBox;
class RscText;
#include "RscTreatmentOption.hpp"
class RscTitles {
  #include "RscInteractUI.hpp"
  #include "RscProgressBar.hpp"
  #include "RscBleedoutBar.hpp"
  #include "RscEffectsLayers.hpp"
};

class FRL {
    #include "cfgVirtualItems.hpp"
    #include "cfgTreatments.hpp"

    class Modules {
        class Medical {
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\medical";

            class General {
              class blockInput;
              class clientInit;
              class clientInitIcons;
              class ehDamage;
              class ehHit;
              class init;
              class saveIncapBy;
              class setRespawnButton;
              class showBleedoutBar;
              class showHints;
            }

            class Effects {
              class clientInitEffects;
              class setDamageEffects;
              class setIncapEffects;
              class setHitEffects;
            };

            class States {
              class isAlive;
              class isBleeding;
              class isDead;
              class isInPain;
              class isMedic;
              class isUnconscious;
              class reviveTime;
              class setAlive;
              class setBleeding;
              class setDamage;
              class setDead;
              class setIncapHandling;
              class setPain;
              class setUnconscious;
            };

            class Treatment {
              class UI {
                  class clientInitUI;
                  class showProgressBar;
                  class stopTreatment;
                  class updateInteractUI;
              };


              class adrenalineEnd;
              class adrenalineStart;
              class bandageEnd;
              class bandageStart;
              class canTreat;
              class clientInitTreatments;
              class cprStart;
              class diagnoseStart;
              class diagnoseEnd;
              class diagnoseHighlight;
              class dragEnd;
              class dragOut;
              class dragStart;
              class hasBandages;
              class morphineEnd;
              class morphineStart;
              class treatmentEnd;
              class treatmentStart;
              class treatmentGesture;
              class treatmentKeepProgress;
              class treatmentEndLocal;
              class treatmentStartLocal;
            };

        };
    };
};
