class VirtualItems {
	class FRL_Medikit {
		actionName = "Medikit";
    scope = 2;
    model = "\A3\Weapons_F\Items\FirstAidkit.p3d";
    itemRequired[] = {};
    attachPoint = "lwrist";
    attachOffset[] = {0.05, 0.10, -0.01};
    vectorDir[] = {{1, 0, 0}, {0, 0, 1}};
	};
};
