#define SIZE_WIDTH 			GRIDX(12)
#define SIZE_HEIGHT 		GRIDY(1.25)
#define ORIGIN_X CENTER_X - (SIZE_WIDTH / 2)
#define ORIGIN_Y ((getResolution select 3) * 0.85 * pixelH)

class GVAR(bleedoutBar) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_Medical_bleedoutBar', _this select 0];";
    onUnLoad = "";
    class Controls {

      class ProgressBackground: RscBackground {
        idc = 2;
        colorBackground[] = {0, 0, 0, 0.25};
        x = ORIGIN_X;
        y = ORIGIN_Y;
        w = SIZE_WIDTH;
        h = SIZE_HEIGHT;
      };

      class Progress : RscProgress {
        idc = 1;
        colorFrame[] = {0,0,0,0};
        colorBar[] = {0.77, 0.51, 0.08, 1};
        x = ORIGIN_X;
        y = ORIGIN_Y;
        w = SIZE_WIDTH;
        h = SIZE_HEIGHT;
      };

      class ProgressText: RscText {
        idc = 3;
        text = "";
        colorBackground[] = {0, 0, 0, 0};
        colorText[] = {1, 1, 1, 1};
        x = ORIGIN_X;
        y = ORIGIN_Y;
        w = SIZE_WIDTH;
        h = SIZE_HEIGHT;
        style = 2;
      };

      class ActionText: RscStructuredText {
        idc = 10;
        text = "";
        colorBackground[] = {0, 0, 0, 0};
        colorText[] = {1, 1, 1, 1};
        x = CENTER_X - GRIDX(10);
        y = ORIGIN_Y + SIZE_HEIGHT + GRIDY(1);
        w = GRIDX(20);
        h = GRIDY(2);

        class Attributes {
            //font = FONTLIGHT_ALT;
            color = "#ffffff";
            align = "center";
            shadow = 1;
        };
      };
    };
};
