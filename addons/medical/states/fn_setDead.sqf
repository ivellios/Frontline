#include "macros.hpp"
/*
 *	File: fn_respawnKilled.sqf
 *	Author: Adanteh
 *	Set a unit as actual dead, combined function for dying for the engine (setDamage 1) or dying from an incapped state (In which case you were already dead to the engine)
 *
 *	0: The unit <OBJECT>
 *
 *	Example:
 *	[player] call FUNC(respawnKilled);
 */

(_this select 0) params [["_unit", objNull], ["_killer", objNull]];

if (alive _unit) exitWith {
	_unit setDamage 1;
};

setPlayerRespawnTime 10e10;
private _incappedSince = (CLib_Player getVariable [QGVAR(bleedoutInfo), []]) param [2, -1];
GVAR(timeIncapped) = [0, (serverTime - _incappedSince)] select (_incappedSince != -1);

[{
	CLib_Player setVariable [QGVAR(bleedoutInfo), nil];
}] call CFUNC(execNextFrame);

private _killers = if (isNull _killer) then {
	_unit getVariable [QGVAR(killedBy), []];
} else {
	// TODO make sure clicking respawn button retrieves the killer, not saves the suiciding person as killers
	if (_killer isEqualTo _unit) then {
		_unit getVariable [QGVAR(killedBy), []];
	} else {
		[_unit, _killer] call FUNC(saveIncapBy);
	};
};

_unit setVariable [QGVAR(draggedBy), nil, true];
[_unit, false] call FUNC(setBleeding);
[_unit, false] call FUNC(setPain);
[_unit, false] call FUNC(setUnconscious);

["KilledPlayer", [_unit, _killers]] call CFUNC(serverEvent);
// -- Fade out effects while screen blacks out (On delay so black fadein of main UI can happen first) -- //
[{
	[(_this select 0), 4, 0] call FUNC(setDamageEffects);
	[0, [0], 4, 0, true] call MFUNC(setPPBlur);
}, 2, [_unit]] call CFUNC(wait);

[false] call FUNC(showBleedoutBar);
