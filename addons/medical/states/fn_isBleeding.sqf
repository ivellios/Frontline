#include "macros.hpp"
/*
 *	File: fn_isBleeding.sqf
 *	Author: Adanteh
 *	Stacked condition for alive state
 *
 *	[player] call FUNC(isBleeding);
 */

params ["_unit"];
(_unit getVariable [QGVAR(bleeding), false])
