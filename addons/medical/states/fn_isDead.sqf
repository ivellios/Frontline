#include "macros.hpp"
/*
 *	File: fn_isDead.sqf
 *	Author: Adanteh
 *	Stacked condition for alive state
 *
 *	[player] call FUNC(isDead);
 */

params ["_unit"];
!(alive _unit)
