/*
    Function:       FRL_Medical_fnc_setBleeding
    Author:         Adanteh
    Description:    Sets bleeding variables and creates perFrame for effects (heartbeat, blood overlay)
	Example: 		[player, true] call FRL_Medical_fnc_setBleeding
*/
#include "macros.hpp"
#define CTRL(var1) ((uiNamespace getVariable [QGVAR(Effects), displayNull]) displayCtrl var1)

params ["_unit", ["_toggle", true]];

if (_toggle) then {
	if !([_unit] call FUNC(isBleeding)) then {
		_unit setVariable [QGVAR(bleeding), true, true];
		[{
			(_this select 0) params ["_unit"];
			if ([_unit] call FUNC(isDead) || {!([_unit] call FUNC(isBleeding))}) exitWith {
				[(_this select 1)] call MFUNC(removePerFrameHandler);
			};

			if !([_unit] call FUNC(isUnconscious)) then {
				playSound QGVAR(heartbeat);
				[3, [0.5], 0.3, 0.6] call MFUNC(setPPBlur);

				// -- Pulse in the bleeding overlay
				CTRL(101) ctrlSetFade 0;
				CTRL(101) ctrlCommit 0.3;
				[{
					CTRL(101) ctrlSetFade 1;
					CTRL(101) ctrlCommit 0.6;
				}, 0.3] call CFUNC(wait);

				[_unit, (damage _unit + 0.005)] call FUNC(setDamage);
			} else {
				if (((CLib_Player getVariable [QGVAR(bleedoutTime), -1]) - serverTime) <= 0) then {
					[[_unit, objNull]] call FUNC(setDead);
					[(_this select 1)] call MFUNC(removePerFrameHandler);
				};
			};
		}, 1, [_unit]] call MFUNC(addPerFramehandler);
	};

	if (isNil QGVAR(selfBleedHintCD)) then {
		GVAR(selfBleedHintCD) = true;
		[
			{
				["selfBleed", player] call FUNC(showHints);
				GVAR(selfBleedHintCD) = nil;
			}, 5,
			{
				[Clib_Player] call FUNC(hasBandages) &&
				!{currentWeapon player != "FRL_FakeHandgunWeapon"} &&
				!{[Clib_Player] call FUNC(isUnconscious)}
			}
		] call CFUNC(wait);
	};

} else {
	if ([_unit] call FUNC(isBleeding)) then {
		_unit setVariable [QGVAR(bleeding), nil, true];
		if ([_unit] call FUNC(isUnconscious)) then { // -- Disable timer
			missionNamespace setVariable ["RscRespawnCounter_Custom", 9999];
		};
	};
};
