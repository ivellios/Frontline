#include "macros.hpp"
/*
 *	File: fn_isAlive.sqf
 *	Author: Adanteh
 *	Stacked condition for alive state
 *
 *	[player] call FUNC(isAlive);
 */

params ["_unit"];
(alive _unit)
