/*
    Function:       FRL_Medical_fnc_setDamage
    Author:         Adanteh
    Description:    Handles setting damage through medical system
	Example:		[player, 0.5] call FRL_Medical_fnc_setDamage;
*/
#include "macros.hpp"

params ["_unit", ["_damageNew", 0]];

if (_damageNew >= 0.95) then {
	_damageNew = 0.95;
	[_unit, true] call FUNC(setUnconscious);
};

private _damage = damage _unit;
private _damageHitPoints = +((getAllHitPointsDamage _unit) select 2);
_unit setDamage _damageNew;
if (_damageNew > _damage) then {
    {
    	if (_x > _damageNew) then {
        	_unit setHitIndex [_forEachIndex, _x];
    	};
    } forEach _damageHitPoints;
} else {
	if (_damageNew > 0) then {
        {
        	if (_x < _damageNew) then {
            	_unit setHitIndex [_forEachIndex, _x];
        	};
        } forEach _damageHitPoints;
	};
};

// -- Prevents switch to walking when receiving one or two hits. Only allow leg damage on high damage -- //
if (_damageNew <= 0.7) then {
	_unit setHitPointDamage ["HitLegs", 0];
};

[_unit, 0.7, _damageNew] call FUNC(setDamageEffects);
