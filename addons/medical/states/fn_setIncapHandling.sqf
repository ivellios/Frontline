#include "macros.hpp"
/*
 *	File: fn_setIncapHandling.sqf
 *	Author: Adanteh
 *	Disables AI behaviour and locks buttons + action menu
 *
 *	0: The unit <OBJECT>
 *	1: Toggle <BOOLEAN>
 *
 *	Example:
 *	[player] call FUNC(setIncapHandling);
 */

params ["_unit", ["_toggle", true]];

if (_toggle) then {
	showHud [true, false, false, false, false, false, true, true];
	/*CVAR(DisablePrevAction) = true;
	CVAR(DisableNextAction) = true;
	CVAR(DisableAction) = true;*/
	[true] call FUNC(blockInput);
} else {
	showHUD [true, true, true, true, true, true, true, true];
	/*CVAR(DisablePrevAction) = false;
	CVAR(DisableNextAction) = false;
	CVAR(DisableAction) = false;*/
	[false] call FUNC(blockInput);
};
