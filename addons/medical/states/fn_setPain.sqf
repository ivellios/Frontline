#include "macros.hpp"
/*
 *	File: fnc_setPain.sqf
 *	Author: Adanteh
 *	Creates pain effects (Blur pulsing and camshake) and sets variables
 *
 *	Locality: _unit
 *
 *	0: The unit <OBJECT>
 *	1: Toggle <BOOLEAN>
 *
 *	Example:
 *	[player, true] call FUNC(setPain);
 */

params ["_unit", ["_toggle", true]];

if (_toggle) then {
	if !([_unit] call FUNC(inPain)) then {
		_unit setVariable [QGVAR(pain), true, true];
		[2, [0.8]] call MFUNC(setPPBlur);

		// -- Create loop with pain effects -- //
		[{
			private _unit = Clib_Player;
			if ([_unit] call FUNC(isDead) || {!([_unit] call FUNC(isInPain))}) exitWith {
				[(_this select 1)] call MFUNC(removePerFrameHandler);
			};
			if !([_unit] call FUNC(isUnconscious)) then {
				[5, [3], 0.25, 4] call MFUNC(setPPBlur);
				addCamShake [5, 2, 12];
			};
		}, 10] call MFUNC(addPerFramehandler);
	};
} else {
	if ([_unit] call FUNC(inPain)) then {
		_unit setVariable [QGVAR(pain), false, true];
		[2, [0]] call MFUNC(setPPBlur);
	};
};
