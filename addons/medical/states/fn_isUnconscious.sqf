#include "macros.hpp"
/*
 *	File: fn_isUnconscious.sqf
 *	Author: Adanteh
 *	Retrieves incap states of unit
 *
 *	[player] call FUNC(isUnconscious);
 */

params ["_unit"];
(_unit getVariable ["Unconscious", false])
