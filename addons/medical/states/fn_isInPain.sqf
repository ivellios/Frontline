#include "macros.hpp"
/*
 *	File: fnc_isInPain.sqf
 *	Author: Adanteh
 *	Checks if player is in pain state
 *
 *	[player] call FUNC(isInPain);
 */

params ["_unit"];
(_unit getVariable [QGVAR(pain), false])
