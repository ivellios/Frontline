#include "macros.hpp"
/*
 *	File: fn_setIncapped.sqf
 *	Author: Adanteh
 *	Handles the post-respawn section of incap system
 *
 *	0: The unit <OBJECT>
 *	1: Killer <OBJECT>
 *	2: Time unit can stay unconscious for before dying <SCALAR>
 *
 *	Example:
 *	[player, false, objNull] call frl_medical_fnc_setUnconscious;
 */

params ["_unit", ["_toggle", true], ["_killer", objNull]];

if (_toggle) then {
	if !([_unit] call FUNC(isUnconscious)) then {
		if (isNil {_unit getVariable QGVAR(bleedoutTime)}) then {
			[_unit] call FUNC(reviveTime);
		};

		// TODO Need to eject out of static weapons!
		if (vehicle _unit != _unit) then {
			if ((vehicle _unit) isKindOf "StaticWeapon") then {
				moveOut _unit;
			} else {
				[_unit, [_unit] call MFUNC(getDeathAnim)] call CFUNC(doAnimation);
			};
		};

		_unit setVariable ["Unconscious", true, true];
		_unit setUnconscious true;
		_unit spawn {
			waitUntil { (animationState _this) in ["unconsciousfaceleft", "unconsciousfaceright", "unconsciousfacedown", "unconsciousfaceup"] };
			if !(isNull (_this getVariable [QGVAR(draggedBy), objNull])) exitWith { }; // -- If being dragged right away do't disable sim
			_this enableSimulation false;
			["enableSimulation", [_this, false]] call CFUNC(serverEvent);
		};

		[_unit, true] call FUNC(setBleeding);
		[_unit, true] call FUNC(setIncapHandling);
		[_unit, true] call FUNC(setIncapEffects);

		// -- Sets custom time for vanilla respawn counter -- //
		["IncapPlayer", [_unit, ([_unit, _killer] call FUNC(saveIncapBy))]] call CFUNC(serverEvent);
		["UnconsciousnessChanged", true] call CFUNC(localEvent);
	};
} else {
	if ([_unit] call FUNC(isUnconscious)) then {
		_unit setVariable ["Unconscious", nil, true];

		private _bleedoutInfo = [serverTime, ((_unit getVariable [QGVAR(bleedoutInfo), [-500, 0, -1]]) select 1), -1];
		_unit setVariable [QGVAR(bleedoutInfo), _bleedoutInfo, false];
		_unit setVariable [QGVAR(bleedoutTime), nil];
		[_unit, false] call FUNC(setIncapHandling);
		[_unit, false] call FUNC(setIncapEffects);
		[_unit, false] call FUNC(setBleeding);

		_unit enableSimulation true;
		["enableSimulation", [_unit, true]] call CFUNC(serverEvent);
		_unit spawn {
			sleep 0.3;
			if ((alive _this) && !([_this] call FUNC(isUnconscious))) then {
                if (binocular _this != "" && { currentWeapon _this == binocular _this }) then {
                    if (primaryWeapon _this != "") then {
                        _this selectWeapon (primaryWeapon _this);
                    } else {
                        if (handGunWeapon _this != "") then {
                            _this selectWeapon (handGunWeapon _this);
                        };
                    };
                };

				if (currentWeapon _this == secondaryWeapon _this) then {
					private _primaryWeapon = primaryWeapon _this;
					if (_primaryWeapon != "") then {
						private _muzzles = getArray (configFile >> "cfgWeapons" >> _primaryWeapon >> "muzzles");
						if (count _muzzles > 1) then {
							_this selectWeapon (_muzzles select 0);
						} else {
							_this selectWeapon _primaryWeapon
						};
					};
				};
				_this setUnconscious false;
			};
		};

		if (alive _unit) then {
			["UnconsciousnessChanged", false] call CFUNC(localEvent);
		};
	};
};
