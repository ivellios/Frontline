#include "macros.hpp"
/*
 *	File: fn_isMedic.sqf
 *	Author: Adanteh
 *	Checks if given unit is a medic
 *
 *	[player] call FUNC(isMedic);
 */

params ["_unit"];
(_unit getUnitTrait "Medic")
