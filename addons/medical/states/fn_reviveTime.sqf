#include "macros.hpp"
/*
 *	File: fn_reviveTime.sqf
 *	Author: Adanteh
 *	Gets the time unit can spend in bleedout before passing out
 *
 *	[player, objNull, 10] call FUNC(reviveTime);
 */

params ["_unit"];

/*---------------------------------------------------------------------------
	Going incapped within a certain time of your last revive takes a one step lower bleedout time.
	When 0 is used it results in death. Example, with following steps [150, 90, 0] and 180 second reset

	1) First incapped >> 2:30 bleedout timer
	2) Get revived and go incapped again within 3 minutes >> 1:30 bleedout timer.
	3) Get revived and go incapped after 4 minutes >> 1:30 bleedout timer.
	4) Get revived and go incapped after 7 minutes >> 2:30 bleedout timer.
	5) Get revived and go incapped after 1 minute >> 1:30 bleedout timer
	6) Get revived and go incapped after 2 minutes >> Death

	The time when incapped is also saved, because that will be counted towards the respawn timer
---------------------------------------------------------------------------*/

private _bleedoutTimes = [QGVAR(Settings_bleedoutTimeSteps), [210,150,0]] call MFUNC(cfgSetting);
private _bleedoutTimeReset = [QGVAR(Settings_bleedoutTimeReset), 300] call MFUNC(cfgSetting);

private _bleedoutInfo = _unit getVariable [QGVAR(bleedoutInfo), [-500, 0, -1]]; // [time when last revived, current timer step, time when went incapped]
private _timeCurrent = serverTime;
private _timeSinceRevive = _timeCurrent - (_bleedoutInfo select 0);
private _bleedoutStepOld = _bleedoutInfo select 1;
private _bleedoutStepDifference = (floor (_timeSinceRevive / _bleedoutTimeReset)) - 1;
private _bleedoutStepNew = (0 max (_bleedoutStepOld - _bleedoutStepDifference)) min (count _bleedoutTimes - 1); // Make sure we stay within the possible steps
private _bleedoutTime = _bleedoutTimes select _bleedoutStepNew;
private _allowIncap = (_bleedoutTime > 0);
if (_allowIncap) then {
	_unit setVariable [QGVAR(bleedoutTime), (_bleedoutTime + serverTime), true];
	_unit setVariable [QGVAR(bleedoutInfo), [(_bleedoutInfo select 0), _bleedoutStepNew, _timeCurrent], false];
} else {
	GVAR(instaKill) = true;
};

[_allowIncap, _bleedoutTime]
