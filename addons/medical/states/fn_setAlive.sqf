#include "macros.hpp"
/*
 *	File: fn_setAlive.sqf
 *	Author: Adanteh
 *	Restores variables and graphical effects on units when unit is alive again after respawn
 *
 *	[player] call FUNC(setAlive);
 */

// #UNUSED
// #UNUSED
// #UNUSED
// #UNUSED
// #UNUSED

params ["_unit"];

//hotfix: revived while having no weapon (BIS)
if (currentWeapon player == "") then {
	[] spawn {
		sleep 0.1;
		if (currentWeapon player == "") then {player playAction "Civil";};
	};
};

// -- Hide respawn counter (Without spawn it doesn't seem to work) -- //
_unit setVariable [QGVAR(bleedoutInfo), nil];
