## How to into apply hello kitty bandaid

* Selecting medikit will show possible actions in the bottom right
* Using Diagnose will highlight the first required action
* Medics will autodiagnose
* Bandage stops bleeding and will increase health
* Adrenaline revives (after bleeding is stopped)
* Morphine stops pain (Blurred screen + shaking)
* Only medic can heal to full health
* When you go unconscious. When timer reaches 0 you will die
* Going unconscious in quick succession will cause you to instantly die
* CPR can be used to increase the timer
