class GVAR(Effects) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_Medical_Effects', _this select 0];";
    onUnLoad = "";
    class Controls {
        class DamageLayer : RscPicture {
          idc = 100;
          x = safeZoneX;
          y = safeZoneY;
          w = safeZoneW;
          h = safeZoneH;
          text = "\pr\frl\addons\medical\ui\layers\layer_damage.paa";
          fade = 1;
        };
        class BleedingLayer : RscPicture {
          idc = 101;
          x = safeZoneX;
          y = safeZoneY;
          w = safeZoneW;
          h = safeZoneH;
          text = "\pr\frl\addons\medical\ui\layers\layer_bleeding.paa";
          fade = 1;
        };
    };
};
