class CfgVehicles {
  class Man;
  class CAManBase: Man {
		class PulsationSound {
		    sample0[] = {{{{"A3\sounds_f\characters\human-sfx\other\heart_4_1",0,0.7},1}}};
		    sample1[] = {{{{"A3\sounds_f\characters\human-sfx\other\heart_4_3",0,0.7},1}}};
		    sample2[] = {{{{"A3\sounds_f\characters\human-sfx\other\heart_4_4",0,0.7},1}}};
		    sample3[] = {{{{"A3\sounds_f\characters\human-sfx\other\heart_4_5",0,0.7},1}}};
		};
	};
};
