#define SIZE_WIDTH 			GRIDX(8)
#define SIZE_HEIGHT 		GRIDY(15)
#define ORIGIN_X CENTER_X

class GVAR(InteractUI) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_Medical_InteractUI', _this select 0];";
    onUnLoad = "";
    fadeIn = 0.2;
    fadeOut = 0.2;

    class Controls {
        class MedicalTarget: RscText {
          idc = 2;
          x = safeZoneX + (safeZoneW - SIZE_WIDTH - (safeZoneW * 0.1));
          y = safeZoneY + (safeZoneH * 0.5) - GRIDY(1.5);
          w = SIZE_WIDTH;
          h = GRIDY(1.25);
          size = GRIDY(1);
          sizeEx = GRIDY(1);

          font = FONTMED;
          text = "Target";
          colorText[] = {1, 1, 1, 1};
          colorBackground[] = {0, 0, 0, 0};
          colorShadow[] = {0, 0, 0, 0.5};
          shadow = 1;
        };

        class MedicalOptions : RscControlsGroupNoScrollbars {
          idc = 3;
          x = safeZoneX + (safeZoneW - SIZE_WIDTH - (safeZoneW * 0.1));
          y = safeZoneY + (safeZoneH * 0.5);
          w = SIZE_WIDTH;
          h = SIZE_HEIGHT;
          class Controls {

          };
        };
    };
};
