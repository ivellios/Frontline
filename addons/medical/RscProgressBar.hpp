#define SIZE_WIDTH 			GRIDX(12)
#define SIZE_HEIGHT 		GRIDY(0.2)
#define ORIGIN_X CENTER_X - (SIZE_WIDTH / 2)
#define ORIGIN_Y ((getResolution select 3) * 0.85 * pixelH)

class GVAR(ProgressBar) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_Medical_ProgressBar', _this select 0];";
    onUnLoad = "";
    class Controls {

      class ProgressBackground: RscBackground {
        idc = 2;
        colorBackground[] = {1, 1, 1, 0.3};
        x = ORIGIN_X;
        y = ORIGIN_Y;
        w = SIZE_WIDTH;
        h = SIZE_HEIGHT;
      };

      class Progress : RscProgress {
        idc = 1;
        colorFrame[] = {0,0,0,0};
        colorBar[] = {0.77, 0.51, 0.08, 1};
        x = ORIGIN_X;
        y = ORIGIN_Y;
        w = SIZE_WIDTH;
        h = SIZE_HEIGHT;
      };

      class ActionText: RscText {
        idc = 10;
        text = "U wot m8";
        //font = FONTLIGHT_ALT;
        colorBackground[] = {0, 0, 0, 0};
        colorText[] = {1, 1, 1, 1};
        x = ORIGIN_X;
        y = ORIGIN_Y - GRIDY(2);
        w = SIZE_WIDTH;
        h = GRIDY(2);
        style = 2; // Center
      };
    };
};
