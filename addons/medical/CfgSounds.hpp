class CfgSounds {
	class GVAR(monotone) {sound[] = {"\CA\sounds\Air\C130\int-wind1.wss", db0-1, 1}; name = "FRL_wounded_monotone"; titles[] = {}; };
	class GVAR(coughing) {sound[] = {"\CA\sounds\Characters\Noises\Damage\george-damage-g2-02.wss", db, 1}; name = "FRL_wounded_coughing"; titles[] = {}; };
	class GVAR(panting) {sound[] = {"\CA\sounds\Characters\Noises\Damage\george-damage-g2-07.wss", db0, 1}; name = "FRL_wounded_panting"; titles[] = {}; };
	class GVAR(heartbeat) {sound[] = {"\a3\sounds_f\characters\human-sfx\Other\heart_1.wss", db0-1, 1}; name = "FRL_wounded_heartbeat"; titles[] = {}; };
};
