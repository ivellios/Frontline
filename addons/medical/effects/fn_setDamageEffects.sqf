/*
    Function:       FRL_Medical_fnc_setDamageEffects
    Author:         Adanteh
    Description:    Adjusts the effects when receiving damage
*/
#include "macros.hpp"
#define CTRL(var1) ((uiNamespace getVariable [QGVAR(Effects), displayNull]) displayCtrl var1)

params ["_unit", ["_speed", 0.7]];

private _damage = param [2, damage _unit];

// -- Only show effects on 0.2 damage and higher -- //
if (_damage < 0.1) then { _damage = 0; };
GVAR(ppDesat) ppEffectEnable true;
GVAR(ppDesat) ppEffectAdjust [1, 1, 0, [0, 0, 0, (_damage * 0.7)], [1, 1, 1, (1 min ((1.4 - _damage * 2.2) max 0.2))], [0.35, 0.35, 0.35, 0]];
GVAR(ppDesat) ppEffectCommit _speed;

GVAR(ppVignette) ppEffectEnable true;
GVAR(ppVignette) ppEffectAdjust [1, 1, 0, [1, 0, 0, 0.25 - (_damage / 10)],[0, 0, 0, 0],[0, 0, 0, 0], [1.8 - (_damage * 1.4), 1.8 - (_damage * 1.4), 1, 0, 0, 0.25, 1 - (_damage * 0.5)]];
GVAR(ppVignette) ppEffectCommit _speed;

CTRL(100) ctrlSetFade (1 - _damage);
CTRL(100) ctrlCommit _speed;
