/*
    Function:       FRL_Medical_fnc_setIncapEffects
    Author:         Adanteh
    Description:    Sets the effects for being incapacitated
*/
#include "macros.hpp"

params ["_unit", ["_toggle", true]];

if (_toggle) then {
	[6, [4.5], 2.5] call MFUNC(setPPBlur);
	[_unit, 0.7, 1] call FUNC(setDamageEffects);
	[true] call FUNC(showBleedoutBar);
} else {
	[6, [0], 0.5] call MFUNC(setPPBlur);
	[false] call FUNC(showBleedoutBar);
};
