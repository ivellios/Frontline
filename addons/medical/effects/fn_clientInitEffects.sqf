/*
    Function:       FRL_Medical_fnc_clientInitEffects
    Author:         Adanteh
    Description:    clientInit for medical effects (pp and overlays)
*/
#include "macros.hpp"
#define CTRL(var1) ((uiNamespace getVariable [QGVAR(Effects), displayNull]) displayCtrl var1)
GVAR(ppChromAberration) = ["ChromAberration", 2220+3, [0, 0, false]] call CFUNC(createPPEffect); // -- Adrenaline effect -- //
GVAR(ppDesat) = ["ColorCorrections", 2220+4, [1, 1, 0, [0, 0, 0, 0], [1, 1, 1, 1], [0.35, 0.35, 0.35, 0]]] call CFUNC(createPPEffect); // -- Low health desaturation -- //
GVAR(ppVignette) = ["ColorCorrections", 2220+5, [1, 1, 0, [1, 0, 0, 0.2],[0, 0, 0, 0],[0, 0, 0, 0], [2, 2, 1, 0, 0, 0.25, 1]]] call CFUNC(createPPEffect); // -- Low health red vignette -- //


// -- Prepare texture overlay, for fading in damage and pulsed bleeding
QGVAR(Effects) cutRsc [QGVAR(Effects), "PLAIN", 0];
CTRL(100) ctrlSetFade 1;
CTRL(100) ctrlCommit 0;
CTRL(101) ctrlSetFade 1;
CTRL(101) ctrlCommit 0;

["MedicalEffect", {
    (_this select 0) params ["_actionSlug"];
    switch (toLower _actionSlug) do {
        case "morphine": {
            [] spawn {
        		private ["_i", "_j", "_k"];
        		GVAR(ppChromAberration) ppEffectEnable true;
        		for "_j" from 0 to 3 do {
        			_k = random 0.05;
        			for "_i" from 0 to (0.02+_k) step 0.01 do {
        				GVAR(ppChromAberration) ppEffectAdjust [_i,_i,true];
        				GVAR(ppChromAberration) ppEffectCommit 0.1;
        				sleep 0.05;
        			};
        			for "_i" from (0.02+_k-0.01) to 0 step -0.01 do {
        				GVAR(ppChromAberration) ppEffectAdjust [_i,_i,true];
        				GVAR(ppChromAberration) ppEffectCommit 0.1;
        				sleep 0.1;
        			};
        			sleep (0.2+random 0.2);
        		};

        		GVAR(ppChromAberration) ppEffectAdjust [0,0,0];
        		GVAR(ppChromAberration) ppEffectCommit 2.0;
        		sleep 2;
        		GVAR(ppChromAberration) ppEffectEnable false;
        	};
        };

        case "adrenaline": {
            [] spawn {
        		private ["_i", "_j", "_k"];
        		GVAR(ppChromAberration) ppEffectEnable true;
        		for "_j" from 0 to 3 do {
        			_k = random 0.05;
        			for "_i" from 0 to (0.02+_k) step 0.01 do {
        				GVAR(ppChromAberration) ppEffectAdjust [_i,_i,true];
        				GVAR(ppChromAberration) ppEffectCommit 0.1;
        				sleep 0.05;
        			};

        			for "_i" from (0.02+_k-0.01) to 0 step -0.01 do {
        				GVAR(ppChromAberration) ppEffectAdjust [_i,_i,true];
        				GVAR(ppChromAberration) ppEffectCommit 0.1;
        				sleep 0.1;
        			};
        			sleep (0.5+random 0.2);
        		};

        		GVAR(ppChromAberration) ppEffectAdjust [0,0,0];
        		GVAR(ppChromAberration) ppEffectCommit 2.0;
        		sleep 2;
        		GVAR(ppChromAberration) ppEffectEnable false;
        	};
        };
    };
}] call CFUNC(addEventHandler);
