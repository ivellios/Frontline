/*
    Function:       FRL_Medical_fnc_setHitEffects
    Author:         Adanteh
    Description:    Handles effects when you're hit (Direct hit effect, and adjusted damage update)
*/
#include "macros.hpp"

params ["_unit", ["_damageReceived", 0, [0]]];

// -- Create quick blur effect with slow fade when hit. -- //
[4, [(_damageReceived * 1.5)], 0.1, 3] call MFUNC(setPPBlur);
[_unit, 0.4, ((damage _unit) + (_damageReceived)) min 1] call FUNC(setDamageEffects);
