#include "..\macros.hpp"

#define RHSBASECLASS RHS_M2A2_Base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

class cfgAmmo{
	class BulletBase;
	class B_19mm_HE: BulletBase{};
	class B_30mm_HE: B_19mm_HE{};
	class RHS_ammo_M792_HEI: B_30mm_HE
	{
		CraterEffects = "ExploAmmoCrater";
		explosionEffects = "ExploAmmoExplosion";
		timeToLive = 7.5;
		airfriction = -0.00056;
		typicalspeed = 1100;
		maxSpeed = 1100;
		model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_red";
	};
};

class CfgMagazines
{
	class Default;
	class CA_Magazine: Default{};
	class VehicleMagazine: CA_Magazine{};
	class 250Rnd_30mm_HE_shells : VehicleMagazine {};
	class 140Rnd_30mm_MP_shells : 250Rnd_30mm_HE_shells {};
	class 140Rnd_30mm_MP_shells_Tracer_Red : 140Rnd_30mm_MP_shells {};
	class rhs_mag_230Rnd_25mm_M242_HEI : 140Rnd_30mm_MP_shells_Tracer_Red {
        scope = 2;
        ammo = "RHS_ammo_M792_HEI";
        count = 160;
        displayname = "25x137mm M792 HEI-T";
        displaynameshort = "M792";
        initspeed = 1100;
        lastroundstracer = 4;
        maxleadspeed = 300;
        namesound = "cannon";
        tracersevery = 1;
    };
};


// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)

TURRET_INHERIT

	class RHSBASECLASS: APC_Tracked_03_base_F {
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				lockWhenDriverOut = 1;
				turretInfoType = "RscWeaponRangeZeroing";
				discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000};
				discreteDistanceInitIndex = 5;
				gunnerHasFlares = 0;
				gunnerAction = "RHS_M2A2_GunnerOut";
				gunnerInAction = "RHS_M2A2_Gunner";
				gunnerGetInAction = "GetInHigh";
				gunnerGetOutAction = "GetOutHigh";
				gunnerDoor = "hatchG";
				minElev = -9;
				maxElev = 57;
				initElev = 0;
				viewGunnerInExternal = 1;
				showCrewAim = 0;
				maxhorizontalrotspeed = 1.04;
				maxverticalrotspeed = 1.04;
				stabilizedinaxes = 3;
				startengine = 0;
				hideWeaponsGunner = 1;
				selectionFireAnim = "zasleh2";
				weapons[] = {"RHS_weap_M242BC","rhs_weap_m240veh","Rhs_weap_TOW_Launcher"};
				magazines[] = {"rhs_mag_1100Rnd_762x51_M240","rhs_mag_1100Rnd_762x51_M240","rhs_mag_230Rnd_25mm_M242_HEI","rhs_mag_70Rnd_25mm_M242_APFSDS","rhs_mag_2Rnd_TOW2A"};
				class OpticsIn
				{
					class Wide
					{
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = 30;
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = 100;
						initfov = "0.7/1";
						minFov = "0.7/1";
						maxFov = "0.7/1";
						visionMode[] = {"Normal"};
						thermalMode[] = {4};
						gunnerOpticsModel = "\rhsusf\addons\rhsusf_a2port_armor\M2A2_Bradley\gunnerOptics_M2A2";
						gunnerOpticsEffect[] = {};
					};
					class Narrow: Wide
					{
						gunnerOpticsModel = "\rhsusf\addons\rhsusf_a2port_armor\M2A2_Bradley\gunnerOptics_M2A2_2";
						initfov = "0.2";
						minFov = "0.2";
						maxFov = "0.2";
					};
				};
				class Turrets: Turrets
				{
					class CommanderOptics: CommanderOptics
					{
						magazines[] = {"SmokeLauncherMag","SmokeLauncherMag","SmokeLauncherMag","SmokeLauncherMag","SmokeLauncherMag","SmokeLauncherMag"};
					};
				};
			};
		};
		/*class Turrets: Turrets {
			class MainTurret: MainTurret {
				gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
				turretInfoType = "RscOptics_APC_Wheeled_03_gunner";
				discreteDistance[] = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000};
				// minElev = -9; // D: -9
				// maxElev = 57; // D: 57
				// maxHorizontalRotSpeed = 1.04; // D: 1.04
				// maxVerticalRotSpeed = 1.04; // D: 1.04
				// stabilizedInAxes = 3; // D: 3

				class OpticsIn {
					class Wide {
						minFov = 0.4; // D: 0.117
						maxFov = 0.4; // D: 0.117
						initFov = 0.4;
						visionMode[] = {"Normal", "Ti"};
						thermalMode[] = {4}; // {4}
						gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d"; // "\rhsusf\addons\rhsusf_a2port_armor\M2A2_Bradley\gunnerOptics_M2A2";
					};

					class Narrow: Wide {
						gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d"; // "\rhsusf\addons\rhsusf_a2port_armor\M2A2_Bradley\gunnerOptics_M2A2_2";
						initfov = 0.2;
						minFov = 0.2;
						maxFov = 0.2;
					};
				};
				class Turrets: Turrets {
					class CommanderOptics: CommanderOptics {
						minElev = -4;
						maxElev = 20;
						minTurn = -360;
						maxTurn = 360;
						turretInfoType = "RscWeaponRangeFinder";
						// gunnerOpticsEffect[] = {};
						// gunnerOpticsModel = "\rhsusf\addons\rhsusf_a2port_armor\M2A2_Bradley\gunnerOptics_M2A2_2";
						class ViewOptics {
							// minAngleX = -30;
							// maxAngleX = 30;
							// minAngleY = -100;
							// maxAngleY = 100;
							initFov = 0.155;
							minFov = 0.067;
							maxFov = 0.155;
						};
					};
				};
			};

		};*/
	};
	class RHS_M6: RHS_M2A2_Base{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = {"RHS_weap_M242BC","rhs_weap_m240veh","Rhs_weap_stinger_Launcher"};
				magazines[] = {"rhs_mag_1100Rnd_762x51_M240","rhs_mag_1100Rnd_762x51_M240","Rhs_mag_4Rnd_stinger","Rhs_mag_4Rnd_stinger","Rhs_mag_4Rnd_stinger","rhs_mag_230Rnd_25mm_M242_HEI","rhs_mag_70Rnd_25mm_M242_APFSDS"};
			};
		};
	};

	class RHS_M2A2: RHS_M2A2_Base {

	};

	class RHS_M2A3: RHS_M2A2 {
		class Turrets: Turrets {
			class MainTurret: MainTurret {
				class Turrets: Turrets {
					class CommanderOptics: CommanderOptics {
						stabilizedInAxes = "StabilizedInAxesBoth";
						// gunnerOpticsModel = "\rhsusf\addons\rhsusf_a2port_armor\M2A2_Bradley\comTI_M2A2";
						gunnerOpticsEffect[] = {};
						class ViewOptics: ViewOptics {
							initFov = 0.466;
							minFov = 0.015;
							maxFov = 0.466;
							visionMode[] = {"Normal", "NVG", "Ti"};
							thermalMode[] = {0, 1};
						};
						minElev = -90;
						maxElev = 90;
					};
				};
			};
		};
	};
};
