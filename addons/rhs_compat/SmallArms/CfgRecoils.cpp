#include "..\macros.hpp"

class CfgRecoils {
	class Default;
    class recoil_default: Default {
        muzzleOuter[] = {0.3, 1, 1.1, 0.75};// muzzleOuter[] = {0.3, 1, 0.3, 0.2};
        muzzleInner[] = {0, 0, 0.1, 0.1};
        kickBack[] = {0.03, 0.06};
        permanent = 0.1;
        temporary = 0.0125;
    };
	class recoil_ak74m: recoil_default {
		muzzleOuter[] = {0.28, 0.733, 0.25, 0.3};
		kickBack[] = {0.0266, 0.0366};
		permanent = 0.2;
	};
	class recoil_ak74su: recoil_ak74m {
		muzzleOuter[] = {0.366, 0.8, 0.25, 0.3};
		kickBack[] = {0.0575, 0.07};
		permanent = 0.25;
	};
	class recoil_akm: recoil_default {
		muzzleOuter[] = {0.315, 0.65, 0.325, 0.3};
		//muzzleInner[] = {0.465, 0.55, 0.175, 0.3};
		kickBack[] = {0.04, 0.05};
		permanent = 0.32;
		temporary = 0.005;
	};
	class recoil_asval: recoil_default {
		muzzleOuter[] = {0.25, 0.475, 0.2, 0.275};
		muzzleInner[] = {0.35, 0.375, 0.3, 0.2};
		kickBack[] = {0.0333, 0.04};
		permanent = 0.3;
	};
	class recoil_m16: recoil_default {
		muzzleOuter[] = {0.275, 0.715, 0.25, 0.3};
		kickBack[] = {0.025, 0.0325};
		permanent = 0.175;
		temporary = 0.0075;
	};
	class recoil_m4: recoil_default {
		muzzleOuter[] = {0.285, 0.725, 0.275 , 0.325};
		kickBack[] = {0.03, 0.04};
		permanent = 0.2;
	};
	class recoil_mp7: recoil_default {
		muzzleOuter[] = {0.233, 0.45, 0.233, 0.25};
		kickBack[] = {0.025, 0.0275};
		permanent = 0.175;
		temporary = 0.0125;
	};
	/*
	class recoil_m27IAR: recoil_m16 {

	};
	*/
	class recoil_m240: recoil_default {
		muzzleOuter[] = {0.525, 1.25, 0.25 , 0.66};
		muzzleInner[] = {0, 0, 0, 0};
		kickBack[] = {0.02, 0.0266};
		permanent = 0.175;
		temporary = 0.0066;
	};
	class recoil_pistol_acpc2: recoil_default {
		// -- muzzleOuter[] = {0.2,1.5,0.2,0.3}; vanilla
		muzzleOuter[] = {0.2,1.8,0.6,0.3};
		permanent = 0.2;
	};
	class recoil_pistol_p07: recoil_default {
		muzzleOuter[] = {0.2,1.7,0.525,0.3}; //-- {0.2,1.5,0.2,0.3}; vanilla
		kickBack[] = {0.07, 0.09};
		permanent = 0.2;
	};
	class recoil_pkm: recoil_default {
		muzzleOuter[] = {0.55, 1.33, 0.2 , 0.66};
		muzzleInner[] = {0, 0, 0, 0};
		kickBack[] = {0.02, 0.0266};
		permanent = 0.175;
		temporary = 0.0066;
	};
	class recoil_sa58: recoil_default {
		muzzleOuter[] = {0.315, 0.733, 0.266, 0.366};
		muzzleInner[] = {0, 0, 0.1, 0.1};
		kickBack[] = {0.035, 0.045};
		permanent = 0.266;
	};
	class recoil_saw: recoil_default {
		muzzleOuter[] = {0.35, 0.6, 0.275 , 0.366};
		kickBack[] = {0.0115, 0.0135};
		permanent = 0.225;
		temporary = 0.0066;
	};
	class recoil_shotgun: recoil_default {
		muzzleOuter[] = {0.5, 2.5, 1.6, 0.9};//muzzleOuter[] = {0.5, 2.5, 0.8, 0.6};
		kickBack[] = {0.08, 0.1};
		permanent = 0.9;
	};
};
