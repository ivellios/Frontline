#include "..\macros.hpp"

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)

//rpg7 he HEShellExplosion
class CfgWeapons {
	class Default;
    class PistolCore: Default {};
    class Pistol: PistolCore {};
    class Pistol_Base_F: Pistol {};
    class hgun_ACPC2_F: Pistol_Base_F {
		inertia = 1.15;
		maxRecoilSway = 0.02;
		//swayDecaySpeed = 1.25;
		dexterity = 1.5;
	};
	class hgun_P07_F: Pistol_Base_F {
		inertia = 1.15;
		maxRecoilSway = 0.02;
		//swayDecaySpeed = 1.25;
		dexterity = 1.5;
	};
	class hgun_Rook40_F: Pistol_Base_F {
		inertia = 1.15;
		maxRecoilSway = 0.02;
		//swayDecaySpeed = 1.25;
		dexterity = 1.5;
	};
	class rhsusf_weap_m1911a1: hgun_ACPC2_F {
		inertia = 1.15;
		maxRecoilSway = 0.02;
		//swayDecaySpeed = 1.25;
		dexterity = 1.5;
		recoil = "recoil_pistol_acpc2";
	};
	class rhsusf_weap_glock17g4: hgun_P07_F {
		inertia = 1.15;
		maxRecoilSway = 0.02;
		//swayDecaySpeed = 1.25;
		dexterity = 1.5;
		recoil = "recoil_pistol_p07";
	};
	class rhsusf_weap_m9: rhsusf_weap_glock17g4 {};
	class rhs_weap_pya:  hgun_Rook40_F {
		recoil = "recoil_pistol_p07";
	};
	class rhs_weap_pb_6p9: rhs_weap_pya {
		recoil = "recoil_pistol_p07";
	};
	class rhs_weap_makarov_pm: rhs_weap_pya {
		recoil = "recoil_pistol_p07";
	};
	class rhs_weap_tt33: rhs_weap_pya {
		recoil = "recoil_pistol_acpc2";
	};
    class rhs_weap_suicide_vest: rhsusf_weap_m1911a1 {
        dlc = "Frontline";
        author = "Frontline";
        scope = 2;
        initSpeed = 2;
        //model = "\A3\Structures_F\Items\Electronics\MobilePhone_old_F.p3d";
        model = "\rhsusf\addons\rhsusf_weapons\pistols\M1911A1\rhs_m1911a1";
        modelOptics = "-";
        picture = "\rhsusf\addons\rhsusf_weapons\icons\p_m1911a1_ca.paa";
        magazines[] = {"frl_mag_suicide"};
        displayname = "Suicide Detonator";
        descriptionShort = "Detonator";
        dispersion = 0.02;
        reloadTime = 0.13;
        ffCount = 1;
    };
	class RifleCore: Default {};
	class Rifle: RifleCore{};
	class Rifle_Base_F: Rifle {};
	class arifle_MX_Base_F: Rifle_Base_F {};
	class Rifle_Long_base_F: Rifle_Base_F {
		FRLRecoilResetTime[] = {0.175,0.15,0.135,0.125};
		FRLRecoilShakePwr = 2.5;
		FRLRecoilMax[] = {1.4,1.35,1.3,1.3};
		RLRecoilTimeconstant[] = {6,9,11,15};
		FRLRecoilDrift[] = {0.2, 0.3}; //reserved for MG or mby prone
	};
	class Rifle_Short_Base_F: Rifle_Base_F {
		// FRLRecoilResetTime[] = {0.175,0.15,0.135,0.125};
		FRLRecoilShakePwr = 2;
		FRLRecoilMax[] = {1.4,1.35,1.5,1.5};
		RLRecoilTimeconstant[] = {3,4,5,5};
		FRLRecoilDrift[] = {0.2, 0.15}; //reserved for MG or mby prone
	};
	// -- vanilla weapons
	class LMG_Mk200_F: Rifle_Long_Base_F {
		inertia = 1.8;
	};
	class SMG_01_Base: Rifle_Short_Base_F {};
	class SMG_01_F: SMG_01_Base {};
	class SMG_02_base_F: Rifle_Short_Base_F {};
	// -- RHS weapons
	class rhs_weap_ak74m_Base_F: Rifle_Base_F {
		inertia = 0.7;
		recoil = "recoil_ak74m";
	};
	class rhs_weap_ak74m: rhs_weap_ak74m_Base_F {
		inertia = 0.7;
		//FRLRecoilDrift[] = {0.2,0.25};
		recoil = "recoil_ak74m";
		recoilProne = "recoil_ak74m";
	};
	class rhs_weap_ak74: rhs_weap_ak74m {};
	class rhs_weap_ak74s: rhs_weap_ak74 {};
	class rhs_weap_aks74u: rhs_weap_ak74s {
		FRLRecoilMax[] = {1.45,1.4,1.35,1.333};
		FRLRecoilShakePwr = 3;
		recoil = recoil_ak74su;
	};
	class rhs_weap_akm: rhs_weap_ak74m {
		inertia = 0.8;
		recoil = "recoil_akm";
		recoilProne = "recoil_akm";
        FRLRecoilShakePwr = 3;
	};
	class rhs_weap_asval: rhs_weap_ak74m {
		recoil = "recoil_asval";
		recoilProne = "recoil_asval";
		inertia = 0.4;
	};
	class rhs_weap_m4_Base: arifle_MX_Base_F {
		recoil = "recoil_m4";
	};
	class rhs_weap_m16a4: rhs_weap_m4_Base {
		recoil = "recoil_m16";
		intertia = 0.7;
        FRLRecoilShakePwr = 2.25;
	};
	class rhs_weap_m4a1: rhs_weap_m4_Base{
		recoil = "recoil_m4";
	};
	class rhs_weap_m70_base: Rifle_Base_F {};
	class rhs_weap_m21_base: rhs_weap_m70_base {
		inertia = 0.7;
		recoil = "recoil_ak74m";
		FRLRecoilShakePwr = 2.75; // -- a bit less shake than AK74
	};
	class rhs_weap_M249_base: LMG_Mk200_F {};
	class rhs_weap_m240_base: rhs_weap_M249_base {
		inertia = 3.3;
		recoil = "recoil_m240";
		FRLRecoilShakePwr = 2.5;
	};
	class rhs_weap_m27iar: rhs_weap_m4a1 {
		recoil = recoil_m16;
	};
	class rhs_weap_M590_5RD: Rifle_Base_F {
		recoil = "recoil_shotgun";
		inertia = 0.7;
	};
	class rhs_weap_M590_8RD: rhs_weap_M590_5RD {
		recoil = "recoil_shotgun";
		inertia = 0.8;
	};
	class rhsusf_weap_MP7A1_base_f: SMG_02_base_F {
		recoil = "recoil_mp7";
		inertia = 0.4;
	};
	class rhs_pkp_base: Rifle_Long_Base_F {
		inertia = 2.5;
		recoil = "recoil_pkm";
	};
	class rhs_weap_pkp: rhs_pkp_base {
		inertia = 2.9;
	};
	class rhs_weap_pkm: rhs_weap_pkp {
		inertia = 2.7;
	};
	class rhs_weap_saw_base: Rifle_Base_F {
		inertia = 1.5;
		recoil = "recoil_m249";
		FRLRecoilShakePwr = 1.25;
	};
	class rhs_weap_savz58_base: Rifle_Base_F {
		inertia = 0.6;
		recoil = "recoil_sa58";
        FRLRecoilShakePwr = 2.75;
	};
	class rhs_weap_savz58v: rhs_weap_savz58_base{
		inertia = 0.6;
	};
	class rhs_weap_savz58p: rhs_weap_savz58_base{
		inertia = 0.6;
	};
	class rhs_weap_savz61: SMG_01_F {
		recoil = "recoil_mp7";
		inertia = 0.4;
		FRLRecoilShakePwr = 2.5;
	};
	class LMG_Zafir_F: Rifle_Long_Base_F {
		inertia = 2.2;
	};

	class LauncherCore: Default{};
	class Launcher: LauncherCore {};
	class Launcher_Base_F: Launcher {};
	class rhs_weap_M136: Launcher_Base_F {
		class GunParticles {
            class effect1 {
                positionName = "konec hlavne";
                directionName = "usti hlavne";
                effectName = "RHS_Fired_M136HEAT";
            };
			class effect2 {
				effectName = "ATCloud";
                positionName = "Usti hlavne";
                directionName = "Konec hlavne";
			};
			class effect3 {
                effectName = "MortarFired";
                positionName = "Usti Hlavne";
                directionName = "Konec Hlavne";
            };
        };
	};
	class rhs_weap_M136_hedp: rhs_weap_M136 {
        class GunParticles {
            class effect1 {
                positionName = "konec hlavne";
                directionName = "usti hlavne";
                effectName = "RHS_Fired_M136HEAT";
            };
			class effect2 {
				effectName = "ATCloud";
                positionName = "Usti hlavne";
                directionName = "Konec hlavne";
			};
			class effect3 {
                effectName = "MortarFired";
                positionName = "Usti Hlavne";
                directionName = "Konec Hlavne";
            };
        };
    };
    class rhs_weap_M136_hp: rhs_weap_M136 {
        class GunParticles {
            class effect1 {
                positionName = "konec hlavne";
                directionName = "usti hlavne";
                effectName = "RHS_Fired_M136HEAT";
            };
			class effect2 {
				effectName = "ATCloud";
                positionName = "Usti hlavne";
                directionName = "Konec hlavne";
			};
			class effect3 {
                effectName = "MortarFired";
                positionName = "Usti Hlavne";
                directionName = "Konec Hlavne";
            };
        };
    };
	class launch_RPG32_F: Launcher_Base_F {
		class GunParticles {
            class effect1 {
                positionName = "konec hlavne";
                directionName = "usti hlavne";
                effectName = "RHS_Fired_M136HEAT";
            };
			class effect2 {
				effectName = "ATCloud";
                positionName = "Usti hlavne";
                directionName = "Konec hlavne";
			};
			class effect3 {
                effectName = "MortarFired";
                positionName = "Usti Hlavne";
                directionName = "Konec Hlavne";
            };
        };
    };
	class rhs_weap_rpg26: Launcher_Base_F {
		class GunParticles {
            class effect1 {
                positionName = "konec hlavne";
                directionName = "usti hlavne";
                effectName = "RHS_Fired_M136HEAT";
            };
			class effect2 {
				effectName = "ATCloud";
                positionName = "Usti hlavne";
                directionName = "Konec hlavne";
			};
			class effect3 {
                effectName = "MortarFired";
                positionName = "Usti Hlavne";
                directionName = "Konec Hlavne";
            };
        };
    };
	class ItemCore;
	class InventoryOpticsItem_Base_F;
	class rhsusf_acc_sniper_base: ItemCore {};
	class rhsusf_acc_ACOG: rhsusf_acc_sniper_base {
		 class ItemInfo: InventoryOpticsItem_Base_F {
			class OpticsModes {
                class elcan_scope {
                    opticsZoomMin = "0.13";
                    opticsZoomMax = "0.13";
                    opticsZoomInit = "0.13";
                };
            };
		 };
	};

	class rhsusf_acc_ACOG_3d: rhsusf_acc_ACOG {
		 class ItemInfo: ItemInfo {
				class OpticsModes: OpticsModes {
          class elcan_scope: elcan_scope {
						memoryPointCamera = "opticview_3d";
						opticsZoomMin = "0.148";
						opticsZoomMax = "0.148";
						opticsZoomInit = "0.148";
						useModelOptics = 0;
                };
            };
		};
	};

	class rhsusf_acc_g33_xps3: ItemCore {
			class ItemInfo: InventoryOpticsItem_Base_F {
				class OpticsModes {
                    class MAG {
						opticsZoomMin = "0.148";
						opticsZoomMax = "0.148";
						opticsZoomInit = "0.148";
                };
            };
		};
	};

	class rhsusf_acc_ELCAN: rhsusf_acc_sniper_base {
		class ItemInfo: InventoryOpticsItem_Base_F {
			class OpticsModes {
                class elcan_scope {
                    opticsZoomMax = "0.1875";
                    opticsZoomMin = "0.1875";
                    opticsZoomInit = "0.1875";
                };
            };
		};
	};

	class rhsusf_acc_SpecterDR: rhsusf_acc_sniper_base {
		class ItemInfo: InventoryOpticsItem_Base_F {
			class OpticsModes {
                class Elcan_x4 {
                    opticsZoomMax = "0.17";
                    opticsZoomMin = "0.17";
                    opticsZoomInit = "0.17";
                };
            };
		};
	};
	class rhs_acc_sniper_base: ItemCore {};
	class rhs_acc_1pn93_base: rhs_acc_sniper_base {
		class ItemInfo: InventoryOpticsItem_Base_F {
			class OpticsModes {
                class 1pn93_scope {
                    opticsZoomMax = "0.13";
                    opticsZoomMin = "0.13";
                    opticsZoomInit = "0.13";
                };
            };
		};
	};
	class rhs_acc_1p29: rhs_acc_sniper_base {
		class ItemInfo: InventoryOpticsItem_Base_F {
			class OpticsModes {
                class rhs_1p29_scope {
                    opticsZoomMax = "0.13";
                    opticsZoomMin = "0.13";
                    opticsZoomInit = "0.13";
                };
            };
		};
	};
	class rhs_acc_1p78: rhs_acc_1p29 {
		class ItemInfo: InventoryOpticsItem_Base_F {
			class OpticsModes {
                class rhs_1p29_scope {
                    opticsZoomMax = "0.148 * 1.2";
                    opticsZoomMin = "0.148 * 1.2";
                    opticsZoomInit = "0.148 * 1.2";
                };
            };
		};
	};

    class rhs_acc_1p78_3d: rhs_acc_1p78 {
        class ItemInfo: ItemInfo {
            class OpticsModes: OpticsModes {
                class rhs_1p29_scope: rhs_1p29_scope {
                   opticsZoomMax = 0.148;
                   opticsZoomMin = 0.148;
                   opticsZoomInit = 0.148;
                };
            };
        };
    };

	class rhs_acc_pkas: rhs_acc_sniper_base {
		class ItemInfo: InventoryOpticsItem_Base_F {
			 class OpticsModes {
                class ACO {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.1;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "opticview";
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    distanceZoomMin = 100;
                    distanceZoomMax = 100;
                    cameraDir = "";
                    visionMode[] = {};
                    opticsPPEffects[] = {"OpticsBlur1"};
                };

                class rhs_1p29_iron {
                };
				class ACOscope {
                    cameradir = "";
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                    memorypointcamera = "opticView";
                    opticsdisableperipherialvision = 0;
                    opticsflare = 1;
                    opticsid = 2;
                    opticsppeffects[] = {"OpticsCHAbera1", "OpticsBlur1"};
                    opticszoominit = 0.1875;
                    opticszoommax = 0.1875;
                    opticszoommin = 0.1875;
                    discretefov[] = {0.1875};
                    discreteinitindex = 0;
                    usemodeloptics = 0;
                    visionmode[] = {};
                };
            };
		};
	};
	//Flashlights
	class acc_pointer_IR: ItemCore {};
    class InventoryItem_Base_F;
    class InventoryFlashLightItem_Base_F: InventoryItem_Base_F { };
	class rhsusf_acc_anpeq15: acc_pointer_IR {};
	class rhsusf_acc_anpeq15_light: rhsusf_acc_anpeq15
	{
		class ItemInfo: InventoryFlashLightItem_Base_F
		{
			class Pointer
			{
			};
			class FlashLight
			{
				color[] = {25,22,20};
				ambient[] = {0.001,0.001,0.001};
				size = 1;
				innerAngle = 50;
				outerAngle = 120;
				coneFadeCoef = 10;
				intensity = 530;
				useFlare = 1;
				FlareSize = 6.7;
				class Attenuation
                {
                    start = 160;
                    constant = 280;
                    linear = 160;
                    quadratic = 40;
                    hardLimitStart = 420;
                    hardLimitEnd = 690;
                };
			};
		};
	};
};
