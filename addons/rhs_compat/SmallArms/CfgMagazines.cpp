#include "..\macros.hpp"

class CfgMagazines {
	class Default;
	class CA_Magazine: Default {};
    class frl_mag_suicide: CA_Magazine {
        author = "Bohemia Interactive";
        scope = 2;
        type = 16;
        displayName = "Vest Detonator";
        displayNameShort = "Suicide Vest";
        picture = "\rhsusf\addons\rhsusf_weapons\icons\m_12g_frag.paa";
        ammo = "suicide_HE";
        initSpeed = 1;
        count = 1;
        nameSound = "";
        descriptionShort = "Type: Suicide Detonator<br />Caliber: Big <br />Rounds: 1<br />Used in: Haji Mode";
        mass = 4;
    };
	class 60Rnd_CMFlareMagazine: CA_Magazine {
        author = "Bohemia Interactive";
        count = 32;
        ammo = "CMflareAmmo";
        initSpeed = 30;
    };
    class 120Rnd_CMFlareMagazine: 60Rnd_CMFlareMagazine {
        count = 32;
    };
    class 240Rnd_CMFlareMagazine: 60Rnd_CMFlareMagazine {
        count = 32;
    };
    class 60Rnd_CMFlare_Chaff_Magazine: 60Rnd_CMFlareMagazine {
        ammo = "CMflare_Chaff_Ammo";
    };
    class 120Rnd_CMFlare_Chaff_Magazine: 60Rnd_CMFlare_Chaff_Magazine {
        count = 32;
    };
    class 240Rnd_CMFlare_Chaff_Magazine: 60Rnd_CMFlare_Chaff_Magazine {
        count = 32;
    };
    class 192Rnd_CMFlare_Chaff_Magazine: 60Rnd_CMFlare_Chaff_Magazine {
        count = 32;
    };
    class 168Rnd_CMFlare_Chaff_Magazine: 60Rnd_CMFlare_Chaff_Magazine {
        count = 32;
    };
    class 300Rnd_CMFlare_Chaff_Magazine: 60Rnd_CMFlare_Chaff_Magazine {
        count = 32;
    };
	class rhsgref_5Rnd_762x54_m38: CA_Magazine {
		ammo = "rhs_B_762x54_7N1_Ball";
	};
};
