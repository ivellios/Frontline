#include "..\macros.hpp"

#define RHSBASECLASS rhs_vests
class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
    // author = AUTHOR_STR;
    // authors[] = AUTHOR_ARR;
    // authorUrl = AUTHOR_URL;
    version = VERSION;
    versionStr = QUOTE(VERSION);
    versionAr[] = {VERSION_AR};
		addonRootClass="FRL_Rhs_compat";
		requiredAddons[] = {
			"FRL_Main",
			"FRL_Rhs_compat"
		};
	};
};

class CfgWeapons {
	class ItemCore;
	class InventoryItem_Base_F;
	class VestItem;

	class Vest_Camo_Base : ItemCore {
		class ItemInfo: VestItem {};
	};

	class rhs_6b23 : Vest_Camo_Base {
		class ItemInfo: ItemInfo {
			class HitpointsProtectionInfo {
				class Neck {
					HitpointName = "HitNeck";
					armor = 8;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b23_crew : rhs_6b23 {};
	class rhs_6b23_engineer : rhs_6b23 {};
	class rhs_6b23_medic : rhs_6b23 {};
	class rhs_6b23_rifleman : rhs_6b23 {};
	class rhs_6b23_crewofficer : rhs_6b23 {};
	class rhs_6b23_sniper : rhs_6b23 {};
	class rhs_6b23_6sh92 : rhs_6b23 {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b23_6sh92_vog : rhs_6b23_6sh92 {};
	class rhs_6b23_6sh92_vog_headset : rhs_6b23_6sh92_vog {};
	class rhs_6b23_6sh92_headset : rhs_6b23_6sh92 {};
	class rhs_6b23_6sh92_headset_mapcase : rhs_6b23_6sh92 {};
	class rhs_6b23_6sh92_radio : rhs_6b23_6sh92 {};
	class rhs_6sh46 : Vest_Camo_Base {};
	class rhs_vest_commander : Vest_Camo_Base {};
	class rhs_vest_pistol_holster : rhs_vest_commander {};
	class rhs_6b23_digi : rhs_6b23 {};
	class rhs_6b23_digi_crew : rhs_6b23_crew {};
	class rhs_6b23_digi_engineer : rhs_6b23_engineer {};
	class rhs_6b23_digi_medic : rhs_6b23_medic {};
	class rhs_6b23_digi_rifleman : rhs_6b23_rifleman {};
	class rhs_6b23_digi_crewofficer : rhs_6b23_crewofficer {};
	class rhs_6b23_digi_sniper : rhs_6b23_sniper {};
	class rhs_6b23_digi_6sh92 : rhs_6b23_6sh92 {};
	class rhs_6b23_digi_6sh92_vog : rhs_6b23_6sh92_vog {};
	class rhs_6b23_digi_6sh92_vog_headset : rhs_6b23_6sh92_vog_headset {};
	class rhs_6b23_digi_6sh92_headset : rhs_6b23_6sh92_headset {};
	class rhs_6b23_digi_6sh92_headset_mapcase : rhs_6b23_6sh92_headset_mapcase {};
	class rhs_6b23_digi_6sh92_radio : rhs_6b23_6sh92_radio {};
	class rhs_6b23_digi_6sh92_Spetsnaz : rhs_6b23_6sh92_radio {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 8;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b23_digi_6sh92_Vog_Radio_Spetsnaz : rhs_6b23_6sh92_radio {};
	class rhs_6b23_digi_6sh92_headset_spetsnaz : rhs_6b23_6sh92_radio {};
	class rhs_6b23_ML : rhs_6b23 {};
	class rhs_6b23_ML_crew : rhs_6b23_crew {};
	class rhs_6b23_ML_engineer : rhs_6b23_engineer {};
	class rhs_6b23_ML_medic : rhs_6b23_medic {};
	class rhs_6b23_ML_rifleman : rhs_6b23_rifleman {};
	class rhs_6b23_ML_crewofficer : rhs_6b23_crewofficer {};
	class rhs_6b23_ML_sniper : rhs_6b23_sniper {};
	class rhs_6b23_ML_6sh92 : rhs_6b23_6sh92 {};
	class rhs_6b23_ML_6sh92_vog : rhs_6b23_6sh92_vog {};
	class rhs_6b23_ML_6sh92_vog_headset : rhs_6b23_6sh92_vog_headset {};
	class rhs_6b23_ML_6sh92_headset : rhs_6b23_6sh92_headset {};
	class rhs_6b23_ML_6sh92_headset_mapcase : rhs_6b23_6sh92_headset_mapcase {};
	class rhs_6b23_ML_6sh92_radio : rhs_6b23_6sh92_radio {};
	class rhs_vydra_3m : Vest_Camo_Base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					armor = 0;
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b23_vydra_3m : rhs_6b23_6sh92 {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 8;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b23_digi_vydra_3m : rhs_6b23_6sh92 {};
	class rhs_6b23_ML_vydra_3m : rhs_6b23_6sh92 {};
	class rhs_6b23_6sh116: rhs_6b23_digi {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b23_6sh116_flora: rhs_6b23_6sh116 {};
	class rhs_6b23_6sh116_od: rhs_6b23_6sh116 {};
	class rhs_6b23_6sh116_vog: rhs_6b23_6sh116 {};
	class rhs_6b23_6sh116_vog_flora: rhs_6b23_6sh116_vog {};
	class rhs_6b23_6sh116_vog_od: rhs_6b23_6sh116_vog {};
	class rhs_6b13 : rhs_6b23 {
		class ItemInfo: ItemInfo
		{

			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 16;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 28;
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b13_crewofficer : rhs_6b13 {};
	class rhs_6b13_6sh92 : rhs_6b13 {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 16;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6b13_6sh92_vog : rhs_6b13_6sh92 {};
	class rhs_6b13_6sh92_headset_mapcase : rhs_6b13_6sh92 {};
	class rhs_6b13_6sh92_radio : rhs_6b13_6sh92 {};
	class rhs_6b13_EMR : rhs_6b13 {};
	class rhs_6b13_Flora : rhs_6b13 {};
	class rhs_6b13_Flora_crewofficer : rhs_6b13_crewofficer {};
	class rhs_6b13_Flora_6sh92 : rhs_6b13_6sh92 {};
	class rhs_6b13_Flora_6sh92_vog : rhs_6b13_6sh92_vog {};
	class rhs_6b13_Flora_6sh92_headset_mapcase : rhs_6b13_6sh92_headset_mapcase {};
	class rhs_6b13_Flora_6sh92_radio : rhs_6b13_6sh92_radio {};
	class rhs_6sh92 : Vest_Camo_Base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					armor = 0;
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhs_6sh92_vog : rhs_6sh92 {};
	class rhs_6sh92_vog_headset : rhs_6sh92_vog {};
	class rhs_6sh92_headset : rhs_6sh92 {};
	class rhs_6sh92_radio : rhs_6sh92 {};
	class rhs_6sh92_digi : rhs_6sh92 {};
	class rhs_6sh92_digi_vog : rhs_6sh92_vog {};
	class rhs_6sh92_digi_vog_headset : rhs_6sh92_vog_headset {};
	class rhs_6sh92_digi_headset : rhs_6sh92_headset {};
	class rhs_6sh92_digi_radio : rhs_6sh92_radio {};
	class rhsusf_iotv_ocp_base : Vest_Camo_Base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ocp : rhsusf_iotv_ocp_base {};
	class rhsusf_iotv_ocp_Grenadier : rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ocp_Medic : rhsusf_iotv_ocp_base {};
	class rhsusf_iotv_ocp_Repair : rhsusf_iotv_ocp_base {};
	class rhsusf_iotv_ocp_Rifleman : rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ocp_SAW : rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ocp_Squadleader : rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ocp_Teamleader : rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ucp_base : rhsusf_iotv_ocp_base {};
	class rhsusf_iotv_ucp : rhsusf_iotv_ocp {};
	class rhsusf_iotv_ucp_Grenadier : rhsusf_iotv_ucp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ucp_Medic : rhsusf_iotv_ucp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ucp_Repair : rhsusf_iotv_ucp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ucp_Rifleman : rhsusf_iotv_ucp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ucp_SAW : rhsusf_iotv_ucp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ucp_Squadleader : rhsusf_iotv_ucp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_iotv_ucp_Teamleader : rhsusf_iotv_ucp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					HitpointName = "HitNeck";
					armor = 12;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "22+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc : rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 28;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_rifleman : rhsusf_spc {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_iar : rhsusf_spc_rifleman {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_corpsman : rhsusf_spc_rifleman {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_crewman : rhsusf_spc_rifleman {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.4;
				};
			};
		};
	};
	class rhsusf_spc_light : rhsusf_spc_rifleman {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_marksman : rhsusf_spc_rifleman {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_mg : rhsusf_spc_rifleman {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_squadleader : rhsusf_spc_rifleman {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spc_teamleader : rhsusf_spc_rifleman {};
	class rhsusf_spc_patchless: rhsusf_spc_light {};
	class rhsusf_spc_patchless_radio: rhsusf_spc_squadleader {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = "28+ 3";
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 3;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_spcs_ocp : rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 28;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 28;
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 12;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_mbav: rhsusf_iotv_ocp_base {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_mbav_light: rhsusf_mbav {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_mbav_rifleman: rhsusf_mbav_light {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_mbav_mg: rhsusf_mbav_light {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_mbav_grenadier: rhsusf_mbav_light {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.7;
				};
			};
		};
	};
	class rhsusf_mbav_medic: rhsusf_mbav_light {
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.7;
				};
			};
		};
	};
	
	class rhsusf_spcs_ocp_rifleman: rhsusf_spcs_ocp {};
	class rhsusf_spcs_ucp : rhsusf_spcs_ocp {};
	class rhsusf_spcs_ucp_rifleman: rhsusf_spcs_ocp_rifleman {};

	//------------insurgents vests-------------
	
	//copied from rhs_6b23
	class V_TacVest_khk: Vest_Camo_Base {
		class ItemInfo: ItemInfo {
			class HitpointsProtectionInfo {
				class Neck {
					HitpointName = "HitNeck";
					armor = 8;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};

		
	class V_TacVest_camo: Vest_Camo_Base {
		class ItemInfo: ItemInfo {
			class HitpointsProtectionInfo {
				class Neck {
					HitpointName = "HitNeck";
					armor = 8;
					PassThrough = 0.7;
				};
				class Chest
				{
					HitpointName = "HitChest";
					armor = 22;
					PassThrough = 0.7;
				};
				class Diaphragm
				{
					HitpointName = "HitDiaphragm";
					armor = 22;
					PassThrough = 0.7;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 22;
					PassThrough = 0.7;
				};
				class Body
				{
					hitpointName = "HitBody";
					PassThrough = 0.7;
				};
			};
		};
	};
};
