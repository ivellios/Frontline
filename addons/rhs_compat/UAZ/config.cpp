#include "..\macros.hpp"

#define RHSBASECLASS RHS_UAZ_Base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
		requiredAddons[] = {
			"FRL_Main",
			"FRL_Rhs_compat"
		};
	};
};

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)

TURRET_INHERIT
	class Car: LandVehicle {};
	class Car_F: Car {};
	class Offroad_01_base_F : Car_F {};
	class RHS_UAZ_Base : Offroad_01_base_F {};
	class RHS_UAZ_DShKM_Base : RHS_UAZ_Base {
	class Turrets : Turrets {
            class MainTurret : MainTurret {
                weapons[] = {"rhs_weap_DSHKM"};
                magazines[] = {"rhs_mag_127x108mm_150", "rhs_mag_127x108mm_150", "rhs_mag_127x108mm_150", "rhs_mag_127x108mm_150", "rhs_mag_127x108mm_150"};
                stabilizedInAxes = 3;
            };
        };
	};
};