#include "..\macros.hpp"
#define RHSBASECLASS rhs_btr_base

class CfgPatches {
  class DOUBLE(ADDON,RHSBASECLASS) {
    units[] = {
      QUOTE(RHSBASECLASS)
    };
    weapons[] = {};
    requiredVersion = REQUIRED_VERSION;
    // author = AUTHOR_STR;
    // authors[] = AUTHOR_ARR;
    // authorUrl = AUTHOR_URL;
    version = VERSION;
    versionStr = QUOTE(VERSION);
    versionAr[] = {
      VERSION_AR
    };
    addonRootClass = "FRL_Rhs_compat";
    requiredAddons[] = {
      "FRL_Main",
      "FRL_Rhs_compat"
    };
  };
};

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
TURRET_INHERIT
class Car: LandVehicle {};
class Car_F: Car {};
class Wheeled_APC_F: Car_F {
  class Turrets {
    class MainTurret: NewTurret {
      class Turrets {
        class CommanderOptics: CommanderOptics {
          class ViewGunner: ViewCargo {};
        };
      };
    };
  };
};
class RHSBASECLASS: Wheeled_APC_F {
  class Turrets: Turrets {
    class MainTurret: MainTurret {
      //turretInfoType = "RscOptics_APC_Wheeled_03_gunner";
      //gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";
      gunnerOpticsEffect[] = {
        "TankGunnerOptics1",
        "WeaponsOptics",
        "OpticsCHAbera3"
      };
      gunnerOpticsColor[] = {
        1,
        1,
        1,
        1
      };
      stabilizedInAxes = 3; //"StabilizedInAxesNone";
      class HitPoints {
        class HitTurret {
          armor = 0.4; //.8
          material = -1;
          name = "vez";
          visual = "vez";
          passThrough = 1;
          minimalHit = 0.44;
        };
        class HitGun {
          armor = 0.2; //.4
          material = -1;
          name = "zbran";
          visual = "zbran";
          passThrough = 0.1;
          minimalHit = 0.44;
        };
      };
      class ViewGunner {
        initAngleX = 5;
        minAngleX = -65;
        maxAngleX = 85;
        initAngleY = 0;
        minAngleY = -150;
        maxAngleY = 150;
        initFov = 0.2;
        minFov = 0.2;
        maxFov = 0.4;
      };
      class OpticsIn {
        class Wide {
          initAngleX = 5;
          minAngleX = -65;
          maxAngleX = 85;
          initAngleY = 0;
          minAngleY = -150;
          maxAngleY = 150;
          minFov = 0.4; // D: 0.117
          maxFov = 0.4; // D: 0.117
          initFov = 0.4;
          visionMode[] = {
            "Normal",
            "Ti"
          };
          thermalMode[] = {
            4
          }; // {4}
          // gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";
        };

        class Narrow: Wide {
          initAngleX = 5;
          minAngleX = -65;
          maxAngleX = 85;
          initAngleY = 0;
          minAngleY = -150;
          maxAngleY = 150;
          // gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d";
          initfov = 0.2;
          minFov = 0.2;
          maxFov = 0.2;
        };
      };
      class Turrets {};
    };
  };
};
class rhs_btr60_base: RHSBASECLASS {
  class Turrets: Turrets {
    class MainTurret: MainTurret {
      // turretInfoType = "RscOptics_APC_Wheeled_03_gunner";
      // gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d";
      gunnerOpticsEffect[] = {
        "TankGunnerOptics1",
        "WeaponsOptics",
        "OpticsCHAbera3"
      };
      gunnerOpticsColor[] = {
        1,
        1,
        1,
        1
      };
      stabilizedInAxes = 3; //"StabilizedInAxesNone";

      class ViewOptics {
        initAngleX = 0;
        minAngleX = -30;
        maxAngleX = 30;
        initAngleY = 0;
        minAngleY = -100;
        maxAngleY = 100;
        initFov = 0.2;
        minFov = 0.2;
        maxFov = 0.2;
      };
      class ViewGunner {
        initAngleX = 5;
        minAngleX = -65;
        maxAngleX = 85;
        initAngleY = 0;
        minAngleY = -150;
        maxAngleY = 150;
        initFov = 0.7;
        minFov = 0.25;
        maxFov = 1.1;
      };
      class OpticsIn {
        class Wide {
          initAngleX = 5;
          minAngleX = -65;
          maxAngleX = 85;
          initAngleY = 0;
          minAngleY = -150;
          maxAngleY = 150;
          minFov = 0.4; // D: 0.117
          maxFov = 0.4; // D: 0.117
          initFov = 0.4;
          visionMode[] = {
            "Normal",
            "Ti"
          };
          thermalMode[] = {
            4
          }; // {4}
          gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";
        };

        class Narrow: Wide {
          initAngleX = 5;
          minAngleX = -65;
          maxAngleX = 85;
          initAngleY = 0;
          minAngleY = -150;
          maxAngleY = 150;
          gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d";
          initfov = 0.2;
          minFov = 0.2;
          maxFov = 0.2;
        };
      };
      class Turrets {};
    };
  };
};
class rhs_btr60_vmf: rhs_btr60_base {};
class rhs_btr60_vv: rhs_btr60_vmf {};
class rhs_btr60_msv: rhs_btr60_vmf {};
class rhs_btr70_vmf: rhs_btr_base {};
class rhs_btr70_vdv: rhs_btr70_vmf {};
class rhs_btr70_vv: rhs_btr70_vmf {};
class rhs_btr70_msv: rhs_btr70_vmf {};
class rhs_btr80_msv: rhs_btr70_msv {
  class Turrets: Turrets {
    class MainTurret: MainTurret {
      //gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
      //turretInfoType = "RscOptics_APC_Wheeled_03_gunner";
      //stabilizedInAxes = "1";//"StabilizedInAxesNone";
      // minElev = -9; // D: -9
      // maxElev = 57; // D: 57
      // maxHorizontalRotSpeed = 1.04; // D: 1.04
      // maxVerticalRotSpeed = 1.04; // D: 1.04
      stabilizedInAxes = 3; // D: 3
      magazines[] = {
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_145x115mm_50",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_3d17_6",
        "rhs_mag_3d17_6",
        "rhs_mag_3d17_6",
        "rhs_mag_3d17_6"
      };
      class OpticsIn {
        class 1pz3x12 {
          initAngleX = 0;
          minAngleX = -110;
          maxAngleX = "+110";
          initAngleY = 0;
          minAngleY = -110;
          maxAngleY = "+110";
          opticsZoomMin = 0.14;
          opticsZoomMax = 0.14;
          distanceZoomMin = 200;
          distanceZoomMax = 2000;
          initFov = 0.4; //initFov = "0.7/1.2";
          minFov = 0.4; // D: 0.117
          maxFov = 0.4; // D: 0.117
          visionMode[] = {
            "Normal"
          };
          //gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";
          gunnerOpticsEffect[] = {
            "TankGunnerOptics1",
            "OpticsBlur2",
            "OpticsCHAbera3"
          };
        };
        class 1pz3x4: 1pz3x12 {
          initFov = "0.7/4";
          //gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d";
        };
        class bpk142n {
          initAngleX = 0;
          minAngleX = -110;
          maxAngleX = "+110";
          initAngleY = 0;
          minAngleY = -110;
          maxAngleY = "+110";
          opticsZoomMin = 0.14;
          opticsZoomMax = 0.14;
          distanceZoomMin = 200;
          distanceZoomMax = 2000;
          initfov = 0.2;
          minFov = 0.2;
          maxFov = 0.2;
          visionMode[] = {
            "NVG"
          };
          //gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d";
          gunnerOpticsEffect[] = {
            "TankGunnerOptics1",
            "OpticsBlur2",
            "OpticsCHAbera3"
          };
        };
      };
      /*class OpticsIn {
      	class Wide {
      		initAngleX = 5;
      		minAngleX = -65;
      		maxAngleX = 85;
      		initAngleY = 0;
      		minAngleY = -150;
      		maxAngleY = 150;
      		minFov = 0.4; // D: 0.117
      		maxFov = 0.4; // D: 0.117
      		initFov = 0.4;
      		visionMode[] = {"Normal", "Ti"};
      		thermalMode[] = {4}; // {4}
      		gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";
      	};

      	class Narrow: Wide {
      		initAngleX = 5;
      		minAngleX = -65;
      		maxAngleX = 85;
      		initAngleY = 0;
      		minAngleY = -150;
      		maxAngleY = 150;
      		gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d";
      		initfov = 0.2;
      		minFov = 0.2;
      		maxFov = 0.2;
      	};
      };*/
    };

  };
};
class rhs_btr80_vdv: rhs_btr80_msv {};
class rhs_btr80_vv: rhs_btr80_msv {};
class rhs_btr80_vmf: rhs_btr80_msv {};
class rhs_btr80a_msv: rhs_btr80_msv {
  class Turrets: Turrets {
    class MainTurret: MainTurret {
      // turretInfoType = "RscOptics_APC_Wheeled_03_gunner";
      minElev = -7;
      maxElev = 70;
      minTurn = -360;
      maxTurn = 360;
      class OpticsIn {
        class bpk142 {
          initAngleX = 0;
          minAngleX = -110;
          maxAngleX = "+110";
          initAngleY = 0;
          minAngleY = -110;
          maxAngleY = "+110";
          opticsZoomMin = 0.14;
          opticsZoomMax = 0.14;
          distanceZoomMin = 200;
          distanceZoomMax = 2000;
          initFov = "0.7/5.6";
          minFov = 0.14;
          maxFov = 0.14;
          visionMode[] = {
            "Normal"
          };
          // gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";
          gunnerOpticsEffect[] = {
            "TankGunnerOptics1",
            "OpticsBlur2",
            "OpticsCHAbera3"
          };
        };
        class bpk142n {
          initAngleX = 0;
          minAngleX = -110;
          maxAngleX = "+110";
          initAngleY = 0;
          minAngleY = -110;
          maxAngleY = "+110";
          opticsZoomMin = 0.14;
          opticsZoomMax = 0.14;
          distanceZoomMin = 200;
          distanceZoomMax = 2000;
          initFov = 0.14;
          minFov = 0.14;
          maxFov = 0.14;
          visionMode[] = {
            "NVG"
          };
          // gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";
          gunnerOpticsEffect[] = {
            "TankGunnerOptics1",
            "OpticsBlur2",
            "OpticsCHAbera3"
          };
        };
      };
      weapons[] = {
        "rhs_weap_2a72",
        "rhs_weap_pkt_btr",
        "rhs_weap_902a"
      };
      magazines[] = {
        "rhs_mag_3uof8_340",
        "rhs_mag_3ubr6_160",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_762x54mm_250",
        "rhs_mag_3d17_6",
        "rhs_mag_3d17_6",
        "rhs_mag_3d17_6"
      };
    };
  };

};
};

#define SIZE_HEIGHT_VEH GRIDY(5)# define SIZE_HEIGHT_VEH_WPN GRIDY(3.65)# define SIZE_WIDTH_VEH GRIDX(14)
#define SPACING_X GRIDX(0.5)# define SPACING_Y GRIDY(0.9)# define ORIGIN_X_VEH safeZoneX + safeZoneW - SIZE_WIDTH_VEH - SPACING_X# define ORIGIN_Y_VEH safeZoneY + safeZoneH - SIZE_HEIGHT_VEH - GRIDY(0.4)

class RscControlsGroupNoScrollbars;
class RscText;
class RscIngameUI {
  class RscUnitInfo;
  class RHS_RscUnitInfoBTR: RscUnitInfo {
    class RHS_cmControls: RscControlsGroupNoScrollbars {
      x = ORIGIN_X_VEH;
      y = ORIGIN_Y_VEH;
      w = SIZE_WIDTH_VEH;
      h = SIZE_HEIGHT_VEH_WPN;

      class controls {
        class RHS_cmType: RscText {
          style = 2;
          idc = 52;
          font = FONTMED;
          colorText[] = COLOR_TEXT;
          x = GRIDX(0);
          y = GRIDY(2.5);
          w = GRIDX(3.3);
          h = GRIDY(0.8);
          sizeEx = GRIDY(0.8);
          text = "";
        };
        class RHS_cmCount: RscText {
          font = FONTMED;
          colorText[] = COLOR_TEXT;
          style = 1;
          idc = 51;
          x = GRIDX(3.8);
          y = GRIDY(2.5);
          w = GRIDX(3);
          h = GRIDY(0.8);
          sizeEx = GRIDY(0.8);
          text = "";
        };
      };
    };
  };
};
