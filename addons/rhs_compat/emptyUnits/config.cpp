#include "..\macros.hpp"

#define RHSBASECLASS EmptyUnits

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {
      "FRL_B_Soldier_USA_W_F",
      "FRL_B_Soldier_USA_D_F",
      "FRL_B_Soldier_USMC_F",
      "FRL_I_Soldier_INS_W_F",
      "FRL_O_Soldier_RUA_W_F",
      "FRL_O_Soldier_RUA_D_F"
    };
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
    // author = AUTHOR_STR;
    // authors[] = AUTHOR_ARR;
    // authorUrl = AUTHOR_URL;
    version = VERSION;
    versionStr = QUOTE(VERSION);
    versionAr[] = {VERSION_AR};
    addonRootClass="FRL_Rhs_compat";
    requiredAddons[] = {
        "FRL_Main",
        "FRL_Rhs_compat"
    };
	};
};

class cfgVehicles {
  class FRL_B_Soldier_F;
  class FRL_B_Soldier_USA_W_F: FRL_B_Soldier_F {
    uniformClass = "rhs_uniform_cu_ocp";
  };
  class FRL_B_Soldier_USA_D_F: FRL_B_Soldier_F {
    uniformClass = "rhs_uniform_cu_ucp";
  };
  class FRL_B_Soldier_USMC_F: FRL_B_Soldier_F {
    uniformClass = "rhs_uniform_FROG01_wd";
  };

  class FRL_I_Soldier_F;
  class FRL_I_Soldier_INS_W_F: FRL_I_Soldier_F {
    uniformClass = "rhsgref_uniform_para_ttsko_urban";
  };

  class FRL_O_Soldier_F;
  class FRL_O_Soldier_RUA_W_F: FRL_O_Soldier_F {
    uniformClass = "rhs_uniform_emr_patchless";
  };
  class FRL_O_Soldier_RUA_D_F: FRL_O_Soldier_F {
    uniformClass = "rhs_uniform_emr_des_patchless";
  };
};
