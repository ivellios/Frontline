#include "..\macros.hpp"
#define RHSBASECLASS rhs_gaz66_zu23_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

TURRET_INHERIT
	class Car: LandVehicle {};
	class Car_F: Car {};
	class Truck_F: Car_F {};
	class rhs_truck: Truck_F {};
	class rhs_gaz66_vmf: rhs_truck {};
	class rhs_gaz66_zu23_base: rhs_gaz66_vmf  {

			class Turrets: Turrets {
				class MainTurret: MainTurret
				{
					magazines[] = {"RHS_mag_AZP23_100", "RHS_mag_AZP23_100", "RHS_mag_AZP23_100", "RHS_mag_AZP23_100", "RHS_mag_AZP23_100"};
					class ViewOptics
					{
						initFov = 0.3; //0.093
						minFov = 0.3;
						maxFov = 0.3;
					};
                };
            };
        };
	class StaticWeapon: LandVehicle {};
	class StaticCannon: StaticWeapon {};
    class RHS_ZU23_base: StaticCannon {
			class Turrets: Turrets
			{
				class MainTurret: MainTurret
				{
					magazines[] = {"RHS_mag_AZP23_100", "RHS_mag_AZP23_100", "RHS_mag_AZP23_100", "RHS_mag_AZP23_100", "RHS_mag_AZP23_100"};

					class ViewOptics
					{
						initFov = 0.3; //0.093
						minFov = 0.3;
						maxFov = 0.3;
					};
				};
            };
        };
};
	class CfgWeapons{
	class Default;
	class CannonCore: Default {};
	class RHS_weap_AZP23: CannonCore {};
        class rhs_weap_2A14: RHS_weap_AZP23  {
                magazineReloadTime = 15;
		};
	};



