#include "..\macros.hpp"
#define RHSBASECLASS rhs_SPG9M_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

    TURRET_INHERIT
    class rhs_SPG9_base;
    class rhs_SPG9M_base: rhs_SPG9_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            magazines[] = {"rhs_mag_PG9VNT", "rhs_mag_PG9VNT","rhs_mag_PG9VNT", "rhs_mag_PG9VNT", "rhs_mag_PG9VNT", "rhs_mag_PG9VNT", "rhs_mag_PG9VNT", "rhs_mag_PG9VNT", "rhs_mag_OG9VM", "rhs_mag_OG9VM", "rhs_mag_OG9VM", "rhs_mag_OG9VM", "rhs_mag_OG9VM", "rhs_mag_OG9VM", "rhs_mag_OG9VM"};
            };
        };
    };
};




