#include "..\macros.hpp"

#define RHSBASECLASS Helicopter_Base_H

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

TURRET_INHERIT
	class AllVehicles;
	class Air: AllVehicles {};
	class Helicopter: Air {};
	class Helicopter_Base_F: Helicopter {};
	class Helicopter_Base_H: Helicopter_Base_F {};
	class Heli_light_03_base_F : Helicopter_Base_F {
		/*liftForceCoef = 2.3;
		cyclicForwardForceCoef = 3.0;
		class RotorLibHelicopterProperties {
            RTDconfig = "A3\Air_F_EPB\Heli_Light_03\RTD_Heli_Light_03.xml";
            defaultCollective = 0.7;
            autoHoverCorrection[] = {4, 3.3, 0};
            maxTorque = 2700;
            stressDamagePerSec = 0.00333333;
            retreatBladeStallWarningSpeed = 87.5;
            maxHorizontalStabilizerLeftStress = 100000;
            maxHorizontalStabilizerRightStress = 100000;
            maxVerticalStabilizerStress = 100000;
            horizontalWingsAngleCollMin = 0;
            horizontalWingsAngleCollMax = 0;
            maxMainRotorStress = 1100000;
            maxTailRotorStress = 250000;
        };*/
	};
};
