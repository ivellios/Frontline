#include "..\macros.hpp"

#define RHSBASECLASS rhsusf_hmmwe_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
		requiredAddons[] = {
			"FRL_Main",
			"FRL_Rhs_compat"
		};
	};
};

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)

TURRET_INHERIT
	class Car: LandVehicle {};
	class Car_F: Car {};
	class MRAP_01_base_F : Car_F {};
	class RHSBASECLASS: MRAP_01_base_F {

	};
	class rhsusf_m998_w_2dr : RHSBASECLASS {
		acceleration = 1;
		maxSpeed = 80;
		//new test
		torqueCurve[] = {{"(0/3200)", "(0/284)"}, {"(1000/3200)", "(255/284)"}, {"(1200/3200)", "(267/284)"}, {"(1600/3200)", "(273/284)"}, {"(2000/3200)", "(284/284)"}, {"(2500/3200)", "(284/284)"}, {"(2800/3200)", "(273/284)"}, {"(3200/3200)", "(132/284)"}};
        maxOmega = 335.1;
        enginePower = 89;
        peakTorque = "(284*1)";
        idleRPM = 500;
        redRPM = 3200;
        engineLosses = 10;
        thrustDelay = 1.5;
		turnCoef = 5;
		//
	};
	class rhsusf_m998_d_2dr : rhsusf_m998_w_2dr {};
	class rhsusf_m998_w_s_2dr : rhsusf_m998_w_2dr {};
	class rhsusf_m998_d_s_2dr : rhsusf_m998_w_s_2dr {};
	class rhsusf_m998_w_2dr_halftop : rhsusf_m998_w_2dr {};
	class rhsusf_m998_d_2dr_halftop : rhsusf_m998_w_2dr_halftop {};
	class rhsusf_m998_w_s_2dr_halftop : rhsusf_m998_w_2dr_halftop {};
	class rhsusf_m998_d_s_2dr_halftop : rhsusf_m998_w_s_2dr_halftop {};
	class rhsusf_m998_w_2dr_fulltop : rhsusf_m998_w_2dr_halftop {};
	class rhsusf_m998_d_2dr_fulltop : rhsusf_m998_w_2dr_fulltop {};
	class rhsusf_m998_w_s_2dr_fulltop : rhsusf_m998_w_2dr_fulltop {};
	class rhsusf_m998_d_s_2dr_fulltop : rhsusf_m998_w_s_2dr_fulltop {};
	class rhsusf_m998_w_4dr : rhsusf_m998_w_2dr {};
	class rhsusf_m998_d_4dr : rhsusf_m998_w_4dr {};
	class rhsusf_m998_w_s_4dr : rhsusf_m998_w_4dr {};
	class rhsusf_m998_d_s_4dr : rhsusf_m998_w_s_4dr {};
	class rhsusf_m998_w_4dr_halftop : rhsusf_m998_w_4dr {};
	class rhsusf_m998_d_4dr_halftop : rhsusf_m998_w_4dr_halftop {};
	class rhsusf_m998_w_s_4dr_halftop : rhsusf_m998_w_4dr_halftop {};
	class rhsusf_m998_d_s_4dr_halftop : rhsusf_m998_w_s_4dr_halftop {};
	class rhsusf_m998_w_4dr_fulltop : rhsusf_m998_w_4dr_halftop {};
	class rhsusf_m998_d_4dr_fulltop : rhsusf_m998_w_4dr_fulltop {	};
	class rhsusf_m998_w_s_4dr_fulltop : rhsusf_m998_d_4dr_fulltop {	};
	class rhsusf_m998_d_s_4dr_fulltop : rhsusf_m998_w_s_4dr_fulltop {	};
	class rhsusf_m1025_w : rhsusf_m998_w_4dr_fulltop {
		acceleration = 1;
		maxSpeed = 80;
		//newtest
		torqueCurve[] = {{"(0/3200)", "(0/284)"}, {"(1000/3200)", "(255/284)"}, {"(1200/3200)", "(267/284)"}, {"(1600/3200)", "(273/284)"}, {"(2000/3200)", "(284/284)"}, {"(2500/3200)", "(284/284)"}, {"(2800/3200)", "(273/284)"}, {"(3200/3200)", "(132/284)"}};
        maxOmega = 335.1;
        enginePower = 89;
        peakTorque = "(284*1)";
        idleRPM = 500;
        redRPM = 3200;
        engineLosses = 10;
        thrustDelay = 1.5;
		turnCoef = 5;
		//
	};
	class rhsusf_m1025_d : rhsusf_m1025_w {	};
	class rhsusf_m1025_w_s : rhsusf_m1025_w {};
	class rhsusf_m1025_d_s : rhsusf_m1025_w_s {	};
	class rhsusf_m1025_w_m2 : rhsusf_m1025_w {

		class Turrets : Turrets {
            class M2_Turret : MainTurret {
                stabilizedInAxes = 3;
				inGunnerMayFire = 1;
                class ViewOptics : ViewOptics {
                    initFov = 0.7;
                    minFov = 0.25;
                    maxFov = 1.1;
                };
                class ViewGunner : ViewOptics {
                };
            };
        };
	};
	class rhsusf_m1025_d_m2 : rhsusf_m1025_w_m2 {};
	class rhsusf_m1025_w_s_m2 : rhsusf_m1025_w_m2 {};
	class rhsusf_m1025_d_s_m2 : rhsusf_m1025_w_s_m2 {};
	class rhsusf_m1025_w_mk19 : rhsusf_m1025_w_m2 {};
	class rhsusf_m1025_w_s_Mk19 : rhsusf_m1025_w_mk19 {};
	class rhsusf_m1025_d_s_Mk19 : rhsusf_m1025_w_s_Mk19 {};
};