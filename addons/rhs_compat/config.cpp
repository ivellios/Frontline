#include "macros.hpp"

class CfgPatches {
	class ADDON {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};

		requiredAddons[] = {
            "FRL_Main",
            "rhs_c_a2port_armor",
            "rhsusf_a2port_armor",
            "rhs_weapons2",
            "rhs_weapons",
            "rhs_mtlb",
            "rhsusf_c_identity",
            "rhsusf_hemtt_a2",
            "rhsusf_hemtt_a4",
            "RHS_c_radio",
            "rhsgref_c_radio",
            "rhsusf_c_radio",
            "rhs_main",
            "rhsusf_c_uav",
            "rhsusf_main",
            "rhsgref_main",
            "rhs_c_cars",
            "rhs_c_a2port_car",
            "RHS_A2_CarsImport",
            "rhs_c_heavyweapons",
            "rhs_c_weapons",
            "rhsgref_c_a2port_armor",
            "rhsusf_c_heavyweapons",
            "rhs_c_troops",
            "rhs_c_trucks",
            "rhsusf_c_airweapons",
            "rhs_c_a2port_air",
            "RHS_A2_AirImport",
            "rhs_c_a3retex",
            "rhs_c_air",
            "rhs_c_btr",
            "rhs_c_tanks",
            "rhsgref_c_air",
            "rhsgref_c_tohport_air",
            "rhsusf_c_weapons",
            "rhs_cti_insurgents",
            "rhsusf_c_troops",
            "RHS_US_A2_AirImport",
            "RHS_US_A2Port_Armor",
            "rhsusf_c_fmtv",
            "rhsusf_c_HEMTT_A4",
            "rhsusf_vehicles",
            "rhsusf_c_hmmwv",
            "rhsusf_c_m1117",
            "rhsusf_c_m113",
            "rhsusf_c_m1a1",
            "rhsusf_c_rg33",
            "rhsgref_c_vehicles_ret",
            "rhsusf_c_melb"
		};
	};
};

// -- Keep empty. -- //
