#include "..\macros.hpp"

#define KH_GRASS_CUT_COEF 0.55
#define KH_WHEAT_CUT_COEF 0.75

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

class CfgWorlds {

  class DefaultClutter;
  class DefaultWorld;
  class CAWorld: DefaultWorld {};
    class Chernarus: CAWorld {
		class clutter {
			class AutumnFlowers{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.2;
				swLighting = 1;
			};
			class FernAutumn{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.5;
				swLighting = 1;
			};
			class FernAutumnTall{
				affectedByWind = 0;
				scaleMax = 0.7;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassBunch{
				affectedByWind = 0;
				scaleMax = 0.7;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassCrooked{
				affectedByWind = 0;
				scaleMax = 0.7;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassCrookedForest{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassCrookedGreen{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassTall{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class WeedDead{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class WeedDeadSmall{
				affectedByWind = 0;
				scaleMax = 0.9;
				scaleMin = 0.6;
				swLighting = 1;
			};
			class WeedSedge{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.5;
				swLighting = 1;
			};
			class WeedTall{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.5;
				swLighting = 1;
			};
		};
	};
	class Utes: CAWorld {
		class clutter {
			class GrassBunch{
				affectedByWind = 0;
				scaleMax = 0.4;
				scaleMin = 0.2;
				swLighting = 1;
			};
			class GrassCrooked{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassCrookedForest{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassCrookedGreen{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class GrassTall{
				affectedByWind = 0;
				scaleMax = 0.4;
				scaleMin = 0.2;
				swLighting = 1;
			};
			class StubbleClutter{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class UTAutumnFlowers{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.6;
				swLighting = 1;
			};
			class UTFernAutumn{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class UTFernAutumnTall{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.3;
				swLighting = 1;
			};
			class UTGrassDryBunch{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.3;
				swLighting = 1;
			};
			class UTGrassDryLongBunch{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class UTHeatherBrush{
				affectedByWind = 0;
				scaleMax = 0.9;
				scaleMin = 0.6;
				swLighting = 1;
			};
			class UTWeedDead{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.5;
				swLighting = 1;
			};
			class UTWeedDeadSmall{
				affectedByWind = 0;
				scaleMax = 0.7;
				scaleMin = 0.5;
				swLighting = 1;
			};
			class UTWeedSedge{
				affectedByWind = 0;
				scaleMax = 0.7;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class UTWeedTall{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.5;
				swLighting = 1;
			};
		};
	};
	class ChernobylZone: CAWorld {
		class clutter {
			class chz_AutumnFlowers{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.2;
				swLighting = 1;
			};
			class chz_BlueBerry{
				affectedByWind = 0;
				scaleMax = 1.2;
				scaleMin = 0.6;
				swLighting = 1;
			};
			class chz_FernAutumn{
				affectedByWind = 0;
				scaleMax = 0.8;
				scaleMin = 0.3;
				swLighting = 1;
			};
			class chz_FernAutumnTall{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class chz_GrassCrooked{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class chz_GrassCrookedGreen{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class chz_GrassLimnos{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.4;
				swLighting = 1;
			};
			class chz_GrassTall{
				affectedByWind = 0;
				scaleMax = 0.3;
				scaleMin = 0.2;
				swLighting = 1;
			};
			class chz_m_grassDry01{
				affectedByWind = 0;
				scaleMax = 0.7;
				scaleMin = 0.5;
				swLighting = 1;
			};
			class chz_m_grassTall01{
				affectedByWind = 0;
				scaleMax = 0.4;
				scaleMin = 0.3;
				swLighting = 1;
			};
			class chz_m_grassWet01{
				affectedByWind = 0;
				scaleMax = 0.4;
				scaleMin = 0.3;
				swLighting = 1;
			};
			class chz_WeedDead{
				affectedByWind = 0;
				scaleMax = 0.24;
				scaleMin = 0.1;
				swLighting = 1;
			};
		};
	};
	class Bozcaada: CAWorld {
        
		class clutter {
			class FlowerCakile{
				affectedByWind = 0;
			};
			class FlowerLowYellow2{
				affectedByWind = 0;
			};
			class GrassBrushHighGreen{
				affectedByWind = 0;
				scaleMax = 0.4;
				scaleMin = 0.2;
			};
			class GrassBunchSmall{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.2;
			};
			class GrassCrookedDead{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.2;
			};
			class GrassDesertGroupSoft{
				affectedByWind = 0;
			};
			class GrassDry{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.2;
			};
			class GrassGreen{
				affectedByWind = 0;
			};
			class GrassLong_DryBunch{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.2;
			};
			class GrassTall{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.2;
			};
			class GrassTalltwo{
				affectedByWind = 0;
				scaleMax = 0.6;
				scaleMin = 0.2;
			};
			class PlantGreenSmall{
				affectedByWind = 0;
			};
			class StrGrassDry{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.2;
			};
			class StrGrassDryGroup{
				affectedByWind = 0;
				scaleMax = 0.5;
				scaleMin = 0.2;
			};
			class StrGrassDryMediumGroup{
				affectedByWind = 0;
			};
			class StrGrassGreenGroup{
				affectedByWind = 0;
			};
			class StrPlantGermaderGroup{
				affectedByWind = 0;
			};
			class StrPlantGreenShrub{
				affectedByWind = 0;
			};
			class StrPlantMullein{
				affectedByWind = 0;
			};
			class StrThistlePurpleSmall{
				affectedByWind = 0;
			};
			class StrThistleSmallYellow{
				affectedByWind = 0;
			};
			class StrThistleYellowShrub{
				affectedByWind = 0;
			};
			class StrThornGrayBig{
				affectedByWind = 0;
			};
			class StrThornGraySmall{
				affectedByWind = 0;
			};
			class StrThornGreenBig{
				affectedByWind = 0;
			};
			class StrThornGreenSmall{
				affectedByWind = 0;
			};
			class StrThornKhakiBig{
				affectedByWind = 0;
			};
			class StrThornKhakiSmall{
				affectedByWind = 0;
			};
			class StrWeedBrownTallGroup{
				affectedByWind = 0;
			};
			class ThistleHigh{
				affectedByWind = 0;
			};
			class ThistleHighDead{
				affectedByWind = 0;
			};
			class ThistleSmallYellow{
				affectedByWind = 0;
			};
			class ThistleThornBrown{
				affectedByWind = 0;
			};
			class ThistleThornBrownSmall{
				affectedByWind = 0;
			};
			class ThistleThornDesert{
				affectedByWind = 0;
			};
			class ThistleThornGray{
				affectedByWind = 0;
			};
			class ThistleThornGreen{
				affectedByWind = 0;
			};
			class ThistleThornGreenSmall{
				affectedByWind = 0;
			};
		};
	};
	class ruha: CAWorld {
			class Clutter
			{
				class ruha_sammal1_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_GrassCrookedGreen_summer.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
				class ruha_sammal2_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_blueBerry_summer.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal4_Clutter: DefaultClutter
				{
					model = "CA\plants2\clutter\c_picea.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal5_Clutter: DefaultClutter
				{
					model = "a3\plants_f\Clutter\c_Grass_Dry.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal6_Clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\forest\c_forest_roots.p3d";
					affectedByWind = 0;
					swLighting = 0;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal7_Clutter: DefaultClutter
				{
					model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_violet_leaves.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal9_Clutter: DefaultClutter
				{
					model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine.p3d";
					affectedByWind = 0;
					swLighting = 0;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal10_Clutter: DefaultClutter
				{
					model = "a3\plants_f\Clutter\c_StrGrassGreen_group.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal13_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_raspBerry_summer.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal14_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_caluna_summer.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal15_Clutter: DefaultClutter
				{
					model = "CA\plants2\Clutter\c_fern.p3d";
					affectedByWind = 0.2;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_kukka4_Clutter: DefaultClutter
				{
					model = "ca\plants\bolsevnik_group.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_pelto5_Clutter: DefaultClutter
				{
					model = "A3\Plants_F\Clutter\c_Grass_Green.p3d";
					affectedByWind = 0.4;
					swLighting = 1;
					scaleMin = 0.3;
					scaleMax = 0.9;
				};
				class ruha_pelto6_Clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\grass\c_Grass_Tropic.p3d";
					affectedByWind = 0.4;
					swLighting = 1;
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1.7 * KH_GRASS_CUT_COEF;
				};
				class ruha_pelto7_Clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\grass\c_GrassBunch_HI.p3d";
					affectedByWind = 0.4;
					swLighting = 1;
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				};
				class ruha_uusi_grass_long_Clutter1: DefaultClutter
				{
					model = "ca\plants\clutter_grass_long.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_vaalea_heina: DefaultClutter
				{
					model = "ca\plants2\clutter\c_stubble.p3d";
					affectedByWind = 0.1;
					swLighting = 1;
					scaleMin = 0.9 * KH_WHEAT_CUT_COEF;
					scaleMax = 1.1 * KH_WHEAT_CUT_COEF;
				};
				class ruha_WeedDead: DefaultClutter
				{
					model = "ca\plants2\clutter\c_WeedDead.p3d";
					affectedByWind = 0.3;
					swLighting = 1;
					scaleMin = 0.75 * KH_GRASS_CUT_COEF;
					scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				};
				class ruha_GrassDesert: DefaultClutter
				{
					model = "ca\plants_pmc\Clutter\c_GrassDesert_GroupSoft_PMC.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.8 * KH_GRASS_CUT_COEF;
				};
				class ruha_vaalea_ruohoa: DefaultClutter
				{
					model = "a3\plants_f\clutter\c_Grass_TuftDry.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.8 * KH_GRASS_CUT_COEF;
				};
				class ruha_FernAutumnTall_clutter: DefaultClutter
				{
					model = "ca\plants2\clutter\c_fernTall.p3d";
					affectedByWind = 0.15;
					scaleMin = 0.7;
					scaleMax = 1.2;
					swLighting = 1;
				};
				class ruha_RaspBerry_clutter: DefaultClutter
				{
					model = "ca\plants2\clutter\c_raspBerry.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.5;
					scaleMax = 1;
				};
				class ruha_suokukka1_Clutter: DefaultClutter
				{
					model = "CA\plants_e\clutter\c_papaver_06_ep1.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_suokukka2_Clutter: DefaultClutter
				{
					model = "CA\plants\bodlak_group.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_suokukka3_clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\volcano\c_volcano_grassmix.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 1;
					scaleMax = 1.5;
				};
				class ruha_suokukka4_clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\grass\c_grass_tropic.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 1;
					scaleMax = 1.5;
				};
				class ruha_suokukka5_clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\forest\c_forest_fern.p3d";
					affectedByWind = 1;
					swLighting = 0;
					scaleMin = 1;
					scaleMax = 1.5;
				};
			};
		};
};
