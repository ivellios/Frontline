#include "..\macros.hpp"

#define RHSBASECLASS rhsusf_m113tank_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)

TURRET_INHERIT
	class APC_Tracked_02_base_F : Tank_F {};
	class RHSBASECLASS:  APC_Tracked_02_base_F {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
                class Turrets {
                };
				magazines[] = {"rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhsusf_mag_L8A3_8", "rhsusf_mag_L8A3_8", "rhsusf_mag_L8A3_8", "rhsusf_mag_L8A3_8", "rhsusf_mag_L8A3_8", "rhsusf_mag_L8A3_8"};
                //gunnerAction = "rhs_SPG_Gunner";RHS_M113_Gunner_M2
                //gunnerInAction = "rhs_SPG_Gunner";RHS_M113_Gunner_M2
				stabilizedInAxes = 3;
                class ViewOptics : RCWSOptics {
                    initFov = 0.375;
                    minFov = 0.375;
                    maxFov = 0.375;
                    visionMode[] = {"Normal"};
                };
                class ViewGunner : ViewOptics {
                    initAngleX = -15;
                    minAngleX = -45;
                    maxAngleX = 45;
                    initFov = 0.75;
                    minFov = 0.375;
                    maxFov = 0.75;
                    visionMode[] = {};
                };
                class OpticsIn {
                    class ViewOptics : ViewGunner {
                        gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
                        gunnerOutOpticsModel = "\A3\weapons_f\reticle\optics_empty";
                    };
                };
                class CommanderOptics {
                };
            };
        };
	};
	class rhsusf_m113_usarmy : RHSBASECLASS {};
	class rhsusf_m113_usarmy_supply : rhsusf_m113_usarmy {
		transportAmmo = 50;
        supplyRadius = 0;
	};
	class rhsusf_m113_usarmy_unarmed : RHSBASECLASS {};
	class rhsusf_m113_usarmy_medical : rhsusf_m113_usarmy_unarmed {};
	class rhsusf_m113_usarmy_M240 : RHSBASECLASS {};
	class rhsusf_m113_usarmy_MK19 : RHSBASECLASS {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
                discreteDistance[] = {100, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500};
                discreteDistanceInitIndex = 2;
                gunnerLeftHandAnimName = "OtocHlaven";
                gunnerRightHandAnimName = "OtocHlaven";
                gunnerAction = "RHS_M113_Gunner_Mk19";
                gunnerInAction = "RHS_M113_Gunner_Mk19";
                weapons[] = {"RHS_MK19", "rhsusf_weap_M259"};
                magazines[] = {"RHS_48Rnd_40mm_MK19", "RHS_48Rnd_40mm_MK19", "rhsusf_mag_L8A3_8", "rhsusf_mag_L8A3_8", "rhsusf_mag_L8A3_8"};
            };
		};
	};
	class rhsusf_m113d_usarmy : RHSBASECLASS {};
	class rhsusf_m113d_usarmy_supply : rhsusf_m113_usarmy_supply {};
	class rhsusf_m113d_usarmy_unarmed : rhsusf_m113_usarmy_unarmed {};
	class rhsusf_m113d_usarmy_medical : rhsusf_m113_usarmy_medical {};
	class rhsusf_m113d_usarmy_M240 : rhsusf_m113_usarmy_M240 {};
	class rhsusf_m113d_usarmy_MK19 : rhsusf_m113_usarmy_MK19 {};
};