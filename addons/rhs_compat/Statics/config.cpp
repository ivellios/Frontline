#include "..\macros.hpp"
#define RHSBASECLASS statics

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};
/*class cfgAmmo {
	class Default;
	class ShellCore : Default {};
	class ShellBase : ShellCore {};
	class Sh_155mm_AMOS : ShellBase {};
	class Sh_82mm_AMOS : Sh_155mm_AMOS {
        SoundSetExplosion[] = {"Mortar_Exp_SoundSet", "Mortar_Tail_SoundSet", "Explosion_Debris_SoundSet"};
        hit = 165;
        indirectHit = 32;//indirectHit = 52;
        indirectHitRange = 18;
    };
	class rhs_ammo_3vo18 : Sh_82mm_AMOS {
        hit = 145;
        indirectHit = 32;//indirectHit = 42;
        indirectHitRange = 18;
    };
};*/
class CfgVehicleClasses {
    access = 1;
	class staticUSA {
        displayName = "Static Weapons";
    };
	class staticRU {
        displayName = "Static Weapons";
    };
	class staticINS {
        displayName = "Static Weapons";
    };
	class FRLFortifications {
        displayName = "Fortifications";
    };
};
class Mode_SemiAuto {};
class CannonCore {};
class cfgWeapons {
	class mortar_82mm: CannonCore
		{

			class Single1: Mode_SemiAuto
			{
				artilleryCharge = 0.35;
			};
			class Single2: Single1
			{
				artilleryCharge = 0.39;
			};
			class Single3: Single1
			{
				artilleryCharge = 0.42;
			};
		};
};
class cfgMagazines {
	class Default;
	class CA_Magazine : Default {};
	class VehicleMagazine : CA_Magazine {};
	class rhs_mag_100rnd_127x99_mag : VehicleMagazine {};
	class rhs_mag_100rnd_127x99_mag_Tracer_Red : rhs_mag_100rnd_127x99_mag {};
	class rhs_mag_200rnd_127x99_mag_Tracer_Red : rhs_mag_100rnd_127x99_mag_Tracer_Red {};
	class rhs_mag_150rnd_127x99_mag_Tracer_Red : rhs_mag_200rnd_127x99_mag_Tracer_Red {
		count = 150;
    };
};

TURRET_INHERIT
	class StaticWeapon : LandVehicle {
		class Turrets : Turrets {
			class MainTurret : NewTurret {};
		};
	};
	class StaticAAWeapon : StaticWeapon {
		class Turrets : Turrets {
			class MainTurret : MainTurret {};
		};
	};
	class StaticATWeapon : StaticWeapon {
		class Turrets : Turrets {
			class MainTurret : MainTurret {};
		};
	};
	class StaticGrenadeLauncher : StaticWeapon {
		class Turrets : Turrets {
			class MainTurret : MainTurret {};
		};
	};
	class StaticMGWeapon : StaticWeapon {
		class Turrets : Turrets {
			class MainTurret : MainTurret {};
		};
	};
	class AT_01_base_F : StaticMGWeapon {
		class Turrets : Turrets {
			class MainTurret : MainTurret {};
		};
	};
	class StaticMortar : StaticWeapon {
		class Turrets : Turrets {
			class MainTurret : MainTurret {};
		};
	};
	class rhs_m2staticmg_base : StaticMGWeapon {
		class Turrets : Turrets {
			class MainTurret : MainTurret {
				magazines[] = {
					"rhs_mag_200rnd_127x99_mag_Tracer_Red","rhs_mag_200rnd_127x99_mag_Tracer_Red",
					"rhs_mag_200rnd_127x99_mag_Tracer_Red","rhs_mag_200rnd_127x99_mag_Tracer_Red"
				};
			};
		};
		cost = 15000;
		scope = 2;
		vehicleClass = "staticUSA";
		class assembleInfo {
			primary = 0;
			base = "";
			assembleTo = "";
			dissasembleTo[] = {""};
			displayName = "";
		};
	};
	class rhs_nsv_tripod_base : StaticMGWeapon {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
                magazines[] = {"rhs_mag_127x108mm_100","rhs_mag_127x108mm_100"};
            };
        };
		cost = 15000;
		vehicleClass = "staticRU";
	};
	class RHS_KORD_Base : rhs_nsv_tripod_base {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
                magazines[] = {
					"rhs_mag_127x108mm_100","rhs_mag_127x108mm_100","rhs_mag_127x108mm_100",
					"rhs_mag_127x108mm_100","rhs_mag_127x108mm_100","rhs_mag_127x108mm_100",
					"rhs_mag_127x108mm_100","rhs_mag_127x108mm_100"
				};
            };
        };
		cost = 15000;
		scope = 2;
		vehicleClass = "staticRU";
		class assembleInfo {
            primary = 0;
            base = "";
            assembleTo = "";
            dissasembleTo[] = {""};
            displayName = "";
        };
	};
	class rhs_DSHKM_base : StaticMGWeapon {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
                magazines[] = {
					"rhs_mag_127x108mm_150","rhs_mag_127x108mm_150","rhs_mag_127x108mm_150",
					"rhs_mag_127x108mm_150","rhs_mag_127x108mm_150"
				};
            };
        };
		cost = 15000;
		scope = 2;
		vehicleClass = "staticRU";
		class assembleInfo {
            primary = 0;
            base = "";
            assembleTo = "";
            dissasembleTo[] = {""};
            displayName = "";
        };
	};
	class RHS_M252_Base : StaticMortar {
        class Turrets : Turrets {
            class MainTurret : MainTurret {
                weapons[] = {"mortar_82mm"};
                magazines[] = {"8Rnd_82mm_Mo_shells", "8Rnd_82mm_Mo_Flare_white", "8Rnd_82mm_Mo_Smoke_white", "8Rnd_82mm_Mo_Flare_white", "8Rnd_82mm_Mo_Smoke_white"};
            };
        };
        class assembleInfo {
            dissasembleTo[] = {"RHS_M252_Gun_Bag", "RHS_M252_Bipod_Bag"};
        };
		cost = 60000;
		vehicleClass = "staticUSA";
    };
	class rhs_2b14_82mm_Base : StaticMortar {
        class Turrets : Turrets {
            class MainTurret : MainTurret {
                magazines[] = {"rhs_mag_3vo18_10", "rhs_mag_3vs25m_10", "rhs_mag_d832du_10"};
            };
        };
        class assembleInfo {
            dissasembleTo[] = {"rhs_Podnos_Gun_Bag", "rhs_Podnos_Bipod_Bag"};
        };
		cost = 60000;
		vehicleClass = "staticRU";
    };
	class RHS_Stinger_AA_pod_Base : StaticAAWeapon {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
            };
        };
		cost = 60000;
		vehicleClass = "staticUSA";
    };
	class RHS_TOW_TriPod_base : StaticATWeapon {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
            };
        };
		cost = 50000;
		vehicleClass = "staticUSA";
    };
	class RHS_MK19_TriPod_base : StaticGrenadeLauncher {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
            };
        };
		cost = 30000;
		vehicleClass = "staticUSA";
    };
	class rhs_Metis_Base : AT_01_base_F {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
            };
        };
		cost = 40000;
		vehicleClass = "staticRU";
    };
	class rhs_Igla_AA_pod_Base : StaticAAWeapon {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
            };
        };
		cost = 60000;
		vehicleClass = "staticRU";
    };
	class RHS_AGS30_TriPod_base : StaticGrenadeLauncher {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
            };
        };
		cost = 30000;
		vehicleClass = "staticRU";
    };
	class rhs_SPG9_base : AT_01_base_F {
		class Turrets : Turrets {
            class MainTurret : MainTurret {
            };
        };
		cost = 40000;
		vehicleClass = "staticRU";
    };
	//fortifications
	class NonStrategic;
	class Static;
	class HouseBase : NonStrategic {};
	class Land_Obstacle_Bridge_F : NonStrategic {};
	class HBarrier_base_F : NonStrategic {};
	class Land_HBarrierBig_F : HBarrier_base_F {};
	class House : HouseBase {};
	class Building : Static {};
	class Strategic : Building {};
	class BagFence_base_F : Strategic {};
	class BagBunker_base_F : Strategic {};
	class Land_HBarrier_large : NonStrategic {};
	class House_F : House {};
	class House_Small_F : House_F {};
	class Cargo_Patrol_base_F : House_F {};
	class Fort_EnvelopeSmall : House {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_fort_rampart : House {
		cost = 2000;
		vehicleClass = "FRLFortifications";
	};
	class Fort_EnvelopeBig : House {
		cost = 700;
		vehicleClass = "FRLFortifications";
	};
	class Land_fort_bagfence_round : House {
		cost = 1500;
		vehicleClass = "FRLFortifications";
	};
	class Land_fort_bagfence_corner : House {
		cost = 1500;
		vehicleClass = "FRLFortifications";
	};
	class Land_BagFence_Short_F : BagFence_base_F {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_BagFence_Round_F : BagFence_base_F {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_BagFence_Corner_F : BagFence_base_F {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_BagFence_Long_F : BagFence_base_F {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_BagFence_End_F : BagFence_base_F {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_Razorwire_F : NonStrategic {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_Sign_MinesTall_F : House {
		cost = 100;
		vehicleClass = "FRLFortifications";
	};
	class Land_BagBunker_Small_F : BagBunker_base_F {
		cost = 2500;
		vehicleClass = "FRLFortifications";
	};
	class Land_BagBunker_Large_F : BagBunker_base_F {
		cost = 5000;
		vehicleClass = "FRLFortifications";
	};
	class Land_Cargo_Patrol_V2_F : Cargo_Patrol_base_F {
		cost = 7500;
		vehicleClass = "FRLFortifications";
	};
	class Land_HBarrier_Big_F : Land_HBarrierBig_F {
		cost = 3000;
		vehicleClass = "FRLFortifications";
	};
	class Land_CnCShelter_F : House_Small_F {
		cost = 1000;
		vehicleClass = "FRLFortifications";
	};
	class Land_Obstacle_Ramp_F : Land_Obstacle_Bridge_F {
		cost = 1000;
		vehicleClass = "FRLFortifications";
	};
	class Land_HBarrierWall_corner_F : HBarrier_base_F {
		cost = 4000;
		vehicleClass = "FRLFortifications";
	};
	class Land_HBarrierWall6_F : HBarrier_base_F {
		cost = 4000;
		vehicleClass = "FRLFortifications";
	};
	class Land_HBarrierWall4_F : HBarrier_base_F {
		cost = 3500;
		vehicleClass = "FRLFortifications";
	};
	class Land_HBarrierWall_corridor_F : HBarrier_base_F {
		cost = 4000;
		vehicleClass = "FRLFortifications";
	};
	class Land_BarGate2 : Land_HBarrier_large {
		cost = 3000;
		vehicleClass = "FRLFortifications";
	};
	class Land_Barricade_01_10m_F : Strategic {
		cost = 3000;
		vehicleClass = "FRLFortifications";
	};
	class Land_Barricade_01_4m_F : Strategic {
		cost = 2500;
		vehicleClass = "FRLFortifications";
	};
	class Hhedgehog_concreteBig : Strategic {
		cost = 1500;
		vehicleClass = "FRLFortifications";
	};
	class Land_CzechHedgehog_01_F : Strategic {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_SandbagBarricade_01_F : BagFence_base_F {
		cost = 1000;
		vehicleClass = "FRLFortifications";
	};
	class Land_SandbagBarricade_01_half_F : BagFence_base_F {
		cost = 500;
		vehicleClass = "FRLFortifications";
	};
	class Land_SandbagBarricade_01_hole_F : BagFence_base_F {
		cost = 1500;
		vehicleClass = "FRLFortifications";
	};
	class MineGeneric : Static {};
	class MineBase : MineGeneric {};
	class ATMine : MineBase {
		cost = 5000;
		vehicleClass = "FRLFortifications";
	};
	class SatchelCharge_F : MineBase {};
	class APERSMineDispenser_F : SatchelCharge_F {
		cost = 20000;
		vehicleClass = "FRLFortifications";
	};
	/*class ReammoBox_F;
	class Slingload_base_F : ReammoBox_F {};
	class Slingload_01_Base_F : Slingload_base_F {};
	class B_Slingload_01_Ammo_F : Slingload_01_Base_F {
		transportAmmo = 30000;
		cost = 40000;
	};vehicle re-arm bbgurl*/
};
