// -- Module defines -- //
#define MODULE Rhs_compat

// -- Global defines -- //
#include "..\main\macros_local.hpp"



#define RHSCONFIG(var1) TRIPLE(PREFIX,RHS,var1)
#define TURRET_INHERIT  \
class NewTurret;\
class ViewOptics;\
class Turrets;\
class MainTurret;\
class CommanderOptics;\
class ViewGunner;\
class ViewCargo;\
class RCWSOptics;\
class Optics_Armored {\
	class Wide: RCWSOptics { };\
	class Medium: Wide { };\
	class Narrow: Medium { };\
};\
class Optics_Gunner_APC_03: Optics_Armored {\
    class Wide: Wide { };\
    class Medium: Medium { };\
    class Narrow: Narrow { };\
};\
class FRL_Optics_Gunner_APC_01: Optics_Gunner_APC_03 {\
    class Wide: Wide {\
    	gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_w_F.p3d";\
    };\
    class Medium: Medium {\
    	gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_m_F.p3d";\
    };\
    class Narrow: Narrow {\
    	gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d";\
    };\
};\
\
class CfgVehicles {\
	class Land;\
	class LandVehicle: Land {\
		class CommanderOptics: NewTurret { };\
		class ViewOptics: ViewOptics { };\
		class ViewGunner: ViewCargo { };\
	};\
\
	class Tank: LandVehicle {\
		class ViewOptics: ViewOptics { };\
		class Turrets {\
			class MainTurret: NewTurret {\
				class Turrets {\
					class CommanderOptics: NewTurret {\
\
					};\
				};\
			};\
		};\
	};\
\
	class Tank_F: Tank {\
		class Turrets {\
			class MainTurret: NewTurret {\
				class Turrets {\
					class CommanderOptics: CommanderOptics {\
\
					};\
				};\
			};\
		}\
	};\
\
\
	class APC_Tracked_03_base_F: Tank_F {\
		class Turrets: Turrets {\
			class MainTurret: MainTurret {\
				class Turrets: Turrets {\
					class CommanderOptics: CommanderOptics {\
						class ViewOptics: ViewOptics { };\
					};\
				};\
				class OpticsIn: Optics_Gunner_APC_03 {\
					class Wide: Wide { };\
					class Medium: Medium { };\
					class Narrow: Narrow { };\
				};\
			};\
		};\
	};\
