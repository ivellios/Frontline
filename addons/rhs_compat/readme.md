#Contents

The addon is the main driving force behind our reconfigs for RHS. This addon is the only one that should have a requiredAddons entry on the RHS mods and should be completely modular. Each addon should be in a seperate config.cpp file so we can easily determine exactly which changes we are making and disable / add changes easily without needing to dive around in a massive file.

#Zoom Values
// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)