#include "..\macros.hpp"

class CfgPatches {
	class RHSCONFIG(ammocrates) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
	};
};

#define disposeable_lnch_cnt 2
#define disposeable_lnch_cnt 2
#define reuseable_lnch_cnt 1
#define explosive_cnt 3
#define backpack_cnt 3

class CfgVehicles {
    class All {};
    class Thing: All {};
    class ThingX: Thing {};
    class ReammoBox_F: ThingX {};
    class NATO_Box_Base: ReammoBox_F {};
    class Box_NATO_WpsLaunch_F: NATO_Box_Base {};
    class frl_rhs_AT_box: Box_NATO_WpsLaunch_F {};

    class frl_rhs_ATCrate_ins: frl_rhs_AT_box {
        class TransportMagazines {
            class _rpg_75_ammo {
                magazine = "rhs_rpg75_mag";
                count = 2;
            };
            class _IED_big {
                magazine = "IEDUrbanBig_Remote_Mag";
                count = 1;
            };
        };
        class TransportWeapons {
            class _rpg_75 {
                weapon = "rhs_weap_rpg75";
                count = 2;
            };
            class _rpg_26 {
                weapon = "rhs_weap_rpg26";
                count = 3;
            };
        };
        class TransportBackpacks {
            class _rpg_carrier {
                backpack = "rhs_rpg_empty";
                count = backpack_cnt;
            };
        };
        class TransportItems {
            class _expl_charge {
                name = "FRL_ExplosiveCharge_Wpn";
                count = explosive_cnt - 1;
            };
        };
    };

    class frl_rhs_ATCrate_AFRF: frl_rhs_AT_box {

        class TransportMagazines {
            class _rpg_7_VL {
                magazine = "rhs_rpg7_PG7VL_mag";
                count = reuseable_lnch_cnt * 2;
            };
            class _rpg_7_VR {
                magazine = "rhs_rpg7_PG7VR_mag";
                count = reuseable_lnch_cnt;
            };
        };

        class TransportWeapons {
            class _rpg_26 {
                weapon = "rhs_weap_rpg26";
                count = disposeable_lnch_cnt;
            };
            class _rpg_7 {
                weapon = "rhs_weap_rpg7";
                count = reuseable_lnch_cnt;
            };
        };

        class TransportBackpacks {
            class _rpg_carrier {
                backpack = "rhs_rpg_empty";
                count = backpack_cnt;
            };
        };

        class TransportItems {
            class _expl_charge {
                name = "FRL_ExplosiveCharge_Wpn";
                count = explosive_cnt;
            };
            class _rpg_7_scope {
                name = "rhs_acc_pgo7v2";
                count = reuseable_lnch_cnt;
            };
        };

    };

    class frl_rhs_ATCrate_IRAQ: frl_rhs_AT_box {

        class TransportMagazines {
            class _rpg_7_VL {
                magazine = "rhs_rpg7_PG7VL_mag";
                count = reuseable_lnch_cnt * 2;
            };
            class _rpg_7_VR {
                magazine = "rhs_rpg7_PG7VR_mag";
                count = reuseable_lnch_cnt;
            };
        };

        class TransportWeapons {
            class _M136 {
                weapon = "rhs_weap_M136";
                count = disposeable_lnch_cnt;
            };
            class _rpg_7 {
                weapon = "rhs_weap_rpg7";
                count = reuseable_lnch_cnt;
            };
        };

        class TransportBackpacks {
            class _rpg_carrier {
                backpack = "rhs_rpg_empty";
                count = backpack_cnt;
            };
        };

        class TransportItems {
            class _expl_charge {
                name = "FRL_ExplosiveCharge_Wpn";
                count = explosive_cnt;
            };
            class _rpg_7_scope {
                name = "rhs_acc_pgo7v2";
                count = reuseable_lnch_cnt;
            };
        };

    };

    class frl_rhs_ATCrate_USAF: frl_rhs_AT_box {

        class TransportMagazines {
            class _maaws_heat {
                magazine = "rhs_mag_maaws_HEAT";
                count = reuseable_lnch_cnt * 2;
            };
        };

        class TransportWeapons {
            class _M136_hp {
                weapon = "rhs_weap_M136_hp";
                count = disposeable_lnch_cnt;
            };
            class _maaws {
                weapon = "rhs_weap_maaws";
                count = reuseable_lnch_cnt;
            };
        };

        class TransportBackpacks {
            class _eagleBP {
                backpack = "rhsusf_assault_eagleaiii_coy";
                count = backpack_cnt;
            };
        };

        class TransportItems {
            class _expl_charge {
                name = "FRL_ExplosiveCharge_Wpn";
                count = explosive_cnt;
            };
            class _maaws_scope {
                name = "rhs_optic_maaws";
                count = reuseable_lnch_cnt;
            };
        };

    };

};
