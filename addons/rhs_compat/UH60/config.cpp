#include "..\macros.hpp"

#define RHSBASECLASS RHS_UH60M_US_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
class cfgMagazines{
	class Default;
	class CA_Magazine : Default {};
	class VehicleMagazine : CA_Magazine {};
	class 200Rnd_65x39_Belt : VehicleMagazine {};
	class 2000Rnd_65x39_Belt : 200Rnd_65x39_Belt {};
	class 5000Rnd_762x51_Belt : 2000Rnd_65x39_Belt {
        count = 1500;
        weight = 20;
    };
};
TURRET_INHERIT
	class AllVehicles;
	class Air: AllVehicles {};
	class Helicopter: Air {};
	class Helicopter_Base_F: Helicopter {};
	class Helicopter_Base_H: Helicopter_Base_F {};
	class Heli_Transport_01_base_F: Helicopter_Base_H {};
	class RHS_UH60_Base: Heli_Transport_01_base_F {};
	class RHS_UH60M_base: RHS_UH60_Base {};
	class RHS_UH60M_US_base: RHS_UH60M_base {
		/*liftForceCoef = 2.3;
		cyclicForwardForceCoef = 3.0;
		class RotorLibHelicopterProperties {
			RTDconfig = "A3\Air_F_Beta\Heli_Transport_01\RTD_Heli_Transport_01.xml";
            autoHoverCorrection[] = {3, 2.45, 0};
            defaultCollective = 0.625;
            retreatBladeStallWarningSpeed = 85.556;
            maxTorque = 1100;
            stressDamagePerSec = 0.00333333;
            maxHorizontalStabilizerLeftStress = 100000;//10000;
            maxHorizontalStabilizerRightStress = 100000;//10000;
            maxVerticalStabilizerStress = 100000;//10000;
            horizontalWingsAngleCollMin = 0;
            horizontalWingsAngleCollMax = 0;
            maxMainRotorStress = 2500000;//maxMainRotorStress = 200000;
            maxTailRotorStress = 250000;//maxTailRotorStress = 25000;   
        };*/
	};
	class RHS_UH60M: RHS_UH60M_US_base {
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				stabilizedInAxes = 3;
			};
			class RightDoorGun: MainTurret
			{
				stabilizedInAxes = 3;
			};
		};
	};
};

class cfgWeapons {
	class Default;
	class Mode_SemiAuto{
		multiplier = 1;
		burst = 1;
		dispersion = 0.0002;
		sound[] = {"", 10, 1};
		soundBegin[] = {"sound", 1};
		soundBeginWater[] = {"sound", 1};
		soundClosure[] = {"sound", 1};
		soundEnd[] = {};
		soundLoop[] = {};
		soundContinuous = 0;
		weaponSoundEffect = "";
		reloadTime = 0.1;
		ffCount = 1;
		ffMagnitude = 0.5;
		ffFrequency = 11;
		flash = "gunfire";
		flashSize = 0.1;
		autoFire = 0;
		useAction = 0;
		useActionTitle = "";
		showToPlayer = 1;
		minRange = 30;
		minRangeProbab = 0.25;
		midRange = 300;
		midRangeProbab = 0.58;
		maxRange = 600;
		maxRangeProbab = 0.04;
		artilleryDispersion = 1;
		artilleryCharge = 1;
		canShootInWater = 0;
		sounds[] = {"StandardSound", "SilencedSound"};
		displayName = "Semi";
		textureType = "semi";
		recoil = "recoil_single_primary_3outof10";
		recoilProne = "recoil_single_primary_prone_3outof10";
		aiDispersionCoefY = 1.7;
		aiDispersionCoefX = 1.4;
		soundBurst = 0;
		requiredOpticType = -1;
		aiRateOfFire = 2;
		aiRateOfFireDispersion = 1;
		aiRateOfFireDistance = 500;
	};
	class Mode_FullAuto : Mode_SemiAuto {
		dispersion = 0.0005;
		sound[] = {"", 10, 1};
		soundEnd[] = {"sound", 1};
		soundContinuous = 0;
		reloadTime = 0.08;
		autoFire = 1;
		minRange = 1;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.58;
		maxRange = 80;
		maxRangeProbab = 0.04;
		displayName = "Full";
		textureType = "fullAuto";
		recoil = "recoil_auto_primary_3outof10";
		recoilProne = "recoil_auto_primary_prone_3outof10";
		aiDispersionCoefY = 3;
		aiDispersionCoefX = 2;
		soundBurst = 0;
	};
	class MGunCore : Default {};
	class M134_minigun : MGunCore {
		class LowROF : Mode_FullAuto {
            displayName = "M134 Minigun 2000 RPM";
            showToPlayer = 1;
        };
        class HighROF : LowROF {
            displayName = "M134 Minigun 4000 RPM";
        };
	};
	class rhs_weap_m134_minigun_1 : M134_minigun {
        displayName = "M134 Minigun 7.62mm";
        class GunParticles {
            class FirstEffect {
                effectName = "MachineGun1";
                positionName = "muzzle_1";
                directionName = "chamber_1";
            };
            class effect1 {
                positionName = "machinegun_eject_pos";
                directionName = "machinegun_eject_dir";
                effectName = "MachineGunCartridge";
            };
            class RHSUSF_BarrelRefract {
                positionName = "muzzle_1";
                directionName = "muzzle_1";
                effectName = "RHSUSF_BarrelRefractHeavy";
            };
        };
        class LowROF : Mode_FullAuto {
            sounds[] = {"StandardSound"};
            class StandardSound {
                begin1[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_1", 2.5, 1, 2700};
                begin2[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_2", 2.5, 1, 2700};
                begin3[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_3", 2.5, 1, 2700};
                begin4[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_4", 2.5, 1, 2700};
                begin5[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_5", 2.5, 1, 2700};
                soundBegin[] = {"begin1", 0.2, "begin2", 0.2, "begin3", 0.2, "begin4", 0.2, "begin5", 0.2};
                closure1[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_2", 0.316228, 1, 20};
                closure2[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_3", 0.316228, 1, 20};
                soundClosure[] = {"closure1", 0.5, "closure2", 0.5};
            };
        };
        class HighROF : HighROF {
            sounds[] = {"StandardSound"};
            class StandardSound {
                begin1[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_1", 2.5, 1, 2700};
                begin2[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_2", 2.5, 1, 2700};
                begin3[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_3", 2.5, 1, 2700};
                begin4[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_4", 2.5, 1, 2700};
                begin5[] = {"rhsusf\addons\rhsusf_sounds\m134\m134_5", 2.5, 1, 2700};
                soundBegin[] = {"begin1", 0.2, "begin2", 0.2, "begin3", 0.2, "begin4", 0.2, "begin5", 0.2};
                closure1[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_2", 0.316228, 1, 20};
                closure2[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_3", 0.316228, 1, 20};
                soundClosure[] = {"closure1", 0.5, "closure2", 0.5};
            };
        };
    };
};