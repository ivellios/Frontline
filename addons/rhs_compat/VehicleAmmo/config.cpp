#include "..\macros.hpp"

#define RHSBASECLASS RHS_VehicleAmmo_reconfig

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};
class CfgAmmo {
    class Default;
    class ShellCore : Default {};
    class ShellBase : ShellCore {};
    class Sh_155mm_AMOS: ShellBase {
			explosionEffects = "MortarExplosion";
            //IEDMineBigExplosion
			effectsSmoke = "SmokeShellWhiteEffect";
			soundFly[] = {"pr\frl\addons\data_sound\data\artillery\Bm_37_SoundFly.wss", 1, 1.5, 700};
        };
        class Sh_82mm_AMOS: Sh_155mm_AMOS {
			//hit = 165;  //165
			explosionEffects = "MortarExplosion";
			indirectHit = 10;  //52
			indirectHitRange = 10;  //18
       };
       
    class ShotDeployCore : Default {};
    class ShotDeployBase : ShotDeployCore {};
	class Smoke_82mm_AMOS_White: ShotDeployBase {
			soundFly[] = {"pr\frl\addons\data_sound\data\artillery\Bm_37_SoundFly.wss", 1, 1.5, 700};
       };
       
    //class RocketBase;
    class Sh_120mm_HE: ShellBase {};
        class FRL_RocketSmall: Sh_120mm_HE{
			model = "\rhsafrf\addons\rhs_airweapons\s5_rockets\rhs_r_s5m1_fly";
			hit = 220; //1200
			indirectHit = 15; //800
			indirectHitRange = 15; //30
			effectFly = "ArtilleryTrails";
			//explosionEffects = "GrenadeExplosion";
			class CamShakeExplode
			{
				power = 12;
				duration = 1.6;
				frequency = 20;
				distance = 141.968;
			};
			class CamShakeHit
			{
				power = 60;
				duration = 0.6;
				frequency = 20;
				distance = 1;
			};
			soundFly[] = {"A3\Sounds_F\weapons\Rockets\rocket_fly_2", 1, 1.5, 700};
			SoundSetExplosion[] = {"RocketsMedium_Exp_SoundSet", "RocketsMedium_Tail_SoundSet", "Explosion_Debris_SoundSet"};
       };
    class R_230mm_fly: ShellBase {};
        class IRAM: R_230mm_fly{
			model = "\A3\Weapons_F_EPC\Ammo\Bomb_03_F.p3d";
			hit = 1200; //1200
			indirectHit = 125; //800
			indirectHitRange = 45; //30
			effectFly = "";
			CraterEffects = "BombCrater";
			explosionEffects = "IEDMineBigExplosion";
			effectsSmoke = "SmokeShellWhiteEffect";
			class CamShakeExplode
			{
				power = 70;
				duration = 2.4;
				frequency = 20;
				distance = 500;
			};
			class CamShakeHit
			{
				power = 155;
				duration = 0.8;
				frequency = 20;
				distance = 10;
			};
			soundFly[] = {"pr\frl\addons\data_sound\data\artillery\Bm_37_SoundFly.wss", 1, 1.5, 700};
			SoundSetExplosion[] = {"BombsHeavy_Exp_SoundSet", "BombsHeavy_Tail_SoundSet", "Explosion_Debris_SoundSet"};
      };
};


class cfgMagazines {
	class Default;
	class CA_Magazine: Default{};
	class VehicleMagazine: CA_Magazine{};
	class rhs_mag_pg15v_24: VehicleMagazine {
		class FRL_VehicleMaintenance {
			simulatedMag = 1;
			rearmTime = 4;
		};
	};
	class rhs_mag_og15v_16: VehicleMagazine {
		class FRL_VehicleMaintenance {
			simulatedMag = 1;
			rearmTime = 4;
		};
	};
};