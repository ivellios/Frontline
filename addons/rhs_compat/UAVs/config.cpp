#include "..\macros.hpp"
#define RHSBASECLASS UAV_01_base_F

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

TURRET_INHERIT
	class AllVehicles;
	class Air: AllVehicles {};
	class Helicopter: Air {};
	class Helicopter_Base_F: Helicopter {};
	class UAV_01_base_F: Helicopter_Base_F {
			class Turrets: Turrets
			{
				class MainTurret: MainTurret
				{
					class OpticsIn
					{
						class Wide
						{
							initFov = 0.5;
							minFov = 0.5;
							maxFov = 0.5;
							visionMode[] = {"Normal", "NVG"};
						};
						class Medium: Wide
						{
							initFov = 0.35;
							minFov = 0.35;
							maxFov = 0.35;
						};
						class Narrow: Wide
						{
							initFov = 0.2;
							minFov = 0.2;
							maxFov = 0.2;
						};
					};
			};
	};
};
};