#include "..\macros.hpp"

#define RHSBASECLASS O_Truck_02_fuel_F

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

TURRET_INHERIT
    class Car: LandVehicle {};
    class Car_F: Car {};
    class Truck_F: Car_F {};
    class Truck_02_base_F: Truck_F {};
    class Truck_02_fuel_base_F: Truck_02_base_F {};
    class C_Truck_02_fuel_F: Truck_02_fuel_base_F {};
    class FRL_VBIED: C_Truck_02_fuel_F {
        author = "Frontline";
        _generalMacro = "FRL_VBIED_Boom";
        side = 0;
        displayName = "VBIED";
        fuelExplosionPower = 50;
        weapons[] = {"VBIED_Detonator"};
        magazines[] = {"1Rnd_FRL_Big_Boom"};
    };
};

class CfgWeapons {
    class Default;
    class CannonCore : Default {};
    class Gatling_30mm_Plane_CAS_01_F : CannonCore {};
    class VBIED_Detonator : CannonCore {
		scope = 2;
        displayName = "Gatling Cannon 30mm";
        magazines[] = {"1Rnd_FRL_Big_Boom"};
    };  
};
class CfgMagazines {
    class Default;
	class CA_Magazine : Default {};
	class VehicleMagazine : CA_Magazine {};
    class 1Rnd_FRL_Big_Boom : VehicleMagazine {
        author = "Frontline";
        scope = 2;
        displayNameShort = "";
        ammo = "FRL_Big_Boom";
        count = 1;
        initSpeed = 10;
        maxLeadSpeed = 10;
        sound[] = {"", 1, 1};
        reloadSound[] = {"", 0.000316228, 1};
        nameSound = "cannon";
        tracersEvery = 1;
    };
};
class CfgAmmo {
    class Default;
    class BulletCore : Default {};
    class BulletBase : BulletCore {};
    class FRL_Big_Boom : BulletBase {
        SoundSetExplosion[] = {"Shell19mm25mm_Exp_SoundSet"};
        model = "\A3\Weapons_f\Data\bullettracer\tracer_red.p3d";
        cost = 20;
        hit = 8000;
        indirectHit = 2000;
        indirectHitRange = 40;
        caliber = 5;
        explosive = 1;
        airlock = 1;
        deflecting = 5;
        airFriction = -0.00036;
        typicalSpeed = 960;
        visibleFire = 32;
        audibleFire = 32;
        visibleFireTime = 3;
        fuseDistance = 0.1;
        dangerRadiusBulletClose = 20;
        dangerRadiusHit = 60;
        explosionSoundEffect = "DefaultExplosion";
        explosionEffects = "VBIEDExplosion";
        craterEffects = "VBIEDCrater";
        soundHit1[] = {"A3\Sounds_F\arsenal\explosives\Grenades\Explosion_gng_grenades_01", 3.16228, 1, 1300};
        soundHit2[] = {"A3\Sounds_F\arsenal\explosives\Grenades\Explosion_gng_grenades_02", 3.16228, 1, 1300};
        soundHit3[] = {"A3\Sounds_F\arsenal\explosives\Grenades\Explosion_gng_grenades_03", 3.16228, 1, 1300};
        soundHit4[] = {"A3\Sounds_F\arsenal\explosives\Grenades\Explosion_gng_grenades_04", 3.16228, 1, 1300};
        multiSoundHit[] = {"soundHit1", 0.25, "soundHit2", 0.25, "soundHit3", 0.25, "soundHit4", 0.25};
        /*class EventHandlers {
            class FRL_EventHandlers {
                fired = "_this call FRL_fnc_VBIED;";
            };
        };*/
        class CamShakeExplode {
            power = "(30*0.2)";
            duration = "((round (30^0.5))*0.2 max 0.2)";
            frequency = 20;
            distance = 90;
        };
        class CamShakeHit {
            power = 30;
            duration = "((round (30^0.25))*0.2 max 0.2)";
            frequency = 20;
            distance = 1;
        };
        class CamShakeFire {
            power = "(25^0.25)";
            duration = "((round (25^0.5))*0.2 max 0.2)";
            frequency = 20;
            distance = 90;
        };
        class CamShakePlayerFire {
            power = 0.01;
            duration = 0.1;
            frequency = 20;
            distance = 90;
        };
    };
};