#include "..\macros.hpp"

#define RHSBASECLASS rhs_bmp1tank_base
class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
    // author = AUTHOR_STR;
    // authors[] = AUTHOR_ARR;
    // authorUrl = AUTHOR_URL;
    version = VERSION;
    versionStr = QUOTE(VERSION);
    versionAr[] = {VERSION_AR};
    addonRootClass="FRL_Rhs_compat";
		requiredAddons[] = {
			"FRL_Main",
			"FRL_Rhs_compat"
		};
	};
};

class CfgMagazines
{
	class Default;
	class CA_Magazine: Default{};
	class VehicleMagazine: CA_Magazine{};
	class 450Rnd_127x108_Ball : VehicleMagazine {};
	class rhs_mag_3uor6_230 : 450Rnd_127x108_Ball {};
	class rhs_mag_3ubr6_230 : rhs_mag_3uor6_230 {};
	class rhs_mag_3ubr8_160 : rhs_mag_3ubr6_230 {
        displayName = "APDS 3UBR8";
        displayNameShort = "3UBR8";
        ammo = "rhs_ammo_3ubr8";
        count = 70;
        weight = "0.3*70";
    };

	  class rhs_mag_3uof8_340 : rhs_mag_3uor6_230 {
        displayName = "HE-T 3UOF8";
        displayNameShort = "3UOF8";
        ammo = "rhs_ammo_3uof8";
        count = 160;
        initSpeed = 960;
        weight = "0.389*160";
    };
	class rhs_mag_3uof8_305 : rhs_mag_3uof8_340 {
        count = 160;
        weight = "0.389*305";
    };
	
};


// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
TURRET_INHERIT
	class RHSBASECLASS: Tank_F {
		/*class Turrets: Turrets {
			class MainTurret: MainTurret {
				gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
				turretInfoType = "RscOptics_APC_Wheeled_03_gunner";
				discreteDistance[] = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000};
				discreteDistanceInitIndex = 4;
				stabilizedInAxes = "StabilizedInAxesBoth";
				class Turrets
				{
				};
				class OpticsIn
				{
					class pn22m1
					{
						minFov = 0.4;
						maxFov = 0.4;
						initFov = 0.4;
						visionMode[] = {"Normal", "Ti"};
						thermalMode[] = {4}; // {4}
						gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
					};
					class pn22m1n
					{
						gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_APC_03_n_F.p3d"; // "\rhsusf\addons\rhsusf_a2port_armor\M2A2_Bradley\gunnerOptics_M2A2_2";
						initfov = 0.2;
						minFov = 0.2;
						maxFov = 0.2;
					};
				};
			};
		};*/
	};
	class rhs_bmp_base: RHSBASECLASS{};
	class rhs_bmp1_vdv: rhs_bmp_base{};
	class rhs_bmp1_tv: rhs_bmp1_vdv{};
	class rhs_bmp1_msv: rhs_bmp1_vdv{};
	class rhs_bmp1_vmf: rhs_bmp1_vdv{};
	class rhs_bmp1_vv: rhs_bmp1_vdv{};
	class rhs_bmp1p_vdv: rhs_bmp1_vdv{};
	class rhs_bmp1p_tv: rhs_bmp1p_vdv{};
	class rhs_bmp1p_msv: rhs_bmp1p_vdv{};
	class rhs_bmp1p_vmf: rhs_bmp1p_vdv{};
	class rhs_bmp1p_vv: rhs_bmp1p_vdv{};
	class rhs_bmp1k_vdv: rhs_bmp1_vdv{};
	class rhs_bmp1k_tv: rhs_bmp1k_vdv{};
	class rhs_bmp1k_msv: rhs_bmp1k_vdv{};
	class rhs_bmp1k_vmf: rhs_bmp1k_vdv{};
	class rhs_bmp1k_vv: rhs_bmp1k_vdv{};
	class rhs_bmp1d_vdv: rhs_bmp1_vdv{};
	class rhs_bmp1d_tv: rhs_bmp1d_vdv{};
	class rhs_bmp1d_msv: rhs_bmp1d_vdv{};
	class rhs_bmp1d_vmf: rhs_bmp1d_vdv{};
	class rhs_bmp1d_vv: rhs_bmp1d_vdv{};
	class rhs_prp3_vdv: rhs_bmp1_vdv{};
	class rhs_prp3_tv: rhs_prp3_vdv{};
	class rhs_prp3_msv: rhs_prp3_vdv{};
	class rhs_prp3_vmf: rhs_prp3_vdv{};
	class rhs_prp3_vv: rhs_prp3_vdv{};
	class rhs_bmp2e_vdv: rhs_bmp1_vdv{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = {"rhs_weap_2a42","rhs_weap_pkt","rhs_weap_9m113","rhs_weap_902a"};
				magazines[] = {"rhs_mag_3uof8_340","rhs_mag_3ubr8_160","rhs_mag_9m113","rhs_mag_9m113","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6"};
				stabilizedInAxes = "StabilizedInAxesBoth";
				gunnerOutOpticsModel = "";
				maxhorizontalrotspeed = 0.61;
				maxverticalrotspeed = 0.104;
				turretInfoType = "RHS_RscWeaponBPK42_FCS";
				discreteDistance[] = {100};
				discreteDistanceInitIndex = 0;
				canUseScanners = 0;
				class OpticsIn
				{
					class bpk142
					{
						opticsDisplayName = "DAY";
						initAngleX = 0;
						minAngleX = -110;
						maxAngleX = "+110";
						initAngleY = 0;
						minAngleY = -110;
						maxAngleY = "+110";
						opticsZoomMin = 0.4;
						opticsZoomMax = 0.4;
						distanceZoomMin = 200;
						distanceZoomMax = 2000;
						initFov = "0.7/1";
						minFov = "0.7/1";
						maxFov = "0.7/1";
						visionMode[] = {"Normal"};
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_empty";
						gunnerOpticsEffect[] = {"TankGunnerOptics1","OpticsBlur2","OpticsCHAbera3"};
					};
					class 9sh119m1: bpk142
					{
						opticsDisplayName = "ATGM";
						initFov = "0.2";
						minFov = "0.2";
						maxFov = "0.2";
						visionMode[] = {"Normal"};
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_9sh119m1";
						gunnerOpticsEffect[] = {"TankGunnerOptics1","OpticsBlur2","OpticsCHAbera3"};
					};
					class bpk142n
					{
						opticsDisplayName = "NIGHT";
						initAngleX = 0;
						minAngleX = -110;
						maxAngleX = "+110";
						initAngleY = 0;
						minAngleY = -110;
						maxAngleY = "+110";
						opticsZoomMin = 0.14;
						opticsZoomMax = 0.14;
						distanceZoomMin = 200;
						distanceZoomMax = 2000;
						initFov = 0.4;
						minFov = 0.4;
						maxFov = 0.4;
						visionMode[] = {"NVG"};
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_bpk142n";
						gunnerOpticsEffect[] = {"TankGunnerOptics1","OpticsBlur2","OpticsCHAbera3"};
					};
				};
				class Turrets: Turrets
				{
					class CommanderOptics: CommanderOptics
					{
						dontCreateAI = 1;
						gunnerDoor = "hatchC";
						gunnerName = "Commander";
						body = "RHS_BMP1_com_coppula_BMP2";
						gun = "RHS_BMP1_OU3_BMP2";
						gunnerOutForceOptics = 0;
						gunnerForceoptics = 1;
						weapons[] = {};
						magazines[] = {};
						animationSourceHatch = "HatchCommander_BMP2";
						animationSourceBody = "obsturret_BMP2";
						animationSourceGun = "obsGun_BMP2";
						memoryPointGunnerOptics = "commanderview";
						memoryPointGunnerOutOptics = "commander_out_view";
						nightVision = 1;
						proxyType = "CPCommander";
						proxyIndex = 1;
						lockWhenDriverOut = 0;
						primaryGunner = 0;
						primaryObserver = 0;
						hasCommander = 1;
						viewGunnerInExternal = 1;
						gunBeg = "Mgun_end";
						gunEnd = "Mgun_start";
						gunnerAction = "rhs_bmp2_commander";
						gunnerInAction = "rhs_bmp2_commanderIn";
						stabilizedInAxes = "StabilizedInAxesBoth";
						selectionFireAnim = "";
						minElev = -4;
						maxElev = 12;
						initElev = 0;
						minTurn = -135;
						maxTurn = 135;
						initTurn = 0;
						gunnerOutOpticsModel = "";
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_tpku2b.p3d";
						soundServo[] = {};
						outGunnerMayFire = 0;
						inGunnerMayFire = 1;
						startEngine = 0;
						canUseScanners = 0;
					};
				};
				class TurnIn
				{
				};
			};
		};
	};
	class rhs_bmp2e_tv: rhs_bmp2e_vdv{};
	class rhs_bmp2e_msv: rhs_bmp2e_vdv{};
	class rhs_bmp2e_vmf: rhs_bmp2e_vdv{};
	class rhs_bmp2e_vv: rhs_bmp2e_vdv{};
	class rhs_bmp2_vdv: rhs_bmp2e_vdv{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				magazines[] = {"rhs_mag_3uof8_340","rhs_mag_3ubr8_160","rhs_mag_9m113","rhs_mag_9m113","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6"};
			};
		};
	};
	class rhs_bmp2_tv: rhs_bmp2_vdv{};
	class rhs_bmp2_msv: rhs_bmp2_vdv{};
	class rhs_bmp2_vmf: rhs_bmp2_vdv{};
	class rhs_bmp2_vv: rhs_bmp2_vdv{};
	class rhs_bmp2k_vdv: rhs_bmp2_vdv{};
	class rhs_bmp2k_tv: rhs_bmp2k_vdv{};
	class rhs_bmp2k_msv: rhs_bmp2k_vdv{};
	class rhs_bmp2k_vmf: rhs_bmp2k_vdv{};
	class rhs_bmp2k_vv: rhs_bmp2k_vdv{};
	class rhs_bmp2d_vdv: rhs_bmp2_vdv{};
	class rhs_bmp2d_tv: rhs_bmp2d_vdv{};
	class rhs_bmp2d_msv: rhs_bmp2d_vdv{};
	class rhs_bmp2d_vmf: rhs_bmp2d_vdv{};
	class rhs_bmp2d_vv: rhs_bmp2d_vdv{};
	class rhs_Ob_681_2: rhs_bmp2e_msv{};//wut?
	class rhs_brm1k_base: rhs_bmp2e_vdv{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				stabilizedInAxes = "StabilizedInAxesBoth";
				weapons[] = {"rhs_weap_2a28","rhs_weap_pkt","rhs_weap_902a"};
				magazines[] = {"rhs_mag_pg15v_20","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_3d17_6"};
				class OpticsIn
				{
					class pn22m1
					{
						initAngleX = 0;
						minAngleX = -110;
						maxAngleX = "+110";
						initAngleY = 0;
						minAngleY = -110;
						maxAngleY = "+110";
						opticsZoomMin = 0.14;
						opticsZoomMax = 0.14;
						distanceZoomMin = 200;
						distanceZoomMax = 2000;
						initFov = 0.14;
						minFov = 0.14;
						maxFov = 0.14;
						visionMode[] = {"Normal"};
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_1pn22m2";
						gunnerOpticsEffect[] = {"TankGunnerOptics1","OpticsBlur2","OpticsCHAbera3"};
					};
					class pn22m1n
					{
						initAngleX = 0;
						minAngleX = -110;
						maxAngleX = "+110";
						initAngleY = 0;
						minAngleY = -110;
						maxAngleY = "+110";
						opticsZoomMin = 0.14;
						opticsZoomMax = 0.14;
						distanceZoomMin = 200;
						distanceZoomMax = 2000;
						initFov = 0.14;
						minFov = 0.14;
						maxFov = 0.14;
						visionMode[] = {"NVG"};
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_1pn22m1n";
						gunnerOpticsEffect[] = {"TankGunnerOptics1","OpticsBlur2","OpticsCHAbera3"};
					};
				};
				class Reflectors
				{
				};
			};
		};
	};
	class rhs_brm1k_vdv: rhs_brm1k_base{};
	class rhs_brm1k_tv: rhs_brm1k_vdv{};
	class rhs_brm1k_msv: rhs_brm1k_vdv{};
	class rhs_brm1k_vmf: rhs_brm1k_vdv{};
	class rhs_brm1k_vv: rhs_brm1k_vdv{};
	class rhs_bmp3tank_base: Tank_F{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class Turrets: Turrets
				{
				};
				weapons[] = {"rhs_weap_2a70","rhs_weap_2a72","rhs_weap_pkt_bmd_coax","rhs_weap_902a"};
				magazines[] = {"rhs_mag_3UOF17_22","rhs_mag_9m117_8","rhs_mag_3uof8_305","rhs_mag_3ubr8_195","rhs_mag_9m113_4","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6","rhs_mag_3d17_6"};
				class OpticsIn
				{
					class DayMain: ViewOptics
					{
						initAngleX = 0;
						minAngleX = -30;
						maxAngleX = "+30";
						initAngleY = 0;
						minAngleY = -100;
						maxAngleY = "+100";
						initFov = "0.7/1";
						minFov = "0.7/1";
						maxFov = "0.7/1";
						visionMode[] = {"Normal"};
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_1k13_3s_1x.p3d";
						gunnerOpticsEffect[] = {};
					};
					class Day2: DayMain
					{
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_1k13_3s_5x.p3d";
						initFov = "0.7/2";
						minFov = "0.7/2";
						maxFov = "0.7/2";
					};
					class Day3: DayMain
					{
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_1k13_3s_14x.p3d";
						initFov = "0.7/4";
						minFov = "0.7/4";
						maxFov = "0.7/4";
					};
					class night: DayMain
					{
						initFov = "0.7/4";
						minFov = "0.7/4";
						maxFov = "0.7/4";
						visionMode[] = {"NVG"};
						gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_1k13_3s_5x_nvg.p3d";
					};
				};
			};
			class GPMGTurret1: NewTurret
			{
				gunnerDoor = "hatchRA";
				animationSourceHatch = "hatchR";
				proxyIndex = 8;
				body = "obsTurret2";
				gun = "obsGun2";
				animationSourceBody = "obsTurret2";
				animationSourceGun = "obsGun2";
				selectionFireAnim = "zasleh3";
				gunnerName = "Right Hull Gunner";
				hasGunner = 1;
				gunnerType = "rhs_msv_rifleman";
				isPersonTurret = 1;
				showAsCargo = 1;
				proxyType = "CPCargo";
				personTurretAction = "vehicle_turnout_1";
				minOutElev = -10;
				maxOutElev = 15;
				initOutElev = 0;
				minOutTurn = -100;
				maxOutTurn = 120;
				initOutTurn = 0;
				dontCreateAI = 1;
				forceHideGunner = 0;
				primaryObserver = 0;
				primaryGunner = 0;
				commanding = 0;
				minElev = -30;
				maxElev = 30;
				minTurn = -30;
				maxTurn = 30;
				weapons[] = {"rhs_weap_pkt_bmd_bow1"};
				magazines[] = {"rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250","rhs_mag_762x54mm_250"};
				ejectDeadGunner = 0;
				gunBeg = "muzzle2";
				gunEnd = "end2";
				memoryPointGun = "memoryPointGun2";
				memoryPointGunnerOptics = "gunnerview3";
				memoryPointsGetInGunner = "pos cargo R";
				memoryPointsGetInGunnerDir = "pos cargo R dir";
				usePreciseGetInAction = 0;
				preciseGetInOut = 0;
				gunnerAction = "rhs_bmp3_gunner03";
				gunnerInAction = "rhs_bmp3_gunner03";
				gunnerGetInAction = "GetInHigh";
				gunnerGetOutAction = "GetOutHigh";
				viewGunnerInExternal = 1;
				startEngine = 0;
				class Turrets
				{
				};
				gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_pp61am";//gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_tnpp220a_right";
				gunnerOpticsColor[] = {1,1,1,1};
				gunnerForceOptics = 1;
				gunnerOpticsEffect[] = {"TankGunnerOptics1","WeaponsOptics","OpticsCHAbera3"};
				class ViewOptics
				{
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					opticsZoomMin = 0.166666;
					opticsZoomMax = 0.166666;
					distanceZoomMin = 200;
					distanceZoomMax = 2000;
					initFov = 0.166666;
					minFov = 0.166666;
					maxFov = 0.166666;
				};
			};
			class GPMGTurret2: GPMGTurret1
			{
				gunnerOpticsModel = "\rhsafrf\addons\rhs_optics\vehicles\rhs_pp61am";
			};
		};
	};
	
	
	
};

class CfgRemoteExec{
	class Functions {
	        class FRL_fnc_effectFiredSmokeGenerator {
            allowedTargets = 0;
        };
	};
};
class cfgFunctions {
	class FUNCTIONPREFIX {
		class WeaponsEH {
			class effectFiredSmokeGenerator {
				file = "\pr\frl\addons\rhs_compat\BMP\fn_effectFiredSmokeGenerator.sqf";
				description = "Effects for smoke generator";
			};
		};
	};
};

class cfgAmmo {
	class Default;
	class BulletCore : Default {};
	class BulletBase : BulletCore {};
	class SmokeLauncherAmmo : BulletBase {};
	class rhs_ammo_smokegen : SmokeLauncherAmmo {
        muzzleEffect = "FRL_fnc_effectFiredSmokeGenerator";
        rhs_muzzleEffect = "";
        weaponLockSystem = "2";
        simulation = "shotCM";
        AIAmmoUsageFlags = "4";
        hit = 0;
        indirectHit = 0;
        indirectHitRange = 0;
    };
};
