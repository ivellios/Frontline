class CfgPatches {
    class FRL_NoZoom_ifa {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.70;
        requiredAddons[] = {
            "FRL_RHS_Compat",
            "FRL_RHS_Compat_RHS_Weapon_reconfig",
            "FRL_NoZoom"
        };
    };
};

#define __ZOOMMIN 0.75
#define __ZOOMMAX 0.375
#define __ZOOMINIT 0.75

class RCWSOptics {};
class CfgWeapons {
    class Default {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class PistolCore: Default {
    };
    class RifleCore: Default {
    };
    class LauncherCore: Default {
    };
    class Launcher: LauncherCore {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class Launcher_Base_F: Launcher {
    };
    class Rifle: RifleCore {
    };
    class Rifle_Base_F: Rifle {
    };
    class Rifle_Short_Base_F: Rifle_Base_F {
    };
    class Rifle_Long_Base_F: Rifle_Base_F {
    };
    class Pistol: PistolCore {
    };
    class Pistol_Base_F: Pistol {
    };
    class ItemCore: Default {
    };
    class InventoryItem_Base_F {
    };
    class InventoryOpticsItem_Base_F: InventoryItem_Base_F {
    };
    class launch_Titan_base: Launcher_Base_F {
    };
    class launch_O_Titan_F: launch_Titan_base {
    };
    class GM6_base_F: Rifle_Long_Base_F {
    };
    class LMG_Mk200_F: Rifle_Long_Base_F {
    };
    class arifle_MX_Base_F: Rifle_Base_F {
    };
    class SMG_01_Base: Rifle_Short_Base_F {
    };
    class SMG_01_F: SMG_01_Base {
    };
    class SMG_02_base_F: Rifle_Short_Base_F {
    };
    class rhs_weap_ak74m_Base_F: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_orsis_Base_F: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_pkp_base: Rifle_Long_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_strela: launch_O_Titan_F {
        class OpticsModes {
            class StepScope {
                opticsZoomMin = __ZOOMMAX;
                opticsZoomMax = __ZOOMMIN;
                opticsZoomInit = __ZOOMINIT;

            };
        };
    };
    class rhs_acc_sniper_base: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_1p29_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_1p29: rhs_acc_sniper_base {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_1p29_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_1p78: rhs_acc_1p29 {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_above_sight {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_pkas: rhs_acc_sniper_base {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
                class rhs_1p29_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_1p63: rhs_acc_pkas {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_pso1m2: rhs_acc_sniper_base {
    };
    class rhs_acc_pso1m2_pkp: rhs_acc_pso1m2 {
        class ItemInfo: ItemInfo {
            class OpticsModes: OpticsModes {
                class rhs_1p29_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_pso1m21: rhs_acc_sniper_base {
        class ItemInfo: ItemInfo {
            class OpticsModes: OpticsModes {
            };
        };
    };
    class rhs_acc_pso1m21_pkp: rhs_acc_pso1m21 {
        class ItemInfo: ItemInfo {
            class OpticsModes: OpticsModes {
                class rhs_1p29_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_nita: rhs_acc_pkas {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_nita_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_pgo7v: rhs_acc_sniper_base {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_pgo7v_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_pgo7v_ak: rhs_acc_pgo7v {
        class ItemInfo: ItemInfo {
            class OpticsModes: OpticsModes {
                class rhs_pgo7v_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_1pn93_base: rhs_acc_sniper_base {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_1p29_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_1pn34: rhs_acc_1pn93_base {
        class ItemInfo: ItemInfo {
            class OpticsModes {
                class rhs_1pn34_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_acc_rakursPM: rhs_acc_1p63 {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_rakurs_collimator {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_weap_kar98k_Base_F: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_m38_Base_F: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_XM2010_Base_F: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_m70_base: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_savz61: SMG_01_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_savz58_base: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_stgw57_base: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_acc_scope_base: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class rhs_1p29_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhs_weap_M320_Base_F: Pistol_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_m4_Base: arifle_MX_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_M249_base: LMG_Mk200_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_saw_base: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_lmg_minimipara: rhs_weap_saw_base {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_M107_Base_F: GM6_base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_m32_Base_F: Rifle_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhsusf_weap_MP7A1_base_f: SMG_02_base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_smaw: Launcher_Base_F {
        class rhs_weap_smaw_SR: Launcher_Base_F {
            opticsZoomMin = __ZOOMMAX;
            opticsZoomMax = __ZOOMMIN;
            opticsZoomInit = __ZOOMINIT;

        };
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_maaws: Launcher_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_M136: Launcher_Base_F {
        class OpticsModes {
            class ironsight {
                opticsZoomMin = __ZOOMMAX;
                opticsZoomMax = __ZOOMMIN;
                opticsZoomInit = __ZOOMINIT;

            };
        };
    };
    class rhs_weap_m72a7: rhs_weap_M136 {
        class OpticsModes {
            class ironsight {
                opticsZoomMin = __ZOOMMAX;
                opticsZoomMax = __ZOOMMIN;
                opticsZoomInit = __ZOOMINIT;

            };
        };
    };
    class rhs_weap_fim92: launch_O_Titan_F {
        class OpticsModes {
            class StepScope {
                opticsZoomMin = __ZOOMMAX;
                opticsZoomMax = __ZOOMMIN;
                opticsZoomInit = __ZOOMINIT;

            };
        };
    };
    class rhsusf_acc_sniper_base: ItemCore {
    };
    class rhsusf_acc_compm4: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ACO {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhsusf_acc_anpvs27: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class PVS27 {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhsusf_acc_ELCAN: rhsusf_acc_sniper_base {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class alternative_view {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhsusf_acc_ELCAN_pip: rhsusf_acc_ELCAN {
        class ItemInfo: ItemInfo {
            class OpticsModes: OpticsModes {
                class elcan_scope {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhsusf_acc_SpecterDR: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class Elcan_iron {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhsusf_acc_ACOG: rhsusf_acc_sniper_base {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class alternative_view {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhsusf_acc_ACOG_pip: rhsusf_acc_ACOG {
        class ItemInfo: ItemInfo {
            class OpticsModes: OpticsModes {
                class elcan_scope {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class rhsusf_acc_ACOG_MDO: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class RMR {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class optic_ERCO_blk_F: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class ARCO2collimator {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
    class launch_RPG7_F: Launcher_Base_F {
        class OpticsModes {
            class irons {
                opticsZoomMin = __ZOOMMAX;
                opticsZoomMax = __ZOOMMIN;
                opticsZoomInit = __ZOOMINIT;

            };
        };
    };
    class rhs_weap_scorpion: SMG_01_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class rhs_weap_m84: Rifle_Long_Base_F {
        opticsZoomMin = __ZOOMMAX;
        opticsZoomMax = __ZOOMMIN;
        opticsZoomInit = __ZOOMINIT;

    };
    class CUP_optic_ACOG: ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            class OpticsModes {
                class Kolimator {
                    opticsZoomMin = __ZOOMMAX;
                    opticsZoomMax = __ZOOMMIN;
                    opticsZoomInit = __ZOOMINIT;

                };
            };
        };
    };
};
class CfgVehicles {
    class All {
    };
    class AllVehicles: All {
        class NewTurret {
            class ViewOptics {
            };
        };
        class ViewPilot {
        };
        class ViewOptics {
        };
    };
    class Land: AllVehicles {
    };
    class LandVehicle: Land {
        class CommanderOptics: NewTurret {
        };
    };
    class Car: LandVehicle {
        class ViewPilot: ViewPilot {
        };
    };
    class Tank: LandVehicle {
        class ViewPilot: ViewPilot {
        };
    };
    class Air: AllVehicles {
    };
    class Helicopter: Air {
        class ViewPilot: ViewPilot {
        };
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
        class ViewOptics: ViewOptics {
        };
    };
    class Ship: AllVehicles {
        class ViewOptics;
    };
    class StaticWeapon: LandVehicle {
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
        class MainTurret;
    };
    class StaticMGWeapon: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class StaticATWeapon: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class StaticCannon: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class StaticGrenadeLauncher: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class StaticMortar: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class Tank_F: Tank {
        class Turrets {
            class MainTurret: NewTurret {
                class Turrets {
                    class CommanderOptics: CommanderOptics {
                    };
                };
            };
        };
    };
    class Car_F: Car {
        class ViewPilot: ViewPilot {
        };
        class NewTurret: NewTurret {
        };
        class Turrets {
            class MainTurret: NewTurret {
                class ViewOptics: ViewOptics {
                };
            };
        };
    };
    class Ship_F: Ship {
    };
    class Truck_F: Car_F {
        class Turrets: Turrets {
        };
    };
    class Helicopter_Base_F: Helicopter {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class Helicopter_Base_H: Helicopter_Base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
            class CopilotTurret: MainTurret {
            };
        };
    };
    class Heli_Light_02_base_F: Helicopter_Base_H {
        class Turrets: Turrets {
        };
    };
    class Heli_Attack_02_base_F: Helicopter_Base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class Heli_Transport_01_base_F: Helicopter_Base_H {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class Heli_Transport_02_base_F: Helicopter_Base_H {
        class Turrets: Turrets {
        };
    };
    class Wheeled_APC_F: Car_F {
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
    };
    class APC_Tracked_02_base_F: Tank_F {
    };
    class MRAP_01_base_F: Car_F {
    };
    class Offroad_01_base_F: Car_F {
    };
    class Truck_01_base_F: Truck_F {
    };
    class AT_01_base_F: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class Heli_light_03_base_F: Helicopter_Base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class UAV_01_base_F: Helicopter_Base_F {
        class PilotCamera {
            class OpticsIn {
                class Wide {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_UAZ_Base: Offroad_01_base_F {
        class Turrets: Turrets {
        };
    };
    class RHS_UAZ_DShKM_Base: RHS_UAZ_Base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class RHS_UAZ_SPG9_Base: RHS_UAZ_DShKM_Base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_Ural_BaseTurret: Truck_F {
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
    };
    class RHS_Ural_Zu23_Base: RHS_Ural_BaseTurret {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_ZU23_base: StaticCannon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_nsv_tripod_base: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class RHS_KORD_Base: rhs_nsv_tripod_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_DSHKM_base: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_AGS30_TriPod_base: StaticGrenadeLauncher {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_SPG9_base: AT_01_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_Metis_Base: AT_01_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_Kornet_Base: AT_01_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsgref_BRDM2: Wheeled_APC_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: RCWSOptics {
                };
                class ViewGunner: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsgref_BRDM2UM: rhsgref_BRDM2 {
        class Turrets: Turrets {
        };
    };
    class rhsgref_BRDM2_HQ: rhsgref_BRDM2UM {
        class Turrets: Turrets {
            class MainTurret: NewTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_Mi24_base: Heli_Attack_02_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class RHS_Mi24V_Base: RHS_Mi24_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class RHS_Mi24V_VVS_Base: RHS_Mi24V_Base {
    };
    class RHS_Mi8_base: Heli_Light_02_base_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsgref_cdf_Mi35: RHS_Mi24V_VVS_Base {
    };
    class rhsgref_mi24g_base: rhsgref_cdf_Mi35 {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class OpticsOut {
                    class Monocular {
                        minFov = __ZOOMMAX;
                        maxFov = __ZOOMMIN;

                    };
                };
            };
        };
    };
    class rhs_m70c_60mm_base: StaticMortar {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_CH_47F_base: Heli_Transport_02_base_F {
    };
    class RHS_CH_47F: RHS_CH_47F_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_UH60_Base: Heli_Transport_01_base_F {
    };
    class RHS_UH60M_base: RHS_UH60_Base {
    };
    class RHS_UH60M_US_base: RHS_UH60M_base {
    };
    class RHS_UH60M: RHS_UH60M_US_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_UH1_Base: Heli_light_03_base_F {
    };
    class RHS_UH1Y_base: RHS_UH1_Base {
    };
    class RHS_UH1Y_US_base: RHS_UH1Y_base {
    };
    class RHS_UH1Y: RHS_UH1Y_US_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_caiman_base: Truck_01_base_F {
        class Turrets {
        };
    };
    class rhsusf_caiman_GPK_base: rhsusf_caiman_base {
    };
    class rhsusf_M1220_M2_usarmy_d: rhsusf_caiman_GPK_base {
        class Turrets: Turrets {
            class M2_Turret: NewTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_M1220_M153_M2_usarmy_d: rhsusf_M1220_M2_usarmy_d {
        class Turrets: Turrets {
            class M2_CROWS_Turret: NewTurret {
                class ViewOptics: RCWSOptics {
                };
                class ViewGunner: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_M1230_M2_usarmy_d: rhsusf_caiman_GPK_base {
        class Turrets: Turrets {
            class M2_Turret: NewTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_fmtv_base: Truck_01_base_F {
        class Turrets {
        };
    };
    class rhsusf_M1078A1P2_fmtv_usarmy: rhsusf_fmtv_base {
    };
    class rhsusf_M1078A1P2_B_fmtv_usarmy: rhsusf_M1078A1P2_fmtv_usarmy {
    };
    class rhsusf_M1078A1P2_B_M2_fmtv_usarmy: rhsusf_M1078A1P2_B_fmtv_usarmy {
        class Turrets: Turrets {
            class M2_Turret: NewTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_M1083A1P2_fmtv_usarmy: rhsusf_M1078A1P2_fmtv_usarmy {
    };
    class rhsusf_M1083A1P2_B_fmtv_usarmy: rhsusf_M1083A1P2_fmtv_usarmy {
    };
    class rhsusf_M1083A1P2_B_M2_fmtv_usarmy: rhsusf_M1083A1P2_B_fmtv_usarmy {
        class Turrets: Turrets {
            class M2_Turret: NewTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_M1084A1P2_B_M2_fmtv_usarmy: rhsusf_M1083A1P2_B_M2_fmtv_usarmy {
        class Turrets: Turrets {
        };
    };
    class rhsusf_M1078A1R_SOV_M2_D_fmtv_socom: rhsusf_M1078A1P2_B_M2_fmtv_usarmy {
        class Turrets: Turrets {
            class M2_Turret: NewTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_M1084A1R_SOV_M2_D_fmtv_socom: rhsusf_M1084A1P2_B_M2_fmtv_usarmy {
        class Turrets: Turrets {
            class M2_Turret: NewTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_HEMTT_A4_base: Truck_01_base_F {
        class Turrets {
        };
    };
    class rhsusf_M977A4_usarmy_wd: rhsusf_HEMTT_A4_base {
    };
    class rhsusf_M977A4_BKIT_M2_usarmy_wd: rhsusf_M977A4_usarmy_wd {
        class Turrets: Turrets {
            class M2_Turret: NewTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_hmmwe_base: MRAP_01_base_F {
    };
    class rhsusf_m998_w_2dr: rhsusf_hmmwe_base {
        class Turrets: Turrets {
        };
    };
    class rhsusf_m998_w_4dr: rhsusf_m998_w_2dr {
        class Turrets: Turrets {
        };
    };
    class rhsusf_m998_w_4dr_halftop: rhsusf_m998_w_4dr {
        class Turrets: Turrets {
        };
    };
    class rhsusf_m998_w_4dr_fulltop: rhsusf_m998_w_4dr_halftop {
        class Turrets: Turrets {
        };
    };
    class rhsusf_m1025_w: rhsusf_m998_w_4dr_fulltop {
        class Turrets: Turrets {
        };
    };
    class rhsusf_m1025_w_m2: rhsusf_m1025_w {
        class Turrets: Turrets {
            class M2_Turret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_m113tank_base: APC_Tracked_02_base_F {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class RHS_Ship: Ship_F {
        class Turrets {
            class MainTurret;
        };
    };
    class rhsusf_mkvsoc: RHS_Ship {
        class Turrets: Turrets {
            class MainTurret: NewTurret {
                class m2_p_ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
                class m2_p_ViewGunner: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
            class m134_p_gunTurret: MainTurret {
                class m134_p_ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
                class m134_p_ViewGunner: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
            class mk19_s_gunTurret: MainTurret {
                class mk19_s_ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
                class mk19_s_ViewGunner: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_rg33_base: MRAP_01_base_F {
        class Turrets: Turrets {
        };
    };
    class rhsusf_rg33_m2_d: rhsusf_rg33_base {
        class Turrets: Turrets {
            class M2_Turret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_RG33L_base: MRAP_01_base_F {
        class Turrets: Turrets {
        };
    };
    class rhsusf_RG33L_GPK_base: rhsusf_RG33L_base {
    };
    class rhsusf_M1232_M2_usarmy_d: rhsusf_RG33L_GPK_base {
        class Turrets: Turrets {
            class M2_Turret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhsusf_M1237_M2_usarmy_d: rhsusf_RG33L_GPK_base {
        class Turrets: Turrets {
            class M2_Turret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_TOW_TriPod_base: StaticATWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class RHS_MELB_base: Helicopter_Base_H {
        class ViewOptics: ViewOptics {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class Turrets: Turrets {
            class CopilotTurret: CopilotTurret {
                minFov = __ZOOMMAX;
                maxFov = __ZOOMMIN;

            };
        };
    };
    class UAV_06_base_F: Helicopter_Base_F {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class rhs_truck: Truck_F {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class Turrets: Turrets {
        };
    };
    class rhs_gaz66_vmf: rhs_truck {
    };
    class rhs_gaz66_zu23_base: rhs_gaz66_vmf {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_bmd_base: Tank_F {
        class Turrets: Turrets {
            class CommanderOptics: NewTurret {
            };
            class MainTurret: MainTurret {
                class Turrets {
                };
            };
        };
    };
    class rhs_bmd2_base: rhs_bmd_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class Turrets: Turrets {
                    class Launcher: CommanderOptics {
                        class OpticsOut {
                            class Out {
                                minFov = __ZOOMMAX;
                                maxFov = __ZOOMMIN;

                            };
                        };
                    };
                };
            };
        };
    };
    class rhs_bmd1_base: rhs_bmd_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class rhs_bmd1p: rhs_bmd1_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class Turrets: Turrets {
                    class Launcher: CommanderOptics {
                        class OpticsOut {
                            class Out {
                                minFov = __ZOOMMAX;
                                maxFov = __ZOOMMIN;

                            };
                        };
                    };
                };
            };
        };
    };
    class rhs_bmp3tank_base: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class Turrets: Turrets {
                    class CommanderOptics: CommanderOptics {
                        class ViewGunner {
                            minFov = __ZOOMMAX;
                            maxFov = __ZOOMMIN;

                        };
                    };
                };
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_bmp1tank_base: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class Turrets {
                };
            };
        };
    };
    class rhs_bmp_base: rhs_bmp1tank_base {
    };
    class rhs_bmp1_vdv: rhs_bmp_base {
    };
    class rhs_bmp1p_vdv: rhs_bmp1_vdv {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class Turrets: Turrets {
                    class Launcher: CommanderOptics {
                        class OpticsOut {
                            class Out {
                                minFov = __ZOOMMAX;
                                maxFov = __ZOOMMIN;

                            };
                        };
                    };
                };
            };
        };
    };
    class rhs_btr_base: Wheeled_APC_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
            class CommanderOptics: CommanderOptics {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class rhs_tank_base: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
                class Turrets: Turrets {
                    class CommanderOptics: CommanderOptics {
                        class ViewGunner {
                            minFov = __ZOOMMAX;
                            maxFov = __ZOOMMIN;

                        };
                    };
                };
            };
        };
    };
    class rhs_t80b: rhs_tank_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class rhs_t80bv: rhs_t80b {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class Turrets: Turrets {
                    class CommanderOptics: CommanderOptics {
                        class ViewGunner {
                            minFov = __ZOOMMAX;
                            maxFov = __ZOOMMIN;

                        };
                    };
                };
            };
        };
    };
    class rhs_btr60_base: rhs_btr_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
            class CommanderOptics: CommanderOptics {
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
};
