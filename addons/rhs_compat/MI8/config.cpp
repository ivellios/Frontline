#include "..\macros.hpp"

#define RHSBASECLASS RHS_Mi8_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)

class CfgMagazines
{
	class Default;
	class CA_Magazine: Default{};
	class VehicleMagazine: CA_Magazine{};
	class rhs_mag_762x54mm_100: VehicleMagazine{};
	class rhs_mag_762x54mm_500: rhs_mag_762x54mm_100
	{
		count = 500;
		ammo = "rhs_ammo_127x108mm";
		tracersEvery = 2;
	};
};


TURRET_INHERIT
	class AllVehicles;
	class Air: AllVehicles {};
	class Helicopter: Air {};
	class Helicopter_Base_F: Helicopter {};
	class Helicopter_Base_H: Helicopter_Base_F {};
	class Heli_Light_02_base_F: Helicopter_Base_H {};
	class RHS_Mi8_base: Heli_Light_02_base_F {
		/*liftForceCoef = 2.3;
		cyclicForwardForceCoef = 3.0;
		class RotorLibHelicopterProperties {
            RTDconfig = "A3\Air_F\Heli_Light_02\RTD_Heli_Light_02.xml";
            defaultCollective = 0.68;
            autoHoverCorrection[] = {4, -3.3, 0};
            maxTorque = 2700;
            stressDamagePerSec = 0.00333333;
            retreatBladeStallWarningSpeed = 87.5;
            maxHorizontalStabilizerLeftStress = 100000;
            maxHorizontalStabilizerRightStress = 100000;
            maxVerticalStabilizerStress = 100000;
            horizontalWingsAngleCollMin = 0;
            horizontalWingsAngleCollMax = 0;
            maxMainRotorStress = 1100000;
            maxTailRotorStress = 250000;
        };*/
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = {"rhs_weap_pkt_v1"};
				magazines[] = {"rhs_mag_762x54mm_500","rhs_mag_762x54mm_500","rhs_mag_762x54mm_500","rhs_mag_762x54mm_500","rhs_mag_762x54mm_500","rhs_mag_762x54mm_500"};
				stabilizedinaxes = 3;
			};
			class BackTurret: MainTurret
			{
				stabilizedinaxes = 3;
			};
		};
	};
};