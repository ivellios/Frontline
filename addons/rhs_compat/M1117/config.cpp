#include "..\macros.hpp"
#define RHSBASECLASS rhsusf_M1117_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

TURRET_INHERIT
	class Car: LandVehicle {};
	class Car_F: Car {};
	class Wheeled_APC_F: Car_F {};
	class rhsusf_M1117_base: Wheeled_APC_F {
	//class rhsusf_M1117_D: rhsusf_M1117_base
			
			class Turrets: Turrets {
				class MainTurret: MainTurret
				{
					magazines[] = {"rhs_mag_200rnd_127x99_mag", "rhs_mag_200rnd_127x99_mag", "rhs_mag_200rnd_127x99_mag", "rhs_mag_200rnd_127x99_mag_Tracer_Red", "RHS_96Rnd_40mm_MK19_M430I", "rhsusf_mag_L8A3_8"};
				};
		};
	};
};