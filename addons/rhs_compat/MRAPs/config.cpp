#include "..\macros.hpp"

#define RHSBASECLASS rhsusf_rg33_base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

// Actual zoom = ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)
// Config entry = 0.4 / ((round (((([0.5,0.5] distance2D worldToScreen positionCameraToWorld [0,3,4]) * (getResolution select 5) / 2) * 1.59989) * 1000)) / 1000)

TURRET_INHERIT
	class Car: LandVehicle {};
	class Car_F: Car {};
	class MRAP_01_base_F : Car_F {};
	class RHSBASECLASS: MRAP_01_base_F {
	};
	class rhsusf_rg33_d : rhsusf_rg33_base {};
	class rhsusf_rg33_wd : rhsusf_rg33_base {};
	class rhsusf_rg33_m2_d : rhsusf_rg33_base {
		class Turrets : Turrets {
            class M2_Turret : MainTurret {
                gunnerLeftHandAnimName = "OtocHlaven_Shake";
                gunnerRightHandAnimName = "OtocHlaven_Shake";
                body = "mainTurret";
                gun = "mainGun";
                animationSourceBody = "mainTurret";
                animationSourceGun = "mainGun";
                turretInfoType = "RscOptics_Offroad_01";
                discreteDistance[] = {100, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500};
                discreteDistanceInitIndex = 2;
                gunnerForceOptics = 0;
                gunnerOutOpticsShowCursor = 0;
                weapons[] = {"RHS_M2"};
                magazines[] = {"rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red", "rhs_mag_100rnd_127x99_mag_Tracer_Red"};
                minElev = -10;
                maxElev = 40;
                soundServo[] = {"A3\sounds_f\dummysound", 1e-006, 1};
                soundAttenuationTurret = "HeliAttenuationGunner";
                disableSoundAttenuation = 0;
                gunnerAction = "RHS_HMMWV_Gunner03";//RHS_HMMWV_Gunner03_in
                gunnerGetInAction = "GetInMRAP_01_cargo";
                gunnerGetOutAction = "GetOutMRAP_01";
                gunnerDoor = "Door_RB";
                gunnerCompartments = "Compartment1";
                ejectDeadGunner = 0;
                castGunnerShadow = 1;
				inGunnerMayFire = 1;
                stabilizedInAxes = 3;
                gunBeg = "usti hlavne";
                gunEnd = "konec hlavne";
                memoryPointGunnerOptics = "gunnerview";
				memoryPointGunnerOutOptics = "gunnerview";
				gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
				gunnerOutOpticsModel = "\A3\weapons_f\reticle\optics_empty";
                optics = 0;
                memoryPointsGetInGunner = "pos cargo 1";//MRAP_01_base_F
                memoryPointsGetInGunnerDir = "pos cargo dir 1";//MRAP_01_base_F
                class ViewOptics : ViewOptics {
                    initFov = 0.7;
                    minFov = 0.25;
                    maxFov = 1.1;
                };
                class ViewGunner : ViewOptics {
                };
            };
        };
	};
	class rhsusf_RG33L_base: MRAP_01_base_F {};
	class rhsusf_RG33L_GPK_base: rhsusf_RG33L_base {};
		class rhsusf_M1237_M2_usarmy_d: rhsusf_RG33L_GPK_base{
	
		class Turrets : Turrets {
  				class M2_Turret: NewTurret {
                gunnerAction = "RHS_HMMWV_Gunner03";//RHS_HMMWV_Gunner03_in
                gunnerGetInAction = "GetInMRAP_01_cargo";
                gunnerGetOutAction = "GetOutMRAP_01";
				inGunnerMayFire = 1;
                stabilizedInAxes = 3;
            };
        };
	};
		class rhsusf_M1232_M2_usarmy_d: rhsusf_RG33L_GPK_base{
	
		class Turrets : Turrets {
  				class M2_Turret: NewTurret {
                gunnerAction = "RHS_HMMWV_Gunner03";//RHS_HMMWV_Gunner03_in
                gunnerGetInAction = "GetInMRAP_01_cargo";
                gunnerGetOutAction = "GetOutMRAP_01";
				inGunnerMayFire = 1;
                stabilizedInAxes = 3;
            };
        };
	};
	
	class rhsusf_rg33_m2_wd : rhsusf_rg33_m2_d {};
	class rhsusf_rg33_usmc_d : rhsusf_rg33_d {};
	class rhsusf_rg33_usmc_wd : rhsusf_rg33_wd {};
	class rhsusf_rg33_m2_usmc_d : rhsusf_rg33_m2_d {};
	class rhsusf_rg33_m2_usmc_wd : rhsusf_rg33_m2_d {};
	
	class rhsusf_M1232_MK19_usarmy_d: rhsusf_M1232_M2_usarmy_d {};
	class rhsusf_M1237_MK19_usarmy_d: rhsusf_M1232_M2_usarmy_d {};

	
	/////
	class Truck_F: Car_F {};
	class Truck_01_base_F: Truck_F {};
	class rhsusf_caiman_base: Truck_01_base_F {};
	class rhsusf_caiman_GPK_base: rhsusf_caiman_base {};
	
	class rhsusf_M1230_M2_usarmy_d: rhsusf_caiman_GPK_base{
	
		class Turrets : Turrets {
  				class M2_Turret: NewTurret {
                gunnerAction = "RHS_HMMWV_Gunner03";//RHS_HMMWV_Gunner03_in
                gunnerGetInAction = "GetInMRAP_01_cargo";
                gunnerGetOutAction = "GetOutMRAP_01";
				inGunnerMayFire = 1;
                stabilizedInAxes = 3;
            };
        };
	};
	class rhsusf_M1220_M2_usarmy_d: rhsusf_caiman_GPK_base{
	
		class Turrets : Turrets {
  				class M2_Turret: NewTurret {
                gunnerAction = "RHS_HMMWV_Gunner03";//RHS_HMMWV_Gunner03_in
                gunnerGetInAction = "GetInMRAP_01_cargo";
                gunnerGetOutAction = "GetOutMRAP_01";
				inGunnerMayFire = 1;
                stabilizedInAxes = 3;
            };
        };
	};
	class rhsusf_M1220_MK19_usarmy_d: rhsusf_M1220_M2_usarmy_d {};
	class rhsusf_M1230_MK19_usarmy_d: rhsusf_M1230_M2_usarmy_d {};
};