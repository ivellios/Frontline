#include "..\macros.hpp"

#define RHSBASECLASS RHS_UAZ_Base

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {QUOTE(RHSBASECLASS)};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
		requiredAddons[] = {
			"FRL_Main",
			"FRL_Rhs_compat"
		};
	};
};

TURRET_INHERIT
	class Car: LandVehicle {};
	class Car_F: Car {};
	class MRAP_02_base_F : Car_F {};
	class rhs_tigr_base: MRAP_02_base_F {};
	class rhs_tigr_vdv: rhs_tigr_base {};
	class rhs_tigr_sts_vdv: rhs_tigr_vdv {
		
		
			class Turrets: Turrets {
				class MainTurret: MainTurret
				{
					magazines[] = {"rhs_mag_762x54mm_100", "rhs_mag_762x54mm_100", "rhs_mag_762x54mm_100", "rhs_mag_762x54mm_100", "rhs_mag_762x54mm_100", "rhs_mag_762x54mm_100", "rhs_mag_762x54mm_100", "rhs_mag_762x54mm_100", "RHS_mag_VOG30_30"};
				};
				class AGS_Turret: MainTurret
				{
					magazines[] = {"RHS_mag_VOG30_30", "rhs_mag_762x54mm_100"};
				};
		};
	};
};
