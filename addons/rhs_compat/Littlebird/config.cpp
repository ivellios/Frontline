#include "..\macros.hpp"

#define RHSBASECLASS Heli_Light_01_base_F

class CfgPatches {
	class DOUBLE(ADDON,RHSBASECLASS) {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        addonRootClass="FRL_Rhs_compat";
        requiredAddons[] = {
            "FRL_Main",
            "FRL_Rhs_compat"
        };
	};
};

TURRET_INHERIT
	class AllVehicles;
	class Air: AllVehicles {};
	class Helicopter: Air {};
	class Helicopter_Base_F: Helicopter {};
	class Helicopter_Base_H: Helicopter_Base_F {};
	class Heli_Light_01_base_F : Helicopter_Base_H {
		/*liftForceCoef = 2.3;
		cyclicForwardForceCoef = 3.0;
		class RotorLibHelicopterProperties {
            RTDconfig = "A3\Air_F\Heli_Light_01\RTD_Heli_Light_01.xml";
            autoHoverCorrection[] = {0.28, 2.88, 0};
            defaultCollective = 0.635;
            maxTorque = 900;
            stressDamagePerSec = 0.00333333;
            maxHorizontalStabilizerLeftStress = 100000;
            maxHorizontalStabilizerRightStress = 100000;
            maxVerticalStabilizerStress = 100000;
            horizontalWingsAngleCollMin = 0;
            horizontalWingsAngleCollMax = 0;
            maxMainRotorStress = 310000;
            maxTailRotorStress = 52000;
            retreatBladeStallWarningSpeed = 77.222;
        };*/
	};
};
