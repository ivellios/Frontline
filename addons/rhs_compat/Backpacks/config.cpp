#include "..\macros.hpp"

#define RHSBASECLASS B_FieldPack_Base
class CfgPatches {
  class DOUBLE(ADDON,RHSBASECLASS) {
    units[] = {QUOTE(RHSBASECLASS)};
    weapons[] = {};
    requiredVersion = REQUIRED_VERSION;
    // author = AUTHOR_STR;
    // authors[] = AUTHOR_ARR;
    // authorUrl = AUTHOR_URL;
    version = VERSION;
    versionStr = QUOTE(VERSION);
    versionAr[] = {VERSION_AR};
    addonRootClass="FRL_Rhs_compat";
    requiredAddons[] = {
      "FRL_Main",
      "FRL_Rhs_compat"
    };
  };
};

class CfgVehicles {
  class Weapon_Bag_Base;
  class RHS_M2_Gun_Bag : Weapon_Bag_Base {
    mass = 110;
  };
  class RHS_M2_Tripod_Bag : Weapon_Bag_Base {
    mass = 100;
  };
  class RHS_M2_MiniTripod_Bag : RHS_M2_Tripod_Bag {
    mass = 100;
  };
  class RHS_Mk19_Gun_Bag : RHS_M2_Gun_Bag {
    mass = 110;
  };
  class RHS_Mk19_Tripod_Bag : RHS_M2_Tripod_Bag {
    mass = 100;
  };
  class rhs_Tow_Gun_Bag : RHS_M2_Gun_Bag {
    mass = 110;
  };
  class rhs_TOW_Tripod_Bag : RHS_M2_Tripod_Bag {
    mass = 100;
  };
  class rhs_M252_Gun_Bag : RHS_M2_Gun_Bag {
    mass = 110;
  };
  class rhs_M252_Bipod_Bag : RHS_M2_Tripod_Bag {
    mass = 100;
  }; 
};
