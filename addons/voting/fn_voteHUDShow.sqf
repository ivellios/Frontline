/*
    Function:       FRL_Voting_fnc_voteHUDShow
    Author:         Adanteh
    Description:    Shows the F5/F6 box
*/
#include "macros.hpp"

if !(isNull (uiNamespace getVariable [QGVAR(questionHUD), controlNull])) exitWith { };

private _topDisplay = [(allDisplays - [findDisplay 12]) select { !(_x getVariable [QSVAR(noOverlay), false]) }] call MFUNC(arrayPeek);
private _ctrlQuestionHUD = _topDisplay ctrlCreate [QSVAR(questionHUD), 8601, controlNull];
uiNamespace setVariable [QGVAR(questionHUD), _ctrlQuestionHUD];

// -- We move the main group up, while moving the other group down to simulate a animation from bottom to top -- //
private _groupPosition = ctrlPosition _ctrlQuestionHUD;
private _groupSubPosition = ctrlPosition CTRL(8602);
_groupPosition set [1, (_groupPosition select 1) - PY(10)];
_groupSubPosition set [1, 0];

//[[_fnc_scriptNameShort, _topDisplay, _ctrlQuestionHUD, _groupPosition]] call MFUNC(debugMessage);

_ctrlQuestionHUD ctrlSetPosition _groupPosition;
_ctrlQuestionHUD ctrlCommit 0.2;
CTRL(8602) ctrlSetPosition _groupSubPosition;
CTRL(8602) ctrlCommit 0.2;

if (_topDisplay getVariable [QGVAR(hudKeyHandle), -1] isEqualTo -1) then {
    _topDisplay setVariable [QGVAR(hudKeyHandle), _topDisplay displayAddEventHandler ["keyDown", FUNC(voteHUDkeyhandle)]];
    //uiNamespace setVariable [QGVAR(questionEHDisplay), _topDisplay];
};

if (GVAR(hudHandle) isEqualTo -1) then {
    GVAR(hudHandle) = [{ _this call FUNC(voteHUDloop) }, 0.25] call MFUNC(addPerFramehandler);
};
