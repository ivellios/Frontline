#include "macros.hpp"

/*
 *	File: fn_voteProgress.sqf
 *	Author: Adanteh
 *	Describe your function
 *
 *	0 ARRAY: _votes - Description
 *
 */

(_this select 0) params ["_votes", "_duration"];
VAR(_yesVotes) = _votes param [__yesIndex, 0];
VAR(_noVotes) = _votes param [__noIndex, 0];
VAR(_undecidedVotes) = _votes param [2, 0];
VAR(_msg) = format ["Votes so far: [%1 yes, %2 no], %3 undecided", _yesVotes, _noVotes, _undecidedVotes];
["Voting: " + _msg] call MFUNC(customMsg);