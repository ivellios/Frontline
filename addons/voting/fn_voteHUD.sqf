/*
    Function:       FRL_Voting_fnc_voteHUD
    Author:         Adanteh
    Description:    This shows a little F5 / F6 question in the bottom right, that passed back code
    Example:        [[player, player, "Does this work?", { hint (["No", "Yes"] select (_this select 2) )}]] call FRL_voting_fnc_voteHUD;
*/
#define __INCLUDE_DIK
#include "macros.hpp"

(_this select 0) params ["_target", "_caller", "_question", ["_callback", { true }, [{}, ""]], ["_timeout", 60], ["_options", [[DIK_F5, DIK_F6], [1, 0], ["Yes", "No"]]]];
if (_callback isEqualType "") then { _callback = compile _callback; };

// -- Format keybind names once -- //
if (count _options <= 3) then {
	_options set [3, []];
};
{
	(_options select 3) pushBack (format ["[%1]", [_x] call MFUNC(getKeybindText)]);
} forEach (_options select 0);

GVAR(queueIndex) = GVAR(queueIndex) + 1;
GVAR(queue) pushBack [GVAR(queueIndex), _target, _caller, _question, _callback, serverTime, (serverTime + _timeout), _options];

// -- Already a question being asked -- //
if (GVAR(hudHandle) != -1) exitWith { };

// -- Create the hud and add key handling -- //
[] call FUNC(voteHUDShow);
