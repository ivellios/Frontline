#include "macros.hpp"

/*
 *	File: fn_showOutcome.sqf
 *	Author: Adanteh
 *	Describe your function
 *
 *	0 OBJECT: _unit - Description
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(showOutcome);
 */

(_this select 0) params ["_outcome", "_votesCount", ["_msgResult", ""], ["_requiredRatio", 0.5]];

VAR(_yesVotes) = _votesCount select __yesIndex;
VAR(_noVotes) = _votesCount select __noIndex;
VAR(_useDelay) = false;

// -- Show required ratio -- //
[format ["Voting: %1%2 'yes' required to pass this vote.", ( _requiredRatio * 100), '%']] call MFUNC(customMsg);

// -- Show message with outcome -- //
VAR(_msg) = "";
if (_outcome == __yesIndex) then {
	_msg = format ["The 'yes' votes won (%1 'yes' to %2 'no').", _yesVotes, _noVotes];
};

if (_outcome == __noIndex) then {
	_msg = format ["The 'no' votes won (%2 'no' to %1 'yes').", _yesVotes, _noVotes];
};

if (_outcome == __insufficientVotes) then {
	["Voting: There were insufficient votes for an outcome."] call MFUNC(customMsg);
	_useDelay = true;
};

if !(_useDelay) then {
	["Voting: " + _msg] call MFUNC(customMsg);
};

if (_msgResult != "") then {
	["Voting: " + _msgResult] call MFUNC(customMsg);
};
