#include "macros.hpp"

/*
 *	File: fn_voteHUD.sqf
 *	Author: Adanteh
 *	Hides the voting HUD
 *
 *	Example:
 *	[player] call FUNC(voteHUD);
 */


[GVAR(hudHandle)] call MFUNC(removePerFrameHandler);
GVAR(hudHandle) = -1;
// (uiNamespace getVariable [QGVAR(questionEHDisplay), displayNull]) displayRemoveEventHandler ["keyDown", GVAR(hudKeyHandle)];
// GVAR(hudKeyHandle) = -1;

private _ctrlGroup = uiNamespace getVariable [QGVAR(questionHUD), controlNull];
/*
private _ctrlPos = ctrlPosition _ctrlGroup;
_ctrlPos set [1, (_ctrlPos select 1) + PY(10)];
_ctrlGroup ctrlSetPosition _ctrlPos;
_ctrlGroup ctrlCommit 0.15;
*/
ctrlDelete _ctrlGroup;

true;
