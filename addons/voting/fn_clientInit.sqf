#include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Client init for voting system
 *
 */


if (isNil QGVAR(startTime)) then { GVAR(startTime) = (-__voteInitiationPeriod) };
if (isNil QGVAR(initiated)) then { GVAR(initiated) = false };

GVAR(queue) = [];
GVAR(queueIndex) = 0;
GVAR(hudHandle) = -1;
GVAR(hudKeyHandle) = -1;
GVAR(hudRespawnOverlay) = false;

[QGVAR(askQuestion), { _this call FUNC(voteHUD) }] call CFUNC(addEventHandler);
[QGVAR(startVote), { _this call FUNC(voteStart) }] call CFUNC(addEventHandler);
[QGVAR(progress), { _this call FUNC(showProgress) }] call CFUNC(addEventHandler);
[QGVAR(end), { _this call FUNC(showOutcome) }] call CFUNC(addEventHandler);
