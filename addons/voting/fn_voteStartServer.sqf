/*
 *	File: fn_voteStartServer.sqf
 *	Author: Adanteh
 *	Starts the voting system on the server
 *
 *	Parameter(s):
 *		#0 OBJECT: _caller - Person that started the vote
 *		#1 SCALAR: _voteAction - What we're voting for (Change mission, extend time, pause battleprep)
 *		#2 SCALAR: _voteReason - Why you're starting the vote (Not enough players, unbalanced, etc)
 *
 *	Returns:
 *	true <BOOL>
 *
 */

#include "macros.hpp"

// -- Make sure a different vote hasn't started yet -- //
if (GVAR(initiated)) exitWith { };

(_this select 0) params ["_caller", ["_voteAction", "", [0, ""]], ["_voteReason", "", [0, ""]]];

GVAR(initiated) = true;
GVAR(startTime) = serverTime;
GVAR(votes) = [[], []];
GVAR(displayTime) = serverTime;
publicVariable QGVAR(startTime);
publicVariable QGVAR(initiated);

[{
	params ["_caller", "_voteAction", "_voteReason"];

	private _voteTarget = ([_voteAction] call FUNC(getVoteActions)) param [7, "all"];
	private _voteTargetReal = switch (toLower _voteTarget) do {
		case "callerside": { side group _caller; };
		case "callergroup": { group _caller; };
		case "all";
		default { 0; };
	};

	private _polledUnitCount = switch (toLower _voteTarget) do {
		case "callerside": { ({side group _x == _voteTargetReal} count allPlayers) };
		case "callergroup": { (count units _caller); };
		case "all";
		default { (count allPlayers); };
	};

	diag_log format ["[FRL VOTING - %1, %2, %3]", _voteTarget, _voteTargetReal, _polledUnitCount];
	[QGVAR(startVote), _voteTargetReal, [_caller, _voteAction, _voteReason]] call CFUNC(targetEvent);
	_endTime = GVAR(startTime) + __questionTimeout;

	[{
		(_this select 0) params ["_caller", "_voteAction", "_voteReason", "_endTime", "_polledUnitCount", "_voteTargetReal"];


		if (((count (GVAR(votes) select __yesIndex)) + (count (GVAR(votes) select __noIndex)) >= _polledUnitCount) || ({serverTime >= _endTime})) then {

			// -- Retrieve code connected to this voting action -- //
			([_voteAction] call FUNC(getVoteActions)) params ["_voteAction", "_voteReasons", "_voteCodeYes", "_voteCodeNo", "_voteMsgYes", "_voteMsgNo", "_requiredRatio"];

			// -- Calculate outcome.  -- //
			VAR(_yesVotes) = count (GVAR(votes) select __yesIndex);
			VAR(_noVotes) = count (GVAR(votes) select __noIndex);
			VAR(_totalVotes) = _yesVotes + _noVotes;
			VAR(_totalVotesReqd) = _polledUnitCount * __requiredPercentage;

			VAR(_resultIndex) = switch (true) do {
				case (_totalVotes < _totalVotesReqd): { __insufficientVotes; };
				case ((_yesVotes / _totalVotes) > _requiredRatio): { __yesIndex; };
				default { __noIndex; };
			};

			[_this select 1] call MFUNC(removePerFrameHandler);

			// -- Reset the current voting progress. This allows creation of new votes and stops votes coming in for this question -- //
			GVAR(initiated) = false;
			publicVariable QGVAR(initiated);

			VAR(_msgResult) = [_voteMsgNo, _voteMsgYes] select (_resultIndex == __yesIndex);
			[QGVAR(end), [_resultIndex, [_yesVotes, _noVotes], _msgResult, _requiredRatio]] call CFUNC(globalEvent);

			// -- Execute code connected to this voting action -- //
			if (_resultIndex == __yesIndex) then {
				[_caller, _voteTargetReal] call _voteCodeYes;
			};

			if (_resultIndex == __noIndex) then {
				[_caller, _voteTargetReal] call _voteCodeNo;
			};
		};

	}, 1, [_caller, _voteAction, _voteReason, _endTime, _polledUnitCount, _voteTargetReal]] call MFUNC(addPerFramehandler);
}, 1, [_caller, _voteAction, _voteReason]] call CFUNC(wait);
