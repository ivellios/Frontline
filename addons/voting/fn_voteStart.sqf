#include "macros.hpp"

/*
 *	File: fn_voteStart.sqf
 *	Author: Adanteh
 *	Describe your function
 *
 *	#0
 *		#0 OBJECT: _caller - Person that started the vote
 *		#1 SCALAR: _voteAction - What we're voting for (Change mission, extend time, pause battleprep)
 *		#2 SCALAR: _voteReason - Why you're starting the vote (Not enough players, unbalanced, etc)
 *
 *	#1
 *		#0 SCALAR: _startTime - Time the vote was started on the server
 *
 *	Returns:
 *	true <BOOL>
 *
 */

(_this select 0) params ["_caller", ["_voteAction", "", [0, ""]], ["_voteReason", "", [0, ""]]];

([_voteAction] call FUNC(getVoteActions)) params ["_voteAction", "_voteReasons"];
_voteReason = _voteReasons param [_voteReason, ""];

[{
	params ["_voteAction"];
	VAR(_voteMessage) = format ["Vote - %1", _voteAction];
	[QGVAR(askQuestion), [CLib_Player, CLib_Player, _voteMessage, {
		params ["_target", "_caller", "_answer"];
		[QGVAR(voted), [_caller, (_answer >= 1)]] call CFUNC(serverEvent);
	}]] call CFUNC(localEvent);
}, 3, [_voteAction]] call CFUNC(wait);
