/*
    Function:       FRL_Voting_fnc_endMissionVote
    Author:         Adanteh
    Description:    Function to end the mission through voting
    Locality:       Server only
*/
#include "macros.hpp"

params ["_params", "_reason", ["_loser", 'gamemode']];
_params params ["_caller", "_voteTargetReal"];

private _loserTeam = switch (toLower _loser) do {
    case 'gamemode': { nil };
    case 'callerside': { _voteTargetReal };
};

[{
    ['endVote', _this] call CFUNC(globalEvent);
}, 10, [_reason, _loserTeam]] call CFUNC(wait);
