#include "macros.hpp"

/*
 *	File: fn_voteReceive.sqf
 *	Author: Adanteh
 *	Describe your function
 *
 *	Parameter(s):
 *		#0 OBJECT: _caller - Unit that answered the vote
 *		#1 BOOLEAN: _answer - yes/no answer to the question
 *
 */

if (!GVAR(initiated)) exitWith { }; // Voting completed early.

(_this select 0) params ["_caller", "_answer"];

// -- Yes / No index. Units that voted are saved (We probably want to save name instead) -- //
VAR(_voteIndex) = [__noIndex, __yesIndex] select _answer;
(GVAR(votes) select _voteIndex) pushBack _caller;

// only display the message after 10 seconds of voting
// only display the message every 5 seconds (or so).
if (serverTime > (GVAR(startTime) + 10) && {serverTime > (GVAR(displayTime) + 5)}) then {
	GVAR(displayTime) = serverTime;
	VAR(_duration) = if (serverTime < (GVAR(startTime) + (__questionTimeout / 2))) then {5} else {8};
	VAR(_yesVotes) = count (GVAR(votes) select __yesIndex);
	VAR(_noVotes) = count (GVAR(votes) select __noIndex);
	VAR(_undecidedVotes) = (count allPlayers) - (_yesVotes + _noVotes);
	[QGVAR(progress), [[_yesVotes, _noVotes, _undecidedVotes]]] call CFUNC(globalEvent);
};
