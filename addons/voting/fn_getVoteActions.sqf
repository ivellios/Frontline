#include "macros.hpp"

/*
 *	File: fn_voteActionsDefault.sqf
 *	Author: Adanteh
 *	Retrieves full voting options based on ID
 *
 *	Parameter(s):
 *		#0
 *			#0 SCALAR: _voteActionInput - ID of action we are voting for (Change mission, extend time, pause BattlePrep)
 *			#1 SCALAR: _voteReason - Why you are starting the vote (Not enough players, unbalanced, etc)
 *
 *		#1
 *			#0 SCALAR: _startTime - Time the vote was started on the server
 *
 *	Returns:
 *	Array with full details on a voting option shorthand <ARRAY>
 *
 */

params [
	["_voteActionInput", "", [0, ""]],
	["_voteReason", "", [0, ""]]
];

VAR(_requiredRatio) = __requiredPercentage;
VAR(_voteAction) = "";
VAR(_voteMsgYes) = "";
VAR(_voteCodeYes) = {};
VAR(_voteMsgNo) = "";
VAR(_voteCodeNo) = {};
VAR(_voteReasons) = [];
VAR(_voteTarget) = "all";

// #TRANSLATE#

// -- Allows using integer for vote category (More effecient than sending full string) -- //
if (_voteActionInput isEqualType 0) then {
	private _config = (configFile >> "FRL" >> "cfgVoting") select _voteActionInput;
	_requiredRatio = getNumber (_config >> "requiredRatio");
	_voteAction = getText (_config >> "questionText");
	_voteMsgYes = getText (_config >> "voteYesText");
	_voteMsgNo = getText (_config >> "voteNoText");
	_voteCodeYes = compile getText (_config >> "voteYesCode");
	_voteCodeNo = compile getText (_config >> "voteNoCode");
	_requiredRatio = getNumber (_config >> "requiredRatio");
	_voteTarget = getText (_config >> "voteTarget");
};

[_voteAction, _voteReasons, _voteCodeYes, _voteCodeNo, _voteMsgYes, _voteMsgNo, _requiredRatio, _voteTarget];
