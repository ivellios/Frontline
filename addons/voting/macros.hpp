// -- Module defines -- //
#define MODULE Voting

#define __yesIndex 0
#define __noIndex 1
#define __insufficientVotes 2

#define __requiredPercentage 0.50 // a min of this % of players must vote, in order to avoid an __insufficientVotes outcome
#define __voteInitiationPeriod (10 * 60) // this should be set to around 5-20 minutes. Set to 1 for testing only.
#define __questionTimeout 60 // clients get 60 secs to vote, as per Question HUD allows.

#define _V_switchMission 1
#define _V_pauseBattlePrep 2
#define _V_continueBattlePrep 3
#define _V_gameTimeExtend 6
#define _V_gameTimeShorten 7
#define _V_ticketsIncrease 8
#define _V_ticketsLower 9
#define _V_reduceBattlePrep 12
#define _V_increaseBattlePrep 13

#define CTRL(var1) ((uiNamespace getVariable QGVAR(questionHUD)) controlsGroupCtrl var1)

// -- Global defines -- //
#include "..\main\macros_local.hpp"
