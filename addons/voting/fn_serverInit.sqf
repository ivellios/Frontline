#include "macros.hpp"
/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Server init for voting system
 *
 */

GVAR(startTime) = (-__voteInitiationPeriod);
GVAR(initiated) = false;

[QGVAR(start), FUNC(voteStartServer)] call CFUNC(addEventHandler);
[QGVAR(voted), FUNC(voteReceive)] call CFUNC(addEventHandler);

DFUNC(adjustTickets) = {
	params ["_ticketAdjust"];
    EGVAR(Tickets,ticketsToWin) = EGVAR(Tickets,TicketsToWin) - _ticketAdjust;
    publicVariable QEGVAR(tickets,ticketsToWin);
};
