/*
    Function:       FRL_Voting_fnc_voteHUDkeyhandle
    Author:         Adanteh
    Description:    Handles button presses for voting/question HUD
*/
#include "macros.hpp"

params ["_display", "_button"];

if (GVAR(queue) isEqualTo []) exitWith { nil };

private _return = false;
_buttonIndex = (((GVAR(queue) select 0) select 7) select 0) find _button;
if (_buttonIndex != -1) then {

    private _question = GVAR(queue) deleteAt 0;
    _question params ["", "_target", "_caller", "", "_callback"];
    private _answer = ((_question select 7) select 1) select _buttonIndex;
    [QGVAR(answerQuestion), _question] call CFUNC(localEvent);
    [_target, _caller, _answer] call _callBack;

    // -- Clean HUD instantly if no remainig questsion -- //
    if (GVAR(queue) isEqualTo []) then {
        call FUNC(voteHUDHide);
    };

    _return = true;
};

_return;
