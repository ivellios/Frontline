/*
    Function:       FRL_Voting_fnc_voteHUDloop
    Author:         Adanteh
    Description:    Handles passing the questions one by one, plus showing them in the HUD
*/
#include "macros.hpp"

if (GVAR(queue) isEqualTo []) exitWith {
	call FUNC(voteHUDHide);
};

// -- Cleanup timedout questions -- //
private _exit = false;
while { (serverTime - ((GVAR(queue) select 0) select 6) >= 0) } do {
	GVAR(queue) deleteAt 0;
	if (GVAR(queue) isEqualTo []) exitWith {
		_exit = true;
	};
};

if (_exit) exitWith {
	call FUNC(voteHUDHide);
};

// -- Reopen the HUD if needed
[] call FUNC(voteHUDShow);

(GVAR(queue) select 0) params ["", "", "_caller", "_question", "", "_startTime", "_timeout", "_options"];
private _fnc_updateOptions = {
	params ["_ctrlGroup"];
	lnbClear CTRL(8604);
	private _questionText = format ["<t font='RobotoCondensedLight'>%1 (%2)</t>", _question, ([(_timeout - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString)];
	CTRL(8603) ctrlSetStructuredText (parseText _questionText);
	{
		if ((_forEachIndex mod 2) == 0) then {
			private _rowContents = [(_options select 3) select _forEachIndex, _x];
			private _nextRow = (_options select 3) param [_forEachIndex + 1, "-"];
			if (_nextRow != "-") then {
				_rowContents append [_nextRow, ((_options select 2) select _forEachIndex + 1)];
			};
			CTRL(8604) lnbAddRow _rowContents
		};
	} forEach (_options select 2);
};

[uiNamespace getVariable QGVAR(questionHUD)] call _fnc_updateOptions;
