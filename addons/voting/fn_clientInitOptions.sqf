#include "macros.hpp"

/*
 *	File: fn_clientInitOptions.sqf
 *	Author: Adanteh
 *	This adds all the vote options under the voting menu, accessed through Esc Menu
 *
 */

{
	if ((getNumber (_x >> "scope")) >= 2) then {
		['add', ["voting", 'Voting', 'Voting', [
			"",
			getText (_x >> "questionText"),
			QMVAR(CtrlButton),
			{ false },
			{ !GVAR(initiated); },
			(format ["[%1] call %2; closeDialog 1;", _forEachIndex, QFUNC(voteInitiate)]),
			[getText (_x >> "displayName")]
		]]] call MFUNC(settingsWindow);
	};

} forEach ("true" configClasses (configFile >> "FRL" >> "cfgVoting"));
