#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\voting";

            class clientInit;
            class clientInitOptions;
            class endMissionVote;
            class getVoteActions;
            class serverInit;
            class showOutcome;
            class showProgress;
            class voteHUD;
            class voteHUDHide;
            class voteHUDkeyhandle;
            class voteHUDloop;
            class voteHUDShow;
            class voteInitiate;
            class voteReceive;
            class voteStart;
            class voteStartServer;
        };
    };
};
