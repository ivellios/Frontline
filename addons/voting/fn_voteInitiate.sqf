#include "macros.hpp"

/*
 *	File: fn_voteInitiate.sqf
 *	Author: Adanteh
 *	Starts a vote from a player
 *
 *	Parameter(s):
 *		#0 SCALAR: _voteAction - What we're voting for (Change mission, extend time, pause battleprep)
 *		#1 SCALAR: _voteReason - Why you're starting the vote (Not enough players, unbalanced, etc)
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[0,1] call FRL_voting_fnc_voteInitiate;
 */

params [["_voteAction", 0, [0, ""]], ["_voteReason", 0, [0, ""]]];

VAR(_caller) = player;
VAR(_time) = serverTime;
VAR(_recentVoteCast) = _time < GVAR(startTime) + __voteInitiationPeriod;

hintSilent "";

// -- Delay between mission votes -- //
if ((_voteAction in [_V_switchMission, _V_gameTimeExtend, _V_gameTimeShorten, _V_ticketIncrease, _V_ticketLower]) && _recentVoteCast) exitWith {
	private _voteCooldown = [(GVAR(startTime) + __voteInitiationPeriod - _time), "MM:SS", false] call bis_fnc_secondsToString;
	VAR(_msg) = format ["A vote was already concluded recently.<br/><br/>Wait another %1s before initiating another vote of this type. Other vote types might already be possible.", _voteCooldown];
	hintSilent parseText _msg;
};

[QGVAR(start), [_caller, _voteAction, _voteReason]] call CFUNC(serverEvent);