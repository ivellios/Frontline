/*
 *	File: fn_initMissionDuration.sqf
 *	Author: Adanteh
 *	Inits the maximum duration for mission. Mission will auto complete after this time
 *
 *  Locality: Server only
 *
 *	Example:
 *	[] call FUNC(initMissionDuration);
 */

#include "macros.hpp"

[QGVAR(Settings), configFile >> "FRL" >> "cfgMissionDuration"] call MFUNC(cfgSettingLoad);
[QGVAR(Settings), missionConfigFile >> "FRL" >> "cfgMissionDuration"] call MFUNC(cfgSettingLoad);

VAR(_maxDuration) = ([QGVAR(Settings_durationLimited), 1] call MFUNC(cfgSetting) >= 1);
if !(_maxDuration) exitWith { };

if (isServer) then {
    VAR(_duration) = [QGVAR(Settings_missionDuration), 90*60] call MFUNC(cfgSetting);

    GVAR(missionDuration) = _duration;
    publicVariable QGVAR(missionDuration);
    // -- Check time remaining -- //

    [{
        VAR(_timeRemaining) = [true] call FUNC(getMissionDuration);
        if (_timeRemaining <= 0) then {
            [_this select 1] call MFUNC(removePerFrameHandler);
            ["durationEnd", [time]] call MFUNC(persistentEventGlobal);
        };
    }, 5, []] call MFUNC(addPerFramehandler);

    GVAR(missionDurationStarted) = true;

    [QGVAR(changeDuration), {
        (_this select 0) params ["_duration", "_caller"];
        if !(_duration isEqualType 0) exitWith { };
        [(_duration * 60), false] call FUNC(changeMissionDuration);
    }] call CFUNC(addEventHandler);
};

if (hasInterface) then {
    [{
        private _timeLeft = ([true] call FUNC(getMissionDuration)) max 0;
        private _durationText = format ["<t align='left'>TIME LEFT <t size='1' font='EtelkaMonospacePro' align='right' color='#ffffff'>%1</t></t>", ([ceil _timeLeft,"HH:MM:SS"] call bis_fnc_secondsToString)];
        _durationText
    }, 1] call MFUNC(addMapHUD);

    ['add', ["admin", 'Admin Settings', 'Misc', [
        'Duration',
        'Set time left in minutes',
        QMVAR(CtrlTextBoxButton),
        { round (([true] call FUNC(getMissionDuration)) / 60) },
        { [Clib_Player] call MFUNC(isAdmin); },
        (format ["
            if (parseNumber _value != 0) then {
                ['%1', [parseNumber _value, Clib_Player]] call %2;
            };
            false;
        ", QGVAR(changeDuration), QCFUNC(serverEvent)]),
        "SET"
    ]]] call MFUNC(settingsWindow);
};
