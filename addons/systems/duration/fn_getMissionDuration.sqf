/*
 *	File: fn_getMissionDuration.sqf
 *	Author: Adanteh
 *	Gets the maximum mission duration
 *
 *	0: True to return time left. False to return total time <BOOLEAN>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(getMissionDuration);
 */

#include "macros.hpp"

params [["_timeLeft", false]];

VAR(_duration) = (missionNamespace getVariable [QGVAR(missionDuration), -1]);
([_duration, (_duration - time)] select _timeLeft);
