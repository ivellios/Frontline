class duration {
    defaultLoad = 1;
    path = PATHTO(duration);
    dependencies[] = {"Main"};

    class initMissionDuration;
    class getMissionDuration;
    class changeMissionDuration;
};
