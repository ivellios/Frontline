/*
 *	File: fn_changeMissionDuration.sqf
 *	Author: Adanteh
 *	Changes the time left in this mission
 *
 *	0: Time to add / set <SCALAR>
 *	1: True to offset current time. False to set new total time <BOOLEAN>
 *
 *	Returns:
 *	New mission duration in seconds <SCALAR>
 *
 *	Example:
 *	[(30*60), true] call FUNC(changeMissionDuration);
 */

#include "macros.hpp"

if !(isServer) exitWith { };

params [["_time", (30*60)], ["_offset", true]];

VAR(_duration) = (missionNamespace getVariable [QGVAR(missionDuration), -1]);

if (_offset) then {
	_duration = 0 max (_duration + _time);
} else {
	_duration = _time + time;
};

GVAR(missionDuration) = _duration;
publicVariable QGVAR(missionDuration);

_duration
