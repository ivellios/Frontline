class unitMarkers {
    defaultLoad = 1;
    path = PATHTO(unitMarkers);
    dependencies[] = {"Main"};

    class clientInit;
    class addGroupMarker;
    class addUnitMarker;
    class groupMouseOverText;
    class handleUnit;
    class markerAddMouseOver;
};
