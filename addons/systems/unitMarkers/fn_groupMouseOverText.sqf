/*
    Function:       FRL_UnitMarkers_fnc_groupMouseOverText
    Author:         Adanteh
    Description:    Adjust the mouseover pane and adds the squadnames + icons
*/
#include "macros.hpp"
#define __CTRL(var1) (_ctrlGrp controlsGroupCtrl var1)

params ["_group", "_map", "_ctrlGrp", "_attachTo"];

private _crewUnits = "";
private _unitCount = {
    private _name = [_x] call CFUNC(name);
    if (_name != "Error: No unit") then {
        private _kitIcon = _x getVariable [QSVAR(rkitIcon), ""];
        _crewUnits = _crewUnits + format ["<img color='#ffffff' image='%1'/> %2<br />", _kitIcon, _name];
        true;
    } else {
        false
    }
} count (units _group);

__CTRL(3) ctrlSetStructuredText parseText format ["<t>%1</t>", _crewUnits];
__CTRL(4) ctrlSetStructuredText parseText format ["<t align='right'>%1 / %2</t>", _unitCount,  _group getVariable [QMVAR(gSize), ""]];

private _textHeight = ctrlTextHeight __CTRL(3);
private _pos = _map ctrlMapWorldToScreen (getPosATL leader _group);
_pos set [0, (_pos select 0) + 15 / 640];
_pos set [1, (_pos select 1) + (((_attachTo) select 1) + 5) / 480];
_pos append [GRIDX(10), _textHeight + GRIDY(1.2) + ([GRIDY(0.4), 0] select (_textHeight == 0))];
_ctrlGrp ctrlSetPosition _pos;
_ctrlGrp ctrlShow true;
_ctrlGrp ctrlCommit 0;

// -- Adjust the background and group so we don't get scrolling
{
    _x params ["_idc", "_offset"];
    private _pos = ctrlPosition __CTRL(_idc);
    _pos set [3, _textHeight + GRIDY(_offset)];
    __CTRL(_idc) ctrlSetPosition _pos;
    __CTRL(_idc) ctrlCommit 0;
} forEach [[2, 0.4], [3, 0.2]];
