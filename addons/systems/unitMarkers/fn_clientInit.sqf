/*
    Function:       FRL_UnitMarkers_fnc_clientInit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

GVAR(blockUpdate) = false;
GVAR(currentHoverGroup) = grpNull;
GVAR(groupInfoPFH) = -1;
GVAR(processedIcons) = [];
GVAR(lastProcessedIcons) = [];

// -- On team switch delete all current markers and replace them by new team ones -- //
["playerSideChanged", {
	GVAR(switchedTeams) = true;
}] call CFUNC(addEventHandler);

// -- Add the statemachine that handles changing markers when there are units inside them -- //
GVAR(markerSM) = call CFUNC(createStatemachine);

[GVAR(markerSM), "init", {
    GVAR(processedIcons) = [];

    private _units = +(allUnits);
    [["updateMarkers", _units], "init"] select (_units isEqualTo []);
}] call CFUNC(addStatemachineState);

[GVAR(markerSM), "updateMarkers", {
    params ["", "_units"];

    private _unit = _units deleteAt 0;
    [_unit, group _unit] call FUNC(handleUnit);

    [["updateMarkers", _units], "cleanup"] select (_units isEqualTo []);
}] call CFUNC(addStatemachineState);

[GVAR(markerSM), "cleanup", {

    {
        // [_x, "hoverin"] call CFUNC(removeMapGraphicsEventHandler);
        // [_x, "hoverout"] call CFUNC(removeMapGraphicsEventHandler);
        [_x] call CFUNC(removeMapGraphicsGroup);
        nil
    } count (GVAR(lastProcessedIcons) - GVAR(processedIcons));
    GVAR(lastProcessedIcons) = +GVAR(processedIcons);

    "init"
}] call CFUNC(addStatemachineState);

["DrawMapGraphics", {
    GVAR(markerSM) call CFUNC(stepStatemachine);
}] call CFUNC(addEventhandler);
