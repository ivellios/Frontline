/*
    Function:       FRL_UnitMarkers_fnc_addUnitToTracker
    Author:         BadGuy, joko
    Description:    Respawns a unit
*/
#include "macros.hpp"

/*---------------------------------------------------------------------------
    This is a modified version of AAW functions, June 11th 2016 version
    Released under APL license by the AAW Team.

    Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
    Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
---------------------------------------------------------------------------*/

params ["_unit", "_iconId", "_group", "_incapped", "_hover", "_selected"];
private ["_color", "_texture"];

if (_incapped) then {
    _texture = "a3\ui_f\data\IGUI\Cfg\Actions\heal_ca.paa";
    _color = [1,0.2,0.2,1];
} else {
    _texture = _unit getVariable [QSVAR(rMapIcon), "\A3\ui_f\data\map\vehicleicons\iconMan_ca.paa"];
    if (_selected) exitWith { _color = [0.0, 0.76, 0.48, 1] };
    if (group Clib_Player isEqualTo _group) then {
        _color = [0, 0.87, 0, 1];
    } else {
        _color = +([side _group, "color"] call MFUNC(getSideData));
    };

    if (_hover) then {
        _color = _color apply { _x + 0.2 };
    };
};


[_iconId, [
    ["ICON", _texture, _color, _unit, 20, 20, _unit, "", 1],
    ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _unit, 20, 20, 0, [_unit] call CFUNC(name), 2]
]] call CFUNC(addMapGraphicsGroup);

/*
[_iconId, "hoverin", {
    (_this select 0) params ["_map", "_xPos", "_yPos"];
    (_this select 1) params ["_group"];

    if (_group isEqualTo GVAR(currentHoverGroup)) exitWith {};
    GVAR(currentHoverGroup) = _group;

    {
        [_x, _group] call FUNC(handleUnit);
    } forEach (units _group);
}, _group] call CFUNC(addMapGraphicsEventHandler);
*/

/*
[_iconId, "hoverout", {
    (_this select 0) params ["_map", "_xPos", "_yPos"];
    (_this select 1) params ["_group"];
    if (GVAR(currentHoverGroup) isEqualTo _group) then {
        GVAR(currentHoverGroup) = grpNull;
    };

    {
        [_x, _group] call FUNC(handleUnit);
    } forEach (units _group);

}, _group] call CFUNC(addMapGraphicsEventHandler);
_iconId;
*/
