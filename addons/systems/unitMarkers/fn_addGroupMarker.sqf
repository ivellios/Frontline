/*
    Function:       FRL_UnitMarkers_fnc_addGroupMarker
    Author:         BadGuy, joko
    Description:    Adds marker for group
*/
#include "macros.hpp"

/*---------------------------------------------------------------------------
    This is a modified version of AAW functions, June 11th 2016 version
    Released under APL license by the AAW Team.

    Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
    Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
---------------------------------------------------------------------------*/

params ["_group", "_groupIconId", ["_attachTo", [0, -20]], "_hover", "_selected"];
private ["_color"];

if (true) then {
    if (_selected) exitWith { _color = [0.0, 0.76, 0.48, 1] };
    if (group Clib_Player isEqualTo _group) then {
        _color = [0, 0.87, 0, 1];
    } else {
        _color = +([side _group, "color"] call MFUNC(getSideData));
    };

    if (_hover) then {
        _color = _color apply { _x + 0.2 };
    };
};

private _groupType = _group getVariable [QMVAR(gType), "Rifle"];
private _groupMapIcon = _group getVariable [QMVAR(gIcon), "\A3\ui_f\data\map\markers\nato\b_inf.paa"];
private _iconPos = [vehicle leader _group, _attachTo];

[_groupIconId, [
    ["ICON", _groupMapIcon, _color,_iconPos, 25, 25],
    ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _iconPos, 25, 25, 0, (groupId _group) select [0, 1], 2]
]] call CFUNC(addMapGraphicsGroup);

[_groupIconId, _group, _attachTo] call FUNC(markerAddMouseOver);
