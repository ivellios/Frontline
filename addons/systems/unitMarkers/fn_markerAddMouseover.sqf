/*
    Function:       FRL_unitMarkers_fnc_markerAddMouseOver
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_groupIconId", "_group", "_attachTo"];

[
    _groupIconId,
    "hoverin",
    {
        (_this select 0) params ["_map", "_xPos", "_yPos"];
        (_this select 1) params ["_group", "_attachTo"];

        //[[_group, _attachTo], "orange"] call MFUNC(debugMessage);

        if (_group isEqualTo GVAR(currentHoverGroup)) exitWith {};
        GVAR(currentHoverGroup) = _group;

        // -- Create UI element
        #define __CTRL(var1) (_ctrlGrp controlsGroupCtrl var1)
		private _idd = ctrlIDD (ctrlParent _map);
		private _controlName = format [QGVAR(spawnInfo_Group_%1), _idd];
        private _ctrlGrp = uiNamespace getVariable [_controlName, controlNull];
        if (isNull _ctrlGrp) then {
            _ctrlGrp = (ctrlParent _map) ctrlCreate [QSVAR(CtrlGroupMouseOver), -1];
            uiNamespace setVariable [_controlName, _ctrlGrp];
        };

        __CTRL(1) ctrlSetText toUpper groupId _group;

        if (GVAR(groupInfoPFH) != -1) then {
            GVAR(groupInfoPFH) call MFUNC(removePerFrameHandler);
        };

        GVAR(groupInfoPFH) = [{
            params ["_args", "_id"];
            _args params ["_group", "_controlName", "_map", "_attachTo"];

			private _ctrlGrp = (uiNamespace getVariable [_controlName, controlNull]);
			if (isNull _ctrlGrp) exitWith {
				[_id] call MFUNC(removePerFrameHandler);
			};

			[_group, _map, _ctrlGrp, _attachTo] call FUNC(groupMouseOverText);
        }, 0.5, [_group, _controlName, _map, _attachTo]] call MFUNC(addPerFramehandler);
    },
    [_group, _attachTo]
] call CFUNC(addMapGraphicsEventHandler);


[
    _groupIconId,
    "hoverout",
    {
        (_this select 0) params ["_map", "_xPos", "_yPos"];
        (_this select 1) params ["_group"];
        private _idd = ctrlIDD (ctrlParent _map);
        private _controlName = format [QGVAR(spawnInfo_Group_%1), _idd];

        if (GVAR(currentHoverGroup) isEqualTo _group) then {
            GVAR(currentHoverGroup) = grpNull;
        };

        if (GVAR(groupInfoPFH) != -1) then {
            GVAR(groupInfoPFH) call MFUNC(removePerFrameHandler);
            GVAR(groupInfoPFH) = -1;
        };

        private _grp = uiNamespace getVariable [_controlName, controlNull];
        if (!isNull _grp) then {
            _grp ctrlShow false;
            _grp ctrlCommit 0;
        };
    },
    _group
] call CFUNC(addMapGraphicsEventHandler);
_groupIconId;
