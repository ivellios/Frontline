/*
    Function:       FRL_UnitMarkers_fnc_handleUnit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_unit", "_group"];

#ifdef DEBUGFULL
if (alive _unit && !isObjectHidden _unit) then {
#else
if (alive _unit && !isObjectHidden _unit && { side _group == playerSide }) then {
#endif

    private _hover = _group isEqualTo GVAR(currentHoverGroup);
    private _selected = _group isEqualTo (missionNamespace getVariable [QEGVAR(Commander,selectedSquad), grpNull]);
    if (isNull objectParent _unit) then {
        private _iconId = toLower format [QGVAR(%1), _unit];
        GVAR(processedIcons) pushBack _iconId;
        [_unit, _iconId, _group, _unit call MFUNC(isUnconscious), _hover, _selected] call FUNC(addUnitMarker);
    };

    // -- Add the group marker above the leader unit
    if ([_unit] call MFUNC(isLeader)) then {
        _iconId = toLower format [QGVAR(%1), _group];
        GVAR(processedIcons) pushBack _iconId;
        [_group, _iconId, nil, _hover, _selected] call FUNC(addGroupMarker);
    };
};

true
