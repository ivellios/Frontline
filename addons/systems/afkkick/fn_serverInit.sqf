/*
    Function:       FRL_Afkkick_fnc_serverInit
    Author:         Adanteh
    Description:    Inits the settings for AFK kicking
*/
#include "macros.hpp"

if !(isDedicated) exitWith { };
private _enabled = ["afkick.enabled", true] call MFUNC(getExternalConfig);

// -- Default to false for client, so no need to broadcast
if (_enabled) then {
    GVAR(enabled) = _enabled;
    publicVariable QGVAR(enabled);

    private _kickTime = ["afkick.time", 60*15] call MFUNC(getExternalConfig);
    GVAR(time) = _kickTime;
    publicVariable QGVAR(time);
};
