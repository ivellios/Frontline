/*
    Function:       FRL_AfkKick_fnc_clientInit
    Author:         Adanteh
    Description:    Kicks you if you're afk above a certain time
*/
#include "macros.hpp"

// -- Register the event, even if the loop isn't enabled, so we can call it manually if we ever need to
[QGVAR(afkKick), {
	["kick", [], Clib_Player, "AFK For 15 minutes"] call MFUNC(useAdminOption);
}] call CFUNC(addEventHandler);

private _enabled = missionNamespace getVariable [QGVAR(enabled), false];
if !(_enabled) exitWith { false };

GVAR(lastActive) = 9e10;
GVAR(warningShown) = false;
["missionStarted", {
	(findDisplay 46) displayAddEventHandler ["KeyDown", {
		GVAR(lastActive) = time;
		nil;
	}];
}] call CFUNC(addEventHandler);

[{
	private _kickAt = (GVAR(lastActive) + GVAR(time));
	private _warningAt = _kickAt - 60;

	// -- Do a warning first on one minute
	if (time > _warningAt) then {
		if (time > _kickAt) then {
			[QGVAR(afkKick)] call CFUNC(localEvent);
			[(_this select 1)] call MFUNC(removePerFrameHandler);
		} else {
			if !(GVAR(warningShown)) then {
				hint "You are AFK and will be kicked in one minute. Press any keyboard button to cancel this";
				GVAR(warningShown) = false;
			};
		};
	} else {
		GVAR(warningShown) = false;
	};

}, 10, []] call MFUNC(addPerFramehandler);
