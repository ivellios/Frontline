/*
    Function:       FRL_Prep_fnc_setPausedPrep
    Author:         Adanteh
    Description:    Pauses the battleprep
*/
#include "macros.hpp"

params [["_pause", true]];

GVAR(paused) = _pause;
publicVariable QGVAR(paused);

if (_pause) then {
    ["showNotification", ["Battle Prep is paused.", MEDIAPATH + "icons\battleprep.paa"]] call CFUNC(globalEvent);
} else {
    ["showNotification", ["Battle Prep resumed.", MEDIAPATH + "icons\battleprep.paa"]] call CFUNC(globalEvent);
};
