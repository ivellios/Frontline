#include "macros.hpp"
#define __WIDTH (GRID_W * 100)
#define __HEIGHT (GRID_H * 12.5)


class GVAR(prepIndicator) {
    idd = -1;
    duration = 1e11;
    fadeIn = 0;
    fadeOut = 0;
    movingEnable = false;
    enableSimulation = 1;
    enableDisplay = 1;
    onLoad = "uiNamespace setVariable ['frl_prep_prepIndicator', _this select 0]";

    class ControlsBackground {
        class ctrlGroup: ctrlControlsGroupNoScrollbars {
            x = safeZoneX + (safeZoneW * 0.5) - (__WIDTH / 2);
            y = (safeZoneY + (safeZoneH * 0.04));
            w = __WIDTH;
            h = __HEIGHT;

            class Controls {
                class Background : RscBackground {
                    moving = 0;
                    colorBackground[] = {0,0,0,0.7};

                    x = 0;
                    y = 0;
                    w = __WIDTH;
                    h = __HEIGHT;
                };

                class ProgressBackground : Background {
                    x = GRID_W * 1;
                    y = GRID_H * 1;
                    w = __WIDTH - (GRID_W * 2);
                    h = __HEIGHT - (GRID_H * 2);
                };

                class Progress : ProgressBackground {
                    idc = 1;
                    colorBackground[] = {1,1,1,0};
                };

                class ProgressText : RscText {
                    idc = 2;
                    style = 2;
                    text = "";
                    x = GRID_W * 1;
                    y = GRID_H * 1;
                    w = __WIDTH - (GRID_W * 2);
                    h = __HEIGHT - (GRID_H * 2);
                };
            };

        };

    };
};
