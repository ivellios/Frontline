/*
    Function:       FRL_Prep_fnc_setGameStarted[functionName]
    Author:         Adanteh
    Description:    Sets the battleprep as started
    Locality:       Anywhere
*/
#include "macros.hpp"

params [["_startGame", true], ["_broadcast", false]];

GVAR(ended) = _startGame;
GVAR(endTimeAt) = serverTime;
publicVariable QGVAR(endTimeAt);
publicVariable QGVAR(ended);

private _event = ["gamePrep", "gameStart"] select _startGame;
[_event, [GVAR(endTimeAt)]] call MFUNC(persistentEventGlobal);

["showNotification", ["The round has started", MEDIAPATH + "icons\battleprep.paa"]] call CFUNC(globalEvent);
