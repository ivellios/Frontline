/*
    Function:       FRL_prep_fnc_freezeTimeWait
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

[{
    _this call EFUNC(prep,showFreezetime)
}, { EGVAR(prep,ended) }, _this] call CFUNC(waitUntil);
