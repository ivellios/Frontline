/*
    Function:       FRL_Prep_fnc_getPrepDuration
    Author:         Adanteh
    Description:    Gets duration of battleprep
*/
#include "macros.hpp"

#ifdef DEBUGFULL
    if (true) exitWith {20};
#endif

[QGVAR(settings_duration), 240] call MFUNC(cfgSetting);
