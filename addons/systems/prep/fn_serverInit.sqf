/*
    Function:       FRL_Prep_fnc_serverInit
    Author:         Adanteh
    Description:    Runs battleprep on the server
*/
#include "macros.hpp"

private _enabled = [QGVAR(Settings_enabled), 1] call MFUNC(cfgSetting);
if (_enabled < 1) exitWith { };

if !(isMultiplayer) exitWith {
    [true] call FUNC(setGameStarted);
};

["missionStarted", {
    [false] call FUNC(setGameStarted);

    private _prepDuration = [] call FUNC(getPrepDuration);
    GVAR(timeEndAt) = serverTime + _prepDuration;
    publicVariable QGVAR(timeEndAt);

    [{
        private _gameStart = false;
        private _prepDurationLeft = (GVAR(timeEndAt) - serverTime);

        if ([] call FUNC(getPausedPrep)) then {
            GVAR(timeEndAt) = serverTime + (_prepDurationLeft + 1);
            publicVariable QGVAR(timeEndAt);
        } else {
            if (_prepDurationLeft <= 0) then {
                _gameStart = true;
            };
        };

        if ([false] call FUNC(isGameStarted)) then {
            _gameStart = true;
        };

        if (_gameStart) then {
            [true] call FUNC(setGameStarted);
            [_this select 1] call MFUNC(removePerFrameHandler);
        };
    }, 1, []] call MFUNC(addPerFramehandler);
}] call CFUNC(addEventHandler);
