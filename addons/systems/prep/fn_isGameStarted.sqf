/*
    Function:       FRL_Prep_fnc_isGameStarted
    Author:         Adanteh
    Description:    Checks if game has started already (Battle prep ended)
    Example:        [false] call FRL_Prep_fnc_isGameStarted
*/
#include "macros.hpp"

params [["_default", false]];
(missionNamespace getVariable [QGVAR(ended), _default]);
