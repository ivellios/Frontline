class prep {
    defaultLoad = 0;
    path = PATHTO(prep);
    dependencies[] = {"Main"};

    class clientInit;
    class clientInitAdmin;
    class serverInit;
    class getPausedPrep;
    class getPrepDuration;
    class initPrep;
    class isGameStarted;
    class prepCheckLoop;
    class setGameStarted;
    class setPausedPrep;
};
