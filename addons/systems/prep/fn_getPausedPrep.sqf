/*
    Function:       FRL_Prep_fnc_getPausedPrep
    Author:         Adanteh
    Description:    Checks whether battle prep is paused
*/
#include "macros.hpp"

params [["_default", false]];
(missionNamespace getVariable [QGVAR(paused), _default]);
