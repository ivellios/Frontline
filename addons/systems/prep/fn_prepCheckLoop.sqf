/*
    Function:       FRL_Prep_fnc_prepCheckLoop
    Author:         Adanteh
    Description:    Updates timer, check if unit is leaving base and so on
*/
#include "macros.hpp"
#define CTRL(x) ((uiNamespace getVariable QGVAR(prepIndicator)) displayCtrl x)

disableSerialization;

if ([false] call FUNC(isGameStarted)) exitWith {
    [_this select 1] call MFUNC(removePerFrameHandler);
    QGVAR(uiLayer) cutText ["", "PLAIN"];
    GVAR(prepWarningTime) = nil;
    GVAR(prepPosReset) = nil;
};

// -- Reopen the hud if it was closed for some reason
if (isNull (uiNamespace getVariable QGVAR(prepIndicator))) then {
    QGVAR(uiLayer) cutRsc [QGVAR(prepIndicator), "plain", 0, true];
};

private _prepDuration = [] call FUNC(getPrepDuration);
private _prepDurationLeft = (GVAR(timeEndAt) - serverTime);
private _barText = format ["Battleprep remaining: %1", ([(_prepDurationLeft max 0), "MM:SS", false] call BIS_fnc_secondsToString)];

private _canLeaveBaseText = "";
private _canLeaveBase = ["battleprep_canLeaveBase", [], false] call MFUNC(checkConditions);
if (_canLeaveBaseText != "") then {
    _barText = format ["%1 [%2]", _barText, _canLeaveBaseText];
};

if ([] call FUNC(getPausedPrep)) then {
    _barText = format ["%1 (Paused)", _barText];
};

if (!_canLeaveBase && {alive Clib_Player && !isObjectHidden Clib_Player}) then {
    private _inBase = [CLib_Player] call MFUNC(sectorInBase);
    if !(_inBase) then {
        if (local (vehicle Clib_Player)) then { // ## Set vehicle position
            (vehicle CLib_Player) setPosATL GVAR(prepPosReset);
        };
        CTRL(1) ctrlSetBackgroundColor [1, 0.2, 0.2, 1];
        CTRL(2) ctrlSetText "DON'T LEAVE BASE";

        // -- Make sure it doesn't queue up a million notificationss -- //
        if (GVAR(prepWarningTime) < time) then {
            ["showNotification", ["Don't leave the base during battleprep", MEDIAPATH + "icons\battleprep.paa"]] call CFUNC(localEvent);
            GVAR(prepWarningTime) = time + 5;
        };
    } else {
        GVAR(prepPosReset) = getPosATL vehicle CLib_Player;
    };
};

if (GVAR(prepWarningTime) < time) then {
    CTRL(1) ctrlSetBackgroundColor [0.5, 0.5, 0.5, 0.8];
    CTRL(2) ctrlSetText _barText;
};
