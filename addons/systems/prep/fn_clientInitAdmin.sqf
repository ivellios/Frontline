/*
    Function:       FRL_Prep_fnc_clientInitAdmin
    Author:         Adanteh
    Description:    Creates admin and debug options for battleprep
*/
#include "macros.hpp"

['add', ["admin", 'Admin Settings', 'Battleprep', [
    'End',
    'End the battleprep',
    QMVAR(CtrlButton),
    { true },
    { (([CLib_Player] call MFUNC(isAdmin)) && {!([false] call FUNC(isGameStarted))}); },
    (format ["[true] call %1; false;", QFUNC(setGameStarted)]),
    ["END"]
]]] call MFUNC(settingsWindow);

// -- Volume levels -- //
['add', ["admin", 'Admin Settings', 'Battleprep', [
    'Pause',
    'Toggles battleprep pause',
    QMVAR(CtrlButton),
    { true },
    { (([CLib_Player] call MFUNC(isAdmin)) && {!([false] call FUNC(isGameStarted))}); },
    (format ["[!([false] call %1)] call %2; false;", QFUNC(getPausedPrep), QFUNC(setPausedPrep)]),
    ["PAUSE TOGGLE"]
]]] call MFUNC(settingsWindow);
