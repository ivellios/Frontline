/*
    Function:       FRL_Prep_fnc_initPrep
    Author:         Adanteh
    Description:    Inits some stuff.
*/
#include "macros.hpp"

[QGVAR(Settings), configFile >> "FRL" >> "CfgBattleprep"] call MFUNC(cfgSettingLoad);
[QGVAR(Settings), missionConfigFile >> "FRL" >> "CfgBattleprep"] call MFUNC(cfgSettingLoad);

if (isServer) then {
    private _enabled = ([QGVAR(Settings_enabled), false] call MFUNC(cfgSetting)) > 0;
    if !(_enabled) exitWith { };

    ["vehicleLoad", {
        // params ["_vehicle"];
        private _prepDuration = [] call FUNC(getPrepDuration);
        if (_initialSpawnTime > -1) then {
            _initialSpawnTime = _initialSpawnTime + (_prepDuration / 60);
        };
        true;
    }] call MFUNC(addCondition);
};

// -- Prevent spawning on forward spawn if game didnt start yet
["forward_canSpawn", {
    if ([false] call FUNC(isGameStarted)) exitWith { true };

    params ["_spawnpoint", "_position", "_side"];
    private _canLeaveBase = ["battleprep_canLeaveBase", [_side], false] call MFUNC(checkConditions);
    if (_canLeaveBase) exitWith { true };

    // -- Returns message when you click spawn
    _returnMessage = format ["You can't deploy here during battleprep<br />%1 Left",
        [((GVAR(timeEndAt) - serverTime) max 0), "MM:SS", false] call BIS_fnc_secondsToString
    ];
    //_spawnblock_return = "Battleprep";
    false

}] call MFUNC(addCondition);

["isGameStarted", {
    [false] call FUNC(isGameStarted);
}] call MFUNC(addCondition);
