/*
    Function:       FRL_Prep_fnc_clientInit[functionName]
    Author:         Adanteh
    Description:    Runs stuff on client for battleprep
*/
#include "macros.hpp"

private _enabled = [QGVAR(Settings_enabled), 1] call MFUNC(cfgSetting);
if (_enabled < 1) exitWith { };

["missionStarted", {
    if ([false] call FUNC(isGameStarted)) exitWith { };

    QGVAR(uiLayer) cutRsc [QGVAR(prepIndicator), "plain", 0, true];
    GVAR(prepWarningTime) = 0;
    GVAR(prepPosReset) = getPosATL Clib_Player;

    [{ _this call FUNC(prepCheckLoop) }, 0.99, []] call MFUNC(addPerFramehandler);
}] call CFUNC(addEventHandler);
