#include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Creates FPS counter in top right corner.
 *
 */

#define __FPS_HUD uiNamespace getVariable QSVAR(fpsCounter)
#define __FPS_IDC 1

params [["_mode", "init"]];
// -- Init vars and load profile setting. Default = on -- //
switch (toLower _mode) do {
	// -- Toggle and save to profile -- //
	case "toggle": {
		GVAR(counterToggle) = !GVAR(counterToggle);
		profileNamespace setVariable [QGVAR(counterToggle), GVAR(counterToggle)];
	};
	case "init";
	default {
		GVAR(counterPFH) = -1;
		GVAR(counterToggle) = profileNamespace getVariable [QGVAR(counterToggle), true];

		// -- Add settings to the main menu bar -- //
		['add', ["gui", 'Frontline Settings', 'FPS HUD', [
			'Show FPS',
			'Shows the current FPS in top right corner',
			QMVAR(CtrlCheckbox),
			{ GVAR(counterToggle) },
			true,
			{	["toggle"] call FUNC(clientInit); },
			[]
		]]] call MFUNC(settingsWindow);
	};
};

// -- Show or hide hud -- //
if (GVAR(counterToggle)) then {
	QGVAR(counterLayer) cutRsc [QSVAR(fpsCounter), "PLAIN", 0];
	GVAR(counterPFH) = [{ (__FPS_HUD displayCtrl __FPS_IDC) ctrlSetText (str round diag_fps); }, 1, []] call MFUNC(addPerFramehandler);
} else {
	if (GVAR(counterPFH) != -1) then {
		[GVAR(counterPFH)] call MFUNC(removePerFrameHandler);
		GVAR(counterPFH) = -1;
		QGVAR(counterLayer) cutText ["", "PLAIN"];
	};
};
