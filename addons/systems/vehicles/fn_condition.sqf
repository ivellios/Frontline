/*
    Function:       FRL_Vehicle_fnc_condition
    Author:         Adanteh
    Description:    Wrapper for common vehicle conditions to check. Uses caching
    Example:        this setVariable ["condition", "[['minplayers', 15]] call frl_vehicles_fnc_condition"];
*/
#include "macros.hpp"

private _conditions = _this;
if ((_conditions param [0, ""]) isEqualType "") then {
    _conditions = [_this];
};

private _return = true;
private _returnText = [];
private _returnTextUsage = !(isNil "_conditionTextReturn");
{
    _x params ["_condition", "_args"];
    if !(switch (toLower _condition) do {
        // -- Minimum players needed for vehicle (Takes smallest team)
        case "minplayers": {
            private _count = [QGVAR(condition_minPlayers), { [false] call MFUNC(getSideSmallest) }, true, 4] call CFUNC(cachedCall);
            if (_returnTextUsage) then {
                _returnText pushBack format ["Minimum players: %2 / %1<br /><t size='0.8'>(per side)</t>", _args, _count];
                true
            } else {
                (_count >= _args)
            };
        };

        // -- Maximum players allowed for vehicle (Takes biggest team)
        case "maxplayers": {
            private _count = [QGVAR(condition_maxPlayers), { [false] call MFUNC(getSideLargest) }, true, 4] call CFUNC(cachedCall);
            if (_returnTextUsage) then {
                _returnText pushBack format ["Maximum players: %2 / %1<br /><t size='0.8'>(per side)</t>", _args, _count];
                true
            } else {
                (_count >= _args)
            };
        };

        default { true };
    }) exitWith { _return = false; nil };
    nil;
} count _conditions;
[_return, _returnText] select _returnTextUsage;
