/*
    Function:       FRL_Vehicles_fnc_getCrewmanRequired
    Author:         Adanteh
    Description:    Checks if given vehicle type needs crewman
    Example:		[cursorObject] call frl_vehicles_fnc_getCrewmanRequired
*/
#include "macros.hpp"

params [["_vehicle", "", ["",objnull]]];
if (_vehicle isEqualType objNull) then {
    _vehicle = typeOf _vehicle;
};

private _requiresCrewman = GVAR(vehicleNamespace) getVariable (_vehicle + "_needsCrewman");
if !(isNil "_requiresCrewman") exitWith { _requiresCrewman };

private _vehicleCategory = ([_vehicle] call FUNC(getVehicleType) select 1);
_requiresCrewman = switch (toLower _vehicleCategory) do {
    case "tank";
    case "mbt";
    case "armour";

    case "wheeledifv";
    case "trackedapc";
    case "ifv": { true };

    case "armored_car": { true };

    case "vehicleautonomous": { false };

    case "jet";
    case "plane";
    case "plane_heavytransport": { false };
    case "plane_recon": { true };

    case "attackhelicopter";

    case "heli_lighttransport";
    case "heli_mediumtransport";
    case "heli_heavytransport";
    case "helicopter": { true };
    case "heli_mediumtransport_armed": { false };

    case "apc";
    case "halftrack";
    case "armedmrap": { false };

    case "mrap";
    case "truck";
    case "armed_car": { false };

    case "motorcycle";
    case "quadbike";
    case "car": { false };

    case "ship";
    case "submarine";
    case "boat": { false };
    case "lcvp": { false };

    case "staticweapon";//
    case "static": { false };
    default {
        diag_log format ["[FRL Warning] Vehicle type: '%1' doesn't have a preset respawn time (Category: '%2')", _vehicleType, _vehicleCategory];
        false
    };
};

if (_requiresCrewman) then {
    {
        if (_vehicle isKindOf _x) exitWith {
            _requiresCrewman = false;
            nil;
        };
        nil;
    } count GVAR(noCrewmanList);
};

GVAR(vehicleNamespace) setVariable [(_vehicle + "_needsCrewman"), _requiresCrewman];
_requiresCrewman
