#include "macros.hpp"
/*
 *	File: fn_init.sqf
 *	Author: Adanteh
 *	Init for client and server (And HC)
 *
 *	Example:
 *	[player] call FUNC(init);
 */


GVAR(vehicleNamespace) = call CFUNC(createNamespace);
GVAR(allVehiclesSide) = [[], [], [], []];
if (isNil QGVAR(vehicleQueue)) then { GVAR(vehicleQueue) = [[], [], [], []]; };

["vehicleInit", {
	(_this select 0) params ["_vehicle", "_side", "_varName"];
	private _sideID = (_side call BIS_fnc_sideID);
	if (_sideID <= 3) then {
		(GVAR(allVehiclesSide) select _sideID) pushBack [_vehicle, _varName];
	};
}] call CFUNC(addEventHandler);

["engineOn", {
	(_this select 0) params ["_vehicle", ["_state", true]];
	_vehicle engineOn _state;
}] call CFUNC(addEventHandler);
