#include "macros.hpp"
/*
 *	File: fn_vehicleLoad.sqf
 *	Author: Adanteh
 *	Loads details onto a vehicle, but setting everything as vars if these are not yet set.
 *	This is only used for first vehicle init
 *
 *	Locality: Server only
 *
 */

params ["_vehicle", "_index"];

private _vehicleType = typeOf _vehicle;
([_vehicleType] call FUNC(getRespawnTime)) params [["_respawnTime", 5], ["_despawnRadius", 5], ["_despawnTime", 5], ["_initialSpawnTime", 0]];

_respawnTime = _respawnTime * 60;
_despawnTime = _despawnTime * 60;

// -- Respawn time in minutes -- //
if !(isNil { (_vehicle getVariable "respawnTime") }) then {
	_respawnTime = (_vehicle getVariable "respawnTime");
};

// -- Automatic side detection, by checking if it's a base
private _side = if (isNil { (_vehicle getVariable "side") }) then {
	private _sideDetect = sideUnknown;
	{
		if (_vehicle inArea (format ["base_%1", _x])) exitWith {
			_sideDetect = _x;
		};
	} forEach [west, east, resistance];
	if (_sideDetect isEqualTo sideUnknown) then {
		_sideDetect = [_vehicle] call FUNC(getVehicleSide);
	};
	_vehicle setVariable ["side", _sideDetect, true];
	_sideDetect
} else {
	_vehicle getVariable "side";
};

if !(isNil { (_vehicle getVariable "despawnRadius") }) then {
	_despawnRadius = (_vehicle getVariable "despawnRadius");
};

if !(isNil { (_vehicle getVariable "despawnTime") }) then {
	_despawnTime = (_vehicle getVariable "despawnTime");
};

private _varName = vehicleVarName _vehicle;
if (_varName == "") then {
	_varName = (format [QGVAR(%1), _index]);
};
_vehicle setVariable ["varName", _varName, true];

private _condition = if !(isNil { (_vehicle getVariable "condition") }) then {
	(_vehicle getVariable "condition");
} else {
	"true";
};

private _conditionCompiled = if (_condition isEqualType "") then { compile _condition; } else { _condition };
#ifdef DEBUGFULL
	_initialSpawnTime = 0;
	_conditionCompiled = { true };
	_respawnTime = 10;
#endif

// -- Keep a list of additional variables that we want to reassign on respawn
private _vars = [];
{
    private _value = _vehicle getVariable _x;
    if !(isNil "_value") then {
        _vars pushBack [_x, _value];
    };
} forEach [QSVAR(aoBypass)];

// Runs some extra code, if you don't use privates in these code blocks, then you can change presets, make sure that the conditions are added in INIT (not client/serverInit!!)
["vehicleLoad", [_vehicle], true] call MFUNC(checkConditions);

// -- Keep a local list of all vehicle var names and which details to apply to them -- //
GVAR(vehicleDetailNamespace) setVariable [_varName, [_vehicleType, _side, _respawnTime, _despawnTime, _despawnRadius, getPosATL _vehicle, getDir _vehicle, _conditionCompiled, _vars], true];


// -- Delete the vehicle if we want a delayed spawn on it, and/or original spawn condition is not matched
private _conditionMatched = [_vehicleType, _varName] call _conditionCompiled;
if ((_initialSpawnTime > 0) || !_conditionMatched) exitWith {
	GVAR(vehicleDetailNamespace) setVariable [(_varName + "_respawnAt"), (_initialSpawnTime * 60 + serverTime), true];
	[FUNC(performVehicleRespawn), (_initialSpawnTime * 60), [objNull, _vehicleType, _varname]] call CFUNC(wait);
	deleteVehicle _vehicle;

	private _sideID = _side call BIS_fnc_sideID;
	if (_sideID <= 3) then {
		(GVAR(vehicleQueue) select _sideID) pushBackUnique _varName;
	};
};

// -- Clear all cargo -- //
clearItemCargoGlobal _vehicle;
clearMagazineCargoGlobal _vehicle;
clearWeaponCargoGlobal _vehicle;
clearBackpackCargoGlobal _vehicle;
_vehicle disableTIEquipment true;

if !(_vehicle getVariable [QGVAR(init), false]) then {
	_vehicle setVariable [QGVAR(init), true];
	["vehicleInit", [_vehicle, _side, _varName]] call CFUNC(localEvent);
};
