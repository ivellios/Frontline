#include "macros.hpp"
/*
 *	File: fn_clientInitLock.sqf
 *	Author: Adanteh
 *	Initializes the side/squad locking system for vehicles
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(clientInitLock);
 */


// -- Only use the vehicles that are actually loaded as a mod. This means some quicker processing if we use different content -- //
private _canSoloList = configProperties [(configFile >> "FRL" >> "CfgVehicles"), ("getNumber (_x >> ""canSoloCrew"") > 0"), false];
private _canSoloListProcessed = [];
{
    if (isClass (configFile >> "cfgVehicles" >> (configName _x))) then {
        _canSoloListProcessed pushBack (configName _x);
    };
    nil;
} count _canSoloList;
GVAR(canSoloList) = _canSoloListProcessed;

// -- Prevent soloing seats gunner seat without someone else already in driver (Or another turret seat) -- //
["crewSeatEntered", {
    (_this select 0) params ["_vehicle", "_crewSeat"];
    if (_crewSeat in ["turret", "gunner"]) then {
        private _requiresCrewman = [_vehicle] call FUNC(getCrewmanRequired);
        if !(_requiresCrewman) exitWith { }; // -- These will never require a full crew either

        private _otherCrew = (fullCrew _vehicle) select { !((_x select 0) isEqualTo Clib_Player) };
        _canUse = ({ (toLower (_x select 1) in ["driver", "commander", "gunner"]) } count _otherCrew) > 0;
        #ifdef DEBUGFULL
            _canUse = true;
            [["crewSeatEntered", "Bypass solo prevention"], "orange"] call MFUNC(debugMessage);
        #endif
        if !(_canUse) then {
            private _canSolo = false;
            {
                if (_vehicle isKindOf _x) exitWith {
                    _canSolo = true;
                    nil;
                };
                nil;
            } count GVAR(canSoloList);
            if (_canSolo) exitWith { };

            if (count allPlayers <= 8) exitWith {
                ["debugMessage", ["Bypassing vehicle solo prevention due to low player number", "orange", nil, -1]] call MFUNC(localEvent);
            };

            moveOut CLib_Player;
            ["You need a driver / commander to use this seat!"] call MFUNC(notificationShow);
        };
    };
}] call CFUNC(addEventHandler);


// -- Side lock -- //
["vehicleChanged", {
    (_this select 0) params ["_newVehicle", "_oldVehicle"];
    if (_newVehicle == CLib_Player) exitWith { };
    private _vehicle = vehicle CLib_Player;
    private _side = _vehicle getVariable ["side", sideLogic];
    if (_vehicle getVariable [QSVAR(bypassSide), false]) exitWith { };
    if (_side != sideLogic && {(_side != playerSide)}) then {
        moveOut CLib_Player;
        ["You're not allowed to use enemy vehicles"] call MFUNC(notificationShow);
    };
}] call CFUNC(addEventHandler);


// -- Used for interact menu, prevents you from getting in if vehicle is wrong side
["canEnterVehicle", {
    (_this select 0) params ["_unit", "_vehicle", ["_silent", true]];
    private _side = _vehicle getVariable ["side", sideLogic];
    private _return = (_side == sideLogic || {(_side == playerSide)});
    if (!_silent && !_return) then {
        ["showNotification", ["You're not allowed to use enemy vehicles", "nope"]] call CFUNC(localEvent);
    };
    _return
}] call MFUNC(addCondition);
