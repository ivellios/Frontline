/*
    Function:       FRL_Vehicles_fnc_vehicleFlipAction
    Author:         Adanteh
    Description:    Unflips vehicles
*/
#include "macros.hpp"

params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {
    //
    //
    // Called every frame while the hold action button is still pressed, return should be true to keep action going, false to exit it
    case "callback": {
        _args params ["_caller", "_target", "_menuTarget"];
        if !(_target isEqualTo (call _menuTarget)) exitWith { // -- Not aiming at vehicle anymore
            _return = false;
        };

        _return = (alive _caller && { !([_caller] call MFUNC(isUnconscious)) })
    };



    //
    //
    // Condition to grey out the interact menu option
    case "condition": {
        _args params ["_caller", "_target", "_menuTarget"];

        if !(alive _target) exitWith {
            ["showNotification", ["Can't unflip destroyed vehicles", "nope"]] call CFUNC(localEvent);
            _return = false;
        };

        if (1 > (vectorUp _target) vectorDistance (surfaceNormal getPosATL _target)) exitWith {
            ["showNotification", ["Can't unflip vehicles that aren't flipped", "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };


    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_target", "_targetRealtime"];
        private _duration = 15;
        #ifdef DEBUGFULL
            _duration = _duration / 10;
        #endif

        [
            "Packing",
            nil, // UNFLIP ICON PLS
            _duration,
            [_caller, _target, _targetRealtime],
            { ["condition", _this] call FUNC(vehicleFlipAction) },
            { ["callback", _this] call FUNC(vehicleFlipAction) },
            { ["end", _this] call FUNC(vehicleFlipAction) },
            true,
            true,
            10,
            _dikButton // -- This is passed from menuUseActionKey
        ] call MFUNC(holdActionStart);
    };


    //
    //
    // Called when we press the button from the interact menu
    case "end": {
        _args params ["_data", "_exitCode"];
        if (_exitCode isEqualTo 0) then {
            _data params ["_caller", "_target", "_menuTarget"];
            ["setVectorUp", _target, [_target, surfaceNormal getPosATL _target]] call CFUNC(targetEvent);
            private _posATL = getPosATL _target;
            if (_posATL select 2 < 0) then {
                _posATL set [2, 0.5];
                _target setPosATL _posATL;
            };
        };
    };
};

_return;
