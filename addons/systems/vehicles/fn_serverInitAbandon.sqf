#include "macros.hpp"

/*
 *    File: fn_serverInitAbandon.sqf
 *    Author: Adanteh
 *    Basic vehicle abandon. Only checks friendly units to cancel despawn.
 *
 */

GVAR(abandonSM) = call CFUNC(createStatemachine);

[GVAR(abandonSM), "init", {
    private _vehicles = +(GVAR(allVehicles));
    [["checkVehicles", _vehicles], "init"] select (_vehicles isEqualTo []);
}] call CFUNC(addStatemachineState);

[GVAR(abandonSM), "checkVehicles", {
    params ["_dummy", "_vehicles"];
    private _vehicle = _vehicles deleteAt 0;

    while {isNull _vehicle && {!(_vehicles isEqualTo [])}} do {
        _vehicle = _vehicles deleteAt 0;
    };

    private _defaultState = [["checkVehicles", _vehicles], "init"] select (_vehicles isEqualTo []);

    if !(isNull _vehicle) then {

        // -- Dont respawn if vehicle got crew -- //
        private _aliveCrew = { !([_x] call MFUNC(isDead)) } count (crew _vehicle);
        if (_aliveCrew > 0) exitWith {
            _vehicle setVariable ["lastUsed", -1];
        };

        private _varName = _vehicle getVariable ["varname", "##"];
        (GVAR(vehicleDetailNamespace) getVariable _varName) params ["_type", "_side", "_respawnTime", ["_despawnTime", 1e9], "_despawnRadius", "_position", "_direction", "_condition"];
        if (_despawnTime == 1e9) exitWith { };

        // -- Don't despawn if vehicle barely moved from start pos -- //

        if ((_position distance (getPosATL _vehicle)) < 30) exitWith {
            _vehicle setVariable ["lastUsed", -1];
        };

        // -- Check units that are friendly to the vehicle only -- //
        private _friendlyNearby = { (side group _x == _side) && { _x distance _vehicle < _despawnRadius } && { !([_x] call MFUNC(isDead)) } } count allPlayers;
        if (_friendlyNearby > 0) exitWith {
            _vehicle setVariable ["lastUsed", -1];
        };

        _lastUsed = _vehicle getVariable ["lastUsed", -1];
        if (_lastUsed < 0) then {
            _vehicle setVariable ["lastUsed", diag_tickTime];
        } else {
            if (isEngineOn _vehicle) then { // -- Do slower vehicle despawn if engine is on
                _despawnTime = _despawnTime * 4;
            };
            if ((diag_tickTime - _lastUsed) >= _despawnTime) then {
                if (_respawnTime >= 0) then {
                    GVAR(allVehicles) deleteAt (GVAR(allVehicles) find _vehicle);
                    [FUNC(performVehicleRespawn), _respawnTime, [objNull, _type, _varName]] call CFUNC(wait);
                    deleteVehicle _vehicle;
                };
            };
        };
    };
    _defaultState;
}] call CFUNC(addStatemachineState);

[GVAR(abandonSM), "init"] call CFUNC(startStateMachine);
