#include "macros.hpp"
/*
    Function:       FRL_Vehicles_fnc_clientInit
    Author:         Adanteh
    Description:    Client init for vehicles
*/

GVAR(inCrewSeat) = false;
GVAR(crewSeatArray) = [objNull, []];
GVAR(crewDetails) = [objNull, ""];
GVAR(allVehicles) = [];

["vehicleInit", {
	(_this select 0) params ["_vehicle"];
	GVAR(allVehicles) pushBackUnique _vehicle;
	GVAR(allVehicles) = GVAR(allVehicles) - [objNull];
}] call CFUNC(addEventHandler);

// -- Run through the existing vehicles (used for JIP and similar) -- //
["MissionStarted", {
	{
		if !(_x getVariable [QGVAR(init), false]) then {
			_x setVariable [QGVAR(init), true];
			private _side = _x getVariable ["side", sideUnknown];
			private _varName = _x getVariable ["varName", vehicleVarName _x];
			["vehicleInit", [_x, _side, _varName]] call CFUNC(localEvent);
		};
		nil;
	} count ((entities "LandVehicle") + (entities "Air") + (entities "Ship"));
}] call CFUNC(addEventHandler);

// -- This simulates vehicle role change when you actually enter the vehicle (Arma kek)
// -- Normally no way to get the full info on which seat you are in, till a second after your role actually changes
["vehicleChanged", {
	(_this select 0) params ["_newVehicle"];
	if !((GVAR(crewSeatArray) select 0) isEqualTo _newVehicle) then {
		["assignedVehicleRoleChanged", [(GVAR(crewSeatArray) select 1), (GVAR(crewSeatArray) select 1)]] call CFUNC(localEvent);
	};
}] call CFUNC(addEventHandler);

["assignedVehicleRoleChanged", {
    (_this select 0) params ["_newVehicleRole", "_oldVehicleRole"];
	private _vehicle = vehicle Clib_Player;
	private _newPosition = "";
	if !(GVAR(crewSeatArray) isEqualTo [_vehicle, _newVehicleRole]) then {
		GVAR(crewSeatArray) = [_vehicle, _newVehicleRole];
	};

	private _isFFVseat = false;
	{
		if ((_x select 0) isEqualTo CLib_Player) exitWith {
			_newPosition = toLower (_x select 1);
			_isFFVseat = _x param [4, false];
			nil;
		};
		nil;
	} count +(fullCrew _vehicle);
	if ((_newPosition in ["turret", "driver", "commander", "gunner"]) && !_isFFVseat) then {
		//if !(GVAR(inCrewSeat)) then {
			GVAR(inCrewSeat) = true;
			GVAR(crewDetails) = [_vehicle, _newPosition];
			["crewSeatEntered", GVAR(crewDetails)] call CFUNC(localEvent);
		//};
	} else {
		if (GVAR(inCrewSeat)) then {
			GVAR(inCrewSeat) = false;
			["crewSeatLeft", GVAR(crewDetails)] call CFUNC(localEvent);
			GVAR(crewDetails) = [objNull, ""];
		};
	};
}] call CFUNC(addEventHandler);



if !(isNil QEFUNC(interactThreeD,menuOptionAdd)) then {
    ["vehicle", [
        "",
        "Unflip",
        { ["start", _this] call FUNC(vehicleFlipAction) },
        nil,
        nil,
        "advanced"
    ]] call EFUNC(interactThreeD,menuOptionAdd);
};
