/*
    Function:       FRL_Vehicles_fnc_markeraddIcon
    Author:         Adanteh
    Description:    Map icon for vehicle
*/
#include "macros.hpp"
params [["_vehicle", objNull], "_varName"];

private _vehicleCfg = configFile >> "cfgVehicles" >> (typeOf _vehicle);
private _size = ((getNumber (_vehicleCfg >> "mapSize")) *  0.075) / GVAR(worldSizeCoef);
private _icon = getText (_vehicleCfg >> "icon");
if (isText (configFile >> "cfgVehicleIcons" >> _icon)) then {
    _icon = getText (configFile >> "cfgVehicleIcons" >> _icon);
};

private _code = {
	_width = (_height * (10^(abs log _mapScale))) max 18;
	_height = _width;
};

[_varName, [["ICON", _icon, [0.2, 0.2, 0.2, 0.7], _vehicle, _size, _size, _vehicle, "", 0, 0.05, "PuristaMedium", "right", _code]], "normal"] call CFUNC(addMapGraphicsGroup);
[_varName] call FUNC(markerAddMouseover);
GVAR(markersSide) pushBackUnique [_varName, _size, _icon, _vehicle];
GVAR(markersAll) pushBackUnique _varName;
