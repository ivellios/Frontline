/*
    Function:       FRL_Vehicles_fnc_markerRemove
    Author:         Adanteh
    Description:    Map icon for vehicle
*/
#include "macros.hpp"

params ["_varName"];
[_varName] call CFUNC(removeMapGraphicsGroup);
GVAR(markersAll) deleteAt (GVAR(markersAll) find _varName);
