/*
    Function:       FRL_Vehicles_fnc_markerMouseOverText
    Author:         Adanteh
    Description:    Gets the text for mouseover
*/
#include "macros.hpp"
#define __CTRL(var1) (_ctrlGrp controlsGroupCtrl var1)

params ["_varName", "_map", "_ctrlGrp"];

private _vehicle = missionNamespace getVariable [_varName, objNull];
private _textToShow = "";
private "_vehiclePos";

if (!(isNull _vehicle) && { alive _vehicle}) then {

    private _units = (crew _vehicle) select { ISFRIEND(_x) };
    private _text = "";
    private _crewCount = {
        private _kitIcon = _x getVariable [QSVAR(rkitIcon), ""];
        private _name = format ["<img size='0.7' color='#ffffff' image='%1'/> %2<br />", _kitIcon, [_x] call CFUNC(name)];
        if (_name != "Error: No unit") then {
            _text = _text + _name;
        };
        true;
    } count _units;

    _textToShow = format ["<t>%1</t>", _text];
    _vehiclePos = getPosATL _vehicle;
    __CTRL(4) ctrlSetStructuredText parseText (format ["<t align='right'>%1 / %2</t>", _crewCount, [typeOf _vehicle, true] call BIS_fnc_crewCount]);

} else {
    (GVAR(vehicleDetailNamespace) getVariable _varName) params ["_type", "_side", ["_respawnTime", -1], "_despawnTime", "_despawnRadius", "_position", "_direction", "_spawnCondition"];
    private _respawnAt = (GVAR(vehicleDetailNamespace) getVariable (_varName + "_respawnAt"));
    private _tillRespawn = (_respawnAt - serverTime) max 0;
    private _respawnTime = format ["Respawn in: %1", [_tillRespawn, "MM:SS"] call bis_fnc_secondsToString];
    private _conditionTextReturn = true;
    private _spawnConditionText = call _spawnCondition;
    _vehiclePos = _position;
    _textToShow = _textToShow + _respawnTime + "<br />";
    if (_spawnConditionText isEqualType []) then {
        {
            _textToShow = _textToShow + _x + "<br />";
            nil;
        } count _spawnConditionText;
    };
};

// -- We need the text here, so we can adjust the box size
__CTRL(3) ctrlSetStructuredText parseText _textToShow;

private _textHeight = ctrlTextHeight __CTRL(3);
private _pos = _map ctrlMapWorldToScreen _vehiclePos;
_pos set [0, (_pos select 0) + 15 / 640];
_pos set [1, (_pos select 1)];
_pos append [GRIDX(10), _textHeight + GRIDY(1.2) + ([GRIDY(0.4), 0] select (_textHeight == 0))];
_ctrlGrp ctrlSetPosition _pos;
_ctrlGrp ctrlShow true;
_ctrlGrp ctrlCommit 0;

// -- Adjust the background and group so we don't get scrolling
{
    _x params ["_idc", "_offset"];
    private _pos = ctrlPosition __CTRL(_idc);
    _pos set [3, _textHeight + GRIDY(_offset)];
    __CTRL(_idc) ctrlSetPosition _pos;
    __CTRL(_idc) ctrlCommit 0;
} forEach [[2, 0.4], [3, 0.2]];
