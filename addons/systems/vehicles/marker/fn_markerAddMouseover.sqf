/*
    Function:       FRL_Vehicles_fnc_markerAddMouseOver
    Author:         Adanteh
    Description:    Adds the mouseover for vehicles
*/
#include "macros.hpp"

params ["_varName"];

[
    _varName,
    "hoverin",
    {
        (_this select 0) params ["_map", "_xPos", "_yPos"];
        (_this select 1) params ["_varName"];

        if (_varName isEqualTo GVAR(vehicleMarkerOver)) exitWith {};
        GVAR(vehicleMarkerOver) = _varName;

		// -- Create UI element
        #define __CTRL(var1) (_ctrlGrp controlsGroupCtrl var1)
		private _idd = ctrlIDD (ctrlParent _map);
		private _controlName = format [QGVAR(spawnInfo_Group_%1), _idd];
        private _ctrlGrp = uiNamespace getVariable [_controlName, controlNull];
        if (isNull _ctrlGrp) then {
            _ctrlGrp = (ctrlParent _map) ctrlCreate [QSVAR(CtrlVehicleMouseOver), -1];
            uiNamespace setVariable [_controlName, _ctrlGrp];
        };

		(GVAR(vehicleDetailNamespace) getVariable _varName) params ["_type", "_side", "_respawnTime", "_despawnTime", "_despawnRadius", "_position", "_direction", "_spawnCondition"];
		__CTRL(1) ctrlSetText toUpper getText (configFile >> "CfgVehicles" >> _type >> "displayName");

		// -- Keep the data updated
		if (GVAR(vehicleMarkerPFH) != -1) then {
            GVAR(vehicleMarkerPFH) call MFUNC(removePerFrameHandler);
        };

		GVAR(vehicleMarkerPFH) = [{
            params ["_args", "_id"];
            _args params ["_varName", "_controlName", "_map"];

			private _ctrlGrp = (uiNamespace getVariable [_controlName, controlNull]);
			if (isNull _ctrlGrp) exitWith {
				[_id] call MFUNC(removePerFrameHandler);
			};

			[_varName, _map, _ctrlGrp] call FUNC(markerMouseOverText);
        }, 0.5, [_varName, _controlName, _map]] call MFUNC(addPerFramehandler);
    },
    _varName
] call CFUNC(addMapGraphicsEventHandler);

[
    _varName,
    "hoverout",
    {
        (_this select 0) params ["_map", "_xPos", "_yPos"];
        (_this select 1) params ["_varName"];
		private _idd = ctrlIDD (ctrlParent _map);
		private _controlName = format [QGVAR(spawnInfo_Group_%1), _idd];

        if (GVAR(vehicleMarkerOver) isEqualTo _varName) then {
            GVAR(vehicleMarkerOver) = "";
        };

		if (GVAR(vehicleMarkerPFH) != -1) then {
            GVAR(vehicleMarkerPFH) call MFUNC(removePerFrameHandler);
			GVAR(vehicleMarkerPFH) = -1;
        };

        private _grp = uiNamespace getVariable [_controlName, controlNull];
        if (!isNull _grp) then {
            _grp ctrlShow false;
            _grp ctrlCommit 0;
        };
    },
    _varName
] call CFUNC(addMapGraphicsEventHandler);
