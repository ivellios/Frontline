#include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Runs random small functions on client not used by any other systems whatsoever
 *
 *	Example:
 *	[player] call FUNC(clientInit);
 */

GVAR(switchedTeams) = false;
GVAR(markersSide) = [];
GVAR(markersAll) = [];
GVAR(vehicleMarkerOver) = "";
GVAR(vehicleMarkerPFH) = -1;
GVAR(worldSizeCoef) = (worldSize / 8192); // -- Needed to scale icons the proper size

["vehicleInit", {
	(_this select 0) params ["_vehicle", "_side", "_varName"];
	if (_side isEqualTo playerSide) then {
		[_vehicle, _varName] call FUNC(markerAddIcon);

        // -- Remove the marker if this is not a respawnign vehicle
        _vehicle addEventHandler ["Deleted", {
            params ["_entity"];
            private _varName = _entity getVariable ["varName", "##"];
            if !(_varName isEqualTo "##") then {
                (GVAR(vehicleDetailNamespace) getVariable _varName) params ["_type", "_side", ["_respawnTime", -1]];
                if (_respawnTime isEqualTo -1) then {
                    [_varName] call FUNC(markerRemove);
                };
            };

        }];
	};
}] call CFUNC(addEventHandler);

// -- On team switch delete all current markers and replace them by new team ones -- //
["playerSideChanged", {
	GVAR(switchedTeams) = true;
}] call CFUNC(addEventHandler);


// -- Add the statemachine that handles changing markers when there are units inside them -- //
GVAR(markerSM) = call CFUNC(createStatemachine);
[GVAR(markerSM), "init", {
    private _vehicles = +(GVAR(markersSide));
	if (GVAR(switchedTeams)) then {
		GVAR(switchedTeams) = false;
		["switchTeams", _vehicles];
	} else {
		[["updateMarkers", _vehicles], "init"] select (_vehicles isEqualTo []);
	};
}] call CFUNC(addStatemachineState);

[GVAR(markerSM), "updateMarkers", {
    params ["", "_vehicles"];
    private _entry = _vehicles deleteAt 0;
    private _vehicle = _entry select 3;

    while {isNull _vehicle && {!(_vehicles isEqualTo [])}} do {
        _entry = _vehicles deleteAt 0;
        _vehicle = _entry select 3;
    };

    private _defaultState = [["updateMarkers", _vehicles], "init"] select (_vehicles isEqualTo []);
    if (!isNull _vehicle) then {
		if (alive _vehicle) then {
	    	private _crew = crew _vehicle;
	    	private _color = [0.2, 0.2, 0.2, 0.7];
			if ((_crew select { ISFRIEND(_x) }) findIf { alive _x } != -1) then {
				if (_crew findIf { group _x == (group CLib_Player) } != -1) then { // -- Do alive check here pls
					_color = [0, 0.87, 0, 1];
				} else {
					_color = +([playerSide, "color"] call MFUNC(getSideData));
				};
			};
			[_entry, _color] call FUNC(markerUpdate);

		};
    };
    _defaultState;
}] call CFUNC(addStatemachineState);

[GVAR(markerSM), "switchTeams", {
    params ["", "_vehicles"];
	private _markers =+ GVAR(markersAll);
	{
		[_x] call FUNC(markerRemove);
		nil;
	} count _markers;
	GVAR(markersSide) = [];
	GVAR(markersAll) = [];
	private _array = +((GVAR(allVehiclesSide)) select (playerSide call BIS_fnc_sideID));
    {
		_x call FUNC(markerAddIcon);
		nil;
    } count _array;
	QGVAR(vehicleQueued) call CFUNC(localEvent);

    "init";
}] call CFUNC(addStatemachineState);

["DrawMapGraphics", {
    GVAR(markerSM) call CFUNC(stepStatemachine);
}] call CFUNC(addEventhandler);

{
	[_x, {
		private _array = +((GVAR(vehicleQueue)) select (playerSide call BIS_fnc_sideID));
		{
			_x call FUNC(markerRespawningInfo);
			nil;
		} count _array;
	}] call CFUNC(addEventHandler);
} forEach [QGVAR(vehicleQueued), "playerSideChanged", "MissionStarted"];
