/*
    Function:       FRL_Vehicles_fnc_markerUpdate
    Author:         Adanteh
    Description:    Map icon for vehicle
*/
#include "macros.hpp"

params ["_entry", "_color"];
_entry params ["_varName", "_size", "_icon", "_vehicle"];
private _code = {
	_width = (_height * (10^(abs log _mapScale))) max 25;
	_height = _width;
};

private _marker = [["ICON", _icon, _color, _vehicle, _size, _size, _vehicle, "", 0, 0.05, "PuristaMedium", "right", _code]];
if ((crew _vehicle) findIf { [_x] call MFUNC(isUnconscious) } != -1) then {
    _marker pushBack ["ICON", "a3\ui_f\data\IGUI\Cfg\Actions\heal_ca.paa", [1, 0.2, 0.2, 1], _vehicle, 20, 20, _vehicle];
};
[_varName, _marker, "normal"] call CFUNC(addMapGraphicsGroup);
