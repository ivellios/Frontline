/*
    Function:       FRL_Vehicles_fnc_markerRespawningInfo
    Author:         Adanteh
    Description:    Icon for respawnning vehicle. Indicates condition
*/
#include "macros.hpp"

params [["_varName", ""]];
(GVAR(vehicleDetailNamespace) getVariable _varName) params ["_type", "_side", "_respawnTime", "_despawnTime", "_despawnRadius", "_position", "_direction", "_condition"];

private _varNameCfg = configFile >> "cfgVehicles" >> _type;
private _size = ((getNumber (_varNameCfg >> "mapSize")) *  0.075) / GVAR(worldSizeCoef);
private _icon = getText (_varNameCfg >> "icon");
if (isText (configFile >> "cfgVehicleIcons" >> _icon)) then {
    _icon = getText (configFile >> "cfgVehicleIcons" >> _icon);
};

private _code = {
	_width = (_height * (10^(abs log _mapScale))) max 18;
	_height = _width;
};

private _color = +([playerSide, "color"] call MFUNC(getSideData));
[_varName, [
	["ICON", "\A3\ui_f\data\map\markers\nato\respawn_unknown_ca.paa", _color, _position, 1 / GVAR(worldSizeCoef), 1 / GVAR(worldSizeCoef), 0, "", 0, 0.05, "PuristaMedium", "right", _code],
	["ICON", _icon, [1, 1, 1, 0.7], _position, _size, _size, _direction, "", 0, 0.05, "PuristaMedium", "right", _code]
], "normal"] call CFUNC(addMapGraphicsGroup);
[_varName] call FUNC(markerAddMouseover);
