class vehicles {
    defaultLoad = 1;
    path = PATHTO(vehicles);
    dependencies[] = {"Main"};

    class init;
    class clientInitLock;
    class clientInitCrewman;
    class clientInit;
    class serverInit;
    class serverInitAbandon;

    class condition;
    class getCrewmanRequired;
    class getRespawnTime;
    class getVehicleSide;
    class getVehicleType;
    class performVehicleRespawn;
    class vehicleFlipAction;
    class vehicleLoad;

    class Marker {
      class clientInitMarkers;
      class markerRespawningInfo;
      class markerAddIcon;
      class markerAddMouseover;
      class markerMouseOverText;
      class markerUpdate;
      class markerRemove;
    };
};
