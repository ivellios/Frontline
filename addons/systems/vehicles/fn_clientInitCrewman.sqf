/*
    Function:       FRL_Vehicles_fnc_clientInitCrewman
    Author:         Adanteh
    Description:    Inits the crewman system for vehicles
*/
#include "macros.hpp"

// if (true) exitWith { };

private _noCrewmanList = configProperties [(configFile >> "FRL" >> "CfgVehicles"), ("getNumber (_x >> ""needsCrewman"") > 0"), false];
private _noCrewmanListProcessed = [];
{
    if (isClass (configFile >> "cfgVehicles" >> (configName _x))) then {
        _noCrewmanListProcessed pushBack (configName _x);
    };
    nil;
} count _noCrewmanList;
GVAR(noCrewmanList) = _noCrewmanListProcessed;

// -- Prevent soloing seats gunner seat without someone else already in driver (Or another turret seat) -- //
["crewSeatEntered", {
    (_this select 0) params ["_vehicle", "_crewSeat"];
    if (({ _x == "Crewman"} count (Clib_Player getVariable [QSVAR(rAbilities), []]) > 0)) exitWith { };
    if ([_vehicle] call FUNC(getCrewmanRequired)) then {
        // excepting gunners in helicopters
        [_vehicle] call FUNC(getVehicleType) params ["_objectCategory", "_objectType"];
        private _isTransportHeli = (_objectCategory == "helicopter" && {_objectType != "attackhelicopter"});
        if !(_crewSeat == "gunner" && _isTransportHeli) then {
            #ifdef DEBUGFULL
                if (true) exitWith {
                    ["debugMessage", ["Bypassing crewman due to debug mode", "orange", nil, -1]] call MFUNC(localEvent);
                };
            #endif
            moveOut CLib_Player;
            ["You need a crewman kit to enter this seat", ICON(content,clear), 'orange'] call MFUNC(notificationShow);
        };
    };
}] call CFUNC(addEventHandler);

// -- Used for F getin, will skip crew seats if you need crewman
["crewSeatAvailable", {
    params ["_unit", "_vehicle"];
    if !([_vehicle] call FUNC(getCrewmanRequired)) exitWith { true };
    ({ _x == "Crewman"} count (Clib_Player getVariable [QSVAR(rAbilities), []]) > 0);
}] call MFUNC(addCondition);
