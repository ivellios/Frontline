 /*---------------------------------------------------------------------------
     This is a modified version of AAW_VehicleRespawn, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"
/*
    Project Reality ArmA 3

    Author: BadGuy

    Description:
    Performs a respawn after 5 seconds

    Parameter(s):
    0: Old Vehicle <Object>
    1: Vehicle Type <String>
    2: Vehicle variable name <String>

*/

params ["_vehicle", "_type", "_varName"];

if (!isNull _vehicle) then {
    GVAR(allVehicles) deleteAt (GVAR(allVehicles) find [_vehicle, _varName]);
    deleteVehicle _vehicle;
};

[{
    params ["_varName"];

    private _condition = (GVAR(vehicleDetailNamespace) getVariable _varName) param [7, "true"];
    if (_condition isEqualType "") then { _condition = compile _condition; };

    [{
        params ["_varName"];
        (GVAR(vehicleDetailNamespace) getVariable _varName) params ["_type", "_side", "_respawnTime", "_despawnTime", "_despawnRadius", "_position", "_direction", "_condition", "_vars"];

        private _sideID = _side call BIS_fnc_sideID;
    	if (_sideID <= 3) then {
    		(GVAR(vehicleQueue) select _sideID) deleteAt ((GVAR(vehicleQueue) select _sideID) find _varName);
            publicVariable QGVAR(vehicleQueue);
    	};

        // -- Adjust a little bit, makes it less likely to explode
        private _spawnPosition = +_position;
        _spawnPosition set [2, (_spawnPosition select 2) + 0.05];
        private _vehicle = _type createVehicle _spawnPosition;

        _vehicle setPosATL _spawnPosition;
        _vehicle setDir _direction;

        {
            _vehicle setVariable [_x select 0, _x select 1, true];
        } forEach _vars;

        clearItemCargoGlobal _vehicle;
        clearMagazineCargoGlobal _vehicle;
        clearWeaponCargoGlobal _vehicle;
        clearBackpackCargoGlobal _vehicle;
        _vehicle disableTIEquipment true;

        ["vehicleInit", [_vehicle, _side, _varName]] call CFUNC(globalEvent);

    }, _condition, _this] call CFUNC(waitUntil);
}, 3, [_varName]] call CFUNC(wait);
