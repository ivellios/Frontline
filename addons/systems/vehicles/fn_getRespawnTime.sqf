#include "macros.hpp"
/*
 *	File: fn_getRespawnTime.sqf
 *	Author: Adanteh
 *	Describe your function
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(getRespawnTime);
 */

params [["_vehicle", objNull, ["", objNull]]];

if (_vehicle isEqualType objNull) then {
    _vehicle = typeOf _vehicle;
};

private _category = ([_vehicle] call FUNC(getVehicleType) select 1);
//++ indicates the current category (Needs to be updated to work okay with RHS at least)
private _respawnDelay = switch (toLower _category) do {
    case "tank";//++
    case "mbt";
    case "armour": { [35, 25, 5, 3] };

	case "tank_amphibious": { [30, 50, 10, -1] };

    case "wheeledifv"; // [Wheeled APC (Not IFV)]
    case "trackedapc"; //++
    case "ifv": { [20, 25, 5, 3] };

    case "armored_car": { [15, 40, 5, 3] };

    case "vehicleautonomous";//++

    case "jet";
    case "plane": { [20, 25, 5, 12] };//++
    case "plane_heavytransport": { [180, 25, 180, -1] };
    case "plane_recon": { [180, 25, 180, -1] };

    case "attackhelicopter": { [20, 25, 5, 12] };

    case "helicopter"; //++
    case "heli_lighttransport";
    case "heli_mediumtransport";
    case "heli_mediumtransport_armed";
    case "heli_heavytransport": { [10, 25, 5, -1] };

    case "halftrack_small";
    case "halftrack": { [12, 50, 10, -1] };
    case "apc";    // [Wheeled/Tracked APC (NOT IFV)]
    case "armedmrap": { [12, 50, 10, 1] };

    case "motorcycle"; //++
    case "quadbike"; //++
    case "car": { [4, 10, 2.5, -1] };

    case "mrap";
    case "truck": { [10, 50, 15, -1] };

    case "armed_car": { [7.5, 50, 10, 1] }; // Humvees, Tigr, Technicals


    case "ship";
    case "submarine";
    case "boat": { [2.5, 10, 2.5, -1] };
    case "lcvp": { [1, 25, 10, -1] };
    case "lcm": { [30, 50, 10, -1] };

    case "staticweapon";//
    case "static": { [10, 200, 10, -1] };
    default {
        diag_log format ["[FRL Warning] Vehicle type: '%1' doesn't have a preset respawn time (Category: '%2')", _vehicle, _category];
        [10, 25, 5, -1]
    };
};

// _respawnDelay = [0.1, 25, 0.25];
// [Respawn delay, despawn distance, despawn time, initial spawn time]
#ifdef DEBUGFULL
    _respawnDelay set [2, 1];
#endif
_respawnDelay;
