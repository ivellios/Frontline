/*
    Function:       FRL_Vehicles_fnc_getVehicleType
    Author:         Adanteh
    Description:    Checks category for given vehicle(type)
    Example:		[cursorObject] call frl_vehicles_fnc_getVehicleType;
*/
#include "macros.hpp"

params [["_vehicle", "", ["",objnull]]];
if (_vehicle isEqualType objNull) then {
    _vehicle = typeOf _vehicle;
};

private _savedType = GVAR(vehicleNamespace) getVariable (_vehicle + "_category");
if !(isNil "_savedType") exitWith { _savedType };

private _vehicleType = switch (true) do {
    case (isclass (configfile >> "cfgvehicles" >> _vehicle)): {
        private ["_cfgObject","_simulation","_namesound","_objectCategory","_objectType"];

        _cfgObject = configfile >> "cfgvehicles" >> _vehicle;
        _simulation = tolower gettext (_cfgObject >> "simulation");
        _namesound = toLower (gettext (_cfgObject >> "namesound"));
        _objectCategory = "Object";
        _objectType = switch _simulation do {
            case "airplane";
            case "airplanex": {
                _objectCategory = "Air";
                switch (toLower (getText (_cfgObject >> "vehicleClass"))) do {
                    default {
                        switch (true) do {
                            case (_vehicle isKindOf "LIB_C47_Skytrain"): { "plane_heavytransport" };
                            case (_vehicle isKindOf "LIB_US_P39"): { "plane_recon" };
                            case (_vehicle isKindOf "LIB_US_P39_2"): { "plane_recon" };
                            case (_vehicle isKindOf "LIB_CG4_WACO"): { "plane_heavytransport" };
                            case (_vehicle isKindOf "LIB_HORSA"): { "plane_heavytransport" };
                            case (_vehicle isKindOf "LIB_Ju52"): { "plane_heavytransport" };
                            default {"plane"};
                        };
                    };
                };
            };
            case "car";
            case "carx": {
                _objectCategory = "Vehicle";
                switch (toLower _nameSound) do {
                    case ("veh_vehicle_mrap_s"): {
                        if (toLower (getText (_cfgObject >> "vehicleClass")) in ["rhs_vehclass_car", "class rhs_vehclass_car"]) then {
                            "Car"
                        } else {
                            _classArray = [_vehicle, "_"] call BIS_fnc_splitString;
                            { _classArray set [_forEachIndex, toLower _x]; } forEach _classArray;
                            switch (true) do {
                                case ("gmg" in _classArray);
                                case ("hmg" in _classArray): {"ArmedMRAP"};
                                default {"MRAP"};
                            };
                        };
                    };
                    case ("veh_vehicle_apc_s"): {
                        if ((getText (_cfgObject >> "vehicleClass")) == "rhs_vehclass_apc") then {
                            "APC";
                        } else {
                            "WheeledIFV"
                        };
                    };
                    case ("veh_vehicle_truck_s"): {
                        if ((getText (_cfgObject >> "vehicleClass")) == "rhs_vehclass_MRAP") then {
                            "MRAP";
                        } else {
                            private _crew = toLower (getText (_cfgObject >> "crew"));
                            if (_crew in ["lib_ger_tank_crew", "lib_us_tank_crew", "fow_s_ger_heer_tankcrew_02_gefreiter"]) exitWith {
                                "armored_car"
                            };
                            "Truck"
                        };
                    };
                    case ("veh_vehicle_armedcar_s"): {
                        if ((getText (_cfgObject >> "vehicleClass")) == "rhs_vehclass_MRAP") then {
                            "MRAP";
                        } else {
                            "Armed_Car"
                        };
                    };
                    case ("veh_vehicle_car_s"): {
                        if (_vehicle isKindOf "Quadbike_01_base_F") then {
                            "Quadbike";
                        } else {
                            "Car"
                        };
                    };
                    case ("veh_apc"): { "APC" };
                    case ("truck");
                    case ("veh_truck"): { "Truck" };
                    default {
                        switch (true) do {
                            case (_vehicle iskindof "uns_BTR152_DSHK"): {"Halftrack"};
                            case (_vehicle iskindof "uns_Type55"): {"car"};
                            case (_vehicle iskindof "uns_Type55_RR57"): {"armed_car"};
                            case (_vehicle iskindof "uns_Type55_LMG"): {"car"};
                            case (_vehicle iskindof "uns_Type55_MG_Base"): {"armed_car"};
                            case (_vehicle iskindof "uns_Type55_MG"): {"armed_car"};
                            case (_vehicle iskindof "uns_Type63_mg"): {"Halftrack"};
                            default {"Car"};
                        };
                    };
                };
            };
            case "tank";
            case "tankx": {
                _objectCategory = "Tank";
                switch (toLower (getText (_cfgObject >> "vehicleClass"))) do {
                    case "rhs_vehclass_apc": { "APC"; };
                    case "rhs_vehclass_ifv": { "IFV"; };
                    default {
                        switch (true) do {
                            case (_vehicle isKindOf "I_APC_tracked_03_cannon_F"): { "IFV" };

                            case (_vehicle isKindOf "LIB_WheeledTracked_APC_base"): { "Halftrack" };
                            case (_vehicle isKindOf "fow_v_sdkfz_251"): { "Halftrack" };
                            case (_vehicle isKindOf "fow_v_sdkfz_250_base"): { "halftrack_small" };
                            case (_vehicle isKindOf "fow_v_universalCarrier"): { "halftrack_small" };
                            case (_vehicle isKindOf "LIB_Scout_M3_base"): { "halftrack_small" };
                            case (_vehicle isKindOf "fow_v_sdkfz_250_9_base"): { "armored_car" };
                            case (_vehicle isKindOf "fow_v_sdkfz_222"): { "armored_car" };
                            case (_vehicle isKindOf "LIB_SdKfz_7_base"): { "Truck" };
                            case (_vehicle iskindof "uns_M113_base"): {"mrap"};
                            case (_namesound == "veh_vehicle_apc_s"): {"IFV"};
                            case (_vehicle iskindof "StaticWeapon"): {"StaticWeapon"};
                            case (_vehicle iskindof "fow_v_lvta2"): {"lcvp"};
                            case (_vehicle iskindof "LIB_M4A3_75_Tubes"): {"tank_amphibious"};
                            case (_vehicle iskindof "fow_v_m5a1"): {"tank_amphibious"};
                            case (_vehicle iskindof "LIB_UniversalCarrier_base"): {"car"};
                            default {"Tank"};
                        };
                    };
                };
            };
            case "helicopter";
            case "helicopterrtd";
            case "helicopterx": {
                _objectCategory = "helicopter";
               switch (toLower (getText (_cfgObject >> "vehicleClass"))) do {
                    default {
                        switch (true) do {
                            case (_vehicle isKindOf "RHS_MELB_H6M"): { "heli_lighttransport" };
                            case (_vehicle isKindOf "RHS_MELB_MH6M"): { "heli_lighttransport" };
                            case (_vehicle isKindOf "uns_oh6_transport"): { "heli_lighttransport" };

                            case (_vehicle isKindOf "RHS_UH1Y_UNARMED"): { "heli_mediumtransport" };
                            case (_vehicle isKindOf "RHS_UH60_Base"): { "heli_mediumtransport" };
                            case (_vehicle isKindOf "rhs_mi8amt_base"): { "heli_mediumtransport" };
                            case (_vehicle isKindOf "RHS_Mi8mt_vvs"): { "heli_mediumtransport" };
                            case (_vehicle isKindOf "uns_UH1D_m60"): { "heli_mediumtransport_armed" };

                            case (_vehicle isKindOf "RHS_CH_47F_base"): { "heli_heavytransport" };
                            case (_vehicle isKindOf "rhsusf_CH53E_USMC"): { "heli_heavytransport" };
                            case (_vehicle isKindOf "uns_ch47_m60_usmc"): { "heli_heavytransport" };

                            case (_vehicle isKindOf "RHS_MELB_AH6M"): { "attackhelicopter" };
                            case (_vehicle isKindOf "RHS_UH1Y"): { "attackhelicopter" };
                            case (_vehicle isKindOf "RHS_UH1Y_FFAR"): { "attackhelicopter" };
                            case (_vehicle isKindOf "RHS_AH1Z_base"): { "attackhelicopter" };
                            case (_vehicle isKindOf "RHS_AH64_base"): { "attackhelicopter" };
                            case (_vehicle isKindOf "rhs_mi8mtv3_base"): { "attackhelicopter" };
                            case (_vehicle isKindOf "RHS_Mi24_base"): { "attackhelicopter" };
                            case (_vehicle isKindOf "rhs_mi28n_base"): { "attackhelicopter" };
                            case (_vehicle isKindOf "RHS_Ka52_base"): { "attackhelicopter" };
                            case (_vehicle isKindOf "uns_oh6_m27"): { "attackhelicopter" };
                            case (_vehicle isKindOf "uns_UH1C_M21_M200"): { "attackhelicopter" };
                            case (_vehicle isKindOf "uns_AH1g_base"): { "attackhelicopter" };
                            case (_vehicle isKindOf "uns_ach47_m200"): { "attackhelicopter" };

                            case (_vehicle isKindOf "B_Heli_Light_01_F"): { "heli_lighttransport" };

                            case (_vehicle isKindOf "B_Heli_Transport_03_F"): { "heli_heavytransport" };
                            case (_vehicle isKindOf "I_Heli_Transport_02_F"): { "heli_heavytransport" };
                            case (_vehicle isKindOf "O_Heli_Transport_04_F"): { "heli_heavytransport" };

                            case (_vehicle isKindOf "B_Heli_Transport_01_F"): { "heli_mediumtransport" };
                            case (_vehicle isKindOf "I_Heli_light_03_unarmed_F"): { "heli_mediumtransport" };
                            case (_vehicle isKindOf "O_Heli_Light_02_unarmed_F"): { "heli_mediumtransport" };

                            case (_vehicle isKindOf "B_Heli_Light_01_dynamicLoadout_F"): { "attackhelicopter" };
                            case (_vehicle isKindOf "I_Heli_light_03_dynamicLoadout_F"): { "attackhelicopter" };
                            case (_vehicle isKindOf "O_Heli_Light_02_dynamicLoadout_F"): { "attackhelicopter" };

                            case (_vehicle isKindOf "B_Heli_Light_01_dynamicLoadout_F"): { "attackhelicopter" };
                            case (_vehicle isKindOf "I_Heli_light_03_dynamicLoadout_F"): { "attackhelicopter" };
                            case (_vehicle isKindOf "O_Heli_Light_02_dynamicLoadout_F"): { "attackhelicopter" };
                            case (_vehicle isKindOf "B_Heli_Attack_01_dynamicLoadout_F"): { "attackhelicopter" };
                            case (_vehicle isKindOf "O_Heli_Attack_02_dynamicLoadout_F"): { "attackhelicopter" };
                            default {"Helicopter"};

                        };
                    };
                };
            };

            case "motorcycle": {
                _objectCategory = "Vehicle";
                "Motorcycle"
            };
            case "ship": {
                _objectCategory = "Ship";
                if (_vehicle isKindOf "LIB_LCM3") then {
                    "lcm"
                } else {
                    "Ship"
                };
            };
           case "shipx": {
                _objectCategory = "Ship";
                if (_vehicle isKindOf "fow_lcvp") then {
                    "lcvp"
                } else {
                    "boat"
                };
            };
            case "submarinex": {
                _objectCategory = "Ship";
                "Submarine"
            };
            case "paraglide": {
                _objectCategory = "Parachute";
                "Parachute";
            };
            case "house";
            case "thing";
            case "thingx": {
                switch (true) do {
                    case (getnumber (_cfgObject >> "isbackpack") > 0): {"Backpack"};
                    case (_object iskindof "ReammoBox");
                    case (_object iskindof "ReammoBox_F"): {"AmmoBox"};
                    case (_object iskindof "Wreck");
                    case (_object iskindof "Wreck_Base"): {"Wreck"};
                    case (_object iskindof "TargetBase"): {"Target"};
                    case (_object iskindof "HeliH"): {"Helipad"};
                    case (_object isKindOf "Land_CargoBox_V1_F"): { "Cargobox" };
                    case (_simulation == "house"): {"House"};
                };
            };
            default {
                "Unknown";
            };
        };
        if (getnumber (_cfgObject >> "isUAV") > 0) then {_objectCategory = "VehicleAutonomous";};
        if (getnumber (_cfgObject >> "hasdriver") == 0 && count (_cfgObject >> "turrets") == 0) then {_objectCategory = "Object"};
        [_objectCategory,_objectType]
    };
    default {["",""]};
};

GVAR(vehicleNamespace) setVariable [(_vehicle + "_category"), _vehicleType];
_vehicleType
