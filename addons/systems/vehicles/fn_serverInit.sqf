 /*---------------------------------------------------------------------------
     This is a modified version of AAW_VehicleRespawn, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"

/*
    Project Reality ArmA 3

    Author: BadGuy

    Description:
    Init for Vehicle Respawn System
*/

GVAR(allVehicles) = [];
GVAR(vehicleDetails) = [[], []];
GVAR(vehicleDetailNamespace) = true call CFUNC(createNamespace);
publicVariable QGVAR(vehicleDetailNamespace);

addMissionEventHandler ["EntityKilled", {
    params ["_vehicle", "_killer"];

    private _varName = _vehicle getVariable ["varname", "##"];
    private _vehicleDetails = GVAR(vehicleDetailNamespace) getVariable _varName;
    if (isNil "_vehicleDetails") exitWith { };

    private _respawnTime = _vehicleDetails param [2, -1];
    if (_respawnTime >= 0) then {
        GVAR(allVehicles) deleteAt (GVAR(allVehicles) find _vehicle);

        private _sideID = (_vehicleDetails param [1, sideUnknown]) call BIS_fnc_sideID;
        private _alreadyDestroyed = 0;
    	if (_sideID <= 3) then {
    		_alreadyDestroyed = (GVAR(vehicleQueue) select _sideID) pushBackUnique _varName;
            publicVariable QGVAR(vehicleQueue);
            QGVAR(vehicleQueued) call CFUNC(globalEvent);
    	};

        if (_alreadyDestroyed == -1) exitWith { };
        GVAR(vehicleDetailNamespace) setVariable [(_varName + "_respawnAt"), (_respawnTime + serverTime), true];
        [FUNC(performVehicleRespawn), _respawnTime, [_vehicle, typeOf _vehicle, _varName]] call CFUNC(wait);
    };
}];

["vehicleInit", {
    (_this select 0) params ["_vehicle", "_side", "_varName"];
    _vehicle setVariable ["varName", _varname, true];
    _vehicle setVariable ["side", _side, true];
    missionNamespace setVariable [_varName, _vehicle, true];
    GVAR(allVehicles) pushBackUnique _vehicle;

    if (isNil { GVAR(vehicleDetailNamespace) getVariable _varName }) then {
        GVAR(vehicleDetailNamespace) setVariable [_varName, [typeOf _vehicle, _side], true];
    };
}] call CFUNC(addEventHandler);

// -- Add all the variables to the vehicles if they aren't already added (Through mission editor) -- //
["missionStarted", {
    {
    	[_x, _forEachIndex] call FUNC(vehicleLoad);
    } forEach ((entities "LandVehicle") + (entities "Air") + (entities "Ship"));
    publicVariable QGVAR(vehicleQueue);
    /*[{
        QGVAR(vehicleQueued) call CFUNC(globalEvent);
    }, 10] call CFUNC(wait);*/
}] call CFUNC(addEventHandler);
