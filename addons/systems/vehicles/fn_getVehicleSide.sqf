#include "macros.hpp"
/*
 *	File: fn_getVehicleSide.sqf
 *	Author: Adanteh
 *	Retrieves the config entry for a vehicle
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(getVehicleSide);
 */

params ["_vehicle"];
private _vehicleType = typeOf _vehicle;
private _side = sideUnknown;
private _classPath = configFile >> "cfgVehicles" >> _vehicleType;
if (isClass _classPath) then {
	_class = _classPath >> "side";
	if (isNumber _class) then {
		_side = (getNumber _class) call BIS_fnc_sideType;
	};
};
_side