#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        // -- Kicks unit if they don't give any input for X time (UNUSED< BUGGED)
        #include "afkkick\module.hpp"

        // -- Shows a little FPS counter in the top right of the screen
        #include "fps\module.hpp"

        // -- Show unit and group markers
        #include "unitMarkers\module.hpp"

        // -- Draws lines to waypoints and allow ctrl+click for leader to set a waypoint
        #include "waypoints\module.hpp"

        // -- Set a maximum mission duration, after which it'll end
        #include "duration\module.hpp"

        // -- Set a maximum mission duration, after which it'll end (UNUSED< We don't spawn people alive)
        #include "prep\module.hpp"

        // -- Vehicle stuffs
        #include "vehicles\module.hpp"
    };

    class CfgBattleprep {
        enabled = 1;
        duration = 300;
    };
};

class RscBackground;
class RscText;
class ctrlControlsGroupNoScrollbars;
class RscTitles {
    #include "prep\RscPrepIndicator.hpp"
};
