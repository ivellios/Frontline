/*
    Function:       FRL_Waypoints_fnc_drawWaypoints
    Author:         Adanteh
    Description:    Draws the waypoint in 3D (We use this instead of vanilla, because we DONT want the ranges showing up)
*/
#include "macros.hpp"

(_this select 0) params ["_waypointID", "_pos", ["_text", ""], ["_icon", MEDIAPATH + "icons\waypointmark_ca.paa"]];

if (_pos isEqualTo []) then {
    [_waypointID] call CFUNC(remove3Dgraphics);
} else {
    [_waypointID, [
    ["ICON",
        _icon,
        [1,1,1,0.6],
        _pos,
        1.35,
        1.35,
        0,
        _text,
        2,
        0.035,
        "PuristaSemiBold",
        "Center",
    true]
    ]] call CFUNC(add3DGraphics);
};
