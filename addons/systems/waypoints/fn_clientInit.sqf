#include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Allows setting of waypoints and handles the drawing of them on the map
 *
*/
#define __GROUPWP_COLOR [0.5, 0.08, 0.5, 1]
#define __PLAYERWP_COLOR [0.9,0.5,0,1] // [0.9,0.5,0,1] // [0.9, 0.66, 0.01, 1]

GVAR(playerWP) = [];

addMissionEventHandler ["MapSingleClick", {
    params ["", "_position", "", "_shift"];

    if (missionNamespace getVariable [QMVAR(ctrlPressed), false]) then {
		private _group = group CLib_Player;
		if ((leader _group) isEqualTo CLib_Player) then {
			while {(count (waypoints _group)) > 0} do {
				deleteWaypoint ((waypoints _group) select 0);
			};
			private _waypoint = _group addWaypoint [_position, 0];

			// -- lel bis. need to set waypoint position to make sure you can actually set it on a place that has objects - //
			// [[_waypoint, [_position, 0]], {(_this select 0) setWaypointPosition (_this select 1)}, 0.1] call CFUNC(wait)
			//_waypoint setWaypointPosition [_position, 0];
			["groupWPChanged", _group] call CFUNC(targetEvent);

		};
    } else {
    	if (_shift) then {
    		GVAR(playerWP) = _position;
    		["playerWPChanged"] call CFUNC(localEvent);
    	};
    };
}];

// -- Waypoint changed event detection -- //
GVAR(waypointsSaved) = [];
[{
	private _datawp = (waypoints (group CLib_Player)) apply { waypointPosition _x };
	{ _x resize 2; nil } count _datawp; // -- Cut out the height thing. This is height to waves, meaning it'll change every frame
	private _data = [_datawp, (currentWaypoint (group CLib_Player))];
	if !(_data isEqualTo GVAR(waypointsSaved)) then {
		["groupWPChanged"] call CFUNC(localEvent);
		GVAR(waypointsSaved) = _data;
	};
}, 1.5] call MFUNC(addPerFramehandler);

// -- Show a line on the map for the group waypoint
["groupWPChanged", {
	(_this select 0) params [["_unit", CLib_Player]];
	private _waypoints = waypoints (group _unit);
	if ((count _waypoints > 0) && {((currentWaypoint (group _unit)) == 0)}) then {
		[QGVAR(groupWP), [
			["LINE", waypointPosition (_waypoints select 0), _unit, __GROUPWP_COLOR],
			["ICON", "\a3\ui_f\data\map\MapControl\taskicon_ca.paa", [1,0.1,0.7,1], waypointPosition (_waypoints select 0), 20, 20, 0]
		]] call CFUNC(addMapGraphicsGroup);
        ["drawWaypoint", [QGVAR(groupWP), waypointPosition (_waypoints select 0), "MOVE"]] call CFUNC(localEvent);
	} else {
		["drawWaypoint", [QGVAR(groupWP), []]] call CFUNC(localEvent);
		[QGVAR(groupWP)] call CFUNC(removeMapGraphicsGroup);
	};
}] call CFUNC(addEventHandler);

["playerWPChanged", {
	(_this select 0) params [["_unit", CLib_Player]];
	if !(GVAR(playerWP) isEqualTo []) then {
		[QGVAR(playerWP), [["LINE", GVAR(playerWP), _unit, __PLAYERWP_COLOR]]] call CFUNC(addMapGraphicsGroup);
	} else {
        [QGVAR(playerWP)] call CFUNC(removeMapGraphicsGroup);
    };
    ["drawWaypoint", [QGVAR(playerWP), GVAR(playerWP), ""]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

// -- Update waypoints after player body changed -- //
["playerChanged", {
	(_this select 0) params ["_unit"];
	["groupWPChanged", [_unit]] call CFUNC(localEvent);
	["playerWPChanged", [_unit]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

["drawWaypoint", { _this call FUNC(drawWaypoints) }] call CFUNC(addEventHandler);
