class waypoints {
    defaultLoad = 1;
    path = PATHTO(waypoints);
    dependencies[] = {"Main"};

    class clientInit;
    class drawWaypoints;
};
