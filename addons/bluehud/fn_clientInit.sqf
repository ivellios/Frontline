#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(stackedPoints) = false call CFUNC(createNamespace);

["radar.addPoint", {
	(_this select 0) params ["_handle", "_type", ["_args", []]];
	GVAR(stackedPoints) setVariable [_handle, [toLower _type, _args]];
}] call CFUNC(addEventHandler);

["radar.removePoint", {
	(_this select 0) params ["_handle"];
	GVAR(stackedPoints) setVariable [_handle, nil];
}] call CFUNC(addEventHandler);

["missionStarted", {
	private _default = [
		false, 	// -- Team draw
		0.035, 	// -- HUD scale
		0.25, 	// -- Background alpha
		false, 	// -- Group only
		true	// -- Show squad hud
	];

	// -- Validate saved settings. If settings are missing or of wrong type, restore to default -- //
	GVAR(settings) = profileNamespace getVariable [QGVAR(settings), _default];
	if (GVAR(settings) isEqualTypeParams _default) then {
		{
			if !((GVAR(settings) param [_forEachIndex, nil]) isEqualType _x) then {
				GVAR(settings) set [_forEachIndex, _x];
			};
		} forEach _default;
		profileNamespace setVariable [QGVAR(settings), GVAR(settings)];
		saveProfileNamespace;
	};

	["fadeHud", {
		(_this select 0) params ["_fade", "_commit"];
		// -- CtrlSetFade doesn't work on map controls, it only supports 0 or 1
		(uiNamespace getVariable QGVAR(map)) ctrlSetFade (round _fade);
		(uiNamespace getVariable QGVAR(map)) ctrlCommit 0;
	}] call CFUNC(addEventHandler);

	[{
		[] spawn {
			QGVAR(map) cutRsc [QGVAR(map), "PLAIN", 1, false];

			// -- Center map on [0,0] for map zoom constant calculation -- //
			(uiNamespace getVariable QGVAR(map)) ctrlMapAnimAdd [0, 0.001, [0, 0, 0]];
			ctrlMapAnimCommit (uiNamespace getVariable QGVAR(map));
			waitUntil {ctrlMapAnimDone (uiNamespace getVariable QGVAR(map))};

			// -- There is no actually reliable way to retrieve how many pixels are a meter, so we determine it by setting the map and then figuring it out like that  -- //
			_delta = (((uiNamespace getVariable QGVAR(map)) ctrlMapWorldToScreen [0,0]) select 1) - (((uiNamespace getVariable QGVAR(map)) ctrlMapWorldToScreen [0,10]) select 1);
			GVAR(mapZoomConstant) = _delta * 0.001;
			_zoom = GVAR(mapZoomConstant) / (GVAR(settings) select 1);

			// -- We simulate everything around the [0, 0] point mark, so we don't get terrain / custom markers in our HUD -- //
			GVAR(mapZero) = ([worldSize, worldSize, 0]);
			(uiNamespace getVariable QGVAR(map)) ctrlMapAnimAdd [0, _zoom, GVAR(mapZero)];

			ctrlMapAnimCommit (uiNamespace getVariable QGVAR(map));
			waitUntil {ctrlMapAnimDone (uiNamespace getVariable QGVAR(map))};

			call FUNC(buildEH);
		};
	}, { !isNull (findDisplay 46) }] call CFUNC(waitUntil);
}] call CFUNC(addEventHandler);
