#include "macros.hpp"
/*
 *	File: fn_vectorRotate.sqf
 *	Author: Bluebear
 *
 *	Example:
 *	[player] call FUNC(vectorRotate);
 */

params ["_vector", "_angle"];

[((_vector select 0) * cos _angle - (_vector select 1) * sin _angle), ((_vector select 0) * sin _angle + (_vector select 1) * cos _angle), 0];
