#define BLUEHUD_SIZE 75

class GVAR(map) {
	idd = -1;
	onLoad = "uiNamespace setVariable ['FRL_blueHUD_map', ((_this select 0) displayCtrl 1)]";
	duration = 10e10;
	fadein = 0;
	fadeout = 0;
	movingEnable = false;
	enableSimulation = 1;
	enableDisplay = 1;

		class Controls {
		class Bluehud: RscMapControlEmpty {
			idc = 1;
			style = 0;
	        x = CENTER_X - (GRID_W * (BLUEHUD_SIZE / 2));
	        y = (safeZoneY + safeZoneH) - (GRID_H * BLUEHUD_SIZE);
	        w = GRID_W * BLUEHUD_SIZE;
	        h = GRID_H * (BLUEHUD_SIZE * 2);
			showCountourInterval = 0;
			maxSatelliteAlpha = 0;
			sizeEx = 0;
			colorBackground[] = {0, 0, 0, 0.3};
			colorText[] = {0, 0, 0, 0};
			colorMainRoads[] = {0, 0, 0, 0};
			colorMainRoadsFill[] = {0, 0, 0, 0};
			colorOutside[] = {0, 0, 0, 0};
			colorRoads[] = {0, 0, 0, 0};
			colorRoadsFill[] = {0, 0, 0, 0};
			colorRoadsTracks[] = {0, 0, 0, 0};
			colorRoadsTracksFill[] = {0, 0, 0, 0};
			colorRoadsTrails[] = {0, 0, 0, 0};
			colorRoadsTrailsFill[] = {0, 0, 0, 0};
			sizeExLabel = 0;
			sizeExGrid = 0;
			sizeExUnits = 0;
			sizeExNames = 0;
			sizeExInfo = 0;
			sizeExLevel = 0;
			stickX[] = {0, {"Gamma", 0, 0}};
			stickY[] = {0, {"Gamma", 0, 0}};
			scaleMin = 0;
			scaleMax = 1;
			scaleDefault = 0.03;
			ptsPerSquareCLn = 2500;
			ptsPerSquareCost = 2500;
			ptsPerSquareExp = 2500;
			ptsPerSquareFor = 2500;
			ptsPerSquareForEdge = 2500;
			ptsPerSquareObj = 2500;
			ptsPerSquareRoad = 2500;
			ptsPerSquareSea = 2500;
			ptsPerSquareTxt = 2500;
		};
	};
};
