#include "macros.hpp"
/*
 *	File: fn_clientInitSettings.sqf
 *	Author: Adanteh
 *	Adds the settings to user setting screen
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(clientInitSettings);
 */

// -- Add settings to the main menu bar -- //
['add', ["gui", 'Frontline Settings', 'Squad Radar', [
	'Enable',
	'Show the squad radar',
	QMVAR(CtrlCheckbox),
	{ (GVAR(settings) select 4); },
	true,
	{
		GVAR(settings) set [4, (_this select 1 == 1)];
		profileNamespace setVariable [QGVAR(settings), GVAR(settings)];
		[] call FUNC(buildEH);
	},
	[]
]]] call MFUNC(settingsWindow);

// -- Squad only -- //
['add', ["gui", 'Frontline Settings', 'Squad Radar', [
	'Squad Only',
	'Show only group members on the radar',
	QMVAR(CtrlCheckbox),
	{ (GVAR(settings) select 3); },
	true,
	{
		GVAR(settings) set [3, (_this select 1 == 1)];
		profileNamespace setVariable [QGVAR(settings), GVAR(settings)];
		[] call FUNC(buildEH);
	},
	[]
]]] call MFUNC(settingsWindow);

// -- Transparency -- //
['add', ["gui", 'Frontline Settings', 'Squad Radar', [
	'Transparency',
	'How transparent the radar is',
	QMVAR(CtrlSlider),
	{ (GVAR(settings) select 2); },
	{ (GVAR(settings) select 4); },
	{
		GVAR(settings) set [2, (_this select 1)];
		profileNamespace setVariable [QGVAR(settings), GVAR(settings)];
		[] call FUNC(buildEH);
		false;
	},
	[[0, 1], [0.05,0.5]]
]]] call MFUNC(settingsWindow);


// -- Allow resize -- //
['add', ["gui", 'Frontline Settings', 'Squad Radar', [
	'Size',
	'Size of the squad radar',
	QMVAR(CtrlSlider),
	{ (GVAR(settings) select 1); },
	{ (GVAR(settings) select 4); },
	{
		(uiNamespace getVariable QGVAR(map)) ctrlMapAnimAdd [0, GVAR(mapZoomConstant) / (_this select 1), GVAR(mapZero)];
		ctrlMapAnimCommit (uiNamespace getVariable QGVAR(map));
		GVAR(settings) set [1, (_this select 1)];
		profileNamespace setVariable [QGVAR(settings), GVAR(settings)];
		[] call FUNC(buildEH);
		false;
	},
	[[0.02,0.065], [0.005,0.005]]
]]] call MFUNC(settingsWindow);
