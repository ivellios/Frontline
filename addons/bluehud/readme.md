## Contents

Squad radar, showing both all team players, with a couple settings for scaling and so. Includes waypoint drawing and lots of caching of data to keep the performance good. This is based on Bluehud made by Blaubär.

Primary changes are different visuals, compatability with Frontline mechanics like waypoints / objectives,
plus caching the data a lot more extensively for better performance.

Original repo can be found here: https://github.com/BaerMitUmlaut/BlueHud
