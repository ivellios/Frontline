#include "macros.hpp"
/*
 *	File: fn_getIcon.sqf
 *	Author: Adanteh
 *	Gets icon for given unit to show on HUD
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	Full path to icon <STRING>
 *
 *	Example:
 *	[player] call FUNC(getIcon);
 */

params [["_unit", objNull]];

(_unit getVariable [QSVAR(rmapIcon), "\A3\ui_f\data\map\vehicleicons\iconMan_ca.paa"])
