#include "macros.hpp"
/*
 *	File: fn_getColor.sqf
 *	Author: BaerMitUmlaut
 *	Describe your function
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(getColor);
 */

#define _DEFAULT_ALPHA 0.9

private _color = if (_this in (units Clib_Player)) then {
	[0.333,0.8,0.333]
} else {
	[1,1,1];
};

_color pushBack (_DEFAULT_ALPHA min ((__RANGE - (Clib_Player distance2D _this)) / 10));
_color