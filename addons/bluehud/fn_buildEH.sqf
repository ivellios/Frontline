/*
 *	File: fn_buildEH.sqf
 *	Author: Bluebaer, Adanteh

	//This is where the magic happens. You know, the whole bee and flower thing.
	//I've decided to build this eventhandler "factory" to improve performance, it just
	//saves a lot of ifs and other calculations that would else need to happen every frame.

	//A quick test has shown that it improves performance quite a bit - it started to
	//stutter when I added custom HUD size and the real center thing below and
	//thankfully that doesn't happen anymore because of this function.
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call FUNC(buildEH)
 */

#include "macros.hpp"

private ["_eventHandler"];

if ((GVAR(settings) select 4)) then {
	(uiNamespace getVariable QGVAR(map)) ctrlShow true;

	private _realCenter = [0, __RANGE, 0] vectorAdd GVAR(mapZero);
	private _iconSizeBig = ((GVAR(settings) select 1) - 0.02) / 0.5 + 20;
	private _iconSizeSmall = ((GVAR(settings) select 1) - 0.02) / 0.5 + 15;
	private _iconSizeBackground = ((GVAR(settings) select 1) * (__RANGE / 2) * 194);
	private _textSize = ((GVAR(settings) select 1) / 4) + 0.02;
	private _backgroundAlpha = (GVAR(settings) select 2);
	private _terrainSizeCoef = (8192 / worldSize) max 1;


	private _header = format ["if !([Clib_Player] call %1) then {
		private _eyeDir = call %2;
		private _posPlayer = (visiblePosition Clib_Player);
        if (cameraOn isEqualTo (getConnectedUAV Clib_Player)) then {
            _posPlayer = visiblePosition (getConnectedUAV Clib_Player);
        };
		private _grpPlayer = group Clib_Player;
		private _grpLeader = leader Clib_Player;
		private _realCenter = %3;
		private _isGrpLeader = ((leader _grpPlayer) == Clib_Player);", QMFUNC(isDead), QFUNC(eyeDir), _realCenter];
	private _footer = "}";

	//Draws the squad on the HUD
	private _drawSquad = format ["{
		if !(Clib_Player == _x) then {
			(uiNamespace getVariable '%3') drawIcon [([_x] call %4),
				(_x call %5),
				([(_posPlayer vectorDiff (visiblePosition _x)), _eyeDir] call %4) vectorAdd %1,
				%2, %2,
				(-1 * _eyeDir + direction _x + 180),
				'', false];
		};
		nil;
	} count units _grpPlayer;", _realCenter, _iconSizeSmall, QGVAR(map), QFUNC(getIcon), QFUNC(vectorRotate), QFUNC(getColor)];

	// -- This code return is cached with 0.75s time, that means if there is a result of this code that's less than 0.75 old, the existing data is used instead of retrieving all new -- //
	// -- This means we only need to retrieve things like role marker, actual objects and color every second, but still being able to use it in a per frame looped (Required for drawIcon) -- //
	private _fnc_nearbyCache = {
		private _return = [];
		{
			if !([_x] call MFUNC(isDead)) then {
				if (_grpLeader != _x) then { // Draw leader separate
					_return pushBack [_x, ([_x] call FUNC(getIcon)), (_x call FUNC(getColor))];
				};
			};
			nil
		} count ((([ASLtoAGL getPosASL Clib_Player, __RANGE] call MFUNC(getNearUnits)) select { (side group _x) == playerSide }) - [Clib_Player]);
		_return;
	};

	private _drawNearby = format ["
	{
		_x params ['_unit', '_icon', '_color'];
		(uiNamespace getVariable '%5') drawIcon [_icon, _color,
			([(_posPlayer vectorDiff (visiblePosition _unit)), _eyeDir] call %6) vectorAdd %1,
			%2, %2, (-1 * _eyeDir + direction _unit + 180), '', false];
		nil;
	} count (['%1', %3, [], 0.75] call %7);", _realCenter, _iconSizeSmall, _fnc_nearbyCache, QGVAR(unitCache), QGVAR(map), QFUNC(vectorRotate), QCFUNC(cachedCall)];
	//Draws the little dot that represents the Clib_Player

	private _drawPlayer = format ["(uiNamespace getVariable '%3') drawIcon [([Clib_Player] call %4), [0.333, 0.8, 0.333, 1], %1, %2, %2, 0, '', false];", _realCenter, _iconSizeSmall, QGVAR(map), QFUNC(getIcon)];
	private _drawLeader = format ["if !(_isGrpLeader) then {
		if (clib_player distance _grpLeader >%5) then {
			(uiNamespace getVariable '%3') drawIcon [([_grpLeader] call %6), [1, 0.1, 0.7,1], ([((vectorNormalized (_posPlayer vectorDiff (visiblePosition _grpLeader))) vectorMultiply %5), _eyeDir] call %4) vectorAdd %1, %2, %2, (-1 * _eyeDir + direction _grpLeader + 180), '', false];
		} else {
			(uiNamespace getVariable '%3') drawIcon [([_grpLeader] call %6), [1, 0.1, 0.7,1], ([(_posPlayer vectorDiff (visiblePosition _grpLeader)), _eyeDir] call %4) vectorAdd %1, %2, %2, (-1 * _eyeDir + direction _grpLeader + 180), '', false];
		};
	};", _realCenter, _iconSizeBig, QGVAR(map), QFUNC(vectorRotate), __RANGE, QFUNC(getIcon)];

	// -- Compass is in the map now
	private _drawCompass = format ["
		(uiNamespace getVariable '%4') drawIcon [('%3\north.paa'), [1,1,1,1], ([[0,(%6 - 2),0], (_eyeDir + 180)] call %5) vectorAdd %1, %2, %2, (_eyeDir * -1 - 180), '', false];
		(uiNamespace getVariable '%4') drawIcon [('%3\west.paa'), [1,1,1,1], ([[0,(%6 - 2),0], (_eyeDir - 90)] call %5) vectorAdd %1, %2, %2, (_eyeDir * -1 + 90), '', false];
		(uiNamespace getVariable '%4') drawIcon [('%3\east.paa'), [1,1,1,1], ([[0,(%6 - 2),0], (_eyeDir + 90)] call %5) vectorAdd %1, %2, %2, (_eyeDir * -1 - 90), '', false];
		(uiNamespace getVariable '%4') drawIcon [('%3\south.paa'), [1,1,1,1], ([[0,(%6 - 2),0], _eyeDir] call %5) vectorAdd %1, %2, %2, (_eyeDir * -1), '', false];
	", _realCenter, _iconSizeBig, (MEDIAPATH + "bluehud"), QGVAR(map), QFUNC(vectorRotate), __RANGE];

	private _drawBearing = format ["
		(uiNamespace getVariable '%3') drawIcon ['#(argb,8,8,3)color(0,0,0,0)', [1,1,1,1], [0, 10, 0] vectorAdd %1, 0, 0, 0, str(round(_eyeDir + 180)), 0, %2, 'RobotoCondensedLight', 'center'];
	", _realCenter, _textSize * _terrainSizeCoef, QGVAR(map)];

	private _stackedPoints = format ["
		private _allVariables = +(allVariables %5);
		{
			private _var = %5 getVariable _x;
			if !(isNil '_var') then {
				_var params ['_type', '_args'];
				switch _type do {
					case 'line': {
						_args params ['_color', '_posStart', '_posEnd'];
						if (_posStart isEqualType {}) then { _posStart = call _posStart } else {
							_posStart = _posPlayer vectorDiff _posStart;
							_posStart = ([((vectorNormalized _posStart) vectorMultiply %4), _eyeDir] call %2) vectorAdd _realCenter;
						};
						if (_posEnd isEqualType {}) then { _posEnd = call _posEnd } else {
							_posEnd = _posPlayer vectorDiff _posEnd;
							_posEnd = ([((vectorNormalized _posEnd) vectorMultiply %4), _eyeDir] call %2) vectorAdd _realCenter;
						};
						(uiNamespace getVariable '%3') drawLine [_posStart, _posEnd, _color];
					};
				};
			};
		} forEach _allVariables;", _realCenter, QFUNC(vectorRotate), QGVAR(map), __RANGE, QGVAR(stackedPoints)];

	private _drawWaypoint = format ["
	if (count waypoints _grpPlayer > 0) then {
		if (currentWaypoint _grpPlayer == 0) then {
			_waypointPos = _posPlayer vectorDiff (waypointPosition (waypoints (group Clib_Player) select 0));
			_waypointPos = ([((vectorNormalized _waypointPos) vectorMultiply %6), _eyeDir] call %4) vectorAdd %1;
			(uiNamespace getVariable '%5') drawLine [
				%1,
				_waypointPos,
				%2
			];
		};
	};
	if !(%7 isEqualTo []) then {
		_waypointPos = _posPlayer vectorDiff %7;
		_waypointPos = ([((vectorNormalized _waypointPos) vectorMultiply %6), _eyeDir] call %4) vectorAdd %1;
			(uiNamespace getVariable '%5') drawLine [
				%1,
				_waypointPos,
				%3
			];
	};
	", _realCenter, [1, 0.1, 0.7,1], [0.9,0.5,0,1], QFUNC(vectorRotate), QGVAR(map), __RANGE, QEGVAR(waypoints,playerWP)];

	private _drawObjectives = format ["
		if !(%4 isEqualTo [0, 0, 0]) then {
			(uiNamespace getVariable '%6') drawIcon [('%3\defend_ca.paa'), [0.01, 0.67, 0.92, 1], ([((vectorNormalized (_posPlayer vectorDiff %4)) vectorMultiply %8), _eyeDir] call %7) vectorAdd %1, %2, %2, 0, '', false];
		};
		if !(%5 isEqualTo [0, 0, 0]) then {
			(uiNamespace getVariable '%6') drawIcon [('%3\attack_ca.paa'), [0.99, 0.26, 0, 1], ([((vectorNormalized (_posPlayer vectorDiff %5)) vectorMultiply %8), _eyeDir] call %7) vectorAdd %1, %2, %2, 0, '', false];
		};
	", _realCenter, _iconSizeBig, (MEDIAPATH + "bluehud"), QEGVAR(frontline,defendZonePos), QEGVAR(frontline,attackZonePos), QGVAR(map), QFUNC(vectorRotate), __RANGE];


	_eventHandler = _header;
	if (_backgroundAlpha > 0) then {
		private _drawBackground = format ["
		    (uiNamespace getVariable '%7') drawEllipse [
		        %1,
		        %2, %2,
		        0,
		        [1, 1, 1, 1],
		        '#(rgb,1,1,1)color(0,0,0,%3)'];
		    (uiNamespace getVariable '%7') drawIcon [('%4\bluehud.paa'), [1, 1, 1, %6], %1, %5, %5, (_eyeDir * -1 - 180), '', false];"

		        , _realCenter, __RANGE, (_backgroundAlpha * 0.7), (MEDIAPATH + "bluehud"), _iconSizeBackground, (_backgroundAlpha / 2 + 0.5), QGVAR(map)];
		_eventHandler = _eventHandler + _drawBackground;
	};


	if ((GVAR(settings) select 3)) then {
		_eventHandler = _eventHandler + _drawSquad;
	} else {
		_eventHandler = _eventHandler + _drawNearby;
	};

	_eventHandler = _eventHandler 	+ _drawWaypoint
									+ _stackedPoints
									+ _drawPlayer
									+ _drawLeader
									/*+ _drawCompass*/
									+ _drawBearing
								    + ([_drawObjectives, ""] select (isNil QEGVAR(frontline,attackZonePos)))
									+ _footer;
} else {
	(uiNamespace getVariable QGVAR(map)) ctrlShow false;
	_eventHandler = "";
};

(uiNamespace getVariable QGVAR(map)) ctrlSetEventHandler ["Draw", _eventHandler];
