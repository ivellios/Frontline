#include "macros.hpp"
/*
 *	File: fn_eyeDir.sqf
 *	Author: Bluebaer
 *
 *	Example:
 *	[player] call FUNC(eyeDir);
 */

private _camPos1 = positionCameraToWorld [0,0,0];
private _camPos2 = positionCameraToWorld [0,0,1];
(((_camPos1 select 0) - (_camPos2 select 0)) atan2 ((_camPos1 select 1) - (_camPos2 select 1)));