#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class Bluehud {
            defaultLoad = 1;
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\bluehud";

            class clientInit;
            class clientInitSettings;
            class buildEH;
            class eyeDir;
            class getColor;
            class getIcon;
            class vectorRotate;
        };
    };
};

class RscMapControlEmpty;
class RscTitles {
  #include "RscBlueHUD.hpp"
};
