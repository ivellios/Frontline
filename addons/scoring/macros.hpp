// -- Module defines -- //
#define MODULE Scoring

// -- Global defines -- //
#include "..\main\macros_local.hpp"



#define PYN 108
#define PX(X) ((X)/PYN*safeZoneH/(4/3))
#define PY(Y) ((Y)/PYN*safeZoneH)

#define _IDC_SCOREBOARD 20000
#define _IDC_MOD_SQUAD 100
#define _IDC_MOD_UNIT 1

#define _AMOUNT_SQUAD 20
#define _SIZE_SIDE_W PX(50)
#define _SIZE_SIDE_H PY(90)
#define _SIZE_UNIT_H PY(3)
#define _SIZE_UNIT_W PX(3)
