#define __INCLUDE_DIK
#include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Inits scoring system on client
 *
 */

GVAR(rowsCreated) = 0;
GVAR(sidesCreated) = 0;
GVAR(uidShown) = [];
GVAR(uidsHandledLast) = [];
GVAR(cachedData) = [];
GVAR(scoreboardNamespace) = call CFUNC(createNamespace);

/*---------------------------------------------------------------------------
	Sort by squad:

	Handle sides completely individually in side-split array.
	Create array of each group per side, with the total score divided by the amount of units in a squad
	Walk through each group, adding a new row for each unit
---------------------------------------------------------------------------*/
// -- Score is only updated every 60 seconds (This means less broadcasting and prevents using scoreboard as kill message replacement) -- //



// TODO List below
/*---------------------------------------------------------------------------

	## Dialog config setup
	Prepare 10 squads controlGroupNoScrollbar for each side, with no scrollbar, appropriate width, but no height and pos [0, 0]
	Each squad control should contain 10 unit rows controlGroupsNoScrollBar already placed on the appropriate coordinates

	Only the squad controls should be moved, resizing the height based on how many roles should be shown.
	Whenever someone leaves a squad, resize the H one role smaller and reload any unit controlsGroup located below the person leaving inside the squad
	Also reposition each squad control below the one that is being resized.
---------------------------------------------------------------------------*/

[QGVAR(scoreboardOpen), {
    if (!(isNull (uiNamespace getVariable [QGVAR(scoreboard), displayNull]))) exitWith {
    	QGVAR(scoreboardUpdate) call CFUNC(localEvent);
    };

    (findDisplay 46) createDisplay QGVAR(scoreboard);
}] call CFUNC(addEventHandler);

// -- Unload background for scoreboard when respawn screen is opened
[QSVAR(SquadScreen_onLoad), {
    [QGVAR(blur), false] call EFUNC(main,blurBackground);
}] call CFUNC(addEventHandler);

// -- Open the scoreboard -- //
GVAR(ppColor) = -1;
[QGVAR(scoreboardLoad), {
    (_this select 0) params ["_display"];
    uiNamespace setVariable [QGVAR(scoreboard), _display];
	["addKeybinds.display", [_display]] call CFUNC(localEvent);

	[QGVAR(blur), true, 0.5, 1] call EFUNC(main,blurBackground);

    [{
        // -- Add mission details -- // (Needs to be compiled once only)
        // -- Add the ticket view for both teams -- // (Update on ticketChanged event)
        QGVAR(scoreboardUpdate) call CFUNC(localEvent);

    }, _display] call CFUNC(execNextFrame);
}] call CFUNC(addEventHandler);

[QGVAR(scoreboardUnload), {
	[QGVAR(blur), false] call EFUNC(main,blurBackground);
}] call CFUNC(addEventHandler);

// -- Refresh the data on the scoreboard -- //
[QGVAR(scoreboardUpdate), {
    private _display = uiNamespace getVariable [QGVAR(scoreboard), displayNull];
    if (isNull _display) exitWith { };

    call FUNC(updateFull);
}] call CFUNC(addEventHandler);


["General", "scoreboard", ["Scoreboard", "Shows scoreboard and playerlist"], {
	if (isNull (uiNamespace getVariable [QGVAR(scoreboard), displayNull])) then {
		if (isNull (uiNamespace getVariable [QEGVAR(RespawnUI,respawnDisplay), displayNull])) then {
			QGVAR(scoreboardOpen) call CFUNC(localEvent);
		};
	} else {
		(uiNamespace getVariable [QGVAR(scoreboard), displayNull]) closeDisplay 1;
	};
	true;
}, { false }, [DIK_P, [false, false, false]], false, true, { _displayIDD in [46, 1005] }] call MFUNC(addKeybind);
