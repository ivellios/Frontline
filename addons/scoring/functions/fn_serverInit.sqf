#include "macros.hpp"
/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Inits scoring system on server
 *
 */

#define __UPDATESPEED 60

/*---------------------------------------------------------------------------
	Basic architecture is as follows:

		All score gets updated only on the server through events (Added in fn_initScoreEvents)
		They get saved to a key, based on the player UID (This supports reconnecting and team switching), which is LOCAL to server only
		On updating a certain player UID, said UID will be marked, which means in the next update cycle it'll be broadcasted to all clients.

		This means that clients only recieve all score updates every 60 seconds. This prevents being able to quickly confirm kills by opening scoreboard
		It also gives us an easy way to indicate to players on how it works (Have a 'update' progressbar on scoreboard).

---------------------------------------------------------------------------*/
GVAR(scoreNamespace) = true call CFUNC(createNamespace); // -- Global
publicVariable QGVAR(scoreNamespace);

GVAR(cancelUpdate) = false;
GVAR(keyToBroadcast) = []; // This keeps check of this users score we need to broadcast in next update cycle
GVAR(scoreOrder) = ["total", "kill", "incap", "death", "work", "aikills"];

[QGVAR(addScore), {
	private _scoreArray = (_this select 0);
	if !((_scoreArray select 0) isEqualType []) then {
		_scoreArray = [_scoreArray];
	};

	private _keyToBroadcast = [];
	{
		_x params ["_category", "_unit", "_amount"];
		// -- Retrieve player current score (If it doesn't exist yet, create array with 0 for all categories) -- //
        if (isPlayer _unit) then {
            private _clientSteamUID = getPlayerUID _unit;
    		private _clientScore = GVAR(scoreNamespace) getVariable _clientSteamUID;
    		if (isNil "_clientScore") then {
    			_clientScore = [];
    			_clientScore resize (count GVAR(scoreOrder));
    			_clientScore = _clientScore apply { 0 };
    		};

    		private _categoryIndex = GVAR(scoreOrder) find (toLower _category);
    		if (_categoryIndex != -1) then {
    			_clientScore set [_categoryIndex, (_clientScore select _categoryIndex) + _amount];
    			GVAR(scoreNamespace) setVariable [_clientSteamUID, _clientScore];

    			// -- Mark this score to be updated on next broadcast cycle -- //
    			GVAR(keyToBroadcast) pushBackUnique _clientSteamUID;
    		};
        };

		nil;
	} count _scoreArray;
}] call CFUNC(addEventHandler);

// -- This broadcasts the score value for the units that have been update since it's last loop -- //
[QGVAR(scoreBroadcast), {
	{
		private _score = GVAR(scoreNamespace) getVariable _x;
		GVAR(scoreNamespace) setVariable [_x, _score, true];
		nil;
	} count GVAR(keyToBroadcast);
	GVAR(keyToBroadcast) = [];
	QGVAR(scoreboardUpdate) call CFUNC(globalEvent);
}] call CFUNC(addEventHandler);


[{
	if (GVAR(cancelUpdate)) exitWith {
		[(_this select 1)] call MFUNC(removePerFrameHandler);
	};
	[QGVAR(scoreBroadcast)] call CFUNC(localEvent);
}, __UPDATESPEED] call MFUNC(addPerFramehandler);

["endMission", {
    GVAR(cancelUpdate) = true;
    [QGVAR(scoreBroadcast)] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);
