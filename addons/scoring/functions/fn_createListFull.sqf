/*
    Function:       FRL_Scoring_fnc_createListFull
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(scoreboard), displayNull];
if (isNull _display) exitWith {};

private _entries = +GVAR(cachedData);
_entries params ["_allEntries", "_rowsNeeded"];

private _sideIndex = 0;
private _xPos = 0;
private _squadIndex = 0;

{
    private _yPos = 0;
    private _sideEntries = _x;
    {
        private _squad = _x;
        [_squad] call FUNC(addSquadEntry);
        nil;
    } count _sideEntries;
    _sideIndex = _sideIndex + 1;
    _xPos = _xPos + (_sideIndex * _SIZE_SIDE_W);
    nil;
} count _allEntries;

// -- Resize any non-used squads to 0 height so no roles are shown -- //
for "_i" from _squadIndex to (_AMOUNT_SQUAD - 1) do {
    private _squadCtrl = (_display displayCtrl _IDC_SCOREBOARD) controlsGroupCtrl (_IDC_MOD_SQUAD * (_i + 1));
    _squadCtrl ctrlSetPosition [0, 0, _SIZE_SIDE_W, 0];
    _squadCtrl ctrlCommit 0;
};
