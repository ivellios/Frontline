/*
    Function:       FRL_Scoring_fnc_addSquadEntry
    Author:         Adanteh
    Description:    Stuff
*/
#include "macros.hpp"

params ["_squad"];
_squad params ["_squadScore", "_squadName", "_squadEntries", "", "_squadDesignator", "_squadType"];

private _squadCtrlIDC = (_IDC_MOD_SQUAD * (_squadIndex + 1));
private _squadCtrl = (_display displayCtrl _IDC_SCOREBOARD) controlsGroupCtrl _squadCtrlIDC;

(_squadCtrl controlsGroupCtrl 52) ctrlSetText _squadDesignator;
(_squadCtrl controlsGroupCtrl 53) ctrlSetText _squadName;
(_squadCtrl controlsGroupCtrl 54) ctrlSetText _squadType;
(_squadCtrl controlsGroupCtrl 55) ctrlSetText ([(str _squadScore), ""] select (_squadScore == -99));
GVAR(scoreboardNamespace) setVariable [(_squadName + "_cache"), _squadCtrlIDC];

private _unitIndex = 1;
{
    [_x, (_IDC_MOD_UNIT * _unitIndex)] call FUNC(addUnitEntry);
    nil;
} count _squadEntries;

_squadCtrl ctrlSetPosition [_xPos, _yPos, _SIZE_SIDE_W, (_unitIndex * _SIZE_UNIT_H)];
(_squadCtrl controlsGroupCtrl 49) ctrlSetPosition [0, 0, _SIZE_SIDE_W, (_unitIndex * _SIZE_UNIT_H)];
_squadCtrl ctrlCommit 0;
(_squadCtrl controlsGroupCtrl 49) ctrlCommit 0;

_yPos = _yPos + (_unitIndex * _SIZE_UNIT_H);
_squadIndex = _squadIndex + 1;
