#include "macros.hpp"
/*
 *	File: fn_serverInitStatLog.sqf
 *	Author: Adanteh
 *	Inits logging for the server
 *
 */

#define __EXTENSION "kurwagenerator9000"

// -- Log the score entry -- //
if ("ping" callExtension __EXTENSION == "pong") then {
	[QGVAR(addScore), {
		private _scoreArray = (_this select 0);
		if !((_scoreArray select 0) isEqualType []) then {
			_scoreArray = [_scoreArray];
		};

		{
			_x params ["", "_unit", "_amount", ["_logtag", ""]];
			if (_logtag != "") then {
				private _name = [_unit] call CFUNC(name);
				private _uid = getPlayerUID _uid;
				private _input = format ["Utils.logging.log('%1','%2','#3')", _logTag, _name, _uid]; // "Utils.logging.log('fo_helper','adanteh','12345678')"
				private _result = _input callExtension __EXTENSION
			};
			nil;
		} count _scoreArray;
	}] call CFUNC(addEventHandler);
};


/*---------------------------------------------------------------------------
	private _in = "('score_total', 'adanteh', 9001, [1,2,3])";
	private _out = _in splitString ","; // ["'score_total'", "'adanteh'", "9001","[1","2","3]"];

	private _in = "('score_total';'adanteh';9001;[1,2,3])";
	private _out = _in splitString ";"; // ["'score_total'","'adanteh'","9001","[1,2,3]"];

---------------------------------------------------------------------------*/
