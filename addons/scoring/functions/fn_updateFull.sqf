/*
    Function:       FRL_Scoring_fnc_updateFull
    Author:         Adanteh
    Description:    Does a full update
*/
#include "macros.hpp"


private _allSquads = +allGroups;
private _unitsHandled = [];

private _sideSizes = [0, 0, 0, 0];
private _allEntries = [[], [], [], []];
private _unassignedGroups = [[], [], [], []];
private _sides = count (call MFUNC(getSides));
_sideSizes resize _sides;
_allEntries resize _sides;
_unassignedGroups resize _sides;

{
    private _squad = _x;
    private _squadSide = side _squad;
    private _squadSideID = [_squadSide] call MFUNC(getSideIndex);
    private _hasUnits = ({ isPlayer _x } count (units _squad)) > 0;

    if (_squadSideID != -1 && _hasUnits) then {
        private _units = +(units _squad);
        private _friendlySquad = (side _squad == playerSide);
        if ((count _units) > 0) then {
            private _squadEntries = [];
            private _squadScore = 0;

            {
                private _clientIDThingie = getPlayerUID _x;
                private _unitName = [_x] call CFUNC(name);
                private _unitScore = GVAR(scoreNamespace) getVariable [_clientIDThingie, []];
                private _unitRoleIcon = ["", (_x getVariable [QSVAR(rkitIcon), ""])] select _friendlySquad;
                _unitScore params [["_total", 0], ["_kills", 0], ["_incaps", 0], ["_deaths", 0], ["_work", 0], ["_aiKills", 0]];
                _squadEntries pushBack [(_total + _work), str(_kills), str(_incaps), str(_deaths), str(_work), _clientIDThingie, _unitName, _unitRoleIcon, str(_aiKills)];
                _squadScore = _squadScore + _total + _work;

                _sideSizes set [_squadSideID, (_sideSizes select _squadSideID) + 1];
                _unitsHandled pushBack _clientIDThingie;
                nil;
            } count _units;
            _squadEntries sort false; // -- Sort descending by score inside squad

            private _squadId = groupID _squad;
            private _unassigned = !(_squadId in MVAR(squadIds));
            if !(_unassigned) then {
                _squadScore = floor (_squadScore / (count _units)); // -- Average score of unit in squad
                private ["_squadName", "_squadType"];
                if (_friendlySquad) then {
                     _squadName = _squad getVariable [QMVAR(gName), _squadId];
                     _squadType = EGVAR(rolesspawns,squadNamespace) getVariable [(format ["%1_name", (_squad getVariable [QMVAR(gType), ""])]), ""];
                } else {
                    _squadName = "";
                    _squadType = "";
                };
                private _squadDesignator = _squadId select [0, 1];
                (_allEntries select _squadSideID) pushBack [_squadScore, _squadName, _squadEntries, _squad, _squadDesignator, _squadType];
            } else {
                (_unassignedGroups select _squadSideID) append _squadEntries;
            };
        };
    };
    nil;
} count _allSquads;

// -- Sort squads descending by their average score and then add the unassigned entry -- //
{
    private _sideEntries = +_x;
    _sideEntries sort false;

    private _unassignedGroup = (_unassignedGroups select _forEachIndex);
    if (count _unassignedGroup > 0) then {
        _unassignedGroup sort false;
        _sideEntries pushBack [-99, "Unassigned", _unassignedGroup, grpNull, "", ""];
    };
    _allEntries set [_forEachIndex, _sideEntries];
} forEach _allEntries;


// -- Sort descending by amount of units per side -- //
_sideSizes sort false;
private _rowsNeeded = _sideSizes select 0;

GVAR(uidsHandledLast) = _unitsHandled;
GVAR(cachedData) = [_allEntries, _rowsNeeded];
call FUNC(createListFull);
/*---------------------------------------------------------------------------
    _allEntries = [
        [ // EAST
            [ // Group 1
                231.5,
                "English",
                [
                    [412, 3, 6, 2, 222, "93421423", "Adanteh"],
                    [231, 1, 5, 3, 170, "23432423", "Gunther"],
                    [132, 1, 2, 3, 150, "09213457", "Leon"]
                ]
            ],
            [],
            []
        ],
        [ // WEST
        ]
    ];
---------------------------------------------------------------------------*/
