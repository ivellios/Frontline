/*
    Function:       FRL_Scoring_fnc_addUnitEntry
    Author:         Adanteh
    Description:    This updates a single unit controlsGroup with good info. Cleanup for non-used isn't needed seeing those are hiddein overflow on the squad groupControls
*/
#include "macros.hpp"

params ["_unitEntry", "_unitCtrlIDC"];

_unitEntry params ["_total", "_kills", "_incaps", "_deaths", "_work", "_clientIDThingie", "_unitName", "_unitRoleIcon", "_aiKills"];

private _unitRow = _squadCtrl controlsGroupCtrl _unitCtrlIDC;

if (_clientIDThingie == (getPlayerUID Clib_Player)) then {
    private _color = [0.995, 0.714, 0.208, 1];
    for "_i" from 60 to 66 do {
        (_unitRow controlsGroupCtrl _i) ctrlSetTextColor _color;
    };
};

(_unitRow controlsGroupCtrl 60) ctrlSetText _unitRoleIcon;
(_unitRow controlsGroupCtrl 61) ctrlSetText _unitName;

(_unitRow controlsGroupCtrl 62) ctrlSetText _kills;
(_unitRow controlsGroupCtrl 63) ctrlSetText _incaps;
(_unitRow controlsGroupCtrl 64) ctrlSetText _aiKills;
(_unitRow controlsGroupCtrl 65) ctrlSetText _deaths;
//(_unitRow controlsGroupCtrl 65) ctrlSetText _work;
(_unitRow controlsGroupCtrl 66) ctrlSetText str(_total);

GVAR(scoreboardNamespace) setVariable [_clientIDThingie, [_squadCtrlIDC, _unitCtrlIDC]];

_unitRow ctrlSetPosition [0, (_unitIndex * _SIZE_UNIT_H), _SIZE_SIDE_W, _SIZE_UNIT_H];
_unitRow ctrlCommit 0;
_unitIndex = _unitIndex + 1;
