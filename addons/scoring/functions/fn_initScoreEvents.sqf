#include "macros.hpp"
/*
 *	File: fn_initScoreEvents.sqf
 *	Author: Adanteh
 *	This should contain all the score events we want. Instead of adding to the other code, make sure they have an event we can hook in
 *	Keep ALL scoring local to this module. All score should be added on server only, ideally without a (extra) broadcast
 *
 */

#define __KILLWORTH 10
#define __INCAPWORTH 5

if (isServer) then {
	//["frl_scoring_addScore", ["total", player, 100, "test"]] call clib_fnc_localEvent;
	// -- Killing someone and +1 on death -- //
	["KilledPlayer", {
		(_this select 0) params ["_victim", "_killers"];
		private _scoreArray = [];
		if !([_victim] isEqualTo _killers) then {
			{
				if ((side group _victim) getFriend (side group _x) < 0.6) then {
					_scoreArray pushBack ["total", _x, __KILLWORTH];
					_scoreArray pushBack ["kill", _x, 1, "kill"];
				} else {
					// -- TEAMKILL!
				};
				nil;
			} count _killers;
		};
		_scoreArray pushBack ["death", _victim, 1, "death"];
		[QGVAR(addScore), _scoreArray] call CFUNC(localEvent);
	}] call CFUNC(addEventhandler);

	// -- Incapping someone -- //
	["IncapPlayer", {
		(_this select 0) params ["_victim", "_killers"];
		private _scoreArray = [];
		{
			if ((side group _victim) getFriend (side group _x) < 0.6) then {
				_scoreArray pushBack ["total", _x, __INCAPWORTH];
				_scoreArray pushBack ["incap", _x, 1, "incap"];
			} else {
				// -- TEAMKILL!
			};
			nil;
		} count _killers;
		if !(_scoreArray isEqualTo []) theN {
			[QGVAR(addScore), _scoreArray] call CFUNC(localEvent);
		};
	}] call CFUNC(addEventhandler);

    ["KilledAI", {
        (_this select 0) params ["_victim", "_killers"];
		private _scoreArray = [];
		if !([_victim] isEqualTo _killers) then {
			{
				if ((side group _victim) getFriend (side group _x) < 0.6) then {
					_scoreArray pushBack ["total", _x, __KILLWORTH / 2];
					_scoreArray pushBack ["aikills", _x, 1, "aikills"];
				} else {
					// -- TEAMKILL!
				};
				nil;
			} count _killers;
		};
		[QGVAR(addScore), _scoreArray] call CFUNC(localEvent);
    }] call CFUNC(addEventHandler);

	// -- Finishing medical treatment -- //
	["TreatmentDone", {
		(_this select 0) params ["_medic", "_action"];
		_points = switch (toLower _action) do {
			case "bandage": { 2; };
			case "adrenaline": { 4; };
			case "morphine": { 3; };
			default { 0 };
		};
		[QGVAR(addScore), [["work", _medic, _points, "medical_treatment"]]] call CFUNC(localEvent);
	}] call CFUNC(addEventHandler);

	// -- Placing a rally point awards the leader with points -- //
	[QEGVAR(RolesSpawns,rallyPlaced), {
		(_this select 0) params ["", "", "_placedBy"];
		[QGVAR(addScore), [["work", _placedBy, 15]]] call CFUNC(localEvent);
	}] call CFUNC(addEventhandler);

	// -- Placing an FO awards the placer with points, plus anyone within range of deployment -- //
	[QEGVAR(RolesSpawns,foPlaced), {
		(_this select 0) params ["", "", "_placedBy"];

		private _scoreArray = [["work", _placedBy, 50, "fo_placed"]];

		// -- Give points to people nearby to, seeing you need those to show up so you can actually deploy -- //
		private _nearPlayerToBuildRadius = [QEGVAR(RolesSpawns,FO_nearPlayerToBuildRadius), -1] call MFUNC(cfgSetting);
		private _placedSide = side group _placedBy;
		private _helpers = ([getPos _placedBy, _nearPlayerToBuildRadius] call MFUNC(getNearUnits));
		_helpers = (_helpers - [_placedBy]) select { (side group _x == _placedSide) && !([_x] call MFUNC(isUnconscious)) };
		{
			_scoreArray pushBack ["work", _x, 25, "fo_helper"];
			nil;
		} count _helpers;

		[QGVAR(addScore), _scoreArray] call CFUNC(localEvent);
	}] call CFUNC(addEventhandler);
};
