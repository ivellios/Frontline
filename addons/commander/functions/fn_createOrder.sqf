/*
    Function:       FRL_Commander_fnc_createOrder
    Author:         Adanteh
    Description:    Handles accepting waypoints, orders, requests, whatever
*/
#include "macros.hpp"

params ["_entry", ["_position", [0, 0, 0]], ["_quiet", false]];

if (isNull GVAR(selectedSquad)) exitWith {
    if !(_quiet) then {
        ["showNotification", ["Select a squad first (Left click the squad marker)", "nope"]] call CFUNC(localEvent);
    };
};

private _index = GVAR(orderIndex) + 1;
GVAR(orderIndex) = _index;
publicVariable QGVAR(orderIndex);

// -- Add height that is missing from mapclick
if (count _position == 2) then {
    _position pushBack 0;
};

private _orderTarget = leader GVAR(selectedSquad);
private _caller = clib_player;

GVAR(namespace) setVariable [format ["order%1", _index], [_entry, _position, _orderTarget, _caller], true];

if (isPlayer _orderTarget) then {
    ["receiveOrder", _orderTarget, [_index]] call CFUNC(targetEvent);
} else {
    ["receiveAnswer", [_index, 1]] call CFUNC(localEvent);
    [QGVAR(orderAI), [_entry, _position, GVAR(selectedSquad)]] call CFUNC(serverEvent);
};
