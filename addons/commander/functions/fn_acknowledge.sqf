/*
    Function:       FRL_Commander_fnc_acknowledge
    Author:         Adanteh
    Description:    Handles accepting waypoints, orders, requests, whatever
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params [["_mode", "receiveOrder"], ["_args", []]];

switch (toLower _mode) do {

    case "receiveorder": {
        _args params [["_index", 0]];
        private _orderData = GVAR(namespace) getVariable (format ["order%1", _index]);
        if (isNil "_orderData") exitWith { };
        _orderData params ["_entry", "_position", "_orderTarget", "_commander"];

        private _orderName = GVAR(namespaceOptions) getVariable [_entry + "_displayName", ""];
        private _orderText = format ["%1 at %2", _orderName, [_position] call MFUNC(getNearestName)];
        private _text = format ["Commander %1 wants you to %2", [_commander] call CFUNC(name), _orderText];

        // -- Show F5/F6 box in botton right to accept order
        [[_commander, _index, _text, {
            params ["", "_index", "_answer"];
            ["answerLocal", [_index, _answer]] call FUNC(acknowledge);
        }, 15]] call EFUNC(voting,voteHUD);
    };

    case "answerlocal": {
        _args params [["_index", 0], ["_answer", 0]];
        (GVAR(namespace) getVariable (format ["order%1", _index])) params ["_entry", "_position", "_orderTarget", "_commander"];
        ["receiveAnswer", _commander, [_index, _answer]] call CFUNC(targetEvent);

        if (_answer == 1) then {
            [_entry, _position] call FUNC(setOrder);
        };
    };

    case "receiveanswer": {
        _args params [["_index", 0], ["_answer", 0]];
        (GVAR(namespace) getVariable (format ["order%1", _index])) params ["_entry", "_position", "_orderTarget", "_commander"];

        private _orderName = GVAR(namespaceOptions) getVariable [_entry + "_displayName", ""];
        private _orderText = format ["%1 at %2", _orderName, [_position] call MFUNC(getNearestName)];

        if (_answer == 1) then {
            private _text = format ["%1 accepted order to<br />%2", [_orderTarget] call CFUNC(name), _orderText];
            ["showNotification", [_text]] call CFUNC(localEvent);

        } else {
            private _text = format ["%1 declined order to<br /> %2", [_orderTarget] call CFUNC(name), _orderText];
            ["showNotification", [_text]] call CFUNC(localEvent);
        };

    };
};
