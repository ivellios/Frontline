/*
    Function:       FRL_Commander_fnc_clientInit
    Author:         Adanteh
    Description:    Adds static weapon system. Allows deploying one, like RP, replacing old ones, moving and rearming
*/
#define __INCLUDE_DIK
#include "macros.hpp"

[QUOTE(ADDON), configFile >> "FRL" >> "cfgCommander"] call MFUNC(cfgSettingLoad);
GVAR(handleList) = [];
GVAR(markerNamespace) = false call CFUNC(createNamespace);

GVAR(selectedSquad) = grpNull;
GVAR(orderMouseOverGroup) = grpNull;
GVAR(isCommander) = false;
GVAR(hasOrder) = false;

["receiveOrder", { ["receiveOrder", (_this select 0)] call FUNC(acknowledge) }] call CFUNC(addEventHandler);
["receiveAnswer", { ["receiveAnswer", (_this select 0)] call FUNC(acknowledge) }] call CFUNC(addEventHandler);
[{ call FUNC(drawAllOrders); }, 5] call MFUNC(addPerFramehandler);

{
    [_x, {
        call FUNC(drawAllOrders);
    }] call CFUNC(addEventHandler);
} forEach [
    "playerChanged", //  Object to draw line from changes
    "groupChanged", // Group changes, so different order
    "currentOrderChanged" // Order changed by accepting new one
];

[QGVAR(createOrder), { (_this select 0) call FUNC(createorder); }] call CFUNC(addEventHandler);
[QGVAR(useSkill), { (_this select 0) call FUNC(clickSkill); }] call CFUNC(addEventHandler);

// -- Ctrl right click to open up order menu
["map.RightClick.short", { ["right", _this] call FUNC(handleMapClick) }] call CFUNC(addEventHandler);
["map.LeftClick.short", { ["left", _this] call FUNC(handleMapClick) }] call CFUNC(addEventHandler);

addMissionEventHandler ["Map", {
    params ["_mapIsOpened", "_mapIsForced"];
    if (!_mapIsForced && _mapIsOpened && GVAR(isCommander)) then {
        call FUNC(showSkillstatus);
    } else {
        {
            ["progressHudHide", [_x]] call CFUNC(localEvent);
        } forEach GVAR(skillHUDs);
        GVAR(skillHUDs) = [];
    };
}];

[	"General", // -- Keybind category
	"Deleteorder", // -- Keybind slug (Unique name within category)
	["Delete Order", "Deletes the mouseover order"], // -- Display name and tooltip
	{ false; }, // -- Keydown code
	{ _this call FUNC(deleteMouseOver) }, // -- Keyup code
	[DIK_DELETE, [false, false, false]], // -- Keybind to use by default (Can also use [DIK_CODE, [_shift, _ctrl, _alt]])
	false, // -- CBA Compat (Ignore)
	false // -- If keyup code should respect same modifier keys (Usually this is off)
] call MFUNC(addKeybind);
