/*
    Function:       FRL_Commander_fnc_drawAllOrders
    Author:         Adanteh
    Description:    Draws orders for your group, or for all groups (if commander). Also handles cleaiing up old icons
*/
#include "macros.hpp"

private _allGroups = if (GVAR(isCommander) || { leader Clib_Player isEqualTo Clib_Player }) then {
    allGroups select { side _x == playerSide };
} else {
    [group Clib_Player];
};

private _handles = [];
{
    private _handle = [_x] call FUNC(drawOrders);
    if (_handle != "") then {
        _handles pushBack _handle;
    };
} forEach _allGroups;

// -- Rmemove all unused visual parts
private _unused = GVAR(handleList) - _handles;
{
    [_x] call CFUNC(remove3Dgraphics);
    [_x] call CFUNC(removeMapGraphicsGroup);
} forEach _unused;
GVAR(handleList) = _handles;
