/*
    Function:       FRL_Commander_fnc_drawOrders
    Author:         Adanteh
    Description:    Draws a shiny order (On map / 3D)
*/
#include "macros.hpp"
#define __LINE_COLOR [0.0, 0.76, 0.48, 1]

params [["_group", group Clib_Player]];

private _handleName = format [QGVAR(icon_%1), groupID _group];
private _myGroup = _group isEqualTo (group Clib_Player);
private _currentOrder = _group getVariable [QGVAR(currentOrder), []];
private _return = "";

// -- Keep track of which marker refers to which group (used to get the group from mouseover)
GVAR(markerNamespace) setVariable [_handleName, _group];

if !(_currentOrder isEqualTo []) then {
    _currentOrder params ["_entry", "_pos"];

    private _text = GVAR(namespaceOptions) getVariable [_entry + "_displayName", ""];
    private _icon = GVAR(namespaceOptions) getVariable [_entry + "_3dIcon", ""];
    private _iconMap = GVAR(namespaceOptions) getVariable [_entry + "_mapIcon", ""];

    // -- Add shiny map marker
    private _centerUnit = [leader _group, clib_player] select _myGroup;
    private _parts = [
        ["ICON", _iconMap, __LINE_COLOR, _pos, 20, 20, 0],
        ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _pos, 25, 25, 0, toUpper _text, 2, 0.075],
        ["LINE", _pos, _centerUnit, __LINE_COLOR]
    ];

    [_handleName, _parts] call CFUNC(addMapGraphicsGroup);
    [_handleName] call FUNC(orderMouseOver);

    // -- Add line to squad radar (Only for your own group, not when viewing all waypoints)
    if (_myGroup) then {
        GVAR(hasOrder) = true;
        ["radar.addPoint", [QGVAR(currentOrder), "line", [__LINE_COLOR, { _realCenter }, _pos]]] call CFUNC(localEvent);
        [_handleName, [
        ["ICON",
            _icon,
            __LINE_COLOR,
            _pos,
            1.35,
            1.35,
            0,
            toUpper _text,
            2,
            0.035,
            "PuristaSemiBold",
            "Center",
        true]
        ]] call CFUNC(add3DGraphics);
    };

    _return = _handleName;
} else {
    if (_myGroup) then {
        if (GVAR(hasOrder)) then {
            GVAR(hasOrder) = false;
            ["radar.removePoint", [QGVAR(currentOrder)]] call CFUNC(localEvent);
        };
    };
};

_return;
