/*
    Function:       FRL_Commander_fnc_orderMouseOverText
    Author:         Adanteh
    Description:    Adds the mouseover for order markers
*/
#include "macros.hpp"

params ["_group", "_map", "_ctrlGrp", "_position", "_givenAt"];

private _timeSinceOrder = abs (_givenAt - serverTime);
private _squadType = _group getVariable [QMVAR(gType), ""];
private _squadTypeName = EGVAR(rolesspawns,squadNamespace) getVariable [(format ["%1_name", _squadType]), ""];
private _textToShow = format ["Leader: %1<br />", [(leader _group)] call CFUNC(name)];
_textToShow = format ["%1Name: %2<br />", _textToShow, groupID _group];
_textToShow = format ["%1Type: %2<br />", _textToShow, _squadTypeName];
_textToShow = format ["%1Given %2 ago<br />", _textToShow, [_timeSinceOrder, "MM:SS"] call bis_fnc_secondsToString];

// -- We need the text here, so we can adjust the box size
(_ctrlGrp controlsGroupCtrl 3) ctrlSetStructuredText parseText (format ["<t>%1</t>", _textToShow]);

private _textHeight = ctrlTextHeight (_ctrlGrp controlsGroupCtrl 3);
private _pos = _map ctrlMapWorldToScreen _position;
_pos set [0, (_pos select 0) + 15 / 640];
_pos set [1, (_pos select 1)];
_pos append [GRIDX(10), _textHeight + GRIDY(1.2) + ([GRIDY(0.4), 0] select (_textHeight == 0))];
_ctrlGrp ctrlSetPosition _pos;
_ctrlGrp ctrlShow true;
_ctrlGrp ctrlCommit 0;
