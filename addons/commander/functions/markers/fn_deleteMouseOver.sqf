/*
    Function:       FRL_Commander_fnc_deleteMouseOver
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _handled = false;
if !(isNull GVAR(orderMouseOverGroup)) then {
    private _canDelete = call {
        if (leader GVAR(orderMouseOverGroup) isEqualTo Clib_Player) exitWith { true };
        if (GVAR(isCommander)) exitWith { true };
        false;
    };
    if (_canDelete) then {
        GVAR(orderMouseOverGroup) setVariable [QGVAR(currentOrder), nil, true];
        ["currentOrderChanged", GVAR(orderMouseOverGroup)] call CFUNC(targetEvent);
        GVAR(orderMouseOverGroup) = grpNull;
        _handled = true;
    };
};

_handled
