/*
    Function:       FRL_Commander_fnc_orderMouseOver
    Author:         Adanteh
    Description:    Adds the mouseover for order markers
*/
#include "macros.hpp"

params ["_varName"];

[
    _varName,
    "hoverin",
    {
        (_this select 0) params ["_map", "_xPos", "_yPos"];
        (_this select 1) params ["_varName"];

        if (_varName isEqualTo GVAR(orderMouseOver)) exitWith {};
        GVAR(orderMouseOver) = _varName;
        GVAR(orderMouseOverGroup) = GVAR(markerNamespace) getVariable [_varName, grpNull];
        private _group = GVAR(orderMouseOverGroup);

		// -- Create UI element
		private _idd = ctrlIDD (ctrlParent _map);
		private _controlName = format [QGVAR(orderInfo_%1), _idd];
        private _ctrlGrp = uiNamespace getVariable [_controlName, controlNull];
        if (isNull _ctrlGrp) then {
            _ctrlGrp = (ctrlParent _map) ctrlCreate ["FRL_RscOrderMouseOver", -1];
            uiNamespace setVariable [_controlName, _ctrlGrp];
        };

        (_group getVariable [QGVAR(currentOrder), []]) params ["_entry", "_position", "_givenAt"];
        private _text = GVAR(namespaceOptions) getVariable [_entry + "_displayName", ""];
        private _icon = GVAR(namespaceOptions) getVariable [_entry + "_3dIcon", ""];

        (_ctrlGrp controlsGroupCtrl 1) ctrlSetText toUpper _text;
        (_ctrlGrp controlsGroupCtrl 4) ctrlSetText _icon;

		// -- Keep the data updated
		if (GVAR(orderMarkerPFH) != -1) then {
            GVAR(orderMarkerPFH) call MFUNC(removePerFrameHandler);
        };

		GVAR(orderMarkerPFH) = [{
            params ["_args", "_id"];
            _args params ["_group", "_controlName", "_map", "_position", "_givenAt"];

			private _ctrlGrp = (uiNamespace getVariable [_controlName, controlNull]);
			if (isNull _ctrlGrp || (isNull GVAR(orderMouseOverGroup))) exitWith {
                _ctrlGrp ctrlShow false;
                _ctrlGrp ctrlCommit 0;
				[_id] call MFUNC(removePerFrameHandler);
			};

			[_group, _map, _ctrlGrp, _position, _givenAt] call FUNC(orderMouseOverText);
        }, 0.5, [_group, _controlName, _map, _position, _givenAt]] call MFUNC(addPerFramehandler);
    },
    _varName
] call CFUNC(addMapGraphicsEventHandler);

[
    _varName,
    "hoverout",
    {
        (_this select 0) params ["_map", "_xPos", "_yPos"];
        (_this select 1) params ["_varName"];
		private _idd = ctrlIDD (ctrlParent _map);
		private _controlName = format [QGVAR(orderInfo_%1), _idd];

        GVAR(orderMouseOverGroup) = grpNull;
        if (GVAR(orderMouseOver) isEqualTo _varName) then {
            GVAR(orderMouseOver) = "";
        };

		if (GVAR(orderMarkerPFH) != -1) then {
            GVAR(orderMarkerPFH) call MFUNC(removePerFrameHandler);
			GVAR(orderMarkerPFH) = -1;
        };

        private _ctrlGrp = uiNamespace getVariable [_controlName, controlNull];
        if (!isNull _ctrlGrp) then {
            _ctrlGrp ctrlShow false;
            _ctrlGrp ctrlCommit 0;
        };
    },
    _varName
] call CFUNC(addMapGraphicsEventHandler);
