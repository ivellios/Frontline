/*
    Function:       FRL_Commander_fnc_handleMapClick
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params ["_mode", ["_args", []]];

private _return = false;
switch (toLower _mode) do {
    case "left": {
        if ([DIK_LCONTROL] call MFUNC(keyPressed)) exitWith { };
        if ([DIK_LSHIFT] call MFUNC(keyPressed)) exitWith { };

        private _group = EGVAR(unitMarkers,currentHoverGroup);
        if !(isNull _group) then {
            GVAR(selectedSquad) = _group;
            [QGVAR(squadSelected), [_group]] call CFUNC(localEvent);
        } else {
            GVAR(selectedSquad) = objNull;
        };

        _return = false;
    };

    case "right": {
        if ([DIK_LCONTROL] call MFUNC(keyPressed)) then {

            if !(GVAR(isCommander)) exitWith { };
            createDialog QGVAR(orderDialog);

        };
    };
};

_return;
