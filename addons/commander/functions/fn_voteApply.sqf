/*
    Function:       FRL_Commander_fnc_voteApply
    Author:         Adanteh
    Description:    Handles voting to apply for commander
*/
#include "macros.hpp"


params ["_mode", "_args"];
switch (toLower _mode) do {
    case "start": { // -- Local to all units on one side
        _args params ["_commander", "_uid", "_spammer"];
        if (Clib_Player isEqualTo _commander) exitWith {
            if (_spammer) then {
                ["You already recently called a vote.<br />Wait a while before starting a new one", "warning"] call MFUNC(notificationShow);
            } else {
                ["Vote to become commander started", "ok"] call MFUNC(notificationShow);
            };
        };
        if (_spammer) exitWith {};

        private _text = "";
        private _currentCommander = GVAR(namespace) getVariable [str side group CLib_Player, objNull];
        if (isNull _currentCommander) then {
            _text = format ["%1 wants become commander", [_commander] call CFUNC(name)];
        } else {
            _text = format ["%1 wants to take command from %2", [_commander] call CFUNC(name), [GVAR(namespace) getVariable [str playerSide, objNull]] call CFUNC(name)];
        };
        private _length = ([QGVAR(voteLength), 120] call MFUNC(cfgSetting));
        [[_commander, _uid, _text, {
            params ["_commander", "_uid", "_answer"];
            [QGVAR(applyVoteAnswer), [_commander, _uid, _answer]] call CFUNC(serverEvent);
        }, _length]] call EFUNC(voting,voteHUD);
    };

    case "startserver": { // -- Local  to server
        _args params ["_commander", "_side"];
        private _uid = getPlayerUID _commander;
        private _percentageRequired = ([QGVAR(percentageRequired), 40] call MFUNC(cfgSetting)) / 100;
        private _votesRequired = floor (({ side group _x isEqualTo _side } count allPlayers) * _percentageRequired); // -- 50%
        private _timeout = serverTime + (([QGVAR(voteLength), 120] call MFUNC(cfgSetting)) + 2.5);
        private _timeSinceLast = GVAR(namespace) getVariable [_uid + "_lastTime", -500];
        private _voteSpam = (serverTime - _timeSinceLast) < ([QGVAR(voteCooldown), 120] call MFUNC(cfgSetting));

        if (!isMultiplayer || !isDedicated) then { _votesRequired = 0 };
        [QGVAR(applyVoteStart), _side, [_commander, _uid, _voteSpam]] call CFUNC(targetEvent);

        if (_voteSpam) exitWith { }; // -- Stop someone from constantly asking the same thing
        GVAR(namespace) getVariable [_uid + "_lastTime", serverTime];
        GVAR(namespace) setVariable [_uid + "_yes", 0];
        GVAR(namespace) setVariable [_uid + "_no", 0];

        // -- Create loop to handle votes
        [{
            (_this select 0) params ["_commander", "_side", "_uid", "_timeout", "_votesRequired"];
            private _votes = GVAR(namespace) getVariable [_uid + "_yes", 0];
            private _voteSucceeded = (_votes >= _votesRequired);
            if (_voteSucceeded || (serverTime > _timeout)) exitWith {
                [_this select 1] call MFUNC(removePerFrameHandler);

                // -- Get new playerobject, in case it changed
                private _unit = (allPlayers select { getPlayerUID _x isEqualTo _uid }) param [0, objNull];
                if (isNull _unit) exitWith { };

                [QGVAR(applyVoteEnd), _unit, [_unit, _side, _uid, _voteSucceeded]] call CFUNC(targetEvent);
            };
        }, 1, [_commander, _side, _uid, _timeout, _votesRequired]] call MFUNC(addPerFrameHandler);


    };

    case "end": { // -- Local to person trying to become commander
        _args params ["_unit", "_side", "_uid", "_voteSucceeded"];
        if !(_unit isEqualTo Clib_Player) exitWith { };
        if !(_side isEqualTo playerSide) exitWith { };

        if (_voteSucceeded) then {
            ["Vote to become commander succeeded", "ok"] call MFUNC(notificationShow);
            [Clib_Player, false] call FUNC(assignCommander);
        } else {
            ["Vote to become commander failed<br />Not enough Yes votes received", "nope"] call MFUNC(notificationShow);
        };
    };


    case "answer": { // -- Local to server
        _args params ["_commander", "_uid", "_answer"];
        if (_answer isEqualTo 1) then {
            private _votes = GVAR(namespace) getVariable [_uid + "_yes", 0];
            GVAR(namespace) setVariable [_uid + "_yes", _votes + 1];
        };
        if (_answer isEqualTo 0) then {
            private _votesYes = GVAR(namespace) getVariable [_uid + "_no", 0];
            GVAR(namespace) setVariable [_uid + "_yes", _votesYes + 1];
        };
    };
};

true;
