/*
    Function:       FRL_Commander_fnc_loadMenu
    Author:         Adanteh
    Description:    (Un)loads the spotting menu and adds all EHs. Called by onLoad/onUnload dialog entries.
	Example:		["onLoad"] call FRL_Commander_fnc_loadMenu;
*/
#include "macros.hpp"
#define CTRL(x) ((uiNamespace getVariable QGVAR(orderDialog)) displayCtrl x)

params ["_mode", ["_args", []]];
private _return = true;

switch (toLower _mode) do {
    case "onload": {
        private _clickedPos = uiNamespace getVariable ["RMB_pos", [0, 0]];
    	private _worldPos = (uiNamespace getVariable "_map") ctrlMapScreenToWorld _clickedPos;
    	private _tempMarker = createMarkerLocal [QGVAR(tempMarker), _worldPos];
    	_tempMarker setMarkerShapeLocal "ICON";
    	_tempMarker setMarkerTypeLocal "mil_dot";

        // -- Don't draw notifications / messages in this dialog
        (uiNamespace getVariable [QGVAR(orderDialog), displayNull]) setVariable [QSVAR(noOverlay), true];

        private _previousCategory = uiNamespace getVariable [QGVAR(curCategory), ""];
        private _categoryIndexSel = 0;
    	private _categories = +(GVAR(namespaceOptions) getVariable ["categories", []]);
    	{
            private _name = GVAR(namespaceOptions) getVariable [_x + "_displayname", _x];
    		private _lbIndex = CTRL(3) lbAdd _name;
    		CTRL(3) lbSetData [_lbIndex, _x];
    		CTRL(3) lbSetValue [_lbIndex, _forEachIndex];
            if (_x isEqualTo _previousCategory) then {
                _categoryIndexSel = _forEachIndex
            };
    	} forEach _categories;

        ["categorychanged", [CTRL(3), _categoryIndexSel]] call FUNC(loadMenu);
    	CTRL(3) ctrlAddEventHandler ["ToolboxSelChanged", { ["categorychanged", _this] call FUNC(loadMenu) }];
        CTRL(4) ctrlAddEventHandler ["lbSelChanged", { ["selchanged", _this] call FUNC(loadMenu) }];
        CTRL(3) lbSetCurSel _categoryIndexSel;

        ["setwindowposition"] call FUNC(loadMenu);
    };


    // -- Top category changed
    case "categorychanged": {
        _args params ["_control", "_selection"];

        private _category = _control lbData _selection;
        private _category = (GVAR(namespaceOptions) getVariable ["categories", []]) select _selection;

        uiNamespace setVariable [QGVAR(curCategory), _category];
        [_category] call FUNC(populateMenu);
        _return = true;
    };


    // -- Click an entry in the list
    case "selchanged": {
        _args params ["_control", "_selection"];
        if (_selection == -1) exitWith { };
        private _entry = _control lbData _selection;

        if (_entry != "") then {
            private _category = uiNamespace getVariable [QGVAR(curCategory), ""];
            private _categoryEvent = GVAR(namespaceOptions) getVariable [_category + "_eventName", ""];
            private _pos = getMarkerPos QGVAR(tempMarker);

            [format [QGVAR(%1), _categoryEvent], [_entry, _pos]] call CFUNC(localEvent);
            (uiNamespace getVariable QGVAR(orderDialog)) closeDisplay 2;
        };
        _return = true;
    };


    // -- Adjust the window next to your cursor, if it extends outside the screen, flip it to the other side
    case "setwindowposition": {
        private _position = ctrlPosition CTRL(100);
        _position set [0, (_clickedPos select 0) + 0.015];
        _position set [1, (_clickedPos select 1) - 0.15];

        // -- Prevent the window from going below the screen (Move it up if that's the case) -- //
        if (((_position select 1) + (_position select 3)) > (safeZoneY + safeZoneH)) then {
            _position set [1, (safeZoneY + safeZoneH) - (_position select 3)];
        };

        // -- Prevent the window from going to the right-side of screen (Move to left if that's the case)  -- //
        if (((_position select 0) + (_position select 2)) > (safeZoneX + safeZoneW)) then {
            _position set [0, (safeZoneX + safeZoneW) - (_position select 2)];
        };

        CTRL(100) ctrlSetPosition _position;
        CTRL(100) ctrlCommit 0;

        // -- Unload the menu if right-clicked anywhere or if clicked outside the spotting menu -- //
        (uiNamespace getVariable QGVAR(orderDialog)) displayAddEventHandler ["mouseButtonDown", {
            private _groupPos = ctrlPosition CTRL(100);
            params ["", "_button", "_posX", "_posY"];

            if ((_button == 1) || {
                (_posX < (_groupPos select 0)) } || {
                (_posY < (_groupPos select 1)) } || {
                (_posX > ((_groupPos select 0) + (_groupPos select 2)))  } || {
                (_posY > ((_groupPos select 1) + (_groupPos select 3)))  }) then {
                (uiNamespace getVariable QGVAR(orderDialog)) closeDisplay 2;
            };
            true
        }];
    };

    case "onunload": {
        uiNamespace setVariable [QGVAR(categoryList), nil];
        deleteMarkerLocal QGVAR(tempMarker);
    };
};

_return;
