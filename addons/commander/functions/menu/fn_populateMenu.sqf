/*
    Function:       FRL_Commander_fnc_populateMenu
    Author:         Adanteh
    Description:    Adds all entries into spotting from config
	Example:		[0] call FRL_Commander_fnc_populateMenu
*/
#include "macros.hpp"
#define CTRL(x) ((uiNamespace getVariable QGVAR(orderDialog)) displayCtrl x)

params ["_category", "_position"];
private _categoryEntries = +(GVAR(namespaceOptions) getVariable [_category + "#options", ""]);
lbClear CTRL(4);

{
    private _entry = _x;
    private _displayName = GVAR(namespaceOptions) getVariable [_entry + "_displayName", ""];
    private _listPath = GVAR(namespaceOptions) getVariable [_entry + "_listPath", ""];
    private _conditionShow = GVAR(namespaceOptions) getVariable [_entry + "_conditionShow", { true }];
    private _side = GVAR(namespaceOptions) getVariable [_entry + "#SIDE", sideLogic];
    private _tooltip = GVAR(namespaceOptions) getVariable [_entry + "#TOOLTIP", ""];

    if (_side == sideLogic || (_side == playerSide)) then {
        private _show = ([Clib_Player, playerSide]) call _conditionShow;
        if (_show) then {
            private _lbIndex = CTRL(4) lbAdd _displayName;
        	CTRL(4) lbSetPicture [_lbIndex, _listPath];
        	CTRL(4) lbSetTooltip [_lbIndex, _tooltip];
        	CTRL(4) lbSetData [_lbIndex, _entry];
        	CTRL(4) lbSetPictureColor [_lbIndex, [1, 1, 1, 1]];
        };
    };
} forEach _categoryEntries;
