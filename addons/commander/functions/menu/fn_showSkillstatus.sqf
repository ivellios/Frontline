/*
    Function:       FRL_Commander_fnc_showSkillstatus
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"


private _categoryEntries = +(GVAR(namespaceOptions) getVariable ["Skills" + "#options", ""]);
private _index = 0;

{
    _x params ["_txt", "_icon"];
    private _ID = "cmdHint_" + str _index;
    ["progressHudShow", [
        _ID,
        {
            (_controlsGroup controlsGroupCtrl 2) ctrlSetText _icon;
            (_controlsGroup controlsGroupCtrl 3) ctrlSetText _txt;
            //(_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.5,0.5,0.5,0];
            //(_controlsGroup controlsGroupCtrl 5) progressSetPosition 0;
        },
        {},
        [],
        0, // this is the refresh rate
        [(_index + 7) * GRIDY(1.4), GRIDY(1.4)] // _forcepos
        //((count GVAR(currentProgressHuds) - 1) * GRIDY(1.4)), SIZE_WIDTH_PROGRESS, GRIDY(1.4)];
    ]] call CFUNC(localEvent);
    _index = _index + 1;
    GVAR(skillHUDs) pushBackUnique _ID;
} foreach [["Ctrl + RMB to order", "\pr\frl\addons\commander\data\suppress.paa"], ["LMB to select a squad", "\pr\frl\addons\commander\data\support.paa"]];

{
    private _entry = _x;
    private _displayName = GVAR(namespaceOptions) getVariable [_entry + "_displayName", ""];
    private _listPath = GVAR(namespaceOptions) getVariable [_entry + "_listPath", ""];
    private _conditionShow = GVAR(namespaceOptions) getVariable [_entry + "_conditionShow", { true }];
    private _side = GVAR(namespaceOptions) getVariable [_entry + "#SIDE", sideLogic];
    private _tooltip = GVAR(namespaceOptions) getVariable [_entry + "#TOOLTIP", ""];
    private _cooldown = (GVAR(namespaceOptions) getVariable [_entry + "_cooldown", 15]) * 60;

    if (_side == sideLogic || (_side == playerSide)) then {
        private _show = ([Clib_Player, playerSide]) call _conditionShow;
        if (_show) then {
            private _hudID = "cmdskill_" + str _index;
            ["progressHudShow", [
                 _hudID,
                {

                    (_controlsGroup controlsGroupCtrl 2) ctrlSetText "\pr\frl\addons\commander\data\attack.paa";
                    (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.222,0.7,0.222,0.6];
                },
                {
                    params ["_displayName", "_entry", "_cooldown"];

                    private _prepTime = (GVAR(namespaceOptions) getVariable [_entry + "_canUseAfter", 15]) * 60;
                    private _inPreperation = (time < _prepTime);

                    // add check if the skill is ready here
                    // (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor (["GUI", "BCG_RGB"] call BIS_fnc_displayColorGet)
                    if (_inPreperation) then {
                        (_controlsGroup controlsGroupCtrl 3) ctrlSetText _displayName + (format [" in %1", ([_prepTime - time, "MM:SS", false] call BIS_fnc_secondsToString)]);
                        (_controlsGroup controlsGroupCtrl 5) progressSetPosition ( 1 - (_prepTime - time) / _prepTime);
                        (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0,0,0,0.66];
                    } else {
                        private _usedAt = GVAR(namespace) getVariable [[_entry, "usedAt"] joinString "#", -1e9];
                        private _percentage =  1 - (serverTime - _usedAt) / _cooldown;

                        if (_percentage > 0) then {
                            (_controlsGroup controlsGroupCtrl 3) ctrlSetText _displayName + (format [" in %1", ([_percentage * _cooldown, "MM:SS", false] call BIS_fnc_secondsToString)]);
                            (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
                            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [1,0,0,0.5];
                        } else {
                            (_controlsGroup controlsGroupCtrl 3) ctrlSetText _displayName + " is ready!";
                            (_controlsGroup controlsGroupCtrl 5) progressSetPosition 1;
                            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.222,0.7,0.222,0.75];
                        };
                    };
                },
                [_displayName, _entry, _cooldown],
                5, // this is the refresh rate
                [(_index + 7) * GRIDY(1.4), GRIDY(1.4)] // _forcepos
                //((count GVAR(currentProgressHuds) - 1) * GRIDY(1.4)), SIZE_WIDTH_PROGRESS, GRIDY(1.4)];
            ]] call CFUNC(localEvent);
            GVAR(skillHUDs) pushBackUnique _hudID;
            _index = _index + 1;
        };
    };
} forEach _categoryEntries;
