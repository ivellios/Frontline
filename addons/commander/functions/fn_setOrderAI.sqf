/*
    Function:       FRL_Commander_fnc_setOrderAI
    Author:         Adanteh
    Description:    Sets a waypoint for the given group (one of the vanilla types)
*/
#include "macros.hpp"

params ["_entry", "_pos", "_group"];

// -- Set AI data
private _behaviour = GVAR(namespaceOptions) getVariable [_entry + "_waypointBehaviourAI", "AWARE"];
private _type = GVAR(namespaceOptions) getVariable [_entry + "_waypointTypeAI", "MOVE"];
private _speed = GVAR(namespaceOptions) getVariable [_entry + "_waypointSpeed", "FULL"];
[_group, _pos, _type, _behaviour, nil, _speed] call EFUNC(ai,setWaypoint);

_group setVariable [QGVAR(currentOrder), [_entry, _pos, serverTime], true];
