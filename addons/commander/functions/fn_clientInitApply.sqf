/*
    Function:       FRL_Commander_fnc_clientInitApply
    Author:         Adanteh
    Description:    Handles the apply/kick for commander button from respawn menu
*/
#define __INCLUDE_DIK
#include "macros.hpp"
#define CTRL(var1) (_ctrlGroup controlsGroupCtrl var1)

GVAR(skillHUDs) = [];

[QGVAR(applyVoteStart), { ["start", (_this select 0)] call FUNC(voteApply) }] call CFUNC(addEventHandler);
[QGVAR(applyVoteEnd), { ["end", (_this select 0)] call FUNC(voteApply) }] call CFUNC(addEventHandler);


[QGVAR(applyButton), {
    private _commander = Clib_Player;
    if (GVAR(isCommander)) then {
        [_commander, true] call FUNC(assignCommander);
    } else {
        // -- Create a little cute side vote
        [QGVAR(applyVoteServer), [_commander, playerSide]] call CFUNC(serverEvent);
    };
}] call CFUNC(addEventHandler);


// -- Update the button /
[QGVAR(onLoadCtrl), {
    (_this select 0) params [["_ctrlGroup", controlNull]];
    if (isNull _ctrlGroup) then {
        _ctrlGroup = uiNamespace getVariable [QGVAR(ctrlGroup), controlNull];
    } else {
        uiNamespace setVariable [QGVAR(ctrlGroup), _ctrlGroup];
    };
    if (isNull _ctrlGroup) exitWith { };

    _ctrlGroup ctrlSetFade 0;
    _ctrlGroup ctrlCommit 0;
    private "_commanderName";
    // -- Update button text and name
    private _sideCommander = GVAR(namespace) getVariable [str playerSide, objNull];
    if (isNull _sideCommander) then {
        _commanderName = "<Empty>";
        CTRL(704) ctrlSetText "APPLY";
    } else {
        _commanderName = [_sideCommander] call CFUNC(name);
        if (_sideCommander isEqualTo Clib_Player) then {
            CTRL(704) ctrlSetText "LEAVE";
        };
    };
    CTRL(703) ctrlSetText _commanderName;
}] call CFUNC(addEventHandler);

// -- Update the commander variable
["playerChanged", {
    (_this select 0) params ["_newUnit"];
    if (GVAR(isCommander)) then {
        GVAR(namespace) setVariable [str playerSide, _newUnit, true];
    };
}] call CFUNC(addEventHandler);

["playerSideChanged", {
    (_this select 0) params ["_newSide", "_oldSide"];
    if (GVAR(isCommander)) then {
        [Clib_Player, true] call FUNC(assignCommander);
    };
    QGVAR(onLoadCtrl) call CFUNC(localEvent); // -- Update respawn screen if it's open
}] call CFUNC(addEventHandler);

// -- Give notification that commander changed
["commanderChanged", {
    (_this select 0) params ["_sideCommander", "_oldCommander"];
    if (Clib_Player isEqualTo _sideCommander) then {
        GVAR(isCommander) = true;
    } else {
        GVAR(isCommander) = false;
        {
            ["progressHudHide", [_x]] call CFUNC(localEvent);
        } forEach GVAR(skillHUDs);
        GVAR(skillHUDs) = [];
    };

    if (isNull _sideCommander) then {
        [format ["%1 stepped down as commander", [_oldCommander] call CFUNC(name)]] call MFUNC(notificationShow);
    } else {
        [format ["%1 is the new commander", [_sideCommander] call CFUNC(name)]] call MFUNC(notificationShow);
    };
    QGVAR(onLoadCtrl) call CFUNC(localEvent); // -- Update respawn screen if it's open
}] call CFUNC(addEventHandler);
