/*
    Function:       FRL_Commander_fnc_assignCommander
    Author:         Adanteh
    Description:    Assign unit as commander
*/
#include "macros.hpp"

[{
    params ["_unit", ["_unassign", false]];

    private _side = side group _unit;
    private _sideCommander = GVAR(namespace) getVariable [str _side, objNull];
    if (_unit isEqualTo _sideCommander && !_unassign) exitWith { };

    // hangon you can empeach the commander anyway ... why do we keep this :-D
    /*
    if (!isNull _sideCommander && !_unassign) exitWith {
        [format ["Commanderslot already occupied by %1", [_sideCommander] call CFUNC(name)], "nope"] call MFUNC(notificationShow);
    };
    */

    if (_unassign) then { // -- Stepping down
        _unit = objNull;
    };
    GVAR(namespace) setVariable [str _side, _unit, true];
    GVAR(namespace) setVariable [str _side + "_uid", getPlayerUID _unit, true];

    ["commanderChanged", _side, [_unit, _sideCommander]] call CFUNC(targetEvent);
}, _this, QUOTE(MODULE)] call CFUNC(mutex);
