/*
    Function:       FRL_Commander_fnc_serverInit
    Author:         Adanteh
    Description:    Adds static weapon system. Allows deploying one, like RP, replacing old ones, moving and rearming
*/
#include "macros.hpp"

[QUOTE(ADDON), configFile >> "FRL" >> "cfgCommander"] call MFUNC(cfgSettingLoad);

GVAR(orderIndex) = 0;
GVAR(namespace) = true call CFUNC(createNamespace);

{
    publicVariable _x;
} forEach [QGVAR(namespace), QGVAR(orderIndex)];

[QGVAR(applyVoteServer), { ["startserver", (_this select 0)] call FUNC(voteApply) }] call CFUNC(addEventHandler);
[QGVAR(applyVoteAnswer), { ["answer", (_this select 0)] call FUNC(voteApply) }] call CFUNC(addEventHandler);
[QGVAR(orderAI), { (_this select 0) call FUNC(setOrderAI); }] call CFUNC(addEventHandler);
