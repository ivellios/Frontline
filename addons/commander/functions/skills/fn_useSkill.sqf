/*
    Function:       FRL_Commander_fnc_useSkill
    Author:         Adanteh
    Description:    Common skill usage
    Example:        ['CAS', [player, playerSide, player getPos [200, getDir player]]] call FRL_Commander_fnc_useSkill
*/
#include "macros.hpp"
#define __SERVER 2
//#define DEBUGFULL true

params ["_skill", ["_args", []]];
_args params ["_caller", "_side", "_position", ["_entry", ""]];

if (_entry == "") then {
    _entry = format ["Skills_%1_%2", _side, _skill];
};

// -- Get side-settings
private _ammo = GVAR(namespaceOptions) getVariable [[_entry, "ammo"] joinString "_", "32Rnd_155mm_Mo_shells"];
private _altitude = GVAR(namespaceOptions) getVariable [[_entry, "altitude"] joinString "_", 1000];
private _count = GVAR(namespaceOptions) getVariable [[_entry, "count"] joinString "_", 3];
private _delay = GVAR(namespaceOptions) getVariable [[_entry, "delay"] joinString "_", 10];
private _spread = GVAR(namespaceOptions) getVariable [[_entry, "spread"] joinString "_", 50];
private _interval = GVAR(namespaceOptions) getVariable [[_entry, "interval"] joinString "_", 2.5];
private _sound = GVAR(namespaceOptions) getVariable [[_entry, "sound"] joinString "_", ""];
private _originSound = GVAR(namespaceOptions) getVariable [[_entry, "originSound"] joinString "_", ""];
private _originCount = GVAR(namespaceOptions) getVariable [[_entry, "originCount"] joinString "_", _count];
private _angle = GVAR(namespaceOptions) getVariable [[_entry, "angle"] joinString "_", 75];
private _direction = ((getMarkerPos format ["base_%1", _side]) getDir _caller) - 50 + (random 50);
private _originPosition = (getMarkerPos format ["base_%1", _side]) getPos [random 150, _direction - 180];

#undef DEBUGFULL
#ifdef DEBUGFULL
    _ammo = "IRAM";
    _delay = 1;
    _altitude = 1000;
    _spread = 0;
    _count = 1;
    _interval = 1;
    _sound = "pr\frl\addons\data_sound\data\artillery\ARTY_Incoming.wss";
    _originSound = "pr\frl\addons\data_sound\data\artillery\Bm37_Shot.wss";
    _originCount = -1;
    //_altitude = -1;
#endif


switch (toLower _skill) do {
    case "artillerystrike": {
        [[_side, _position, _ammo, _sound, _count, _spread, _interval, _direction, _delay, _originPosition, _originSound, _originCount, _altitude, _angle], QFUNC(skillOrdnance), __SERVER] call CFUNC(remoteExec);
    };

    case "mortarsmoke": {
        [[_side, _position, _ammo, _sound, _count, _spread, _interval, _direction, _delay, _originPosition, _originSound, _originCount, _altitude, _angle], QFUNC(skillOrdnance), __SERVER] call CFUNC(remoteExec);
    };

    case "mortarhe": {
        [[_side, _position, _ammo, _sound, _count, _spread, _interval, _direction, _delay, _originPosition, _originSound, _originCount, _altitude, _angle], QFUNC(skillOrdnance), __SERVER] call CFUNC(remoteExec);
    };

    case "mortarflare": {
        [[_side, _position, _ammo, _sound, _count, _spread, _interval, _direction, _delay, _originPosition, _originSound, _originCount, _altitude, _angle], QFUNC(skillOrdnance), __SERVER] call CFUNC(remoteExec);
    };

    case "airdrop": {
        // -- Get data from cfgFactions
        private _planeClass = GVAR(namespaceOptions) getVariable [[_entry, "planeClass"] joinString "_", "B_Plane_CAS_01_F"];
        private _distance = GVAR(namespaceOptions) getVariable [[_entry, "distance"] joinString "_", 3000];
        private _targetHeight = GVAR(namespaceOptions) getVariable [[_entry, "targetHeight"] joinString "_", 0];
        private _initPlane = GVAR(namespaceOptions) getVariable [[_entry, "initPlane"] joinString "_", { true }];
        private _itemToDrop = GVAR(namespaceOptions) getVariable [[_entry, "itemToDrop"] joinString "_", QSVAR(SupplyContainer)];
        private _forceFlying = (GVAR(namespaceOptions) getVariable [[_entry, "forceFlying"] joinString "_", 1]) > 0;

        _initPlane = [_initPlane] call MFUNC(parseToCode);
        [[_side, _position, _planeClass, _direction, _distance, _altitude, _targetHeight, _initPlane, _itemToDrop, _forceFlying], QFUNC(skillAirdrop), __SERVER] call CFUNC(remoteExec);
    };

    case "cas": {

        // -- Get data from cfgFactions
        private _planeClass = GVAR(namespaceOptions) getVariable [[_entry, "planeClass"] joinString "_", "B_Plane_CAS_01_F"];
        private _distance = GVAR(namespaceOptions) getVariable [[_entry, "distance"] joinString "_", 3000];
        private _guntype = GVAR(namespaceOptions) getVariable [[_entry, "guntype"] joinString "_", 0];
        private _forceFire = GVAR(namespaceOptions) getVariable [[_entry, "forcefire"] joinString "_", 0];
        private _fireDistance = GVAR(namespaceOptions) getVariable [[_entry, "fireDistance"] joinString "_", 1000];
        private _duration = GVAR(namespaceOptions) getVariable [[_entry, "duration"] joinString "_", 3];
        private _targetHeight = GVAR(namespaceOptions) getVariable [[_entry, "targetHeight"] joinString "_", 0];
        private _initPlane = GVAR(namespaceOptions) getVariable [[_entry, "initPlane"] joinString "_", { true }];

        #ifdef DEBUGFULL
        _planeClass = "RHS_Su25SM_vvsc";
        _altitude = 1000;
        _distance = 2500;
        _guntype = 2;
        _forceFire = 1;
        _fireDistance = 1000;
        _duration = 5;
        _interval = 3;
        _targetHeight = 0;
        //_initPlane = "(_this select 0) setVariable ['IFA3_sirenEnabled', 0, true];";
        #endif

        _initPlane = [_initPlane] call MFUNC(parseToCode);
        [[_side, _position, _planeClass, _direction, _distance, _altitude, _guntype, _forceFire > 0, _fireDistance, _interval, _duration, _targetHeight, _initPlane], QFUNC(skillAirstrike), __SERVER] call CFUNC(remoteExec);

    };
};

if (_caller isEqualTo Clib_Player) then {
    private _locationText = [_position] call MFUNC(getNearestName);
    private _name = GVAR(namespaceOptions) getVariable [[_entry, "displayName"] joinString "_", "Support"];
    ["showNotification", _side, [format ["%1 incoming %2!", _name, _locationText], "ok"]] call CFUNC(targetEvent);
};
