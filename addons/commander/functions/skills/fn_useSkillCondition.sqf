/*
    Function:       FRL_Commander_fnc_useSkillCondition
    Author:         Adanteh
    Description:    Common skill conditions
*/
#include "macros.hpp"

params ["_skill", ["_args", []]];
_args params ["_caller", "_side", "_position", "_entry"];
