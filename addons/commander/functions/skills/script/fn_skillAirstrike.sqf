/*
    Function:       FRL_Commander_fnc_skillAirstrike
    Author:         Adanteh
    Description:    Executes an airstrike (Either gun run or bombinb run), edit of BIS_fnc_moduleCAS
                    Expects posATL
    Locality:       SERVER ONLY
*/
#include "macros.hpp"

params ["_side", "_position", "_vehicleType",
    ["_direction", 0],
    ["_startRange", 1000],
    ["_startAltitude", 2000],
    ["_weaponTypesID", 0],
    ["_forceFire", false],
    ["_fireDist", 1000],
    ["_fireDelay", 0.1],
    ["_fireDuration", 3],
    ["_targetHeight", 0],
    ["_initPlane", { true }]
];

private _posWest = getMarkerPos "base_west";
private _posEast = getMarkerPos "base_east";
if !(_posEast isEqualTo [] || _posWest isEqualTo []) then {
    _direction = _posEast getdir _posWest;
    // randomize a bit and make it along the Front
    _direction = _direction + 15 - random 30 + ([-90, 90] select floor random 2);
};

_planeCfg = configfile >> "cfgvehicles" >> _vehicleType;
if !(isclass _planeCfg) exitwith {["Vehicle class '%1' not found", _vehicleType] call bis_fnc_error; false};
if !(canSuspend) exitWith { _this spawn FUNC(skillAirstrike) };

//--- Detect gun
_weaponTypes = switch _weaponTypesID do {
    case 0: { [["machinegun"], ["mgun", "cannon"]] };
    case 1: { [["missilelauncher"], ["missile"]] };
    case 2: { [["machinegun","missilelauncher"], ["mgun", "rockets"]] };
    case 3: { [["bomblauncher"], ["bomb"]] };
    case 4: { [["machinegun", "cannon", "bomblauncher"], ["mgun", "bomb", "cannon"]] };
    default { [[], []] };
};

(_weaponTypes select 0) pushBack "countermeasureslauncher";
private _weaponsToKeep = [];
private _weapons = [];
{
    private _sound = toLower (getText (configFile >> "cfgWeapons" >> _x >> "namesound"));
    if ((tolower ((_x call bis_fnc_itemType) select 1) in (_weaponTypes select 0)) || {
        _sound in (_weaponTypes select 1)
    }) then {

        _modes = getarray (configfile >> "cfgweapons" >> _x >> "modes");
        if (count _modes > 0) then {
            _mode = _modes select 0;
            if (_mode == "this") then {_mode = _x;};
            _weapons pushBack [_x, _mode];
            _weaponsToKeep pushBack toLower _x;
        };
    };
} foreach (_vehicleType call bis_fnc_weaponsEntityType);//getarray (_planeCfg >> "weapons");
if (count _weapons == 0) exitwith {["No weapon of types %2 wound on '%1'", _vehicleType, _weaponTypes] call bis_fnc_error; false};


_pitch = atan (_startAltitude / _startRange);
_speed = (getNumber (_planeCfg >> "maxSpeed") min 400) / 3.6 / 1.5; // -- Get a reasonable cruise speed
_duration = ([0,0] distance [_startRange, _startAltitude]) / _speed;

//--- Create plane
_posASL =+ _position;
_posASL set [2, (_posASL select 2) + (getTerrainHeightASL _posASL) + _targetHeight];

_planePos = [_posASL, _startRange, _direction + 180] call bis_fnc_relpos;
_planePos set [2, (_posASL select 2) + _startAltitude];
_planeSide = (getnumber (_planeCfg >> "side")) call bis_fnc_sideType;
private _plane = ([_planePos, _direction, _vehicleType, _planeSide] call bis_fnc_spawnVehicle) select 0;

// -- Init plane for markers and so
["vehicleInit", [_plane, _side, str _plane]] call CFUNC(globalEvent);
[_plane] call _initPlane;

_plane setposASL _planePos;
_plane move ([_posASL, _startRange, _direction] call bis_fnc_relpos);
_plane disableai "move";
_plane disableai "target";
_plane disableai "autotarget";
_plane setcombatmode "blue";

_vectorDir = [_planePos, _posASL] call bis_fnc_vectorFromXtoY;
_velocity = [_vectorDir, _speed] call bis_fnc_vectorMultiply;
_plane setvectordir _vectorDir;
[_plane,-90 + atan (_startRange / _startAltitude),0] call bis_fnc_setpitchbank;
_vectorUp = vectorup _plane;

//--- Remove all other weapons;
_currentWeapons = weapons _plane;
{
    if !(tolower _x in _weaponsToKeep) then {
        if !(tolower ((_x call bis_fnc_itemType) select 1) in ["countermeasureslauncher"]) then {
            _plane removeweapon _x;
        };
    };
} foreach _currentWeapons;

//--- Approach
_fire = [] spawn {waituntil {false}};
_fireNull = true;
_time = time;
_offset = [0, 20] select ({_x == "missilelauncher"} count (_weaponTypes select 0) > 0);


// -- Steer bomb to target
if (_weaponTypesID in [3, 4]) then {
    if (true) exitWith { };
    _plane setVariable ["targetPos", _posASL];
    _plane addEventHandler ["fired", {
        params ["_plane", "", "", "", "", "", "_bullet"];

        private _adjustShells = _plane getVariable ["shells", []];
        _adjustShells pushBack _bullet;
        _plane setVariable ["shells", _adjustShells];
    }];
};


waituntil {
    _fireProgress = _plane getvariable ["fireProgress",0];

    //--- Set the plane approach vector
    _plane setVelocityTransformation [
        _planePos, [_posASL select 0, _posASL select 1, (_posASL select 2) + _offset + _fireProgress * 12],
        _velocity, _velocity,
        _vectorDir, _vectorDir,
        _vectorUp, _vectorUp,
        (time - _time) / _duration
    ];
    _plane setvelocity velocity _plane;

    //--- Fire!
    if ((getposASL _plane) distance _posASL < _fireDist && _fireNull) then {


        //--- Create laser target
        _target = ((ASLtoAGL _posASL) nearEntities ["LaserTarget", 250]) param [0, objnull];
        if (isnull _target) then {
            _target = createvehicle ["LaserTarget", _posASL, [], 0, "none"];
            _target setPosASL _posASL;
        } else {
            _posASL = (getPosASL _target);
            _vectorDir = [_planePos, _posASL] call bis_fnc_vectorFromXtoY;
            _velocity = [_vectorDir, _speed] call bis_fnc_vectorMultiply;
        };

        _plane setVariable ["targetPos", _posASL, false];
        _plane reveal lasertarget _target;
        _plane dowatch lasertarget _target;
        _plane dotarget lasertarget _target;

        _fireNull = false;
        terminate _fire;


        _fire = [_plane, _weapons, _target, _weaponTypesID, _forceFire, _fireDelay, _fireDuration] spawn {
            params ["_plane", "_weapons", "_target", "_weaponTypesID", "_forceFire", "_fireDelay", "_fireDuration"];
            _planeDriver = driver _plane;
            _time = time + _fireDuration;
            waituntil {
                {
                    if (_forceFire) then { // -- Ju87 doesn't drop bombs without this.
                        _plane selectweapon (_x select 0);
                        _planeDriver forceweaponfire _x;
                    };
                    _planeDriver fireattarget [_target, (_x select 0)];
                } foreach _weapons;
                _plane setvariable ["fireProgress", (1 - ((_time - time) / _fireDuration)) max 0 min 1];
                sleep _fireDelay;
                time > _time || isnull _plane || (_plane distance _target < 300) //--- Shoot only for specific period or only one bomb
            };
            sleep 0.25;
        };
    };

    {
        private _shell = _x;
        private _origin = getPosASL _shell;
        private _vdir = _origin vectorFromTo _posASL;
        private _vlat = vectorNormalized (_vdir vectorCrossProduct [0,0,1]);
        private _vup = _vlat vectorCrossProduct _vdir;

        private _speed = (speed _shell) min (getNumber (configFile >> "cfgAmmo" >> typeOf _shell >> "maxSpeed"));
        private _vel = _vdir vectorMultiply _speed;
        _shell setVectorDirAndUp [_vdir, _vup];
        _shell setVelocity _vel;
    } forEach (_plane getVariable ["shells", []]);

    sleep 0.01;
    scriptdone _fire || isnull _plane
};
_plane setvelocity velocity _plane;
_plane flyinheight _startAltitude;

//--- Fire CM
if ({_x == "bomblauncher"} count (_weaponTypes select 0) == 0) then {
    for "_i" from 0 to 1 do {
        driver _plane forceweaponfire ["CMFlareLauncher","Burst"];
        _time = time + 1.1;
        waituntil {time > _time || isnull _plane};
    };
};


//--- Delete plane
private _maxTime = time + 60;
waitUntil { ((_plane distance _posASL) > 5000) || !(alive _plane) || (time > _maxTime) };
sleep 2;
if (alive _plane) then {
    _group = group _plane;
    _crew = crew _plane;
    deletevehicle _plane;
    {deletevehicle _x} foreach _crew;
    deletegroup _group;
};
