/*
    Function:       FRL_Commander_fnc_skillOrdnance
    Author:         Adanteh
    Description:    Executes an ordnance strike (Arty, mortars, whatever) (Either gun run or bombinb run), edit of BIS_fnc_moduleProjectile
                    Expects posATL
    Example:        [playerSide, player getPos [200, getDir player], "8Rnd_82mm_Mo_shells", 3, 25, 5] call FRL_Commander_fnc_skillOrdnance
    Example:        [playerSide, player getPos [200, getDir player], "8Rnd_82mm_Mo_shells", 3, 25, 5] call FRL_Commander_fnc_skillOrdnance
    Locality:       SERVER ONLY
*/
#include "macros.hpp"

params [
    "_side",
    "_position",
    "_ammo",
    ["_soundPlay", ""],
    ["_count", 5], // Amount of shots
    ["_spread", 10], // Distance between shots (in meter), randomized. 25% always applied, 75% random
    ["_interval", 1], // Interval between shots
    ["_direction", random 360], // Direction shots come from
    ["_delay", 10], // Delay before first shots comes in
    ["_originPosition", []], // Position to play sound from when fired (Before the delay)
    ["_originSound", ""], // Sound class to play at soundPOsition before delay
    ["_originCount", 5], // How often to play the origin sound
    ["_altitudeSet", -1],
    ["_angle", 75]
];

private _cfgAmmo = configfile >> "cfgammo" >> _ammo;
if !(isClass _cfgAmmo) then {
    _ammo = getText (configFile >> "cfgMagazines" >> _ammo >> "ammo");
    _cfgAmmo = configfile >> "cfgammo" >> _ammo;
};

if !(isClass _cfgAmmo) exitWith { };
if !(canSuspend) exitWith { _this spawn FUNC(skillOrdnance) };

private _posAmmo = +_position;
private _simulation = gettext (_cfgAmmo >> "simulation");
private _altitude = 0;
private _velocity = [];
private _sound = "";
private _soundSourceClass = "";
private _speaker = objNull; // -- Used because say3D needs an object

switch (toLower _simulation) do {
    case "shotdeploy";
	case "shotshell": {
		_altitude = 1000;
		_velocity = [0, 0, -100];
		_sounds = if (getnumber (_cfgAmmo >> "hit") < 200) then {["mortar1","mortar2"]} else {["shell1","shell2","shell3","shell4"]};
		_sound = _sounds call bis_fnc_selectrandom;
	};
    case "shotmissile";
    case "shotrocket";
	case "shotsubmunitions": {
		_posAmmo = [_posAmmo, 550, _direction + 180] call bis_fnc_relpos;
		_altitude = 1000 - ((getterrainheightasl _posAmmo) - (getterrainheightasl _position));
		_velocity = [sin _direction * _angle, cos _direction * _angle, -100];
	};
	case "shotilluminating": {
		_altitude = 66;
		_velocity = [wind select 0, wind select 1, 100];
		_sound = "SN_Flare_Fired_4";
		_soundSourceClass = "SoundFlareLoop_F";
	};
	case "shotnvgmarker";
	case "shotsmokex": {
		_altitude = 0;
		_velocity = [0, 0, 0];
	};
	default {["Ammo simulation '%1' is not supported", _simulation] call bis_fnc_error;};
};

if (_soundPlay == "") then {
    _soundPlay = _sound;
};

if (_altitudeSet == -1) then {
    _altitudeSet = _altitude;
};

_posAmmo set [2, _altitudeSet];

if !(_velocity isEqualTo []) then {

    if (_originSound isEqualType []) then {
        if (_originSound isEqualTo []) exitWith { _originSound = "" };
        _originSound = selectRandom _originSound;
    };

    if ((_originSound != "") && !(_originPosition isEqualTo [])) then {
        if (_originCount == -1) then {
            _originCount = _count;
        };

        [_interval, _originSound, _originCount, _originPosition] spawn {
            params ["_interval", "_originSound", "_originCount", "_originPosition"];

            for "_i" from 1 to _originCount do {
                playSound3D [_originSound, objNull, false, ATLtoASL _originPosition, 5];
                sleep _interval;
            };
        };
    };

    sleep _delay;
    private _projectiles = [];
    for "_i" from 1 to _count do {
        //--- Create projectile
        private _posShell = _posAmmo getPos [(random (_spread * 0.75)) + (_spread * 0.25), random 360]; // -- Random offset for shells (Minimum 25% of spread)\
        _posShell set [2, _altitude];

		private _projectile = createvehicle [_ammo, _posShell, [], 0, "none"];
		_projectile setposATL _posShell;
		_projectile setvelocity _velocity;

		if (_soundPlay != "") then {
            //playSound3D [_soundPlay, _projectile, false, ATLtoASL _posShell, 3];
        };

		//--- Create sound source
		private _soundSource = if (_soundSourceClass != "") then {
            createSoundSource [_soundSourceClass, _position, [], 0]
        } else {
            objnull
        };

        _projectiles pushBack [_projectile, _soundSource];
        sleep _interval;
    };

    // -- Cleanup things
    [_speaker, _projectiles] spawn {
        params ["_speaker", "_projectiles"];
        private _alive = 1;
        while { _alive > 0 } do {
            _alive = 0;
            {
                if (!alive (_x select 0)) then {
                    deleteVehicle (_x select 0);
                    deleteVehicle (_x select 1);
                } else {
                    _alive = _alive + 1;
                }
            } forEach _projectiles;
        };
        if !(isNull _speaker) then {
            deleteVehicle _speaker;
        };
    };
};
