/*
    Function:       FRL_Commander_fnc_skillAirdrop
    Author:         Adanteh
    Description:    Executes an airstrike (Either gun run or bombinb run), edit of BIS_fnc_moduleCAS
                    Expects posATL
    Locality:       SERVER ONLY
    Example:        [playerSide, player getPos [200, getDir player], "O_Heli_Transport_04_F", getDir player, 200, 200, 150, { true }, "frl_supplyContainer", true] call FRL_Commander_fnc_skillAirdrop
*/
#include "macros.hpp"

params ["_side", "_position", "_vehicleType",
    ["_direction", random 360],
    ["_startRange", 2000],
    ["_startAltitude", 2000],
    ["_targetHeight", 0],
    ["_initPlane", { true }],
    ["_itemToDrop", QSVAR(SupplyContainer)],
    ["_forceFlying", false]
];

_planeCfg = configfile >> "cfgvehicles" >> _vehicleType;
if !(isclass _planeCfg) exitwith {["Vehicle class '%1' not found", _vehicleType] call bis_fnc_error; false};
if !(canSuspend) exitWith { _this spawn FUNC(skillAirdrop) };

_pitch = atan (_startAltitude / _startRange);
_speed = (getNumber (_planeCfg >> "maxSpeed") min 400) / 3.6 / 1.5; // -- Get a reasonable cruise speed
_duration = ([0,0] distance [_startRange, _startAltitude]) / _speed;

//--- Create plane
_posASL = [_position, 75, _direction + 180] call bis_fnc_relpos;
_posASL set [2, (_position select 2) + (getTerrainHeightASL _posASL) + _targetHeight];

_planePos = [_posASL, _startRange, _direction + 180] call bis_fnc_relpos;
_planePos set [2, (_posASL select 2) + _startAltitude];
_planeSide = (getnumber (_planeCfg >> "side")) call bis_fnc_sideType;
private _plane = ([_planePos, _direction, _vehicleType, _planeSide] call bis_fnc_spawnVehicle) select 0;

// -- Init plane for markers and so
["vehicleInit", [_plane, _side, str _plane]] call CFUNC(globalEvent);
[_plane] call _initPlane;

_plane setposASL _planePos;
_plane move ([_posASL, _startRange, _direction] call bis_fnc_relpos);
_plane disableai "move";
_plane disableai "target";
_plane disableai "autotarget";
_plane setcombatmode "blue";

_vectorDir = [_planePos, _posASL] call bis_fnc_vectorFromXtoY;
_velocity = [_vectorDir, _speed] call bis_fnc_vectorMultiply;
_plane setvectordir _vectorDir;
[_plane,-90 + atan (_startRange / _startAltitude),0] call bis_fnc_setpitchbank;
_vectorUp = vectorup _plane;

//--- Approach
private _fire = [] spawn {waituntil {false}};
private _time = time;
private _progress = 0;
private _closestDistance = 1e9;

waituntil {
    //--- Set the plane approach vector
    _plane setVelocityTransformation [
        _planePos, [_posASL select 0, _posASL select 1, (_posASL select 2) + _progress * 12],
        _velocity, _velocity,
        _vectorDir, _vectorDir,
        _vectorUp, _vectorUp,
        (time - _time) / _duration
    ];
    _plane setvelocity velocity _plane;

    //--- Drop it
    private _distance = (getposASL _plane) distance2D _posASL;
    if (_distance <= _closestDistance) then {
        _closestDistance = _distance;
    } else {
        [_itemToDrop, _plane, _side] call MFUNC(airdropObject);
        terminate _fire;
    };


    sleep 0.01;
    scriptdone _fire || isnull _plane
};
_plane setvelocity velocity _plane;

//--- Fire CM
for "_i" from 0 to 1 do {
    driver _plane forceweaponfire ["CMFlareLauncher","Burst"];
    _time = time + 1.1;
    waituntil {time > _time || isnull _plane};
};

private _planePosNew = getPosASL _plane;
if (_forceFlying) then {
    _plane enableAI "move";
    _plane move (ASLtoATL _planePos);
} else {
    _plane flyinheight _startAltitude;
};

//--- Delete plane
private _maxTime = time + 60;
waitUntil { ((_plane distance _posASL) > 5000) || !(alive _plane) || (time > _maxTime) };
sleep 2;
if (alive _plane) then {
    _group = group _plane;
    _crew = crew _plane;
    deletevehicle _plane;
    {deletevehicle _x} foreach _crew;
    deletegroup _group;
};
