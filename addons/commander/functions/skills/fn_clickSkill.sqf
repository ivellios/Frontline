/*
    Function:       FRL_Commander_fnc_clickSkill
    Author:         Adanteh
    Description:    Uses a skill
*/
#include "macros.hpp"

params ["_entry", "_position", ["_quiet", false]];

if !(GVAR(isCommander)) exitWith { };

private _args = [Clib_Player, playerSide, _position, _entry];
private _conditionReturn = "";
private _canUse = [["commanderSkill_", _entry, "canUse"] joinString "_", [_args], true] call MFUNC(checkCondition);
if !(_canUse) exitWith {
    if (!_quiet && { _conditionReturn != "" }) then {
        ["showNotification", [_conditionReturn, "nope"]] call CFUNC(localEvent);
    };
    false
};

#ifndef DEBUGFULL
private _usedAt = GVAR(namespace) getVariable [[_entry, "usedAt"] joinString "#", -1e9];
private _cooldown = (GVAR(namespaceOptions) getVariable [_entry + "_cooldown", 15]) * 60;
if ((_usedAt + _cooldown) > serverTime) exitWith {
    if !(_quiet) then {
        private _timeLeft = [((_usedAt + _cooldown) - serverTime) max 1, "MM:SS", false] call BIS_fnc_secondsToString;
        ["showNotification", [format ["Still on cooldown. Wait %1", _timeLeft], "nope"]] call CFUNC(localEvent);
    };
    false
};

private _canUseAfter = (GVAR(namespaceOptions) getVariable [_entry + "_canUseAfter", 15]) * 60;
if (time < _canUseAfter) exitWith {
    if !(_quiet) then {
        private _timeLeft = [(_canUseAfter - time) max 1, "MM:SS", false] call BIS_fnc_secondsToString;
        ["showNotification", [format ["This skill can't be used the first %2 Minutes.<br />Wait %1", _timeLeft, floor (_canUseAfter / 60)], "nope"]] call CFUNC(localEvent);
    };
    false
};
#endif


private _condition = (GVAR(namespaceOptions) getVariable [_entry + "_condition", { true }]);
if !(_args call _condition) exitWith {
    if (!_quiet && { _conditionReturn != "" }) then {
        ["showNotification", [_conditionReturn, "nope"]] call CFUNC(localEvent);
    };
    false
};


private _inBase = [_position] call MFUNC(sectorInBase);
if (_inBase) exitWith {
    if (!_quiet) then {
        ["showNotification", [format ["Not allowed to target skills in base<br />(Don't be a retard!)"], "nope"]] call CFUNC(localEvent);
    };
    false
};

GVAR(namespace) setVariable [[_entry, "usedAt"] joinString "#", serverTime, true];

private _function = (GVAR(namespaceOptions) getVariable [_entry + "_function", { true }]);
private _return = (_args call _function);
_return;
