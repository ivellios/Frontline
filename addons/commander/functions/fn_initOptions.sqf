/*
    Function:       FRL_Commander_fnc_initOptions
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

GVAR(namespaceOptions) = false call MFUNC(createNamespace);

{
    private _category = configName _x;
    private _categories = GVAR(namespaceOptions) getVariable ["categories", []];
    private _categoryOptions = [];
    _categories pushBack _category;

    private _displayname = getText (_x >> "displayName");
    if (_displayname == "") then { _displayname = [_category, "_", " "] call MFUNC(replaceInString) };

    private _eventName = getText (_x >> "eventName");
    if (_eventName == "") then { _eventName = _category };

    {
        private _entry = [_category, configName _x] joinString "_";
        [_entry, _x, GVAR(namespaceOptions)] call MFUNC(cfgSettingLoadSpecial);
        _categoryOptions pushBack _entry;
    } forEach ("getNumber (_x >> 'scope') >= 2" configClasses _x);

    GVAR(namespaceOptions) setVariable [_category + "#options", _categoryOptions];
    GVAR(namespaceOptions) setVariable [_category + "_displayname", _displayname];
    GVAR(namespaceOptions) setVariable [_category + "_eventname", _eventName];
    GVAR(namespaceOptions) setVariable ["categories", _categories];
} forEach ("getNumber (_x >> 'category') >= 1" configClasses (configFile >> "FRL" >> "CfgCommanderOptions"));


// -- Faction specific commander skills
private _category = "Skills";
private _categoryOptions = GVAR(namespaceOptions) getVariable [_category + "#options", []];
{
    private _side = _x;
    private _sideStr = str _side;
    private _factionClass = missionNamespace getVariable [format [QSVAR(side_%1_factionClass), _sideStr], "Default"];

    {
        private _skills = ([_x] call BIS_fnc_returnChildren) select { getNumber (_x >> "scope") > 1 };
        {
            // -- Add faction data + overrides
            private _entry = [_category, _sideStr, configName _x] joinString "_";
            [_entry, _x, GVAR(namespaceOptions)] call MFUNC(cfgSettingLoadSpecial);

            _categoryOptions pushBack _entry;
            GVAR(namespaceOptions) setVariable [_entry + "#SIDE", _side];

            // -- Form tooltip
            private _tooltip = [];
            private _players = GVAR(namespaceOptions) getVariable [_entry + "_minPlayers", 0];
            if (_players > 0) then {
                _tooltip pushBack format ["Minimum players: %1", _players];
            };
            private _cooldown = GVAR(namespaceOptions) getVariable [_entry + "_cooldown", 0];
            if (_cooldown > 0) then {
                _tooltip pushBack format ["Cooldown: %1 minutes", _cooldown];
            };
            private _canUseAfter = GVAR(namespaceOptions) getVariable [_entry + "_canUseAfter", 0];
            if (_canUseAfter > 0) then {
                _tooltip pushBack format ["Useable after: %1 minutes", _canUseAfter];
            };


            GVAR(namespaceOptions) setVariable [_entry + "#TOOLTIP", _tooltip joinString "\n"];
        } forEach _skills;

    } forEach [
        (configFile >> "FRL" >> "CfgFactions" >> _factionClass >> "cfgCommanderSkills"),
        (missionConfigFile >> "FRL" >> "cfgFactions" >> _sideStr >> "cfgCommanderSkills")
    ];
} forEach (call MFUNC(getSides));
GVAR(namespaceOptions) setVariable [_category + "#options", _categoryOptions];
