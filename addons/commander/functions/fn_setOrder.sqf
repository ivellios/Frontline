/*
    Function:       FRL_Commander_fnc_setOrder
    Author:         Adanteh
    Description:    Draws a shiny order (On map / 3D)
*/
#include "macros.hpp"

params ["_entry", "_pos"];

(group Clib_Player) setVariable [QGVAR(currentOrder), [_entry, _pos, serverTime], true];
["currentOrderChanged", (group Clib_Player)] call CFUNC(targetEvent);
