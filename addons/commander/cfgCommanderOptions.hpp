class cfgCommanderOptions {
    class DefaultOrder {
        scope = 2;
        displayName = "";
        listPath = "\pr\frl\addons\commander\data\move.paa";
        3dIcon = "\pr\frl\addons\commander\data\move.paa";
        mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
        waypointTypeAI = "MOVE";
        waypointBehaviourAI = "AWARE";
    };

    class Waypoints {
        category = 1;
        displayName = "ORDERS";
        eventName = "createOrder";

        class MoveFast: DefaultOrder {
            displayName = "Move fast";
            listPath = "\pr\frl\addons\commander\data\MoveFast.paa";
            3dIcon = "\pr\frl\addons\commander\data\MoveFast.paa";
            mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
            waypointTypeAI = "MOVE";
            waypointBehaviourAI = "AWARE";
            waypointSpeed = "FULL";
        };

        class Move: DefaultOrder {
            displayName = "Move";
            listPath = "\pr\frl\addons\commander\data\move.paa";
            3dIcon = "\pr\frl\addons\commander\data\move.paa";
            mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
            waypointTypeAI = "MOVE";
        };

        class Attack: DefaultOrder {
            displayName = "Attack";
            listPath = "\pr\frl\addons\commander\data\attack.paa";
            3dIcon = "\pr\frl\addons\commander\data\attack.paa";
            mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
            waypointBehaviourAI = "COMBAT";
        };

        class Defend: DefaultOrder {
            displayName = "Defend";
            listPath = "\pr\frl\addons\commander\data\defend.paa";
            3dIcon = "\pr\frl\addons\commander\data\defend.paa";
            mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
            waypointBehaviourAI = "COMBAT";
        };

        class Suppress: DefaultOrder {
            displayName = "Suppress";
            listPath = "\pr\frl\addons\commander\data\Suppress.paa";
            3dIcon = "\pr\frl\addons\commander\data\Suppress.paa";
            mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
            waypointBehaviourAI = "COMBAT";
        };

        class Support: DefaultOrder {
            displayName = "Support";
            listPath = "\pr\frl\addons\commander\data\Support.paa";
            3dIcon = "\pr\frl\addons\commander\data\Support.paa";
            mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
            waypointBehaviourAI = "AWARE";
        };

        class SetFO: DefaultOrder {
            displayName = "Deploy FO";
            listPath = "\pr\frl\addons\commander\data\SetFO.paa";
            3dIcon = "\pr\frl\addons\commander\data\SetFO.paa";
            mapIcon = "\a3\ui_f\data\map\MapControl\taskicon_ca.paa";
        };
    };

    class Skills {
        category = 1;
        displayName = "SKILLS";
        eventName = "useSkill";

        // -- The actual skills are defined within factionf iles
    };

};
