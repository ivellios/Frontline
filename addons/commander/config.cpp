#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Main", "Voting", "UnitMarkers"};

            class assignCommander;
            class clientInit;
            class clientInitApply;
            class serverInit;
            class acknowledge;
            class createOrder;
            class handleMapClick;
            class initOptions;
            class setOrder;
            class setOrderAI;
            class voteApply;

            class Markers {
                class deleteMouseOver;
                class drawAllOrders;
                class drawOrders;
                class orderMouseOver;
                class orderMouseOverText;
            };

            class Menu {
                class loadMenu;
                class populateMenu;
                class showSkillstatus;
            };

            class Skills {
                class clickSkill;
                class useSkill;
                class useSkillCondition;
                class Script {
                    class skillAirdrop;
                    class skillAirstrike;
                    class skillOrdnance;
                };
            };
        };
    };

    #include "cfgCommanderOptions.hpp"
};


class RscControlsGroupNoScrollbars;
class RscText;
class RscPictureKeepAspect;
class RscToolbox;
class RscListBox;
class RscStructuredText;
#include "RscOrderDialog.hpp"
#include "RscOrderMouseOver.hpp"
