#define _MARKERMOUSEOVER_WIDTH GRIDX(10)
#define _MARKERMOUSEOVER_HEIGHT GRIDY(20)
#define _MARKERMOUSEOVER_HEIGHT_START GRIDY(40)

class FRL_RscOrderMouseOver: RscControlsGroupNoScrollbars {
	idc = -1;
	x = 0;
	y = 0;
	w = _MARKERMOUSEOVER_WIDTH;
	h = _MARKERMOUSEOVER_HEIGHT_START;

	class Controls {
		class Header : RscText {
				idc = 1;
				text = "MOVE";
				shadow = 0;
		    colorBackground[] = {0,0,0,1};
		    colorText[] = COLOR_BUTTON_BG_ACCENT;
				sizeEx = GRIDY(1);
				x = 0;
				y = 0;
				w = _MARKERMOUSEOVER_WIDTH;
				h = GRIDY(1.2);
		};

		class Background: RscText {
				idc = 2;
				colorBackground[] = {0, 0, 0, 0.8};
				x = 0;
				y = GRIDY(1.2);
				w = _MARKERMOUSEOVER_WIDTH;
				h = _MARKERMOUSEOVER_HEIGHT_START - GRIDY(1.2);
		};

		class MarkerIcon: RscPictureKeepAspect {
				idc = 4;
				text = "";
				colorText[] = {1, 1, 1, 1};
				colorBackground[] = {0, 0, 0, 0};
				sizeEx = GRIDY(0.8);
				x = _MARKERMOUSEOVER_WIDTH - GRIDX(4);
				y = 0;
				w = GRIDX(4);
				h = GRIDY(1.2);
		};

		class TextBlock: RscStructuredText {
				idc = 3;
				text = "";
				colorText[] = {1, 1, 1, 1};
				colorBackground[] = {0, 0, 0, 0};
				sizeEx = GRIDY(0.8);
				x = GRIDX(0.1);
				y = GRIDY(1.4);
				w = _MARKERMOUSEOVER_WIDTH - GRIDX(0.2);
				h = _MARKERMOUSEOVER_HEIGHT_START - GRIDY(1.4);
		};
	};
};
