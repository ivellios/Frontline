/*
    Function:       FRL_Fortifications_fnc_populateMenu
    Author:         Adanteh
    Description:    Adds all entries into spotting from config
    Example:		[0] call FRL_Fortifications_fnc_populateMenu
    TODO: filter objects depending on biome
*/
#include "macros.hpp"
#define CTRL(x) ((uiNamespace getVariable QGVAR(dialog)) displayCtrl x)

params ["_categoryIndex"];

private _supplyValue = GVAR(forwardOutpost) getVariable ["supplies", 0];
private _categoryEntries = + ((GVAR(menuOptions) select 2) select _categoryIndex);
lnbClear CTRL(4);

private _squad = [playerSide, "squads"] call MFUNC(getSideData);
#ifdef DEBUGFULL
    //_squad = "RHS_Iraq";
#endif
private _unlockedRestrictions = getArray (configfile >> "FRL" >> "Squads" >> _squad >> "staticCompositions");

// may jam this into PFH if requested... however i don't see the need as of now
private _foTotalSupply = GVAR(forwardOutpost) getVariable ["totalSupplies", 0];
private _foTotalSupplyTxt = format ["Build Fortification from %1 | %2 / %3 total supplies received", GVAR(FOID), _foTotalSupply, GVAR(maxTotalFOSupply)];
(uiNamespace getVariable QGVAR(dialog) displayCtrl 1) ctrlSetText _foTotalSupplyTxt;

private _playercount = [QGVAR(condition_minPlayers), { [false] call MFUNC(getSideSmallest) }, true, 4] call CFUNC(cachedCall);
private _world = worldName;
{
    _x params ["_cfgName", "_displayName", "_listPath", "_cost", "", "", "_minPlayerCount", "_restrictionClasses", "", ["_color", []]];

    #ifdef DEBUGFULL
        _restrictionClasses = [];
        _minPlayerCount = 0;
    #endif

    private _isRestricted = false;
    {
        if (_x in _unlockedRestrictions) exitWith {
            _isRestricted = false;
        };
        _isRestricted = true;
    } forEach _restrictionClasses;

    if !(_isRestricted) then {			//filters out stuff not belonging into this biome
        private _playercountText = "";
        if (_minPlayerCount > 0) then {
            _playercountText = ["%1 %2", _playercount, _minPlayerCount];
        };

        private _lnbIndex = CTRL(4) lnbAddRow [_displayName, _playercountText, str _cost];
        CTRL(4) lnbSetPicture [[_lnbIndex, 0], _listPath];
        CTRL(4) lbSetTooltip [_lnbIndex * 3, _displayName];
        CTRL(4) lnbSetData [[_lnbIndex, 0], str _forEachIndex];


        if !(_color isEqualTo []) then {
            CTRL(4) lnbSetColor [[_lnbIndex, 0], _color];
        };
    };
} forEach _categoryEntries;
