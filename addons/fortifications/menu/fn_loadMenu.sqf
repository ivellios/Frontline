/*
    Function:       FRL_Fortifications_fnc_loadMenu
    Author:         Adanteh
    Description:    (Un)loads the spotting menu and adds all EHs. Called by onLoad/onUnload dialog entries.
	Example:		["open"] call FRL_Fortifications_fnc_loadMenu;
*/
#include "macros.hpp"

#define CTRL(x) ((uiNamespace getVariable QGVAR(dialog)) displayCtrl x)

if (CLib_Player != vehicle CLib_Player) exitWith {
	["You cannot build fortications from inside vehicles", "warning"] call MFUNC(notificationShow);
};

params ["_mode", "_forwardOutpost"];

switch (toLower _mode) do {
	case "open": {
        private _supplyBox = param [1, objNull];
		createDialog QGVAR(dialog);
	};

	case "onload": {

		CTRL(4) ctrlAddEventHandler ["lbSelChanged", {
			if ((_this select 1) == -1) exitWith { };
			private _entryIndex = (_this select 0) lnbData [(_this select 1), 0];
			if (_entryIndex != "") then {
				_entryIndex = parseNumber _entryIndex;
				private _category = uiNamespace getVariable [QGVAR(curCategory), 0];
				(uiNamespace getVariable QGVAR(dialog)) closeDisplay 2;
				// -- Do stuff
				[QGVAR(optionSelected), [_entryIndex, _category]] call CFUNC(localEvent);
			};
			true;
		}];

		private _categoryList = [];
		private _categories = + (GVAR(menuOptions) select 0);
        (uiNamespace getVariable [QGVAR(dialog), displayNull]) setVariable [QSVAR(noOverlay), true];


		{
			private _lbIndex = CTRL(3) lbAdd ((GVAR(menuOptions) select 1) select _forEachIndex);
			CTRL(3) lbSetData [_lbIndex, _x];
			CTRL(3) lbSetValue [_lbIndex, _forEachIndex];
			_categoryList set [_lbIndex, _forEachIndex];
		} forEach _categories;
		uiNamespace setVariable [QGVAR(categoryList), _categoryList];

		CTRL(3) lbSetCurSel 0;
		CTRL(3) ctrlAddEventHandler ["ToolboxSelChanged", {
			private _category = (uiNamespace getVariable [QGVAR(categoryList), []]) param [(_this select 1), 0];
			private _categoryCurrent = uiNamespace getVariable [QGVAR(curCategory), 0];
			if (_category != _categoryCurrent) then {
				uiNamespace setVariable [QGVAR(curCategory), _category];
				[_category] call FUNC(populateMenu);
			};
			true
		}];

		[0] call FUNC(populateMenu);

		private _clickedPos = [-0.25, 0.25]; // EDIT THIS TO PUT IT ON SCREEN WHERE YOU WANT
		private _position = ctrlPosition CTRL(100);
		_position set [0, (_clickedPos select 0) + 0.015];
		_position set [1, (_clickedPos select 1) - 0.15];

		// -- Prevent the window from going below the screen (Move it up if that's the case) -- //
		if (((_position select 1) + (_position select 3)) > (safeZoneY + safeZoneH)) then {
			_position set [1, (safeZoneY + safeZoneH) - (_position select 3)];
		};

		// -- Prevent the window from going to the right-side of screen (Move to left if that's the case)  -- //
		if (((_position select 0) + (_position select 2)) > (safeZoneX + safeZoneW)) then {
			_position set [0, (safeZoneX + safeZoneW) - (_position select 2)];
		};

		CTRL(100) ctrlSetPosition _position;
		CTRL(100) ctrlCommit 0;

		// -- Unload the menu if right-clicked anywhere or if clicked outside the spotting menu -- //
		(uiNamespace getVariable QGVAR(dialog)) displayAddEventHandler ["mouseButtonDown", {
			private _groupPos = ctrlPosition CTRL(100);
			params ["", "_button", "_posX", "_posY"];

			if ((_button == 1) || {
				(_posX < (_groupPos select 0)) } || {
				(_posY < (_groupPos select 1)) } || {
				(_posX > ((_groupPos select 0) + (_groupPos select 2)))  } || {
				(_posY > ((_groupPos select 1) + (_groupPos select 3)))  }) then {
				(uiNamespace getVariable QGVAR(dialog)) closeDisplay 2;
			};
			true
		}];
	};

	case "onunload": {
		uiNamespace setVariable [QGVAR(curCategory), nil];
		uiNamespace setVariable [QGVAR(categoryList), nil];
	};
};
