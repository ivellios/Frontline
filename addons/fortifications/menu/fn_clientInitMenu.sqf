/*
    Function:       FRL_Fortifications_fnc_clientInitMenu
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

// -- Cache so we don't need to do constant lookups
GVAR(menuOptions) = call FUNC(cacheOptions);

// -- Event for when an option from the menu is selected. Passes just the ID, so get details from cached options
[QGVAR(optionSelected), {
    (_this select 0) params ["_entryIndex", "_categoryIndex"];
    private _entry = ((GVAR(menuOptions) select 2) select _categoryIndex) select _entryIndex;
    _entry params [
        "_className",
         "_displayName",
         "_listPath",
         ["_supplyCost", 50],
         ["_constructionTime", 20],
         ["_composition", []],
         ["_minPlayerCount", 0],
         ["_restrictionClasses", []],
         ["_allowElevatedConstruction", false]
     ];

    _entry call FUNC(placementPreview);
}] call CFUNC(addEventHandler);

["FortificationWeapon", [
    "",
    "Rearm",
    { _this call EFUNC(statics,rearmStatic) },
    { true }
]] call EFUNC(interactthreed,menuOptionAdd);


["FortificationWeapon", [
    "",
    "Set weapon to face direction",
    { _this call FUNC(rotateStatic) },
    { true }
]] call EFUNC(interactthreed,menuOptionAdd);

["FortificationWeapon", [
    "",
    "Reposition weapon",
    { _this call FUNC(repositionStart) },
    { true }
]] call EFUNC(interactthreed,menuOptionAdd);
