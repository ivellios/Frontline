/*
    Function:       FRL_Fortifications_fnc_cacheOptions
    Author:         Adanteh
    Description:    Retrieves the available order options and parses them, so we only need to send ID across the network and have faster loading of menus
*/
#include "macros.hpp"

private _allEntries = [[], [], []];

{
	private _categoryEntries = [];
	{
		if ((getNumber (_x >> "scope")) >= 2) then {
            private _classname = configName _x;
            // -- Validate fortification exists
            private _classCfg = (configFile >> "CfgVehicles" >> _classname);
            if (isClass _classCfg || true) then {
                private _displayName = getText (_x >> "displayName");
                if (_displayName == "") then {
                    _displayName = getText (_classCfg >> "displayName");
                };


                private _listPath = getText (_x >> "listPath");
                if (_listPath == "") then {
                    _listPath = getText (_classCfg >> "editorPreview");
                };

                private _composition = getArray (_x >> "composition");
                private _color = [1, 1, 1, 1];
                #ifdef DEBUGFULL
                    // -- Check if all classes are available
                    private _nonExistantClass = {
                        private _class = _x param [0, ""];
                        !(isClass (configFile >> "CfgVehicles" >> _class));
                    } count _composition;
                    if (_nonExistantClass > 0) then {
                        _color = [1, 0.25, 0.25, 1];
                    };
                #endif


                _categoryEntries pushBack [
                    configName _x,
                    _displayName,
                    _listPath,
                    getNumber (_x >> "cost"),
                    getNumber (_x >> "constructionTime"),
                    _composition,
                    getNumber (_x >> "minPlayerCount"),
                    getArray (_x >> "restrictionClasses"),
                    (getNumber (_x >> "allowElevatedConstruction") > 0),
                    _color
			   ];
            };
		};
		nil;
	} count ("true" configClasses (configFile >> "FRL" >> "CfgFortifications" >> (configName _x)));
	if (count _categoryEntries > 0) then {
		(_allEntries select 0) pushBack (configName _x);
		(_allEntries select 1) pushBack (getText (_x >> "displayName"));
		(_allEntries select 2) pushBack _categoryEntries;
	};
	nil;
} count (configProperties [(configFile >> "FRL" >> "CfgFortifications"), "getNumber (_x >> 'category') >= 1", true]);
_allEntries;
