/*
    Function:       FRL_Fortifications_fnc_staticHunkeringLoop
    Author:         N3croo
    Description:    Allows usage of animations to hunker down in static weapons

*/
#define __INCLUDE_DIK
#include "macros.hpp"

params ["_args", "_handle"];
_args params ["_hunkerAnim", "_standUpAnim", "_defaultAnim"];
private _anim = animationState CLib_Player;
if !(GVAR(isHunkering)) then {
    if ([DIK_LCONTROL] call MFUNC(keyPressed)) then {
        GVAR(isHunkering) = true;
        GVAR(delay) = 0;
        CLib_Player switchmove _hunkerAnim;
        ["switchMove", [Clib_Player, _hunkerAnim]] call CFUNC(globalEvent);
    } else {
        GVAR(delay) = GVAR(delay) + 1;
        // fuck me that animation system is retarded
        if (GVAR(delay) > 50) then {
            CLib_Player switchMove _defaultAnim;
            ["switchMove", [Clib_Player, _defaultAnim]] call CFUNC(globalEvent);
        };
    };
} else {
    if !([DIK_LCONTROL] call MFUNC(keyPressed)) then {
        GVAR(delay) = GVAR(delay) + 1;
        if (GVAR(delay) > 3) then {
            GVAR(isHunkering) = false;
            CLib_Player switchmove _standUpAnim;
            ["switchMove", [Clib_Player, _standUpAnim]] call CFUNC(globalEvent);
            GVAR(delay) = 0;
        };
    };
    if (cameraView != "INTERNAL") then {
        // u no using optics bro
        CLib_Player switchCamera "Internal";
    };
};
