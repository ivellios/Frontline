/*
    Function:       FRL_Fortifications_fnc_setToGround
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

_this params ["_static", "_supportStructures", "_criticalBBCorners"];
private _posToSet = getPosATL _static;

if !(GVAR(validPlacement)) then {



    if (GVAR(validPlacementReason) == "Object too high") then {

        private _posToSetASLH = (AGLToASL _posToSet) select 2;
        private _ZDelta = 0;
        private _criticalBBCorners = GVAR(previewObjects) - [_static];

        {
            private _posASL = getPosASL _x;
            private _posTL = getTerrainHeightASL _posASL;
            private _lowPos = +_posASL;

            _posASL set [2, (_posASL select 2) max _posTL];
            _lowPos set [2, (_lowPos select 2) - 1.5];

            // NOTE: if begPosASL is under the ground and endPosASL is above it, the command will only return intersection with the ground, this is engine limitation and none of the intersectXXX commands will work when initiated from under the ground.
            private _intersectedObjs = lineIntersectsSurfaces [_posASL, _lowPos, objNull, objNull, false, -1];
            if (_intersectedObjs isEqualTo []) then {

            } else {
                {
                    _x params ["_intersectPos", "", "_intersectObj"];
                    if ((!(_intersectObj isKindOf "AllVehicles") || _intersectObj isEqualTo objNull) && { !(_x in GVAR(previewObjects)) } ) then {
                        private _currentZDelta = (_intersectPos select 2) - _posToSetASLH;
                        if (_currentZDelta < _ZDelta) then {
                            _ZDelta = _currentZDelta;
                        };
                    };

                } foreach _intersectedObjs;
            };

        } forEach _criticalBBCorners;

        _posToSet set [2, ((_posToSet select 2) + _ZDelta - 0.05)];
        _static setposATL _posToSet;

        if !(_supportStructures isEqualTo []) then {
            [QGVAR(assignSupport), [[_static] , _supportStructures]] call CFUNC(serverEvent);
        };

    } else {
        // in case its not too high but halfway inside a building or so ... we need to get a new safe pos
        private _altPos = _posToSet findEmptyPosition [0, 20, typeOf _static];
        if (_altPos isEqualTo []) then {
            _altPos = _posToSet;
        };
        _static setposATL _altPos;
        _supportStructures = [];
    };

} else {

    if !(_supportStructures isEqualTo []) then {
        [QGVAR(assignSupport), [[_static] , _supportStructures]] call CFUNC(serverEvent);
    } else {
        // check if its vastly below the surface too, we dont want to loose them either
        // if you place it inside a building too low .... man it would get complicated mate :-P
        private _criticalBBCorners = GVAR(previewObjects) - [_static];
        private _posToSetASLH = (AGLToASL _posToSet) select 2;
        private _ZDelta = -10e9;
        {
            private _posASL = getPosASL _x;
            private _posTL = getTerrainHeightASL _posASL;
            private _currentZDelta = (_posASL select 2) - _posTL;
            _ZDelta = _ZDelta max _currentZDelta;
        } forEach _criticalBBCorners;

        _posToSet set [2, (_posToSet select 2) - _ZDelta];
        _static setposATL _posToSet;
    };

};
