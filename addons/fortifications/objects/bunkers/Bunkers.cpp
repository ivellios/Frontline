#include "..\macros.hpp"

class DefaultBunker: DefaultFortification {
    constructionTime = 35;
    scope = 2;
    cost = 100;
    allowElevatedConstruction = 0;
    composition[] = {};
};

class Bunkers: DefaultBunker{
    category = 3;
    displayName = "Bunkers";
    cost = 100;
    scope = 2;
    composition[] = {};

    #include "CUP.cpp"
    #include "IFA.cpp"
    #include "Vanilla.cpp"
    #include "UNS.cpp"
};
