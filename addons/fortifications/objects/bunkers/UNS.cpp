#include "..\macros.hpp"

class uns_spiderhole_leanto_NVA: DefaultBunker {
    cost = 5;
    constructionTime = 10;
    restrictionClasses[] = {"NVA"};
    displayName = "Spiderhole";
    composition[] =
    {
        {"uns_spiderhole_leanto_NVA",-1,0,{0,0,0.00239897},359.999,{0.00290648,0.0229546,0.999732}}
    };
};

class Land_Wood_Tower: DefaultBunker {
    allowElevatedConstruction = 1;
    cost = 100;
    restrictionClasses[] = {"UNSUNG"};
    composition[] =
    {
        {"Land_Wood_Tower",-1,0,{0,0,0},179.695,{0,0,1}}
    };
};

class wx_logbunker01: DefaultBunker {
    cost = 75;
    constructionTime = 25;
    displayName = "Logbunker"
    restrictionClasses[] = {"UNSUNG"};
    composition[] =
    {
        {"wx_logbunker01",-1,0,{0,0,0},181.939,{0,0,1}}
    };
};

class LAND_UNS_GuardHouse: DefaultBunker {
    cost = 100;
    restrictionClasses[] = {"UNSUNG"};
    composition[] =
    {
        {"LAND_UNS_GuardHouse",-1,0,{0,0,0},89.3196,{0,0,1}}
    };
};

class uns_foxhole2: DefaultBunker {
    cost = 50;
    constructionTime = 25;
    restrictionClasses[] = {"UNSUNG"};
    composition[] =
    {
        {"uns_foxhole2",-1,0,{0,0,0},81.1096,{0,0,1}}
    };
};

class LAND_uns_weapon_pit: DefaultBunker {
    cost = 150;
    restrictionClasses[] = {"UNSUNG"};
    composition[] =
    {
        {"LAND_uns_weapon_pit",-1,0,{0,0,-0.0342484},181.1,{0,0,1}}
    };
};

class wx_defenceposition_04: DefaultBunker {
    cost = 250;
    constructionTime = 90;
    restrictionClasses[] = {"UNSUNG"};
    composition[] =
    {
        {"wx_defenceposition_04",-1,0,{0,0,0},178.954,{0,0,1}}
    };
};

class LAND_uns_eastbunker1: DefaultBunker {
    cost = 70;
    constructionTime = 25;
    restrictionClasses[] = {"UNSUNG"};
    composition[] =
    {
        {"LAND_uns_eastbunker1",-1,0,{0,0,0},182.596,{0,0,1}}
    };
};
