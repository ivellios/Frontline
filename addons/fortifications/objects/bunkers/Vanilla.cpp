#include "..\macros.hpp"

class Land_BagBunker_Small_F: DefaultBunker {
    cost = 75;
    constructionTime = 25;
    allowElevatedConstruction = 1;
    composition[] =
    {
        {"Land_BagBunker_Small_F",-1,0,{0,0,0},180,{0,0,1}}
    };
};
class Land_HBarrierTower_F:DefaultBunker {
    cost = 200;
    constructionTime = 60;
    restrictionClasses[] = {"RHS"};
    composition[] =
    {
        {"Land_HBarrierTower_F",-1,0,{0,0,-0.0121603},180,{0,0,1}}
    };
};
class Land_HBarrier_01_big_tower_green_F: Land_HBarrierTower_F {
    scope = 0;
    cost = 200;
    composition[] =
    {
        {"Land_HBarrier_01_big_tower_green_F",-1,0,{0,0,-0.0121603},180,{0,0,1}}
    };
};

class Comp_sandbag_tunnel_tent: DefaultBunker {
    displayName = "small sandbag nest (tent)";
    scope = 0;
    cost = 100;
    composition[] =
    {
        {"CUP_A2_tent_small_west",-1,0,{0,0,-0.672569},91.894,{0,0,1}},
        {"Land_SandbagBarricade_01_hole_F",0,0,{0.195634,-3.11513,-0.047421},271.354,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.883297,1.28935,0.520505},271.894,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.614987,1.34238,-0.100841},271.894,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{1.62785,0.0690145,0.431741},186.788,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{1.67363,0.08505,-0.100841},180.788,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.72046,0.262122,-0.100841},177.788,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.74453,0.28973,0.520505},177.788,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-1.91179,-1.08675,0.158483},181.293,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.08849,-1.16833,0.158483},1.29299,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{-1.3552,1.03196,0.510054},91.861,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{1.34574,0.978721,-0.089407},1.861,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{1.27359,0.956733,0.510054},1.861,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{1.48181,-2.67165,-0.089407},271.293,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{-1.38249,1.1055,-0.089407},91.861,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{-1.2069,-2.58274,-0.089407},184.293,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{1.45501,-2.59813,0.510054},271.293,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{-1.13657,-2.55728,0.510054},184.293,{0,0,1}}
    };
};

class Comp_sandbag_manhole: DefaultBunker {
    displayName = "sandbag manhole";
    cost = 75;
    scope = 0;
    allowElevatedConstruction = 0;
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-1.12945},0,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-0.676758,-1.7478,0.260534},69,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{0.710449,-1.6687,0.260534},282,{0,0,1}}
    };
};
