#include "..\macros.hpp"

class Land_fort_artillery_nest_EP1: DefaultBunker{
    restrictionClasses[] = {"CUP"};
    cost = 100;
    constructionTime = 30;
};

class Land_fort_artillery_nest: DefaultBunker{
    restrictionClasses[] = {"CUP"};
    cost = 100;
    constructionTime = 30;
};

class Land_fort_rampart_EP1: DefaultBunker{
    restrictionClasses[] = {"CUP"};
    cost = 75;
    constructionTime = 20;
};
