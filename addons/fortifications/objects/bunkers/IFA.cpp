#include "..\macros.hpp"

class Comp_concrete_mgnest_ger: DefaultBunker {
    displayName = "MG Nest (splintercamo)";
    cost = 200;
    restrictionClasses[] = {"GER_BUNKERS"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_WW2_BET_MG_Nest_Splinter_A",-1,0,{0,0,-0.0121603},1.89394,{0,0,1}}
    };
};

class Comp_BagBunker_Small_DryCamo: DefaultBunker {
    scope = 0;
    displayName = "Bunker (summer dry camouflage)";
    cost = 100;
    allowElevatedConstruction = 1;
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Fort_Nest",-1,0,{0,0,-0.511281},180,{0,0,1}},
        {"CUP_A2_p_articum",0,0,{0.825124,-1.35811,1.15516},274.894,{0,0,1}},
        {"CUP_A2_p_articum",0,0,{-0.234828,-1.6331,1.15516},250.894,{0,0,1}},
        //{"CUP_A2_p_articum",0,0,{0.423838,-2.66006,-0.10271},181.894,{-0.258678,0.00855407,0.965926}},
        //{"CUP_A2_b_betula2w_summer",0,0,{0.650744,1.18061,0.10655},82.894,{-0.339393,-0.04231,0.939693}},
        {"CUP_A2_b_betula2w_summer",0,0,{-1.59002,1.50673,0.10655},154.894,{-0.145117,0.309708,0.939693}},
        {"CUP_A2_b_betula2w_summer",0,0,{-1.93325,-0.106201,0.16121},280.894,{0,0,1}},
        {"CUP_A2_b_betula2w_summer",0,0,{1.93768,-0.742649,0.16121},343.894,{0,0,1}},
        {"CUP_A2_b_betula2w_summer",0,0,{1.71815,-2.10131,0.59462},316.894,{-0.23372,0.249706,0.939693}},
        {"CUP_A2_b_betula2w_summer",0,0,{-1.7496,-2.03344,0.59803},283.894,{-0.332013,0.0821281,0.939693}},
        {"CUP_A2_b_betula2w_summer",0,0,{2.03359,0.499205,0.42047},28.894,{0,0,1}}
    };
};

class Land_trench_WW2_Big: DefaultBunker{
    scope = 0;
    displayName = "Infantry trench (big)";
    cost = 200;
    restrictionClasses[] = {"IFA"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] = {
        {"WW2_Big_Infantry_Trench",-1,0,{0,5,0},0,{0,0,1}}
    };
};

class Land_trench_WW2_tank: DefaultBunker{
    displayName = "Tank trench";
    cost = 100;
    restrictionClasses[] = {"IFA"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] = {
        {"Land_WW2_TrenchTank",-1,0,{0,0,0},0,{0,0,1}}
    };
};

class Land_WW2_SWU_Trench_Corner_Desert: DefaultBunker{
    displayName = "Trench corner (desert)";
    cost = 100;
    restrictionClasses[] = {"IFA_DESERT"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_WW2_SWU_Trench_Corner_Desert",-1,0,{0,0,0},0,{0,0,1}}
    };
};

class Land_WW2_SWU_Trench_End_Desert: DefaultBunker{
    displayName = "Trench end (desert)";
    cost = 50;
    restrictionClasses[] = {"IFA_DESERT"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_WW2_SWU_Trench_End_Desert",-1,0,{0,0,0},0,{0,0,1}}
    };
};

class Land_WW2_SWU_Trench_Long_Desert: DefaultBunker{
    displayName = "Trench (desert)";
    cost = 100;
    restrictionClasses[] = {"IFA_DESERT"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_WW2_SWU_Trench_Long_Desert",-1,0,{0,0,0},0,{0,0,1}}
    };
};

class Land_WW2_SWU_Trench_Short_Desert: DefaultBunker{
    displayName = "Trench short (desert)";
    cost = 75;
    restrictionClasses[] = {"IFA_DESERT"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_WW2_SWU_Trench_Short_Desert",-1,0,{0,0,0},270.279,{0,0,1}}
    };
};

class Land_WW2_SWU_Trench_TJunction_Desert: DefaultBunker{
    displayName = "Trench TJunction (desert)";
    cost = 100;
    restrictionClasses[] = {"IFA_DESERT"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_WW2_SWU_Trench_TJunction_Desert",-1,0,{0,0,0},0,{0,0,1}}
    };
};

class Land_WW2_Foxhole_Desert: DefaultBunker{
    displayName = "Foxhole (desert)";
    cost = 50;
    restrictionClasses[] = {"IFA_DESERT"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_WW2_Foxhole_Desert",-1,0,{0,0,0},269.462,{0,0,1}}
    };
};

class Land_trench_WW2_pillbox: DefaultBunker{
    displayName = "Pillbox";
    cost = 300;
    constructionTime = 70;
    restrictionClasses[] = {"GER_BUNKERS"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] = {
        {"WW2_Fortification_Pillbox_Small",-1,0,{0,0,0},180,{0,0,1}}
    };
};

class Land_trench_WW2: DefaultBunker{
    scope = 0;
    displayName = "Infantry trench  (small)";
    cost = 100;
    restrictionClasses[] = {"IFA"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] = {
        {"WW2_Fortification_Trench_Wide",-1,0,{0,0,0},0,{0,0,1}}
    };
};

class Land_trench_WW2_mortar: DefaultBunker{
    displayName = "Mortar trench";
    cost = 75;
    scope = 0;
    restrictionClasses[] = {"IFA"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] = {
        {"Land_WW2_Trench_Mortar",-1,0,{0,0,0},0,{0,0,1}}
    };
};

class Land_trench_WW2_small: DefaultBunker{
    scope = 0;
    displayName = "Infantry trench";
    cost = 200;
    restrictionClasses[] = {"IFA"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] = {
        {"WW2_Small_Infantry_Trench1",-1,0,{0,0,0},0,{0,0,1}}
    };
};
