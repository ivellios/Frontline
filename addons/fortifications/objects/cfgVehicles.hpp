class cfgVehicles {
    class Land_CargoBox_V1_F;
	class SVAR(SupplyContainer): Land_CargoBox_V1_F {
		scope = 2;
		scopeCurator = 2;
		armor = 275;
		displayName = "Supply Container";
		displayNameShort = "Supply Container";
		model = "\A3\Weapons_F\Ammoboxes\Supplydrop.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"a3\weapons_f\ammoboxes\data\supplydrop_co.paa"};
		slingLoadCargoMemoryPoints[] = {"SlingLoadCargo1","SlingLoadCargo2","SlingLoadCargo3","SlingLoadCargo4"};
		vehicleClass = "Container";
		destrType = "DestructBuilding";
		//destrType = "DestructNo";
		class EventHandlers {
			init = "if (local (_this select 0)) then { [{ (_this select 0) setMass 1750; (_this select 0) setVariable ['frl_interactMenuCategory', 'SupplyContainer', true]; (_this select 0) setVariable ['suppliesLoaded', 500, true]; }, _this] call clib_fnc_execNextFrame; };";
		};
		//damageResistance = 0.01;
	};
    class SVAR(VC_SupplyContainer): SVAR(SupplyContainer) {
		displayName = "VC upply Container";
		displayNameShort = "VC Supply Container";
        model = "\A3\Weapons_F\Ammoboxes\Proxy_UsBasicExplosives.p3d";
        class EventHandlers {
            init = "if (local (_this select 0)) then { [{ (_this select 0) setVariable ['frl_interactMenuCategory', 'SupplyContainer', true]; (_this select 0) setVariable ['suppliesLoaded', 500, true]; }, _this] call clib_fnc_execNextFrame; };";
        };
    };
};
