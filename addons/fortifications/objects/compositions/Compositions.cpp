#include "..\macros.hpp"

class DefaultComposition: DefaultFortification {
    scope = 2;
    cost = 300;
    constructionTime = 20;
    displayName = "Compositions";
    composition[] = {};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
};


class Compositions: DefaultComposition {
    category = 2;
    displayName = "Composition";
    scope = 2;

    #include "CUP.cpp"
    #include "IFA.cpp"
    #include "Vanilla.cpp"
    //#include "UNS"
};
