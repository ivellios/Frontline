#include "..\macros.hpp"

class Comp_Sandbag_round_big: DefaultComposition {
    displayName = "Rounded Sandbag nest (tall)";
    restrictionClasses[] = {"CUP"};
    cost = 100;
    constructionTime = 20;
    composition[] =
    {
        {"CraterLong",-1,0,{0,0,0},0,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.16064,1.20508,1.4283},84.9707,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.249512,3.61523,1.41132},318.727,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.09058,1.9209,1.3752},286.301,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.692139,3.04883,1.46443},117.396,{0,0,1}},
        {"Land_fort_bagfence_round",0,0,{4.41724,0.54834,0.586072},100.596,{0,0,1}},
        {"Land_fort_bagfence_round",0,0,{0.887695,3.1084,0.496796},333.503,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{3.80029,3.56396,1.37161},38.976,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{5.33496,1.854,1.34578},72.7551,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{4.83887,-1.0752,1.5121},315.738,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.967529,4.22998,1.49585},162.483,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{5.40161,-0.00488281,1.45459},109.195,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{1.68115,4.31641,1.44275},3.81305,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{5.50757,0.706055,1.50769},267.865,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{4.24048,-1.47363,1.459},157.069,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{4.9978,2.48877,1.39889},231.425,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{3.16724,3.9043,1.42472},197.646,{0,0,1}},
        {"CraterLong",0,0,{4.31543,0.69043,0},197.154,{0,0,1}}
    };
};
