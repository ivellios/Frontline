#include "..\macros.hpp"

class Comp_Sandbag_round: DefaultComposition {
    //crater with sandbags
    displayName = "Rounded Sandbag nest";
    cost = 100;
    constructionTime = 20;
    scope = 0;
    composition[] =
    {
        {"CraterLong",-1,0,{0,0,0},0,{0,0,1}},
        {"Land_fort_bagfence_round",0,0,{0.887695,3.1084,0.496796},333.503,{0,0,1}},
        {"Land_fort_bagfence_round",0,0,{4.41724,0.54834,0.586072},100.596,{0,0,1}},
        {"CraterLong",0,0,{4.31543,0.69043,0},197.154,{0,0,1}}
    };
};
class Comp_Sandbag_round_small: DefaultComposition {
    displayName = "Rounded Sandbag nest (small) ";
    cost = 70;
    constructionTime = 15;
    composition[] =
    {
        {"Land_BagFence_01_round_green_F",-1,0,{0,0,0},174.931,{0,0,1}},
        {"Land_WoodenShelter_01_F",0,0,{-0.218627,1.43663,0.140194},174.882,{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{-1.67372,1.49621,-0.00896072},262.21,{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{1.38175,1.42834,-0.0138435},91.3405,{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{1.38453,1.30987,0.709084},97.0123,{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{-1.63383,1.41642,0.713218},259.831,{0,0,1}},
        {"CraterLong_small",0,0,{0.618013,2.93648,-0.754232},27.5674,{0,0,1}},
        {"CraterLong_small",0,0,{-2.6075,1.80148,-0.754232},344.846,{0,0,1}}
    };
};
class comp_MG_shoothole: DefaultComposition {
    displayName = "MG position (empty)";
    cost = 60;
    scope = 0;
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,0.903654},0,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.2749,-2.4021,-0.956639},268.217,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.40918,-2.27759,-1.03732},90.8158,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.18652,-2.1687,-0.310174},86.0195,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.62939,-1.98706,-0.373136},88.6576,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-0.097168,0.277832,-0.138055},0,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.55566,-2.0127,-0.368575},86.0195,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-0.03125,-0.106201,-0.688312},0,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.31641,-1.96729,-0.298594},88.9534,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-0.00830078,0.284912,-0.903654},0,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-2.56934,-1.56177,0.0366583},79.4179,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{2.4458,-1.91089,-0.0167513},86.3612,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.58691,-0.742432,-0.0212226},225.275,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.61279,-0.335449,-0.413542},214.219,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.70996,-0.212891,-1.0937},215.625,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.84961,-0.720703,-1.57054},125.781,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.71191,-0.414063,0.0409956},138.42,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.75342,-0.360107,-0.895838},218.092,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.73926,-0.382813,0.0651016},220.13,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.94385,-0.121094,-0.934502},138.42,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.50146,-0.397949,-1.80052},206.894,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.57471,-0.546875,-0.106192},133.514,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.5835,-0.920654,-0.661189},229.779,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.82031,-0.25415,0.00909472},139.696,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.60547,-0.650879,-0.680732},135.479,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.65527,-0.220215,-0.591561},214.219,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.25635,-0.623291,-1.67521},206.894,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-2.0127,-0.313965,-0.49136},129.949,{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.870117,-1.5166,-2.31659},356.849,{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.949219,-1.42065,-2.29388},358.638,{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.241211,-0.75708,-0.975314},88.5024,{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.153809,-2.70898,-1.10328},87.8772,{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.00585938,-1.53394,-0.972215},87.8772,{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.0180664,-2.30786,-0.977518},87.8772,{0,0,1}}
    };
    class Comp_sandbag_bunker_tent: DefaultComposition {
        allowElevatedConstruction = 1;
        displayName = "sandbag bunker (tent)";
        cost = 60;
        constructionTime = 15;
        scope = 2;
        composition[] =
        {
            {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-1.12945},0,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{-0.777832,-3.84326,-0.387943},18,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{-1.44482,-3.04102,-0.387943},84,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{1.36963,-2.2334,-0.387943},300,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{-1.55176,-1.77417,-0.387943},90,{0,0,1}},
            {"Land_covering_hut_EP1",0,1,{-0.156738,-1.17554,-0.35},180,{-3.94658e-008,-0.261371,0.965238}},
            {"Land_SandbagBarricade_01_F",0,0,{-1.34961,-0.641846,-0.387943},120,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{1.35693,-0.596191,-0.387943},255,{0,0,1}},
            {"Land_BagFence_End_F",0,0,{1.09131,-0.332275,0.783466},27,{0,0,1}},
            {"Land_BagFence_End_F",0,0,{-1.12842,-0.279053,0.783466},159,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{0.620605,-4.22095,-0.521913},6.00001,{0,0,1}}
        };
    };
    class Comp_Tower_Sandbags: DefaultComposition {
        allowElevatedConstruction = 1;
        scope = 0;
        displayName = "fortified tower";
        composition[] =
        {
            {"Land_PierWooden_02_ladder_F",-1,0,{0,0,0},170.525,{0,0,1}},
            {"Land_BagFence_01_long_green_F",0,0,{-0.999381,-1.13904,20.1429},260.345,{0,0,1}},
            {"Land_BagFence_01_long_green_F",0,0,{0.80113,-2.37079,20.1347},348.793,{0,0,1}},
            {"Land_BagFence_01_long_green_F",0,0,{1.76499,-1.04697,20.1347},262.215,{0,0,1}},
            {"Land_Stanek_1",0,0,{0.293818,-1.22189,20.8625},259.204,{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{-0.797194,-1.81329,20.4771},80.5068,{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{1.95046,-2.33365,20.5107},348.932,{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{2.04111,-1.76446,20.4964},82.2148,{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{-0.297168,-2.34902,20.4801},179.359,{0,0,1}}
        };
    };
};
