#include "..\macros.hpp"

class comp_MGtrench_empty: DefaultComposition {
    scope = 0;
    displayName = "Machinegun trench (empty)";
    cost = 60;
    restrictionClasses[] = {"IFA_SUMMER"};
    allowElevatedConstruction = 0;
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,0},177.499,{0,0,1}},
        {"I44_Vegetation_MB_C_Ulmus02",0,0,{-0.973633,-1.5708,0.619687},113.94,{0,0,1}},
        {"WW2_Trench_MG_Low",0,0,{0.275391,2.28271,-0.36243},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{-2.16992,1.1189,0.00864792},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{-2.2085,2.40063,-0.23537},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{2.91699,2.99097,-0.320167},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{3.31543,1.34937,-0.325333},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{1.45703,-1.84375,-0.298847},0,{0,0,1}},
        {"LIB_B_Salix2s",0,0,{-1.1123,-1.42358,-0.226203},334.511,{0,0,1}},
        {"LIB_B_Salix2s",0,0,{2.89648,-0.160889,-0.355716},113.003,{0,0,1}},
        {"LIB_B_Salix2s",0,0,{-1.86816,-0.494629,-0.345438},0,{0,0,1}}
    };
};
class comp_MGtrench_empty_w: DefaultComposition {
    displayName = "Machinegun trench (empty winter)";
    cost = 60;
    restrictionClasses[] = {"IFA_WINTER"};
    allowElevatedConstruction = 0;
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,0},177.499,{0,0,1}},
        {"I44_Vegetation_MB_C_Ulmus02",0,0,{-0.833108,-1.57082,0.619687},113.94,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{3.1847,1.34932,-0.325333},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{-2.40976,2.40067,-0.23537},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{-2.25933,1.11894,0.00864792},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{1.61226,-1.84365,-0.298847},0,{0,0,1}},
        {"Land_I44_Buildings_Weeds",0,0,{2.64463,2.99083,-0.320167},0,{0,0,1}},
        {"LIB_B_Salix2s_w",0,0,{-0.984136,-1.42376,-0.226203},334.511,{0,0,1}},
        {"LIB_B_Salix2s_w",0,0,{-1.81834,-0.494624,-0.345438},0,{0,0,1}},
        {"LIB_B_Salix2s_w",0,0,{2.89962,-0.160964,-0.355716},113.003,{0,0,1}},
        {"Land_WW2_Trench_MG_Low_w",0,0,{0.0750991,2.28265,-0.36243},0,{0,0,1}}
    };
};
