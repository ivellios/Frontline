#include "..\macros.hpp"

class DefaultStaticWeapon: DefaultFortification {
    restrictionClasses[] = {"Locked"};
    scope = 2;
    cost = 350;
    constructionTime = 60;
    displayName = "Static Weapons";
    composition[] = {};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
};


class DefaultStaticWeaponSimple: DefaultStaticWeapon {
    allowElevatedConstruction = 1;
    cost = 200;
    constructionTime = 30;
};

class DefaultStaticWeaponBunker: DefaultStaticWeapon {
    cost = 300;
    constructionTime = 50;
};

class DefaultStaticWeaponHESCO: DefaultStaticWeapon {
    cost = 400;
    constructionTime = 120;
};

class DefaultStaticWeaponAA: DefaultStaticWeapon {
    cost = 500;
    constructionTime = 60;
    minPlayerCount = 5;
};

class DefaultStaticWeaponAAA: DefaultStaticWeapon {
    cost = 500;
    constructionTime = 60;
    minPlayerCount = 10;
};

class DefaultStaticWeaponHAT: DefaultStaticWeapon {
    cost = 500;
    constructionTime = 60;
};

class DefaultStaticWeaponMortar: DefaultStaticWeapon {
    cost = 500;
    constructionTime = 60;
	minPlayerCount = 15;
};

class DefaultStaticWeaponATGun: DefaultStaticWeapon {
    cost = 500;
    constructionTime = 60;
};

class DefaultWeaponCrate: DefaultStaticWeapon {
    allowElevatedConstruction = 1;
    displayName = "Weaponcrate";
    cost = 100;
    constructionTime = 15;
};

class StaticWeapons: DefaultStaticWeapon {
    category = 1;
    displayName = "Static Weaponry";
    scope = 2;

    //#include "CUP";
    #include "IFA.cpp"
    #include "Vanilla.cpp"
    #include "RHS.cpp"
    #include "UNS.cpp"

};
