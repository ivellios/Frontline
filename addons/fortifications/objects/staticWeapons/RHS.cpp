#include "..\macros.hpp"

class frl_rhs_ATCrate_ins: DefaultWeaponCrate {
    cost = 200;
    displayName = "AT supplies and explosives";
    restrictionClasses[] = {"INS_HMGS"};
};
class frl_rhs_ATCrate_USAF: DefaultWeaponCrate {
    cost = 200;
    displayName = "AT supplies and explosives";
    restrictionClasses[] = {"US_RHS_HMGS"};
};
class frl_rhs_ATCrate_IRAQ: DefaultWeaponCrate {
    cost = 200;
    displayName = "AT supplies and explosives";
    restrictionClasses[] = {"US_RHS_HMGS"};
};
class frl_rhs_ATCrate_AFRF: DefaultWeaponCrate {
    cost = 200;
    displayName = "AT supplies and explosives";
    restrictionClasses[] = {"RU_HMGS"};
};

class comp_M2_sandbagNest_chevron: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    restrictionClasses[] = {"US_RHS_HMGS"};
    displayName = "M2, sandbag nest (chevron)";
    scope = 2;
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.0479999},178.193,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.00556236,0.550028,-1.65266},0,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.0238522,1.09844,-1.66366},0,{0,0,1}},
        {"RHS_M2StaticMG_D",0,0,{-0.414604,0.855815,0.470438},0,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.11493,1.94955,-0.670643},126.15,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.38482,1.99985,-0.771474},236.359,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-1.24133,0.526228,-0.345884},226.106,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.17083,0.549588,-0.28618},127.334,{0,0,1}}
    };
};
class comp_DSHKM_sandbagNest_chevron: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    restrictionClasses[] = {"INS_HMGS"};
    displayName = "Dshkm, sandbag nest (chevron)";
    scope = 2;
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.178},178.193,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.11491,1.95028,-0.43271},126.15,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.23265,2.11258,-1.32489},126.15,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.0719573,0.500884,-1.46266},356.811,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.0306582,1.23815,-1.46066},0,{0,0,1}},
        {"rhsgref_ins_g_DSHKM",0,0,{-0.36803,1.09495,0.500768},357.599,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.44243,2.23423,-1.37884},236.359,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.38484,2.00058,-0.43271},236.359,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-1.32027,0.63281,-1.31313},227.625,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{0.029241,-0.0923386,-1.26865},178.079,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{1.23585,0.559482,-1.29933},121.042,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-1.24136,0.52696,-0.346583},226.106,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.1708,0.55032,-0.286878},127.334,{0,0,1}}
    };
};
class comp_Kord_sandbagNest_chevron: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    restrictionClasses[] = {"RU_HMGS"};
    displayName = "Kord, sandbag nest (chevron)";
    scope = 2;
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.182},178.193,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.0136001,0.650698,-1.52466},0,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.0252244,0.946763,-1.52666},0,{0,0,1}},
        {"rhs_KORD_high_VDV",0,0,{-0.232243,1.07165,0.532756},0,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{0.194136,0.646221,-1.33787},273.289,{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{-0.0991893,0.70228,-1.36641},90.864,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.23314,2.11256,-1.32482},126.15,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.44194,2.23422,-1.37877},236.359,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.11539,1.95027,-0.432642},126.15,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-1.31978,0.632795,-1.31306},227.625,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{1.23634,0.559466,-1.29926},121.042,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.38435,2.00057,-0.432642},236.359,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{0.029729,-0.092354,-1.26858},178.079,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-1.24087,0.526944,-0.346514},226.106,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.17129,0.550305,-0.286809},127.334,{0,0,1}}
    };
};


class comp_M2_blank: DefaultStaticWeaponSimple {
    displayName = "M2 HMG";
    restrictionClasses[] = {"US_RHS_HMGS"};
    composition[] =
    {
        {"Land_BagFence_Corner_F",-1,0,{0,0,-0.582392},329.97,{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.0942383,-0.250977,-0.183094},{0.500454,0.865763,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.400391,-0.708984,-0.194094},{0.500454,0.865763,0},{0,0,1}},
        {"RHS_M2StaticMG_D",0,0,{0.114746,-0.710205,1.939},{0.500454,0.865763,0},{0,0,1}}
    };
};

class comp_DSHKM_blank: DefaultStaticWeaponSimple {
    displayName = "Dshkm HMG";
    restrictionClasses[] = {"INS_HMGS"};
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,-0.599503},0,{0,0,1}},
        {"rhsgref_ins_g_DSHKM",0,0,{0.200684,-1.22437,1.85756},{-0.041893,0.999122,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.133301,-0.526367,-0.189871},{-0.0556299,0.998451,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.166504,-0.811279,-0.200871},{0,1,0},{0,0,1}}
    };
};

class comp_KORD_blank: DefaultStaticWeaponSimple {
    displayName = "KORD HMG";
    restrictionClasses[] = {"RU_HMGS"};
    composition[] =
    {
        {"Land_BagFence_Corner_F",-1,0,{0,0,-0.595522},90.864,{0,0,1}},
        {"rhs_KORD_high_VDV",0,0,{0.364014,0.12793,1.89941},{-0.999886,-0.0150787,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.0463867,-0.112305,-0.158005},{-0.999886,-0.0150787,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{0.250732,-0.12793,-0.161005},{-0.999886,-0.0150787,0},{0,0,1}},
        {"Land_BagFence_Corner_F",0,0,{-0.0424805,-0.29248,0.0285392},{-0.0423135,-0.999104,0},{0,0,1}}
    };
};

class comp_DSHKM_basic: DefaultStaticWeaponSimple {
    displayName = "Dshkm (basic cover)";
    restrictionClasses[] = {"INS_HMGS"};
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,-0.432303},0,{0,0,1}},
        {"rhsgref_ins_DSHKM_Mini_TriPod",0,0,{0.126953,-1.37354,1.30893},{0,1,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{1.02295,-0.884766,-0.14797},{0.99847,0.0552975,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.16504,-0.806396,-0.14797},{0.996419,-0.0845534,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.19434,-0.716064,0.415355},{0.996419,-0.0845534,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.993652,-0.794434,0.415355},{0.99847,0.0552975,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.215332,-0.631592,-0.408424},{-0.0556299,0.998451,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.685059,-0.144287,0.415355},{-0.718311,-0.695722,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.917969,-0.135742,0.415355},{-0.698446,0.715662,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.714355,-0.234619,-0.14797},{-0.718311,-0.695722,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.888672,-0.226074,-0.14797},{-0.698446,0.715662,0},{0,0,1}}
    };
};

class comp_NSV_basic: DefaultStaticWeaponSimple {
    scope = 0;
    displayName = "NSV HMG (basic cover)";
    restrictionClasses[] = {"RU_HMGS"};
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,-0.410713},0,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.17822,-0.751709,-0.152114},{0.996419,-0.0845534,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.19434,-0.716064,0.415355},{0.996419,-0.0845534,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.179199,-0.733398,-0.400871},{-0.0556299,0.998451,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.917969,-0.135742,0.415355},{-0.698446,0.715662,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.901855,-0.171387,-0.152114},{-0.698446,0.715662,0},{0,0,1}},
        {"RHS_NSV_TriPod_VDV",0,0,{0.197266,-0.875732,1.33089},{-0.0371321,0.99931,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.993652,-0.794434,0.415355},{0.99847,0.0552975,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{1.00977,-0.830078,-0.152114},{0.99847,0.0552975,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.685059,-0.144287,0.415355},{-0.718311,-0.695722,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.701172,-0.179932,-0.152114},{-0.718311,-0.695722,0},{0,0,1}}
    };
};

class comp_M2_basic: DefaultStaticWeaponSimple {
    displayName = "M2 HMG (basic cover)";
    restrictionClasses[] = {"US_RHS_HMGS"};
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,-0.402173},0,{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.993652,-0.794434,0.415355},{0.99847,0.0552975,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.19434,-0.716064,0.415355},{0.996419,-0.0845534,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.685059,-0.144287,0.415355},{-0.718311,-0.695722,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.917969,-0.135742,0.415355},{-0.698446,0.715662,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.663574,-0.220459,-0.199361},{-0.718311,-0.695722,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{0.972168,-0.870605,-0.199361},{0.99847,0.0552975,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-0.939453,-0.211914,-0.199361},{-0.698446,0.715662,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-1.21582,-0.792236,-0.199361},{0.996419,-0.0845534,0},{0,0,1}},
        {"RHS_M2StaticMG_MiniTripod_WD",0,0,{0.108398,-0.571777,0.0127974},{0,1,0},{0,0,1}},
        {"Land_Pallets_stack_F",0,0,{-0.0141602,-0.741455,-0.420298},{-0.0556299,0.998451,0},{0,0,1}}
    };
};

class comp_M2_Foxhole: DefaultStaticWeaponBunker {
    displayName = "M2 .50 cal HMG (foxhole)";
    allowElevatedConstruction = 0;
    scope = 2;
    restrictionClasses[] = {"US_RHS_HMGS"};
    composition[] =
    {
        {"Land_BagFence_01_long_green_F",-1,0,{0,0,0.054451},0.925,{0,0,1}},
        {"RHS_M2StaticMG_USMC_WD",0,0,{0.291748,-0.065918,1.2069},{-0.00344609,0.999994,0},{0,0,1}},
        {"CUP_Krovi",0,0,{-2.66821,-1.42285,-0.131943},{0.99987,0.0161512,0},{0,0,1}},
        {"CUP_Krovi",0,0,{2.73804,-1.37842,-0.127485},{0.99987,0.0161512,0},{0,0,1}},
        {"CUP_Krovi",0,0,{0.034668,1.22314,-0.170213},{0.0161512,-0.99987,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.0834961,1.25244,-1.33671},{0.0139873,-0.865912,0.5},{-0.00807561,0.499935,0.866025}},
        {"Land_Trench_01_forest_F",0,0,{2.67627,-1.36035,-1.33671},{-0.865912,-0.0139873,0.5},{0.499935,0.00807561,0.866025}},
        {"Land_Trench_01_forest_F",0,0,{-2.72827,-1.38135,-1.33671},{0.865912,0.0139874,0.5},{-0.499935,-0.00807561,0.866025}},
        //{"Land_Trench_01_forest_F",0,0,{-0.0932617,-4.12305,-1.33671},{-0.0139873,0.865912,0.5},{0.00807561,-0.499935,0.866025}},
        {"Land_BagFence_01_long_green_F",0,0,{-1.49121,-1.55225,-0.0627174},{0.99987,0.0161512,0},{0,0,1}},
        //{"Land_BagFence_01_long_green_F",0,0,{-0.010498,-2.86914,-0.0627174},{0,1,0},{0,0,1}},
        {"Land_BagFence_01_long_green_F",0,0,{1.39429,-1.52637,-0.0627174},{-0.99987,-0.0161511,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{-0.0788574,0.234375,0.173538},{0.0172819,-0.999851,0},{0,0,1}}
    };
};

class comp_DHSKM_Foxhole: DefaultStaticWeaponBunker {
    displayName = "DSHKM HMG (foxhole)";
    allowElevatedConstruction = 0;
    scope = 2;
    restrictionClasses[] = {"INS_HMGS"};
    composition[] =
    {
        {"Land_BagFence_01_long_green_F",-1,0,{0,0,0.328731},0.925,{0,0,1}},
        {"CUP_Krovi",0,0,{-2.66772,-1.42236,-0.131943},{0.99987,0.0161512,0},{0,0,1}},
        {"CUP_Krovi",0,0,{0.0349121,1.22266,-0.170213},{0.0161512,-0.99987,0},{0,0,1}},
        {"CUP_Krovi",0,0,{2.73828,-1.37842,-0.127485},{0.99987,0.0161512,0},{0,0,1}},
        {"rhsgref_ins_g_DSHKM",0,0,{0.30542,-0.37207,1.19456},{0.00493334,0.999988,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-2.72803,-1.38086,-1.33671},{0.865912,0.0139874,0.5},{-0.499935,-0.00807561,0.866025}},
        {"Land_Trench_01_forest_F",0,0,{-0.0834961,1.25195,-1.33671},{0.0139873,-0.865912,0.5},{-0.00807561,0.499935,0.866025}},
        {"Land_Trench_01_forest_F",0,0,{2.67651,-1.35986,-1.33671},{-0.865912,-0.0139873,0.5},{0.499935,0.00807561,0.866025}},
        //{"Land_BagFence_01_long_green_F",0,0,{-0.010498,-2.86914,-0.0627174},{0,1,0},{0,0,1}},
        {"Land_BagFence_01_long_green_F",0,0,{1.39453,-1.52588,-0.0627174},{-0.99987,-0.0161511,0},{0,0,1}},
        {"Land_BagFence_01_long_green_F",0,0,{-1.49097,-1.55225,-0.0627174},{0.99987,0.0161512,0},{0,0,1}},
        //{"Land_BagFence_01_round_green_F",0,0,{-0.0788574,0.234375,0.173538},{0.0172819,-0.999851,0},{0,0,1}},
        //{"Land_Trench_01_forest_F",0,0,{-0.0932617,-4.12305,-1.33671},{-0.0139873,0.865912,0.5},{0.00807561,-0.499935,0.866025}}
    };
};

class comp_KORD_Foxhole: DefaultStaticWeaponBunker {
    displayName = "KORD HMG (foxhole)";
    allowElevatedConstruction = 0;
    scope = 2;
    restrictionClasses[] = {"RU_HMGS"};
    composition[] =
    {
        {"Land_BagFence_01_long_green_F",-1,0,{0,0,0.328731},0.925,{0,0,1}},
        {"CUP_Krovi",0,0,{2.73828,-1.37842,-0.127485},{0.99987,0.0161512,0},{0,0,1}},
        {"CUP_Krovi",0,0,{0.034668,1.22266,-0.170213},{0.0161512,-0.99987,0},{0,0,1}},
        {"CUP_Krovi",0,0,{-2.66797,-1.42285,-0.131943},{0.99987,0.0161512,0},{0,0,1}},
        {"rhs_KORD_high_VDV",0,0,{0.259277,-0.0805664,1.32877},{0.00774567,0.99997,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.0834961,1.25195,-1.33671},{0.0139873,-0.865912,0.5},{-0.00807561,0.499935,0.866025}},
        {"Land_Trench_01_forest_F",0,0,{2.67627,-1.35986,-1.33671},{-0.865912,-0.0139873,0.5},{0.499935,0.00807561,0.866025}},
        //{"Land_Trench_01_forest_F",0,0,{-0.0935059,-4.12256,-1.33671},{-0.0139873,0.865912,0.5},{0.00807561,-0.499935,0.866025}},
        {"Land_Trench_01_forest_F",0,0,{-2.72827,-1.38135,-1.33671},{0.865912,0.0139874,0.5},{-0.499935,-0.00807561,0.866025}},
        {"Land_BagFence_01_long_green_F",0,0,{1.39404,-1.52637,-0.0627174},{-0.99987,-0.0161511,0},{0,0,1}},
        //{"Land_BagFence_01_long_green_F",0,0,{-0.0107422,-2.86914,-0.0627174},{0,1,0},{0,0,1}},
        {"Land_BagFence_01_long_green_F",0,0,{-1.49097,-1.55225,-0.0627174},{0.99987,0.0161512,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{-0.0791016,0.234863,0.173538},{0.0172819,-0.999851,0},{0,0,1}}
    };
};

class Comp_Sandbagbunker_Kord: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    displayname = "Kord HMG (sandbag bunker)";
    scope = 2;
    restrictionClasses[] = {"RU_HMGS"};
    composition[] =
    {
        {"Land_BagBunker_Small_F",-1,0,{0,0,0},190.322,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-2.10362,-0.986428,0.320094},98.0568,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-1.39857,0.820485,-0.281651},277.537,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.41675,-1.05218,0.0778637},281.341,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.5798,3.79336,0.318409},191.523,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.98932,0.797483,-0.28735},98.7139,{0,0,1}},
        {"rhs_KORD_high_VDV",0,0,{-0.0726481,0.400591,0.646938},12.607,{-0.000311518,0.000450777,1}}
    };
};

class Comp_Sandbagbunker_M2: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    displayname = "M2 HMG (sandbag bunker)";
    scope = 2;
    restrictionClasses[] = {"US_RHS_HMGS"};
    composition[] =
    {
        {"Land_BagBunker_Small_F",-1,0,{0,0,0},190.322,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-2.10362,-0.986428,0.320094},98.0568,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-1.39857,0.820485,-0.281651},277.537,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.41675,-1.05218,0.0778637},281.341,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.20023,3.23424,0.320094},188.621,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.98932,0.797483,-0.28735},98.7139,{0,0,1}},
        {"RHS_M2StaticMG_WD",0,0,{-0.367985,0.0973965,0.722586},6.9646,{-9.30081e-007,2.60381e-005,1}}
    };
};

class Comp_Sandbagbunker_Dshkm: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    displayname = "Dshkm HMG (sandbag bunker)";
    scope = 2;
    restrictionClasses[] = {"INS_HMGS"};
    composition[] =
    {
        {"Land_BagBunker_Small_F",-1,0,{0,0,0},190.322,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-2.10362,-0.986428,0.320094},98.0568,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-1.39857,0.820485,-0.281651},277.537,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.41675,-1.05218,0.0778637},281.341,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.20023,3.23424,0.320094},188.621,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.98932,0.797483,-0.28735},98.7139,{0,0,1}},
        {"rhsgref_nat_DSHKM",0,0,{-0.367985,0.0973965,0.722586},6.9646,{-9.30081e-007,2.60381e-005,1}}
    };
};

class Comp_Ruins_DSHKM: DefaultStaticWeaponBunker {
    displayname = "Dshkm HMG (fortified ruins) ";
    scope = 0;
    restrictionClasses[] = {"INS_DESERT"};
    allowElevatedConstruction = 0;
    composition[] =
    {
        {"Land_BagBunker_Small_F",-1,0,{0,0,0},173.454,{0,0,1}},
        {"Land_House_L_1_ruins_EP1",0,0,{0.474148,-0.659019,-0.00929165},0,{0,0,1}},
        {"Land_House_L_1_ruins_EP1",0,0,{0.0204209,-0.161275,-0.00929165},174.159,{0,0,1}},
        {"Land_WoodenShelter_01_F",0,0,{0.639496,-1.0466,1.93786},158.291,{0,0,1}},
        {"rhsgref_ins_g_DSHKM_Mini_TriPod",0,0,{0.25389,0.0527262,2.31341},346.75,{-3.25742e-006,-3.65683e-006,1}}
    };
};

class comp_DSHKM_shoothole: DefaultStaticWeaponBunker {
    displayName = "DSHKM position (sandbags)";
    restrictionClasses[] = {"INS_HMGS"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.485196},180.505,{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.42383,2.26587,-0.494192},{-0.999985,0.00542252,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.64648,1.97729,0.169996},{-0.99948,-0.0322407,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.3335,1.95483,0.244538},{-0.999633,-0.0270792,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{2.04443,0.299072,0.0517716},{-0.772246,0.635324,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{2.59033,1.55176,0.57979},{-0.981335,-0.192304,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.164551,2.67725,-0.58564},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0322266,2.2749,-0.517279},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.26123,2.34912,-0.413508},{0.999203,0.0399279,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.269043,0.726563,-0.533515},{-0.999389,-0.034948,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0976563,1.49805,-0.522366},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.1709,2.11646,0.232957},{-0.996937,-0.0782088,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.53857,1.95728,0.174557},{-0.996937,-0.0782088,0},{0,0,1}},
        {"rhsgref_ins_DSHKM_Mini_TriPod",0,0,{-0.267578,0.830811,0.801858},{0.053686,-0.998558,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.22705,0.579346,-1.13208},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.67725,0.165039,-0.550565},{0.575282,0.817955,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.47021,0.352051,-1.25739},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.62256,0.172852,-0.0484295},{0.555042,0.831823,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55908,0.695801,0.521909},{0.704259,0.709943,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.70801,0.334717,0.608233},{0.637756,0.770238,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.87793,0.704346,-1.02741},{-0.816381,0.577514,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.74268,0.396484,0.584127},{-0.670237,0.742147,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.97754,0.105469,-0.39137},{-0.670237,0.742147,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.60449,0.528076,0.43694},{-0.731253,0.682106,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.85254,0.237549,0.552226},{-0.653539,0.756893,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.63428,0.632324,-0.137601},{-0.707424,0.706789,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.72217,0.312012,-0.352707},{0.609969,0.792425,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55713,0.874023,-0.118057},{0.757835,0.652446,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.58105,0.288574,0.129589},{0.555042,0.831823,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.848633,1.47607,-1.77346},{0.0637712,-0.997965,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.971191,1.39624,-1.75075},{0.032585,-0.999469,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-2.42822,1.85669,0.52638},{-0.997386,-0.0722622,0},{0,0,1}}
    };
};
class comp_NSV_shoothole: DefaultStaticWeaponBunker {
    scope = 0;
    displayName = "NSV position (sandbags)";
    restrictionClasses[] = {"RU_HMGS"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.500005},180.505,{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.164551,2.67725,-0.401649},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0322266,2.2749,-0.275889},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.53857,1.95728,0.333054},{-0.996937,-0.0782088,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.3335,1.95459,0.403035},{-0.999633,-0.0270792,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.1709,2.11646,0.391454},{-0.996937,-0.0782088,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.64648,1.97729,0.328493},{-0.99948,-0.0322407,0},{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.26123,2.34912,-0.255011},{0.999203,0.0399279,0},{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.42383,2.26587,-0.335695},{-0.999985,0.00542252,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-2.42822,1.85645,0.684877},{-0.997386,-0.0722622,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.269043,0.726563,-0.273685},{-0.999389,-0.034948,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0976563,1.49805,-0.270586},{-0.998948,-0.0458508,0},{0,0,1}},
        {"RHS_NSV_TriPod_VDV",0,0,{-0.283203,0.772705,0.868223},{-0.000373052,-1,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55859,0.695801,0.680406},{0.704259,0.709943,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.58105,0.288574,0.288086},{0.555042,0.831823,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.47021,0.352051,-1.0989},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{2.04443,0.299072,0.210269},{-0.772246,0.635324,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.62256,0.172852,0.110068},{0.555042,0.831823,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.74268,0.396484,0.742624},{-0.670237,0.742147,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.97754,0.105469,-0.232873},{-0.670237,0.742147,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.60449,0.528076,0.595437},{-0.731253,0.682106,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55713,0.874023,0.0404401},{0.757835,0.652446,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.67725,0.165039,-0.392067},{0.575282,0.817955,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.63428,0.632324,0.0208964},{-0.707424,0.706789,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.72217,0.312012,-0.19421},{0.609969,0.792425,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.70801,0.334717,0.76673},{0.637756,0.770238,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.22705,0.579346,-0.973585},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.87793,0.704346,-0.868916},{-0.816381,0.577514,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.85254,0.237549,0.710723},{-0.653539,0.756893,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.848633,1.47607,-1.61496},{0.0637712,-0.997965,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.971191,1.39624,-1.59225},{0.032585,-0.999469,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{2.59033,1.55176,0.738287},{-0.981335,-0.192304,0},{0,0,1}}
    };
};
class comp_M2_shoothole: DefaultStaticWeaponBunker {
    displayName = "M2 position (sandbags)";
    restrictionClasses[] = {"US_RHS_HMGS"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.587094},180.505,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.3335,1.95483,0.403035},{-0.999633,-0.0270792,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.64648,1.97729,0.328493},{-0.99948,-0.0322407,0},{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.42334,2.26587,-0.335695},{-0.999985,0.00542252,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.97705,0.105469,-0.232873},{-0.670237,0.742147,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{2.04443,0.299072,0.210269},{-0.772246,0.635324,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{2.58984,1.55151,0.738287},{-0.981335,-0.192304,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0322266,2.27515,-0.275889},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.164551,2.67749,-0.401649},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.53906,1.95728,0.333054},{-0.996937,-0.0782088,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.17139,2.11646,0.391454},{-0.996937,-0.0782088,0},{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.26172,2.34912,-0.255011},{0.999203,0.0399279,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-2.42822,1.85645,0.684877},{-0.997386,-0.0722622,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.269043,0.726563,-0.273685},{-0.999389,-0.034948,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0976563,1.4978,-0.270586},{-0.998948,-0.0458508,0},{0,0,1}},
        {"RHS_M2StaticMG_MiniTripod_WD",0,0,{-0.0253906,0.578369,-0.394804},{0.00881622,-0.999961,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.22754,0.57959,-0.973585},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.4707,0.352051,-1.0989},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.67725,0.165283,-0.392067},{0.575282,0.817955,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.72217,0.312012,-0.19421},{0.609969,0.792425,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55713,0.874023,0.0404401},{0.757835,0.652446,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.58154,0.288574,0.288086},{0.555042,0.831823,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55908,0.695801,0.680406},{0.704259,0.709943,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.87744,0.704346,-0.868916},{-0.816381,0.577514,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.74268,0.396484,0.742624},{-0.670237,0.742147,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.60449,0.528076,0.595437},{-0.731253,0.682106,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.85254,0.237549,0.710723},{-0.653539,0.756893,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.63428,0.632324,0.0208964},{-0.707424,0.706789,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.62305,0.172852,0.110068},{0.555042,0.831823,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.70801,0.334717,0.76673},{0.637756,0.770238,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.971191,1.39648,-1.59225},{0.032585,-0.999469,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.849121,1.47607,-1.61496},{0.0637712,-0.997965,0},{0,0,1}}
    };
};

class Comp_Hesco_M2: DefaultStaticWeaponHESCO {
    displayName = "M2 HMG (hesco)";	//small issues with sandbags on side
    scope = 2;
    restrictionClasses[] = {"US_RHS_HMGS"};
    allowElevatedConstruction = 0;
    composition[] =
    {
        {"Land_HBarrierTower_F",-1,0,{0,0,-2.17618},175.797,{0,0,1}},
        {"Land_BagFence_Long_F",0,0,{2.2091,-1.54271,1.57603},85,{0,0,1}},
        {"Land_BagFence_Long_F",0,0,{-2.2091,-1.54271,1.57603},-89.15,{0,0,1}},
        {"RHS_M2StaticMG_WD",0,0,{-0.0985567,-2.2,1.78476},356.48,{0.00149032,-0.000120237,0.999999}}
    };
};

class Comp_Hesco_KORD: DefaultStaticWeaponHESCO {
    displayName = "KORD HMG (hesco)";
    scope = 2;
    restrictionClasses[] = {"RU_HMGS"};
    allowElevatedConstruction = 0;
    composition[] =
    {
        {"Land_HBarrierTower_F",-1,0,{0,0,-2.17618},175.797,{0,0,1}},
        {"Land_BagFence_Long_F",0,0,{2.2091,-1.54271,1.57603},85,{0,0,1}},
        {"Land_BagFence_Long_F",0,0,{-2.2091,-1.54271,1.57603},-89.15,{0,0,1}},
        {"rhs_KORD_high_VDV",0,0,{-0.0504377,-2.2,1.76687},356.672,{-0.00041105,0.000341832,1}}
    };
};

class comp_SPG9_shoothole: DefaultStaticWeaponATGun {
    displayName = "SPG-9M position (sandbags)";
    restrictionClasses[] = {"INS_HMGS"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.50783},180.505,{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0322266,2.27515,-0.342717},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.164551,2.67749,-0.407271},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.64648,1.97729,0.328493},{-0.99948,-0.0322407,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.33398,1.95483,0.403035},{-0.999633,-0.0270792,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.17041,2.11646,0.391454},{-0.996937,-0.0782088,0},{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.53809,1.95728,0.333054},{-0.996937,-0.0782088,0},{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{-2.26074,2.34912,-0.255011},{0.999203,0.0399279,0},{0,0,1}},
        {"Land_SandbagBarricade_01_half_F",0,0,{2.42383,2.26587,-0.335695},{-0.999985,0.00542252,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.269043,0.726563,-0.368381},{-0.999389,-0.034948,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.0976563,1.49805,-0.339345},{-0.998948,-0.0458508,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.67676,0.165283,-0.392067},{0.575282,0.817955,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.58057,0.288574,0.288086},{0.555042,0.831823,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.62207,0.173096,0.110068},{0.555042,0.831823,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.46973,0.352051,-1.0989},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.22656,0.57959,-0.973585},{0.444457,0.8958,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.87793,0.704346,-0.868916},{-0.816381,0.577514,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55811,0.695801,0.680406},{0.704259,0.709943,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{2.04443,0.299072,0.210269},{-0.772246,0.635324,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.55664,0.874023,0.0404401},{0.757835,0.652446,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.70752,0.334961,0.76673},{0.637756,0.770238,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.85254,0.237549,0.710723},{-0.653539,0.756893,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.63428,0.632324,0.0208964},{-0.707424,0.706789,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.72119,0.312012,-0.19421},{0.609969,0.792425,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.97754,0.105713,-0.232873},{-0.670237,0.742147,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.60449,0.528076,0.595437},{-0.731253,0.682106,0},{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.74316,0.396484,0.742624},{-0.670237,0.742147,0},{0,0,1}},
        {"rhsgref_cdf_b_SPG9M",0,0,{-0.0102539,-0.141357,-0.480497},{0.0217172,-0.999764,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.848145,1.47632,-1.61496},{0.0637712,-0.997965,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.971191,1.39648,-1.59225},{0.032585,-0.999469,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{-2.42725,1.85645,0.684877},{-0.997386,-0.0722622,0},{0,0,1}},
        {"Land_BagFence_End_F",0,0,{2.59033,1.55176,0.738287},{-0.981335,-0.192304,0},{0,0,1}}
    };
};

class wp_SPG9m: DefaultStaticWeaponATGun {
    allowElevatedConstruction = 1;
    displayName = "SPG-9M (blank)";
    restrictionClasses[] = {"INS_HMGS"};
    listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
    composition[] =
    {
        {"Land_BagFence_01_round_green_F",-1,0,{0,0,-0.381072},183.271,{0,0,1}},
        {"Pallet",0,0,{0.101563,0.523682,-0.109514},{0.0570522,-0.998371,0},{0,0,1}},
        {"rhsgref_ins_g_SPG9M",0,0,{-0.019043,-0.0300293,-0.0391827},{0.0570522,-0.998371,0},{0,0,1}}
    };
};

class Comp_Ruins_SPG9: DefaultStaticWeaponATGun {
    displayname = "SPG-9 (fortified ruins) ";
    minPlayerCount = 10;
    scope = 0;
    restrictionClasses[] = {"INS_DESERT"};
    allowElevatedConstruction = 0;
    composition[] =
    {
        {"Land_BagBunker_Small_F",-1,0,{0,0,0},173.454,{0,0,1}},
        {"Land_House_L_1_ruins_EP1",0,0,{0.474148,-0.659019,-0.00929165},0,{0,0,1}},
        {"Land_House_L_1_ruins_EP1",0,0,{0.0204209,-0.161275,-0.00929165},174.159,{0,0,1}},
        {"Land_WoodenShelter_01_F",0,0,{0.639496,-1.0466,2.13729},158.291,{0,0,1}},
        {"rhs_SPG9M_VDV",0,0,{0.83077,-0.727706,1.02766},353.618,{0.000477092,-0.00210377,0.999998}}
    };
};

// -- AAA

class Comp_Sandbag_ZSU23_2: DefaultStaticWeapon {
    allowElevatedConstruction = 1;
    displayName = "ZSU-23-2 (sandbag nest)";
    cost = 1000;
    scope = 2;
    minPlayerCount = 15;
    restrictionClasses[] = {"INS_AA"};
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,-0.5},178.019,{0,0,1}},
        {"CUP_ammobednaX",0,0,{-2.31615,2.52178,-0.0632229},91.1176,{0,0,1}},
        {"CUP_ammobednaX",0,0,{-1.54628,3.92325,-0.0632229},328.484,{0,0,1}},
        {"CUP_ammobednaX",0,0,{1.97253,2.08512,-0.0632229},91.1176,{0,0,1}},
        {"CUP_ammobednaX",0,0,{1.2359,3.61132,-0.0632229},208.342,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.80265,2.49572,0.0164251},268.09,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.49448,2.21377,0.0329061},87.1209,{0,0,1}},
        {"rhsgref_cdf_b_ZU23",0,0,{-0.132048,1.84452,1.80003},359.998,{3.00068e-006,-0.000212739,1}},
        {"Land_BagFence_Round_F",0,0,{1.86025,0.568784,0.0213265},136.046,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-2.00324,0.6905,0.0213265},219.495,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.78402,3.87116,0.0213265},45.0543,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-2.1003,4.27092,0.0213265},316.658,{0,0,1}}
    };
};

// -- manpads

class WP_ilga_blank: DefaultStaticWeaponAA {
    displayName = "9K38 Igla";
    scope = 2;
    restrictionClasses[] = {"RU_MANPADS"};
    composition[] =
    {
        {"Land_BagFence_Round_F",-1,0,{0,0,0},167.368,{0,0,1}},
        {"Land_WoodenTable_large_F",0,1,{-0.551758,1.29004,-0.781016},0,{0,0,1}},
        {"Land_WoodenTable_large_F",0,1,{0.554688,1.02759,-0.790016},0,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-0.0375977,-0.020752,-0.39098},167.368,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.11816,1.38281,0},268.459,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.06592,0.797852,-0.430211},92.1033,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.14502,1.36816,-0.389009},268.459,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.104,0.818604,-0.0392313},92.1033,{0,0,1}},
        {"rhs_Igla_AA_pod_msv",0,0,{-0.26123,1.04443,1.18721},0,{0,0,1}}
    };
};
class WP_stinger_blank: DefaultStaticWeaponAA {
    allowElevatedConstruction = 1;
    displayName = "FIM-92 Stinger";
    scope = 2;
    restrictionClasses[] = {"US_MANPADS"};
    composition[] =
    {
        {"Land_BagFence_Round_F",-1,0,{0,0,0.12227},173.098,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.16016,1.45264,-0.376655},270.177,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.34912,1.2041,-0.388857},82.2532,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.28418,1.19946,0.0209432},82.2532,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-1.2251,1.44849,0.0331459},270.177,{0,0,1}},
        {"RHS_Stinger_AA_pod_USMC_WD",0,0,{0.00390625,0.854492,0.895144},0,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{0.0649414,0.00463867,-0.409801},173.098,{0,0,1}}
    };
};

// -- ATGMS

class Comp_Sandbag_Kornet: DefaultStaticWeaponHAT {
    displayName = "9M133-Kornet (dugout)";
    scope = 2;
    restrictionClasses[] = {"RU_ATGMS"};
    minPlayerCount = 15;
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,-0.18066},180,{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.20459,-2.36597,1.15656},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.166504,-2.46509,1.72829},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.211426,-3.75049,0.158074},{-0.0109917,0.855875,0.517065},{0.0105075,-0.516969,0.85594}},
        {"Land_Plank_01_4m_F",0,0,{-0.586914,-0.982178,1.2118},{-0.0203074,0.999123,0.0366108},{0.000743969,-0.0366033,0.99933}},
        {"Land_Plank_01_4m_F",0,0,{0.171387,-0.980469,1.22129},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.831055,-1.00854,1.20033},{-0.020321,0.999793,0},{0,0,1}},
        {"rhs_Kornet_9M133_2_vmf",0,0,{0.119141,-1.44971,1.79255},{-0.00144148,-0.999999,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.879395,1.3291,1.58515},{0.0203209,-0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.796875,1.37134,1.6285},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.840332,1.50366,1.31115},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.835449,1.46094,1.26789},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.798828,1.15063,1.29196},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.876953,1.1936,1.33522},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{1.73584,-1.39941,1.73963},{-0.8936,0.448864,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-1.59473,-1.36548,1.69892},{-0.875798,-0.482678,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{-3.73584,1.27319,0.484839},{-0.977466,0.211095,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{3.84619,0.887695,0.449244},{0.985121,0.171859,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-2.24414,-2.23608,0.230049},{0.743905,0.423384,0.517061},{-0.447464,-0.259111,0.855942}},
        {"Land_Trench_01_forest_F",0,0,{2.63184,-1.9314,0.175276},{-0.774949,0.363466,0.517056},{0.469766,-0.21605,0.855945}}
    };
};
class Comp_SandbagBarr_Kornet: DefaultStaticWeaponHAT {
    allowElevatedConstruction = 1;
    displayName = "9M133-Kornet (sandbag barricade)";
    scope = 2;
    restrictionClasses[] = {"RU_ATGMS"};
    minPlayerCount = 15;
    composition[] =
    {
        {"Land_SandbagBarricade_01_half_F",-1,0,{0,0,-0.287029},181.09,{0,0,1}},
        {"rhs_Kornet_9M133_2_vmf",0,0,{0.112661,0.79151,0.145576},0,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{1.75388,0.407675,0.402853},151.767,{0,0,1}},
        {"Land_SandbagBarricade_01_F",0,0,{-1.73441,0.384502,0.561415},207.779,{0,0,1}}
    };
};
class Comp_Sandbag_TOW: DefaultStaticWeaponHAT {
    allowElevatedConstruction = 1;
    displayName = "BGM-71 TOW (sandbag nest)";
    scope = 2;
    restrictionClasses[] = {"US_ATGMS"};
    minPlayerCount = 15;
    composition[] =
    {
        {"Land_BagFence_Short_F",-1,0,{0,0,-0.0593286},178.019,{0,0,1}},
        {"RHS_TOW_TriPod_WD",0,0,{-0.205772,2.63716,0.868855},359.819,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{2.49448,2.21377,0.0329061},87.1209,{0,0,1}},
        {"Land_BagFence_Short_F",0,0,{-2.80265,2.49572,0.0164251},268.09,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.334291,2.00264,-0.638386},0,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{-0.687229,2.0989,-0.636386},0,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{-0.677283,2.38905,-0.632386},0,{0,0,1}},
        {"Land_Pallets_stack_F",0,1,{0.344236,2.29279,-0.633387},0,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-2.1003,4.27092,0.0213265},316.658,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.78402,3.87116,0.0213265},45.0543,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{-2.00324,0.6905,0.0213265},219.495,{0,0,1}},
        {"Land_BagFence_Round_F",0,0,{1.86025,0.568784,0.0213265},136.046,{0,0,1}}
    };
};
class Comp_SandbagBarr_TOW: DefaultStaticWeaponHAT {
    displayName = "BGM-71 TOW (dugout)";
    scope = 2;
    restrictionClasses[] = {"US_ATGMS"};
    minPlayerCount = 15;
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,0.237502},180,{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{1.74072,-1.33057,1.54348},{-0.8936,0.448864,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.876953,1.1936,0.942501},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.840332,1.50415,1.31115},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.879395,1.32959,1.58515},{0.0203209,-0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.834961,1.46118,1.26789},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-1.58936,-1.29663,1.50277},{-0.875798,-0.482678,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.79834,1.15088,0.899241},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.796875,1.37183,1.6285},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{2.63232,-1.93091,-0.0582671},{-0.774949,0.363466,0.517056},{0.469766,-0.21605,0.855945}},
        {"Land_Trench_01_forest_F",0,0,{-2.24414,-2.2356,-0.00349474},{0.743905,0.423384,0.517061},{-0.447464,-0.259111,0.855942}},
        {"Land_Trench_01_forest_F",0,0,{0.211426,-3.75,-0.0179939},{-0.0109917,0.855875,0.517065},{0.0105075,-0.516969,0.85594}},
        {"RHS_TOW_TriPod_WD",0,0,{0.0112305,-0.787598,2.30367},{0.103175,-0.994663,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.166504,-2.46484,1.49475},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.20459,-2.36597,0.923019},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{3.84619,0.888184,0.449244},{0.985121,0.171859,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{-3.73584,1.27368,0.484839},{-0.977466,0.211095,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.586914,-0.981689,1.2118},{-0.0203074,0.999123,0.0366108},{0.000743969,-0.0366033,0.99933}},
        {"Land_Plank_01_4m_F",0,0,{0.831055,-1.00806,1.20033},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.171387,-0.97998,1.22129},{-0.020321,0.999793,0},{0,0,1}}
    };
};

// -- Mortars

class WP_M252_pit: DefaultStaticWeaponMortar {
    displayName = "M252 81mm mortar (pit)";
    scope = 2;
    restrictionClasses[] = {"US_RHS_MORTAR"};
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,-0.121288},180,{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.796387,1.37183,1.6285},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.840332,1.50415,1.31115},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.834961,1.46118,1.26789},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{1.74609,-1.38403,1.90652},{-0.8936,0.448864,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-1.58447,-1.35034,1.86581},{-0.875798,-0.482678,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.876465,1.1936,1.33522},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.879395,1.32935,1.58515},{0.0203209,-0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.79834,1.15088,1.29196},{-0.020321,0.999793,0},{0,0,1}},
        {"RHS_M252_USMC_WD",0,0,{0.081543,-0.469971,2.43236},{0.0203209,-0.999793,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.211914,-3.75024,0.157728},{-0.0109917,0.855875,0.517065},{0.0105075,-0.516969,0.85594}},
        {"Land_Trench_01_forest_F",0,0,{2.63232,-1.93091,0.334455},{-0.774949,0.363466,0.517056},{0.469766,-0.21605,0.855945}},
        {"Land_Trench_01_forest_F",0,0,{-2.24365,-2.2356,0.389227},{0.743905,0.423384,0.517061},{-0.447464,-0.259111,0.855942}},
        {"Land_BagFence_01_end_green_F",0,0,{0.473145,-0.548096,1.16009},{0.558419,0.829559,0},{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{-0.145996,-0.537109,1.12148},{0.652881,-0.75746,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.166504,-2.46484,1.88747},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.20459,-2.36597,1.31574},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{-14.0825,0.321289,0.257051},{0.985121,0.171859,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{3.84668,0.888184,0.449244},{0.985121,0.171859,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{-3.73633,1.27344,0.484839},{-0.977466,0.211095,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.586426,-0.981689,1.2118},{-0.0203074,0.999123,0.0366108},{0.000743969,-0.0366033,0.99933}},
        {"Land_Plank_01_4m_F",0,0,{0.830566,-1.00806,1.20033},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.171387,-0.97998,1.22129},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{13.4902,0.787598,0.421046},{-0.977466,0.211095,0},{0,0,1}}
    };
};
class WP_2B14_pit: DefaultStaticWeaponMortar {
    displayName = "2B14 Podnos (mortar pit)";
    scope = 2;
    restrictionClasses[] = {"INS_MORTAR", "RU_MORTAR"};
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,-0.185082},180,{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-14.5938,-1.44507,0.398249},{-0.774949,0.363466,0.517056},{0.469766,-0.21605,0.855945}},
        {"Land_WoodenRamp",0,0,{-13.3794,1.37402,0.513038},{0.985121,0.171859,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.796875,1.37158,1.6285},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.878906,1.32935,1.58515},{0.0203209,-0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.840332,1.50391,1.31115},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.797852,1.15063,1.29196},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.876953,1.1936,1.33522},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{1.7334,-1.36987,1.9194},{-0.8936,0.448864,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-1.59766,-1.33594,1.87869},{-0.875798,-0.482678,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.834473,1.46118,1.26789},{-0.020321,0.999793,0},{0,0,1}},
        {"rhsgref_ins_2b14",0,0,{0.20752,-0.488525,1.99268},{0.0203209,-0.999792,-0.0017975},{0.00699859,-0.00165558,0.999974}},
        {"Land_Trench_01_forest_F",0,0,{0.211426,-3.75049,0.157729},{-0.0109917,0.855875,0.517065},{0.0105075,-0.516969,0.85594}},
        {"Land_Trench_01_forest_F",0,0,{-2.24414,-2.23584,0.389227},{0.743905,0.423384,0.517061},{-0.447464,-0.259111,0.855942}},
        {"Land_Trench_01_forest_F",0,0,{2.63232,-1.93091,0.334456},{-0.774949,0.363466,0.517056},{0.469766,-0.21605,0.855945}},
        {"Land_BagFence_01_end_green_F",0,0,{0.472656,-0.54834,1.16009},{0.558419,0.829559,0},{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{-0.145996,-0.537354,1.12148},{0.652881,-0.75746,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.20459,-2.36597,1.31574},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.166504,-2.46484,1.88747},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{-3.73584,1.27344,0.48484},{-0.977466,0.211095,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{3.84717,0.887695,0.449245},{0.985121,0.171859,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.585938,-0.981934,1.2118},{-0.0203074,0.999123,0.0366108},{0.000743969,-0.0366033,0.99933}},
        {"Land_Plank_01_4m_F",0,0,{0.831055,-1.0083,1.20033},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.171387,-0.97998,1.22129},{-0.020321,0.999793,0},{0,0,1}}
    };
};
