#include "..\macros.hpp"

class comp_DSHKM_armored_NVA: DefaultStaticWeaponSimple {
    displayName = "Dshkm HMG (armored)";
    cost = 100;
    constructionTime = 20;
    restrictionClasses[] = {"NVA"};
    composition[] =
    {
        {"Land_BagFence_01_end_green_F",-1,0,{0,0,-0.425147},180.072,{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{-1.25977,0.0415039,0.044724},{0.00125584,-0.999999,0},{0,0,1}},
        {"uns_dshk_armoured_NVA",0,0,{-0.708496,0.739502,1.35619},{0.00125584,-0.999999,0},{0,0,1}},
        {"Pallet",0,0,{-0.660156,0.636719,-0.0137959},{0.00125584,-0.999999,0},{0,0,1}}
    };
};

class comp_DSHKM_NVA: DefaultStaticWeaponSimple {
    displayName = "Dshkm HMG";
    cost = 100;
    constructionTime = 20;
    restrictionClasses[] = {"NVA"};
    composition[] =
    {
        {"Land_BagFence_01_short_green_F",-1,0,{0,0,-0.335168},0,{0,0,1}},
        {"uns_dshk_low_NVA",0,0,{0.362305,-1.27563,1.24326},{0,1,0},{0,0,1}},
        {"Pallet",0,0,{-0.0688477,-0.618652,-0.132833},{0,1,0},{0,0,1}},
    };
};

class comp_MK18_GL: DefaultStaticWeaponSimple {
    displayName = "Mk 18 Grenade launcher";
    restrictionClasses[] = {"US_UNS"};
    scope = 0; // -- once again Dimensions in class uns_US_MK18_low should be an array of size 2.
    composition[] =
    {
        {"Land_BagFence_01_round_green_F",-1,0,{0,0,-0.253557},176.643,{0,0,1}},
        {"uns_US_MK18_low",0,0,{-0.0498047,0.188965,-0.101411},{-0.0516227,-0.998667,0},{0,0,1}},
        {"Pallet",0,0,{-0.0239258,0.467773,-0.177426},{-0.058559,-0.998284,0},{0,0,1}}
    };
};

class comp_M2_HMG_HIGH: DefaultStaticWeaponSimple {
    displayName = "M2 .50 cal (high)";
    restrictionClasses[] = {"US_UNS"};
    composition[] =
    {
        {"Land_BagFence_01_short_green_F",-1,0,{0,0,-0.61659},0,{0,0,1}},
        {"uns_m2_high",0,0,{-0.193359,-0.498291,0.222604},{0,1,0},{0,0,1}},
        {"Pallet",0,0,{-0.174316,-0.506836,0.149835},{0,1,0},{0,0,1}}
    };
};

class comp_M2_HMG_LOW: DefaultStaticWeaponSimple {
    displayName = "M2 .50 cal (low)";
    restrictionClasses[] = {"US_UNS"};
    composition[] =
    {
        {"Land_BagFence_01_short_green_F",-1,0,{0,0,-0.516784},0,{0,0,1}},
        {"uns_m2_low",0,0,{-0.0869141,-0.574951,0.128798},{0,1,0},{0,0,1}},
        {"Pallet",0,0,{-0.0830078,-0.569092,0.0417833},{0,1,0},{0,0,1}}
    };
};

class comp_DHKM_HMG_HIGH: DefaultStaticWeaponSimple {
    displayName = "Dhkm HMG (low)";
    cost = 100;
    constructionTime = 20;
    restrictionClasses[] = {"NVA"};
    composition[] =
    {
        {"Land_BagFence_01_short_green_F",-1,0,{0,0,-0.335168},0,{0,0,1}},
        {"uns_dshk_low_NVA",0,0,{0.362305,-1.27563,1.24326},{0,1,0},{0,0,1}},
        {"Pallet",0,0,{-0.0688477,-0.618652,-0.132833},{0,1,0},{0,0,1}}
    };
};

class comp_DHKM_HMG_LOW: DefaultStaticWeaponSimple {
    displayName = "Dhkm HMG (high)";
    cost = 100;
    constructionTime = 20;
    restrictionClasses[] = {"NVA"};
    composition[] =
    {
        {"Land_BagFence_01_short_green_F",-1,0,{0,0,-0.552714},0,{0,0,1}},
        {"uns_dshk_high_NVA",0,0,{0.160156,-1.13062,1.76926},{0,1,0},{0,0,1}},
        {"Pallet",0,0,{-0.118652,-0.576416,0.0602455},{0,1,0},{0,0,1}}
    };
};

class comp_DHKM_HMG_ARMORED: DefaultStaticWeaponSimple {
    displayName = "Dhkm HMG (armored)";
    cost = 120;
    constructionTime = 20;
    restrictionClasses[] = {"NVA"};
    composition[] =
    {
        {"Land_BagFence_01_end_green_F",-1,0,{0,0,-0.249423},0,{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{-1.25977,0.0429688,-0.044724},{-0.00125584,-0.999999,0},{0,0,1}},
        {"uns_dshk_armoured_NVA",0,0,{-0.552246,-0.697266,1.31146},{0,1,0},{0,0,1}},
        {"Pallet",0,0,{-0.600586,-0.594482,-0.0585198},{0,1,0},{0,0,1}}
    };
};

class comp_m2_watchtower: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    displayName = "M2 .50 cal watchtower";
    scope = 2;
    restrictionClasses[] = {"US_UNS"};
    composition[] =
    {
        {"Land_Wood_Tower",-1,0,{0,0,0.10316},179.695,{0,0,1}},
        {"uns_m2_high",0,0,{0.130859,-0.986328,1.06481},{-0.127785,-0.991802,-7.42489e-006},{-0.000158323,1.29123e-005,1}}
    };

};

class comp_DSHKM_watchtower: DefaultStaticWeaponBunker {
    allowElevatedConstruction = 1;
    displayName = "Dhkm watchtower";
    scope = 2;
    restrictionClasses[] = {"NVA"};
    composition[] =
    {
        {"Land_Wood_Tower",-1,0,{0,0,0},179.695,{0,0,1}},
        {"uns_dshk_high_NVA",0,0,{0.0615234,-0.647217,2.75221},{-0.00216697,-0.999992,-0.00350757},{0.00154475,-0.00351093,0.999993}}
    };
};

// -- AAA

class Comp_Sandbag_S60_dugout: DefaultStaticWeapon {
    displayName = "S-60 57mm AA dugout";
    cost = 300;
    constructionTime = 30;
    scope = 2;
    minPlayerCount = 15;
    restrictionClasses[] = {"NVA_AA"};
    allowElevatedConstruction = 0;
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,-0.26718},1.16439,{0,0,1}},
        {"uns_S60_NVA",0,0,{-0.0751953,1.052,1.12709},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.168945,2.72363,-0.0505724},{0,1,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-0.0307617,-3.01733,-0.231633},{0.000660846,0.967486,0.252923},{0.0051407,-0.252923,0.967473}},
        {"LAND_t_sb_5_half",0,0,{-12.7476,0.180664,0.61608},{0.999666,0.0258534,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{-4.98145,2.00732,0.510466},{-0.999619,-0.0275964,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{4.70605,2.42944,0.510564},{0.999912,0.0132793,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{-0.23291,7.03662,0.522838},{-0.0555467,0.998456,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{4.66016,-2.26831,0.314751},{0.999575,-0.0291642,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{-4.83252,-2.6311,0.272603},{-0.999619,-0.0275964,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{15.689,-0.493164,0.568954},{-0.999619,-0.0275964,0},{0,0,1}}
    };
};
class Comp_Sandbag_ZU23_dugout: DefaultStaticWeapon {
    displayName = "ZU23-2 dugout";
    cost = 250;
    constructionTime = 30;
    scope = 2;
    minPlayerCount = 15;
    restrictionClasses[] = {"NVA_AA"};
    allowElevatedConstruction = 0;
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,0.183403},1.16439,{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{0.125488,-2.48096,-0.288867},{0.000660846,0.967486,0.252923},{0.0051407,-0.252923,0.967473}},
        {"LAND_t_sb_5_half",0,0,{-0.18457,4.15723,0.129747},{-0.0506927,0.998714,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{-4.8125,-0.716797,0.122688},{-0.999619,-0.0275964,0},{0,0,1}},
        {"uns_ZU23_NVA",0,0,{0.0273438,0.35376,1.0434},{-0.020321,0.999793,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{4.83301,-0.394287,0.165498},{0.999666,0.0258534,0},{0,0,1}},
        {"LAND_t_sb_5_half",0,0,{12.748,-3.20605,-0.177979},{-0.999619,-0.0275964,0},{0,0,1}}
    };
};

class WP_twinDshkm_bunker: DefaultStaticWeaponAA {
    displayName = "Dshkm bunker";
    cost = 150;
    constructionTime = 20;
    scope = 2;
    restrictionClasses[] = {"NVA"};
    composition[] =
    {
        {"uns_dshk_bunker_open_NVA",-1,0,{0,0,0.0488415},359.996,{-0.00155263,0.00350693,0.999993}},
        {"LAND_uns_eastbunker2",0,0,{0.739258,0.57373,-1.29396},{-6.78579e-005,-0.999994,-0.00350693},{0.0015524,-0.00350703,0.999993}},
        {"uns_bunker_plants1",0,0,{0.00341797,-0.0268555,-1.25327},{-6.80963e-005,-0.999994,-0.00350693},{0.0015524,-0.00350703,0.999993}}
    };
};
// -- AT
class comp_M40_SPG: DefaultStaticWeaponSimple {
    displayName = "M40 106mm recoilless rifle";
    cost = 400;
    restrictionClasses[] = {"US_UNS_HAT"};
    composition[] =
    {
        {"Land_BagFence_01_round_green_F",-1,0,{0,0,-0.294897},183.271,{0,0,1}},
        {"uns_M40_106mm_US",0,0,{-0.0224609,0.262695,-0.262494},{0.0570522,-0.998371,0},{0,0,1}},
        {"Pallet",0,0,{0.102051,0.523682,-0.109514},{0.0570522,-0.998371,0},{0,0,1}}
    };
};
class comp_SPG9_SPG: DefaultStaticWeaponSimple {
    displayName = "SPG-9 recoilless rifle";
    cost = 200;
    constructionTime = 20;
    restrictionClasses[] = {"NVA_HAT"};
    composition[] =
    {
        {"Land_BagFence_01_round_green_F",-1,0,{0,0,-0.376706},183.271,{0,0,1}},
        {"uns_SPG9_73mm_NVA",0,0,{-0.0571289,0.318359,-0.0331769},{0.0570522,-0.998371,0},{0,0,1}},
        {"Pallet",0,0,{0.101563,0.523926,-0.109514},{0.0570522,-0.998371,0},{0,0,1}}
    };
};

// -- Mortars

class WP_M1_pit_UNS: DefaultStaticWeaponMortar {
    displayName = "M1 81mm infantry mortar (pit)";
    scope = 0;
    restrictionClasses[] = {"US_UNS_MORTAR"};
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,-0.130239},1.16439,{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{1.74609,-1.38428,1.90652},{-0.8936,0.448864,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.79834,1.15088,1.29196},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.796387,1.37183,1.6285},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.834961,1.46118,1.26789},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.839844,1.50415,1.31115},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.879395,1.32935,1.58515},{0.0203209,-0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.876465,1.1936,1.33522},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-1.58447,-1.35034,1.86581},{-0.875798,-0.482678,0},{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{0.472168,-0.548096,1.16009},{0.558419,0.829559,0},{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{-0.146484,-0.537354,1.12148},{0.652881,-0.75746,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.166504,-2.46484,1.88747},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.20459,-2.36597,1.31574},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{-3.73633,1.27368,0.484839},{-0.977466,0.211095,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{3.84668,0.887939,0.449244},{0.985121,0.171859,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.586426,-0.981689,1.2118},{-0.0203074,0.999123,0.0366108},{0.000743969,-0.0366033,0.99933}},
        {"Land_Plank_01_4m_F",0,0,{0.830566,-1.00806,1.20033},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.171387,-0.97998,1.22129},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{2.63232,-1.93091,0.334456},{-0.774949,0.363466,0.517056},{0.469766,-0.21605,0.855945}},
        {"Land_Trench_01_forest_F",0,0,{0.211426,-3.75024,0.157729},{-0.0109917,0.855875,0.517065},{0.0105075,-0.516969,0.85594}},
        {"Land_Trench_01_forest_F",0,0,{-2.24414,-2.2356,0.389227},{0.743905,0.423384,0.517061},{-0.447464,-0.259111,0.855942}},
        {"uns_M1_81mm_mortar",0,0,{0.131836,-0.458984,1.98211},{-0.00425195,-0.999991,0},{0,0,1}}
    };
};
class WP_m1941_pit: DefaultStaticWeaponMortar {
    displayName = "M1941 81mm infantry mortar (pit)";
    scope = 0;
    restrictionClasses[] = {"NVA_MORTAR"};
    composition[] =
    {
        {"Land_Trench_01_forest_F",-1,0,{0,0,-0.194032},1.16439,{0,0,1}},
        {"Land_WoodenRamp",0,0,{-3.73584,1.27344,0.48484},{-0.977466,0.211095,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.796875,1.37183,1.6285},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.876953,1.1936,1.33522},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.834961,1.46094,1.26789},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.878906,1.32935,1.58515},{0.0203209,-0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-0.79834,1.15088,1.29196},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{1.73291,-1.36987,1.9194},{-0.8936,0.448864,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{0.840332,1.50391,1.31115},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_BagFence_01_short_green_F",0,0,{-1.59766,-1.33594,1.87869},{-0.875798,-0.482678,0},{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{-0.146484,-0.537354,1.12148},{0.652881,-0.75746,0},{0,0,1}},
        {"Land_BagFence_01_end_green_F",0,0,{0.472656,-0.54834,1.16009},{0.558419,0.829559,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.166504,-2.46484,1.88747},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_BagFence_01_round_green_F",0,0,{0.20459,-2.36597,1.31574},{-0.0417353,0.999129,0},{0,0,1}},
        {"Land_WoodenRamp",0,0,{3.84668,0.887939,0.449245},{0.985121,0.171859,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{-0.586426,-0.981689,1.2118},{-0.0203074,0.999123,0.0366108},{0.000743969,-0.0366033,0.99933}},
        {"Land_Plank_01_4m_F",0,0,{0.171875,-0.980225,1.22143},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Plank_01_4m_F",0,0,{0.831055,-1.00806,1.20033},{-0.020321,0.999793,0},{0,0,1}},
        {"Land_Trench_01_forest_F",0,0,{-2.24414,-2.2356,0.389228},{0.743905,0.423384,0.517061},{-0.447464,-0.259111,0.855942}},
        {"Land_Trench_01_forest_F",0,0,{0.211426,-3.75049,0.157729},{-0.0109917,0.855875,0.517065},{0.0105075,-0.516969,0.85594}},
        {"Land_Trench_01_forest_F",0,0,{2.63232,-1.93115,0.334456},{-0.774949,0.363466,0.517056},{0.469766,-0.21605,0.855945}},
        {"uns_m1941_82mm_mortarNVA",0,0,{0.182129,-0.415039,1.99409},{0.0763602,-0.99708,0},{0,0,1}}
    };
};
