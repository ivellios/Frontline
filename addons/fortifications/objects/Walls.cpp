#include "..\macros.hpp"

class DefaultObstacle: DefaultFortification {
    allowElevatedConstruction = 1;
    constructionTime = 15;
    scope = 2;
    cost = 20;
    composition[] = {};
};
class DefaultCover: DefaultFortification {
    allowElevatedConstruction = 1;
};

class Walls: DefaultFortification {
    category = 4;
    cost = 20;
    displayName = "Walls/Obstacles";
    scope = 2;
    composition[] = {};

    class Comp_sandbag_shoothole_v1: DefaultFortification {
        displayName = "sandbag shoothole";
        cost = 75;
        allowElevatedConstruction = 1;
        constructionTime = 15;
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.164},178.193,{0,0,1}},
            {"Land_SandbagBarricade_01_half_F",0,0,{2.11443,1.95005,-0.432478},126.15,{0,0,1}},
            {"Land_SandbagBarricade_01_half_F",0,0,{2.23217,2.11235,-1.32465},126.15,{0,0,1}},
            {"Land_SandbagBarricade_01_half_F",0,0,{-2.38532,2.00036,-0.432478},236.359,{0,0,1}},
            {"Land_SandbagBarricade_01_half_F",0,0,{-2.44291,2.234,-1.37861},236.359,{0,0,1}},
            {"Land_SandbagBarricade_01_half_F",0,0,{-1.32075,0.632582,-1.3129},227.625,{0,0,1}},
            {"Land_SandbagBarricade_01_half_F",0,0,{0.0287607,-0.0925673,-1.26842},178.079,{0,0,1}},
            {"Land_SandbagBarricade_01_half_F",0,0,{1.23537,0.559253,-1.2991},121.042,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{-1.24184,0.526731,-0.346351},226.106,{0,0,1}},
            {"Land_SandbagBarricade_01_F",0,0,{1.17032,0.550091,-0.286646},127.334,{0,0,1}}
        };
    };

    class LAND_t_sb_5_half: DefaultFortification {
        cost = 30;
		constructionTime = 10;
        displayName = "Trench 5m (half)";
        restrictionClasses[] = {"UNSUNG"};
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"LAND_t_sb_5_half",-1,0,{0,0,-9.53674e-007},0,{0,0,1}}
        };
    };
    class LAND_t_sb_5: DefaultFortification {
        cost = 50;
		constructionTime = 10;
        displayName = "Trench 5m";
        restrictionClasses[] = {"UNSUNG"};
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"LAND_t_sb_5",-1,0,{0,0,0},180,{0,0,1}}
        };
    };
    class LAND_CSJ_punjiWall: DefaultFortification {
        cost = 5;
        constructionTime = 10;
        displayName = "Punji wall";
        restrictionClasses[] = {"NVA"};
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"LAND_CSJ_punjiWall",-1,0,{0,0,0},90,{0,0,1}}
        };
    };


    class Wall_dirtwall: DefaultFortification {
        displayName = "Dirt wall";
        scope = 2;
        cost = 20;
        allowElevatedConstruction = 0;
        constructionTime = 10;
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_BagFence_01_short_green_F",-1,0,{0,1,0.606966},180,{0,0,1}},
            {"Land_BagFence_01_short_green_F",0,0,{-1.84619,0.052002,0.0251708},{8.74228e-008,-1,0},{0,0,1}},
            {"Land_BagFence_01_long_green_F",0,0,{-0.890625,-0.266602,-0.0994611},{8.74228e-008,-1,0},{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{-2.25439,-0.286377,-0.0379944},{0,1,0},{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{0.63623,-0.220459,-0.117979},{8.74228e-008,-1,0},{0,0,1}},
            {"Land_Trench_01_grass_F",0,0,{-0.841797,-0.786133,-1.72543},{0.000190284,0.703511,0.710685},{6.213e-008,-0.710685,0.703511}},
            {"Land_Trench_01_grass_F",0,0,{-0.775391,0.703125,-1.6694},{7.2299e-008,-0.827004,0.562195},{-4.91487e-008,0.562195,0.827004}}
        };
    };

    class Wall_Trench_half: DefaultFortification {
        displayName = "Trench wall camouflaged (half)";
        scope = 2;
        cost = 20;
        allowElevatedConstruction = 0;
        constructionTime = 10;
        restrictionClasses[] = {"CUP"};
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_BagFence_01_long_green_F",-1,0,{0,0,0.249697},181.2121,{-0.106495,0,0.994313}},
            {"CUP_Krovi",0,0,{-0.178955,-1.42773,-0.0770874},{-0.0209138,0.994093,-0.106495},{-0.00223995,0.106471,0.994313}},
            {"Land_Trench_01_forest_F",0,0,{-0.0090332,-1.44482,-1.17218},{-0.0192318,0.914146,0.40493},{0.00851707,-0.40484,0.914348}}
        };
    };
    class Wall_Trench_half_blank: DefaultFortification {
        displayName = "Trench wall (half)";
        scope = 2;
        cost = 20;
        allowElevatedConstruction = 0;
        constructionTime = 10;
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_BagFence_01_long_green_F",-1,0,{0,0,0.249697},181.2121,{-0.106495,0,0.994313}},
            {"Land_Trench_01_forest_F",0,0,{-0.0090332,-1.44482,-1.17218},{-0.0192318,0.914146,0.40493},{0.00851707,-0.40484,0.914348}}
        };
    };
    class Foxhole: DefaultFortification {
        displayName = "Foxhole (camouflaged)";
        scope = 2;
        cost = 50;
        allowElevatedConstruction = 0;
        constructionTime = 15;
        restrictionClasses[] = {"CUP"};
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_BagFence_01_long_green_F",-1,0,{0,0,0.0627174},0.925,{0,0,1}},
            {"CUP_Krovi",0,0,{0.034668,1.22314,-0.170213},{0.0161512,-0.99987,0},{0,0,1}},
            {"CUP_Krovi",0,0,{2.78613,-1.06055,-0.236639},{0.99987,0.0161512,0},{0,0,1}},
            {"CUP_Krovi",0,0,{-2.80518,-1.46191,-0.244401},{0.99987,0.0161512,0},{0,0,1}},
            {"Land_Trench_01_forest_F",0,0,{-2.72803,-1.38086,-1.33671},{0.865912,0.0139874,0.5},{-0.499935,-0.00807561,0.866025}},
            {"Land_Trench_01_forest_F",0,0,{2.67651,-1.35938,-1.33671},{-0.865912,-0.0139873,0.5},{0.499935,0.00807561,0.866025}},
            {"Land_Trench_01_forest_F",0,0,{-0.0932617,-4.12256,-1.33671},{-0.0139873,0.865912,0.5},{0.00807561,-0.499935,0.866025}},
            {"Land_Trench_01_forest_F",0,0,{-0.0834961,1.25244,-1.33671},{0.0139873,-0.865912,0.5},{-0.00807561,0.499935,0.866025}},
            {"Land_BagFence_01_long_green_F",0,0,{-1.49097,-1.55176,-0.0627174},{0.99987,0.0161512,0},{0,0,1}},
            //{"Land_BagFence_01_long_green_F",0,0,{-0.010498,-2.86914,-0.0627174},{0,1,0},{0,0,1}},
            {"Land_BagFence_01_long_green_F",0,0,{1.39429,-1.52588,-0.0627174},{-0.99987,-0.0161511,0},{0,0,1}},
            {"Land_BagFence_01_round_green_F",0,0,{-0.0788574,0.234375,0.173538},{0.0172819,-0.999851,0},{0,0,1}}
        };
    };
    class Foxhole_blank: DefaultFortification {
        displayName = "Foxhole";
        scope = 2;
        cost = 50;
        allowElevatedConstruction = 0;
        constructionTime = 15;
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_BagFence_01_long_green_F",-1,0,{0,0,0.0627174},0.925,{0,0,1}},
            {"Land_Trench_01_forest_F",0,0,{-2.72803,-1.38086,-1.33671},{0.865912,0.0139874,0.5},{-0.499935,-0.00807561,0.866025}},
            {"Land_Trench_01_forest_F",0,0,{2.67651,-1.35938,-1.33671},{-0.865912,-0.0139873,0.5},{0.499935,0.00807561,0.866025}},
            {"Land_Trench_01_forest_F",0,0,{-0.0932617,-4.12256,-1.33671},{-0.0139873,0.865912,0.5},{0.00807561,-0.499935,0.866025}},
            {"Land_Trench_01_forest_F",0,0,{-0.0834961,1.25244,-1.33671},{0.0139873,-0.865912,0.5},{-0.00807561,0.499935,0.866025}},
            {"Land_BagFence_01_long_green_F",0,0,{-1.49097,-1.55176,-0.0627174},{0.99987,0.0161512,0},{0,0,1}},
            //{"Land_BagFence_01_long_green_F",0,0,{-0.010498,-2.86914,-0.0627174},{0,1,0},{0,0,1}},
            {"Land_BagFence_01_long_green_F",0,0,{1.39429,-1.52588,-0.0627174},{-0.99987,-0.0161511,0},{0,0,1}},
            {"Land_BagFence_01_round_green_F",0,0,{-0.0788574,0.234375,0.173538},{0.0172819,-0.999851,0},{0,0,1}}
        };
    };

    class Wall_Trench: DefaultFortification {
        displayName = "Trench";
        scope = 2;
        cost = 30;
        allowElevatedConstruction = 0;
        constructionTime = 20;
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_BagFence_01_long_green_F",-1,0,{-3,0,0.5},180,{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{-1.62061,-0.0812988,-0.11571},{-9.62417e-006,-0.978037,0.208432},{0,0.208432,0.978037}},
            {"Land_BagFence_01_end_green_F",0,0,{1.51074,-0.150391,-0.0400338},{0,0.958572,-0.284849},{0,0.284849,0.958572}},
            {"Land_Plank_01_4m_F",0,0,{-0.112305,-2.33374,-0.998645},{1,-4.37114e-008,0},{0,0,1}},
            {"Land_Plank_01_4m_F",0,0,{-0.151367,-1.75269,-0.999928},{1,-4.37114e-008,0},{0,0,1}},
            {"Land_Plank_01_4m_F",0,0,{-0.0932617,-3.1001,-0.544208},{1,-4.37114e-008,0},{0,0,1}},
            {"Land_Trench_01_forest_F",0,0,{-0.0869141,-3.03345,-2.20088},{0,1,0},{0,0,1}},
            {"Land_Trench_01_forest_F",0,0,{0.0126953,-5.1084,-1.04501},{0,0.935066,0.354473},{0,-0.354473,0.935066}},
            {"Land_Trench_01_forest_F",0,0,{0.0180664,-1.1582,-2.51434},{0,1,0},{0,0,1}},
            {"Land_Trench_01_forest_F",0,0,{0.233887,-6.63843,-2.14657},{0,0.770428,0.637527},{0,-0.637527,0.770428}},
            {"Land_Trench_01_forest_F",0,0,{-0.0463867,-4.38403,-1.73918},{0,1,0},{0,0,1}},
            {"Land_Trench_01_forest_F",0,0,{-0.0605469,0.935059,-1.59399},{-3.10818e-005,-0.878732,0.477316},{0,0.477316,0.878732}},
            {"Land_BagFence_01_long_green_F",0,0,{-0.059082,0.242432,-0.209105},{0,1,0},{0,0,1}},
            {"Land_BagFence_01_long_green_F",0,0,{0.0180664,-0.204102,-0.165672},{0,0.990238,-0.13939},{0,0.13939,0.990238}},
            {"Land_BagFence_01_short_green_F",0,0,{-0.831055,-3.75781,0.449827},{-8.74228e-008,-1,0},{0,0,1}},
            {"Land_BagFence_01_short_green_F",0,0,{0.922852,-4.11084,0.367002},{0,0.992401,0.123044},{0,-0.123044,0.992401}},
            {"Land_BagFence_01_short_green_F",0,0,{-0.942383,-4.0813,0.354767},{-8.74228e-008,-0.979782,-0.200069},{0,-0.200069,0.979782}},
            {"Land_BagFence_01_short_green_F",0,0,{0.923828,-3.86157,0.449827},{0,1,0},{0,0,1}},
            {"Land_TrenchFrame_01_F",0,0,{2.25,-5.2627,-1.32669},{-0.990667,-0.095493,-0.0972672},{-0.0254484,-0.571464,0.820233}},
            {"Land_TrenchFrame_01_F",0,0,{-2.13965,-5.48804,-1.22558},{0.999595,0.0149701,-0.0241984},{0.0284222,-0.484708,0.874214}},
            {"Land_WoodenRamp",0,0,{-0.140625,-2.10449,-0.981425},{-7.78119e-005,-0.9871,-0.160107},{0,-0.160107,0.9871}},
            {"Land_WoodenRamp",0,0,{-3.79785,-1.95483,-2.08856},{-0.9793,1.19249e-008,-0.202413},{-0.202413,0,0.9793}},
            {"Land_WoodenRamp",0,0,{3.41211,-2.22729,-2.32868},{0.942439,-0.038328,-0.332175},{0.33242,0,0.943132}}
        };
    };


    class Land_HBarrier_1_F: DefaultFortification {
        displayName = "Hesco block";
        restrictionClasses[] = {"HESCOS"};
        cost = 10;
        constructionTime = 10;
        allowElevatedConstruction = 0;
    };
    class Land_HBarrier_3_F: DefaultFortification {
        displayName = "Triple hesco block";
        restrictionClasses[] = {"HESCOS"};
        cost = 30;
        constructionTime = 20;
        allowElevatedConstruction = 0;
    };
    class Land_HBarrier_01_line_3_green_F: Land_HBarrier_3_F {
		scope = 0;
        displayName = "Triple hesco block (green)";
        allowElevatedConstruction = 0;
    };
    class Land_HBarrier_Big_F: DefaultFortification {
		scope = 0;
        displayName = "Big hesco block";
        restrictionClasses[] = {"HESCOS"};
        cost = 50;
        allowElevatedConstruction = 0;
    };
    class Land_HBarrier_01_big_4_green_F: Land_HBarrier_Big_F {
		scope = 0;
        displayName = "Big hesco block (green)";
        allowElevatedConstruction = 0;
        cost = 50;
    };
    class Land_HBarrier_5_F: DefaultFortification {
        displayName = "Hesco wall";
        restrictionClasses[] = {"HESCOS"};
        cost = 50;
        constructionTime = 25;
        allowElevatedConstruction = 0;
    };
    class Land_HBarrier_01_line_5_green_F: Land_HBarrier_5_F {
		scope = 0;
        displayName = "Hesco wall (green)";
        allowElevatedConstruction = 0;
    };

    class Land_HBarrierWall_corner_F: DefaultFortification {
        displayName = "Hesco wall big, corner";
        restrictionClasses[] = {"HESCOS"};
        cost = 100;
        constructionTime = 20;
        allowElevatedConstruction = 0;
        composition[] = {
            {"Land_HBarrierWall_corner_F",-1,0,{0,0,0},0,{0,0,1}},
        };
    };
    class Land_HBarrierWall4_F:Land_HBarrierWall_corner_F {
        displayName = "Hesco wall big";
        allowElevatedConstruction = 0;
        composition[] = {
            {"Land_HBarrierWall4_F",-1,0,{0,0,0},0,{0,0,1}},
        };
    };
    class Land_HBarrier_01_wall_4_green_F:Land_HBarrierWall_corner_F {
		scope = 0;
        displayName = "Hesco wall big (green)";
        allowElevatedConstruction = 0;
        composition[] = {
            {"Land_HBarrier_01_wall_4_green_F",-1,0,{0,0,0},0,{0,0,1}},
        };
    };
    class Land_HBarrier_01_wall_corner_green_F:Land_HBarrierWall_corner_F {
		scope = 0;
        displayName = "Hesco wall big, corner (green)";
        allowElevatedConstruction = 0;
        composition[] = {
            {"Land_HBarrier_01_wall_corner_green_F",-1,0,{0,0,0},0,{0,0,1}},
        };
    };
    class Land_SandbagBarricade_01_hole_F: DefaultCover {
        cost = 50;
        constructionTime = 20;
    };
    class Land_rjw_EarthMound_Bocage1: DefaultFortification {
		scope = 0;
        restrictionClasses[] = {"IFA"};
        cost = 50;
        allowElevatedConstruction = 0;
        composition[] =
        {
            {"Land_rjw_EarthMound_Bocage1",-1,0,{0,0,0},90,{0,0,1}}
        };
    };
    class Land_rjw_EarthMound_Bocage2: DefaultFortification {
		scope = 0;
        restrictionClasses[] = {"IFA"};
        cost = 50;
        allowElevatedConstruction = 0;
        composition[] =
        {
            {"Land_rjw_EarthMound_Bocage2",-1,0,{0,0,0},90,{0,0,1}}
        };
    };
    class Land_rjw_EarthMound_Bocage3: DefaultFortification {
		scope = 0;
        restrictionClasses[] = {"IFA"};
        cost = 50;
        allowElevatedConstruction = 0;
        composition[] =
        {
            {"Land_rjw_EarthMound_Bocage3",-1,0,{0,0,0},90,{0,0,1}}
        };
    };
    class Land_rjw_EarthMound_Bocage4: DefaultFortification {
		scope = 0;
        restrictionClasses[] = {"IFA"};
        cost = 50;
        allowElevatedConstruction = 0;
        composition[] =
        {
            {"Land_rjw_EarthMound_Bocage4",-1,0,{0,0,0},90,{0,0,1}}
        };
    };
    class Land_rjw_EarthMound_Bocage5: DefaultFortification {
		scope = 0;
        restrictionClasses[] = {"IFA"};
        cost = 50;
        allowElevatedConstruction = 0;
        composition[] =
        {
            {"Land_rjw_EarthMound_Bocage5",-1,0,{0,0,0},90,{0,0,1}}
        };
    };
    class Land_rjw_EarthMound_Bocage6: DefaultFortification {
		scope = 0;
        restrictionClasses[] = {"IFA"};
        cost = 50;
        allowElevatedConstruction = 0;
        composition[] =
        {
            {"Land_rjw_EarthMound_Bocage6",-1,0,{0,0,0},90,{0,0,1}}
        };
    };

    class Land_wall_woodpile: DefaultCover {
        restrictionClasses[] = {"CUP"};
        displayName = "woodpiles with sandbags";
        cost = 20;
        allowElevatedConstruction = 0;
        listPath = "\A3\EditorPreviews_F\Data\CfgVehicles\Default\Prop.jpg";
        composition[] =
        {
            {"Land_BagFence_01_end_green_F",-1,0,{0,0,0.151304},356.29,{0,0,1}},
            {"Land_WoodPile_F",0,0,{-1.06934,0.234131,-0.261448},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{-0.0322266,-0.207275,-0.299762},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{-0.916504,0.0415039,-0.0010376},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{1.01855,-0.287842,-0.268736},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{-1.08105,-0.156494,-0.273396},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{1.03076,0.103027,-0.256787},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{-0.0200195,0.183594,-0.287813},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{-0.634277,0.0410156,-0.00575638},272.383,{0,0,1}},
            {"Land_WoodPile_F",0,0,{-1.29736,-0.0742188,-0.535078},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{0.354004,-0.298096,-0.579572},89.1939,{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{0.940918,0.0319824,0.00651741},176.931,{0,0,1}},
            {"Land_BagFence_01_end_green_F",0,0,{-0.821289,0.0808105,-0.0557718},180.251,{0,0,1}},
            {"CUP_b_salix2s",0,0,{-1.23193,0.731445,-0.750884},152.409,{0,0,1}},
            {"Land_BagFence_01_short_green_F",0,0,{0.493164,-0.00244141,-0.377067},0,{0.0628132,0.0174443,0.997873}},
            {"Land_WoodPile_F",0,0,{1.87598,-0.293457,-0.222015},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{1.88818,0.0974121,-0.210068},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{2.05518,-0.350586,-0.588015},89.1939,{0,0,1}},
            {"Land_WoodPile_F",0,0,{1.50928,0.0166016,0.0768337},89.1939,{0,0,1}},
            {"CUP_b_salix2s",0,0,{2.10547,0.657959,-0.864659},0,{0,0,1}}
        };
    };

    class Land_rjw_EarthMound_chevron: DefaultFortification {
        displayName = "Earthmound Chevron";
        restrictionClasses[] = {"IFA"};
        cost = 100;
        scope = 0;
        allowElevatedConstruction = 0;
        composition[] =
        {
            {"Land_rjw_EarthMound_Bocage3",-1,0,{0,0,0.338634},269.29,{0,0,1}},
            {"Land_rjw_EarthMound_Bocage3",0,0,{-4.00439,6.83057,-0.118108},213.952,{0,0,1}},
            {"Land_rjw_EarthMound_Bocage3",0,0,{-3.49951,-6.78369,-0.0711346},141.159,{0,0,1}}
        };
    };

    class Land_SandbagBarricade_01_F: DefaultCover {
        cost = 40;
        constructionTime = 10;
    };

    class Land_fort_bagfence_round: DefaultCover {
        restrictionClasses[] = {"CUP"};
        cost = 70;
        constructionTime = 10;
    };

    class Land_SandbagBarricade_01_half_F: DefaultCover {
        cost = 10;
        constructionTime = 5;
    };

    class Land_BagFence_Round_F: DefaultCover {
        cost = 10;
        constructionTime = 5;
        composition[] =
        {
            {"Land_BagFence_Round_F",-1,0,{0,0,0},180,{0,0,1}}
        };
    };

    class Land_BagFence_Long_F: DefaultCover {
        cost = 10;
        constructionTime = 5;
    };

    class Land_BagFence_Corner_F: DefaultCover {
		scope = 0;
		constructionTime = 5;
    };

    class Land_BagFence_End_F: DefaultCover {
		scope = 0;
		constructionTime = 5;
    };

    class Land_BagFence_Short_F: DefaultCover {
        cost = 5;
        constructionTime = 5;
    };

    class Land_BagFence_01_round_green_F: DefaultCover {
		scope = 0;
		constructionTime = 5;
        composition[] =
        {
            {"Land_BagFence_01_round_green_F",-1,0,{0,0,0},180,{0,0,1}}
        };

    };

    class Land_BagFence_01_long_green_F: DefaultCover {
		scope = 0;
		constructionTime = 5;
    };

    class Land_BagFence_01_corner_green_F: DefaultCover {
		scope = 0;
		constructionTime = 5;
    };

    class Land_BagFence_01_end_green_F: DefaultCover {
		scope = 0;
		constructionTime = 5;
    };

    class Land_BagFence_01_short_green_F: DefaultCover {
		scope = 0;
		constructionTime = 5;
    };

    class CUP_A2_fort_razorwire: DefaultObstacle {
        restrictionClasses[] = {"CUP"};
		cost = 10;
		constructionTime = 10;
	};
    class Razorwire: DefaultObstacle {
        cost = 10;
		constructionTime = 10;
        restrictionClasses[] = {"UNSUNG"};
    };

    class Hedgehog_EP1: DefaultObstacle {
        restrictionClasses[] = {"CUP"};
        cost = 5;
		constructionTime = 5;
    };
    class Wire: DefaultObstacle {
        restrictionClasses[] = {"CUP"};
		constructionTime = 10;
	};
    class Land_Obstacle_Ramp_F: DefaultObstacle {
        cost = 5;
        constructionTime = 5;
        composition[] =
        {
            {"Land_Obstacle_Ramp_F",-1,0,{0,0,0},180,{0,0,1}}
        };
    };
};
