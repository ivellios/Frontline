#include "..\macros.hpp"

class cfgFortifications {

    class DefaultFortification {
        scope = 2; // -- 0 to disable
        displayName = ""; // -- Name (defaults to displayname of class)
        listPath = ""; // -- Path to picture (defaults to editor picture)
        cost = 20; // -- amount of supplies needed at the FO to construct given object
        constructionTime = 20; // -- Duration of construction
        composition[] = {}; // --
        minPlayerCount = 0;		//minimum playercount required for deployment
        restrictionClasses[] = {};        //means they need to be unlocked in settings/squads/staticCompositions[]
        allowElevatedConstruction = 0;
    };

    class ConstructerObject: DefaultFortification {    // -- used as interaction object during construction
        scope = 0;
        composition[] =
        {
            {"Land_BagFence_Corner_F",-1,0,{0,0,0.0166225},0,{0,0,1}},
            {"Land_BagFence_Corner_F",0,0,{-0.165527,0.0605469,-0.0166225},{-0.0700401,-0.997544,0},{0,0,1}},
            {"Land_Pallet_F",0,0,{-0.0996094,0.0661621,-0.461681},{0,1,0},{0,0,1}},
            {"Land_Shovel_F",0,0,{0.153809,0.0283203,0.47576},{-0.000375636,0.9999,-0.014128},{-6.40829e-006,0.014128,0.9999}},
            {"Land_Shovel_F",0,0,{-0.236328,0.0383301,0.480477},{-0.51999,-0.84718,-0.109068},{-0.0569342,-0.0930289,0.994034}}
        };

    };

    #include "compositions\Compositions.cpp"
    #include "staticweapons\StaticWeapons.cpp"
    #include "Walls.cpp"
    #include "bunkers\bunkers.cpp"


    class Settings {
        maxTotalFOSupply = 5000;
        // maxBuildDistance = 200; replaced with EGVAR(Rolesspawns,buildRadius)
        buildPeriodTime = 7.5;
        buildOffset = 2;
    };

};
