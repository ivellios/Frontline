/*
    Function:       FRL_Fortifications_fnc_dragSupplies
    Author:         N3croo
    Description:		dragging of the supplybox
    [_supplybox, _action] call FRL_Fortifications_fnc_dragSupplies
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params ["_supplybox"];
private _weapon = "";
private _player = CLib_Player;

private _animation = switch (currentWeapon Clib_Player) do {
	case (primaryWeapon Clib_Player): { _weapon = "rifle"; "frl_drag_rfl_stp"; };
	case (handgunWeapon Clib_Player): { _weapon = "pistol"; "frl_drag_pst_stp"; };
	case (secondaryWeapon Clib_Player): { _weapon = "launcher"; "frl_drag_lnr_stp"; };
	default { _weapon = "none"; "frl_drag_non_stp" };
};

Clib_Player setVariable [QGVAR(dragEndAnim), _weapon];
["switchMove", [Clib_Player, _animation]] call CFUNC(globalEvent);

_supplybox attachTo [ _player, [0, 1.7, 0.1] ];
_supplybox setdir 90;

GVAR(dragStartTime) = diag_tickTime;

[{
	(_this select 0) params ["_supplybox"];


	if ( !alive Clib_Player ||  { [Clib_Player] call MFUNC(isUnconscious) } ) exitWith {
		CLib_Player setVariable [QGVAR(dragEndAnim), nil];
		[_this select 1] call MFUNC(removePerFrameHandler);
		if (isNull _supplybox) then {
			{
					if (_x isKindOf "Constructions_base_F") then {
							detach _x;
					};
					nil;
			} foreach (attachedObjects CLib_Player);
		} else {
			detach _supplybox;
		};
	};
	private _noKeyPressed = !( ([DIK_F] call MFUNC(keyPressed))  || ([DIK_S] call MFUNC(keyPressed)) );
	if ( GVAR(dragStartTime) + 500 > diag_tickTime &&  _noKeyPressed ) then {

		private _animation = switch (CLib_Player getVariable [QGVAR(dragEndAnim), ""]) do {
				case "pistol": { "AcinPknlMstpSnonWpstDnon_AmovPknlMstpSrasWpstDnon"; };
				case "rifle": { "AcinPknlMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon" };
				default { "AcinPknlMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon" };
		};

		CLib_Player setVariable [QGVAR(dragEndAnim), nil];
		["switchMove", [Clib_Player, _animation]] call CFUNC(globalEvent);
		if (isNull _supplybox) then {
			{
					if (_x isKindOf "Constructions_base_F") then {
							detach _x;
					};
					nil;
			} foreach (attachedObjects CLib_Player);
		} else {
			detach _supplybox;
		};
		[_this select 1] call MFUNC(removePerFrameHandler);
	};

}, 0, [_supplybox]] call MFUNC(addPerFramehandler);
