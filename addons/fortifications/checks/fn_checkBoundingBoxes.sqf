/*
    Function:       FRL_Fortifications_fnc_checkBoundingBoxes
    Author:         N3croo
    Description:    extracts the widest cross section of the overall boundingboxes,
        to refer in an eleveation check
        TODO: detect lowest from individual BB
*/
#include "macros.hpp"

params ["_objects", "_posCentreAGL"];

private _core = _objects select 0;
_core setDir 0; // -- will be rotated by the script back into place
private _posCentreASL = AGLToASL _posCentreAGL;

private _BBRel = [_objects] call FUNC(getBBs);
private _BBData = [];

private _minZ = 1e39;
private _maxZ = -1e39;
{
    private _entry = _x;
    _entry params ["_object", "_posArr"];
    // locate BB corners in worldspace
    {
        private _posRel = _x;
        private _posASL = _object modelToWorldWorld _posRel;
        private _z = _posASL select 2;
        if (_z < _minZ) then {
            _minZ = _z;
        } else {
            if (_z > _maxZ) then {
                _maxZ = _z;
            };
        };
        _BBData pushBack [_object, _posRel, _z];
    } forEach _posArr;
} foreach _BBRel;

private _criticalBBCorners = [];
private _heightTol = 0.2 max ((_maxZ - _minZ) * 0.2);
{
    _x params ["_object", "_posRel", "_z"];
    if (_z - _minZ < _heightTol) then {
        _criticalBBCorners pushBack [_object, [_posRel]];
    };
} foreach _BBData;


_criticalBBCorners = _criticalBBCorners call FUNC(generateBBs);

_criticalBBCorners;
