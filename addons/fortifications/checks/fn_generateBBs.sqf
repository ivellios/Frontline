/*
    Function:       FRL_Fortifications_fnc_generateBBs
    Author:         N3croo
    Description:    generates BBs from data foprmat of getBBs, returns BB helper objects
*/
#include "macros.hpp"

private _posCentre = getPosATL (_this select 0 select 0);
private _spheres = [];

{
    _x params ["_object", "_posRel"];
    if ("SCALAR" == typeName (_posRel select 0)) then {
        _posRel = [_posRel];
    };
    {
        private _sphere = "Sign_Sphere10cm_F" createVehicleLocal _posCentre;
        _sphere attachto [_object, _x];
        _spheres pushBack _sphere;
    } forEach _posRel;
} forEach _this;

_spheres;
