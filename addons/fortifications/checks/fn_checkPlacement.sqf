/*
    Function:       FRL_Fortifications_fnc_checkPlacement
    Author:         N3croo
    Description:    checks if the (preview-)object is placed on flat ground and not too far away from the FO
    also checks the elevation above ground
    [testVeh,getpos foBox] call FRL_Fortifications_fnc_checkPlacement
*/
#include "macros.hpp"

params ["_posToSetAGL", "_forwardOutpost", "_foPosition", "_supplyCost", "_criticalBBCorners", "_allowElevatedConstruction"];

private _posToSetASL = AGLToASL _posToSetAGL;
private _posToSetATL = ASLToATL _posToSetAGL;

private _supportStructures = [];
private _validity = [];
private _fnc_addText = {
    _validity pushBack _this;
};

if ( _foPosition distance _posToSetAGL > EGVAR(Rolesspawns,buildRadius) ) then {
    "too far from the FO" call _fnc_addText;
};


private _supplyValue = _forwardOutpost getVariable ["supplies", 0];
if (_supplyCost > _supplyValue) exitWith {
    private _txt = format ["FO has Insufficient Supply: %1 / %2", _supplyValue, _supplyCost];
    _txt call _fnc_addText;
    _supportStructures
};



private _totalAbove = 0;
private _aboveTerrain = [];
private _aboveGround = [];
{
    private _BBcorner = _x;
    private _posCorner = getposASL _x;
    _aboveGround pushback ((ASLToAGL _posCorner) select 2);
    private _posCorner = ASLToATL _posCorner;
    private _elevation = _posCorner select 2;
    if (0 < _elevation) then {
        _aboveTerrain pushback _BBcorner;
        _totalAbove = _totalAbove + _elevation;
    };
} foreach _criticalBBCorners;

// tolerance for having single points above terrain
if (_totalAbove < 0.2) then {
    _aboveTerrain = [];
};
private _ctAboveTerrain = count _aboveTerrain;

// Otherwise no checks needed
if (_ctAboveTerrain > 0) then {


    private _isPlacedInBuilding = false;
    if !(_allowElevatedConstruction) then {
        // check if that is a building with a roof, if its a _allowElevatedConstruction false case
        _intersectObjs = lineIntersectsObjs [_posToSetASL, _posToSetASL vectorAdd [0, 0, 5], objNull, objNull, false, 4];
        _isPlacedInBuilding = true;
    };
    if (!_allowElevatedConstruction && _isPlacedInBuilding) exitWith {
        "Object can not be placed inside buildings" call _fnc_addText;
    };
    // -- if one corner of the object is too high above the ground check for whats under it
    // if corners are eleveated, check if its supported in the first place
    private _return = [];
    private _maxReturn = 0;
    {
        scopeName "positions";

        private _posASL = getPosASL _x;

        private _posASLTL = +_posASL;
        _posASLTL set [2, (getTerrainHeightASL _posASLTL + 0.1)];

        // NOTE: if begPosASL is under the ground and endPosASL is above it, the command will only return intersection with the ground, this is engine limitation and none of the intersectXXX commands will work when initiated from under the ground.
        private _lowPos = _posASL vectorAdd [0,0,-1];
        if (_lowPos select 2 < _posASLTL select 2) then {
            _lowPos = _posASLTL;
        };

        private _intersectedObjs = lineIntersectsSurfaces [_posASL vectorAdd [0,0,1.5], _lowPos, objNull, objNull, false, -1];
        private _intersectionsFiltered = [];

        // lets filter
        {
            _x params ["", "", "_intersectObj"];
            //([configfile >> "CfgVehicles" >> _intersectObj, true] call BIS_fnc_returnParents );

            if ((!(_intersectObj isKindOf "AllVehicles") || _intersectObj isEqualTo objNull) && { !(_x in GVAR(previewObjects)) } ) then {
                _intersectionsFiltered pushBack _x;
            };
        } foreach _intersectedObjs;

        // no objects intersecting
        if (count _intersectionsFiltered == 0) then {
            _return pushBack 2;
        } else {

            // no need to check for exact height if it already is exceeded by one corners
            if (_maxReturn == 0) then {
                private _posASLZ = _posASL select 2;
                {
                    _x params ["_intersectPosASL", "", "_intersectObj"];
                    private _delta = _posASLZ - (_intersectPosASL select 2);
                    //systemChat str _delta;
                    // since we are dealing with arma it picks up the lower end of the floor we are in
                    // the delta really is compared to floor thickness

                    if (_delta < 0.15) then {
                        _return pushBack 0;
                        _supportStructures pushBackUnique _intersectObj;
                        breakTo "positions";
                    };
                } forEach _intersectionsFiltered;
            };
            // didnt catch any support so it floats inside the building between/above floors
            _return pushBack 1;

        };

    } foreach _aboveTerrain;

    //systemChat str _return;

    if (_ctAboveTerrain > {_x == 0} count _return) then {
        // seems not all corners are valid
        private _cornersNotSupported = {_x == 2} count _return;

        if (_cornersNotSupported > 0) then {
            if (_cornersNotSupported == _ctAboveTerrain) then {
                "Object too high" call _fnc_addText;
            } else {
                "Object not fully supported" call _fnc_addText;
            };

        } else {
            // there is only one case left, no further counting needed
            // this is object too high inside the building
            "Object too high" call _fnc_addText;
        };
    } else {

    };

};

if (_validity isEqualTo []) then {
    GVAR(validPlacement) = true;
} else {
    GVAR(validPlacement) = false;
};
GVAR(validPlacementReason) = _validity joinString ", ";

_supportStructures;
