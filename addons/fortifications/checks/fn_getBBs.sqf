/*
    Function:       FRL_Fortifications_fnc_getBBs
    Author:         N3croo
    Description:    returns an array of all BB corners
*/
#include "macros.hpp"

params ["_objects"];
private _return = [];
{
    private _object = _x;
    private _posRel = [];

    // creates all 8 corners from the 2 given by BBR
    private _BBR = [];
    private _BBR = boundingBoxReal _object;

    private _bbx = [_BBR select 0 select 0, _BBR select 1 select 0];
    private _bby = [_BBR select 0 select 1, _BBR select 1 select 1];
    private _bbz = [_BBR select 0 select 2, _BBR select 1 select 2];


    private _ATGunClasses = ["StaticCanon", "StaticCannon", "fow_w_6Pounder"];
    private _classParents = ([configfile >> "CfgVehicles" >> typeOf _object, true] call BIS_fnc_returnParents);
    // ["LIB_Flakvierling_38","LIB_Flakvierling_38_base","LIB_StaticCanon_base","StaticCanon","StaticWeapon","LandVehicle","Land","AllVehicles","All"]
    // FOW plebs haz often/always no subclasses
    reverse _classParents;

    // overriding in case of AT guns so that its more limited to the mount/wheels instead of the rotations area the gun could cover
    if ( count _classParents >= 6 && {(_classParents select 5) in _ATGunClasses} ) then {
        _bby = [1,-1];
        _bbx = [1,-1];
    };

    // creating all 8 BB corners
    {
        private _xPos = _x;
        {
            private _yPos = _x;
            {
                private _zPos = _x;
                _posRel pushBack [_xPos, _yPos, _zPos];
            } forEach _bbz;
        } forEach _bby;
    } forEach _bbx;

    private _entry = [_object, _posRel];
    _return pushBack _entry;

} foreach _objects;

#ifdef ISDEV
    [{  { deleteVehicle _x; } forEach _this }, 5, _return call FUNC(generateBBs)] call CFUNC(wait);
#endif

_return;
