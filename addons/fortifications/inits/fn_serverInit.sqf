/*
    Function:       FRL_Fortifications_fnc_serverInit
    Author:         Adanteh / N3croo
    Description:
*/
#include "macros.hpp"

["vehicleInit", {
	(_this select 0) params ["_vehicle", "_side", "_varName"];
	if !(_vehicle isKindOf "StaticWeapon") then {
		private _supplyValue = [(typeOf _vehicle)] call FUNC(getSupplyCapacity);
	    if (_supplyValue > 0) then {
	        _vehicle setVariable ["suppliesLoaded", _supplyValue, true];
	    };
	};
}] call CFUNC(addEventHandler);

// -- Handles the removal of fortifications planted ontop of buildings
[QGVAR(fortificationCreated), {

    (_this select 0) params ["_constructorRoot"];
	private _rootObject = _constructorRoot getVariable ["rootObject", objNull];
	private _startPos = getposASL _rootObject;

	private _constructorObjs = _constructorRoot getVariable ["constructorObjects", []];

	private _allObjects = [_rootObject] + attachedObjects _rootObject;
	private _assignedSupport = _rootObject getVariable ["assignedSupport", []];

	// -- locks the static/crate during the buildprocess to prevent exploitation
	// -- could lead to statics being not fully constructed, used for combat and then salvaged
	{
		if (_x isKindOf "StaticWeapon") then {
			_x lock true;
		};
		if (_x isKindOf "ReammoBox_F") then {
			_x enablesimulation false;
		};
	} foreach _allObjects;

	// start implementing the handler here
	{
		_x setVariable ["assignedObjects", _constructorObjs + (_x getVariable ["assignedObjects", []] )];
		if (_x getVariable ["fortsKilledEH", true]) then {
			_x setVariable ["fortsKilledEH", true];
			_x addEventHandler ["Killed", {
				params ["_building"];
				{
					deleteVehicle _x;
				} foreach ( _building getVariable ["assignedObjects", [] ] );
			}];
		};
	} foreach _assignedSupport;

}] call CFUNC(addEventHandler);

// -- unlocks the fortification upon construction being completed

[QGVAR(fortificationFinished), {
	(_this select 0) params [["_rootObject", objNull]];
	{
		if (_x isKindOf "StaticWeapon") then {
			detach _x;
			_x lock false;
		};
		if (_x isKindOf "ReammoBox_F") then {
			_x enablesimulation true;
		};
	} foreach ([_rootObject] + attachedObjects _rootObject);

}] call CFUNC(addEventHandler);

[QGVAR(assignSupport), {
	(_this select 0) params ["_compositionObjects", ["_supportStructures", []] ];
	{
		_x setVariable ["assignedObjects", _compositionObjects + (_x getVariable ["assignedObjects", []] )];
	} forEach _supportStructures;
	// we only need to assign to the root
	{
		if (_foreachindex == 0 || _x isKindOf "StaticWeapon") then {
			 _x setVariable ["assignedSupport", _supportStructures];
		};
	} forEach _compositionObjects;

}] call CFUNC(addEventHandler);

[QGVAR(detachFromSupport), {
	(_this select 0) params ["_compositionObjects"];
	{
		private _compositionObj = _x;
		private _supports = _compositionObj getVariable ["assignedSupport", []];
		{
			private _assignedObjects = _x getVariable ["assignedObjects", []];
			_assignedObjects = _assignedObjects - [objNull];
			_x setVariable ["assignedObjects", _assignedObjects - [_compositionObj]];
		} foreach _supports;

	} forEach _compositionObjects;
}] call CFUNC(addEventHandler);

{
	private _pos = getposATL _x;
	private _type = typeof _x;
	private _dir = vectorDir _x;
	private _up = vectorUp _x;

	deleteVehicle _x;

	private _box = _type createVehicle _pos;
	_box setpos _pos;
	_box setvectorDirAndUp [_dir,_up];
} foreach allMissionObjects "FRL_SupplyContainer";
