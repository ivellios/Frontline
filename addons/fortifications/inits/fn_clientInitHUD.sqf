/*
    Function:       FRL_Fortifications_fnc_clientInitHUD
    Author:         Adanteh
    Description:	Handles the indiciation of suply values within FO range or inside crew seat of vehiclees with supply
*/

#include "macros.hpp"

// -- Handle the showing of bar when you enter/exit vehicles that have supply in them
GVAR(hudShown) = false;
["crewSeatEntered", {
    (_this select 0) params ["_vehicle", "_crewSeat"];
    if !(GVAR(hudShown)) then {
        if ((_vehicle getVariable ["suppliesLoaded", -1]) != -1) then {
            GVAR(hudShown) = true;
            [_vehicle, "Vehicle"] call FUNC(showSuppliesBar);
        };
    };
}] call CFUNC(addEventHandler);

// -- Hide bar when leaving the crew seats
["crewSeatLeft", {
    if (GVAR(hudShown)) then {
        GVAR(hudShown) = false;
        ["progressHudHide", [QGVAR(hudVehicle)]] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);

// -- Show bar if you're in a crew seat and new supplies were added
[QGVAR(SuppliesLoaded), {
    (_this select 0) params ["_vehicle", "_supplyValue"];
    private _inCrewSeat = missionNamespace getVariable [QEGVAR(Vehicles,inCrewSeat), false];
    if (_inCrewSeat) then {
        if !(GVAR(hudShown)) then {
            GVAR(hudShown) = true;
            [_vehicle, "Vehicle"] call FUNC(showSuppliesBar);
        };
    };
}] call CFUNC(addEventHandler);

// -- Show supply on FO when getting close to it
GVAR(BuildActionIndex) = -1;

[QEGVAR(rolesspawns,foEnter), {
    (_this select 0) params ["_spawnpoint", "_availableFor", "_units"];
    if !(Clib_Player in _units) exitWith {  };

    if (_availableFor != playerSide) exitWitH { }; // -- Only show supplies for friendly FO
    [_spawnpoint, "FO"] call FUNC(showSuppliesBar);
    if (GVAR(BuildActionIndex) <= 0 ) then {
        GVAR(forwardOutpost) = _spawnpoint;
        GVAR(FOID) = _spawnpoint getVariable ["name", "FOB"];
        GVAR(BuildActionIndex) = player addaction ["Build fortification", { ["open", _this select 0] call FUNC(loadMenu)}, [_spawnpoint], 2, false];
    };
}] call CFUNC(addEventHandler);

[QEGVAR(rolesspawns,foLeave), {
	(_this select 0) params ["_spawnpoint", "_availableFor", "_units"];
    if !(Clib_Player in _units) exitWith {  };

	["progressHudHide", [QGVAR(hudFO)]] call CFUNC(localEvent);
    player removeaction GVAR(BuildActionIndex);
    GVAR(BuildActionIndex) = -1;
}] call CFUNC(addEventHandler);
