/*
    Function:       FRL_Fortifications_fnc_clientInitFortifications
    Author:         N3croo
    Description:		adding interaction to objects enabling interaction with the module
		[] call initFortifications
    adding an option for calling [maintenanceBox] call  FRL_Fortifications_fnc_loadSupplies in base to load supploies
*/

#include "macros.hpp"

[QGVAR(settings), configFile >> "FRL" >> "CfgFortifications" >> "Settings"] call MFUNC(cfgSettingLoad);
[QGVAR(settings), missionConfigFile >> "FRL" >> "CfgFortifications" >> "Settings"] call MFUNC(cfgSettingLoad);

// setting parameters
GVAR(maxTotalFOSupply) = [QGVAR(settings_maxTotalFOSupply), 5000] call MFUNC(cfgSetting);
//GVAR(maxBuildDistance) = [QGVAR(settings_maxBuildDistance), 150] call MFUNC(cfgSetting);
GVAR(buildPeriodTime) = [QGVAR(settings_buildPeriodTime), 15] call MFUNC(cfgSetting);
GVAR(buildOffset) = [QGVAR(settings_buildOffset), 2] call MFUNC(cfgSetting);


{
    [QSVAR(fo), _x] call EFUNC(interactthreed,menuOptionAdd);
} forEach [
    ["", "Unload Supplies", { [_this select 1] call FUNC(unloadSupplies) }]
];


[QSVAR(constructor), [
    "",
    "Construct fortification",
    {
        ["start", [
            _this select 0,
            _this select 1,
           ((_this select 1) getVariable ["rootObject", objNull])
        ]] call FUNC(buildProcess);
    }
]] call EFUNC(interactthreed,menuOptionAdd);

[QSVAR(constructor), [
    "",
    "Salvage fortification",
    {
        ["start", [
            _this select 0,
            _this select 1,
           ((_this select 1) getVariable ["rootObject", objNull])
        ]] call FUNC(abortProcess);
    }
]] call EFUNC(interactthreed,menuOptionAdd);

["SupplyContainer", [
    "",
    "Move",
    { _this call EFUNC(statics,moveStatic) },
    { _this call EFUNC(statics,moveStaticCondition) }
]] call EFUNC(interactthreed,menuOptionAdd);

["crewSeatEntered", {
    (_this select 0) params ["_vehicle"];
    if (_vehicle isKindOf "StaticWeapon") then {
        ["entered", (_this select 0)] call FUNC(staticHunkering);
    };
}] call CFUNC(addEventHandler);

["crewSeatLeft", {
    ["exit"] call FUNC(staticHunkering);
}] call CFUNC(addEventHandler);

/*

params ["_constructorObject"];

["start", [
   _constructorObject,
   _constructorObject getVariable ["rootObject", objNull]
] ] call FUNC(buildProcess);

[{ _this call FUNC(buildProcess) }, 0, [
    _previewObjs,
    _pos,
    _setToDir
]] call MFUNC(addPerFrameHandler);

*/




// no longer needed since we moved away from using those boxes
/*
[QGVAR(supplyBoxCreated), {
    (_this select 0) params ["_supplyBox"];
    [_supplyBox, [0, 0, 0.8], "Build fortification", {
        params ["_supplyBox"];
        ["open", _supplyBox] call FUNC(loadMenu);
    }, "generic"] call EFUNC(interactThreeD,addInteractObject);
    [_supplyBox, [1, 0, 0.75], "Drag supplies", { _this call FUNC(dragSupplies); }, "generic"] call EFUNC(interactThreeD,addInteractObject);
    [_supplyBox, [-1, 0, 0.75], "Drag supplies", { _this call FUNC(dragSupplies); }, "generic"] call EFUNC(interactThreeD,addInteractObject);
}] call CFUNC(addEventHandler);


// -- JIP support. Add actions to current boxes
if !(isNil QGVAR(supplyBoxes)) then {
    private _boxes = +GVAR(supplyBoxes);
    {
        private _supplyBox = _x;
        if !(isNull _supplyBox) then {
            [QGVAR(supplyBoxCreated), [_supplyBox]] call CFUNC(localEvent);
        };
    } forEach _boxes;
};
*/
