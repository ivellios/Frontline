#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            dependencies[] = {"Main"};
            path = "pr\frl\addons\fortifications";

            class abortProcess;
            class detectYawAxis;
            class staticHunkering;
            class staticHunkeringLoop;
            class setToGround;

            // -- Add this
            class checks {
                class checkBoundingBoxes;
                class checkPlacement;
                class generateBBs;
                class getBBs;
            };
            class Inits {
                class clientInitFortifications;
                class clientInitHUD;
                class serverInit;
            };
            class Menu {
                class loadMenu;
                class populateMenu;
                class clientInitMenu;
                class cacheOptions;
            };
            class placement {
                class buildProcess;
                class compositionConstructorCreate;
                class compositionCreate;
                class compositionMove;
                class handleManipulationVars;
                class placementPreview;
                class placementPreviewHandler;
                class repositionStart;
                class repositionLoop;
                class rotateStatic;
            };
            class Supply {
                class getSupplyCapacity;
                class loadSupplies;
                class showSuppliesBar;
                class unloadSupplies;
            };
        };
    };
    #include "objects\CfgFortifications.cpp"

};

#include "objects\cfgVehicles.hpp"

class RscControlsGroupNoScrollbars;
class RscText;
class RscPictureKeepAspect;
class RscToolbox;
class RscListBox;
class RscListNBox;
class RscStructuredText;
#include "RscDialog.hpp"
