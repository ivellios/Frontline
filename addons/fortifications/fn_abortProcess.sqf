/*
    Function:       FRL_Fortifications_fnc_abortProcess
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {

    case "getduration": {
        _return = 2;
    };

    case "condition": {
        _args params ["_caller", "_constructorObj", "_rootObject"];

        if (isNull _rootObject) exitWith {
            ["showNotification", ["construction finished", "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };
    //
    //
    // Called every frame while the hold action button is still pressed, return should be true to keep action going, false to exit it
    case "callback": {
        _args params ["_caller"];
        _return = (alive _caller &&  !([_caller] call MFUNC(isUnconscious) ) );
    };

    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_constructorObj", "_rootObject"];
        [
            "Abort construction",
            TEXTURE(abort.paa),
            2,
            _args,
            { ["condition", _this] call FUNC(abortProcess) },
            { ["callback", _this] call FUNC(abortProcess) },
            { ["end", _this] call FUNC(abortProcess) },
            true,
            true,
            10,
            _dikButton // -- This is passed from menuUseActionKey
        ] call MFUNC(holdActionStart);
    };

    //
    //
    // Called when we press the button from the interact menu
    case "end": {
        _args params ["_data", "_endCode"];
        _data params ["_caller", "_constructorObj", "_rootObject"];

        // -- Succesfull abort
        if (_endCode isEqualTo 0) then {
            private _abortInformation = _constructorObj getVariable "abortInformation";
            _abortInformation params [ ["_fo", objNull], ["_supplycost", 0] ];
            _fo setVariable ["supplies", (_fo getVariable ["supplies", 0] ) + _supplyCost , true];

            {
                deleteVehicle _x;
            } foreach (attachedObjects _rootObject + [_rootObject]);

            private _constructorObjs = (_constructorObj getVariable ["constructorObjects", []]);
            [QGVAR(detachFromSupport), [_constructorObjs]] call CFUNC(serverEvent);
            [] spawn {
                sleep 0.5;
                {
                    deleteVehicle _x;
                } foreach _constructorObjs;
            }

        };
    };

};

_return;
