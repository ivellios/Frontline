/*
    Function:       FRL_Fortifications_fnc_compositionMove
    Author:         N3croo
    Description:    moves a compositions
*/
#include "macros.hpp"

params ["_mode", "_compositionObjects", ["_compositionData", []], ["_posToSet", [0, 0, 0]], ["_dirToSet", 0]];

//needs rework with terrainaligment mode
// -- Move the root object, the rest is attached to this and doesn't need to be moved
(_compositionData select 0) params [
    "_objType",
    ["_rootIndex", 0],
    ["_disableSim", 0],
    ["_posRel", [0, 0, 0]],
    ["_dirRel", 0],
    ["_vectorUpRelative", [0, 0, 1]]
];
private _rootObject = (_compositionObjects select 0);

_posToSet = (_posToSet vectoradd _posRel);
_rootObject setposATL _posToSet;

// -- rotation and direction wizzardry, do no touch. may contain traces of (incoorect) math

switch (_mode) do {
    case "preview": {
        params ["", "", "", "", ["_dirToSet", 0]];

        _vectorDir = [[0,1,0], - (_dirRel + _dirToSet)] call BIS_fnc_rotateVector2D;

        if ( GVAR(PitchRotation) != 0) then {
            private _offAngle =  (_dirToSet + _dirRel + GVAR(yawCorrection));
            _vectorUpRelative = [sin _offAngle * sin(GVAR(PitchRotation)), cos _offAngle * sin(GVAR(PitchRotation)), cos(GVAR(PitchRotation))];
            _vectorUpRelative = [_vectorUpRelative, _dirRel] call BIS_fnc_rotateVector2D;
        };

        //_rootObject setVectorDirAndUp [_vectorDir, _vectorUpRelative];
        _rootObject setvectorDir _vectorDir;
        _rootObject setvectorUP _vectorUpRelative;
    };
    case "elevate": {
        params ["", "", "", "", ["_vectorUpRelative", [0,0,1]] ];
        _rootObject setVectorUp _vectorUpRelative;
    };
};
