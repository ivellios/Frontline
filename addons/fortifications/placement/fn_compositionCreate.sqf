/*
    Function:       FRL_Fortifications_fnc_compositionCreate
    Author:         N3croo
    Description:    creates a compositions
*/
#include "macros.hpp"

params ["_mode", "_compositionObjects", "_compositionData", "_posToSet", ["_dirToSet", -1], ["_linkedSpawnpoint", objNull], ["_supportStructures",[]] ];

private _rootDir = 0;
{
    _x params [
        "_objectType",
        ["_rootIndex", 0],
        ["_disableSim", 0],
        ["_posRel", [0, 0, 0]],
        ["_dirRel", 0],
        ["_vectorUpRelative", [0, 0, 1]]
    ];

    private _vectorDir = 0;
    private _object = objNull;
    private _pos = _posToSet;
    private _rootObject = _rootIndex < 0;

    if (_rootObject) then {    //rootobject
        _pos = _posToSet vectoradd _posRel;
        _rootDir = _dirRel;
        _vectorDir = [[0,1,0], -(_dirRel + _dirToSet)] call BIS_fnc_rotateVector2D;
    } else {
        // backwards compability with old extraction method
        if (typeName _dirRel == "SCALAR") then {
            _dirRel = [[0,1,0], (-_dirRel+_rootDir)] call BIS_fnc_rotateVector2D;
        };
    };

    switch (toLower _mode) do {
        case "preview": {
            _object = _objectType createVehicleLocal _pos;
            player disablecollisionwith _object;
        };
        case "create": {
            _object = createVehicle [_objectType, _pos, [], 0, "CAN_COLLIDE"];

            // -- If this vehicle has seats, lock it (Don't allow moving of composition statics) and init it as a vehicle
            if !((fullCrew [_object, "", true]) isEqualTo []) then {
                _object setVariable [QSVAR(canMove), false, true];
                _object setVariable [QSVAR(bypassSide), true, true];
                ["vehicleInit", [_object, side group Clib_Player, str _object]] call CFUNC(globalEvent);

                if !(isNull _linkedSpawnpoint) then {
                    _object setVariable ["spawnpoint", _linkedSpawnpoint, true];
                };
            };
        };
        case "constructor": {
            _object = createVehicle [_objectType, _pos, [], 0, "CAN_COLLIDE"];
        };
    };

    if !(isNull _object) then {
        if (_rootObject) then {
            _object setPosATL _pos;
            _object setVectorDirAndUp [_vectorDir, _vectorUpRelative];
            if (_mode == "create") then {
                if (_disableSim > 0) then {
                    _object enablesimulation false;
                };
            };
        } else {
            private _root = (_compositionObjects select _rootIndex);
            _object attachto [_root, _posRel];
            _object setVectorDirAndUp [_dirRel, _vectorUpRelative];
        };
        _compositionObjects pushBack _object;
    };

    if (_object isKindOf "StaticWeapon") then {
        _object disableTIEquipment true;
    };

} foreach _compositionData;

_compositionObjects;
