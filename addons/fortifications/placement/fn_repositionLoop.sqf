/*
    Function:       FRL_Fortifications_fnc_repositionLoop
    Author:         N3croo
    Description:
*/

#define __INCLUDE_DIK
#include "macros.hpp"

(_this select 0) params [
    "_static",
    "_mouseEH",
    "_offsetRange",
    "_criticalBBCorners",
    "_allowElevatedConstruction"
];

//handling inputs
switch (true) do {
    case ([DIK_LSHIFT] call MFUNC(keyPressed)): {
        GVAR(rotationOffSet) = GVAR(rotationOffSet) + 3 * GVAR(mwheelInput);
    };
    case ([DIK_LCONTROL] call MFUNC(keyPressed)): {
        GVAR(PitchRotation) = GVAR(PitchRotation) + 1.5 * GVAR(mwheelInput);
        if (ABS(GVAR(PitchRotation) ) > 15) then {
            GVAR(PitchRotation) = ( abs(GVAR(PitchRotation)) / GVAR(PitchRotation) ) * 15;
        };
    };
    case ([DIK_LALT] call MFUNC(keyPressed)): {
        GVAR(xOffSet) = GVAR(xOffSet) + 0.25 * GVAR(mwheelInput);
        if ( GVAR(xOffSet) != 0 ) then {
            GVAR(xOffSet) = ( GVAR(xOffSet) min 4.5) max 0;
        };
    };
};

GVAR(mwheelInput) = 0;

//setting previewObject position
private _posToSet = positionCameraToWorld [0, 0, ( _offsetRange - GVAR(xOffSet))];
private _dirToSet = getDirVisual CLib_Player + GVAR(rotationOffSet);

_static setposATL _posToSet;
_vectorDir = [[0,1,0], -_dirToSet] call BIS_fnc_rotateVector2D;

if (GVAR(PitchRotation) != 0) then {
    private _offAngle =  (_dirToSet + GVAR(yawCorrection));
    _vectorUpRelative = [sin _offAngle * sin(GVAR(PitchRotation)), cos _offAngle * sin(GVAR(PitchRotation)), cos(GVAR(PitchRotation))];
};

_static setvectorDir _vectorDir;
_static setvectorUP _vectorUpRelative;

//automatic abort conditions;
if (!alive Clib_Player ||  { [Clib_Player] call MFUNC(isUnconscious) } ) then {
    GVAR(confirmCreation) = -1;
};

private _supportStructures = [_posToSet, objNull, _posToSet, 0, _criticalBBCorners, _allowElevatedConstruction] call FUNC(checkPlacement);

if (GVAR(confirmCreation) != 0) then {
    [QMVAR(hideControlsHUD)] call CFUNC(localEvent);
    ["forceWalk", "fortification", false] call CFUNC(setStatusEffect);
    [false] call MFUNC(disableMouse);
    (findDisplay 46) displayRemoveEventHandler ["MouseZChanged", _mouseEH];
    [_static, _supportStructures, _criticalBBCorners] call FUNC(setToGround);
    {
        deleteVehicle _x;
    } foreach (GVAR(previewObjects) - [_static]);
    "delete" call FUNC(handleManipulationVars);
    [_this select 1] call MFUNC(removePerFrameHandler);
};
