/*
    Function:       FRL_Fortifications_fnc_compositionConstructorCreate
    Author:         n3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_compositionObjects", "_buildTime", ["_positions", [[0, 0, 0], [0, 0, 0]]], ["_dirConstructor", 0], ["_posComposition", [0, 0, 0]], ["_dirComposition", 0], ["_up", [0, 0, 1]], "_abortInformation"];

_positions params ["_eyepos", "_fortPos"];
_fortPos = AGLToASL _fortPos;

private _posVector = _eyepos vectorFromTo (_fortPos vectoradd [0,0,1.65]);
private _posConstructor = _eyepos vectoradd (_posVector vectorMultiply 1.8);

private _intersects = lineIntersectsSurfaces [_posConstructor, (_posConstructor vectordiff [0,0,2.5]), objNull, objNull, true, 1];

if (count _intersects >= 1) then {
    _posConstructor = ((_intersects select 0) select 0);
};
_posConstructor = ASLtoAGL _posConstructor;

private _constructorCompositions = getArray (configfile >> "FRL" >> "cfgFortifications" >> "ConstructerObject" >> "composition");
private _constructorObjects = ["constructor", [], _constructorCompositions, _posConstructor, _dirConstructor] call FUNC(compositionCreate);
private _constructorObj = _constructorObjects select 0;
private _rootObject = _compositionObjects select 0;

private _posAboveSurface = getpos _constructorObj select 2;
if (abs _posAboveSurface > 0.15) then {
    _constructorObj setPosATL (_posConstructor vectorDiff [0, 0, _posAboveSurface] );
};

#ifdef DEBUGFULL
    _buildTime = 1;
#endif

_constructorObj setVariable ["rootObject", _rootObject, true];
_constructorObj setVariable ["finalPos", [_posComposition, _dirComposition, _up], true];
_constructorObj setVariable ["constructorObjects", _constructorObjects, true];
_constructorObj setVariable ["constructionPeriodsRemaining", floor (_buildTime / GVAR(buildPeriodTime)) max 1, true];
_constructorObj setVariable ["constructionPeriods", floor (_buildTime / GVAR(buildPeriodTime)) max 1, true];
_constructorObj setVariable ["abortInformation", _abortInformation, true];

_constructorObj setVariable [QSVAR(interactMenuCategory), QSVAR(constructor), true];
for "_i" from 1 to (count _constructorObjects - 1) do {
    (_constructorObjects select _i) setVariable [QSVAR(proxy), _constructorObj, true];
};

[QGVAR(fortificationCreated), [_constructorObj]] call CFUNC(serverEvent);
