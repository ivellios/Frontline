/*
    Function:       FRL_Fortifications_fnc_repositionStart
    Author:         N3croo
    Description:
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params ["", "_static"];
private _player = CLib_Player;

[QGVAR(detachFromSupport), [[_static]]] call CFUNC(serverEvent);

"create" call FUNC(handleManipulationVars);

private _offsetRange = 5;
private _posToSet = (positionCameraToWorld [0, 0, _offsetRange]);
_static setPosATL _posToSet;

GVAR(previewObjects) = [_static];
private _criticalBBCorners = [[_static], _posToSet] call FUNC(checkBoundingBoxes);
GVAR(previewObjects) = GVAR(previewObjects) + _criticalBBCorners;

private _mouseEH = (findDisplay 46) displayAddEventHandler ["MouseZChanged", { GVAR(mwheelInput) = GVAR(mwheelInput) + (_this select 1)}];

[QMVAR(showControlsHUD), [
    ["To place the weapon", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { if (GVAR(validPlacement)) then {GVAR(confirmCreation) = 1}; }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
    ["To drop the weapon", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\rmb_ca.paa"], { GVAR(confirmCreation) = -1}, true, [1]],
    ["To rotate", [TEXTURE(Shift.paa), TEXTURE(Mouse_roll.paa)], {}, true, [2], { true }],
    ["To pitch", [TEXTURE(Ctrl.paa), TEXTURE(Mouse_roll.paa)], {}, true, [2], { true }],
    ["To change distance", [TEXTURE(Alt.paa), TEXTURE(Mouse_roll.paa)], {}, true, [2], { true }]
]] call CFUNC(localEvent);

private _allowElevatedConstruction = !(_static isKindOf "staticCannon" || _static isKindOf "staticCanon");

["forceWalk", "fortification", true] call CFUNC(setStatusEffect);

[{
    _this call FUNC(repositionLoop);
}, 0, [
    _static,
    _mouseEH,
    _offsetRange,
    _criticalBBCorners,
    _allowElevatedConstruction
]] call MFUNC(addPerFrameHandler);
