/*
    Function:       FRL_Fortifications_fnc_handleManipulationVars
    Author:         N3croo
    Description:    A Function That Does stuff

*/
#include "macros.hpp"

switch (_this) do {

    case "create": {
        GVAR(confirmCreation) = 0;

        if (isNil QGVAR(xOffSet)) then {
            GVAR(xOffSet) = 0;
        };
        if (isNil QGVAR(PitchRotation)) then {
            GVAR(PitchRotation) = 0;
        };
        if (isNil QGVAR(rotationOffSet)) then {
            GVAR(rotationOffSet) = 0;
        };

        GVAR(yawCorrection) = 0;
        GVAR(mwheelInput) = 0;
        GVAR(validPlacement) = false;
        GVAR(validPlacementReason) = "";
        if (isNil QGVAR(previewObjects)) then {
            GVAR(previewObjects) = [];
        };

        if (!weaponLowered CLib_Player) then {
            if (local CLib_Player) then {
                if (cameraView == "GUNNER") then { CLib_Player switchCamera "INTERNAL" };
            };
            CLib_Player action ["WeaponOnBack", CLib_Player];
        };
        [true] call MFUNC(disableMouse);
    };

    case "delete": {
        GVAR(confirmCreation) = nil;
        GVAR(xOffSet) = nil;
        GVAR(PitchRotation) = nil;
        GVAR(rotationOffSet) = nil;
        GVAR(yawCorrection) = nil;
        GVAR(mwheelInput) = nil;
        GVAR(validPlacement) = nil;
        GVAR(validPlacementReason) = nil;
        GVAR(previewObjects) = nil;
    };
};
