/*
    Function:       FRL_Fortifications_fnc_placementPreview
    Author:         N3croo
    Description:		Previewing the placement of fortifications
        [FoBox, "Land_BagFence_Long_F",5,0,false] call FRL_Fortifications_fnc_placementPreview
        TODO: improve gradient detection for comps
*/
#include "macros.hpp"

GVAR(placementParams) = _this;

params ["_objType", "_displayName", "", "_supplyCost", "_buildTime", "_compositionData", "_minPlayerCount", "", "_allowElevatedConstruction"];
private _forwardOutpost = GVAR(forwardOutpost);

#ifdef DEBUGFULL
    _minPlayerCount = 0;
    _supplyCost = 0;
#endif

// -- Exit straight away when FO is destroyed
if (_forwardOutpost getVariable ["destroyed", false]) exitWith {
    ["The FO has been destroyed", "nope"] call MFUNC(notificationShow);
};

// -- Exit straighta way if you don't have the supply required
if (_supplyCost > (_forwardOutpost getVariable ["supplies", 0])) exitWith {
    ["Insufficient supply to build this", "nope"] call MFUNC(notificationShow);
};

private _playerCount = [false] call MFUNC(getSideSmallest);
if (_minPlayerCount > 0 && _playerCount < _minPlayerCount ) exitWith {
    ["Not enough players to allow this construction", "nope"] call MFUNC(notificationShow);
};

"create" call FUNC(handleManipulationVars);

// -- Create the preview composition
private _offsetRange = 5;
if (count _compositionData == 0) then {		// handles non composition object
    _compositionData = [[_objType, -1]];
} else {
    _offsetRange = 10;
};
// private _posToSet = getPosATL player vectoradd ((getCameraViewDirection player) vectormultiply _offsetRange);
private _posToSet = (positionCameraToWorld [0, 0, _offsetRange]);
private _objectArray = ["preview", [], _compositionData, _posToSet, getDirVisual Clib_Player] call FUNC(compositionCreate);

// -- changes yaw axis if necessary
_objectArray call FUNC(detectYawAxis);

GVAR(previewObjects) = GVAR(previewObjects) + _objectArray;

if ({ alive _x } count _objectArray == 0) exitWith {
    [format ["invalid return of %1", _fnc_scriptName], "error"] call MFUNC(notificationShow);
    { deleteVehicle  _x } foreach _objectArray;
};

private _foPosition = _forwardOutpost getVariable ["position", [0, 0, 0]];

private _criticalBBCorners = [_objectArray, _posToSet] call FUNC(checkBoundingBoxes);
GVAR(previewObjects) = GVAR(previewObjects) + _criticalBBCorners;


private _mouseEH = (findDisplay 46) displayAddEventHandler ["MouseZChanged", { GVAR(mwheelInput) = GVAR(mwheelInput) + (_this select 1)}];

[QMVAR(showControlsHUD), [
    ["To place the fortification", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { if (GVAR(validPlacement)) then { GVAR(confirmCreation) = 1 }; }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
    ["To place multiple", [TEXTURE(Shift.paa), "\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], {}, true, [2], { true }],
    ["To cancel placement", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\rmb_ca.paa"], { GVAR(confirmCreation) = -1}, true, [1]],
    ["To rotate", [TEXTURE(Shift.paa), TEXTURE(Mouse_roll.paa)], {}, true, [2], { true }],
    ["To pitch", [TEXTURE(Ctrl.paa), TEXTURE(Mouse_roll.paa)], {}, true, [2], { true }],
    ["To change distance", [TEXTURE(Alt.paa), TEXTURE(Mouse_roll.paa)], {}, true, [2], { true }]
]] call CFUNC(localEvent);

[{
    _this call FUNC(placementPreviewHandler);
}, 0, [
    _forwardOutpost,
    _foPosition,
    _objectArray,
    _mouseEH,
    _objType,
    _supplyCost,
    _buildTime,
    _compositionData,
    _offsetRange,
    _criticalBBCorners,
    _allowElevatedConstruction
]] call MFUNC(addPerFrameHandler);
