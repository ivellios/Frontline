/*
    Function:       FRL_Fortifications_fnc_rotateStatic
    Author:         N3croo
    Description:    Rotates statics to player viewing direction
*/
#include "macros.hpp"

params ["_player", ["_static", objNull]];

if !(alive _static) exitWith {
    ["Staticweapon destroyed or invalid", "nope"] call MFUNC(notificationShow);
};
if ( (crew _static findif {alive _x}) >= 0) exitWith {
    ["Static needs to be empty to rotate", "nope"] call MFUNC(notificationShow);
};


private _vectorUp = vectorUp _static;
private _setToVectorDir = eyeDirection player;
_setToVectorDir set [2,0];
private _direction = (_setToVectorDir select 0) atan2 (_setToVectorDir select 1);

private _attachedToObject = attachedTo _static;
if (isNull _attachedToObject) then {
    _static setDir _direction;
    _static setVectorDirAndUp [_setToVectorDir, _vectorUp];
} else {
    private _direction = - (_setToVectorDir - (getdir _attachedToObject) );
    _static setdir _direction;
    private _directionVector = [[0,1,0], _direction ] call BIS_fnc_rotateVector2D;
    _static setVectorDirAndUp [_directionVector, _vectorUp];
};
