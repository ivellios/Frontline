/*
    Function:        FRL_Fortifications_fnc_buildProcess
    Author:         N3croo
    Description:    handles the building process of the fortification
    TODO: make loos animation for longer constructiontimes... till now limited to 15s time
*/

#include "macros.hpp"

params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {


    case "getduration": {
        _return = GVAR(buildPeriodTime);
        #ifdef DEBUGFULL
            _return = 2;
        #endif
    };

    //
    //
    // Called every frame while the hold action button is still pressed, return should be true to keep action going, false to exit it
    case "callback": {
        _args params ["_caller", "_constructorObj"];
        private _distance = _constructorObj distance CLib_Player;
        _return = (alive _caller &&  !([_caller] call MFUNC(isUnconscious)) && _distance <= 3 );
    };

    case "condition": {
        _args params ["_caller", "_constructorObj"];

        if !(alive _constructorObj) exitWith {
            ["showNotification", ["construction finished", "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };

    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_constructorObj", "_rootObject"];
        [
            "Build",
            TEXTURE(build.paa),
            ["getduration"] call FUNC(buildProcess),
            _args,
            { ["condition", _this] call FUNC(buildProcess) },
            { ["callback", _this] call FUNC(buildProcess) },
            { ["end", _this] call FUNC(buildProcess) },
            true,
            true,
            10,
            _dikButton // -- This is passed from menuUseActionKey
        ] call MFUNC(holdActionStart);
    };

    //
    //
    // Called when we press the button from the interact menu
    case "end": {
        _args params ["_data", "_endCode"];
        _data params ["_caller", "_constructorObj", "_rootObject"];



        // -- Succesfull build
        if (_endCode isEqualTo 0) then {
            private _constructionPeriodsRemaining = (_constructorObj getVariable ["constructionPeriodsRemaining", 1]);
            private _rootObject = _constructorObj getVariable ["rootObject", objNull];
            private _rootFinalPos = _constructorObj getVariable ["finalPos", objNull];
            _rootFinalPos params ["_finalPos", "_dirToSet", "_upToSet"];

             // not finished with construction
            if (_constructionPeriodsRemaining > 1) then {

                _constructionPeriodsRemaining = _constructionPeriodsRemaining - 1;
                _constructorObj setVariable ["constructionPeriodsRemaining", _constructionPeriodsRemaining, true];
                private _constructionPeriods = _constructorObj getVariable ["constructionPeriods", 1];
                private _offsetFactor = _constructionPeriodsRemaining / _constructionPeriods;

                [
                    "elevate",
                    [_rootObject],
                    [[typeOf _rootObject, -1]],
                    _finalPos vectorDiff [0, 0, _offsetFactor * GVAR(buildOffset)],
                    _upToSet
                ] call FUNC(compositionMove);

            } else {    // finished with construction

                [
                    "elevate",
                    [_rootObject],
                    [[typeOf _rootObject, -1]],
                    _finalPos,
                    _upToSet
                ] call FUNC(compositionMove);

                createVehicle ["Land_ClutterCutter_medium_F",_finalPos, [], 0, "CAN_COLLIDE"];

                {
                    deleteVehicle _x;
                } foreach (_constructorObj getVariable ["constructorObjects", []]);

                [{ [QGVAR(fortificationFinished), _this] call CFUNC(serverEvent); }, 0.5, [_rootObject]] call CFUNC(wait);

            };
        };
    };

};

_return;
