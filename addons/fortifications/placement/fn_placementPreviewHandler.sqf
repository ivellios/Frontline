/*
    Function:       FRL_Fortifications_fnc_placementPreviewHandler
    Author:         N3croo
    Description:    updates preview in PFH
*/

#define __INCLUDE_DIK
#include "macros.hpp"

(_this select 0) params [
    "_forwardOutpost",
    "_foPosition",
    "_objectArrayPreview",
    "_mouseEH",
    "_objType",
    "_supplyCost",
    "_buildTime",
    "_compositionData",
    "_offsetRange",
    "_criticalBBCorners",
    "_allowElevatedConstruction"
];

//handling inputs
switch (true) do {
    case ([DIK_LSHIFT] call MFUNC(keyPressed)): {
        GVAR(rotationOffSet) = GVAR(rotationOffSet) + 3 * GVAR(mwheelInput);
    };
    case ([DIK_LCONTROL] call MFUNC(keyPressed)): {
        GVAR(PitchRotation) = GVAR(PitchRotation) + 1.5 * GVAR(mwheelInput);
        if (ABS(GVAR(PitchRotation) ) > 15) then {
            GVAR(PitchRotation) = ( abs(GVAR(PitchRotation)) / GVAR(PitchRotation) ) * 15;
        };
    };
    case ([DIK_LALT] call MFUNC(keyPressed)): {
        GVAR(xOffSet) = GVAR(xOffSet) + 0.25 * GVAR(mwheelInput);
        if ( GVAR(xOffSet) != 0 ) then {
            GVAR(xOffSet) = ( GVAR(xOffSet) min 4.5) max 0;
        };
    };
};

GVAR(mwheelInput) = 0;

//setting previewObject position
private _posToSet = positionCameraToWorld [0, 0, ( _offsetRange - GVAR(xOffSet))];
private _dirToSet = getDirVisual CLib_Player + GVAR(rotationOffSet);
["preview", _objectArrayPreview, _compositionData, _posToSet, _dirToSet] call FUNC(compositionMove);
private _supportStructures = [_posToSet, _forwardOutpost, _foPosition, _supplyCost, _criticalBBCorners, _allowElevatedConstruction] call FUNC(checkPlacement);

//automatic abort conditions;
if (!alive Clib_Player ||  { [Clib_Player] call MFUNC(isUnconscious) } ) then {
    GVAR(confirmCreation) = -1;
};

if (!alive _forwardOutpost || _forwardOutpost getVariable ["destroyed", false]) then {
    GVAR(confirmCreation) = -1;
    ["The FO has been destroyed", "nope"] call MFUNC(notificationShow);
};

//from here confirm or abort handling
if (GVAR(confirmCreation) != 0) then {

    [false] call MFUNC(disableMouse);

    if ((GVAR(confirmCreation) == 1) && GVAR(validPlacement)) then {		//placement confirmation and valid
        private _supplyValue = _forwardOutpost getVariable ["supplies", 0];

        if (_supplyCost > _supplyValue) then {
            ["showNotification", ["Insufficient supply", "nope"]] call CFUNC(localEvent);
        } else {

            // -- Create the real composition
            private _buildPosition = _posToSet vectorDiff [0, 0, GVAR(buildOffset)];
            private _dirComposition = getDirVisual CLib_Player + GVAR(rotationOffSet);
            private _compositionObjects = ["create", [], _compositionData, _buildPosition, _dirComposition, _forwardOutpost, _supportStructures] call FUNC(compositionCreate);

            // -- Create constructor objects and prepare the composition
            private _posConstructor = [eyePos Clib_Player, _buildPosition];
            private _finalPos = getPosATL (_objectArrayPreview param [0, objNull]);
            private _finalDir = getDir (_objectArrayPreview param [0, objNull]);
            private _finalUp = vectorUp (_objectArrayPreview param [0, objNull]);

            if !(_supportStructures isEqualTo []) then {
                // we only need to send this to the server, hence not setVariable
                [QGVAR(assignSupport), [_compositionObjects, _supportStructures]] call CFUNC(serverEvent);
            };

            [_compositionObjects, _buildTime, _posConstructor, _dirComposition, _finalPos, _finalDir, _finalUp, [_forwardOutpost, _supplycost] ] call FUNC(compositionConstructorCreate);

            // -- Reserve supply, it'll be restored if construction is aborted
            _forwardOutpost setVariable ["supplies", _supplyValue - _supplyCost, true];

        };
    };

    (findDisplay 46) displayRemoveEventHandler ["MouseZChanged", _mouseEH];
    //copied from fo placement
    [QMVAR(hideControlsHUD)] call CFUNC(localEvent);
    //["forceWalk", "fortification", false] call CFUNC(setStatusEffect);
    [false] call MFUNC(disableMouse);

    //avoiding interference with buildprocess since these vars are used in manipulate

    if (
        [DIK_LSHIFT] call MFUNC(keyPressed) &&
        { (GVAR(confirmCreation) == 1) }
    ) then {
        // attempting multiple placements
        if (GVAR(validPlacement)) then {
            ["showNotification", ["Fortification placed", "nope"]] call CFUNC(localEvent);
            { deleteVehicle _x } forEach _criticalBBCorners;
            GVAR(placementParams) call FUNC(placementPreview);
        };
    } else {
        GVAR(placementParams) = nil;
        { deleteVehicle _x; } forEach GVAR(previewObjects);
        "delete" call FUNC(handleManipulationVars);
    };
    // removes this PerFrameHandler
    [_this select 1] call MFUNC(removePerFrameHandler);

};

/*
#ifdef ISDEV
    systemChat str GVAR(previewObjects);
#endif
*/
