/*
    Function:       FRL_Fortifications_fnc_unloadSupplies
    Author:         N3croo
    Description:		Unloads the supplies to FOB and creates Object for interaction to call the buidprview
    [foBox] call FRL_Fortifications_fnc_unloadSupplies

    todo interactive object or interaction on FOB
*/
#include "macros.hpp"

params ["_forwardOutpost"];

//[[_fnc_scriptNameShort, _forwardOutpost], "orange"] call MFUNC(debugMessage);
private _spawnpoint = _forwardOutpost getVariable ["spawnpoint", objNull];
if (_spawnpoint getVariable ["destroyed", false]) exitWith  {
    ["FO is destroyed", "nope"] call MFUNC(notificationShow);
};

private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
if (_availableFor != playerSide) exitWith  {
    ["Can't unload supplies at enemy FOs", "nope"] call MFUNC(notificationShow);
};

private _supplyValue = 0;
private _vehList = (getpos _forwardOutpost) nearObjects ["AllVehicles", 50];
_vehList append ((getpos _forwardOutpost) nearObjects [QSVAR(SupplyContainer), 100]);
private _vehicle = objNull;
{
    _vehicle = _x;
    _supplyValue = _vehicle getVariable ["suppliesLoaded", 0];
    if (_supplyValue > 0 && speed _vehicle < 5 && (getpos _vehicle) select 2 <= 3) exitWith {
        _vehicle setVariable ["suppliesLoaded", nil, true];
    };
} foreach _vehList;

//UNS_VC
if (backpack player == "UNS_CIV_R1") then {
    removebackpack player;
    [format ["Unloaded 50 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 50;
};
if (backpack player == "UNS_CIV_R2") then {
    removebackpack player;
    [format ["Unloaded 200 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 200;
};
//UNS_USA
if (backpack player == "UNS_Alice_2") then {
    removebackpack player;
    [format ["Unloaded 50 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 50;
};
if (backpack player == "UNS_TROP_R1") then {
    removebackpack player;
    [format ["Unloaded 200 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 200;
};
//RHS_INS
if (backpack player == "B_FieldPack_khk") then {
    removebackpack player;
    [format ["Unloaded 50 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 50;
};
if (backpack player == "B_Carryall_khk") then {
    removebackpack player;
    [format ["Unloaded 200 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 200;
};
//IFA_USSR_P
if (backpack player == "B_LIB_SOV_RA_Rucksack_Green") then {
    removebackpack player;
    [format ["Unloaded 50 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 50;
};
if (backpack player == "fow_b_uk_bergenpack") then {
    removebackpack player;
    [format ["Unloaded 200 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 200;
};
//IFA_GER
if (backpack player == "B_LIB_GER_Backpack") then {
    removebackpack player;
    [format ["Unloaded 50 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 50;
};
if (backpack player == "B_LIB_GER_SapperBackpack_empty") then {
    removebackpack player;
    [format ["Unloaded 200 supplies from your backpack to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);
    _supplyValue = _supplyValue + 200;
};

If (_supplyValue > 0) then {

    private _oldTotalSupplyVal = _spawnpoint getVariable ["totalSupplies", 0];
    private _oldSupplyVal = _spawnpoint getVariable ["supplies", 0];

    if (_oldTotalSupplyVal < GVAR(maxTotalFOSupply) ) then {
        _spawnpoint setVariable ["totalSupplies", _oldTotalSupplyVal + _supplyValue, true];

        if (_oldTotalSupplyVal + _supplyValue > GVAR(maxTotalFOSupply)) then {
            _supplyValue = GVAR(maxTotalFOSupply) - _oldTotalSupplyVal;
        };
        _spawnpoint setVariable ["supplies", _oldSupplyVal + _supplyValue, true];


        private _dispName = getText (configfile >> "CfgVehicles" >> typeof _vehicle >> "displayName");
        if (_dispName == "") then {
            _dispName = typeOf _vehicle;
        };

        [format ["Unloaded %1 supplies from %2 to the FO", _supplyValue, _dispName], "ok"] call MFUNC(notificationShow);

        if (_vehicle isKindOf QSVAR(SupplyContainer)) then {
            deleteVehicle _vehicle;
        };
        ["suppliesUnload", [_spawnpoint, _forwardOutpost, _oldSupplyVal + _supplyValue]] call CFUNC(serverEvent);

    } else {
        ["The FO is a maximum Total supplies delivered, you can no longer supply this FO", "nope"] call MFUNC(notificationShow);
    };





} else {
   ["No vehicle carrying supplies found", "nope"] call MFUNC(notificationShow);
};
