/*
    Function:       FRL_Fortifications_fnc_showSuppliesBar
    Author:         Adanteh
    Description:    Shows bar with supplies (Top left corner, when you enter a crew seat withsupplies)
*/
#include "macros.hpp"

params ["_vehicle", ["_type", "Vehicle"]];

// -- Get the proper code depending ontype (different variable names, indicate text)
private _callbackCode = switch (toLower _type) do {
    case "fo": { {
        params ["_vehicle", "_type"];
        private _supplies = _vehicle getVariable ["supplies", 0];
        private _totalSupplyVal = _vehicle getVariable ["totalSupplies", 0];
        (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["FO Supplies: %1", ceil _supplies, _totalSupplyVal, GVAR(maxTotalFOSupply)]);
        (_controlsGroup controlsGroupCtrl 5) progressSetPosition (_totalSupplyVal / GVAR(maxTotalFOSupply));
    } };
    case "vehicle";
    default { {
        params ["_vehicle", "_type"];
        private _supplies = _vehicle getVariable ["suppliesLoaded", 0];
        (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Supplies: %1", ceil _supplies]);
    } };
};




["progressHudShow", [QGVAR(hud) + _type,
{
    params ["_vehicle", "_type"];
    (_controlsGroup controlsGroupCtrl 2) ctrlSetText TEXTURE(local_shipping.paa);
    (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.73, 0.46, 0.05, 0.6];
}, _callbackCode, [_vehicle, _type]]
] call CFUNC(localEvent);
