/*
    Function:       FRL_Fortifications_fnc_getSupplyCapacity
    Author:         N3croo
    Description:    returns the supply that can be carried by given vehicle class
*/

#include "macros.hpp"

params ["_vehicle"];
private _vehicleTypes = [_vehicle] call EFUNC(vehicles,getVehicleType);
reverse _vehicleTypes;

private _supplyValue = 0;
{
    _supplyValue = switch (toLower _x) do {

        case "tank": {
            0;
        };
        // -- 100
        case "armed_car": {
            100;
        };
        case "car": {
            100;
        };
        case "wheeledifv": {
            100;
        };
        // -- 200
        case "boat": {
            200;
        };
        case "ifv": {
            200;
        };
        case "heli_lighttransport": {
            200;
        };
        // -- 300
        case "apc": {
            300;
        };
        case "halftrack": {
            300;
        };
        case "heli_mediumtransport_armed": {
            300;
        };
        case "mrap": {
            300;
        };
        // -- 500
        case "heli_mediumtransport": {
            500;
        };
        case "truck": {
            500;
        };
        // -- 1000
        case "heli_heavytransport": {
            1000;
        };
        case "cargobox": {
            1000;
        };
        default {
            0;
        };

    };
    if (_supplyValue > 0) exitWith {};
} forEach _vehicleTypes;

_supplyValue;
