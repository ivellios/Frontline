/*
    Function:       FRL_Fortifications_fnc_loadSupplies
    Author:         N3croo
    Description:		Adds supplies to the closes vehicle upon activation
    [foBox] call  FRL_Fortifications_fnc_loadSupplies
*/
#include "macros.hpp"

params ["_playerVehicle"];  //rework for interaction

if (CLib_Player != driver _playerVehicle) exitWith {};
if ((_playerVehicle getVariable ["suppliesLoaded", 0]) > 0 ) exitwith {
    ["Vehicle already carrying supplies", "ok"] call MFUNC(notificationShow);
};

//isClass (configFile >> "FRL" >> "CfgFortifications"

private _playerVehicleType  = typeOf _playerVehicle;

private _dispName = getText (configfile >> "CfgVehicles" >> _playerVehicleType >> "displayName");
if (_dispName == "") then {
  _dispName = _playerVehicleType;
};

private _supplyValue = _playerVehicleType call FUNC(getSupplyCapacity);

if (_supplyValue > 0) then {
    _playerVehicle setVariable ["suppliesLoaded", _supplyValue, true];
    [format ["%1 has been loaded with %2 supplies", _dispName, _supplyValue], "ok"] call MFUNC(notificationShow);
    [QGVAR(SuppliesLoaded), [_playerVehicle, _supplyValue]] call CFUNC(localEvent);
} else {
    ["Vehicle has no cargo capacity to carry supplies", "nope"] call MFUNC(notificationShow);
};
