#define _WIDTH_FULL PX(115)

class GVAR(dialog) {
    idd = -1;
    duration = 1e11;
    name = QGVAR(dialog);
    onLoad = "uiNamespace setVariable ['FRL_Fortifications_dialog', _this select 0]; ['onLoad'] call FRL_Fortifications_fnc_loadMenu";
    onUnLoad = "['onUnload'] call FRL_Fortifications_fnc_loadMenu; uiNamespace setVariable ['FRL_Fortifications_dialog', nil]; ";
    class Controls {
        class CtrlGroup : RscControlsGroupNoScrollbars {
            idc = 100;
            x = -45;
            y = -20;
            w = _WIDTH_FULL;
            h = PY(40.2);

            class Controls {
                class Header: RscText {   //header
                    idc = 1;
                    colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"};
                    text = "BUILD FORTIFICATION";
                    x = PX(0);
                    y = PY(0);
                    w = _WIDTH_FULL;
                    h = PY(2);
                };

                class Background: RscText {
                    idc = 2;
                    colorBackground[] = {0, 0, 0, 0.6};
                    x = PX(0);
                    y = PY(2.2);
                    w = _WIDTH_FULL;
                    h = PY(37.9);
                };

                class MarkerTabs: RscToolbox {      //column selection
                    access = 0;
                    color[] = {0, 0, 0, 0};
                    colorDisable[] = {0, 0, 0, 0};
                    colorSelect[] = {0.95,0.95,0.95,1};
                    colorSelectedBg[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])",0.5};
                    colorText[] = {0.95,0.95,0.95,1};
                    colorTextDisable[] = {0, 0, 0, 0};
                    colorTextSelect[] = {0.95,0.95,0.95,1};
                    columns = 5;
                    deletable = 0;
                    fade = 0;
                    font = "RobotoCondensed";
                    rows = 1;
                    shadow = 0;
                    sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 2)";
                    strings[] = {};
                    values[] = {};
                    style = 2;
                    idc = 3;
                    x = PX(0);
                    y = PY(2);
                    w = _WIDTH_FULL;
                    h = PY(4);
                };

                class MarkerList: RscListNBox {
                    rowHeight = 0;
                    sizeEx = "((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.6)";
                    idc = 4;
                    x = PX(0);
                    y = PY(6.0);
                    w = _WIDTH_FULL;
                    h = PY(33.8);
                    columns[] = {0.001, 0.7, 0.9};
                };
            };
        };
    };
};
