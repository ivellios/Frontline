## how to Add fortifications

-design desired fortifications in missions_dev/fortifications.VR
-run the mission and use the interactions to set the scan distance and extract the object via extractObjs

hints: the very object you are looking at will be the rootobject, meaning all other will be attached to it, therefore do not use objects with Physics for the root and neither easily destructable objects.
the default direction the object faces to shall be north

-extractObjs copies all objects you scanned to you clipboard after you scanned the desired objects output those into tools/forts_extract.txt and run reformatObjs.py in python. This formats the objects into the composition array used by the config entrys in the objects folder.

the array entries of those output are as follows

example: {"Land_SandbagBarricade_01_hole_F",-1,0,{0,0,-0.164},178.193,{0,0,1}},

{objectType, flag for rootobject (-1 root), enablesimulation, offset, direction (vectorDir format, or direction), vectorup}

if you have questions contact N3croo
