#Author N3croo

import fileinput
import re

fileName = "forts_extract.txt"


try:
    fileInput = open(fileName, "r")
except FileNotFoundError:
    fileInput = open(fileName, "w")

fileOutput = open("output.txt", "w")

next = fileInput.readline()
while next != "":
    next = next.replace("[","{")
    next = next.replace("]","}")
    # we only want the subarrays so remove the outer backets
    next = re.sub('{{{"', '{{"', next)
    next = re.sub("}}}}", "}}}\n\n", next)

    next = re.sub('{{"', 'composition[] =\n{\n{"', next)
    next = re.sub('},{"', '},\n{"', next)

    #closing the composition bracket and ;
    next = re.sub('}}}', '}}\n};\n', next)
    next = re.sub(",composition", "\ncomposition", next)

    print(next)
    fileOutput.write(next)
    next = fileInput.readline()

fileOutput.close()
fileInput.close()
