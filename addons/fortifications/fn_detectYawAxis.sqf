/*
    Function:       FRL_Fortifications_fnc_detectYawAxis
    Author:         N3croo
    Description:    detects elongated single object fortifcations and changes their yaw axis
        makes placing razorwire less of a pain in the ass
*/
#include "macros.hpp"

if !(count _this == 1) exitWith {};

private _object = _this select 0;
if (_object isKindOf "StaticWeapon") exitWith {};
private _bb = _object call BIS_fnc_boundingBoxDimensions;

if ((_bb select 0) > (_bb select 1)) then {
    GVAR(yawCorrection) = 90;
};
