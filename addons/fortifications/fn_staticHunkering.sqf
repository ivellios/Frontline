/*
    Function:       FRL_Fortifications_fnc_staticHunkering
    Author:         N3croo
    Description:    Allows usage of animations to hunker down in static weapons

*/
#include "macros.hpp"
#define __INCLUDE_DIK

params ["_mode", "_params"];

switch (_mode) do {
    case "entered": {
        _params params ["_static"];
        _defaultAnim = animationState CLib_Player;

        private _animationSet = switch true do {
            case (_static isKindOf "AA_01_base_F"): {"kneeling"};
            case (_static isKindOf "AT_01_base_F"): {
                switch true do {
                    case (_static isKindOf "rhs_Metis_Base"): {""};
                    default {"kneeling"};
                };
            };
            case (_static isKindOf "StaticATWeapon"): {
                switch true do {
                    // case (_static isKindOf "rhs_Metis_Base"): {""};
                    default {"kneeling"};
                };
            };
            case (_static isKindOf "StaticMortar"): {"kneeling"};
            case (_static isKindOf "StaticGrenadeLauncher"): {"kneeling"};
            case (_static isKindOf "StaticMGWeapon"): {
                switch true do {
                    case (_static isKindOf "rhs_DSHkM_Mini_TriPod_base"): {"kneeling"};
                    case (_static isKindOf "rhs_DSHKM_base"): {"standing"};
                    case (_static isKindOf "RHS_KORD_high_base"): {"standing"};
                    case (_static isKindOf "fow_w_m1919_tripod"): {""};
                    case (_static isKindOf "LIB_M1919_M2"): {""};
                    case (_static isKindOf "LIB_Maxim_M30_Trench"): {"kneeling"};
                    case (_static isKindOf "LIB_Maxim_M30_base"): {""};
                    case (_static isKindOf "rhs_m2staticmg_base"): {"standing"};
                    case (_static isKindOf "LIB_MG42_Lafette_low"): {""};
                    case (_static isKindOf "HMG_01_high_base_F"): {"standing"};
                    case (_static isKindOf "GMG_01_high_base_F"): {"standing"};
                    default {"kneeling"};
                };
            };
            case (_static isKindOf "StaticMortar"): {"kneeling"};
            case (_static isKindOf "StaticCannon"): {
                switch true do {
                    case (_static isKindOf "rhs_D30_base"): {"standing"};
                    case (_static isKindOf "LIB_61k_base"): {""};
                    case (_static isKindOf "LIB_FlaK_36_base"): {""};
                    case (_static isKindOf "LIB_StaticCannon_base"): {""};
                    case (_static isKindOf "RHS_M119_base"): {"kneeling"};
                    default {"kneeling"};
                };
            };
            default {""};
        };

        private _anims = switch _animationSet do {
            case "standing": {["AmovPercMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon", "AmovPknlMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon"]};
            case "kneeling": {["AmovPknlMstpSnonWnonDnon_AmovPpneMstpSnonWnonDnon", "AmovPpneMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon"]};
            // case "prone": {["BasicDriverOutDying", ""]};
            // may be needed and suitable anim somewhere
            default {[]};
        };
        // systemChat str _animationSet;
        if (_anims isEqualTo []) exitWith {};

        if (isNil QGVAR(hunkerHandle)) then {
            GVAR(isHunkering) = false;
            GVAR(hunkerHandle) = [{
                _this call FUNC(staticHunkeringLoop);
            }, 0, (_anims + [_defaultAnim])] call MFUNC(addPerFramehandler);
            [QMVAR(showControlsHUD), [
                ["To hunker down", [TEXTURE(Ctrl.paa)]]
            ], false] call CFUNC(localEvent);

        };
    };
    case "exit": {
        if !(isNil QGVAR(hunkerHandle)) then {
            [QMVAR(hideControlsHUD)] call CFUNC(localEvent);
            GVAR(hunkerHandle) call MFUNC(removePerFrameHandler);
            GVAR(hunkerHandle) = nil;
            GVAR(isHunkering) = nil;
            GVAR(delay) = nil;
        };
    };
};


/*
[{
	(_this select 0) params ["_weapon"];

	if ( !alive Clib_Player ||  { [Clib_Player] call MFUNC(isUnconscious) } ) exitWith {
		CLib_Player setVariable [QGVAR(dragEndAnim), nil];
		[_this select 1] call MFUNC(removePerFrameHandler);
		if (isNull _weapon) then {
			{
					if (_x isKindOf "StaticWeapon") then {
							detach _x;
					};
					nil;
			} foreach (attachedObjects CLib_Player);
		} else {
			detach _weapon;
		};
	};
	private _noKeyPressed = !( ([DIK_F] call MFUNC(keyPressed))  || ([DIK_S] call MFUNC(keyPressed)) );
	if ( GVAR(dragStartTime) + 500 > diag_tickTime &&  _noKeyPressed ) then {

		private _animation = switch (CLib_Player getVariable [QGVAR(dragEndAnim), ""]) do {
				case "pistol": { "AcinPknlMstpSnonWpstDnon_AmovPknlMstpSrasWpstDnon"; };
				case "rifle": { "AcinPknlMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon" };
				default { "AcinPknlMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon" };
		};

		CLib_Player setVariable [QGVAR(dragEndAnim), nil];
		["switchMove", [Clib_Player, _animation]] call CFUNC(globalEvent);
		if (isNull _weapon) then {
			{
					if (_x isKindOf "Constructions_base_F") then {
							detach _x;
					};
					nil;
			} foreach (attachedObjects CLib_Player);
		} else {
			detach _weapon;
		};
		[_this select 1] call MFUNC(removePerFrameHandler);
	};

}, 0, [_weapon]] call MFUNC(addPerFramehandler);

*/
