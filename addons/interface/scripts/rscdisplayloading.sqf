#include "macros.hpp"
#include "..\incLoadingSizes.hpp"

disableserialization;

_mode = _this select 0;
_params = _this select 1;
_class = _this select 2;

switch _mode do {
	case "onLoad": {
		_display = _params select 0;
		RscDisplayLoading_display = _display;

		//--- Initial loading - maintain visual style of RscDisplayStart
		if !(uinamespace getvariable ["BIS_initGame",false]) exitwith {};

		///////////////////////////////////////////////////////////////////////////////////////////

		//--- Hide start loading screen
		_ctrlLoadingStart = _display displayctrl 2310;
		_ctrlLoadingStart ctrlsetfade 1;
		_ctrlLoadingStart ctrlcommit 0;

		//--- Map
		if (worldname != "") then {
			_cfgWorld = configfile >> "cfgworlds" >> worldname;
			_worldName = gettext (_cfgWorld >> "description");
			_worldAuthor = getText (_cfgWorld >> "author");

			// -- Make sure we dont'constantly switch pictures
			private _loadingPicture = "";
			(uiNamespace getVariable ["frl_loadingPictureUsed", [-1, "", ""]]) params ["_lastPictureUsedAt", "_lastPictureWorld", "_lastPicture"];
			if ((_lastPictureWorld != worldName) || { ((diag_tickTime - _lastPictureUsedAt) > 60) }) then {
				_picturesIsland = getArray (configFile >> "FRL" >> "LoadingScreen" >> worldName);
				if !(_picturesIsland isEqualTo []) then {
					_loadingPicture = selectRandom _picturesIsland;
				} else {
					private _default = gettext (_cfgWorld >> "pictureMap");
					if (_default != "") then {
						_loadingPicture = _default;
					} else {
						_loadingPicture = getText (configFile >> "FRL" >> "LoadingScreen" >> "default")
					};
				};
				(uiNamespace setVariable ["frl_loadingPictureUsed", [diag_tickTime, worldName, _loadingPicture]]);
			} else {
				_loadingPicture = _lastPicture;
			};

			_worldText = toUpper _worldName;
			if (_worldAuthor != "") then {
				_worldText = format ["%1, %2 %3", _worldText, "by", _worldAuthor];
			};
			private _loadingHint = "";
			_loadingHints = getarray (configFile >> "FRL" >> "loadingTexts");
			if (count _loadingHints > 0) then {
				(uiNamespace getVariable ["frl_loadingHintUsed", [-1, ""]]) params ["_lasHintUsedAt", "_lasHintUsed"];
				if ((diag_tickTime - _lasHintUsedAt) > 30) then {
					_loadingHint = selectRandom _loadingHints;
					(uiNamespace setVariable ["frl_loadingHintUsed", [diag_tickTime, _loadingHint]]);
				} else {
					_loadingHint = _lasHintUsed;
				};
			};

			//--- Set texts
			(_display displayCtrl 6999) ctrlsettext _loadingPicture;
			(_display displayctrl 6001) ctrlsettext _worldText;
			(_display displayctrl 6004) ctrlSetText _loadingHint;
		};

		//--- Mission
		_fnc_loadMission = {
			disableserialization;
			_display = _this select 0;
			_isMultiplayer = servertime > 0;

			//--- When loading a different terrain, current mission is sometimes still available. Check if it belongs to the terrain.
			_last = uinamespace getvariable ["RscDisplayLoading_last",[worldname,missionname]];
			_lastWorld = _last select 0;
			_lastMission = _last select 1;
			_showMission = if (missionname == _lastMission) then {worldname == _lastWorld} else {true};
			uinamespace setvariable ["RscDisplayLoading_last",[worldname,missionname]];

			//--- Get loading bars
			_progressMap = _display displayctrl 104;
			if (isnull _progressMap) then {_display displayctrl 1013};
			_progressMission = _display displayctrl 1013;
			RscDisplayLoading_progress = _progressMap;


			_fnc_getMissionDescription = {
				private _description = "";
				_loadingName = getmissionconfigvalue ["onLoadName",""];
				if (_loadingName == "") then {
					_loadingName = briefingname;
				};
				if (_loadingName != "") then {
					_description = toUpper _loadingName;
					_loadingAuthor = getmissionconfigvalue ["author",""];
					if (_loadingAuthor != "") then {
						_description = format ["%1<br/><t size='0.4'>by %2</t><br/>", _description, _loadingAuthor];
					};
				};
				_loadingDescription = getmissionconfigvalue ["onLoadMission",""];
				if (_loadingDescription == "") then {
					_loadingDescription = getmissionconfigvalue ["overviewText",""];
				};
				if (_loadingDescription != "") then {
					_description = format ["%1<br/>%2", _description, _loadingDescription];
				};
				_description
			};
			private _missionDescription = call _fnc_getMissionDescription;
			if ((_missionDescription != "") && _showMission) then {
				private _positionY = ORIGIN_BOX_Y;

				private _ctrlMissionDescription = _display displayCtrl 6100;
				if (_missionDescription != "") then {
					private _textBox = _ctrlMissionDescription controlsGroupCtrl 6502;
					_textBox ctrlsetStructuredText parseText _missionDescription;
					private _textHeight = ctrltextheight _textBox + GRIDY(2.0);
					_ctrlMissionDescriptionPos = ctrlposition _ctrlMissionDescription;
					_ctrlMissionDescriptionPos set [3, _textHeight];
					_ctrlMissionDescriptionPos set [1, _positionY - _textHeight];
					_ctrlMissionDescription ctrlSetPosition _ctrlMissionDescriptionPos;
					_ctrlMissionDescription ctrlCommit 0;
					_positionY = _positionY - _textHeight - SIZE_SPACING_BOX;
				} else {
					_ctrlMissionDescription ctrlshow false;
				};

				// -- Game type description
				_fnc_getGametypeDescription = {
					private _description = "";
					_gameType = gettext (missionconfigfile >> "Header" >> "gameType");
					_gameTypeName = gettext (configfile >> "CfgMPGameTypes" >> _gameType >> "name");
					if (_gameTypeName != "") then {
						_description = toUpper _gameTypeName;
						_gameTypeRules = gettext (configfile >> "CfgMPGameTypes" >> _gameType >> "rules");
						if (_gameTypeRules != "") then {
							_description = format ["%1<br/><t size='0.6'>%2</t><br/>", _description, _gameTypeRules];
						};
					};
					_description
				};
				private _gameTypeDescription = call _fnc_getGametypeDescription;
				private _ctrlGametypeDescription = _display displayCtrl 6200;
				if (_gameTypeDescription != "") then {
					private _textBox = _ctrlGametypeDescription controlsGroupCtrl 6502;
					_textBox ctrlsetStructuredText parseText _gameTypeDescription;
					private _textHeight = ctrltextheight _textBox + GRIDY(2.0);
					_ctrlGametypeDescriptionPos = ctrlposition _ctrlGametypeDescription;
					_ctrlGametypeDescriptionPos set [3, _textHeight];
					_ctrlGametypeDescriptionPos set [1, _positionY - _textHeight];
					_ctrlGametypeDescriptionPos ctrlSetPosition _ctrlMissionDescriptionPos;
					_ctrlGametypeDescriptionPos ctrlCommit 0;
					_positionY = _positionY - _textHeight - SIZE_SPACING_BOX;
				} else {
					_ctrlGametypeDescription ctrlshow false;
				};

				//--- Mission loading bar
				_progressMapPos = ctrlposition _progressMap;
				_progressMissionPos = ctrlposition _progressMission;
				if (missionnamespace getvariable ["RscDisplayLoading_progressMission",false]) then {
					//--- Mission loading - make the terrain bar full and animate only the mission bar
					_progressMap ctrlsetposition _progressMissionPos;
					_progressMap ctrlcommit 0;
					_progressMission ctrlsetposition _progressMapPos;
					_progressMission ctrlcommit 0;
					_progressMission progresssetposition 1;
				} else {

					//--- When loading a different map, a rogue loading screen without progress bar appears. Move the progress bar by script.
					_limit = [1,2] select _isMultiplayer;
					if (count (uinamespace getvariable "loading_displays") > _limit) then {
						_progressMap ctrlshow false;
						_progressMission ctrlsetposition _progressMapPos;
						_progressMission ctrlcommit 0;
						_progressMission progresssetposition 0.33;
					} else {
						_progressMission ctrlshow false;
					};
				};
			} else {
				_ctrlMission ctrlsetfade 1;
				_ctrlMission ctrlcommit 0;
			};
		};

		_ctrlMission = _display displayctrl 6300;
		if (!(isnull _ctrlMission)) then {
			[_display,0] call _fnc_loadMission;
		};
	};
};
