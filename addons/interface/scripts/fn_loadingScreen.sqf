#include "macros.hpp"
private _formatting = [
	[
		"</font>",
		"<br/>",
		"<font face='RobotoCondensed'>",
		"<font face='RobotoCondensedLight'>",
		"<font color='#83BB47'>",
		"&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
	],
	[
		"</t>",
		"<br/>",
		"<t font='RobotoCondensed' size='1.25'>",
		"<t align='left' font='RobotoCondensedLight' size='1'>",
		"<t color='#83BB47'>",
		"&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
	]
] select (_this select 1);

private _text = format (["%4
%3RALLY POINT (RPs)%1%2
%6Requires Squad Leader role%2
%6Can be placed in friendly territory%2
%6Limited Uptime (1:30 default)%2
%6Cooldown to place again (2:30 default)%2
%6Can rearm at rallypoint (5:00 default cooldown)%2
%2
%3FORWARD OUTPOSTS (FOs)%1%2
%6Requires 2 friendly players nearby%2
%6Max distance of friendly zone (400m default)%2
%6Min distance of enemy zone (200m default)%2
%6Min distance of other FOs (400m default)%2
%6Gets blocked by 2+ enemies in range (75m default)%2
%6Gets destroyed by capturing it down (Progress in top left)%2
%2
%3RADIO%1%2
%6%5[Caps Lock]%1 Talk: Selected channel (Defaults to Group Channel)%2
%6%5[Ctrl + Caps Lock]%1 Talk: Command channel (SL only)%2
%6%5[Teamspeak PTT]%1 Talk: Direct comms%2
%2
%3MAP%1%2
%6%5[Shift + Left Mouse]%1 Personal waypoint%2
%6%5[Shift + Right Mouse]%1 Squad waypoint (SL only)%2
%6%5[Right Mouse]%1 Spot enemies%2
%2
%3MEDIC SYSTEM%1%2
%6%5[Scrollwheel]%1 to select medical item%2
%6%5[Hold Left Mouse]%1 to treat friendly%2
%6%5[Hold Right Mouse]%1 to treat self%2
%6%5[Hold Q]%1 to apply CPR, which extends bleedout time%2
%6Before you can revive with Adrenaline, you need to stop bleeding first (Bandage)%2
%1"

/* Text formats */

] + _formatting);

if (_this select 1) then {
	(_this select 0 select 0) ctrlSetStructuredText (parseText _text);
} else {
	CLib_Player createDiarySubject [QGVAR(diary), "Frontline", "\A3\ui_f\data\map\markers\military\unknown_CA.paa"];
	CLib_Player createDiaryRecord [QGVAR(diary), ["Instructions", _text]];
};
