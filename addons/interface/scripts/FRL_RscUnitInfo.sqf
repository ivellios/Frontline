//## UNSUSED ##//

disableserialization;
_mode = _this select 0;
_params = _this select 1;
_class = _this select 2;

hint str(_this);

#define QUOTE(var1) #var1
#define GVAR(var1) FRL_RscUnitInfo_##var1
#define QGVAR(var1) QUOTE(GVAR(var1))
switch _mode do {
	case "onLoad": {
		with uinamespace do {
			GVAR(lastAmmo) = "";
			GVAR(magType) = "";
			GVAR(magAmmoCount) = 1;
			if !(isnil QGVAR(loop)) then { terminate GVAR(loop); };
			GVAR(loop) = [(_params select 0)] spawn {
				scriptname QGVAR(loop);
				disableserialization;
				private _display = _this select 0;

				while {!isnull _display} do {
					private _ammoCount = (ctrlText (_display displayCtrl 184));
					if (_ammoCount != GVAR(lastAmmo) && {_ammoCount != ""}) then {
						GVAR(lastAmmo) = _ammoCount;
						_ammoCount = parseNumber _ammoCount;
						systemChat format ["ammo: %1", _ammoCount];
						private _currentMagazine = (currentMagazine (vehicle player));
						if (_currentMagazine != GVAR(magType)) then {
							GVAR(magAmmoCount) = (getNumber (configFile >> "CfgMagazines" >> _currentMagazine >> "count")) max 1;
							GVAR(magType) = _currentMagazine;

							// -- If the ammo count is higher than ammo in a magazine, it means it's a launcher magazine / launcher grenades

						};

						if (_ammoCount > GVAR(magAmmoCount)) then {
							(_display displayCtrl 52003) ctrlSetText str(_ammoCount);
							(_display displayCtrl 52002) ctrlSetText "\pr\frl\addons\client\ui\media\unitinfo\round_0.paa";
						} else {
							// -- TODO: Make a picture for every percentage -- //
							// -- Get percentage left in multiples of 12.5
							private _percentageLeft = 100 - (0 max ((round (_ammoCount / GVAR(magAmmoCount) * 100 / 5)) * 5)) min 100;
							(_display displayCtrl 52002) ctrlSetText format ["\pr\frl\addons\client\ui\media\unitinfo\round_%1.paa", _percentageLeft];
							(_display displayCtrl 52003) ctrlSetText "";
						};
					};
					uiSleep 0.05;
				};
			};

			// -- VANILLA SCRIPT -- //
			private ["_display","_veh","_vehType","_uavPosition","_isPlayerInAV","_icon"];

			_display = _params select 0;
			_veh = vehicle cameraon;//player;
			_vehType = typeof _veh;
			_uavPosition = (uavControl (getConnectedUav player)) select 1;
			_isPlayerInAV = false;

			if((_uavPosition == "DRIVER") || (_uavPosition == "GUNNER")) then {
				_isPlayerInAV = true;
			};

			if !(isnil "RscUnitInfo_loop") then {terminate RscUnitInfo_loop;};

			//if ((_veh != player) || _isPlayerInAV) then {
			if !(isnull (_display displayctrl 250)) then {
				//--- Radar icon
				_icon = gettext (configfile >> "cfgvehicles" >> _vehType >> "icon");

				if (_isPlayerInAV) then
				{
					_icon = gettext (configfile >> "cfgvehicles" >> (typeof (getConnectedUav player)) >> "icon");
				};

				(_display displayctrl 250) ctrlsettext _icon;

				//--- Stabilized
				_stabilized = _display displayctrl 1204;
				_stabilized ctrlshow false;

				RscUnitInfo_loop = [_display,_hitzones,_isPlayerInAV] spawn {
					scriptname "RscUnitInfo_loop";
					disableserialization;
					_display = _this select 0;
					_isPlayerInAV = _this select 2;
					_stabilized = _display displayctrl 1204;

					while {!isnull _display} do {
						//--- Stabilized
						if(_isPlayerInAV) then
						{
							_stabilized ctrlshow (isAutoHoverOn (getConnectedUav player));
						}
						else
						{
							_stabilized ctrlshow (isAutoHoverOn vehicle cameraon);
						};
						sleep .1;
					};
				};
			};
		};
	};

	case "onUnload": {
		with uiNamespace do {
			if !(isnil QGVAR(loop)) then { terminate GVAR(loop); };
		};
	};
};
