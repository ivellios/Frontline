#define SIZE_WIDTH 			GRIDX(12)
#define SIZE_HEIGHT 		GRIDY(1.5)
#define ORIGIN_X CENTER_X
#define ORIGIN_Y ((getResolution select 3) * 0.85 * pixelH)

class SVAR(InteractHUD) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['frl_InteractHUD', _this select 0];";
    onUnLoad = "";
    class Controls {
      class CtrlGroup : RscControlsGroupNoScrollbars {
        idc = 3000;
        x = ORIGIN_X - (SIZE_WIDTH / 2);
        y = ORIGIN_Y;
        w = SIZE_WIDTH;
        h = SIZE_HEIGHT;

        class Controls {
          class ProgressText : RscStructuredText {
            idc = 1002;
            shadow = 1;
            x = 0;
            y = 0;
            w = SIZE_WIDTH;
            h = GRIDY(1.3);
            text = "";
            size = GRIDY(1);
            sizeEx = GRIDY(1);
            class Attributes {
              font = FONTMED;
              color = "#ffffff";
              align = "center";
              shadow = 0;
            };
          };

          class BackgroundProgress : RscBackground {
            idc = 1003;
            colorBackground[] = {1, 1, 1, 0.2};
            x = 0;
            y = GRIDY(1.1);
            w = SIZE_WIDTH;
            h = GRIDY(0.2);
          };

          class Progress : RscProgress {
            idc = 1004;
            colorFrame[] = {0,0,0,0};
            colorBar[] = {0.77, 0.51, 0.08, 1};
            x = 0;
            y = GRIDY(1.1);
            w = SIZE_WIDTH;
            h = GRIDY(0.2);
          };
        };
      };
      class ButtonsText : RscStructuredText {
        idc = 3004;
        shadow = 1;
        x = safeZoneX + (safeZoneW * 0.7);
        y = CENTER_Y - GRIDY(2.5);
        w = (safeZoneW * 0.2);
        h = GRIDY(5);
        text = "";
        size = GRIDY(1);
        class Attributes {
          font = FONTMED;
          color = "#ffffff";
          align = "left";
          shadow = 1;
        };
      };
    };
};
