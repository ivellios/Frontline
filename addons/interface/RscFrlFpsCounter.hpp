#define _w (0.05*safeZoneW)
#define _h (0.023*safeZoneH)

class SVAR(fpsCounter) {
	idd = -1;
	duration = 1e11;
	fadeIn = 0;
	fadeOut = 0;
	onLoad = "uiNamespace setVariable [""frl_fpsCounter"", _this select 0]";

	class ControlsBackground {
		class Framerate : RscText {
			moving = 0;
			idc = 1;
			font = FONTLIGHT;
			colorBackground[] = {0,0,0,0};
			colorText[] = {0.25,0.25,0.25,1};
			style = 1;
			shadow = 0;
			text = "Over9000";
			x = safeZoneX+safeZoneW - GRIDX(2.1);
			y = safeZoneY+GRIDY(0.1);
			w = GRIDX(2);
			h = GRIDY(0.8);
			sizeEx = GRIDY(0.8);
		};
	};
};
