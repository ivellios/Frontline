#define ORIGIN_X CENTER_X - SIZE_WIDTH/2
#define ORIGIN_Y safeZoneY + GRIDY(0.25)
#define SIZE_WIDTH 			GRIDX(12)
#define SIZE_HEIGHT 		GRIDY(1.5)

class SVAR(CaptureBar) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['frl_CaptureBar', _this select 0];";
    class Controls {
      class CtrlGroup : RscControlsGroupNoScrollbars {
          idc = 1000;
          x = ORIGIN_X;
          y = ORIGIN_Y;
          w = SIZE_WIDTH;
          h = SIZE_HEIGHT;

          class Controls {
              class SectorDescription : RscStructuredText {
                  idc = 1002;
                  shadow = 1;
                  x = 0;
                  y = GRIDY(0.25);
                  w = SIZE_WIDTH;
                  h = GRIDY(1.3);
                  text = "";
                  size = GRIDY(1);
                  sizeEx = GRIDY(1);
                  class Attributes {
                      font = FONTLIGHT;
                      color = "#ffffff";
                      align = "center";
                      shadow = 1;
                  };
              };

              class BackgroundProgress : RscText {
                  idc = 1003;
                  shadow = 0;
                  colorBackground[] = {1, 1, 1, 0.3};
                  x = 0;
                  y = 0;
                  w = SIZE_WIDTH;
                  h = GRIDY(0.2);
              };

              class Progress : RscProgress {
                  idc = 1004;
                  colorFrame[] = {0,0,0,0};
                  colorBar[] = {1, 1, 1, 1};
                  x = 0;
                  y = 0;
                  w = SIZE_WIDTH;
                  h = GRIDY(0.2);
              };
          };
      };
    };
};
