#define ORIGIN_X safeZoneX + GRIDX(0.25)
#define ORIGIN_Y safeZoneY + GRIDY(2)
#define SIZE_HEIGHT 		safeZoneH - GRIDY(2)

class SVAR(progressHUD) {
	idd = -1;
	duration = 1e11;
	onLoad = "uiNamespace setVariable [""frl_progressHUD"", _this select 0]";

	class ControlsBackground {
		class MainGroup: RscControlsGroupNoScrollbars {
			idc = 1;
			x = ORIGIN_X;
			y = ORIGIN_Y;
			w = SIZE_WIDTH_PROGRESS;
			h = SIZE_HEIGHT;
			class Controls { };
		};
	};
	class Controls { };
};
