#define _WIDTH_FULL PX(30)

class EGVAR(Spotting,spotDialog) {
    idd = -1;
    duration = 1e11;
    name = QEGVAR(Spotting,spotDialog);
    onLoad = "uiNamespace setVariable ['FRL_Spotting_spotDialog', _this select 0]; ['onLoad'] call FRL_Spotting_fnc_loadMenu";
    onUnLoad = "['onUnload'] call FRL_Spotting_fnc_loadMenu; uiNamespace setVariable ['FRL_Spotting_spotDialog', nil]; ";
    class Controls {
        class CtrlGroup : RscControlsGroupNoScrollbars {
            idc = 100;
            x = -3;
            y = -3;
            w = _WIDTH_FULL;
            h = PY(40.2);

            class Controls {
                class Header: RscText {
                    idc = 1;
                    colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"};
                    text = "INSERT MARKER";
                    x = PX(0);
                    y = PY(0);
                    w = _WIDTH_FULL;
                    h = PY(2);
                };

                class Background: RscText {
                    idc = 2;
                    colorBackground[] = {0, 0, 0, 0.6};
                    x = PX(0);
                    y = PY(2.2);
                    w = _WIDTH_FULL;
                    h = PY(37.9);
                };

                class MarkerTabs: RscToolbox {
                    access = 0;
                    color[] = {0, 0, 0, 0};
                    colorDisable[] = {0, 0, 0, 0};
                    colorSelect[] = {0.95,0.95,0.95,1};
                    colorSelectedBg[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])",0.5};
                    colorText[] = {0.95,0.95,0.95,1};
                    colorTextDisable[] = {0, 0, 0, 0};
                    colorTextSelect[] = {0.95,0.95,0.95,1};
                    columns = 2;
                    deletable = 0;
                    fade = 0;
                    font = "RobotoCondensed";
                    rows = 1;
                    shadow = 0;
                    sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 2)";
                    strings[] = {};
                    values[] = {};
                    style = 2;
                    idc = 3;
                    x = PX(0);
                    y = PY(2.2);
                    w = _WIDTH_FULL;
                    h = PY(4);
                };

                class MarkerList: RscListBox {
                    rowHeight = 0;
                    sizeEx = "((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.6)";
                    idc = 4;
                    x = PX(0);
                    y = PY(6.2);
                    w = _WIDTH_FULL;
                    h = PY(33.8);
                };
            };
        };
    };
};