class SVAR(ControlsHUD) {
  idd = -1;
  duration = 1e11;
  onLoad = "uiNamespace setVariable ['frl_ControlsHUD', _this select 0];";
  onUnLoad = "";
  class Controls {
    class CtrlGroup : RscControlsGroupNoScrollbars {
      idc = 1;
      x = safeZoneX + (safeZoneW * 0.7);
      y = CENTER_Y - GRIDY(2.5);
      w = (safeZoneW * 0.2);
      h = GRIDY(10);

      class Controls {
        class ControlsText : RscStructuredText {
            idc = 2;
            shadow = 1;
            x = 0;
            y = 0;
            w = (safeZoneW * 0.2);
            h = GRIDY(10);
            text = "";
            size = GRIDY(1);
            class Attributes {
              font = FONTMED;
              color = "#ffffff";
              align = "left";
              shadow = 1;
            };
          };
      };
    };
  };
};
