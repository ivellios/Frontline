#define _SIZE_SIDE_W PX(50)
#define _SIZE_SIDE_H PY(90)
#define _SIZE_UNIT_H PY(3)
#define _SIZE_UNIT_W PX(3)

#define _IDC_SCOREBOARD 20000
#define _IDC_MOD_SQUAD 100
#define _IDC_MOD_UNIT 1

class EGVAR(scoring,scoreboard) {
    idd = 1005;
    onLoad = "['frl_scoring_scoreboardLoad', _this] call CLib_fnc_localEvent;";
    onUnload = "'frl_scoring_scoreboardUnload' call CLib_fnc_localEvent;";
    movingEnable = 1;

    class EGVAR(scoring,sideEntryBase) : RscBackground {
        text = "";
        colorBackground[] = {0,0,0,0.4};
        x = 0;
        y = 0;
        w = _SIZE_SIDE_W;
        h = _SIZE_SIDE_H;
    };

    class EGVAR(Scoring,UnitEntryBase): RscControlsGroupNoScrollbars  {
        x = 0;
        y = 0;
        w = _SIZE_SIDE_W;
        h = 0;

        class Controls {
            class RoleIcon: RscPicture {
                idc = 60;
                text = "";
                x = 0;
                y = 0;
                w = _SIZE_UNIT_W;
                h = _SIZE_UNIT_H;
                sizeEx = PY(2.5);
            };

            class UnitName: RscText {
                font = FONTLIGHT;
                idc = 61;

                text = "";
                x = PX(3);
                y = 0;
                w = _SIZE_SIDE_W - PX(3) - PX(22);
                h = _SIZE_UNIT_H;
                sizeEx = PY(2.5);
            };

            class Column1: RscText {
                font = FONTLIGHT;
                idc = 62;

                text = "0";
                tooltip = "Kills";
                x = _SIZE_SIDE_W - PX(19);
                //colorBackground[] = {1, 1, 1, 0.1};
                y = 0;
                w = PX(3);
                h = _SIZE_UNIT_H;
                sizeEx = PY(2.5);
            };

            class Column2: Column1 {
                idc = 63;
                tooltip = "Incaps";
                x = _SIZE_SIDE_W - PX(16);
            };

            class Column3: Column1 {
                idc = 64;
                tooltip = "AI Kills";
                x = _SIZE_SIDE_W - PX(13);
            };

            class Column4: Column1 {
                idc = 65;
                x = _SIZE_SIDE_W - PX(10);
                tooltip = "Deaths";
                //text = "";
            };

            class Column5: Column1 {
                idc = 66;
                font = FONTMED;
                tooltip = "Score";
                x = _SIZE_SIDE_W - PX(9);
                w = PX(7);
                style = 1;
            };
        };
    };

    #define UNITENTRY(var1) class UnitEntry_##var1 : EGVAR(Scoring,UnitEntryBase) {\
        idc = var1;\
        y = 0;\
    }

    class EGVAR(scoring,squadEntryBase): RscControlsGroupNoScrollbars {
        idc = 0;
        x = 0;
        y = 0;
        w = _SIZE_SIDE_W;
        h = _SIZE_UNIT_H * 11; // CHANGE TO 0


        class Controls {
            class squadBackground: RscBackground {
                idc = 49;
                x = 0;
                y = 0;
                h = 0;
                w = _SIZE_SIDE_W;
                colorBackground[] = {1, 1, 1, 0.15};
            };

            class SquadHeader1: RscBackground {
                idc = 50;
                x = 0;
                y = 0;
                w = _SIZE_SIDE_W;
                h = PY(0.2);
                colorBackground[] = {0, 0, 0, 0.8};
            };
            class SquadHeader2: SquadHeader1 {
                idc = 51;
                y = PY(0.2);
                h = _SIZE_UNIT_H - PY(0.2);
                colorBackground[] = {0, 0, 0, 0.8};
            };

            class SquadDesignator: RscText {
                font = FONTMED;
                idc = 52;
                x = 0;
                y = 0;
                h = _SIZE_UNIT_H;
                w = PX(3);
                sizeEx = PY(2.5);
                text = "A";
                tooltip = "Squad Designator";
            };

            class SquadDescription: SquadDesignator {
                idc = 53;
                text = "ultraleetsquad";
                x = PX(3);
                w = PX(19);
                tooltip = "Squad Name";
            };

            class SquadType: SquadDesignator {
                idc = 54;
                x = PX(22);
                w = PX(16);
                text = "Infantry A";
                tooltip = "Squad Type";
            };

            class SquadScore: SquadDesignator {
                idc = 55;
                w = PX(7);
                x = _SIZE_SIDE_W - PX(9);
                text = "9001";
                tooltip = "Average score in squad";
                style = 1;
            };


            UNITENTRY(1);
            UNITENTRY(2);
            UNITENTRY(3);
            UNITENTRY(4);
            UNITENTRY(5);
            UNITENTRY(6);
            UNITENTRY(7);
            UNITENTRY(8);
            UNITENTRY(9);
            UNITENTRY(10);
        };
    };

    #define SQUADENTRY(var1) class SquadEntry_##var1 : EGVAR(Scoring,squadEntryBase) {\
        idc = (var1 * 100);\
        y = ((_SIZE_UNIT_H * 11) * (var1 - 1));\
    };

    class ControlsBackground {
      /*class FrlBackground: RscBackground {
  			x = safeZoneX;
  			y = safeZoneY;
  			w = safeZoneW;
  			h = safeZoneH;
        colorBackground[] = {0.1,0.1,0.1,0};
  			fade = 1;
  			onLoad = "(_this select 0) ctrlSetFade 1; (_this select 0) ctrlCommit 0; (_this select 0) ctrlSetBackgroundColor [0.1,0.1,0.1,0.3]; (_this select 0) ctrlSetFade 0; (_this select 0) ctrlCommit 0.5;";
  		};*/
    };
    class Controls {
        class Scoreboard: RscControlsGroupNoHScrollbars {
            idc = _IDC_SCOREBOARD;
            x = CENTER_X - PX(50);
            y = CENTER_Y - PY(45);
            w = (2 * _SIZE_SIDE_W);
            h = (_SIZE_SIDE_H);

            class ControlsBackground { };
            class Controls {
                SQUADENTRY(1);
                SQUADENTRY(2);
                SQUADENTRY(3);
                SQUADENTRY(4);
                SQUADENTRY(5);
                SQUADENTRY(6);
                SQUADENTRY(7);
                SQUADENTRY(8);
                SQUADENTRY(9);
                SQUADENTRY(10);

                SQUADENTRY(11);
                SQUADENTRY(12);
                SQUADENTRY(13);
                SQUADENTRY(14);
                SQUADENTRY(15);
                SQUADENTRY(16);
                SQUADENTRY(17);
                SQUADENTRY(18);
                SQUADENTRY(19);
                SQUADENTRY(20);
            };
        };
    };
};
