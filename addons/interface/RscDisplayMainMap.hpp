/*
Adds extra windows to the map
*/

class RscDisplayMainMap {
    class controls {
        class SVAR(Menu): RscControlsGroupNoScrollbars {
            idc = 51001;
            x = __X;
            y = __Y;
            w = __W;
            h = __H;
            onLoad = "uiNamespace setVariable ['FRL_main_mapInfoHUD', (_this select 0)];";

            class Controls {
                // -- Contents
                class SVAR(Contents): RscControlsGroupNoHScrollbars {
                    idc = 51005;
                    x = 0;
                    y = 0;
                    w = __W;
                    h = __H_CONTENTS;
                    class Controls {
                        class Background: ctrlStaticBackground {
                            colorBackground[] = {0, 0, 0, 0.5};
                            idc = 51006;
                            x = 0;
                            y = 0;
                            w = __W;
                            h = __H_CONTENTS;
                        };


                        class TestSetting: RscControlsGroupNoScrollbars {
                            x = 0;
                            y = 0;
                            w = __W_SETTING;
                            h = __H_SETTING;
                            idc = 51022;

                            class Controls {
                                class Title: RscText {
                                    idc = 51031;
                                    x = PX(__PADDING);
                                    y = PY(2);
                                    w = PX(10);
                                    h = PY(2.2);
                                    text = "RP RANGE";
                                    sizeEx = PY(2.2);
                                };

                                class Button: RscButton {
                                    idc = 51032;
                                    x = PX(17);
                                    y = PY(2);
                                    w = PX(14);
                                    h = PY(2.2);
                                    text = "SHOW";
                                    action = "['rally', 20, false] call FRL_Rolesspawns_fnc_spawnShowRanges;";
                                };
                            };
                        };
                        class TestSetting2: TestSetting {
                            y = 0 + __H_SETTING;
                            idc = 51023;
                            class Controls: Controls {
                                class Title: Title {
                                    text = "FO RANGE";
                                };

                                class Button: Button {
                                //onLoad = "if !(missionNamespace getVariable ['frl_spawnpoints_initDone', false]) then { (_this select 0) ctrlEnable false };";
                                    action = "['fo', 20, false] call FRL_Rolesspawns_fnc_spawnShowRanges;";
                                };
                            };
                        };
                        class StructuredText: RscStructuredText {
                            idc = 51007;
                            style = 2;
                            x = PX(__PADDING);
                            y = 0 + (2 * __H_SETTING) + PY(__PADDING);
                            w = __W - (2 * PX(__PADDING));
                            h = __H_CONTENTS  - (2 * PY(__PADDING)) - (2 * __H_SETTING);
                        };
                    };
                };
            };
        };
    };

    // -- Doesn't do anything....
    // class Objects {
    // 	class Compass: RscObject {
    // 		scale = 0.5;
    // 		zoomDuration = 0.3;
    // 	};
    // };
};
