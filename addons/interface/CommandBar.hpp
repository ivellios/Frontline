class CommandBar {
  elementSpacing = 0.05;
  colorRedTeam[] = {1, 0, 0, 1};
  colorBlueTeam[] = {0, 0, 1, 1};
  colorGreenTeam[] = {0.259, 0.463, 0.149, 1};
  colorYellowTeam[] = {0.8, 0.8, 0, 1};
  colorWhiteTeam[] = {0.95, 0.95, 0.95, 1};
  colorText[] = {1, 1, 1, 1};
  colorIDNone[] = {0, 0, 0, 1};
  colorIDNormal[] = {0.8, 0.8, 0.8, 1};
  colorIDSelected[] = {0, 0.6, 0, 1};
  colorIDSelecting[] = {0.8, 0.8, 0.8, 1};
  colorIDPlayer[] = {0.8, 0.6, 0, 1};
  class prevPage {
    color[] = {0.259, 0.463, 0.149, 1};
    shadow = 0;
    x = "0 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
    y = "0 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
    w = "0.5 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
    h = "0 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
    texture = "A3\ui_f\data\igui\cfg\commandbar\prevPage_ca.paa";
  };
  class nextPage {
    color[] = {0.259, 0.463, 0.149, 1};
    shadow = 0;
    x = "0 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
    y = "0 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
    w = "0.5 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
    h = "0 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
    texture = "A3\ui_f\data\igui\cfg\commandbar\nextPage_ca.paa";
  };
  class UnitInfo {
    w = 0.976;
    h = 0.105/2;
    class GroupIcon {
      shadow = 0;
      color[] = {1, 1, 1, 1};
      x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
      y = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
      w = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
      h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
    };
    class UnitBackground {
      x = 0;
      y = 0;
      w = 0.028;
      h = 0.04;
      textureNormal = "#(argb,8,8,3)color(1,1,1,1)";
      textureSelected = "#(argb,8,8,3)color(1,1,1,1)";
      texturePlayer = "#(argb,8,8,3)color(1,1,1,1)";
      textureFocus = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 0;
    };
    class UnitFocus {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 0;
    };
    class UnitIcon {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      color[] = {1, 1, 1, 1};
      colorPlayer[] = {1, 1, 1, 1};
      colorNoAmmo[] = {0.8, 0.4, 0.5, 1};
      colorWounded[] = {0.8, 0, 0, 1};
      colorNoFuel[] = {0.8, 0.7, 0, 1};
      colorWoundedFade[] = {0, 0, 0, 1};
      colorFocus[] = {0, 0, 0, 1};
      colorSelected[] = {0, 0, 0, 1};
      colorNormal[] = {0, 0, 0, 1};
      colorDamaged[] = {1, 0, 0, 1};
      shadow = 0;
    };
    class UnitVehicleStatus {
      x = 0;
      y = 0;
      w = 0.028;
      h = 0.04;
      texture = "#(argb,8,8,3)color(0,0,0,1)";
    };
    class Semaphore {
      x = 0;
      y = 0;
      w = 0.04;
      h = 0.03;
      color[] = {0.8, 0, 0, 1};
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 0;
    };
    class CommandBackground {
      color[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])", "(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"};
      shadow = 0;
      texture = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\commandBackground_ca.paa";
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class HoldFire {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      color[] = {1, 1, 1, 1};
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 0;
    };
    class OrderBackground {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      color[] = {1, 1, 1, 1};
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 0;
    };
    class OrderText {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      colorText[] = {1, 1, 1, 1};
      font = "TahomaB";
      SizeEx = 0.05;
      shadow = 0;
    };
    class VehicleBackground {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      color[] = {1, 1, 1, 1};
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 0;
    };
    class VehicleText {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      colorText[] = {1, 1, 1, 1};
      font = "TahomaB";
      SizeEx = 0.05;
      shadow = 0;
    };
    class UnitSpecialRole {
      color[] = {1, 1, 1, 1};
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 2;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class UnitRole {
      color[] = {1, 1, 1, 1};
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      shadow = 2;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class UnitNumberBackground {
      color[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])", "(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"};
      shadow = 0;
      texture = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitNumberBackground_ca.paa";
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class UnitNumberText {
      text = "";
      font = FONTMED;
      shadow = 2;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      colorText[] = {1, 1, 1, 1};
      sizeEx = 0;
    };
    class CommandText {
      color[] = {1, 1, 1, 1};
      font = FONTMED;
      shadow = 1;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      colorText[] = {1, 1, 1, 1};
      sizeEx = 0;
    };
    class VehicleNumberText {
      color[] = {1, 1, 1, 1};
      font = FONTMED;
      shadow = 2;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      colorText[] = {1, 1, 1, 1};
      sizeEx = 0;
    };
    class VehicleNumberBackground {
      color[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])", "(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"};
      shadow = 0;
      texture = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\vehicleNumberBackground_ca.paa";
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class CombatMode {
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      textureMCareless = "#(argb,8,8,3)color(1,1,1,1)";
      textureMSafe = "#(argb,8,8,3)color(0,1,0,1)";
      textureMAware = "#(argb,8,8,3)color(1,1,0,1)";
      textureMCombat = "#(argb,8,8,3)color(1,0,0,1)";
      textureMStealth = "#(argb,8,8,3)color(0,0,0,1)";
      shadow = 0;
    };
    class VehicleStatus {
      color[] = {0, 0, 0, 0};
      shadow = 0;
      colorDamaged[] = {"(profilenamespace getvariable ['IGUI_ERROR_RGB_R',0.8])", "(profilenamespace getvariable ['IGUI_ERROR_RGB_G',0.0])", "(profilenamespace getvariable ['IGUI_ERROR_RGB_B',0.0])", 0.8};
      colorNoAmmo[] = {"(profilenamespace getvariable ['IGUI_WARNING_RGB_R',0.8])", "(profilenamespace getvariable ['IGUI_WARNING_RGB_G',0.5])", "(profilenamespace getvariable ['IGUI_WARNING_RGB_B',0.0])", 0.6};
      colorNoFuel[] = {"(profilenamespace getvariable ['IGUI_WARNING_RGB_R',0.8])", "(profilenamespace getvariable ['IGUI_WARNING_RGB_G',0.5])", "(profilenamespace getvariable ['IGUI_WARNING_RGB_B',0.0])", 0.6};
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class UnitCombatMode {
      textureMCareless = "#(argb,8,8,3)color(1,1,1,1)";
      textureMSafe = "#(argb,8,8,3)color(0,1,0,1)";
      textureMAware = "#(argb,8,8,3)color(1,1,0,1)";
      textureMCombat = "#(argb,8,8,3)color(1,0,0,1)";
      textureMStealth = "#(argb,8,8,3)color(0,0,0,1)";
      texture = "#(argb,8,8,3)color(0,0,0,1)";
      colorBlue[] = {1, 0, 0, 0.5};
      colorGreen[] = {1, 0, 0, 0.7};
      colorWhite[] = {1, 0, 0, 0.7};
      colorYellow[] = {0, 0, 0, 0};
      colorRed[] = {0, 0, 0, 0};
      shadow = 0;
      class UnitCombatMode {
        textureBlue = "#(argb,8,8,3)color(0,0,1,1)";
        textureWhite = "#(argb,8,8,3)color(1,1,1,1)";
        textureYellow = "#(argb,8,8,3)color(1,1,0,1)";
        textureRed = "#(argb,8,8,3)color(1,0,0,1)";
        textureGreen = "#(argb,8,8,3)color(0,1,0,1)";
      };
      textureBlue = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitCombatMode_ca.paa";
      textureGreen = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitCombatMode_ca.paa";
      textureWhite = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitCombatMode_ca.paa";
      textureYellow = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitCombatMode_ca.paa";
      textureRed = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitCombatMode_ca.paa";
      text = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitCombatMode_ca.paa";
      x = 0;
      y = 0;
      w = 0;
      h = 0;
      colorText[] = {1, 0, 0, 1};
    };
    class UnitNameBackground {
      colorBlue[] = {0, 0, 0, 0};
      colorGreen[] = {0, 0, 0, 0};
      colorWhite[] = {0, 0, 0, 0};
      colorYellow[] = {0, 0, 0, 0};
      colorRed[] = {0, 0, 0, 0};
      shadow = 0;
      textureNormal = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitNameBackground_normal_ca.paa";
      texturePlayer = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitNameBackground_normal_ca.paa";
      textureSelected = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitNameBackground_selected_ca.paa";
      textureFocus = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitNameBackground_selected_ca.paa";
      colorNormal[] = {1, 1, 1, 0.2};
      colorPlayer[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])", 0.4};
      colorFocus[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])", "(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])", 0.8};
      colorSelected[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])", 0.8};
      x = "0 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
      y = "0 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
      w = "3 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
      h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
      colorText[] = {1, 1, 1, 0.2};
    };
    class UnitName {
      colorText[] = {1, 1, 1, 1};
      font = FONTMED;
      shadow = 1;
      x = "0 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
      y = "0.1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
      w = "3 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
      h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
      sizeEx = "0.8 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
    };
    class UnitBehavior {
      /*textureMAware = "#(argb,8,8,3)color(1,1,0,1)";
      colorCareless[] = {0, 0, 0, 0};
      colorSafe[] = {0, 0, 0, 0};
      colorAware[] = {0, 0, 0, 0};
      colorCombat[] = {1, 0.25, 0, 0.7};
      colorStealth[] = {0, 0.8, 1, 0.7};
      shadow = 0;
      textureMCareless = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitBehavior_ca.paa";
      textureMSafe = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitBehavior_ca.paa";
      textureMCombat = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitBehavior_ca.paa";
      textureMYellow = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitBehavior_ca.paa";
      textureMStealth = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitBehavior_ca.paa";
      text = "\a3\Ui_f\data\IGUI\Cfg\CommandBar\unitBehavior_ca.paa";*/
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class VehicleRole {
      color[] = {0, 0, 0, 0.5};
      shadow = 0;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class VehicleIcon {
      color[] = {1, 1, 1, 1};
      shadow = 2;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class UnitRank {
      color[] = {0, 0, 0, 0.5};
      shadow = 0;
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    class UnitStatus {
      /*color[] = {0, 0, 0, 0};
      shadow = 0;
      colorWounded[] = {"(profilenamespace getvariable ['IGUI_ERROR_RGB_R',0.8])", "(profilenamespace getvariable ['IGUI_ERROR_RGB_G',0.0])", "(profilenamespace getvariable ['IGUI_ERROR_RGB_B',0.0])", 0.8};
      colorWoundedFade[] = {"(profilenamespace getvariable ['IGUI_ERROR_RGB_R',0.8])", "(profilenamespace getvariable ['IGUI_ERROR_RGB_G',0.0])", "(profilenamespace getvariable ['IGUI_ERROR_RGB_B',0.0])", 1};
      colorNoAmmo[] = {"(profilenamespace getvariable ['IGUI_WARNING_RGB_R',0.8])", "(profilenamespace getvariable ['IGUI_WARNING_RGB_G',0.5])", "(profilenamespace getvariable ['IGUI_WARNING_RGB_B',0.0])", 0.8};
      texture = "#(argb,8,8,3)color(1,1,1,1)";*/
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };
    font = FONTMED;
  };
  left = "(safezoneX + 0.1 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
  top = "(safezoneY + safezoneH - 1.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
  width = "(36 * (((safezoneW / safezoneH) min 1.2) / 40))";
  height = "(2 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
  imageDefaultWeapons = "A3\ui_f\data\igui\cfg\commandbar\imageDefaultWeapons_ca.paa";
  imageNoWeapons = "A3\ui_f\data\igui\cfg\commandbar\imageNoWeapons_ca.paa";
  imageCommander = "A3\ui_f\data\igui\cfg\commandbar\imageCommander_ca.paa";
  imageDriver = "A3\ui_f\data\igui\cfg\commandbar\imageDriver_ca.paa";
  imageGunner = "A3\ui_f\data\igui\cfg\commandbar\imageGunner_ca.paa";
  imageCargo = "A3\ui_f\data\igui\cfg\commandbar\imageCargo_ca.paa";
  dimm = 0.3;
};
