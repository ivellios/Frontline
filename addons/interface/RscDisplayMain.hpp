/*
	Adds Frontline versioning to main menu
*/

class RscDisplayMain: RscStandardDisplay {
	class controls {
		#define VERSIONINFO_W 120
		#define VERSIONINFO_H 40
		class FRL_VersionInfo: RscHTML {
			idc = 49001;
	        x = safeZoneX + (GRID_W * 5);
	        y = safeZoneY + safeZoneH - (GRID_H * (20 + VERSIONINFO_H));
	        w = GRID_W * VERSIONINFO_W;
	        h = GRID_H * VERSIONINFO_H;
	        onLoad = "['menu', [(_this select 0)]] call (uiNamespace getVariable 'FRL_main_fnc_versionCheck')";
	        colorBackground[] = {0, 0, 0, 0};
			class H1 {
				font = FONTLIGHT_ALT;
				fontBold = FONTBOLD_ALT;
				sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.3)";
				align = "left";
			};
			class H2 {
				font = FONTLIGHT_ALT;
				fontBold = FONTBOLD_ALT;
				sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.3)";
				align = "left";
				colorText[] = {1, 0, 0, 1};
			};
			class P {
				font = FONTMED;
				fontBold = FONTBOLD;
				sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
				align = "left";
			};
		};
		class FRL_ApexCheck: FRL_VersionInfo {
				x = -1;
				y = -1;
				w = 0.1;
				h = 0.1;
				idc = 49002;
				onLoad = "['apexcheck', [(_this select 0)]] call (uiNamespace getVariable 'FRL_main_fnc_versionCheck')";
		};
	};
};
