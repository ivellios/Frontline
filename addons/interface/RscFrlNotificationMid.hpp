class EGVAR(Main,NotificationMid) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['frl_main_notificationmid', _this select 0];";
    fadeIn = 0;
    fadeOut = 0;
    class Controls {
        class CtrlGroup : RscControlsGroupNoScrollbars {
            idc = 1;
            x = CENTER_X - (FRL_MAIN_NOTIFICATION_W / 2);
            y = (safeZoneY + (safeZoneH * 0.8));
            w = FRL_MAIN_NOTIFICATION_W;
            h = 0;

            class Controls {
                class Background: RscBackground {
                    idc = 2;
                    colorBackground[] = {0, 0, 0, 0.3};
                    x = 0;
                    y = 0;
                    w = FRL_MAIN_NOTIFICATION_W_MAX;
                    h = FRL_MAIN_NOTIFICATION_H;
                };

                class MainText: RscText {
                    idc = 3;
                    text = "";
                    shadow = 0;
                    style = 2;
                    x = 0;
                    y = 0;
                    w = FRL_MAIN_NOTIFICATION_W_MAX;
                    h = FRL_MAIN_NOTIFICATION_H;
                };
            };
        };
    };
};
