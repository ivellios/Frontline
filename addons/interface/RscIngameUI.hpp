#define SIZE_WIDTH 			GRIDX(12.85)
#define SIZE_HEIGHT 		GRIDY(2)
#define SIZE_HEIGHT_VEH 	GRIDY(5)
#define SIZE_HEIGHT_VEH_WPN GRIDY(3.65)
#define SIZE_WIDTH_VEH 			GRIDX(14)

#define SPACING_X			GRIDX(0.5)
#define SPACING_Y			GRIDY(0.9)

#define ORIGIN_X safeZoneX + safeZoneW - SIZE_WIDTH - SPACING_X
#define ORIGIN_Y safeZoneY + safeZoneH - SIZE_HEIGHT - SPACING_Y
#define ORIGIN_X_VEH safeZoneX + safeZoneW - SIZE_WIDTH_VEH - SPACING_X
#define ORIGIN_Y_VEH safeZoneY + safeZoneH - SIZE_HEIGHT_VEH - GRIDY(0.4)

class RscInGameUI {
    class RscUnitInfo {
        //onLoad = "[""onLoad"",_this,""FRL_RscUnitInfo"",'FRL_IGUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
        //onUnload = "[""onLoad"",_this,""FRL_RscUnitInfo"",'FRL_IGUI'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
        class WeaponInfoControlsGroupLeft: RscControlsGroup {
            idc = 2302;
            x = ORIGIN_X;
            y = ORIGIN_Y;
            w = SIZE_WIDTH;
            h = SIZE_HEIGHT;
            testrandomshit = 1;

            class VScrollbar: VScrollbar { width = 0; };
            class HScrollbar: HScrollbar { height = 0; };
            class controls {
                delete CA_BackgroundWeapon;
                delete CA_BackgroundWeaponTitleDark;
                delete CA_Mode;
                delete CA_GunnerWeapon;
                delete CA_Weapon;

                #define ORIGIN_X_WPN 0
                #define ORIGIN_Y_WPN 0

                class CA_BackgroundWeaponTitle: RscText {
                    font = FONTMED;
                    colorBackground[] = COLOR_BACKGROUND;
                    idc = 52000;
                    text = "";
                    x = ORIGIN_X_WPN + GRIDX(0);
                    y = ORIGIN_Y_WPN + GRIDY(0);
                    w = 0; //SIZE_WIDTH;
                    h = 0; //SIZE_HEIGHT;
                };

                class CA_BackgroundWeaponMode: RscPicture {
                    colorText[] = {1, 1, 1, 0.2};
                    idc = 1203;
                    text = ""; //"\A3\ui_f\data\igui\rscingameui\rscunitinfo\mode_background_ca.paa";
                    x = ORIGIN_X_WPN + GRIDX(0.22);
                    y = ORIGIN_Y_WPN + GRIDY(1.8);
                    w = SIZE_WIDTH;
                    h = GRIDY(0.15);
                };

                // -- Needed for vehicles and more shit
                class CA_AmmoType: RscText {
                    font = FONTMED;
                    idc = 155;
                    x = ORIGIN_X_WPN + GRIDX(2.2);
                    y = ORIGIN_Y_WPN + GRIDY(0.1);
                    w = GRIDX(4.0);
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.8);
                };

                // -- We keep this, but make it invisible so we can use ctrlText on it and use that data to set our own ammo count
                class CA_AmmoCount: RscText {
                    font = FONTBOLD_ALT;
                    idc = 184;
                    /*x = ORIGIN_X_WPN + GRIDX(0);
                    y = ORIGIN_Y_WPN + GRIDY(0);
                    w = GRIDX(0);
                    h = GRIDY(0);*/
                    x = ORIGIN_X_WPN + GRIDX(8.9);
                    y = ORIGIN_Y_WPN + GRIDY(0.2);
                    w = GRIDX(2.4);
                    h = GRIDY(1.8);
                    style = 2;
                    //sizeEx = GRIDY(0);
                    colorText[] = {1, 1, 1, 0};
                };

                class CA_ValueReload: RscIGProgress {
                    idc = 154;
                    colorBar[] = {0, 1, 1, 0.25};
                    colorFrame[] = {0, 0, 0, 0};
                    style = 0;
                    x = ORIGIN_X_WPN + GRIDX(0.4);
                    y = ORIGIN_Y_WPN + GRIDY(0.2);
                    w = GRIDX(8.45);
                    h = GRIDY(0.15);
                };

                class CA_ModeTexture: RscPicture {
                    idc = 187;
                    colorText[] = COLOR_TEXT;
                    x = ORIGIN_X_WPN + GRIDX(0.22);
                    y = ORIGIN_Y_WPN + GRIDY(1.8);
                    w = SIZE_WIDTH;
                    h = GRIDY(0.15);
                };

                /*class FRL_RoundIndicatorBackground: RscPicture {
                    colorText[] = {1, 1, 1, 0.3};
                    idc = 52001;
                    text = "\pr\frl\addons\client\ui\media\unitinfo\round_0.paa";
                    x = ORIGIN_X_WPN + GRIDX(9.1);
                    y = ORIGIN_Y_WPN + GRIDY(0.2);
                    w = GRIDX(1.8);
                    h = GRIDY(1.8);
                };

                class FRL_RoundIndicatorForeground: FRL_RoundIndicatorBackground {
                    colorText[] = {1, 1, 1, 1};
                    text = "\pr\frl\addons\client\ui\media\unitinfo\round_0.paa";
                    idc = 52002;
                };*/

                /*class FRL_RoundIndicatorText: RscText {
                    font = FONTMED;
                    colorText[] = COLOR_TEXT;
                    idc = 52003;
                    x = ORIGIN_X_WPN + GRIDX(11.25);
                    y = ORIGIN_Y_WPN + GRIDY(0.35);
                    w = GRIDX(2);
                    h = GRIDY(1.5);
                    sizeEx = GRIDY(1.5);
                    style = 2;
                };*/

                class CA_MagCount: RscText {
                    font = FONTLIGHT_ALT;
                    colorText[] = COLOR_TEXT;
                    idc = 185;
                    x = ORIGIN_X_WPN + GRIDX(11);
                    y = ORIGIN_Y_WPN + GRIDY(0.35);
                    w = GRIDX(2);
                    h = GRIDY(1.5);
                    sizeEx = GRIDY(1.5);
                };

                class CA_GrenadeType: RscText {
                    font = FONTMED;
                    colorText[] = COLOR_TEXT;
                    style = 0;
                    idc = 152;
                    x = ORIGIN_X_WPN + GRIDX(0);
                    y = ORIGIN_Y_WPN + GRIDY(0.9);
                    w = GRIDX(6);
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.8);
                };

                class CA_GrenadeCount: RscText {
                    font = FONTMED;
                    colorText[] = COLOR_TEXT;
                    style = 1;
                    idc = 151;
                    x = ORIGIN_X_WPN + GRIDX(6.25);
                    y = ORIGIN_Y_WPN + GRIDY(0.9);
                    w = GRIDX(1);
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.8);
                };

                class CA_BackgroundFuel: RscPicture {
                    x = 0;
                    y = 0;
                    w = 0;
                    h = 0;
                    colorText[] = {0, 0, 0, 0};
                };
            };
        };


        // -- Vehicle weapons info
        class WeaponInfoControlsGroupRight: RscControlsGroup {
            idc = 2303;
            class VScrollbar: VScrollbar { width = 0;	};
            class HScrollbar: HScrollbar { height = 0; };

            #undef ORIGIN_Y_WPN
            #define ORIGIN_Y_WPN GRIDY(0.9)

            x = ORIGIN_X_VEH;
            y = ORIGIN_Y_VEH;
            w = SIZE_WIDTH_VEH;
            h = SIZE_HEIGHT_VEH_WPN;

            class controls {
                delete CA_BackgroundWeaponTitleDark;
                delete CA_BackgroundWeaponTitle;
                delete CA_Mode;
                delete CA_BackgroundWeapon;
                /*class CA_BackgroundWeapon: RscText {
                    font = FONTMED;
                    colorBackground[] = COLOR_BACKGROUND;
                    idc = 51999;
                    text = "";
                    x = GRIDX(0);
                    y = GRIDY(0);
                    w = SIZE_WIDTH_VEH;
                    h = GRIDY(1.2);
                };*/

                class CA_Weapon: RscText {
                    font = FONTMED;
                    idc = 118;
                    colorText[] = COLOR_TEXT;
                    style = 1;
                    x = GRIDX(0);
                    y = GRIDY(0.2);
                    w = SIZE_WIDTH_VEH;
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.9);
                };

                delete CA_GunnerWeapon;
                /*class CA_GunnerWeapon: RscText {
                    font = FONTMED;
                    idc = 150;
                    colorBackground[] = {0, 0, 0, 0};
                    colorText[] = COLOR_TEXT;
                    style = 1;
                    x = GRIDX(0);
                    y = GRIDY(0.2);
                    w = SIZE_WIDTH;
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.8);
                };*/

                class CA_BackgroundWeaponMode: RscText {
                    colorBackground[] = {1, 1, 1, 0.2};
                    idc = 1203;
                    text = ""; //"\A3\ui_f\data\igui\rscingameui\rscunitinfo\mode_background_ca.paa";
                    x = GRIDX(0.18);
                    y = GRIDY(1.2);
                    w = SIZE_WIDTH_VEH;
                    h = GRIDY(0.2);
                };

                // -- Needed for vehicles and more shit
                class CA_AmmoType: RscText {
                    font = FONTMED;
                    idc = 155;
                    style = 0;
                    x = GRIDX(0.1);
                    y = GRIDY(1.7);
                    w = SIZE_WIDTH_VEH;
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.8);
                };

                class CA_AmmoCount: RscText {
                    font = FONTBOLD_ALT;
                    idc = 184;
                    style = 0;
                    x = GRIDX(0);
                    y = GRIDY(0.15);
                    w = GRIDX(2.7);
                    h = GRIDY(1);
                    sizeEx = GRIDY(1.2);
                    colorText[] = COLOR_TEXT;
                };

                class CA_MagCount: RscText {
                    font = FONTLIGHT_ALT;
                    colorText[] = COLOR_TEXT;
                    idc = 185;
                    style = 1;
                    x = GRIDX(2.5);
                    y = GRIDY(0.15);
                    w = GRIDX(2);
                    h = GRIDY(1);
                    sizeEx = GRIDY(1.2);
                };

                class CA_ValueReload: RscIGProgress {
                    idc = 154;
                    colorBar[] = {1, 1, 1, 0.25};
                    colorFrame[] = {0, 0, 0, 0};
                    style = 0;
                    x = GRIDX(0.18);
                    y = GRIDY(1.2);
                    w = SIZE_WIDTH_VEH;
                    h = GRIDY(0.2);
                };

                class CA_ModeTexture: RscPicture {
                    idc = 187;
                    colorText[] = COLOR_TEXT;
                    x = GRIDX(0.18);
                    y = GRIDY(1.2);
                    w = SIZE_WIDTH_VEH;
                    h = GRIDY(0.2);
                };

                // -- Countermeasures / smoke
                class CA_GrenadeType: RscText {
                    font = FONTMED;
                    colorText[] = COLOR_TEXT;
                    style = 0;
                    idc = 152;
                    x = GRIDX(0);
                    y = GRIDY(2.5);
                    w = GRIDX(3.3);
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.8);
                };

                class CA_GrenadeCount: RscText {
                    font = FONTMED;
                    colorText[] = COLOR_TEXT;
                    style = 0;
                    idc = 151;
                    x = GRIDX(3.8);
                    y = GRIDY(2.5);
                    w = GRIDX(3);
                    h = GRIDY(0.8);
                    sizeEx = GRIDY(0.8);
                };
            };
        };

        // -- Vehicles zeroing
        class CA_Zeroing: RscText {
            font = FONTBOLD;
            colorText[] = COLOR_TEXT;
            style = 1;
            x = ORIGIN_X_VEH + GRIDX(4.50);
            y = ORIGIN_Y_VEH + GRIDY(1.65);
            w = GRIDX(2.2);
            h = GRIDY(0.8);
            sizeEx = GRIDY(0.8);
        };

        class CA_BackgroundVehicleTitle: RscText {
            font = FONTMED;
            colorBackground[] = {0.5, 0.5, 0.5, 0};
            idc = 53000;
            x = ORIGIN_X_VEH + GRIDX(0);
            y = ORIGIN_Y_VEH + GRIDY(1.45);
            w = GRIDX(0);
            h = GRIDY(0);
        };

        class CA_VehicleTogglesBackground: RscText {
            font = FONTMED;
            colorBackground[] = {1,1,1,0};
            x = ORIGIN_X_VEH + GRIDX(0);
            y = ORIGIN_Y_VEH + GRIDY(1.8);
            w = SIZE_WIDTH_VEH - 2*GRIDX(1.35);
            h = GRIDY(1.35);
        };

        class CA_VehicleToggles: RscVehicleToggles {
            idc = 112;
            x = ORIGIN_X_VEH + GRIDX(6);
            y = ORIGIN_Y_VEH + GRIDY(1.9);
            w = SIZE_WIDTH_VEH - GRIDX(2.7);
            h = GRIDY(1.15);
            xSpace = GRIDX(0.05);
            iconW = GRIDX(1);
            iconH = GRIDY(1);
        };

        class CA_BackgroundFuel: RscPicture {
            x = 0;
            y = 0;
            w = 0;
            h = 0;
            colorText[] = {0, 0, 0, 0};
        };

        class CA_ValueFuelBackground: RscText {
            font = FONTMED;
            colorBackground[] = {1,1,1,0.3};
            x = ORIGIN_X_VEH + GRIDX(0.3);
            y = ORIGIN_Y_VEH + GRIDY(3.45);
            w = SIZE_WIDTH_VEH - GRIDX(0.3);
            h = GRIDY(0.2);
        };

        class CA_ValueFuel: RscProgress {
            idc = 113;
            texture = "#(argb,8,8,3)color(1,1,1,1)";
            text = "#(argb,8,8,3)color(1,1,1,1)";
            x = ORIGIN_X_VEH + GRIDX(0.3);
            y = ORIGIN_Y_VEH + GRIDY(3.45);
            w = SIZE_WIDTH_VEH - GRIDX(0.3);
            h = GRIDY(0.2);
        };

        class CA_HitZones: RscHitZones {
            idc = 111;
            xCount = 6;
            yCount = 1;
            xSpace = GRIDX(0.2);
            ySpace = GRIDY(0.1);
            x = ORIGIN_X_VEH + GRIDX(0.3);
            y = ORIGIN_Y_VEH + GRIDY(3.85);
            w = SIZE_WIDTH_VEH - GRIDX(0.3);
            h = GRIDY(0.9);
        };

        #define __HIDE_CLASS x = 0;\
                    y = 0;\
                    w = 0;\
                    h = 0;\
                    colorText[] = {0, 0, 0, 0};\
                    colorBackground[] = {0, 0, 0, 0};

        class CA_BackgroundVehicle: RscPicture {
            __HIDE_CLASS
            text = "";
        };
        class CA_BackgroundVehicleTitleDark: RscText {
            __HIDE_CLASS
        };
        class CA_Vehicle: RscText {
            __HIDE_CLASS
        };
        class CA_VehicleRole: RscPicture {
            __HIDE_CLASS
            textureCargo = "";
            textureCommander = "";
            textureDriver = "";
            textureGunner = "";
            text = "";
        };

        class CA_AltBackground: RscText {
            __HIDE_CLASS
        };
        class CA_Alt: RscText {
            __HIDE_CLASS
        };
        class CA_AltUnits: RscText {
            __HIDE_CLASS
        };
        class CA_Depth: RscText {
            __HIDE_CLASS
        };
        class CA_SpeedBackground: RscText {
            __HIDE_CLASS
        };
        class CA_Speed: RscText {
            __HIDE_CLASS
        };
        class CA_SpeedUnits: RscText {
            __HIDE_CLASS
        };
    };

    // -- Infantry zeroing
    class RscWeaponZeroing: RscUnitInfo {
        idd = 300;
        controls[] = {"CA_Zeroing"};
        class CA_Zeroing: RscText {
            font = FONTMED;
            idc = 168;
            colorText[] = COLOR_TEXT;
            x = ORIGIN_X;
            y = ORIGIN_Y - GRIDY(0.1);
            w = GRIDX(2.2);
            h = GRIDY(0.8);
            sizeEx = GRIDY(0.8);
        };
    };

    class RscUnitInfoTank: RscUnitInfo {
        controls[] = {"WeaponInfoControlsGroupRight", "CA_Zeroing", "CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel", "CA_Radar"};
    };

    class RscUnitInfoAirNoWeapon: RscUnitInfo {
        controls[] = {"CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel", "CA_Radar"};
    };
    class RscUnitInfoAir: RscUnitInfoAirNoWeapon {
        controls[] = {"CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel", "CA_Radar", "WeaponInfoControlsGroupRight"};
    };

    class RscUnitVehicle {
        controls[] = {"WeaponInfoControlsGroupRight", "CA_Zeroing", "CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel", "CA_Alt"};
    };

    class RscUnitInfoUAV {
        controls[] = {"WeaponInfoControlsGroupRight", "CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel", "CA_Radar", "RscTextUAV", "TextPosition", "Position", "TextZoom", "Zoom", "Time", "Date"};
    };

    class RscUnitInfoSubmarine: RscUnitInfo {
        controls[] = {"WeaponInfoControlsGroupRight", "CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel", "CA_Depth"};
    };
    class RscUnitInfoShip: RscUnitInfo {
        controls[] = {"WeaponInfoControlsGroupRight", "CA_Zeroing", "CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel", "CA_Radar"};
    };
    class RscUnitInfoNoWeapon: RscUnitInfo {
        controls[] = {"CA_HitZones", "CA_VehicleTogglesBackground", "CA_VehicleToggles", "CA_ValueFuelBackground", "CA_ValueFuel"};
    };

    #include "RscStaminaBar.hpp"
    #include "RscStanceInfo.hpp"
};
