class SVAR(SettingsButton): RscButtonMenu {
	default = 0;
	font = FONTLIGHT;
	fontSecondary = FONTLIGHT;
	period = 10;
	periodFocus = 10;
	periodOver = 10;
	style = 0;
	colorBackground[] = {0, 0, 0, 0.5};
	colorBackground2[] = {0.2, 0.2, 0.2, 0.5};
	colorBackgroundFocused[] = {0.2, 0.2, 0.2, 0.5};
	color[] = {0.749, 0.749, 0.749, 1};
	colorFocused[] = {0.749, 0.749, 0.749, 1};
	color2[] = {0.749, 0.749, 0.749, 1};
	colorText[] = {0.749, 0.749, 0.749, 1};
	colorDisabled[] = {0.25, 0.25, 0.25, 1};
	colorSecondary[] = {0.749, 0.749, 0.749, 1};
	color2Secondary[] = {0.749, 0.749, 0.749, 1};
	colorFocusedSecondary[] = {0.749, 0.749, 0.749, 1};
	class Attributes {
		color = "#FFFFFF";
		font = FONTLIGHT;
		shadow = "false";
		align = "left";
	};
	text = "FRL - Settings";
	action = "(findDisplay 49) closeDisplay 0; 0 spawn { ['show', 'gui'] call FRL_main_fnc_settingsWindow; };";
	tooltip = "Change Settings";
	x = "(safezoneX)";
	y = "safeZoneY";
	h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
	w = SIZE_BUTTON_WIDTH;
  size = GRIDY(1.4);
  sizeEx = GRIDY(1.4);
	onLoad = "if !('gui' call FRL_Main_fnc_settingsCategoryExists) then { (_this select 0) ctrlShow false; };";
};

class SVAR(VotingButton): SVAR(SettingsButton) {
	tooltip = "Call Vote";
	text = "FRL - Voting";
	action = "(findDisplay 49) closeDisplay 0; 0 spawn { ['show', 'voting'] call FRL_main_fnc_settingsWindow; };";
	y = "safeZoneY + 1.05 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
	onLoad = "if !('voting' call FRL_Main_fnc_settingsCategoryExists) then { (_this select 0) ctrlShow false; };";
};

class SVAR(KeybindsButton): SVAR(SettingsButton) {
	tooltip = "Adjust frontline keybinds";
	text = "FRL - Keybinds";
	action = "(findDisplay 49) closeDisplay 0; 0 spawn { ['show', 'frl_main_keybinds'] call FRL_main_fnc_settingsWindow; };";
	y = "safeZoneY + 2.1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
	onLoad = "if !('frl_main_keybinds' call FRL_Main_fnc_settingsCategoryExists) then { (_this select 0) ctrlShow false; };";
};

class SVAR(AdminButton): SVAR(SettingsButton) {
	tooltip = "Admin Options";
	text = "FRL - Admin";
	action = "(findDisplay 49) closeDisplay 0; 0 spawn { ['show', 'admin'] call FRL_main_fnc_settingsWindow; };";
	y = "safeZoneY + 3.15 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
	onLoad = "if (!('admin' call FRL_Main_fnc_settingsCategoryExists) || !(serverCommandAvailable '#kick')) then { (_this select 0) ctrlShow false; };";
};

class SVAR(invisibleButton): RscButton {
	shadow = 0;
	colorText[] = {0, 0, 0, 0};
	colorDisabled[] = {0, 0, 0, 0};
	colorBackground[] = {0, 0, 0, 0};
	colorBackgroundActive[] = {0, 0, 0, 0};
	colorBackgroundDisabled[] = {0, 0, 0, 0};
	colorFocused[] = {0, 0, 0, 0};
	colorShadow[] = {0, 0, 0, 0};
	colorBorder[] = {0, 0, 0, 0};
};
