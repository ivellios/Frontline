#define SIZE_WIDTH_QUESTION PX(40)
#define SIZE_HEIGHT_QUESTION PY(10)
class SVAR(questionHUD) : RscControlsGroupNoScrollbars {
  idc = 8601;
  x = safeZoneX + safeZoneW - SIZE_WIDTH_QUESTION;
  y = safeZoneY + safeZoneH - SIZE_HEIGHT_QUESTION;
  w = SIZE_WIDTH_QUESTION;
  h = SIZE_HEIGHT_QUESTION;

  class Controls {
    class Subgroup: RscControlsGroupNoScrollbars {
      idc = 8602;
      x = 0;
      y = y = -1 * PY(40);
      w = SIZE_WIDTH_QUESTION;
      h = SIZE_HEIGHT_QUESTION;

      class Controls {
        class questionBackground : RscBackground {
          colorBackground[] = {0, 0, 0, 0.85};
          x = 0;
          y = 0;
          w = SIZE_WIDTH_QUESTION;
          h = SIZE_HEIGHT_QUESTION;
        };
        class questionText : RscStructuredText {
          idc = 8603;
          text = "";
          x = PX(1);
          y = PY(1);
          w = SIZE_WIDTH_QUESTION - PX(2);
          h = PY(5);
        };
        class questionOptions : RscListNBox {
          idc = 8604;
          x = PX(1);
          y = PY(5);
          w = SIZE_WIDTH_QUESTION - PX(2);
          h = PY(5);
          columns[] = {0, 0.1, 0.5, 0.6};
        };
      };
    };
  };
};
