class SVAR(squadTypeEntry): RscControlsGroupNoScrollbars {
  idc = 6000;
  x = 0;
  y = 0;
  w = (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE);
  h = _SQUADSELECTOR_ROWHEIGHT;

  class Controls {
    class TypeBackground: RscBackground {
      idc = -1;
      x = _SQUADSELECTOR_SPACING_X;
      y = _SQUADSELECTOR_SPACING_Y;
      w = _SQUADSELECTOR_WIDTH_TYPE;
      h = _SQUADSELECTOR_ROWHEIGHT - _SQUADSELECTOR_SPACING_Y;
      colorBackground[] = COLOR_BUTTON_BG_FOCUS;
    };

    class ConditionBackground: TypeBackground {
      idc = -1;
      x = _SQUADSELECTOR_SPACING_X + _SQUADSELECTOR_WIDTH_TYPE + _SQUADSELECTOR_SPACING_X;
      w = _SQUADSELECTOR_WIDTH_CONDITION - _SQUADSELECTOR_SPACING_X;
      colorBackground[] = COLOR_BUTTON_BG_FOCUS;
    };

    class SquadIcon: RscPictureKeepAspect {
      idc = 6001;
      x = _SQUADSELECTOR_SPACING_X + GRIDX(0.25);
      y = _SQUADSELECTOR_SPACING_Y + GRIDX(0.25);
      w = GRIDX(2);
      h = GRIDY(2);
      text = "";
    };

    class SquadName: RscStructuredText {
      idc = 6002;
      x = _SQUADSELECTOR_SPACING_X + GRIDX(2.5);
      y = _SQUADSELECTOR_SPACING_Y;
      w = _SQUADSELECTOR_WIDTH_TYPE - GRIDX(2.5);
      h = _SQUADSELECTOR_ROWHEIGHT - _SQUADSELECTOR_SPACING_Y;
      text = "";
    };

    class SquadConditions: RscStructuredText {
      idc = 6003;
      x = _SQUADSELECTOR_SPACING_X + _SQUADSELECTOR_WIDTH_TYPE + _SQUADSELECTOR_SPACING_X;
      y = _SQUADSELECTOR_SPACING_Y;
      w = _SQUADSELECTOR_WIDTH_CONDITION - _SQUADSELECTOR_SPACING_X;
      h = _SQUADSELECTOR_ROWHEIGHT - _SQUADSELECTOR_SPACING_Y;
      text = "";
    };

    class SelectSquadType: SVAR(invisibleButton) {
      idc = 6004;
      x = _SQUADSELECTOR_SPACING_X;
      y = _SQUADSELECTOR_SPACING_Y;
      w = (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE);
      h = _SQUADSELECTOR_ROWHEIGHT - _SQUADSELECTOR_SPACING_Y;
      //colorFocused[] = {1, 1, 1, 0.3};
      //colorBackgroundActive[] = {1, 1, 1, 0.3};
    };
  };
};
