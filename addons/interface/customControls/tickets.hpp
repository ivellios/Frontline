class RscFrlTicketsBars : RscControlsGroupNoScrollbars {
    idc = 100;
    x = 0;
    y = 0;
    w = GRIDX(3.4);
    h = GRIDY(2.5);
    class Controls {
        class TeamFlag : RscPicture {
            idc = 2011;
            text = "#(argb,8,8,3)color(0.5,0.5,0.5,1)";
            x = 0;
            y = GRIDY(0.4);
            w = GRIDX(0.8);
            h = GRIDY(0.8);
        };

        class Tickets : RscText {
            idc = 2013;
            text = "0";
            font = FONTLIGHT;
            style = 1;
            x = GRIDY(0.9);
            y = GRIDY(0.35);
            w = GRIDX(2.2);
            h = GRIDY(1);
            sizeEx = GRIDY(1);
            shadow = 1;
        };

        class BackgroundProgress : RscText {
            idc = 2014;
            shadow = 0;
            colorBackground[] = {1, 1, 1, 0.2};
            x = 0;
            y = 0;
            w = GRIDX(3.4);
            h = GRIDY(0.2);
        };

        class Progress : RscProgress {
            idc = 2015;
            colorFrame[] = {0,0,0,0};
            colorBar[] = {1, 1, 1, 1};
            x = 0;
            y = 0;
            w = GRIDX(3.4);
            h = GRIDY(0.2);
        };
    };
};
