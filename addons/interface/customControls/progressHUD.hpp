class RscFrlProgressGroup: RscControlsGroupNoScrollbars {
	idc = -1;
	x = 0;
	y = 0;
	w = SIZE_WIDTH_PROGRESS;
	h = 0;

	class Controls {
		class Picture : RscPicture {
			idc = 2;
			text = "#(argb,8,8,3)color(0.5,0.5,0.5,0)";
			x = 0;
			y = GRIDY(0.1);
			w = GRIDX(1);
			h = GRIDY(1);
      shadow = 1;
		};

		class ProgressText : RscText {
			idc = 3;
			style = 0;
			text = "";
			x = GRIDX(1.1);
      font = FONTLIGHT;
			y = 0;
			w = SIZE_WIDTH_PROGRESS - GRIDX(1.1);
			h = GRIDY(1.2);
		};

		class ProgressBackground : RscBackground {
			idc = 4;
			colorBackground[] = {1, 1, 1, 0.3};
			x = 0;
			y = GRIDY(1.2);
			w = SIZE_WIDTH_PROGRESS;
			h = GRIDY(0.2);
		};

		class ProgressBar : RscProgress {
			idc = 5;
			colorFrame[] = {0,0,0,0};
			colorBar[] = {1,1,1,1};
			x = 0;
			y = GRIDY(1.2);
			w = SIZE_WIDTH_PROGRESS;
			h = GRIDY(0.2);
		};

	};
};
