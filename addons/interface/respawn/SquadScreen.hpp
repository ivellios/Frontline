class SVAR(SquadScreen) {
    idd = 2000;
    onLoad = "";
    onUnload = "";

    class ControlsBackground { };
    class Controls {
        class TicketInfo : SVAR(TicketInfo) { };
        class TeamInfo : SVAR(TeamInfo) { };
        class CommanderSelect: SVAR(CommanderSelect) { };
        class SquadManagement : SVAR(SquadManagement) { };
    };
};
