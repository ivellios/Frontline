#include "customControls.hpp"

#include "controls\TeamInfo.hpp"
#include "controls\SquadManagement.hpp"
#include "controls\RoleManagement.hpp"
#include "controls\DeploymentManagement.hpp"
#include "controls\SquadTypeSelector.hpp"
#include "controls\TicketInfo.hpp"
#include "controls\CommanderSelect.hpp"

class SVAR(RespawnScreen) {
    idd = 1000;
    onLoad = "['frl_RespawnScreen_onLoad', _this] call CLib_fnc_localEvent;";
    onUnload = "'frl_RespawnScreen_onUnload' call CLib_fnc_localEvent;";

    class ControlsBackground {
        class Map : RscMapControl {
            idc = 800;
            x = safeZoneX;
            y = safeZoneY;
            //y = PY(10.5) + safeZoneY;
            w = safeZoneW;
            //h = safeZoneH-PY(10.5);
            h = safeZoneH;
        };
    };
    class Controls {
      class RespawnHints : RscControlsGroupNoScrollbars {
          idc = 500;
          x = safeZoneX;
          y = safeZoneY + safeZoneH - PY(6);
          w = safeZoneW;
          h = PY(6);
          fade = 0;

          class Controls {
              class Background : RscPicture {
                  idc = 499;
                  text = "#(argb,8,8,3)color(0,0,0,0.8)";
                  x = 0;
                  y = 0;
                  w = safeZoneW;
                  h = PY(6);
              };
              class Hint : RscText {
                  idc = 501;
                  text = "";
                  x = PX(41);
                  y = PY(0.5);
                  w = safeZoneW - 2*PX(40);
                  h = PY(6);
                  sizeEx = PY(2);
                  font = FONTLIGHT;
                  colorText[] = {1, 1, 1, 1};
              };
          };
      };

        class TicketInfo : SVAR(TicketInfo) {};
        class TeamInfo : SVAR(TeamInfo) {};
        class CommanderSelect: SVAR(CommanderSelect) {};

        class SquadManagement : SVAR(SquadManagement) {};
        class RoleManagement : SVAR(RoleManagement) {};
        class DeploymentManagement : SVAR(DeploymentManagement) {};
    };
};
