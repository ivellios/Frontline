class frl_buttonMenuSmall: RscButton {
  text = "";
  font = FONTLIGHT_ALT;
  style = 0;
  sizeEx = PY(1.6);
  shadow = 0;
  colorBackground[] = COLOR_BUTTON_BG;
  colorBackgroundActive[] = COLOR_BUTTON_BG_FOCUS;
  colorFocused[] = COLOR_BUTTON_BG_FOCUS;
  colorText[] = COLOR_BUTTON_TEXT;
  x = PX(0);
  y = PY(0);
  w = PX(9);
  h = PY(3);
  period = 0;
};

class frl_buttonMenuBig_Accent: frl_buttonMenuSmall {
  colorBackground[] = COLOR_BUTTON_BG_ACCENT;
  colorBackgroundActive[] = COLOR_BUTTON_BG_ACCENT_FOCUS;
  colorFocused[] = COLOR_BUTTON_BG_ACCENT_FOCUS;
  colorText[] = COLOR_BUTTON_TEXT_ACCENT;
  // offsetX = PX(1);
  // offsetPressedX = PX(1);
  sizeEx = PY(4);
  font = FONTBOLD_ALT;
};

class frl_H1Text : RscText {
    text = "";
    font = FONTBOLD_ALT;
    sizeEx = PY(2.9);
    shadow = 0;
    colorBackground[] = {0,0,0,0};
    colorText[] = COLOR_BUTTON_BG_ACCENT;
    x = PX(0);
    y = PY(0);
    w = PX(30);
    h = PY(4);
};

class frl_H2Text : frl_H1Text {
    font = FONTBOLD_ALT;
    sizeEx = PY(2.5);
    h = PY(3);
};

class frl_RscListNBox : RscListNBox {
  sizeEx = PY(2);
  rowHeight = PY(3);
  columns[] = {0};
  itemSpacing = PY(0.25);
  colorText[] = {0.8,0.8,0.8,1}; // Selected item color
  pictureColor[] = {0.8,0.8,0.8,1}; // Picture color
  pictureColorSelect[] = {1,1,1,1}; // Selected picture color
  colorSelectBackground[] = {0.2,0.2,0.2,1}; // Selected item fill color
  colorSelectBackground2[] = {0.2,0.2,0.2,1}; // Selected item fill color (oscillates between this and colorSelectBackground)
  colorSelect[] = {1,1,1,1}; // Selected item color
  colorSelect2[] = {1,1,1,1}; // Selected item color (oscillates between this and colorSelectBackground)
};
