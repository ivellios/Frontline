#undef GHEIGHT
#undef GWIDTH
#define GWIDTH 40
#define GHEIGHT 96

class SVAR(SquadManagement) : RscControlsGroupNoScrollbars {
    idc = 200;
    x = safeZoneX;
    y = PY(14.5) + safeZoneY;
    w = PX(GWIDTH);
    h = PY(GHEIGHT);
    fade = 0;
    onLoad = "['frl_SquadScreen_onLoad', _this] call CLib_fnc_localEvent;";
    onUnload = "'frl_SquadScreen_onUnload' call CLib_fnc_localEvent;";

    class Controls {
        class Background : RscPicture {
            idc = 299;
            text = "#(argb,8,8,3)color(0,0,0,0.8)";
            x = PX(0);
            y = PY(0);
            w = PX(GWIDTH);
            h = PY(GHEIGHT);
        };
        class HeadingBackground : RscHeaderBackground {
            idc = 201;
            y = PY(0);
        };
        class Heading : frl_H2Text {
            idc = 202;
            text = "SQUAD";
            x = PX(0.5);
            y = PY(0);
            w = PX(GWIDTH-1);
            h = PY(3);
        };

        class NewSquadDesignator : RscText {
            idc = 203;
            text = "";
            x = PX(0.5);
            y = PY(4);
            w = PX(2.5);
            h = PY(3);
        };

        class NewSquadDescriptionInput : RscEdit {
            idc = 204;
            x = PX(3.5);
            y = PY(4);
            w = PX(GWIDTH-18);
            h = PY(3);
            onChar = "'frl_RespawnScreen_SquadDescriptionInput_TextChanged' call CLib_fnc_localEvent;";
        };

        class squadCreateBtn : frl_buttonMenuSmall {
            idc = 206;
            text = "CREATE";
            x = PX(GWIDTH-12);
            y = PY(4);
            w = PX(11);
            h = PY(3);

            onButtonClick = "'frl_squadUI_createClick' call CLib_fnc_localEvent;";
        };

        class SquadList : frl_RscListNBox {
            idc = 207;
            x = PX(0);
            y = PY(8);
            w = PX(GWIDTH);
            h = PY(43.5-8);

            columns[] = {0,0.075,0.5,0.85};

            onLBSelChanged = "'frl_squadUI_squadListSelChanged' call CLib_fnc_localEvent;";
        };

        class BackgroundSquadDetails : RscPicture {
            idc = 208;
            text = "#(argb,8,8,3)color(0.1,0.1,0.1,1)";
            x = PX(0);
            y = PY(43.5);
            w = PX(GWIDTH);
            h = PY(52.5);
        };

        class HeadingSquadDetails : frl_H2Text {
            idc = 209;
            text = "";
            x = PX(0.5);
            y = PY(43.5);
            w = PX(GWIDTH-22);
            h = PY(3);
        };

        class SquadMemberList : frl_RscListNBox {
            idc = 210;
            x = PX(0);
            y = PY(48);
            w = PX(GWIDTH);
            h = PY(37.5);

            columns[] = {0};

            onLBSelChanged = "'frl_squadUI_memberListSelChanged' call CLib_fnc_localEvent;";
        };

        class JoinLeaveBtn : frl_buttonMenuSmall {
            idc = 211;
            text = "JOIN";
            x = PX(GWIDTH-5);
            y = PY(43.5);
            w = PX(5);
            h = PY(3);
            sizeEx = PY(1.5);
            style = ST_CENTER;

            onButtonClick = "'frl_RespawnScreen_JoinLeaveBtn_onButtonClick' call CLib_fnc_localEvent;";
        };

        class LockBtn : frl_buttonMenuSmall {
            idc = 214;
            text = "LOCK";
            x = PX(GWIDTH-12.5);
            y = PY(43.5);
            w = PX(7);
            h = PY(3);
            sizeEx = PY(1.5);
            style = ST_CENTER;
            onButtonClick = "";
        };

        class KickBtn : frl_buttonMenuSmall {
            idc = 212;
            text = "KICK";
            x = PX(GWIDTH-18);
            y = PY(43.5);
            w = PX(5);
            h = PY(3);
            sizeEx = PY(1.5);
            style = ST_CENTER;

            onButtonClick = "";
        };

        class PromoteBtn : frl_buttonMenuSmall {
            idc = 213;
            text = "PROMOTE";
            x = PX(GWIDTH-26.5);
            y = PY(43.5);
            w = PX(8);
            h = PY(3);
            sizeEx = PY(1.5);
            style = ST_CENTER;

            onButtonClick = "'frl_RespawnScreen_PromoteBtn_onButtonClick' call CLib_fnc_localEvent;";
        };

    };
};
