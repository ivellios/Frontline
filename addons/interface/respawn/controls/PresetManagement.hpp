
class RscPanelPresets: RscControlsGroupNoScrollbars {
  idc = 306;
  x = PX(0);
  y = PY(42);
  w = PRESET_WIDTH;
  h = PRESET_HEIGHT;
  class Controls {

    class HeadingBackgroundPresets : RscHeaderBackground {
      idc = 401;
      y = 0;
    };
    class HeadingPresets : frl_H2Text {
      idc = 402;
      text = "PRESETS";
      x = PX(0.5);
      y = 0;
      w = PX(GWIDTH-1);
      h = PY(3);
    };

    class PresetsBackground: RscBackground {
      x = 0;
      y = PY(3);
      w = PRESET_WIDTH;
      h = PRESET_HEIGHT - PY(3);
      colorBackground[] = {0.1, 0.1, 0.1, 1};
    };

    class InspectVariant : ctrlXListbox {
      idc = 3303;
      x = 0;
      y = PY(3) + PRESET_ITEM_SPACING_Y;
      w = PRESET_WIDTH;
      h = PRESET_TAB_HEIGHT;
      onLBSelChanged = "['frl_RoleScreen_presetChanged', _this] call Clib_fnc_localEvent;";
      colorActive[] = COLOR_BUTTON_BG_ACCENT;
      colorSelect[] = COLOR_BUTTON_BG_ACCENT;
      colorText[] = COLOR_BUTTON_TEXT_ACCENT;
      colorBackground[] = COLOR_BUTTON_BG_ACCENT;
      border = "\pr\frl\addons\interface\img\slider\background_ca.paa";
      arrowEmpty="\pr\frl\addons\interface\img\slider\arrowEmpty_ca.paa";
      arrowFull="\pr\frl\addons\interface\img\slider\arrowFull_ca.paa";
      font = FONTLIGHT_ALT;
    };

    class VariantContent: ctrlControlsGroupNoHScrollbars {
      idc = 3304;
      x = 0;
      y = PY(3);
      w = PRESET_WIDTH;
      h = PRESET_HEIGHT - PY(3);
      class Controls {
        class VariantContent1: ctrlControlsGroupNoScrollbars {
          idc = 4100;
          x = 0;
          y = PRESET_ITEM_SPACING_Y;
          w = PRESET_WIDTH;
          h = PRESET_WEAPON_HEIGHT;

          class Controls {
            class InspectWeaponBg: ctrlStatic {
              //idc = 5201;
              x = 0;
              y = 0;
              w = PRESET_ITEM_POS_X(4) - PRESET_ITEM_SPACING_X;
              h = PRESET_WEAPON_HEIGHT;
              colorBackground[] = COLOR_BUTTON_BG_FOCUS;
            };
            class InspectWeapon: ctrlStaticPictureKeepAspect {
              idc = 5101;
              x = 0;
              y = 0;
              w = PRESET_ITEM_POS_X(4) - PRESET_ITEM_SPACING_X;
              h = PRESET_WEAPON_HEIGHT;
            };
            class InspectMuzzleBg: ctrlStatic {
              //idc = 5202;
              x = PRESET_ITEM_POS_X(4);
              y = 0;
              w = PRESET_ITEM_WIDTH;
              h = PRESET_ITEM_HEIGHT;
              colorBackground[] = COLOR_BUTTON_BG_FOCUS;
            };
            class InspectMuzzle: ctrlStaticPictureKeepAspect {
              idc = 5102;
              x = PRESET_ITEM_POS_X(4);
              y = 0;
              w = PRESET_ITEM_WIDTH;
              h = PRESET_ITEM_HEIGHT;
            };
            class InspectRailBg: InspectMuzzleBg {
              //idc = 5203;
              x = PRESET_ITEM_POS_X(3);
            };
            class InspectRail: InspectMuzzle {
              idc = 5103;
              x = PRESET_ITEM_POS_X(3);
            };
            class InspectBipodBg: InspectMuzzleBg {
              //idc = 5204;
              x = PRESET_ITEM_POS_X(2);
            };
            class InspectBipod: InspectMuzzle {
              idc = 5104;
              x = PRESET_ITEM_POS_X(2);
            };
            class InspectScopeBg: InspectMuzzleBg {
              //idc = 5205;
              x = PRESET_ITEM_POS_X(1);
            };
            class InspectScope: InspectMuzzle {
              idc = 5105;
              x = PRESET_ITEM_POS_X(1);
            };
            class InspectMagsBg: InspectMuzzleBg {
              //idc = 5206;
              x = PRESET_ITEM_POS_X(4);
              y = PRESET_ITEM_HEIGHT + PRESET_ITEM_SPACING_Y; //(SIZE_ITEM + 1) * GRID_H;
              w = 4*PRESET_ITEM_WIDTH + 3*PRESET_ITEM_SPACING_Y;
              h = PRESET_ITEM_HEIGHT;
              colorBackground[] = COLOR_BUTTON_BG_FOCUS;
            };
            class InspectMags: ctrlControlsGroupNoVScrollbars {
              idc = 5106;
              x = PRESET_ITEM_POS_X(4);
              y = PRESET_ITEM_HEIGHT + PRESET_ITEM_SPACING_Y; //(SIZE_ITEM + 1) * GRID_H;
              w = 4*PRESET_ITEM_WIDTH + 3*PRESET_ITEM_SPACING_Y;
              h = PRESET_ITEM_HEIGHT;
              class Controls {

              };
            };
          };
        };

        class VariantItems: VariantContent1 {
          idc = 4200;
          y = PRESET_WEAPON_HEIGHT*1 + 2*PRESET_ITEM_SPACING_Y;
          class Controls {
            class InspectBackpackBg: ctrlStatic {
              //idc = 5201;
              x = 0;
              y = 0;
              w = PRESET_WEAPON_WIDTH;
              h = PRESET_WEAPON_HEIGHT;
              colorBackground[] = COLOR_BUTTON_BG_FOCUS;
            };

            class InspectBackpack: ctrlStaticPictureKeepAspect {
              idc = 5101;
              x = 0;
              y = 0;
              w = PRESET_WEAPON_WIDTH;
              h = PRESET_WEAPON_HEIGHT;
              text = "\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_backpack_gs.paa";
            };

            class InspectMagsBg: InspectBackpackBg {
              //idc = 5206;
              x = PRESET_WEAPON_WIDTH + PRESET_ITEM_SPACING_X;
              w = PRESET_WIDTH - PRESET_WEAPON_WIDTH - PRESET_ITEM_SPACING_X;
            };

            class InspectMags: ctrlControlsGroupNoVScrollbars {
              idc = 5106;
              x = PRESET_WEAPON_WIDTH + PRESET_ITEM_SPACING_X;
              y = 0;
              w = PRESET_WIDTH - PRESET_WEAPON_WIDTH - PRESET_ITEM_SPACING_X;
              h = PRESET_WEAPON_HEIGHT;
              class Controls {

              };
            };
          };
        };

        class VariantContent2: VariantContent1 {
          idc = 4300;
          y = PRESET_WEAPON_HEIGHT*2 + 3*PRESET_ITEM_SPACING_Y;
        };
        class VariantContent3: VariantContent1 {
          idc = 4400;
          y = PRESET_WEAPON_HEIGHT*3 + 4*PRESET_ITEM_SPACING_Y;
        };
      };
    };
  };
};
