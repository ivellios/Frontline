#undef GHEIGHT
#undef GWIDTH
#define GWIDTH 40
#define GHEIGHT 63

class SVAR(RoleManagement) : RscControlsGroupNoScrollbars {
    idc = 300;
    x = safeZoneX + safeZoneW - PX(40);
    y = safeZoneY;
    w = PX(GWIDTH);
    h = PY(GHEIGHT);
    fade = 0;
    onLoad = "['frl_RoleScreen_onLoad', _this] call CLib_fnc_localEvent;";
    onUnload = "'frl_RoleScreen_onUnload' call CLib_fnc_localEvent;";

    class Controls {
        class Background : RscPicture {
            idc = 399;
            text = "#(argb,8,8,3)color(0,0,0,0.8)";
            x = PX(0);
            y = PY(0);
            w = PX(GWIDTH);
            h = PY(GHEIGHT);
        };

        class HeadingBackground : RscHeaderBackground {
            idc = 301;
            y = PY(0);
        };

        class Heading : frl_H2Text {
            idc = 302;
            text = "ROLE";
            x = PX(0.5);
            y = PY(0);
            w = PX(GWIDTH-1);
            h = PY(3);
        };

        class RoleList : frl_RscListNBox {
            idc = 303;
            x = PX(0);
            y = PY(4);
            w = PX(GWIDTH);
            h = PY(36);
            columns[] = {0,0.85};
            onLBSelChanged = "'frl_RoleScreenList_onLBSelChanged' call CLib_fnc_localEvent;";
        };
        #include "PresetManagement.hpp"
    };
};
