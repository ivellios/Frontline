
class SVAR(SquadSelector): RscControlsGroupNoHScrollbars {
  x = 0;
  y = 0;
  w = (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE);
  h = _SQUADSELECTOR_ROWHEIGHT;
  idc = 600;

  class Controls {
    class GeneralBackground: RscBackground {
      idc = 601;
      x = 0;
      y = 0;
      w = (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE);
      h = _SQUADSELECTOR_ROWHEIGHT;
      colorBackground[] = {0, 0, 0, 0.8};
    };
  };
};
