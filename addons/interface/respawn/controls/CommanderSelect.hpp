#define GWIDTH 40
#define GHEIGHT 4.5

class SVAR(CommanderSelect) : RscControlsGroupNoScrollbars {
    idc = 700;
    x = safeZoneX;
    y = PY(10.5) + safeZoneY;
    w = PX(GWIDTH);
    h = PY(GHEIGHT);
    onLoad = "['frl_commander_onLoadCtrl', _this] call Clib_fnc_localEvent;";
    fade = 0;

    class Controls {
        class Background : RscPicture {
            idc = 701;
            text = "#(argb,8,8,3)color(0,0,0,0.8)";
            x = PX(0);
            y = PY(0);
            w = PX(GWIDTH);
            h = PY(GHEIGHT);
        };
        class Header : frl_H2Text {
            idc = 702;
            text = "Commander:";
            x = PX(0.5);
            y = PY(0.75);
            w = PX(12);
            h = PY(3);
        };

        class CommanderName : RscText {
            idc = 703;
            text = "<Empty>";
            x = PX(14);
            y = PY(0.75);
            w = PX(GWIDTH-12-10);
            h = PY(3);
            colorText[] = {1,1,1,1};
        };

        class ChangeSideBtn : frl_buttonMenuSmall {
            idc = 704;
            text = "APPLY";
            x = PX(GWIDTH-10);
            y = PY(0.75);
            w = PX(9);
            h = PY(3);
            onButtonClick = "'frl_commander_applyButton' call CLib_fnc_localEvent;";
        };
    };
};
