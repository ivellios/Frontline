#undef GHEIGHT
#undef GWIDTH
#define GWIDTH 40
#define GHEIGHT 45

class SVAR(DeploymentManagement) : RscControlsGroupNoScrollbars {
    idc = 400;
    x = safeZoneX + safeZoneW - PX(40);
    y = PY(63) + safeZoneY;
    w = PX(GWIDTH);
    h = PY(GHEIGHT);
    fade = 0;
    onLoad = "['frl_DeploymentScreen_onLoad', _this] call CLib_fnc_localEvent;";
    onUnload = "'frl_DeploymentScreen_onUnload' call CLib_fnc_localEvent;";

    class Controls {
        class Background : RscPicture {
            idc = 499;
            text = "#(argb,8,8,3)color(0,0,0,0.8)";
            x = PX(0);
            y = PY(0);
            w = PX(GWIDTH);
            h = PY(GHEIGHT);
        };
        class HeadingBackground : RscHeaderBackground {
            idc = 401;
            y = PY(0);
        };
        class Heading : frl_H2Text {
            idc = 402;
            text = "DEPLOYMENT";
            x = PX(0.5);
            y = PY(0);
            w = PX(GWIDTH-1);
            h = PY(3);
        };
        class SpawnPointList : frl_RscListNBox {
            idc = 403;
            x = PX(0);
            y = PY(4);
            w = PX(GWIDTH);
            h = PY(GHEIGHT-10);
            columns[] = {0,0.075,0.875};
            onLBSelChanged = "'frl_DeployScreen_listOnLBSelChanged' call CLib_fnc_localEvent;";
        };
        class DeployButton : frl_buttonMenuBig_Accent {
            idc = 404;
            text = "DEPLOY";
            x = PX(0);
            y = PY(GHEIGHT - 6);
            w = PX(GWIDTH);
            h = PY(6);
            action = "'frl_DeployScreen_buttonAction' call CLib_fnc_localEvent;";
        };
    };
};
