#define GWIDTH 40
#define GHEIGHT 4.5

class SVAR(TeamInfo) : RscControlsGroupNoScrollbars {
    idc = 100;
    x = safeZoneX;
    y = PY(6) + safeZoneY;
    w = PX(GWIDTH);
    h = PY(GHEIGHT);
    fade = 0;
    onLoad = "['frl_sideUI_onLoad', _this] call CLib_fnc_localEvent;";
    onUnload = "'frl_sideUI_onUnload' call CLib_fnc_localEvent;";

    class Controls {
        class Background : RscPicture {
            idc = 199;
            text = "#(argb,8,8,3)color(0,0,0,0.8)";
            x = PX(0);
            y = PY(0);
            w = PX(GWIDTH);
            h = PY(GHEIGHT);
        };
        class TeamFlag : RscPicture {
            idc = 102;
            text = "#(argb,8,8,3)color(0.5,0.5,0.5,1)";
            x = PX(0.5);
            y = PY(0.75);
            w = PX(3);
            h = PY(3);
        };
        class TeamName : frl_H1Text {
            idc = 103;
            text = "";
            x = PX(4);
            y = PY(0.25);
            w = PX(GWIDTH-12);
            h = PY(GHEIGHT-0.5);
            colorText[] = {1,1,1,1};
        };

        class ChangeSideBtn : frl_buttonMenuSmall {
            idc = 104;
            text = "SWITCH TEAM";
            x = PX(GWIDTH-15);
            y = PY(0.75);
            w = PX(14);
            h = PY(3);
            onButtonClick = "'frl_sideUI_buttonClick' call CLib_fnc_localEvent;";
        };
    };
};
