class SVAR(TicketInfo) : RscControlsGroupNoScrollbars {
    idc = 600;
    x = safeZoneX;
    y = safeZoneY;
    w = PX(40);
    h = PY(6);
    fade = 0;
    onLoad = "['frl_RespawnScreen_ticketInfo_onLoad', _this] call CLib_fnc_localEvent;";
    onUnload = "'frl_RespawnScreen_ticketInfo_onUnload' call CLib_fnc_localEvent;";

    class Controls {
        class Background : RscPicture {
            idc = 699;
            text = "#(argb,8,8,3)color(0,0,0,0.8)";
            x = PX(0);
            y = PY(0);
            w = PX(40);
            h = PY(6);
        };
        class TeamFlag : RscPicture {
            idc = 601;
            text = "#(argb,8,8,3)color(0.5,0.5,0.5,1)";
            x = PX(20-4);
            y = PY(1);
            w = PX(3.5);
            h = PY(3.5);
        };
        class TeamFlag2 : TeamFlag {
            idc = 602;
            x = PX(20+0.5);
        };
        class TeamName : RscText {
            idc = 603;
            text = "NATO";
            style = ST_RIGHT;
            x = PX(1);
            y = PY(0);
            w = PX(15);
            h = PY(2.5);
            sizeEx = PY(2.9);
            font = "PuristaSemiBold";
        };
        class TeamName2 : TeamName {
            idc = 604;
            text = "CSAT";
            style = ST_LEFT;
            x = PX(20+4);
        };
        class Tickets : RscText {
            idc = 605;
            text = "1234";
            style = ST_RIGHT;
            x = PX(1);
            y = PY(1.9);
            w = PX(15);
            h = PY(2);
        };
        class Tickets2 : Tickets {
            idc = 606;
            style = ST_LEFT;
            x = PX(20+4);
        };
        class Players : RscText {
            idc = 607;
            text = "0 Players";
            style = ST_RIGHT;
            x = PX(1);
            y = PY(3.7);
            w = PX(15);
            h = PY(2);
        };
        class Players2 : Players {
            idc = 608;
            style = ST_LEFT;
            x = PX(20+4);
        };
    };
};
