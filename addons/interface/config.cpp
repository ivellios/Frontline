#define __INCLUDE_IDC
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};


class ctrlButton;
class ctrlButtonOk;
class ctrlCheckboxBaseline;
class ctrlControlsGroup;
class ctrlControlsGroupNoHScrollbars;
class ctrlControlsGroupNoScrollbars;
class ctrlControlsGroupNoVScrollbars;
class ctrlEdit;
class CtrlStatic;
class ctrlStaticBackground;
class ctrlStaticFooter;
class ctrlStaticPicture;
class ctrlStaticPictureKeepAspect;
class ctrlStaticTitle;
class CtrlToolbox;
class ctrlXListbox;
class ctrlXSliderH;
class ctrlCombo;

class ScrollBar;
class RscBackground;
class RscButton;
class RscButtonMenu;
class RscCombo;
class RscControlsGroup {
  class VScrollbar: ScrollBar { };
  class HScrollbar: ScrollBar { };
};
class RscControlsGroupNoHScrollbars;
class RscControlsGroupNoScrollbars;
class RscDebugConsole;
class RscDisplayEmpty;
class RscEdit;
class RscHitZones;
class RscHTML;
class RscIGProgress;
class RscListBox;
class RscListNBox;
class RscMapControl;
class RscMapControlEmpty;
class RscObject;
class RscPicture;
class RscPictureKeepAspect;
class RscProgress;
class RscStandardDisplay;
class RscStructuredText;
class RscText;
class RscToolbox;
class RscVehicleToggles;
class RscVignette: RscPicture {
  text = "";
};
class RscHeaderBackground : RscPicture {
    text = "#(argb,8,8,3)color(0,0,0,1)";
    x = PX(0);
    y = PY(0);
    w = PX(40);
    h = PY(3);
};

#include "incLoadingSizes.hpp"

#include "customControls\buttons.hpp"
#include "customControls\tickets.hpp"
#include "customControls\progressHUD.hpp"
#include "customControls\questionHUD.hpp"
#include "customControls\squadTypeEntry.hpp"
#include "customControls\ctrlGroupMouseOver.hpp"
#include "customControls\ctrlVehicleMouseOver.hpp"


#include "RscDisplayMain.hpp"
#include "RscDisplayMainMap.hpp"
#include "RscDisplayMPInterrupt.hpp"
#include "RscDisplayLoadMission.hpp"
//#include "RscChatListDefault.hpp"
#include "RscIngameUI.hpp"
#include "CfgIngameUI.hpp"


#include "DlgFrlScoreboard.hpp"
#include "DlgFrlSpotting.hpp"
#include "respawn\RespawnScreen.hpp"
#include "respawn\SquadScreen.hpp"

class RscTitles {
  #include "RscFrlCaptureBar.hpp"
  #include "RscFrlControlHUD.hpp"
  #include "RscFrlFpsCounter.hpp"
  #include "RscFrlInteractHUD.hpp"
  #include "RscFrlNotificationMid.hpp"
  #include "RscFrlProgressHUD.hpp"
  #include "RscFrlQuestionHUD.hpp"
  #include "RscFrlTickets.hpp"
  #include "RscFrlInteractMenu.hpp"
};

class CfgScriptPaths {
    FRL_Interface = "pr\frl\addons\interface\scripts\";
};

class FRL {
  #include "configLoadingHints.hpp"
};
