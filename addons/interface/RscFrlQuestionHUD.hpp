class EGVAR(voting,questionHUD) {
    idd = -1;
    duration = 1e11;
    name = QEGVAR(voting,questionHUD);
    onLoad = "uiNamespace setVariable [""frl_voting_questionHUD"", _this select 0]";

    class ControlsBackground {
        class questionGroup: SVAR(questionHUD) {
            y = safeZoneY + safeZoneH;
            class Controls: Controls {
                class Subgroup: Subgroup {
                    //idc = 8602;
                    y = -1 * PY(40);
                };
            };
        };
    };
};
