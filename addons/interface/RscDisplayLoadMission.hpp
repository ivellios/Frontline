#include "incLoadingSizes.hpp"

/*
class SVAR(RscLoadingKeybindBackground) : RscText {
	idc = 1;
	colorBackground[] = {0, 0, 0, 0.7};
	text = "";
	x = "safezoneX + (safezoneW / 3 * 2)";
	y = "safezoneY + (2.7 * (( ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
	w = "safezoneW / 3 * 2";
	h = "(safezoneW * 4/3) - (4.7 * (( ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
	show = 1;
	onLoad = "";
};

class SVAR(RscLoadingKeybindText) : RscStructuredText {
	idc = 2;
	colorBackground[] = {0, 0, 0, 0};
	text = "";
	x = "safezoneX + (safezoneW / 3 * 2) + (1.5 * (( ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
	y = "safezoneY + (2.7 * (( ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)) + (1.5 * (( ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
	w = "safezoneW / 3 * 2";
	h = "(safezoneW * 4/3) - (4.7 * (( ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
	onLoad = "[_this, true] call compile preprocessFileLineNumbers '\pr\frl\addons\interface\scripts\fn_loadingScreen.sqf'";
	sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
};
*/

// Classname is a lie, it also is used for loading terrains (Which don't show the mission info)
class RscDisplayLoadMission: RscStandardDisplay {
  idd = 101;
  scriptName="RscDisplayLoading";
	scriptPath="FRL_Interface";
	onLoad="[""onLoad"",_this,""RscDisplayLoading"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
	onUnload="[""onUnload"",_this,""RscDisplayLoading"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
  class controls {
    class MapBackTop: RscText {
      x = safezoneX;
      y = safezoneY;
      w = safezoneW;
      idc = 1000;
      h = SIZE_BAR_HEIGHT;
      colorBackground[] = COLOR_BAR;
    };

    delete MapAuthor;
    class MapBackBottom: RscText { // This is progressbackground, need to use existing control because of Z order
      idc = 1003;
      colorBackground[] = COLOR_BACKGROUND_PROGRESS;
      x = ORIGIN_X;
      y = ORIGIN_Y;
      w = safeZoneW;
      h = SIZE_LOAD_HEIGHT;
    };

    /*
    class FrlTopLine: RscText {
      x = safezoneX;
      y = safezoneY + SIZE_BAR_HEIGHT;
      w = safezoneW;
      idc = 6000;
      h = GRIDY(0.2);
      colorBackground[] = COLOR_BACKGROUND_PROGRESS;
    };
    */


    class FrlLoadingBottom: RscText {
      idc = 6003;
      x = safezoneX;
      y = safezoneY + safeZoneH - GRIDY(5);
      w = safezoneW;
      h = SIZE_BAR_HEIGHT;
      colorBackground[] = COLOR_BAR;
    };

    class FrlLoadingStripe1: RscText {
      x = ORIGIN_X + GRIDX(0.2);
      y = ORIGIN_Y;
      w = GRIDX(0.2);
      h = SIZE_LOAD_HEIGHT;
      colorBackground[] = COLOR_BAR;
    };
    class FrlLoadingStripe2: FrlLoadingStripe1 {
      x = ORIGIN_X + GRIDX(0.6);
    };
    class FrlLoadingStripe3: FrlLoadingStripe1 {
      x = ORIGIN_X + GRIDX(1);
    };

    delete MapDescription;

    class FrlMissionDetails: RscControlsGroupNoScrollbars {
      idc = 6300;
      x = safeZoneX + safeZoneW - SIZE_BOX_WIDTH;
      y = 0;
      w = SIZE_BOX_WIDTH;
      h = safeZoneH;
      class Controls {
        class MissionDescriptionGroup: RscControlsGroupNoScrollbars {
          idc = 6100;
          x = 0;
          y = 0;
          w = SIZE_BOX_WIDTH;
          h = 0;
          class Controls {
            class DescriptionBackground: RscBackground {
              idc = 6501;
              x = 0;
              y = 0;
              w = SIZE_BOX_WIDTH;
              h = safeZoneH;
              colorBackground[] = COLOR_BACKGROUND_BOX;
            };
            class MissionDescription: RscStructuredText {
              class Attributes {
                font = FONTLIGHT;
                size = 1;
              };
              idc = 6502;
              x = GRIDX(2);
              y = GRIDY(1);
              size = GRIDY(1.8);
              sizeEx = GRIDY(1.8);
              w = SIZE_BOX_WIDTH - GRIDX(2);
              h = safeZoneH;
              colorBackground[] = {0, 0, 0, 0};
            };
          };
        };

        class GametypeDescriptionGroup: MissionDescriptionGroup {
          idc = 6200;
        };
      };
    };

    delete Mission;

    class ProgressMap: RscProgress {
      idc = 104;
      colorBar[] = COLOR_ACCENT;
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      x = ORIGIN_X;
      y = ORIGIN_Y;
      w = safeZoneW;
      h = SIZE_LOAD_HEIGHT;
    };

    class ProgressMission: ProgressMap {
      idc = 1013;
      x = ORIGIN_X;
      y = ORIGIN_Y;
      w = safeZoneW;
      h = SIZE_LOAD_HEIGHT;
    };

    delete MapName;
    class FrlLoadingText: RscText {
      idc = 6001;
      x = ORIGIN_X + SIZE_SPACING;
      y = safezoneY + safeZoneH - GRIDY(5.9);
      w = safezoneW;
      h = GRIDY(1);
      sizeEx = GRIDY(1);
      font = FONTBOLD_ALT;
      colorText[] = {0, 0, 0, 1};
      text = "CONNECTING TO SERVER";
      //colorShadow[] = {1, 1, 1, 0.5};
      shadow = 0;
    };

    delete Disclaimer;
    class Logo3DEN: ctrlStaticPicture {
      text = "\a3\3DEN\Data\Logos\3den_512_ca.paa";
      x = safezoneX + safezoneW - GRIDX(6.5);
      y = safeZoneY + safeZoneH - GRIDY(4.5);
      w = GRIDX(6);
      h = GRIDY(4);
      show = 0;
      onLoad = "if !(isnull (finddisplay 313)) then {(_this select 0) ctrlshow true;};";
    };

    class FrlLogoFrontline: ctrlStaticPicture {
      text = "\pr\frl\addons\main\ui\frontline_logo_256.paa";
      x = safeZoneX + SIZE_SPACING;
      y = safeZoneY - GRIDY(0.8);
      w = GRIDX(20);
      h = GRIDY(20);
    };

    class FrlDonateText: RscText {
      style = 1;
      x = safeZoneX + safeZoneW - GRIDX(41);
      y = safeZoneY + GRIDY(1);
      w = GRIDX(40);
      h = GRIDY(1);
      sizeEx = GRIDY(1);
      colorText[] = {1, 1, 1, 0.5};
      font = FONTLIGHT;
      text = "Donate at www.patreon.com/frontline";
    };

    class FrlLoadingHints: RscText {
        idc = 6004;
        text = "";
        x = ORIGIN_X + SIZE_SPACING;
        y = safeZoneY + safeZoneH - SIZE_BAR_HEIGHT + GRIDY(1);
        w = safeZoneW;
        h = SIZE_LOAD_HEIGHT;
        sizeEx = GRIDY(1);
        font = FONTLIGHT;
        colorText[] = {1, 1, 1, 1};
    };

    class LoadingStart: RscControlsGroup {
      idc = 2310;
      x = "0 * safezoneW + safezoneX";
      y = "0 * safezoneH + safezoneY";
      w = "safezoneW";
      h = "safezoneW * 4/3";
      class VScrollbar: VScrollbar { width = 0;	};
      class HScrollbar: HScrollbar { height = 0; };
      class controls {
        class Black: RscText {
          idc = 1000;
          colorBackground[] = COLOR_BACKGROUND;
        };

        delete Noise;
        class FrlLoadingPicture: RscPicture {
          idc = 6998;
          text = "";
          colorText[] = {1, 1, 1, 1};
          x = 0;
          y = "-(safezoneW/4)";
          w = "safezoneW";
          h = "safezoneW * 4/3";
          onLoad = "(_this select 0) ctrlsettext (getText (configfile >> 'FRL' >> 'LoadingScreen' >> 'default'));";
        };

        class Logo: RscPictureKeepAspect {
          idc = 1200;
          text = "\pr\frl\addons\main\ui\frontline_logo_512.paa";
          x = "0.15 * safezoneW";
          y = "(0.5 - 0.35/2) * safezoneH";
          w = "0.7 * safezoneW";
          h = "0.35 * safezoneH";
          onLoad = "";
        };
      };
    };

  };

  class controlsBackground {
    class Black: RscText {
      colorBackground[] = {0, 0, 0, 1};
      x = "safezoneXAbs";
      y = "safezoneY";
      w = "safezoneWAbs";
      h = "safezoneH";
    };

    class Map: RscPicture {
      idc = 999;
      text = "#(argb,8,8,3)color(0,0,0,1)";
      colorText[] = {1, 1, 1, 0.42};
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };

    class FrlLoadingPicture: RscPicture {
      idc = 6999;
      text = "\pr\frl\addons\loadingScreen\images\default_01.paa";
      colorText[] = {1, 1, 1, 1};
      x = "safezoneX";
      y = "safezoneY - (safezoneW * 4/3) / 4";
      w = "safezoneW";
      h = "safezoneW * 4/3";
    };

    delete CA_Vignette;
    delete Noise;
    delete Line;
  };
};

class RscDisplayNotFreeze: RscStandardDisplay {
  idd = 105;
  scriptName="RscDisplayLoading";
	scriptPath="FRL_Interface";
	onLoad="[""onLoad"",_this,""RscDisplayLoading"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
	onUnload="[""onUnload"",_this,""RscDisplayLoading"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
  class controls {
    class MapBackTop: RscText {
      x = safezoneX;
      y = safezoneY;
      w = safezoneW;
      idc = 1000;
      h = SIZE_BAR_HEIGHT;
      colorBackground[] = COLOR_BAR;
    };

    delete MapAuthor;
    class MapBackBottom: RscText { // This is progressbackground, need to use existing control because of Z order
      idc = 1003;
      colorBackground[] = COLOR_BACKGROUND_PROGRESS;
      x = ORIGIN_X;
      y = ORIGIN_Y;
      w = safeZoneW;
      h = SIZE_LOAD_HEIGHT;
    };

    /*class FrlTopLine: RscText {
      x = safezoneX;
      y = safezoneY + SIZE_BAR_HEIGHT;
      w = safezoneW;
      idc = 6000;
      h = GRIDY(0.2);
      colorBackground[] = COLOR_BACKGROUND_PROGRESS;
    };*/


    class FrlLoadingBottom: RscText {
      idc = 6003;
      x = safezoneX;
      y = safezoneY + safeZoneH - GRIDY(5);
      w = safezoneW;
      h = SIZE_BAR_HEIGHT;
      colorBackground[] = COLOR_BAR;
    };

    class FrlLoadingStripe1: RscText {
      x = ORIGIN_X + GRIDX(0.2);
      y = ORIGIN_Y;
      w = GRIDX(0.2);
      h = SIZE_LOAD_HEIGHT;
      colorBackground[] = COLOR_BAR;
    };
    class FrlLoadingStripe2: FrlLoadingStripe1 {
      x = ORIGIN_X + GRIDX(0.6);
    };
    class FrlLoadingStripe3: FrlLoadingStripe1 {
      x = ORIGIN_X + GRIDX(1);
    };

    delete MapDescription;

    class FrlMissionDetails: RscControlsGroupNoScrollbars {
      idc = 6300;
      x = safeZoneX + safeZoneW - SIZE_BOX_WIDTH;
      y = 0;
      w = SIZE_BOX_WIDTH;
      h = safeZoneH;
      class Controls {
        class MissionDescriptionGroup: RscControlsGroupNoScrollbars {
          idc = 6100;
          x = 0;
          y = 0;
          w = SIZE_BOX_WIDTH;
          h = 0;
          class Controls {
            class DescriptionBackground: RscBackground {
              idc = 6501;
              x = 0;
              y = 0;
              w = SIZE_BOX_WIDTH;
              h = safeZoneH;
              colorBackground[] = COLOR_BACKGROUND_BOX;
            };
            class MissionDescription: RscStructuredText {
              class Attributes {
                font = FONTLIGHT;
                size = 1;
              };
              idc = 6502;
              x = GRIDX(2);
              y = GRIDY(1);
              size = GRIDY(1.8);
              sizeEx = GRIDY(1.8);
              w = SIZE_BOX_WIDTH - GRIDX(2);
              h = safeZoneH;
              colorBackground[] = {0, 0, 0, 0};
            };
          };
        };

        class GametypeDescriptionGroup: MissionDescriptionGroup {
          idc = 6200;
        };
      };
    };

    delete Mission;

    class ProgressMap: RscProgress {
      idc = 104;
      colorBar[] = COLOR_ACCENT;
      texture = "#(argb,8,8,3)color(1,1,1,1)";
      x = ORIGIN_X;
      y = ORIGIN_Y;
      w = safeZoneW;
      h = SIZE_LOAD_HEIGHT;
    };

    class ProgressMission: ProgressMap {
      idc = 1013;
      x = ORIGIN_X;
      y = ORIGIN_Y;
      w = safeZoneW;
      h = SIZE_LOAD_HEIGHT;
    };

    delete MapName;
    class FrlLoadingText: RscText {
      idc = 6001;
      x = ORIGIN_X + SIZE_SPACING;
      y = safezoneY + safeZoneH - GRIDY(5.9);
      w = safezoneW;
      h = GRIDY(1);
      sizeEx = GRIDY(1);
      font = FONTBOLD_ALT;
      colorText[] = {0, 0, 0, 1};
      text = "CONNECTING TO SERVER";
      //colorShadow[] = {1, 1, 1, 0.5};
      shadow = 0;
    };

    delete Disclaimer;
    class Logo3DEN: ctrlStaticPicture {
      text = "\a3\3DEN\Data\Logos\3den_512_ca.paa";
      x = safezoneX + safezoneW - GRIDX(6.5);
      y = safeZoneY + safeZoneH - GRIDY(4.5);
      w = GRIDX(6);
      h = GRIDY(4);
      show = 0;
      onLoad = "if !(isnull (finddisplay 313)) then {(_this select 0) ctrlshow true;};";
    };

    class FrlLogoFrontline: ctrlStaticPicture {
      text = "\pr\frl\addons\main\ui\frontline_logo_256.paa";
      x = safeZoneX + SIZE_SPACING;
      y = safeZoneY - GRIDY(0.8);
      w = GRIDX(20);
      h = GRIDY(20);
    };

    class FrlDonateText: RscText {
      style = 1;
      x = safeZoneX + safeZoneW - GRIDX(41);
      y = safeZoneY + GRIDY(1);
      w = GRIDX(40);
      h = GRIDY(1);
      sizeEx = GRIDY(1);
      colorText[] = {1, 1, 1, 0.5};
      font = FONTLIGHT;
      text = "Donate at www.patreon.com/frontline";
    };

    class FrlLoadingHints: RscText {
        idc = 6004;
        text = "";
        x = ORIGIN_X + SIZE_SPACING;
        y = safeZoneY + safeZoneH - SIZE_BAR_HEIGHT + GRIDY(1);
        w = safeZoneW;
        h = SIZE_LOAD_HEIGHT;
        sizeEx = GRIDY(1);
        font = FONTLIGHT;
        colorText[] = {1, 1, 1, 1};
    };
    class LoadingStart: RscControlsGroup {
      idc = 2310;
      x = "0 * safezoneW + safezoneX";
      y = "0 * safezoneH + safezoneY";
      w = "safezoneW";
      h = "safezoneW * 4/3";
      class VScrollbar: VScrollbar { width = 0;	};
      class HScrollbar: HScrollbar { height = 0; };
      class controls {
        class Black: RscText {
          idc = 1000;
          colorBackground[] = COLOR_BACKGROUND;
        };

        delete Noise;
        class FrlLoadingPicture: RscPicture {
          idc = 6998;
          text = "";
          colorText[] = {1, 1, 1, 1};
          x = 0;
          y = "-(safezoneW/4)";
          w = "safezoneW";
          h = "safezoneW * 4/3";
          onLoad = "(_this select 0) ctrlsettext (getText (configfile >> 'FRL' >> 'LoadingScreen' >> 'default'));";
        };

        class Logo: RscPictureKeepAspect {
          idc = 1200;
          text = "\pr\frl\addons\main\ui\frontline_logo_512.paa";
          x = "0.15 * safezoneW";
          y = "(0.5 - 0.35/2) * safezoneH";
          w = "0.7 * safezoneW";
          h = "0.35 * safezoneH";
          onLoad = "";
        };
      };
    };
  };


  class controlsBackground {
    class Black: RscText {
      colorBackground[] = {0, 0, 0, 1};
      x = "safezoneXAbs";
      y = "safezoneY";
      w = "safezoneWAbs";
      h = "safezoneH";
    };

    class Map: RscPicture {
      idc = 999;
      text = "#(argb,8,8,3)color(0,0,0,1)";
      colorText[] = {1, 1, 1, 0.42};
      x = 0;
      y = 0;
      w = 0;
      h = 0;
    };

    class FrlLoadingPicture: RscPicture {
      idc = 6999;
      text = "\pr\frl\addons\loadingScreen\images\default_01.paa";
      colorText[] = {1, 1, 1, 1};
      x = "safezoneX";
      y = "safezoneY - (safezoneW * 4/3) / 4";
      w = "safezoneW";
      h = "safezoneW * 4/3";
    };

    delete CA_Vignette;
    delete Noise;
    delete Line;
  };
};

class RscDisplayLoading {
	idd=102;
  scriptName="RscDisplayLoading";
	scriptPath="FRL_Interface";
	onLoad="[""onLoad"",_this,""RscDisplayLoading"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
	onUnload="[""onUnload"",_this,""RscDisplayLoading"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
};

class RscDisplayStart: RscStandardDisplay {
  scriptName="RscDisplayLoading";
	scriptPath="FRL_Interface";
  onLoad = "[2] call compile preprocessfilelinenumbers gettext (configfile >> 'CfgFunctions' >> 'init'); ['onLoad',_this,'RscDisplayLoading','FRL_Interface'] call (uinamespace getvariable 'BIS_fnc_initDisplay')";
  onUnload = "[""onUnload"",_this,""RscDisplayLoading"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
  idd = 104;
  class controls {
    class LoadingStart: RscControlsGroup {
      idc = 2310;
      x = "0 * safezoneW + safezoneX";
      y = "0 * safezoneH + safezoneY";
      w = "1 * safezoneW";
      h = "1 * safezoneH";
      class controls {

        class Black: RscText {
          idc = 1000;
          x = "0 * safezoneW";
          y = "0 * safezoneH";
          w = "1 * safezoneW";
          h = "1 * safezoneH";
          colorBackground[] = COLOR_BACKGROUND;
        };

        delete Noise;
        class Logo: RscPictureKeepAspect {
          idc = 1200;
          text = "\pr\frl\addons\main\ui\frontline_logo_512.paa";
          x = "0.15 * safezoneW";
          y = "(0.5 - 0.35/2) * safezoneH";
          w = "0.7 * safezoneW";
          h = "0.35 * safezoneH";
          onLoad = "";
        };
      };
    };
  };

  class controlsBackground {
    class Black: RscText {
      colorBackground[] = COLOR_BACKGROUND;
    };
    class Map: RscPicture {};
    delete CA_Vignette;
    delete Noise;
  };
};
