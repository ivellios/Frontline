#define __INTERACT_WIDTH GRIDX(10)

class EGVAR(interactthreed,menu) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['frl_interactthreed_menu', _this select 0];";
    fadeIn = 0;
    fadeOut = 0;
    class Controls {
        class CtrlGroup : RscControlsGroupNoScrollbars {
            idc = 1;
            x = 0;
            y = 0;
            w = GRIDX(10);
            h = 0;

            class Controls {
                class Background: RscBackground {
                    idc = 3;
                    colorBackground[] = {0, 0, 0, 0.8};
                    x = 0;
                    y = 0;
                    w = __INTERACT_WIDTH;
                    h = GRIDY(20);
                };

                class MainText: RscListBox {
                    idc = 2;
                    x = 0;
                    y = 0;
                    w = __INTERACT_WIDTH;
                    h = GRIDY(20);
                };
            };
        };
    };
};
