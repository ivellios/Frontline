#define ORIGIN_X safeZoneX + GRIDX(0.25)
#define ORIGIN_Y safeZoneY + GRIDY(0.25)
#define SIZE_WIDTH 			GRIDX(8)
#define SIZE_HEIGHT 		GRIDY(2)

class frl_Tickets {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['frl_Tickets', _this select 0];";
    class Controls {
      class CtrlGroup : RscControlsGroupNoScrollbars {
        idc = 1000;
        x = ORIGIN_X;
        y = ORIGIN_Y;
        w = SIZE_WIDTH;
        h = SIZE_HEIGHT;
        class Controls {
          class TicketsLeft : RscFrlTicketsBars {
            idc = 10;
            x = 0;
            y = 0;
          };

          class TicketsRight : TicketsLeft {
            idc = 20;
            x = GRIDX(4);
            y = 0;
          };
        };
      };
    };
};
