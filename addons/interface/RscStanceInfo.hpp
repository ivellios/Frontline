class RscStanceInfo {
  // idd = 303;
  // scriptName = "RscStanceInfo";
  // scriptPath = "IGUI";
  // onLoad = "[""onLoad"",_this,""RscStanceInfo"",'IGUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
  // onUnload = "[""onUnload"",_this,""RscStanceInfo"",'IGUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
  // controls[] = {"StanceIndicatorBackground", "StanceIndicator"};
  class StanceIndicatorBackground: RscPicture {
    colorText[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])", "(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"};
    idc = 1201;
    text = "\A3\Ui_f\data\IGUI\RscIngameUI\RscUnitInfo\gradient_ca.paa";
    x = "(profilenamespace getvariable [""IGUI_GRID_STANCE_X"",		((safezoneX + safezoneW) - 		(3.7 * 			(			((safezoneW / safezoneH) min 1.2) / 40)) - 0.5 * 			(			((safezoneW / safezoneH) min 1.2) / 40))])";
    y = "(profilenamespace getvariable [""IGUI_GRID_STANCE_Y"",		(safezoneY + 0.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))])";
    w = "(3.7 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
    h = "(3.7 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
  };
  class StanceIndicator: RscPictureKeepAspect {
    idc = 188;
    text = "\A3\Ui_f\data\IGUI\RscIngameUI\RscUnitInfo\SI_stand_ca.paa";
    x = "(profilenamespace getvariable [""IGUI_GRID_STANCE_X"",		((safezoneX + safezoneW) - 		(3.7 * 			(			((safezoneW / safezoneH) min 1.2) / 40)) - 0.5 * 			(			((safezoneW / safezoneH) min 1.2) / 40))])";
    y = "(profilenamespace getvariable [""IGUI_GRID_STANCE_Y"",		(safezoneY + 0.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))])";
    w = "(3.7 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
    h = "(3.7 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
  };
};
