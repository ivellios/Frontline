#define UPDATEBUTTON font = FONTLIGHT;\
fontSecondary = FONTLIGHT;\
period = 10;\
periodFocus = 10;\
periodOver = 10;\
style = 0;\
colorBackground[] = COLOR_BACKGROUND_BOX;\
colorBackground2[] = {0.2, 0.2, 0.2, 0.5};\
colorBackgroundFocused[] = {0.2, 0.2, 0.2, 0.5};\
color[] = {0.749, 0.749, 0.749, 1};\
colorFocused[] = {0.749, 0.749, 0.749, 1};\
color2[] = {0.749, 0.749, 0.749, 1};\
colorText[] = {0.749, 0.749, 0.749, 1};\
colorDisabled[] = {0.25, 0.25, 0.25, 1};\
colorSecondary[] = {0.749, 0.749, 0.749, 1};\
color2Secondary[] = {0.749, 0.749, 0.749, 1};\
colorFocusedSecondary[] = {0.749, 0.749, 0.749, 1};\
h = SIZE_BUTTON_HEIGHT;\
w = SIZE_BUTTON_WIDTH;\
size = GRIDY(1.4);\
sizeEx = GRIDY(1.4);\
class Attributes {\
    color = "#BFBFBF";\
    font = FONTLIGHT;\
    shadow = "false";\
    align = "left";\
};\
class TextPos {\
    left = GRIDX(0.8);\
    top = GRIDY(0.25);\
    right = 0.005;\
    bottom = 0;\
};

#define UPDATEBUTTON_HIGHLIGHT font = FONTBOLD;\
fontSecondary = FONTBOLD;\
period = 10;\
periodFocus = 10;\
periodOver = 10;\
style = 0;\
colorBackground[] = COLOR_ACCENT;\
colorBackground2[] = {"254/255", "200/255", "80/255", "1"};\
colorBackgroundFocused[] = {"254/255", "200/255", "80/255", "1"};\
color[] = {0, 0, 0, 1};\
colorFocused[] = {0, 0, 0, 1};\
color2[] = {0, 0, 0, 1};\
colorText[] = {0, 0, 0, 1};\
colorDisabled[] = {0.25, 0.25, 0.25, 1};\
colorSecondary[] = {0, 0, 0, 1};\
color2Secondary[] = {0, 0, 0, 1};\
colorFocusedSecondary[] = {0, 0, 0, 1};\
h = GRIDY(2.5);\
w = SIZE_BUTTON_WIDTH;\
size = GRIDY(1.7);\
sizeEx = GRIDY(1.7);\
class Attributes {\
    color = "#000000";\
    font = FONTBOLD;\
    shadow = "false";\
    align = "left";\
};\
class TextPos {\
    left = GRIDX(0.8);\
    top = GRIDY(0.4);\
    right = 0.005;\
    bottom = 0;\
};

class SVAR(recompileModule): RscControlsGroupNoScrollbars {
    #ifdef ISDEV
        x = safezoneX;
        y = safeZoneY + 4.2 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25);
        h = 1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25);
        w = SIZE_BUTTON_WIDTH;
        class Controls {
            class SelectionDropdown: RscCombo {
                idc = 101;
                x = 0;
                y = 0;
                w = SIZE_BUTTON_WIDTH * 0.65;
                h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                onLoad = "['load', _this] call frl_main_fnc_recompileModuleBox";
                onLBSelChanged = "['select', _this] call frl_main_fnc_recompileModuleBox";
            };
            class Button: RscButtonMenu {
                text = "RECOMPILE";
                idc = 102;
                x = SIZE_BUTTON_WIDTH * 0.65;
                y = 0;
                w = SIZE_BUTTON_WIDTH * 0.35;
                h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                action = "['recompile', _this] call frl_main_fnc_recompileModuleBox";
            };
        };
    #endif

    #ifndef ISDEV
        x = 0;
        y = 0;
        h = 0;
        w = 0;
        class Controls { };
    #endif
};

class RscDisplayMission: RscDisplayEmpty {
    class Controls {
        class FrlBackground: RscBackground {
            idc = 86011;
            x = safeZoneX;
            y = safeZoneY;
            w = safeZoneW;
            h = safeZoneH;
            colorBackground[] = {0,0,0,0};
            fade = 1;
            onLoad = "(_this select 0) ctrlSetFade 1; (_this select 0) ctrlCommit 0;";
        };
    };
};


class RscDisplayMPInterrupt: RscStandardDisplay {
    scriptName="RscDisplayMPInterrupt";
    scriptPath="FRL_Interface";
    onLoad = "[""onLoad"",_this,""RscDisplayMPInterrupt"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay'); ['RscDisplayMPInterrupt', true, 0.25, 1.4] call FRL_Main_fnc_blurBackground;";
    onUnload = "[""onUnload"",_this,""RscDisplayMPInterrupt"",'FRL_Interface'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay'); ['RscDisplayMPInterrupt', false] call FRL_Main_fnc_blurBackground;";
    class ControlsBackground {
        /*class FrlBackground: RscBackground {
            x = safeZoneX;
            y = safeZoneY;
            w = safeZoneW;
            h = safeZoneH;
            colorBackground[] = {0,0,0,0};
            fade = 1;
            onLoad = "(_this select 0) ctrlSetFade 1; (_this select 0) ctrlCommit 0; (_this select 0) ctrlSetBackgroundColor [0,0,0,0.5]; (_this select 0) ctrlSetFade 0; (_this select 0) ctrlCommit 0.25;";
        };*/
        delete Vignette;
        delete TileGroup;
        delete TitleBackground;
        delete MissionNameBackground;
        delete Pause2;
        delete Pause1;
    };
    class controls {
        // -- Frontline settings buttons
        class SVAR(Settings): SVAR(SettingsButton) {};
        class SVAR(Voting): SVAR(VotingButton) {};
        class SVAR(Keybinds): SVAR(KeybindsButton) {};
        class SVAR(Admin): SVAR(AdminButton) {};
        class SVAR(recompile): SVAR(recompileModule) {};

        delete Title;
        delete MissionTitle;
        delete PlayersName;
        delete Version;
        delete TraffLight;
        delete Feedback;

        class ButtonCancel: RscButtonMenu { // Continue button
            x = safeZoneX;
            y = safeZoneY + safeZoneH - GRIDY(2.4);
            text = "RESUME";
            UPDATEBUTTON_HIGHLIGHT
        };
        delete ButtonSAVE;
        delete ButtonSkip;
        class ButtonRespawn: RscButtonMenu {
            x = safeZoneX;
            y = BUTTON_POS(5);
            text = "RESPAWN";
            UPDATEBUTTON
        };
        class ButtonOptions: RscButtonMenu {
            y = BUTTON_POS(3);
            x = safeZoneX;
            text = "CONFIGURE";
            UPDATEBUTTON
        };
        class ButtonVideo: RscButtonMenu {
            y = BUTTON_POS(6);
            x = safeZoneX + SIZE_BUTTON_WIDTH + GRIDX(0.2);
            text = "VIDEO";
            UPDATEBUTTON
        };
        class ButtonAudio: RscButtonMenu {
            y = BUTTON_POS(5);
            x = safeZoneX + SIZE_BUTTON_WIDTH + GRIDX(0.2);
            text = "AUDIO";
            UPDATEBUTTON
        };
        class ButtonControls: RscButtonMenu {
            y = BUTTON_POS(4);
            x = safeZoneX + SIZE_BUTTON_WIDTH + GRIDX(0.2);
            text = "CONTROLS";
            UPDATEBUTTON
        };
        class ButtonGame: RscButtonMenu {
            y = BUTTON_POS(3);
            x = safeZoneX + SIZE_BUTTON_WIDTH + GRIDX(0.2);
            text = "GAME";
            UPDATEBUTTON
        };
        class ButtonTutorialHints: RscButtonMenu {
            x = safeZoneX;
            y = BUTTON_POS(4);
            text = "FIELD MANUAL";
            UPDATEBUTTON
        };
        class ButtonAbort: RscButtonMenu {
            x = safeZoneX;
            y = BUTTON_POS(2);
            text = "DISCONNECT";
            UPDATEBUTTON
        };

        class DebugConsole: RscDebugConsole {
            x="safeZoneX + safeZoneW - (22.5 * (((safezoneW / safezoneH) min 1.2) / 40))";
            y="(safezoneY + safezoneH - (((safezoneW / safezoneH) min 1.2) / 1.2))";
            w="22 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
            h="21.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
        };
    };
};

class RscDisplayInterrupt: RscStandardDisplay {
    class controls {
        class SVAR(Settings): SVAR(SettingsButton) {};
        class SVAR(Voting): SVAR(VotingButton) {};
        class SVAR(Keybinds): SVAR(KeybindsButton) {};
        class SVAR(Admin): SVAR(AdminButton) {};
        class SVAR(recompile): SVAR(recompileModule) {};
    };
};
class RscDisplayInterruptEditor3D: RscStandardDisplay {
    class controls {
        class SVAR(Settings): SVAR(SettingsButton) {};
        class SVAR(Voting): SVAR(VotingButton) {};
        class SVAR(Keybinds): SVAR(KeybindsButton) {};
        class SVAR(Admin): SVAR(AdminButton) {};
        class SVAR(recompile): SVAR(recompileModule) {};
    };
};
class RscDisplayInterruptEditorPreview: RscStandardDisplay {
    class controls {
        class SVAR(Settings): SVAR(SettingsButton) {};
        class SVAR(Voting): SVAR(VotingButton) {};
        class SVAR(Keybinds): SVAR(KeybindsButton) {};
        class SVAR(Admin): SVAR(AdminButton) {};
        class SVAR(recompile): SVAR(recompileModule) {};
    };
};
