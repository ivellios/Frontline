/*
    Function:       FRL_Maintenance_fnc_clientInitRepair
    Author:         Adanteh
    Description:    Adds client events for repairing
*/
#include "macros.hpp"

// -- Locks vehicle and starts arming loop on server (Local to caller)
[QGVAR(repairStartParts), {
    (_this select 0) params ["_vehicle", "_partsToRepair"];
    if (_partsToRepair isEqualTo []) exitWith { };
    [QGVAR(lockVehicle), _vehicle, [_vehicle, Clib_Player, true]] call CFUNC(targetEvent);
    [QGVAR(repairStartPartServer), [_vehicle, _partsToRepair, Clib_Player]] call CFUNC(serverEvent);
}] call CFUNC(addEventHandler);


[QGVAR(repairStartFull), {
    (_this select 0) params ["_vehicle"];
    private _timeRequired = 60;
    private _vehicleName = getText (configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "displayName");
    if (count _vehicleName > 16) then {
        _vehicleName = (_vehicleName select [0, 13]) + "...";
    };
    [QGVAR(showRepairProgress), [_vehicleName, _timeRequired]] call CFUNC(localEvent);
    [QGVAR(lockVehicle), _vehicle, [_vehicle, Clib_Player, true]] call CFUNC(targetEvent);
    [QGVAR(repairStartFullServer), [_vehicle, _timeRequired, Clib_Player]] call CFUNC(serverEvent);
}] call CFUNC(addEventHandler);


[QGVAR(showRepairProgress), {
    (_this select 0) params ["_partName", "_timeRequired"];
    private _timeEnd = serverTime + _timeRequired;
    private _timeStart = serverTime;
    ["progressHudShow", [QGVAR(repairProgress),
    {
        params ["_partName"];
        (_controlsGroup controlsGroupCtrl 2) ctrlSetText TEXTURE(maintenance_repair.paa);
        (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Repairing %1", _partName]);
        (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor (["GUI", "BCG_RGB"] call BIS_fnc_displayColorGet);
    },
    {
        params ["", "_timeStart", "_timeRequired"];
        private _percentage = (serverTime - _timeStart) / _timeRequired;
        (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
    },
    [_partName, _timeStart, _timeRequired]]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);
