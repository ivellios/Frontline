/*
    Function:       FRL_Maintenance_fnc_getAmmoRearmTime
    Author:         Adanteh
    Description:    Gets time required to rearm magazine of this type.
    Example:        [""] call FRL_Maintenance_fnc_getAmmoRearmTime;
    Params:
        0: Magazine class <STRING>
*/
#include "macros.hpp"

params ["_magClass"];

private _magCfg = (configFile >> "cfgMagazines" >> _magClass);
private _timeRequired = 0;
if (isClass (_magCfg >> "FRL_VehicleMaintenance")) then {
    private _timeRequired = getNumber (_magCfg >> "FRL_VehicleMaintenance" >> "RearmTime");
};
if (_timeRequired != 0) exitWith {
    _timeRequired;
};

private _nameSound = toLower (getText (_magCfg >> "nameSound"));
private _ammoCount = (getNumber (_magCfg >> "count"));
_timeRequired = switch (_nameSound) do {
    case "sabot": { [6 /*per shell*/, 20] select (_ammoCount > 32) };
    case "heat": { [4 /*per shell*/, 40] select (_ammoCount > 32) };
    case "mgun": { [5, 20] select (_ammoCount > 300) };
    case "cannon": { [20, 40] select (_ammoCount > 100) };
    case "missiles": { [(10 * _ammoCount), 25] select (_ammoCount > 4) };
    case "smokeshell": { 15 };
    default {
        if (_ammoCount < 2) exitWith { 10 };
        if (_ammoCount <= 16) exitWith { 20 };
        if (_ammoCount <= 20) exitWith { 30 };
        if (_ammoCount <= 24) exitWith { 40 };
        10
    };
};
_timeRequired
