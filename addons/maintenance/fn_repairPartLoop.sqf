/*
    Function:       FRL_Maintenance_fnc_repairPartLoop
    Author:         Adanteh
    Description:    Handles repairing vehicle parts over time.
    Example:        [cursorTarget] call FRL_Maintenance_fnc_repairPartLoop;
    Locality:       Server
*/
#include "macros.hpp"

params ["_vehicle", "_partsToRepair"];
private _partInfo = _partsToRepair deleteAt 0;
_partInfo params ["_hitPointName", "_hitSelection", "_hitIndex", "_hitDamage", "_timeRequired"];

[QGVAR(repairPart), _vehicle, [_vehicle, _hitIndex, 0]] call CFUNC(targetEvent);

if !(_partsToRepair isEqualTo []) then {
    (_partsToRepair select 0) params ["_hitPointName", "_hitSelection", "_hitIndex", "_hitDamage", "_timeRequired"];
    [FUNC(repairPartLoop), _timeRequired, [_vehicle, _partsToRepair]] call CFUNC(wait);
    private _partName = _hitPointName;
    [QGVAR(showRepairProgress), (_vehicle getVariable [QGVAR(caller), objNull]), [_partName, _timeRequired]] call CFUNC(targetEvent);
} else {
    [QGVAR(repairEnd), [_vehicle]] call CFUNC(localEvent);
    ["progressHudHide", (_vehicle getVariable [QGVAR(caller), objNull]), [QGVAR(repairProgress)]] call CFUNC(targetEvent);
};
