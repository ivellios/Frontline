/*
    Function:       FRL_Maintenance_fnc_clientInit
    Author:         Adanteh
    Description:    Advanced function to retrieve vehicle weapons and (supported) magazines.
*/
#define __INCLUDE_DIK
#include "macros.hpp"

// -- Ask the commander of the vehicle to start maintenance when entering service pad (Detected by server)
[QGVAR(padEnter), {
    (_this select 0) params ["_marker", "_side", "_base"];
    [[Clib_Player, vehicle Clib_Player, "Would you like to start maintenance?", {
        params ["_unit", "_vehicle", "_answer"];
        if ( (getPosATL _vehicle) select 2 < 1.5 ) then {
            switch (_answer) do {
                case 1: {
                    [_vehicle] call FUNC(rearmFull);
                    [_vehicle] call FUNC(repairFull);
                    [_vehicle] call EFUNC(Fortifications,loadSupplies);
                };
                case 2: {
                    [_vehicle] call FUNC(repairFull);
                };
                case 3: {
                    [_vehicle] call FUNC(rearmFull);
                };
                case 4: {
                    [_vehicle] call EFUNC(Fortifications,loadSupplies);
                };
            };
        } else {
            [QGVAR(padEnter), [_marker, _side, _base]] call CFUNC(localEvent);
            ["showNotification", ["Vehicle not safely landed", "nope"]] call CFUNC(localEvent);
        };

    }, 15, [
        [DIK_F5, DIK_F6, DIK_F7, DIK_F8],
        [1, 2, 3, 4],
        ["Yes (Full)", "Yes (Repair)", "Yes (Rearm)", "Load Supplies"]
    ]]] call EFUNC(voting,voteHUD);
    // TODO Add a 'condition' to autohide voting hud when condition is false, in this case that would mean exitting the pad
}] call CFUNC(addEventHandler);

if !(isNil QEFUNC(interactThreeD,menuOptionAdd)) then {
    ["vehicle", [
        "",
        "Rearm",
        { [_this select 1] call FUNC(rearmFull) },
        { ["rearm", _this, false] call FUNC(canMaintenance) },
        true,
        "advanced"
    ]] call EFUNC(interactThreeD,menuOptionAdd);

    ["vehicle", [
        "",
        "Repair",
        { [_this select 1] call FUNC(repairFull) },
        { ["repair", _this, false] call FUNC(canMaintenance) },
        true,
        "advanced"
    ]] call EFUNC(interactThreeD,menuOptionAdd);
};
