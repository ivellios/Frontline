/*
    Function:       FRL_Maintenance_fnc_rearmMagLoop
    Author:         Adanteh
    Description:    Handles adding mag by mag over time.
    Example:        [cursorTarget] call FRL_Maintenance_fnc_rearmMagLoop;
    Locality:       Server
*/
#include "macros.hpp"

params ["_vehicle", "_magsToRearm"];
private _magInfo = _magsToRearm deleteAt 0;
_magInfo params ["_magClass", "_rearmQuantity", "_turretPath", "_removeUsed", "_simulated", "_timeRequired"];

if (_removeUsed > 0) then {
    for "_i" from 1 to _removeUsed do {
        _vehicle removeMagazineTurret [_magClass, _turretPath];
    };
};
if (_simulated) then {
    [QGVAR(rearmMag), _vehicle turretOwner _turretPath, [_vehicle, _magClass, _turretPath, _rearmQuantity]] call CFUNC(targetEvent);
} else {
    for "_i" from 1 to _rearmQuantity do {
        [QGVAR(rearmMag), _vehicle turretOwner _turretPath, [_vehicle, _magClass, _turretPath]] call CFUNC(targetEvent);
    };
};
// -- Readd empty magazines (This is needed, because removeMagazineTurret would delete empty magazines that we want to keep)
if (_removeUsed > _rearmQuantity) then {
    for "_i" from 1 to (_removeUsed - _rearmQuantity) do {
        [QGVAR(rearmMag), _vehicle turretOwner _turretPath, [_vehicle, _magClass, _turretPath, 0]] call CFUNC(targetEvent);
    };
};

if !(_magsToRearm isEqualTo []) then {
    (_magsToRearm select 0) params ["_magClass", "_rearmQuantity", "_turretPath", "_removeUsed", "_simulated", "_timeRequired"];
    [FUNC(rearmMagLoop), _timeRequired, [_vehicle, _magsToRearm]] call CFUNC(wait);
    [QGVAR(showRearmProgress), (_vehicle getVariable [QGVAR(caller), objNull]), [_magClass, _timeRequired]] call CFUNC(targetEvent);
} else {
    [QGVAR(rearmEnd), [_vehicle]] call CFUNC(localEvent);
    ["progressHudHide",  (_vehicle getVariable [QGVAR(caller), objNull]), [QGVAR(rearmProgress)]] call CFUNC(targetEvent);
};
