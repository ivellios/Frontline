/*
    Function:       FRL_Maintenance_fnc_init
    Author:         Adanteh
    Description:    Advanced function to retrieve vehicle weapons and (supported) magazines.
*/
#include "macros.hpp"

[QGVAR(lockVehicle), FUNC(lockVehicle)] call CFUNC(addEventHandler); // Locality: _vehicle
[QGVAR(rearmMag), { // Locality: _turretPath
    (_this select 0) params ["_vehicle", "_magClass", "_turretPath", ["_ammoCount", -1]];
    private _args = [_magClass, _turretPath];
    if (_ammoCount != -1) then {
        _args pushBack _ammoCount;
    };
    _vehicle addMagazineTurret _args;
}] call CFUNC(addEventHandler);

[QGVAR(repairPart), { // Locality: _vehicle
    (_this select 0) params ["_vehicle", "_hitPointIndex", ["_damage", 0]];
    _vehicle setHitIndex [_hitPointIndex, _damage];
}] call CFUNC(addEventHandler);

[QGVAR(repairMobility), FUNC(repairMobility)] call CFUNC(addEventHandler);

[QGVAR(setDamage), {
    (_this select 0) params ["_vehicle", ["_damage", 0], ["_addToolkit", true]];
    _vehicle setDamage _damage;
    if (_addToolkit) then {
        if (({ _x == __ITEMNAME} count (itemCargo _vehicle)) == 0) then {
            _vehicle addItemCargoGlobal [__ITEMNAME, 1];
        };
    };
}] call CFUNC(addEventHandler);
