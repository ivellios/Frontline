/*
    Function:       FRL_Maintenance_fnc_clientInitRepair
    Author:         Adanteh
    Description:    Advanced function to retrieve vehicle weapons and (supported) magazines.
*/
#include "macros.hpp"

// -- Unlocks vehicle and notifies the crew (Local to server)
[QGVAR(repairEnd), {
    (_this select 0) params ["_vehicle"];
    [QGVAR(lockVehicle), _vehicle, [_vehicle, Clib_Player, false]] call CFUNC(targetEvent);
}] call CFUNC(addEventHandler);

// -- Starts repairing loop for parts (Local to server)
[QGVAR(repairStartPartServer), {
    (_this select 0) params ["_vehicle", "_partsToRepair", "_caller"];
    _vehicle setVariable [QGVAR(caller), _caller];
    _partInfo params ["_hitPointName", "_hitSelection", "_hitIndex", "_hitDamage", "_timeRequired"];
    private _partName = _hitPointName;
    [FUNC(repairPartLoop), _timeRequired, [_vehicle, _partsToRepair]] call CFUNC(wait);
    [QGVAR(showRepairProgress), _caller, [_partName, _timeRequired]] call CFUNC(targetEvent);
}] call CFUNC(addEventHandler);

// -- Starts full repair (Local to server)
[QGVAR(repairStartFullServer), {
    (_this select 0) params ["_vehicle", "_timeRequired"];
    [{
        params ["_vehicle"];
        [QGVAR(setDamage), _vehicle, [_vehicle]] call CFUNC(targetEvent);
        [QGVAR(repairEnd), [_vehicle]] call CFUNC(localEvent);
        ["progressHudHide", (_vehicle getVariable [QGVAR(caller), objNull]), [QGVAR(repairProgress)]] call CFUNC(targetEvent);
    }, _timeRequired, [_vehicle]] call CFUNC(wait);
}] call CFUNC(addEventHandler);
