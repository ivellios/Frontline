class CfgWeapons {
    class ItemCore;
    class InventoryItem_Base_F;
    class ToolKitItem: InventoryItem_Base_F {
			type = 620;
			allowedSlots[] = {701,801,901};
		};
    class ToolKit: ItemCore {
			class ItemInfo: ToolKitItem {
				mass = 10;
				uniformModel = "\A3\Weapons_F\Items\Toolkit";
			};
		}
};
