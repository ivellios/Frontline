/*
    Function:       FRL_Maintenance_fnc_serverInitRearm
    Author:         Adanteh
    Description:    Advanced function to retrieve vehicle weapons and (supported) magazines.
*/
#include "macros.hpp"

// -- Unlocks vehicle and notifies the crew (Local to server)
[QGVAR(rearmEnd), {
    (_this select 0) params ["_vehicle"];
    [QGVAR(lockVehicle), _vehicle, [_vehicle, Clib_Player, false]] call CFUNC(targetEvent);
}] call CFUNC(addEventHandler);

// -- Starts arming loop (Local to server)
[QGVAR(rearmStartServer), {
    (_this select 0) params ["_vehicle", "_magsToRearm", "_caller"];
    _vehicle setVariable [QGVAR(caller), _caller];
    (_magsToRearm select 0) params ["_magClass", "_rearmQuantity", "_turretPath", "_removeUsed", "_simulated", "_timeRequired"];
    [FUNC(rearmMagLoop), _timeRequired, [_vehicle, _magsToRearm]] call CFUNC(wait);
    [QGVAR(showRearmProgress), _caller, [_magClass, _timeRequired]] call CFUNC(targetEvent);
}] call CFUNC(addEventHandler);
