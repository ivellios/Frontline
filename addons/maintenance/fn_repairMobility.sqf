/*
    Function:       FRL_Maintenance_fnc_repairMobility
    Author:         Adanteh
    Description:    Repair the parts on vehicle required to be mobile
    Example:        [cursorTarget] call FRL_Maintenance_fnc_repairMobility;
*/
#include "macros.hpp"

#define __REPAIRAMOUNT 0.3
(_this select 0) params [["_vehicle", objNull, [objNull]]];
private _hitPoints = getAllHitPointsDamage _vehicle;
if (_hitpoints isEqualTo []) exitWith { };

private _repairParts = [
    "hitengine", "hitengine1", "hitengine2", "hitengine3",
    "hitltrack", "hitrtrack",
    "hithrotor", "hitvrotor", "hitengine", "hitfuel",
    "hitlfwheel", "hitlf2wheel", "hitlmwheel", "hitlbwheel",
    "hitrfwheel", "hitrf2wheel", "hitrmwheel", "hitrbwheel"];
{
    if (toLower _x in _repairParts) then {
        private _hit = (_hitpoints select 2) select _forEachIndex;
        if (_hit > __REPAIRAMOUNT) then {
            _vehicle setHitPointDamage [_x, __REPAIRAMOUNT];
        };
    };
} forEach (_hitPoints select 0);
if (fuel _vehicle < __REPAIRAMOUNT) then {
    _vehicle setFuel 0.5;
};

_vehicle setVelocity [0, 0, 0.5];
