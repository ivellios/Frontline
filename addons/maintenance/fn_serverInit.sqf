/*
    Function:       FRL_Maintenance_fnc_serverInit
    Author:         Adanteh
    Description:    Advanced function to retrieve vehicle weapons and (supported) magazines.
    Example:        [cursorTarget, false] call FRL_Maintenance_fnc_serverInit;
*/
#include "macros.hpp"

GVAR(allMaintenancePads) = [];
GVAR(maintenanceNamespace) = call CFUNC(createNamespace);

["missionStarted", {
    private _allMarkers = +allMapMarkers;
    {
        if (toLower _x find "maintenance_" != -1) then {
            private _side = switch (markerColor _x) do {
                case "ColorWEST": {west};
                case "ColorEAST": {east};
                case "ColorGUER": {independent};
                default {sideUnknown};
            };
            if (_side != sideUnknown) then {
                [QGVAR(prepareMaintenancePad), [_x, _side, true]] call CFUNC(localEvent);
            };
        };
    } count _allMarkers;
}] call CFUNC(addEventHandler);

[QGVAR(prepareMaintenancePad), {
    (_this select 0) params ["_marker", "_side", ["_base", false]];
    _marker setMarkerBrush QEGVAR(Main,FShaded2);
    GVAR(allMaintenancePads) pushBackUnique [_marker, _side, _base];
    GVAR(maintenanceNamespace) setVariable [(format ["%1_units", _marker]), []];
    private _markerText = markerText _marker;
    if (_markerText == "") then {
        _marker setMarkerText "Service Point";
    };
}] call CFUNC(addEventHandler);

[QGVAR(padEnterServer), {
    (_this select 0) params ["_unit", "_marker", "_side", "_base"];
    if (side group _unit == _side) then {
        if (vehicle _unit != _unit) then {
            private _vehicle = vehicle _unit;
            if (effectiveCommander _vehicle == _unit) then {
                private _canService = call {
                    if (_vehicle isKindOf "Car") exitWith { true };
                    if (_vehicle isKindOf "Tank") exitWith { true };
                    if (_vehicle isKindOf "Air") exitWith { true };
                    if (_vehicle isKindOf "Motorcycle") exitWith { true };
                    if (_vehicle isKindOf "Ship") exitWith { true };
                    false
                };
                if (_canService) then {
                    _vehicle setVariable [QGVAR(onPad), true, true];
                    [QGVAR(padEnter), _unit, [_marker, _side, _base]] call CFUNC(targetEvent);
                };
            };
        };
    };
}] call CFUNC(addEventhandler);

[QGVAR(padLeaveServer), {
    (_this select 0) params ["_unit", "_marker"];
    private _vehicle = vehicle _unit;
    if (_vehicle getVariable [QGVAR(onPad), true]) then {
        _vehicle setVariable [QGVAR(onPad), false, true];
    };
}] call CFUNC(addEventhandler);

[{
    if (isNil QGVAR(allMaintenancePads)) exitWith {};
    private _allPlayers = +allPlayers;
    {
        _x params ["_marker", "_side", "_base"];
        private _unitsInside = [];
        private _unitVar = (format ["%1_units", _marker]);
        private _oldUnits = +(GVAR(maintenanceNamespace) getVariable _unitVar);
        {
            private _player = _x;
            if (_player inArea _marker) then {
                _unitsInside pushBackUnique _player;
            };
            nil;
        } count _allPlayers;
        {
            [QGVAR(padEnterServer), [_x, _marker, _side, _base]] call CFUNC(localEvent);
            nil;
        } count (_unitsInside - _oldUnits);
        {
            [QGVAR(padLeaveServer), [_x, _marker, _side, _base]] call CFUNC(localEvent);
            nil;
        } count (_oldUnits - _unitsInside);
        GVAR(maintenanceNamespace) setVariable [_unitVar, _unitsInside];
    } count GVAR(allMaintenancePads);
}, 0.1, []] call MFUNC(addPerFramehandler);
