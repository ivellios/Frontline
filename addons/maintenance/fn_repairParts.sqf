/*
    Function:       FRL_Maintenance_fnc_repairParts
    Author:         Adanteh
    Description:    Does a repair of parts on the vehicle
    Example:        [cursorTarget] call FRL_Maintenance_fnc_repairParts;
*/
#include "macros.hpp"

params [["_vehicle", objNull, [objNull]]];
private _hitPoints = getAllHitPointsDamage _vehicle;
private _repairParts = [];
{
    private _hitPointName = _x;
    private _hitSelection = (_hitPoints select 1) select _forEachIndex;
    private _hitDamage = (_hitPoints select 2) select _forEachIndex;
    private _timeRequired = 3;
    if (_hitDamage > 0) then {
        _repairParts pushBack [_hitPointName, _hitSelection, _forEachIndex, _hitDamage, _timeRequired];
    };
} forEach (_hitPoints select 0);
if !(_repairParts isEqualTo []) then {
    [QGVAR(repairStartParts), [_vehicle, _repairParts]] call CFUNC(localEvent);
};
