/*
    Function:       FRL_Maintenance_fnc_repairFull
    Author:         Adanteh
    Description:    Does a full repair on the vehicle
    Example:        [cursorTarget] call FRL_Maintenance_fnc_repairFull;
*/
#include "macros.hpp"

params [["_vehicle", objNull, [objNull]]];

private _undamaged = (damage _vehicle == 0);
if (_undamaged) then {
    {
        private _dmg = _vehicle getHitPointDamage _x;
        if (!(isNil "_dmg") && {_dmg > 0.15 }) exitWith {
            _undamaged = false;
        };
    } forEach [
        "hitengine", "hitengine1", "hitengine2", "hitengine3",
        "hitltrack", "hitrtrack",
        "hithrotor", "hitvrotor", "hitengine", "hitfuel",
        "hitlfwheel", "hitlf2wheel", "hitlmwheel", "hitlbwheel",
        "hitrfwheel", "hitrf2wheel", "hitrmwheel", "hitrbwheel",
        "HitMainSight", "HitComSight", "HitTurret", "HitGun"];
};
if (_undamaged) exitWith {
    ["showNotification", ["Vehicle doesn't need repairing", TEXTURE(maintenance_repair.paa), "orange"]] call CFUNC(localEvent);
};

["showNotification", ["Started repairing", TEXTURE(maintenance_repair.paa), "lime"]] call CFUNC(localEvent);
[QGVAR(repairStartFull), [_vehicle]] call CFUNC(localEvent);
