/*
    Function:       FRL_Mainteance_fnc_getTurrets
    Author:         Adanteh
    Description:    Gets turrets of vehicles and executes code over each turret
    Example:        [cursorTarget, false] call FRL_Mainteance_fnc_getTurrets;
*/
#include "macros.hpp"

params [
    ["_vehicle", objNull, ["", objNull]],
    ["_includeCargo", false],
    ["_fnc_return", { (_this select 1) }, [{}]]
];

private _vehTurretPath = [];
// -- 'Real turret' (New Method), for objects only
if (_vehicle isEqualType objNull) then {
    private _turrets = [[-1]]; // Make sure driver turret is included
    _turrets append (allTurrets [_vehicle, _includeCargo]);
    {
        private _turret = _x;
        _vehTurretPath pushBack ([_vehicle, _x] call _fnc_return);
        nil;
    } count _turrets;
} else {
    // -- Old method, with config lookup, works for vehicle classes too
    private _fnc_getTurrets = { // -- Recursive function
        params ["_vehicle", "_turretPath"];
        if (isclass (_turretsCfg >> "turrets")) then {
            private _subTurrets = (_turretsCfg >> "turrets");
            if (count _subTurrets > 0) then {
                for "_t" from 0 to (count _subTurrets - 1) do {
                    private _turret = _subTurrets select _t;
                    if (isclass _turret) then {
                        private _hasWeapons = (isArray (_turret >> "weapons") && {count getArray (_turret >> "weapons") > 0});
                        private _hasGunner = (getnumber (_turret >> "hasGunner") > 0);
                        if (_hasWeapons || _hasGunner) then {
                            _isPersonTurret = (getnumber (_turret >> "isPersonTurret") > 0);
                            if (!_isPersonTurret || _includeCargo) then {
                                _commandReturn = ([_vehicle, (_turretPath + [_t]), _turret] call _fnc_return);
                                _vehTurretPath pushBack _commandReturn;
                            };
                        };
                        [_vehicle, (_turretPath + [_t]), _turret] call _fnc_getTurrets;
                    };
                };
            };
        };
    };
    private _vehicleType = if (_vehicle isEqualType objNull) then { typeOf _vehicle; } else { _vehicle };
    private _vehConfig = configfile >> "cfgvehicles" >> _vehicleType;
    private _hasWeapons = (isArray (_vehConfig >> "weapons") && {count getArray (_vehConfig >> "weapons") > 0});
    if (_hasWeapons) then {
        _vehTurretPath pushBack ([_vehicle, ([-1]), _vehConfig] call _fnc_return);
    };
    [_vehicle, _vehConfig, []] call _fnc_getTurrets;
};

_vehTurretPath;
