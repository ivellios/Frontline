/*
    Function:       FRL_Maintenance_fnc_canMaintenance
    Author:         Adanteh
    Description:    Checks if you can use a maintenance option
*/
#include "macros.hpp"

params ["_type", ["_args", []], ["_quiet", false]];
_args params ["_caller", "_vehicle"];

private _canMaintenance = true;
private _message = "";

if !(alive _vehicle) then {
    _canMaintenance = false;
    _message = "Vehicle is destroyed";
} else {

    private _onPad = _vehicle getVariable [QGVAR(onPad), false];
    if !(_onPad) then {
        _canMaintenance = false;
        _message = "Vehicle is not on a maintenace pad<br />Check map for diagonal shaded marker";
    } else {
        switch (toLower _type) do {
            case "repair": {

            };

            case "rearm": {

            };
        };
    };
};

#ifdef DEBUGFULL
    if (true) exitWith { true };
#endif

if (!_canMaintenance && !_quiet) then {
    ["showNotification", [_message, "nope"]] call CFUNC(localEvent);
};


_canMaintenance
