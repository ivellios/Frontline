/*
    Function:       FRL_Maintenance_fnc_getTurrets
    Author:         Adanteh
    Description:    Advanced function to retrieve vehicle weapons and (supported) magazines.
    Example:        [cursorTarget, false] call FRL_Maintenance_fnc_getTurrets;
    Params:
        0: Vehicle <OBJECT/STRING>
        1: True for current weapons on vehicle or false config entries (Default: False) <BOOLEAN>
        2: True for current magazines on vehicle or false config entries (Default: False) <BOOLEAN>
        3: True for  all supported ammo types by weapons, instead of the one normally assigned (Default: False) <BOOLEAN>
*/
#include "macros.hpp"

params [["_vehicle", objNull, ["", objNull]], ["_realWeapons", false], ["_realMagazines", false], ["_allSupported", false]];

private _fnc_weaponAndMagazineRetrieve = { // -- Recursive function
    params ["_vehicle", "_turretPath", "_turretConfig"];

	private _turretWeapons = [(getArray (_turretConfig >> "Weapons")), (_vehicle weaponsTurret _turretPath)] select _realWeapons;
	private _turretMagazineArray = [(getArray (_turretConfig >> "Magazines")), (_vehicle magazinesTurret _turretPath)] select _realMagazines;
	_turretWeapons = _turretWeapons - (["", "FakeWeapon", "CarHorn", "BikeHorn", "TruckHorn", "TruckHorn2", "SportCarHorn", "MiniCarHorn", "TruckHorn3"]);

	private _turretCombinedArray = [];
	private _supportedTurretMagazines = [];

	{
		private _weapon = _x;
		private _supportedTurretMagazines = (getArray (configFile >> "CfgWeapons" >> _weapon >> "Magazines"));
		private _muzzles = getArray (configFile >> "cfgWeapons" >> _weapon >> "muzzles");
		{
			_muzzleMags = getArray (configFile >> "cfgWeapons" >> _weapon >> _x >> "magazines");
			_supportedTurretMagazines append _muzzleMags;
            nil;
		} count _muzzles;
		if (_allSupported) then {
			_turretCombinedArray pushBack [_x, _supportedTurretMagazines];
		} else {
			private _weaponMagazineArray = [];
			{
				private _magType = _x;
				if (({_magType == _x} count _supportedTurretMagazines) > 0) then {
					_weaponMagazineArray pushBack _magType;
				};
                nil;
			} count _turretMagazineArray;
			_turretCombinedArray pushBack [_x, _weaponMagazineArray];
		};
        nil;
	} count _turretWeapons;
	[_turretPath, _turretCombinedArray];
};

if (_vehicle isEqualType "") then {
	_realWeapons = false;
	_realMagazines = false;
};

([_vehicle, _fnc_weaponAndMagazineRetrieve] call FUNC(getTurrets));
