/*
    Function:       FRL_Maintenance_fnc_clientInitRepairUI
    Author:         Adanteh
    Description:    Adds events / sets vars for the left-mouse hold mobility repairs using toolkit
*/
#include "macros.hpp"

//["Car"] call EFUNC(main,addInteractType);
//["Tank"] call EFUNC(main,addInteractType);
//["Air"] call EFUNC(main,addInteractType);
//["Motorcycle"] call EFUNC(main,addInteractType);
//["Ship"] call EFUNC(main,addInteractType);

GVAR(repairInteractTarget) = [];
GVAR(repairInteractValid) = false;
GVAR(currentRepairTarget) = objNull;


[QMVAR(SwitchWeapon), {
   params ["_item"];
   if (_item == __ITEMNAME) then {
       [{
           params ["", "_id"];
           if ((CLib_Player getVariable [QMVAR(fakeWeaponName), ""]) != __ITEMNAME) then {
               GVAR(repairInteractValid) = false;
               QSVAR(InteractHUD) cutFadeOut 0;
               _id call MFUNC(removePerFrameHandler);
           };

           if (MVAR(fakeWeaponReady)) then {
               private _vehicle = cursorObject;
               if (Clib_Player distance _vehicle > 10) exitWith {
                   GVAR(repairInteractValid) = false;
                   GVAR(repairInteractTarget) = [];
               };
               if !(GVAR(repairInteractTarget) isEqualTo [_vehicle, damage _vehicle]) then {
                   GVAR(repairInteractTarget) = [_vehicle, damage _vehicle];
                   private _canService = call {
                       if (isNull _vehicle) exitWith { false };
                       if (typeOf _vehicle == "") exitWith { false };
                       if (_vehicle isKindOf "Car") exitWith { true };
                       if (_vehicle isKindOf "Tank") exitWith { true };
                       if (_vehicle isKindOf "Air") exitWith { true };
                       if (_vehicle isKindOf "Motorcycle") exitWith { true };
                       if (_vehicle isKindOf "Ship") exitWith { true };
                       false
                   };
                   if (_canService && {alive _vehicle}) then {
                      GVAR(repairInteractValid) = ({ _x > 0.5 } count ((getAllHitPointsDamage _vehicle) select 2) > 0);
                   } else {
                      GVAR(repairInteractValid) = false;
                   };
               };

               private _display = uiNamespace getVariable [QSVAR(InteractHUD), displayNull];
               if (GVAR(repairInteractValid)) then {
                   if (isNull _display) then {
                       QSVAR(InteractHUD) cutRsc [QSVAR(InteractHUD), "PLAIN", 0];
                       _display = uiNamespace getVariable [QSVAR(InteractHUD), displayNull];

                       (_display displayCtrl 3000) ctrlSetFade 1;
                       (_display displayCtrl 3000) ctrlCommit 0;
                       (_display displayCtrl 3004) ctrlSetFade 0;
                       (_display displayCtrl 3004) ctrlCommit 0;
                   };
                   private _actionName = (getText (configFile >> "FRL" >> "VirtualItems" >> __ITEMNAME >> "actionVerb"));
                   (_display displayCtrl 3004) ctrlSetStructuredText parseText (format ["<img size='1.2' image='\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa'/> To %1<br />", _actionName]);
               } else {
                   (_display displayCtrl 3004) ctrlCommit 0;
               };
           };
       }, 0] call MFUNC(addPerFramehandler);
   };
}] call CFUNC(addEventHandler);


["missionStarted", {
   (findDisplay 46) displayAddEventHandler ["MouseButtonDown", {
       params ["", "_button"];

       if (_button > 0) exitWith { false };
       if !(GVAR(repairInteractValid)) exitWith { false };
       private _actionText = (getText (configFile >> "FRL" >> "VirtualItems" >> __ITEMNAME >> "actionProgressText"));
       [QGVAR(startRepairLight), [GVAR(repairInteractTarget) select 0, __ITEMNAME, _actionText]] call CFUNC(localEvent);
       true
   }];

   (findDisplay 46) displayAddEventHandler ["MouseButtonUp", {
       params ["", "_button"];
       if (_button == 0 && {!(isNull GVAR(currentRepairTarget))}) exitWith {
           [QGVAR(StopRepairLight), [GVAR(currentRepairTarget), false]] call CFUNC(localEvent);
           true
       };
       false
   }];
}] call CFUNC(addEventHandler);



[QGVAR(startRepairLight), {
   (_this select 0) params ["_target", "_action", "_displayText"];

   GVAR(currentRepairTarget) = _target;
   private _actionDuration = 45;
   _target setVariable [QGVAR(repairAmount), (1 / _actionDuration)];
   _target setVariable [QGVAR(repairStartTime), serverTime];

   // -- Show the UI
   disableSerialization;
   private _display = uiNamespace getVariable [QSVAR(InteractHUD), displayNull];
   if (isNull _display) then {
       QSVAR(InteractHUD) cutRsc [QSVAR(InteractHUD), "PLAIN", 0.1];
       _display = uiNamespace getVariable [QSVAR(InteractHUD), displayNull];
   };

   (_display displayCtrl 1002) ctrlSetStructuredText parseText format [_displayText];
   (_display displayCtrl 1004) progressSetPosition 0;
   (_display displayCtrl 3000) ctrlSetFade 0;
   (_display displayCtrl 3000) ctrlCommit 0;


   // -- Update the progress bar
   [{
       disableSerialization;
       params ["_params", "_id"];
       _params params ["_display", "_target"];

       if !(GVAR(repairInteractValid)) then {
           [QGVAR(StopRepairLight), [_target, false]] call CFUNC(localEvent);
       };

       if (isNull GVAR(currentRepairTarget)) exitWith {
           (_display displayCtrl 3000) ctrlSetFade 1;
           (_display displayCtrl 3000) ctrlCommit 0.5;
           _id call MFUNC(removePerFrameHandler);
       };

       private _repairAmount = _target getVariable [QGVAR(repairAmount), 0];
       private _repairProgress = _target getVariable [QGVAR(repairProgress), 0];
       private _repairStartTime = _target getVariable [QGVAR(repairStartTime), serverTime];
       _repairProgress = _repairProgress + (serverTime - _repairStartTime) * _repairAmount;
       if (_repairProgress >= 1) then {
           [QGVAR(StopRepairLight), [_target, true]] call CFUNC(localEvent);
       };

       (_display displayCtrl 1004) progressSetPosition _repairProgress;
   }, 0, [_display, _target]] call MFUNC(addPerFramehandler);
}] call CFUNC(addEventHandler);

// -- Hide the progress bar and save progress to resume later
[QGVAR(StopRepairLight), {
    GVAR(currentRepairTarget) = objNull;
    (_this select 0) params ["_target", "_finished"];

    if (_finished) exitWith {
        _target setVariable [QGVAR(repairProgress), nil];
        _target setVariable [QGVAR(repairEndTime), nil];
        [QGVAR(repairMobility), _target, [_target]] call CFUNC(targetEvent);
        Clib_Player removeItem __ITEMNAME;
    };

    private _repairAmount = _target getVariable [QGVAR(repairAmount), 0];
    private _repairProgress = _target getVariable [QGVAR(repairProgress), 0];
    private _repairStartTime = _target getVariable [QGVAR(repairStartTime), serverTime];
    _repairProgress = _repairProgress + (serverTime - _repairStartTime) * _repairAmount;

    _target setVariable [QGVAR(repairEndTime), serverTime];
    _target setVariable [QGVAR(repairProgress), _repairProgress];
    #define __TIMEOUTTIME 30
    #define __LOWERTIME 60

    // -- Lower saved progress over time
    [{
    	(_this select 0) params ["_target", "_lowerTime"];

    	// -- General timeout for progress starts dropping -- //
        private _repairEndTime = _target getVariable [QGVAR(repairEndTime), serverTime];
    	if !(alive _target) exitWith {
            [(_this select 1)] call MFUNC(removePerFrameHandler);
        };
    	if ((serverTime - _repairEndTime) >= __TIMEOUTTIME) then {
    		private _repairProgress = _target getVariable [QGVAR(repairProgress), 0];
    		_repairProgress = _repairProgress - ((1/_lowerTime) * 0.25);

    		if (_repairProgress <= 0) then {
    			_target setVariable [QGVAR(repairProgress), nil];
    			_target setVariable [QGVAR(repairEndTime), nil];
    			[(_this select 1)] call MFUNC(removePerFrameHandler);
    		} else {
    			_target setVariable [QGVAR(repairProgress), _repairProgress];
    		};
    	};

    }, 0.25, [_target, __LOWERTIME]] call MFUNC(addPerFramehandler);
}] call CFUNC(addEventHandler);
