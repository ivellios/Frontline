class GVAR(repairUI) {
    idd = -1;
    duration = 1e11;
    onLoad = "uiNamespace setVariable ['FRL_Maintenance_repairUI', _this select 0];";
    onUnLoad = "";
    class Controls {
        class CtrlGroup : RscControlsGroupNoScrollbars {
            idc = 4000;
            x = 0.5 - PX(25);
            y = 1/*- PY(0)*/;
            w = PX(50);
            h = PY(10);

            class Controls {
                class HelpText : RscStructuredText {
                    idc = 4004;
                    shadow = 1;
                    x = PX(0);
                    y = PY(0);
                    w = PX(50);
                    h = PY(7);
                    text = "";
                    size = PY(2.5);
                    class Attributes
                    {
                        font = "PuristaMedium";
                        color = "#ffffff";
                        align = "center";
                        shadow = 1;
                    };
                };


            };
        };
    };
};
