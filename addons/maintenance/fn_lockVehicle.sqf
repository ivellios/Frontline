/*
    Function:       FRL_Mainteance_fnc_lockVehicle
    Author:         Adanteh
    Description:    Blocks of the usage of this vehicle while its repairing/rearming
    Locality:       Local to _vehicle
*/
#include "macros.hpp"

(_this select 0) params ["_vehicle", "", ["_lock", true]];

if (_lock) then {
    if (_vehicle getVariable [QGVAR(locked), false]) exitWith { };
    _vehicle setVariable [QGVAR(locked), true, true];
    _vehicle setFuel 0;

    /*if !(_vehicle setVariable [QGVAR(reloadLock), false]) then {
        _vehicle setVariable [QGVAR(reloadLock), true, true];
        private _id = [{
            (_this select 0) params ["_vehicle"];
            if !(_vehicle getVariable [QGVAR(reloadLock), false]) exitWith {
                [(_this select 1)] call MFUNC(removePerFrameHandler);
            };
        }, 0, [_vehicle]] call MFUNC(addPerFramehandler);
    };*/
} else {
    if !(_vehicle getVariable [QGVAR(locked), false]) exitWith { };
    _vehicle setVariable [QGVAR(locked), false, true];
    _vehicle setFuel 1;
    _vehicle setVariable [QGVAR(reloadLock), nil, true];
};
