/*
    Function:       FRL_Maintenance_fnc_rearmFull
    Author:         Adanteh
    Description:    Does a full rearm on the vehicle
    Example:        [cursorTarget] call FRL_Maintenance_fnc_rearmFull;
*/
#include "macros.hpp"

params [["_vehicle", objNull, [objNull]]];
private _ammoTypes = +([_vehicle] call FUNC(getVehicleAmmo));
private _magsToRearm = [];
{
    _x params ["_magClass", "_magName", "_turretPath", "_magCounts", "_magCost", ["_simulateMag", false]];
    _magCounts params ["_magEmpty", "_magUsed", "_magFull", "_magMax"];

    // -- Figure out how many ammo we need to add/remove/change rounds for
    private _removeUsed = 0;
    private _rearmQuantity = 0;
    private _timeRequired = 0;
    if (_simulateMag) then {
        if (_magEmpty > 0) then {
            _removeUsed = 1;
            _rearmQuantity = _magMax;
            _timeRequired = _magEmpty * ([_magClass] call FUNC(getAmmoRearmTime));
        };
    } else {
        if (_magUsed > 0) then {
            _rearmQuantity = 1;
        };
        if (_magEmpty > 0) then {
            _rearmQuantity = _magMax - _magFull;
        };
        _removeUsed = _magUsed + _magEmpty; // Workaround to make sure all empty mags get deleted and readded
        _timeRequired = ([_magClass] call FUNC(getAmmoRearmTime));
    };
    if (_rearmQuantity > 0) then {
        _magsToRearm pushBack [_magClass, _rearmQuantity, _turretPath, _removeUsed, _simulateMag, _timeRequired];
    };
    nil;
} count _ammoTypes;
if !(_magsToRearm isEqualTo []) then {
    [QGVAR(rearmStart), [_vehicle, _magsToRearm]] call CFUNC(localEvent);
    ["showNotification", ["Started rearming", (MEDIAPATH + "icons\maintenance_rearm.paa"), "lime"]] call CFUNC(localEvent);
} else {
    ["showNotification", ["Vehicle doesn't need any rearming", (MEDIAPATH + "icons\maintenance_rearm.paa"), "orange"]] call CFUNC(localEvent);
};
