/*
    Function:       FRL_Maintenance_fnc_clientInitRearm
    Author:         Adanteh
    Description:    Advanced function to retrieve vehicle weapons and (supported) magazines.
*/
#include "macros.hpp"

// -- Locks vehicle and starts arming loop on server (Local to caller)
[QGVAR(rearmStart), {
    (_this select 0) params ["_vehicle", "_magsToRearm"];
    if (_magsToRearm isEqualTo []) exitWith { };
    [QGVAR(lockVehicle), _vehicle, [_vehicle, Clib_Player, true]] call CFUNC(targetEvent);
    [QGVAR(rearmStartServer), [_vehicle, _magsToRearm, Clib_Player]] call CFUNC(serverEvent);
}] call CFUNC(addEventHandler);

[QGVAR(showRearmProgress), {
    (_this select 0) params ["_magClass", "_timeRequired"];
    private _magName = getText (configFile >> "CfgMagazines" >> _magClass >> "displayName");
    if (count _magName > 16) then {
        _magName = (_magName select [0, 13]) + "...";
    };
    private _timeEnd = serverTime + _timeRequired;
    private _timeStart = serverTime;
    ["progressHudShow", [QGVAR(rearmProgress),
    {
        params ["_magName"];
        (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\maintenance_rearm.paa");
        (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Rearming %1", _magName]);
        (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor (["GUI", "BCG_RGB"] call BIS_fnc_displayColorGet);
    },
    {
        params ["", "_timeStart", "_timeRequired"];
        private _percentage = (serverTime - _timeStart) / _timeRequired;
        (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
    },
    [_magName, _timeStart, _timeRequired]]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);
