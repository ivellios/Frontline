/*
    Function:       FRL_Maintenance_fnc_repairLightAction
    Author:         Adanteh
    Description:    Handles the mobility repairs action started from the Interact menu, will need to hold button to repair, saves progress
*/
#include "macros.hpp"

params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {
    //
    //
    // Called every frame while the hold action button is still pressed, return should be true to keep action going, false to exit it
    case "callback": {
        _args params ["_caller", "_target", "_menuTarget"];
        if !(_target isEqualTo (call _menuTarget)) exitWith { // -- Not aiming at vehicle anymore
            _return = false;
        };
        _return = (alive _caller && { !([_caller] call MFUNC(isUnconscious)) } && { alive _target })
    };



    //
    //
    // Condition to grey out the interact menu option
    case "condition": {
        _args params ["_caller", "_target"];

        if !(alive _target) exitWith {
            ["showNotification", ["Can't repair destroyed vehicles", "nope"]] call CFUNC(localEvent);
            _return = false;
        };

        if ({ _x > 0.25 } count ((getAllHitPointsDamage _target) select 2) == 0) exitWith {
            ["showNotification", ["Vehicle isn't damaged", "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };



    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_target", "_targetRealtime"];

        [
            "Repair",
            TEXTURE(maintenance_repair.paa),
            ["getduration", [_target]] call FUNC(repairLightAction),
            [_caller, _target, _targetRealtime],
            { ["condition", _this] call FUNC(repairLightAction) },
            { ["callback", _this] call FUNC(repairLightAction) },
            { ["end", _this] call FUNC(repairLightAction) },
            true,
            true,
            10,
            _dikButton // -- This is passed from menuUseActionKey
        ] call MFUNC(holdActionStart);
    };


    //
    //
    // Called when we press the button from the interact menu
    case "end": {
        _args params ["_data", "_exitCode"];
        if (_exitCode isEqualTo 0) then {
            _data params ["_caller", "_target"];
            [QGVAR(repairMobility), _target, [_target]] call CFUNC(targetEvent);
        };
    };



    //
    //
    // Used to get how long a repair action should take IN SECONDS
    case "getduration": { // I
        _args params ["_target"];

        private _category = ([_target] call EFUNC(vehicles,getVehicleType) select 1);
        _return = switch (toLower _category) do {
            case "tank";
            case "mbt";
            case "armour";
        	case "tank_amphibious": { 60 };

            case "wheeledifv";
            case "trackedapc";
            case "ifv";
            case "armored_car": { 45 };


            case "jet";
            case "plane";
            case "plane_heavytransport";
            case "attackhelicopter";
            case "plane_recon": { 60 };

            case "vehicleautonomous";
            case "helicopter";
            case "heli_lighttransport";
            case "heli_heavytransport": { 45 };

            case "apc";
            case "armedmrap";
            case "halftrack": { 30 };

            case "mrap";
            case "truck": { 25 };

            case "car";
            case "motorcycle";
            case "quadbike";
            case "armed_car";
            case "ship": { 15 };

            case "submarine";
            case "boat";
            case "lcvp";
            case "lcm";
            case "staticweapon";
            case "static": { 10 };
            default { 10 };
        };

        #ifdef DEBUGFULL
            _return = _return / 10
        #endif
    };
};

_return;
