#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

#include "cfgWeapons.hpp"

class FRL {
    //#include "cfgVirtualItems.hpp"

    class Modules {
        class MODULE {
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\maintenance";

            class canMaintenance;
            class clientInit;
          	class clientInitRearm;
          	class clientInitRepair;
          	//class clientInitRepairUI;
          	class getAmmoRearmTime;
          	class getAmmoTypes;
          	class getTurrets;
          	class getVehicleAmmo;
          	class init;
          	class lockVehicle;
          	class rearmFull;
          	class rearmMagLoop;
          	class repairParts;
          	class repairFull;
          	class repairMobility;
          	class repairPartLoop;
          	class serverInit;
          	class serverInitRearm;
          	class serverInitRepair;

            class repairLightAction;
        };
    };
};

class RscControlsGroupNoScrollbars;
class RscStructuredText;
class RscTitles {
  #include "rscTitles.hpp"
};
