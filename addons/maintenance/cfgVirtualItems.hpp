class VirtualItems {
	class Toolkit {
    scope = 2;
    model = "A3\Structures_F\Items\Tools\Wrench_F.p3d";
    itemRequired = "ToolKit";
    attachPoint = "lwrist";
    attachOffset[] = {0.02, 0.10, -0.01};
    vectorDir[] = {{1, 1, 1}, {1, 0, 1}};
    actionVerb = "repair";
    actionProgressText = "Repairing...";
    actionName = "Toolkit";
    allowInVehicle = 0;
    gestureUse = "HandSignalFreeze";
	};
};
