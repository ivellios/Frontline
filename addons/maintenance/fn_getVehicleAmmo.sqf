/*
    Function:       FRL_Maintenance_fnc_getVehicleAmmo
    Author:         Adanteh
    Description:    Returns list of ammo on vehicle, grouped by classname, showing how many mags are empty, full and half-empty
    Example:        [cursorTarget] call FRL_Maintenance_fnc_getVehicleAmmo;

    Params:
        0: Vehicle <OBJECT>
*/
#include "macros.hpp"

params [["_vehicle", objNull, [objNull]]];
private _allMagazines = magazinesAllTurrets _vehicle;
private _ammoList = [[], []];
{
    _x params ["_magClass", "_turretPath", "_ammoCount", "_id", "_creator"];
    private "_magDetails";
    private _magIndex = -1;
    {
        _x params ["_name", "_turret"];
        if ((_name == _magClass) && (_turret isEqualTo _turretPath)) exitWith { _magIndex = _forEachIndex };
    } forEach (_ammoList select 0);
    if (_magIndex == -1) then {
        private _magName = getText (configFile >> "cfgMagazines" >> _magClass >> "displayName");
        private _magCost = getNumber (configFile >> "cfgMagazines" >> _magClass >> "supplyCost");
        _magIndex = (_ammoList select 0) pushBack [(toLower _magClass), _turretPath];
        _magDetails = [_magClass, _magName, _turretPath, [0, 0, 0, 0], _magCost];
    } else {
        _magDetails = (_ammoList select 1) select _magIndex;
    };

    // -- Split mag simulation will act like some rounds are actually seperate magazines. Useful to mark individual tank shells
    private _simulatedMagType = false;
    private _magSize = (getNumber (configFile >> "cfgMagazines" >> _magClass >> "count"));
    if (_magSize <= 36) then {
        if (getNumber (configFile >> "cfgMagazines" >> _magClass >> "FRL_VehicleMaintenance" >> "SimulatedMag") >= 1) exitWith { _simulatedMagType = true };
        if (toLower (getText (configFile >> "cfgMagazines" >> _magClass >> "nameSound")) in ["sabot", "heat"]) exitWith { _simulatedMagType = true; };
    };
    if (_simulatedMagType) then {
        _magDetails set [3, [(_magSize - _ammoCount), 0, _ammoCount, _magSize]];
        _magDetails set [5, true];
    } else {
        (_magDetails select 3) set [3, ((_magDetails select 3) select 3) + 1];
        if (_ammoCount <= 0) then {
            (_magDetails select 3) set [0, ((_magDetails select 3) select 0) + 1];
        } else {
        	if (_ammoCount < _magSize) then {
                (_magDetails select 3) set [1, ((_magDetails select 3) select 1) + 1];
        	} else {
                (_magDetails select 3) set [2, ((_magDetails select 3) select 2) + 1];
        	};
        };
    };

    (_ammoList select 1) set [_magIndex, _magDetails];
    nil;
} count _allMagazines;
(_ammoList select 1);

// Sample output
/*
Full
    [<class>, <display name>, <turretpath>, [<empty>, <used>, <full>, <max>], <supply cost>],
    ["rhs_mag_M829A3","M829A3 APFSDS-T",[0],[0,1,0,1],0]
    ["rhs_mag_M830A1","M830A1 MPAT",[0],[0,1,0,1],0]
    ["rhs_mag_762x51_M240_1200","7.62x51mm Ball",[0],[0,0,9,9],0]
    ["rhs_mag_100rnd_127x99_mag_Tracer_Red","12.7mm M2 HMG Tracer (Red) Belt",[0,0],[1,1,8,10],0]
    ["SmokeLauncherMag","",[0,0],[1,0,1,2],0]
    ["rhs_mag_762x51_M240_200","100rnd M240",[0,1],[1,1,1,3],0]
*/
