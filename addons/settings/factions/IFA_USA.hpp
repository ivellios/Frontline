class IFA_USA {
    name = "U.S. Army";
    playerClass = "O_G_Soldier_F";
    flag = "\WW2\Core_t\IF_Data_t\Factions\US_Army.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_West";
    fo[] = {{"LIB_AmmoCrates_NoInteractive_Large", {0,0,0}, 0}, {"Vysilacka", {0,-0.28,0.81}, 177}};
    squads = "IFA_USA";
    unitNames = "EnglishMen";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            displayName = "P39 Gun run";
            planeClass = "LIB_P39";
            altitude = 500;
            distance = 2500;
            guntype = 0;
            forceFire = 0;
            fireDistance = 1000;
            interval = 0.05;
            duration = 7;
        };
        class airdrop: airdrop {
            planeClass = "LIB_C47_Skytrain";
        };
    };
};

class IFA_USA_P: IFA_USA {
    name = "USMC";
    squads = "IFA_USA_P";
};

class IFA_USA_AB: IFA_USA {
    name = "US Airborne";
    squads = "IFA_USA_AB";
    class CfgCommanderSkills: CfgCommanderSkills {
        class airdrop: airdrop {
            cooldown = 15;
        };
        class CAS2: CAS {
            displayName = "P47 Rocket run";
            altitude = 1000;
            distance = 2500;
            forceFire = 1;
            fireDistance = 800;
            planeClass = "LIB_P47";
            guntype = 2;
            interval = 0.1;
            duration = 4;
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 0;
        };
    };
};
