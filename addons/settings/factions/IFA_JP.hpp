class IFA_JP: IFA_GER {
    name = "Japan";
    playerClass = "fow_s_ija_rifleman";
    flag = "\pr\frl\addons\client\ui\media\flags\JP.paa";
    squads = "IFA_JP";
    unitNames = "AsianMen";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            displayName = "Zero Gun run";
            planeClass = "fow_va_a6m_white";
            altitude = 500;
            distance = 2500;
            guntype = 0;
            forceFire = 0;
            fireDistance = 1000;
            interval = 0.05;
            duration = 7;
        };
        class airdrop: airdrop {
            planeClass = "LIB_Ju52";
        };
    };
};
