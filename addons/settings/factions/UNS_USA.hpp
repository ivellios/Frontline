class UNS_USA {
    name = "USA";
    playerClass = "FRL_B_Soldier_USMC_F";
    flag = "\pr\frl\addons\client\ui\media\flags\US.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
    color[] = {0, 0.3, 0.8, 1};
    rally = "FRL_Backpacks_West";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "UNS_USA";
    unitNames = "EnglishMen";
    baseRole = "UNS_USA_Baserole"; 

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            displayName = "Gun run";
            planeClass = "uns_A1J_HCAS";
            altitude = 1000;
            distance = 3500;
            guntype = 0;
            forceFire = 0;
            fireDistance = 1200;
            interval = 0.1;
            duration = 8;
            initPlane = "";
            cooldown = 15;
        };
        /*
        class CAS_Rocket: CAS {
            displayName = "Rocket run";
            planeClass = "uns_f100b_CAS";
            altitude = 1000;
            distance = 3500;
            forceFire = 1;
            fireDistance = 800;
            guntype = 2;
            interval = 0.4;
            duration = 4;
            cooldown = 25;
        };
        */
        class CAS_Napalm: CAS {
             displayName = "Napalm";
            planeClass = "uns_F4E_CAS";
            altitude = 1000;
            distance = 3500;
            guntype = 3;
            forceFire = 1;
            fireDistance = 900;
            interval = 0.6;
            targetHeight = 150;
            initPlane = "";
            cooldown = 20;
        };
        class airdrop: airdrop {
            planeClass = "uns_ch47_m60_army";
            cooldown = 15;
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 0;
            //cooldown = 40;
        };
    };
};
