class IFA_UK {
    name = "UK";
    playerClass = "O_G_Soldier_F";
    flag = "\pr\frl\addons\client\ui\media\flags\UK.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_West";
    fo[] = {{"LIB_AmmoCrates_NoInteractive_Large", {0,0,0}, 0}, {"Vysilacka", {0,-0.28,0.81}, 177}};
    squads = "IFA_UK";
    unitNames = "BritishMen";
    baseRole = "RHS_GREE_Baserole";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            displayName = "P39 Gun run";
            planeClass = "LIB_RAF_P39";
            altitude = 500;
            distance = 2500;
            guntype = 0;
            forceFire = 0;
            fireDistance = 1000;
            interval = 0.05;
            duration = 7;
        };
        class airdrop: airdrop {
            planeClass = "LIB_C47_RAF_bob";
        };
    };
};

class IFA_UK_Com: IFA_UK {
    name = "Commandos";
    squads = "IFA_UK_Com";
};

class IFA_UK_D: IFA_UK {
    name = "UK Desert Rats";
    squads = "IFA_UK_D";
};

class IFA_UK_AB: IFA_UK {
    name = "UK Para";
    squads = "IFA_UK_AB";
    class CfgCommanderSkills: CfgCommanderSkills {
        class airdrop: airdrop {
            cooldown = 15;
        };
        class CAS2: CAS {
            displayName = "P47 Rocket run";
            altitude = 1000;
            distance = 2500;
            forceFire = 1;
            fireDistance = 800;
            planeClass = "LIB_P47";
            guntype = 2;
            interval = 0.1;
            duration = 4;
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 0;
        };
    };
};

class IFA_GREE: IFA_UK {
    name = "Resistance";
    flag = "\pr\frl\addons\client\ui\media\flags\GREE.paa";
    squads = "IFA_GREE";
    
    class CfgCommanderSkills: CfgCommanderSkills {
        class CAS: CAS {
            scope = 0;
        };
        class CAS2: CAS {
            scope = 0;
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 0;
        };
        class MortarHE: MortarHE {
            scope = 2;
        };
        class airdrop: airdrop {
            cooldown = 15;
            canUseAfter = 3;
        };
    };
};
