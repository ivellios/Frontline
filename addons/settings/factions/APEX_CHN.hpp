class PRC {
    name = "PRC";
    playerClass = "O_Soldier_FRL_CHN_07Woodland";
    flag = "pr\frl\addons\uniforms\factions\Cfgfactionclasses_chn_pla.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_East";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
	squads = "Apex_China";
    unitNames = "AsianMen";
    baseRole = "APEX_CHN_Baserole";

    class CfgCommanderSkills: CommanderSkillsBase {
        class airdrop: airdrop {
            scope = 0;
            planeClass = "O_Heli_Transport_04_F";
        };
    };
};
