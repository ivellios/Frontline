class RHS_INS_W {
    name = "INS";
    playerClass = "FRL_O_Soldier_RUA_W_F";
    flag = "\rhsgref\addons\rhsgref_main\data\flag_insurgents_co.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_East";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "RHS_Insurgents_Woodland";
    unitNames = "TakistaniMen";
    baseRole = "RHS_INS_W_Baserole"; 

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            scope = 0;
        };
        class ArtilleryStrike: ArtilleryStrike {
            displayName = "Hell Cannon";
            scope = 2;
            ammo = "IRAM";
            count = 1;
            spread = 50;
            delay = 15;
            originSound = "pr\frl\addons\data_sound\data\artillery\mortar.wss";
            
        };
        class RocketBarrage: ArtilleryStrike {
            displayName = "Rocket Barrage";
            scope = 2;
            minPlayers = 15;
            cooldown = 15;
            canUseAfter = 20;

            ammo = "FRL_RocketSmall";
            count = 6;
            spread = 50;
            interval = 1;
            delay = 10;
            originSound = "pr\frl\addons\data_sound\data\artillery\ATGM_2.wss";
            originCount = -1;
            altitude = -1;
        };        
        class airdrop: airdrop {
            scope = 0;
        };
    };
};

class RHS_INS_D: RHS_INS_W {
    squads = "RHS_Insurgents_Desert";
    baseRole = "RHS_INS_D_Baserole"; 
};
