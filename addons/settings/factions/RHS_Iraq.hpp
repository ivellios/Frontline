class RHS_Iraq: RHS_USA_W {
    name = "Iraq";
    flag = "\pr\frl\addons\client\ui\media\flags\iraq.paa";
    squads = "RHS_Iraq";
    unitNames = "TakistaniMen";
    
    class CfgCommanderSkills: CfgCommanderSkills { 
        class airdrop: airdrop {
            planeClass = "RHS_CH_47F_light";
        };
    };
};
