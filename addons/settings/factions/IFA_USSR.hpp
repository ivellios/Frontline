class IFA_USSR {
    name = "Red Army";
    playerClass = "LIB_SOV_rifleman";
    flag = "\WW2\Core_t\IF_Data_t\Factions\RKKA.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_East";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}};
    //fo[] = {{"Land_Tent_A", {0,0,0}, 0}};
    squads = "IFA_USSR";
    unitNames = "LIB_RussianMen";
    baseRole = "IF_USSR_Baserole";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            displayName = "Pe2 Bombing run";
            planeClass = "LIB_Pe2";
            altitude = 500;
            distance = 3000;
            guntype = 3;
            forceFire = 1;
            fireDistance = 1300;
            interval = 1;
            duration = 5;
            targetHeight = 400;
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 2;
            displayName = "Katyusha";
            cooldown = 25;
            ammo = "LIB_R_BM13";
            delay = 25;
            count = 12;
            spread = 75;
            interval = 0.75;
            sound = "";
            originSound = "pr\frl\addons\data_sound\data\artillery\katyusha.wss";
            originCount = -1;
            altitude = -1;
            angle = 55;
        };
        class airdrop: airdrop {
            planeClass = "LIB_Li2";
        };
    };
};

class IFA_USSR_W: IFA_USSR {
    squads = "IFA_USSR_W";
};

class IFA_USSR_P: IFA_USSR {
    name = "Partisans";
    squads = "IFA_USSR_P";
    class CfgCommanderSkills: CfgCommanderSkills {
        class airdrop: airdrop {
            cooldown = 15;
            canUseAfter = 3;
        };
        class CAS: CAS {
            scope = 0;
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 0;
        };
        class MortarHE: MortarHE {
            scope = 2;
        };
    };
};
