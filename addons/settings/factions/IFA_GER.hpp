class IFA_GER {
    name = "Wehrmacht";
    playerClass = "LIB_GER_rifleman";
    flag = "\WW2\Core_t\IF_Data_t\Factions\Wehrmacht.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
    color[] = {0, 0.3, 0.8, 1};
    rally = "FRL_Rally_Wehrmacht";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}};
    //fo[] = {{"LIB_Static_opelblitz_radio", {0,0,0}, 0}};
    squads = "IFA_GER";
    unitNames = "LIB_GermanMen";
    baseRole = "IF_GER_Baserole";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            displayName = "FW 190 Gun run";
            planeClass = "LIB_FW190F8";
            altitude = 500;
            distance = 2500;
            guntype = 0;
            forceFire = 0;
            fireDistance = 1000;
            interval = 0.05;
            duration = 7;
            initPlane = "(_this select 0) setVariable ['IFA3_sirenEnabled', 0, true];";
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 2;
            displayName = "Nebelwerfer";
            cooldown = 25;
            ammo = "LIB_R_150mm_WGr41";
            delay = 10;
            count = 6;
            spread = 75;
            interval = 2;
            sound = "";
            originSound = "pr\frl\addons\data_sound\data\artillery\nebelwerfer_01.wss";
            originCount = -1;
            altitude = -1;
            angle = 75;
        };
        class airdrop: airdrop {
            planeClass = "LIB_Ju52";
        };
    };
};

class IFA_GER_W: IFA_GER {
    squads = "IFA_GER_W";
};

class IFA_GER_AB: IFA_GER {
    name = "Fallschirmjäger";
    squads = "IFA_GER_AB";
    class CfgCommanderSkills: CommanderSkillsBase {
        class airdrop: airdrop {
            cooldown = 15;
        };
        class CAS2: CAS {
            displayName = "Ju87 Divebomb";
            planeClass = "LIB_Ju87";
            altitude = 2200;
            distance = 2000;
            guntype = 3;
            forceFire = 1;
            fireDistance = 650;
            interval = 0.5;
            targetHeight = 200;
            initPlane = "(_this select 0) setVariable ['IFA3_sirenEnabled', 0, true];";
        };
        class ArtilleryStrike: ArtilleryStrike {
            scope = 0;
        };
    };
};

class IFA_GER_DAK: IFA_GER {
    name = "DAK";
    squads = "IFA_GER_DAK";
};
