class RHS_RUA_W {
    name = "RUA";
    playerClass = "FRL_O_Soldier_RUA_W_F";
    flag = "\rhsafrf\addons\rhs_main\data\icons\msv.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_East";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "RHS_RUA_Woodland";
    unitNames = "RussianMen";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            planeClass = "RHS_Su25SM_vvsc";
            altitude = 1000;
            distance = 2500;
            guntype = 2;
            forceFire = 1;
            fireDistance = 1000;
            interval = 3;
            duration = 5;
            initPlane = "";
        };
        class airdrop: airdrop {
            planeClass = "RHS_Mi8mt_vvs";
            targetheight = 300;
        };
    };
};
/*
class RHS_RUA_D: RHS_RUA_W {
    squads = "RHS_RUA_Desert";
};
*/
class RHS_RUA_W_Blu: RHS_RUA_W {
    playerClass = "FRL_B_Soldier_USMC_F";
    squads = "RHS_RUA_Woodland_Blu";
};
