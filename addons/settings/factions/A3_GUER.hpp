class A3_GUER {
    name = "Insurgents";
    playerClass = "O_Soldier_F";
    flag = "a3\data_f\flags\flag_fia_co.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_West";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "A3_GUER";
    unitNames = "NATOMen";
    baseRole = "A3_GUER_Default";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            scope = 0;
        };
        class airdrop: airdrop {
            scope = 0;
        };
        class MortarHE: MortarHE {
            scope = 2;
        };
    };
};
/*
class A3_GUER_BLU: A3_GUER {
    color[] = {0, 0.3, 0.8, 1};
    playerClass = "B_Soldier_F";
    squads = "A3_CSAT";
};
*/
