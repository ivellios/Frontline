class RHS_UN: RHS_USA_W {
    name = "UN";
    flag = "\pr\frl\addons\client\ui\media\flags\UN.paa";
    squads = "RHS_UN_Desert";
    unitNames = "NATOMen";

    class CfgCommanderSkills: CfgCommanderSkills { 
        class airdrop: airdrop {
            planeClass = "RHS_CH_47F_light";
        };
    };
};
