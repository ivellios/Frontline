class A3_NATO {
    name = "NATO";
    playerClass = "B_Soldier_F";
    flag = "a3\Data_f\cfgFactionClasses_BLU_ca.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
    color[] = {0, 0.3, 0.8, 1};
    rally = "FRL_Backpacks_West";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "A3_NATO";
    unitNames = "NATOMen";
    baseRole = "A3_NATO_Default";

    class CfgCommanderSkills: CommanderSkillsBase {
        class airdrop: airdrop {
            scope = 0;
        };
    };
};
