class RHS_AFP: RHS_USA_W {
    name = "AFP";
    flag = "\pr\frl\addons\client\ui\media\flags\AFP.paa";
    squads = "RHS_AFP";
    unitNames = "AsianMen";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            displayName = "Rocket run";
            planeClass = "rhs_l39_cdf_b_cdf";
            altitude = 1000;
            distance = 2500;
            forceFire = 1;
            fireDistance = 800;
            guntype = 2;
            interval = 0.1;
            duration = 4;
        };
        class airdrop: airdrop {
            planeClass = "RHS_CH_47F_light";
        };
    };
};
