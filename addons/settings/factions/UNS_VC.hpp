class UNS_VC {
    name = "VC";
    playerClass = "O_Soldier_FRL_CHN_07Woodland";
    flag = "\pr\frl\addons\client\ui\media\flags\VC.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_East";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "UNS_VC";
    unitNames = "AsianMen";
    baseRole = "UNS_VC_Baserole"; 

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS {};
        class airdrop {};
        class MortarHE: MortarHE {
            scope = 2;
            count = 7;
            spread = 50;
        };
        class ArtilleryStrike: ArtilleryStrike {
            count = 4;
        };
    };
};
