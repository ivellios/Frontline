class cfgFactions {
    class CommanderSkillsBase {
        class airdrop {
            displayName = "Supply Drop";
            function = "['airdrop', _this] call FRL_Commander_fnc_useSkill;";
            condition = "['airdrop', _this] call FRL_Commander_fnc_useSkillCondition;";
            conditionShow = "true";
            listPath = "\pr\frl\addons\commander\data\Support.paa";
            scope = 2;
            cooldown = 25;
            canUseAfter = 5;

            planeClass = "B_Heli_Transport_03_F";
            distance = 3000; // -- Distance to spawn heli from the waypoint
            altitude = 400; // -- Height to spawn heli at
            itemToDrop = "frl_supplyContainer"; // -- Item to deliver via airdrop (Vehicle, supply crate, whatever)
            initPlane = ""; // -- Extra code to run on plane when spawning
            targetheight = 400; // -- Height heli should be at when making the drop
            forceFlying = 1; // -- Whether to let AI figure out how to get back to base, or do automagic shit (Usually 0 for planes, 1 for helos)
        };

        class ArtilleryStrike {
            displayName = "Artillery Strike";
            function = "['ArtilleryStrike', _this] call FRL_Commander_fnc_useSkill;";
            condition = "['ArtilleryStrike', _this] call FRL_Commander_fnc_useSkillCondition;";
            conditionShow = "true";
            listPath = "\pr\frl\addons\commander\data\Support.paa";
            scope = 2;
            minPlayers = 25;
            cooldown = 30;
            canUseAfter = 15;

            ammo = "Sh_155mm_AMOS";
            count = 3;
            spread = 75;
            interval = 5;
            delay = 25;
            altitude = 1000;
            originSound[] = {"pr\frl\addons\data_sound\data\artillery\verylarge\first.wss", "pr\frl\addons\data_sound\data\artillery\verylarge\third.wss", "pr\frl\addons\data_sound\data\artillery\verylarge\four.wss"};
            originCount = -1;
        };

        class MortarSmoke {
            displayName = "Mortar Smoke";
            function = "['MortarSmoke', _this] call FRL_Commander_fnc_useSkill;";
            condition = "['MortarSmoke', _this] call FRL_Commander_fnc_useSkillCondition;";
            conditionShow = "true";
            listPath = "\pr\frl\addons\commander\data\Support.paa";
            scope = 2;
            minPlayers = 10;
            cooldown = 5;
            canUseAfter = 5;

            ammo = "8Rnd_82mm_Mo_Smoke_white";
            count = 3;
            spread = 35;
            interval = 3;
            delay = 10;
            altitude = 1000;
            originSound = "pr\frl\addons\data_sound\data\artillery\mortar\150m_1.wss";
            originCount = -1;
        };

        class MortarHE: MortarSmoke {
            scope = 0;
            function = "['MortarHE', _this] call FRL_Commander_fnc_useSkill;";
            condition = "['MortarHE', _this] call FRL_Commander_fnc_useSkillCondition;";
            displayName = "Mortar HE";
            minPlayers = 15;
            cooldown = 15;
            canUseAfter = 15;

            ammo = "8Rnd_82mm_Mo_shells";
            count = 5;
            spread = 40;
        };

        class MortarFlare: MortarSmoke {
            function = "['MortarSmoke', _this] call FRL_Commander_fnc_useSkill;";
            condition = "['MortarSmoke', _this] call FRL_Commander_fnc_useSkillCondition;";
            displayName = "Mortar Flares";
            minPlayers = 1;
            cooldown = 3;

            ammo = "F_40mm_Red";
            count = 6;
            spread = 200;
            altitude = 1000;
            interval = 90;
        };

        class CAS {
            function = "['CAS', _this] call FRL_Commander_fnc_useSkill;";
            condition = "['CAS', _this] call FRL_Commander_fnc_useSkillCondition;";
            conditionShow = "true";
            listPath = "\pr\frl\addons\commander\data\Support.paa";
            scope = 2;
            displayName = "Air Support";
            minPlayers = 15;
            cooldown = 15;
            canUseAfter = 20;

            planeClass = "B_Plane_CAS_01_F";
            distance = 3000;
            guntype = 0;
            forcefire = 0;
            firedistance = 1000;
            duration = 5;
            targetheight = 0;
            interval = 0.1;
        };
    };

    #include "A3_CSAT.hpp"
    #include "A3_NATO.hpp"
    #include "A3_GREF.hpp"
    #include "A3_PLA.hpp"
    #include "A3_GUER.hpp"

    #include "APEX_NATO.hpp"
    #include "APEX_CHN.hpp"

    #include "IFA_GER.hpp"
    #include "IFA_JP.hpp"
    #include "IFA_UK.hpp"
    #include "IFA_USA.hpp"
    #include "IFA_USSR.hpp"

    #include "RHS_INS.hpp"
    #include "RHS_RUA.hpp"
    #include "RHS_USA.hpp"
    #include "RHS_UN.hpp"
    #include "RHS_Iraq.hpp"
    #include "RHS_AFP.hpp"

    #include "UNS_USA.hpp"
    #include "UNS_VC.hpp"
};
