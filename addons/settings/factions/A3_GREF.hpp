class A3_GREF {
    name = "AAF";
    playerClass = "O_Soldier_F";
    flag = "a3\Data_f\cfgFactionClasses_IND_ca.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_West";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "A3_GREF";
    unitNames = "NATOMen";
    baseRole = "A3_GREF_Default";

    class CfgCommanderSkills: CommanderSkillsBase {
        class airdrop: airdrop {
            scope = 0;
            planeClass = "I_Heli_Transport_02_F";
        };
        class CAS: CAS {
            planeClass = "I_Plane_Fighter_04_F";
        };
    };
};
