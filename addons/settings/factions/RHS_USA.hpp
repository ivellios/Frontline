class RHS_USA_W {
    name = "USA";
    playerClass = "FRL_B_Soldier_USMC_F";
    flag = "\rhsusf\addons\rhsusf_main\data\armylogo.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
    color[] = {0, 0.3, 0.8, 1};
    rally = "FRL_Backpacks_West";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    squads = "RHS_USA_Woodland";
    unitNames = "EnglishMen";

    class CfgCommanderSkills: CommanderSkillsBase {
        class CAS: CAS {
            planeClass = "RHS_A10";
            altitude = 1000;
            distance = 2500;
            guntype = 0;
            forceFire = 0;
            fireDistance = 1500;
            interval = 0.1;
            duration = 9;
            initPlane = "";
        };
        class airdrop: airdrop {
            planeClass = "RHS_CH_47F";
        };
    };
};

class RHS_USMC_W: RHS_USA_W {
    name = "USMC";
    squads = "RHS_USMC_Woodland";
};