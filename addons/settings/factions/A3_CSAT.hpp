class A3_CSAT {
    name = "CSAT";
    playerClass = "O_Soldier_F";
    flag = "a3\Data_f\cfgFactionClasses_OPF_ca.paa";
    mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
    color[] = {0.5, 0, 0, 1};
    rally = "FRL_Backpacks_East";
    fo[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
	squads = "A3_CSAT";
    unitNames = "AsianMen";
    baseRole = "A3_CSAT_Default";

    class CfgCommanderSkills: CommanderSkillsBase {
        class airdrop: airdrop {
            scope = 0;
            planeClass = "O_Heli_Transport_04_F";
        };
        class CAS: CAS {
            planeClass = "O_Plane_CAS_02_F";
        };
    };
};
