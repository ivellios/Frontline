class CfgRespawn {
    battleprepDuration = 30; // This is basically a synced spawn timer at game start
    respawnCountdown = 90; // Time until respawn is available
    minimumRespawnTime = 10; // Minimum respawn after incap subtraction
    minimumIncapTime = 60;
};
