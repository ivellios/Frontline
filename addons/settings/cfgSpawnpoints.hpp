class CfgZoneSpawning {
    enabled = 1;
    maxEnemyCount = 1;
    maxEnemyCountRadius = 50;
};

class cfgSquadRallyPoint {
    //minDistance = 0;
    minDistanceEnemyZone = 100;
    //spawnCount = 9;
    //nearPlayerToBuild = -1;
    //nearPlayerToBuildRadius = -1;
    maxEnemyCount = 1;
    maxEnemyCountRadius = 30;
    uptime = 90;
    waitTime = 150;
    rearmCooldown = 450; //600;
};

class cfgFOB {
    minDistanceEnemyZone = 250;
    maxDistanceFriendlyZone = 1250;
    blockCount = 2;
    blockRadius = 75;
    blockDuration = 30;
    nearPlayerToBuild = 3;
    nearPlayerToBuildRadius = 50;
    spacingDistance = 600;
    activateTime = 600; // Time before you can spawn on FO after placed
    activateSupplies = 200; // Bypass activating time by using this amount of supplies

    destroyDuration = 40;
    destroyBlockTime = 300;
    destroyBlockRange = 300;

    incapBlock = 1; // 0 to turn off
    incapBlockRange = 75;
    incapBlockDuration = 45;

    buildRadius = 200;
    ticketGain = 10; // Tickets gained for destroyign an FO

    packupDuration = 10;
    maxChainDistance = 1700;
};

class cfgVehicleSpawn {
    minDistanceEnemyZone = 250;
    blockDuration = 30;
    blockCount = 2;
    blockRadius = 50;
};
