class CfgMedical {
    bleedoutTimeSteps[] = {210,150,0};
    bleedoutTimeReset = 300;
    maxDamageOnLegsBeforWalking = 0.7;

    treatmentTimeAdrenalineMedic = 4;
    treatmentTimeAdrenaline = 8;

    treatmentTimeMorphineMedic = 4;
    treatmentTimeMorphine = 8;

    healthIncreaseMedic = 0.5;
    healthIncrease = 0.25;
    healthIncreaseCap = 0.25;

    treatmentTimeBandage = 10;
    treatmentTimeBandageMedic = 5;

    treatmentTimeCPR = 10;
    treatmentTimeCPRMedic = 10;
    treatmentTickCPR = 1;
    treatmentEffectCPR = 8;
    treatmentEffectCPRMedic = 13;

};
