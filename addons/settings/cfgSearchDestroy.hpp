class CfgSearchDestroy {
    bleedStrength = 10; // -- Victory points for defenders per 10 minutes
    bleedPerCache = 25;
    intelStages[] = {20, 30, 40, 50, 60};
    markerSizes[] = {155, 400, 600, 800, 1200, 2000};
    cacheAmount = 2; // How many caches appear per set

    cacheSpreadMin = 40; // -- Minimum distance between caches in a single objective
    cacheSpreadMax = 250; // -- Maximum distance between caches
    objectiveSpacing = 300; // -- Minimum distance to the next objective set

    passiveIntel = 10; // -- Intel automatically gained per 10 minutes
    deathIntel = 2; // Intel per defender death

    tunnelSize = 250;
    tunnelTime = 240;

    bombArmTime = 10; // Time required to arm bomb
    bombDefuseTime = 5; // Time required to defuse bomb
    bombCookoffTime = 90; // Time bomb needs to tick before it goes boom

    preventBuildingDamage = 1; // -- 1 or higher to prevent building of cache taking damage
    aiUpdateSpeedDefender = 240; // override for how often AI waypoints should update
    aiUpdateSpeedAttacker = 240; // override for how often AI waypoints should update

    attackerSide = "west";
    defenderSide = "east";
};
