
class A3_CSAT {

    staticCompositions[] = {
        "CSAT_ATGMS",
        "CSAT_MANPADS",
        "CSAT_HMGS"
    };

    class A3_CSAT_Infantry {
        displayName = "Infantry";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
        maxSize = 9;
        availableAt = -1;
        aiSquadType = 1;
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_CSAT_SquadLeader", 1},
            {"A3_CSAT_Rifleman",-1},
            {"A3_CSAT_Medic", 2},
            {"A3_CSAT_AR", 4},
            {"A3_CSAT_LAT", 5},
            {"A3_CSAT_Engineer", 5},
            {"A3_CSAT_Grenadier", 6},
            {"A3_CSAT_AA", 3},
            {"A3_CSAT_Crewman", -1}

        };
    };

    class A3_CSAT_WPSupport {
        displayName = "Weapons fireteam";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
        maxSize = 6;
        availableAtArray[] = {12, 24};
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_CSAT_SquadLeader", 1},
            {"A3_CSAT_Rifleman", -1},
            {"A3_CSAT_Medic", 2},
            {"A3_CSAT_MG", 3},
            {"A3_CSAT_HAT", 4},
            {"A3_CSAT_Marksman", 6}
        };
    };

    class A3_CSAT_Crewman: Crewman_basesquad {
        side = 0;
        roles[] = {
            {"A3_CSAT_Crewman", -1}
        };
    };

    // class A3_CSAT_StaticMGTeam: HMG_basesquad {
    //     side = 0;
    //     staticWeaponType = "O_HMG_01_F";
    //     roles[] = {
    //         {"A3_CSAT_HMGLeader", 1},
    //         {"A3_CSAT_HMGAmmo", -1}
    //     };
    // };
};
