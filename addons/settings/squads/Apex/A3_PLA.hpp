class A3_PLA {

    staticCompositions[] = {
        "CSAT_ATGMS",
        "CSAT_MANPADS",
        "CSAT_HMGS"
    };

    class A3_PLA_Infantry {
        displayName = "Infantry";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
        maxSize = 9;
        availableAt = -1;
        aiSquadType = 1;
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_PLA_SquadLeader", 1},
            {"A3_PLA_Rifleman",-1},
            {"A3_PLA_Medic", 2},
            {"A3_PLA_AR", 4},
            {"A3_PLA_LAT", 5},
            {"A3_PLA_Engineer", 5},
            {"A3_PLA_Grenadier", 6},
            {"A3_PLA_AA", 3},
            {"A3_PLA_Crewman", -1}

        };
    };

    class A3_PLA_WPSupport {
        displayName = "Weapons fireteam";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
        maxSize = 6;
        availableAtArray[] = {12, 24};
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_PLA_SquadLeader", 1},
            {"A3_PLA_Rifleman", -1},
            {"A3_PLA_Medic", 2},
            {"A3_PLA_MG", 3},
            {"A3_PLA_HAT", 4},
            {"A3_PLA_Marksman", 6}
        };
    };

    class A3_PLA_Crewman: Crewman_basesquad {
        side = 0;
        roles[] = {
            {"A3_PLA_Crewman", -1}
        };
    };

    // class A3_PLA_StaticMGTeam: HMG_basesquad {
    //     side = 0;
    //     staticWeaponType = "O_HMG_01_F";
    //     roles[] = {
    //         {"A3_PLA_HMGLeader", 1},
    //         {"A3_PLA_HMGAmmo", -1}
    //     };
    // };
};
