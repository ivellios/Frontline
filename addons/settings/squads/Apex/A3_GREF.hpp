class A3_GREF {

    staticCompositions[] = {
        "GREF_ATGMS",
        "GREF_MANPADS",
        "GREF_HMGS"
    };

    class A3_GREF_Squad {
        displayName = "Infantry";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
        maxSize = 9;
        availableAt = -1;
        aiSquadType = 1;
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_GREF_SquadLeader", 1},
            {"A3_GREF_Rifleman",-1},
            {"A3_GREF_Medic", 2},
            {"A3_GREF_AR", 4},
            {"A3_GREF_LAT", 5},
            {"A3_GREF_Engineer", 5},
            {"A3_GREF_Grenadier", 6},
            {"A3_GREF_AA", 3},
            {"A3_GREF_Crewman", -1}
        };
    };

    class A3_GREF_WPSupport {
        displayName = "Weapons fireteam";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
        maxSize = 6;
        availableAtArray[] = {12, 24};
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_GREF_SquadLeader", 1},
            {"A3_GREF_Rifleman", -1},
            {"A3_GREF_Medic", 2},
            {"A3_GREF_MG", 3},
            {"A3_GREF_HAT", 4},
            {"A3_GREF_Marksman", 6}
        };
    };

    class A3_GREF_Crewman: Crewman_basesquad {
        side = 0;
        roles[] = {
            {"A3_GREF_Crewman", -1}
        };
    };
};
