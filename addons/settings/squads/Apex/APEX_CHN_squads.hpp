
class Apex_China {

    staticCompositions[] = {
        "CSAT_ATGMS",
        "CSAT_MANPADS",
        "CSAT_HMGS"
    };

    class APEX_CHN_SquadSmall {
        displayName = "Light Inf";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
        side = 0;
        scope = 2;
        maxSize = 6;
        availableAt = -1;
        roles[] = {
            {"APEX_CHN_SquadLeader", 1},
            {"APEX_CHN_Rifleman",-1},
            {"APEX_CHN_Medic", 2},
            {"APEX_CHN_LAT", 5},
            {"APEX_CHN_AR", 5},
            {"APEX_CHN_Crewman", -1}
        };
    };

    class APEX_CHN_Squad1: APEX_CHN_SquadSmall {
        displayName = "Infantry A";
        maxSize = 9;
        availableAt = -1;
        aiSquadType = 1;
        roles[] = {
            {"APEX_CHN_SquadLeader", 1},
            {"APEX_CHN_Rifleman",-1},
            {"APEX_CHN_Medic", 2},
            {"APEX_CHN_AR", 4},
            {"APEX_CHN_Grenadier", 5},
            {"APEX_CHN_LAT", 5},
            {"APEX_CHN_Engineer", 5}

        };
    };

    class APEX_CHN_Squad2: APEX_CHN_Squad1 {
        displayName = "Infantry B";
        roles[] = {
            {"APEX_CHN_SquadLeader", 1},
            {"APEX_CHN_Rifleman",-1},
            {"APEX_CHN_Medic", 2},
            {"APEX_CHN_AR", 4},
            {"APEX_CHN_LAT", 5},
            {"APEX_CHN_Engineer", 5},
            {"APEX_CHN_Marksman", 8}
        };
    };

    class APEX_CHN_SquadMG {
        displayName = "MG";
        side = 0;
        scope = 0;
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
        availableAt = 15;
        roles[] = {
            {"APEX_CHN_TeamLeader_MG", 1},
            {"APEX_CHN_MG", 2},
            {"APEX_CHN_Rifleman", -1}
        };
    };

    class APEX_Crewman: Crewman_basesquad {
        side = 0;
        roles[] = {
            {"APEX_CHN_Crewman", -1}
        };
    };

    class APEX_StaticMGTeam: HMG_basesquad {
        side = 0;
        staticWeaponType = "O_HMG_01_F";
        roles[] = {
            {"APEX_CHN_HMGLeader", 1},
            {"APEX_CHN_HMGAmmo", -1}
        };
    };
};
