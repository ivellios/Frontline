class A3_GUER {

    staticCompositions[] = {
        "GREF_MANPADS",
        "GREF_HMGS"
    };

    class A3_GUER_Infantry {
        displayName = "Infantry";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
        maxSize = 9;
        availableAt = -1;
        aiSquadType = 1;
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_GUER_SquadLeader", 1},
            {"A3_GUER_Rifleman",-1},
            {"A3_GUER_Medic", 2},
            {"A3_GUER_AR", 4},
            {"A3_GUER_LAT", 5},
            {"A3_GUER_Engineer", 5},
            {"A3_GUER_Grenadier", 6},
            {"A3_GUER_AA", 3},
            {"A3_GUER_Crewman", -1}

        };
    };

    class A3_GUER_WPSupport {
        displayName = "Weapons fireteam";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
        maxSize = 6;
        availableAtArray[] = {12, 24};
        side = 0;
        scope = 2;
        roles[] = {
            {"A3_GUER_SquadLeader", 1},
            {"A3_GUER_Rifleman", -1},
            {"A3_GUER_Medic", 2},
            {"A3_GUER_MG", 3},
            {"A3_GUER_HAT", 4},
            {"A3_GUER_Marksman", 6}
        };
    };

    class A3_GUER_Crewman: Crewman_basesquad {
        side = 0;
        roles[] = {
            {"A3_GUER_Crewman", -1}
        };
    };

    // class A3_GUER_StaticMGTeam: HMG_basesquad {
    //     side = 0;
    //     staticWeaponType = "O_HMG_01_F";
    //     roles[] = {
    //         {"A3_GUER_HMGLeader", 1},
    //         {"A3_GUER_HMGAmmo", -1}
    //     };
    // };
};
