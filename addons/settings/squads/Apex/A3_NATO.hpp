
class A3_NATO {

    staticCompositions[] = {
        "NATO_ATGMS",
        "NATO_MANPADS",
        "NATO_HMGS"
    };

    class A3_NATO_Squad {
        displayName = "Infantry";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
        maxSize = 9;
        availableAt = -1;
        aiSquadType = 1;
        side = 1;
        scope = 2;
        roles[] = {
            {"A3_NATO_SquadLeader", 1},
            {"A3_NATO_Rifleman",-1},
            {"A3_NATO_Medic", 2},
            {"A3_NATO_AR", 4},
            {"A3_NATO_LAT", 5},
            {"A3_NATO_Engineer", 5},
            {"A3_NATO_Grenadier", 6},
            {"A3_NATO_AA", 3},
            {"A3_NATO_Crewman", -1}

        };
    };

    class A3_NATO_WPSupport {
        displayName = "Weapons fireteam";
        mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
        maxSize = 6;
        availableAtArray[] = {12, 24};
        side = 1;
        scope = 2;
        roles[] = {
            {"A3_NATO_SquadLeader", 1},
            {"A3_NATO_Rifleman", -1},
            {"A3_NATO_Medic", 2},
            {"A3_NATO_MG", 3},
            {"A3_NATO_HAT", 4},
            {"A3_NATO_Marksman", 6}
        };
    };

    class A3_NATO_Crewman: Crewman_basesquad {
        side = 1;
        roles[] = {
            {"A3_NATO_Crewman", -1}
        };
    };

    // class A3_NATO_StaticMGTeam: HMG_basesquad {
    //     side = 1;
    //     staticWeaponType = "B_HMG_01_F";
    //     roles[] = {
    //         {"A3_NATO_HMGLeader", 1},
    //         {"A3_NATO_HMGAmmo", -1}
    //     };
    // };
};
