class Apex_NATO {

	staticCompositions[] = {
		"NATO_ATGMS",
		"NATO_MANPADS",
		"NATO_HMGS"
	};

	class APEX_NATO_SquadSmall {
		displayName = "Light Inf";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 6;
		availableAt = -1;
		roles[] = {
			{"APEX_NATO_SquadLeader", 1},
			{"APEX_NATO_Rifleman",-1},
			{"APEX_NATO_Medic", 2},
			{"APEX_NATO_LAT", 5},
			{"APEX_NATO_AR", 5},
            {"APEX_NATO_Crewman", -1}
		};
	};

	class APEX_NATO_Squad1: APEX_NATO_SquadSmall {
		displayName = "Infantry A";
		maxSize = 9;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"APEX_NATO_SquadLeader", 1},
			{"APEX_NATO_Rifleman",-1},
			{"APEX_NATO_Medic", 2},
			{"APEX_NATO_AR", 4},
			{"APEX_NATO_Grenadier", 5},
			{"APEX_NATO_LAT", 5},
			{"APEX_NATO_Engineer", 5}
		};
	};

	class APEX_NATO_Squad2: APEX_NATO_Squad1 {
		displayName = "Infantry B";
		roles[] = {
			{"APEX_NATO_SquadLeader", 1},
			{"APEX_NATO_Rifleman",-1},
			{"APEX_NATO_Medic", 2},
			{"APEX_NATO_AR", 4},
			{"APEX_NATO_LAT", 5},
			{"APEX_NATO_Engineer", 5},
			{"APEX_NATO_Marksman", 8}
		};
	};

	class APEX_NATO_SquadMG {
		displayName = "MG";
		side = 1;
		scope = 0;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = 15;
		roles[] = {
			{"APEX_NATO_TeamLeader_MG", 1},
			{"APEX_NATO_MG", 2},
			{"APEX_NATO_Rifleman", -1}
		};
	};

	class APEX_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"APEX_NATO_Crewman", -1}
		};
	};

	class APEX_StaticMGTeam: HMG_basesquad {
		side = 1;
		staticWeaponType = "B_HMG_01_F";
		roles[] = {
			{"APEX_NATO_HMGLeader", 1},
			{"APEX_NATO_HMGAmmo", -1}
		};
	};

};
