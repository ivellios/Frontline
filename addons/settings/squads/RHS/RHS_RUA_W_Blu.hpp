
class RHS_RUA_Woodland_Blu {
	staticCompositions[] = {
		"CUP",

		"RHS",

		"HESCOS",
		"RU_HMGS",
		"RU_MORTAR",
		"RU_ATGMS",
		"RU_MANPADS"
	};

	class RHS_RUA_W_B_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"RHS_RUA_W_SquadLeader", 1},
			{"RHS_RUA_W_Rifleman",-1},
			{"RHS_RUA_W_Medic", 2},
			{"RHS_RUA_W_LAT", 3},
			{"RHS_RUA_W_AR", 4},
			{"RHS_RUA_W_HAT", 4},
			{"RHS_RUA_W_Engineer", 4},
			{"RHS_RUA_W_Grenadier", 6},
			{"RHS_RUA_W_AA", 3},
			{"RHS_RUA_W_Crewman", -1}
		};
	};

	class RHS_RUA_W_B_Squad4: RHS_RUA_W_B_Squad1 {
		displayName = "Recon";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_RUA_W_RECON_TeamLeader", 1},
			{"RHS_RUA_W_RECON_Rifleman",-1},
			{"RHS_RUA_W_RECON_LAT", 3},
			{"RHS_RUA_W_RECON_Engineer", 3}
		};
	};
/*
	class RHS_RUA_W_B_Squad2: RHS_RUA_W_B_Squad1 {
		displayName = "Regular (Marksman)";
		availableAt = -1;
		availableAtArray[] = {25};
		maxSize = 6;
		roles[] = {
			{"RHS_RUA_W_SquadLeader", 1},
			{"RHS_RUA_W_Rifleman",-1},
			{"RHS_RUA_W_Medic", 2},
			{"RHS_RUA_W_LAT", 3},
			{"RHS_RUA_W_AR", 4},
			{"RHS_RUA_W_HAT", 4},
			{"RHS_RUA_W_Engineer", 4},
			{"RHS_RUA_W_Marksman", 5},
			{"RHS_RUA_W_AA", 3},
			{"RHS_RUA_W_Crewman", -1}
		};
	};
	class RHS_RUA_W_B_SquadMG {
		displayName = "MG Team";
		side = 1;
		scope = 2;
		maxSize = 3;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {16,40};
		roles[] = {
			{"RHS_RUA_W_TeamLeader_MG", 1},
			{"RHS_RUA_W_MG", 2},
			{"RHS_RUA_W_Rifleman", -1}
		};
	};

	class RHS_StaticMGTeam: HMG_basesquad {
		side = 1;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "rhs_KORD_MSV";
		roles[] = {
			{"RHS_RUA_W_HMGLeader", 1},
			{"RHS_RUA_W_HMGAmmo", -1}
		};
	};
*/
	class RHS_RUA_W_B_Sniper {
		displayName = "Sniper Team";
		side = 1;
		scope = 2;
		maxSize = 2;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"RHS_RUA_W_Sniper", 1},
			{"RHS_RUA_W_Spotter", -1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"RHS_RUA_W_Crewman", -1}
		};
	};

	class RHS_RUA_W_B_UAVOperator: RHS_RUA_W_B_Squad1 {
		displayName = "UAV Operator";
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_RUA_W_Blu_UAVOperator", 1}
		};
	};
};
