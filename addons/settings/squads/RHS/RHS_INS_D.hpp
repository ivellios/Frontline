
class RHS_Insurgents_Desert {
	staticCompositions[] = {
		"CUP",

		"RHS",
		"RU_MANPADS",

		"INS_HMGS",
		"RU_ATGMS",
		"INS_MORTAR",
		"INS_DESERT",
		"INS_AA"  // -- namely ZSU-23-2
	};

	class RHS_INS_D_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"RHS_INS_D_SquadLeader", 1},
			{"RHS_INS_D_Rifleman",-1},
			{"RHS_INS_D_Medic", 2},
			{"RHS_INS_D_LAT", 3},
			{"RHS_INS_D_AR", 4},
			{"RHS_INS_D_HAT", 4},
			{"RHS_INS_D_Engineer", 4},
			{"RHS_INS_D_Grenadier", 6},
			{"RHS_INS_D_AA", 3},
			{"RHS_INS_D_Crewman", -1}
		};
	};
/*
	class RHS_INS_D_Squad2: RHS_INS_D_Squad1 {
		displayName = "Regular (Marksman)";
        availableAt = -1;
		availableAtArray[] = {25};
		maxSize = 6;
		roles[] = {
			{"RHS_INS_D_SquadLeader", 1},
			{"RHS_INS_D_Rifleman",-1},
			{"RHS_INS_D_Medic", 2},
			{"RHS_INS_D_LAT", 3},
			{"RHS_INS_D_AR", 4},
			{"RHS_INS_D_HAT", 4},
			{"RHS_INS_D_Engineer", 4},
			{"RHS_INS_D_Marksman", 5},
			{"RHS_INS_D_AA", 3},
			{"RHS_INS_D_Crewman", -1}
		};
	};
*/
	class RHS_INS_D_Squad3: RHS_INS_D_Squad1 {
		displayName = "Foreign Fighters";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_INS_D_FF_TeamLeader", 1},
			{"RHS_INS_D_FF_Rifleman",-1},
			{"RHS_INS_D_FF_LAT", 3},
			{"RHS_INS_D_FF_Engineer", 3}
		};
	};
/*
	class RHS_INS_D_SquadMG {
		displayName = "MG Team";
		side = 0;
		scope = 2;
		maxSize = 3;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {16,40};
		roles[] = {
			{"RHS_INS_D_TeamLeader_MG", 1},
			{"RHS_INS_D_MG", 2},
			{"RHS_INS_D_Rifleman", -1}
		};
	};
	class RHS_StaticMGTeam: HMG_basesquad {
		side = 0;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "rhsgref_ins_g_DSHKM_Mini_Tripod";
		roles[] = {
			{"RHS_INS_D_HMGLeader", 1},
			{"RHS_INS_D_HMGAmmo", -1}
		};
	};
*/
	class RHS_INS_D_Sniper {
		displayName = "Sniper Team";
		side = 0;
		scope = 2;
		maxSize = 2;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"RHS_INS_D_Sniper", 1},
			{"RHS_INS_D_Spotter", -1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"RHS_INS_D_Crewman", -1}
		};
	};

	class RHS_StaticSPGTeam: HMG_basesquad {
		displayName = "SPG Team";
		side = 0;
		availableAt = -1;
		availableAtArray[] = {15};
		staticWeaponType = "rhs_SPG9M_MSV";
		roles[] = {
			{"RHS_INS_D_HMGLeader", 1},
			{"RHS_INS_D_HMGAmmo", -1}
		};
	};

	class RHS_INS_D_UAVOperator: RHS_INS_D_Squad1 {
		displayName = "UAV Operator";
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_INS_D_UAVOperator", 1}
		};
	};
};
