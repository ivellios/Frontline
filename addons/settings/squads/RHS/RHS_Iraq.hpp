
class RHS_Iraq {
	staticCompositions[] = {
		"CUP",

		"RHS",

		"HESCOS",
		"US_RHS_HMGS",
		"US_RHS_MORTAR",
		"US_ATGMS",
		"US_MANPADS",

		"INS_AA"  // -- namely ZSU-23-2

	};

	class RHS_Iraq_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"RHS_Iraq_SquadLeader", 1},
			{"RHS_Iraq_Rifleman",-1},
			{"RHS_Iraq_Medic", 2},
			{"RHS_Iraq_LAT", 3},
			{"RHS_Iraq_AR", 4},
			{"RHS_Iraq_HAT", 4},
			{"RHS_Iraq_Engineer", 4},
			{"RHS_Iraq_Grenadier", 6},
			{"RHS_Iraq_AA", 3},
			{"RHS_Iraq_Crewman", -1}
		};
	};

	class RHS_Iraq_Squad4: RHS_Iraq_Squad1 {
		displayName = "Recon";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_Iraq_RECON_TeamLeader", 1},
			{"RHS_Iraq_RECON_Rifleman",-1},
			{"RHS_Iraq_RECON_LAT", 3},
			{"RHS_Iraq_RECON_Engineer", 3}
		};
	};
    
/*
	class RHS_Iraq_Squad2: RHS_Iraq_Squad1 {
		displayName = "Regular (Marksman)";
		availableAt = -1;
		availableAtArray[] = {25};
		maxSize = 6;
		roles[] = {
			{"RHS_Iraq_SquadLeader", 1},
			{"RHS_Iraq_Rifleman",-1},
			{"RHS_Iraq_Medic", 2},
			{"RHS_Iraq_LAT", 3},
			{"RHS_Iraq_AR", 4},
			{"RHS_Iraq_HAT", 4},
			{"RHS_Iraq_Engineer", 4},
			{"RHS_Iraq_Marksman", 5},
			{"RHS_Iraq_AA", 3},
			{"RHS_Iraq_Crewman", -1}
		};
	};
    
	class RHS_Iraq_SquadMG {
		displayName = "MG Team";
		side = 1;
		scope = 2;
		maxSize = 3;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {16,40};
		roles[] = {
			{"RHS_Iraq_TeamLeader_MG", 1},
			{"RHS_Iraq_MG", 2},
			{"RHS_Iraq_Rifleman", -1}
		};
	};
	class RHS_StaticMGTeam: HMG_basesquad {
		side = 1;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "RHS_M2StaticMG_MiniTripod_WD";
		roles[] = {
			{"RHS_Iraq_HMGLeader", 1},
			{"RHS_Iraq_HMGAmmo", -1}
		};
	};
*/
	class RHS_Iraq_Sniper {
		displayName = "Sniper Team";
		side = 1;
		scope = 2;
		maxSize = 2;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"RHS_Iraq_Sniper", 1},
			{"RHS_Iraq_Spotter", -1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"RHS_Iraq_Crewman", -1}
		};
	};

	class RHS_Iraq_UAVOperator: RHS_Iraq_Squad1 {
		displayName = "UAV Operator";
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_Iraq_UAVOperator", 1}
		};
	};
};
