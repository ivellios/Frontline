
class RHS_Insurgents_Woodland {
	staticCompositions[] = {
		"CUP",
		
		"RHS",
		"RU_MANPADS",

		"INS_HMGS",
		"RU_ATGMS",
		"INS_MORTAR",
		//"INS_DESERT",
		"INS_AA"  // -- namely ZSU-23-2
	};



	class RHS_INS_W_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"RHS_INS_W_SquadLeader", 1},
			{"RHS_INS_W_Rifleman",-1},
			{"RHS_INS_W_Medic", 2},
			{"RHS_INS_W_LAT", 3},
			{"RHS_INS_W_AR", 4},
			{"RHS_INS_W_HAT", 4},
			{"RHS_INS_W_Engineer", 4},
			{"RHS_INS_W_Grenadier", 6},
			{"RHS_INS_W_AA", 3},
			{"RHS_INS_W_Crewman", -1}
		};
	};

	class RHS_INS_W_Squad3: RHS_INS_W_Squad1 {
		displayName = "Foreign Fighters";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_INS_W_FF_TeamLeader", 1},
			{"RHS_INS_W_FF_Rifleman",-1},
			{"RHS_INS_W_FF_LAT", 3},
			{"RHS_INS_W_FF_Engineer", 3}
		};
	};
/*
	class RHS_INS_W_Squad2: RHS_INS_W_Squad1 {
		displayName = "Regular (Marksman)";
		availableAt = -1;
		availableAtArray[] = {25};
		maxSize = 6;
		roles[] = {
			{"RHS_INS_W_SquadLeader", 1},
			{"RHS_INS_W_Rifleman",-1},
			{"RHS_INS_W_Medic", 2},
			{"RHS_INS_W_LAT", 3},
			{"RHS_INS_W_AR", 4},
			{"RHS_INS_W_HAT", 4},
			{"RHS_INS_W_Engineer", 4},
			{"RHS_INS_W_Marksman", 5},
			{"RHS_INS_W_AA", 3},
			{"RHS_INS_W_Crewman", -1}
		};
	};
    
	class RHS_INS_W_SquadMG {
		displayName = "MG Team";
		side = 0;
		scope = 2;
		maxSize = 3;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {16,40};
		roles[] = {
			{"RHS_INS_W_TeamLeader_MG", 1},
			{"RHS_INS_W_MG", 2},
			{"RHS_INS_W_Rifleman", -1}
		};
	};

	class RHS_StaticMGTeam: HMG_basesquad {
		side = 0;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "rhsgref_ins_g_DSHKM_Mini_Tripod";
		roles[] = {
			{"RHS_INS_W_HMGLeader", 1},
			{"RHS_INS_W_HMGAmmo", -1}
		};
	};
*/
	class RHS_INS_W_Sniper {
		displayName = "Sniper Team";
		side = 0;
		scope = 2;
		maxSize = 2;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"RHS_INS_W_Sniper", 1},
			{"RHS_INS_W_Spotter", -1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"RHS_INS_W_Crewman", -1}
		};
	};

	class RHS_StaticSPGTeam: HMG_basesquad {
		displayName = "SPG Team";
		side = 0;
		availableAt = -1;
		availableAtArray[] = {15};
		staticWeaponType = "rhs_SPG9M_MSV";
		roles[] = {
			{"RHS_INS_W_HMGLeader", 1},
			{"RHS_INS_W_HMGAmmo", -1}
		};
	};

	class RHS_INS_W_UAVOperator: RHS_INS_W_Squad1 {
		displayName = "UAV Operator";
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_INS_W_UAVOperator", 1}
		};
	};
};
