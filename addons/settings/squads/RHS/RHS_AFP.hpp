class RHS_AFP {
	staticCompositions[] = {
		"CUP",
		"RHS",
		"HESCOS",
		"US_RHS_HMGS",
		"US_RHS_MORTAR",
		"US_ATGMS",
		"US_MANPADS",
		"INS_AA"  // -- namely ZSU-23-2

	};

	class RHS_AFP_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"RHS_AFP_SquadLeader", 1},
			{"RHS_AFP_Rifleman",-1},
			{"RHS_AFP_Medic", 2},
			{"RHS_AFP_LAT", 3},
			{"RHS_AFP_AR", 4},
			{"RHS_AFP_HAT", 4},
			{"RHS_AFP_Engineer", 4},
			{"RHS_AFP_Grenadier", 6},
			{"RHS_AFP_AA", 3},
			{"RHS_AFP_Crewman", -1}
		};
	};

	class RHS_AFP_Squad4: RHS_AFP_Squad1 {
		displayName = "Recon";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_AFP_RECON_TeamLeader", 1},
			{"RHS_AFP_RECON_Rifleman",-1},
			{"RHS_AFP_RECON_LAT", 3},
			{"RHS_AFP_RECON_Engineer", 3}
		};
	};

	class RHS_AFP_Sniper {
		displayName = "Sniper Team";
		side = 1;
		scope = 2;
		maxSize = 2;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"RHS_AFP_Sniper", 1},
			{"RHS_AFP_Spotter", -1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"RHS_AFP_Crewman", -1}
		};
	};

	class RHS_AFP_UAVOperator: RHS_AFP_Squad1 {
		displayName = "UAV Operator";
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_AFP_UAVOperator", 1}
		};
	};
};
