
class RHS_UN_Desert {
	staticCompositions[] = {
		"CUP",

		"RHS",

		"HESCOS",
		"RU_MORTAR",
		"US_RHS_HMGS",
		"US_ATGMS",
		"US_MANPADS",

		"INS_AA"  // -- namely ZSU-23-2
	};

	class RHS_UN_D_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"RHS_UN_D_SquadLeader", 1},
			{"RHS_UN_D_Rifleman",-1},
			{"RHS_UN_D_Medic", 2},
			{"RHS_UN_D_LAT", 3},
			{"RHS_UN_D_AR", 4},
			{"RHS_UN_D_HAT", 4},
			{"RHS_UN_D_Engineer", 4},
			{"RHS_UN_D_Grenadier", 6},
			{"RHS_UN_D_AA", 3},
			{"RHS_UN_D_Crewman", -1}
		};
	};

/*
	class RHS_UN_D_Squad2: RHS_UN_D_Squad1 {
		displayName = "Regular (Marksman)";
		availableAt = -1;
		availableAtArray[] = {25};
		maxSize = 6;
		roles[] = {
			{"RHS_UN_D_SquadLeader", 1},
			{"RHS_UN_D_Rifleman",-1},
			{"RHS_UN_D_Medic", 2},
			{"RHS_UN_D_LAT", 3},
			{"RHS_UN_D_AR", 4},
			{"RHS_UN_D_HAT", 4},
			{"RHS_UN_D_Engineer", 4},
			{"RHS_UN_D_Marksman", 5},
			{"RHS_UN_D_AA", 3},
			{"RHS_UN_D_Crewman", -1}
		};
	};
    
	class RHS_UN_D_Squad4: RHS_UN_D_Squad1 {
		displayName = "Recon";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {20};
		roles[] = {
			{"RHS_UN_D_RECON_TeamLeader", 1},
			{"RHS_UN_D_RECON_Rifleman",-1},
			{"RHS_UN_D_RECON_LAT", 3},
			{"RHS_UN_D_RECON_Engineer", 3}
		};
	};

	class RHS_UN_D_SquadMG {
		displayName = "MG Team";
		side = 1;
		scope = 2;
		maxSize = 3;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {16,40};
		roles[] = {
			{"RHS_UN_D_TeamLeader_MG", 1},
			{"RHS_UN_D_MG", 2},
			{"RHS_UN_D_Rifleman", -1}
		};
	};

	class RHS_StaticMGTeam: HMG_basesquad {
		side = 1;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "rhs_KORD_MSV";
		roles[] = {
			{"RHS_UN_D_HMGLeader", 1},
			{"RHS_UN_D_HMGAmmo", -1}
		};
	};
*/
	class RHS_UN_D_Sniper {
		displayName = "Sniper Team";
		side = 1;
		scope = 2;
		maxSize = 2;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"RHS_UN_D_Sniper", 1},
			{"RHS_UN_D_Spotter", -1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"RHS_UN_D_Crewman", -1}
		};
	};

	class RHS_UN_D_UAVOperator: RHS_UN_D_Squad1 {
		displayName = "UAV Operator";
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"RHS_UN_D_UAVOperator", 1}
		};
	};
};
