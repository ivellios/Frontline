
class IFA_USA {

	staticCompositions[] = {
		"CUP",
		"IFA",
		"IFA_SUMMER",

		"US_ATCrate",
		"US_IFA_HMGS",
		"US_IFA_MORTAR",
		"US_SUMMER",
		//"US_WINTER",
		"US_CANONS",
		"USSR_AA"
	};

	class IFA_USA_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_USA_SquadLeader", 1},
			{"IFA_USA_Medic", 2},
			{"IFA_USA_Rifleman",-1},
			{"IFA_USA_Engineer", 4},
			{"IFA_USA_MG", 4},
            {"IFA_USA_Grenade_Rifleman", 6},
            {"IFA_USA_Flamethrower", 8},
			//{"IFA_USA_Corporal", 8},
            {"IFA_USA_Crewman",-1}
		};
	};
/*
	class IFA_USA_MortarSmall {
		displayName = "Mortar Squad";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAtArray[] = {18};
		roles[] = {
			{"IFA_USA_SquadLeader", 1},
			{"IFA_USA_Medic", 2},
			{"IFA_USA_Rifleman",-1},
			{"IFA_USA_MG", 4},
            {"IFA_USA_MortarTube", 6},
			{"IFA_USA_MortarAmmo", 6}
		};
	};
*/
	class IFA_USA_Sniper {
		displayName = "Sniper Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_USA_Sniper", 1},
			{"IFA_USA_Spotter", 1}
		};
	};
/*
	class IFA_USA_HMGSquad {
		displayName = "HMG Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
		side = 0;
		scope = 2;
		maxSize = 3;
		availableAt = -1;
		availableAtArray[] = {30};
		roles[] = {
			{"IFA_USA_HMG", 1},
			{"IFA_USA_MGAssistant",-1}
		};
	};
*/
	class IFA_USA_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_USA_HAT", 1},
			{"IFA_USA_HAT_Ammo",2}
		};
	};

	class IFA_USA_AirborneSquad {
		displayName = "Airborne";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 5;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"IFA_USA_AB_SquadLeader", 1},
			{"IFA_USA_AB_Medic", 2},
			{"IFA_USA_AB_Corporal", 3},
			{"IFA_USA_AB_MG", 3},
			{"IFA_USA_AB_Engineer", -1}
		};
	};

    class IFA_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"IFA_USA_Crewman", -1}
		};
	};

/*
	class IFA_StaticMGTeam: HMG_basesquad {
		side = 0;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "LIB_M1919_M2";
		roles[] = {
			{"IFA_USA_HMGLeader", 1},
			{"IFA_USA_HMGAmmo", -1}
		};
	};
    class IFA_StaticMortarTeam: HMG_basesquad {
        displayName = "Mortar Team";
		side = 0;
        availableAtArray[] = {20};
        maxSize = 2;
		staticWeaponType = "LIB_M2_M60";
		roles[] = {
			{"IFA_USA_HMGLeader", 1},
			{"IFA_USA_HMGAmmo", -1}
		};
	};*/
};
