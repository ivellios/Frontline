
class IFA_UK_D {

	staticCompositions[] = {
		"CUP",

		"IFA",
		"IFA_SUMMER",

		"UK_ATCrate",
		"UK_HMGS",
		"US_IFA_MORTAR",
		//"UK_SUMMER",
		"IFA_DESERT",
		//"UK_WINTER",
		"UK_CANNONS",
		"USSR_AA"
	};

	class IFA_UK_D_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_UK_D_SquadLeader", 1},
			{"IFA_UK_D_Medic", 2},
			{"IFA_UK_D_Rifleman",-1},
			{"IFA_UK_D_Engineer", 4},
			{"IFA_UK_D_LAT", 4},
			{"IFA_UK_D_MG", 4},
			{"IFA_UK_D_MG_1", 6},
            {"IFA_UK_D_Grenade_Rifleman", 6},
			{"IFA_UK_D_Corporal", 7},
            {"IFA_UK_D_Flamethrower", 8},
            {"IFA_UK_D_Crewman",-1}
		};
	};
	class IFA_UK_D_Sniper {
		displayName = "Sniper Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_UK_D_Sniper", 1},
			{"IFA_UK_D_Spotter", 1}
		};
	};

	class IFA_UK_D_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_UK_D_HAT", 1},
			{"IFA_UK_D_HAT_Ammo",2}
		};
	};
	/*
	class IFA_UK_D_Commandos {
		displayName = "Commandos";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 5;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"IFA_UK_D_Com_SquadLeader", 1},
			{"IFA_UK_D_Com_Medic", 2},
			{"IFA_UK_D_Com_Corporal", 3},
			{"IFA_UK_D_Com_MG", 3},
			{"IFA_UK_D_Com_Engineer", 3}
		};
	};
	*/
    class IFA_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"IFA_UK_D_Crewman", -1}
		};
	};
};
