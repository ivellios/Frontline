
class IFA_USSR_W {

	staticCompositions[] = {
		"CUP",

		"IFA",
		"IFA_WINTER",

		"USSR_ATCrate",
		"USSR_HMGS",
		"USSR_MORTAR",
		//"USSR_SUMMER",
		"USSR_WINTER",
		"USSR_CANNONS",
		//"USSR_CANNONS_SUMMER",
		"USSR_CANNONS_WINTER",
		"USSR_AA"
	};

	class IFA_USSR_W_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_USSR_W_SquadLeader", 1},
			{"IFA_USSR_W_Medic", 2},
			{"IFA_USSR_W_Rifleman",-1},
			{"IFA_USSR_W_Rifleman_Semi", 3},
			{"IFA_USSR_W_LAT", 4},
			{"IFA_USSR_W_MG", 6},
			{"IFA_USSR_W_MGAssistant", 6},
            {"IFA_USSR_W_Grenade_Rifleman", 7},
            {"IFA_USSR_W_Flamethrower", 8},
			{"IFA_USSR_W_SMG", 8},
            {"IFA_USSR_Crewman",-1}
		};
	};
/*
	class IFA_USSR_MortarSmall {
		displayName = "Mortar Squad";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAtArray[] = {18};
		roles[] = {
			{"IFA_USSR_W_SquadLeader", 1},
			{"IFA_USSR_W_Medic", 2},
			{"IFA_USSR_W_Rifleman",-1},
			{"IFA_USSR_W_LAT", 4},
			{"IFA_USSR_W_MG", 6},
			{"IFA_USSR_W_MGAssistant", 6},
            {"IFA_USSR_W_MortarTube", 7},
			{"IFA_USSR_W_MortarAmmo", 7},
			{"IFA_USSR_W_SMG", 8}
		};
	};
*/
	class IFA_USSR_W_Sniper {
		displayName = "Sniper Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_USSR_W_Sniper", 1},
			{"IFA_USSR_W_Spotter", 1}
		};
	};
	class IFA_USSR_W_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_USSR_W_HAT", 1},
			{"IFA_USSR_W_HAT_Ammo",2}
		};
	};
	class IFA_USSR_W_PartisanSquad {
		displayName = "Partisans";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_civ.paa";
		side = 0;
		scope = 2;
		maxSize = 5;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"IFA_USSR_PART_SquadLeader", 1},
			{"IFA_USSR_PART_SMG", -1},
			{"IFA_USSR_PART_Medic", 2},
			{"IFA_USSR_PART_LAT", -1}
		};
	};
    class IFA_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"IFA_USSR_Crewman", -1}
		};
	};

/*
	class IFA_USSR_W_HMGSquad {
		displayName = "HMG Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
		side = 0;
		scope = 2;
		maxSize = 3;
		availableAt = -1;
		availableAtArray[] = {30};
		roles[] = {
			{"IFA_USSR_W_HMG", 1},
			{"IFA_USSR_W_HMGAssistant",2},
			{"IFA_USSR_W_MGAmmo", 2}
		};
	};
	class IFA_StaticMGTeam: HMG_basesquad {
		side = 0;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "LIB_Maxim_M30_base";
		roles[] = {
			{"IFA_USSR_W_HMGLeader", 1},
			{"IFA_USSR_W_HMGAmmo", -1}
		};
	};
    class IFA_StaticMortarTeam: HMG_basesquad {
        displayName = "Mortar Team";
		side = 0;
        availableAtArray[] = {18};
        maxSize = 2;
		staticWeaponType = "LIB_BM37";
		roles[] = {
			{"IFA_USSR_HMGLeader", 1},
			{"IFA_USSR_HMGAmmo", -1}
		};
	};
*/
};
