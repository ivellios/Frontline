
class IFA_JP {

	staticCompositions[] = {
		"CUP",

		"IFA",
		"IFA_SUMMER",

		"GER_BUNKERS",
		"JP_ATCrate",
		"JP_HMGS",
		"JP_MORTAR",
		"JP_SUMMER",
		"GER_AA"
		//"JP_WINTER",

	};

	class IFA_JP_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_JP_SquadLeader", 1},
			{"IFA_JP_Medic", 2},
			{"IFA_JP_Rifleman",-1},
			{"IFA_JP_LAT", 4},
			{"IFA_JP_MG", 4},
			{"IFA_JP_MG_1", 6},
			//{"IFA_JP_MGAssistant", 6},
            {"IFA_JP_Grenadier_Rifleman", 5},
            {"IFA_JP_Crewman",-1}
		};
	};
/*
	class IFA_JP_MortarSmall {
		displayName = "Mortar Squad";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAtArray[] = {18};
		roles[] = {
			{"IFA_JP_SquadLeader", 1},
			{"IFA_JP_Medic", 2},
			{"IFA_JP_Rifleman",-1},
			{"IFA_JP_LAT", 4},
			{"IFA_JP_MG", 6},
			{"IFA_JP_MGAssistant", 6},
            {"IFA_JP_MortarTube", 7},
			{"IFA_JP_MortarAmmo", 7}
		};
	};
*/
	class IFA_JP_Sniper {
		displayName = "Sniper";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 1;
		scope = 2;
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_JP_Sniper", 1}
		};
	};

	class IFA_JP_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 1;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_JP_HAT", 1},
			{"IFA_JP_HAT_Ammo",2}
		};
	};

    class IFA_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"IFA_JP_Crewman", -1}
		};
	};
};
