
class IFA_USA_AB {

	staticCompositions[] = {
		"CUP",

		"IFA",
		"IFA_SUMMER",

		"US_ATCrate",
		"US_IFA_HMGS",
		"US_IFA_MORTAR",
		"US_SUMMER",
		//"US_WINTER",
		"US_CANONS",
		"USSR_AA"


	};

	class IFA_USA_AB_Squad {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_USA_AB_SquadLeader", 1},
			{"IFA_USA_AB_Medic", 2},
			{"IFA_USA_AB_Rifleman", -1},
			{"IFA_USA_AB_Engineer", 3},
			{"IFA_USA_AB_MG", 4},
            {"IFA_USA_AB_Grenade_Rifleman", 7},
			{"IFA_USA_AB_Corporal", 8},
			{"IFA_USA_Crewman", 1}
		};
	};

	class IFA_USA_AB_Sniper {
		displayName = "Sniper Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_USA_AB_Sniper", 1},
			{"IFA_USA_AB_Spotter", 1}
		};
	};

	class IFA_USA_AB_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_USA_AB_HAT", 1},
			{"IFA_USA_AB_HAT_Ammo",2}
		};
	};

    class IFA_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"IFA_USA_Crewman", -1}
		};
	};
};
