
class IFA_GER_AB {

	staticCompositions[] = {
		// -- ww-2 baseobjects for everyone
		"CUP",

		"IFA",
		"IFA_SUMMER",
		//"IFA_WINTER",

		//"GER_BUNKERS",
		"GER_ATCrate",
		"GER_HMGS",
		"GER_MORTAR",
		"GER_SUMMER",
		//"GER_WINTER",
		"GER_CANONS",
		"GER_CANONS_SUMMER",
		//"GER_CANONS_WINTER",
		"GER_AA"

	};

	class IFA_GER_AB_Squad {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_GER_AB_SquadLeader", 1},
			{"IFA_GER_AB_Medic", 2},
			{"IFA_GER_AB_Rifleman",-1},
			{"IFA_GER_AB_Rifleman_Semi", 3},
			{"IFA_GER_AB_LAT", 4},
			{"IFA_GER_AB_MG", 6},
			{"IFA_GER_AB_MGAssistant", 6},
            {"IFA_GER_AB_Grenadier_Rifleman", 7},
			{"IFA_GER_AB_CorporalSMG", 8},
			{"IFA_GER_Crewman", 1}
		};
	};
	class IFA_GER_AB_Sniper {
		displayName = "Sniper";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 1;
		scope = 2;
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_GER_AB_Sniper", 1}
		};
	};

	class IFA_GER_AB_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 1;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_GER_AB_HAT", 1},
			{"IFA_GER_AB_HAT_Ammo",2}
		};
	};
    class IFA_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"IFA_GER_Crewman", -1}
		};
	};
};
