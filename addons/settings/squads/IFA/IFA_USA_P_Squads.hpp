
class IFA_USA_P {

	staticCompositions[] = {
		"CUP",
		// -- ww-2 baseobjects for everyone

		"IFA",
		"IFA_SUMMER",

		"US_ATCrate",
		"US_IFA_HMGS",
		"US_IFA_MORTAR",
		"US_SUMMER",
		//"US_WINTER",
		//"US_CANONS"
		"USSR_AA"
	};

	class IFA_USA_P_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_USA_P_SquadLeader", 1},
			{"IFA_USA_P_Medic", 2},
			{"IFA_USA_P_Rifleman",-1},
			{"IFA_USA_P_Rifleman_Semi", 3},
			{"IFA_USA_P_Engineer", 4},
			{"IFA_USA_P_MG", 4},
            //{"IFA_USA_P_Grenade_Rifleman", 6},
            {"IFA_USA_P_Flamethrower", 6},
			{"IFA_USA_P_Corporal", 8},
            {"IFA_USA_Crewman",-1}
		};
	};
/*
	class IFA_USA_P_MortarSmall {
		displayName = "Mortar Squad";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAtArray[] = {18};
		roles[] = {
			{"IFA_USA_P_SquadLeader", 1},
			{"IFA_USA_P_Medic", 2},
			{"IFA_USA_P_Rifleman",-1},
			{"IFA_USA_P_MG", 4},
            {"IFA_USA_P_MortarTube", 6},
			{"IFA_USA_P_MortarAmmo", 6}
		};
	};
*/
	class IFA_USA_P_Sniper {
		displayName = "Sniper Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_USA_P_Sniper", 1},
			{"IFA_USA_P_Spotter", 1}
		};
	};

	class IFA_USA_P_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_USA_P_HAT", 1},
			{"IFA_USA_P_HAT_Ammo",2}
		};
	};

    class IFA_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"IFA_USA_Crewman", -1}
		};
	};
};
