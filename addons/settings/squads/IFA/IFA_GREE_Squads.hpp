class IFA_GREE {

	staticCompositions[] = {

		// -- ww-2 baseobjects for everyone
		"CUP",
		"IFA",
		"IFA_SUMMER",
		"GER_ATCrate",
		"GER_HMGS",
		"GER_MORTAR",
		"GER_SUMMER",
	};

	class IFA_GREE_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_GREE_SquadLeader", 1},
			{"IFA_GREE_Medic", 2},
			{"IFA_GREE_Rifleman",-1},
			{"IFA_GREE_Rifleman_Semi", 3},
			{"IFA_GREE_LAT", 4},
			{"IFA_GREE_LAT_1", 4},
			{"IFA_GREE_MG", 6},
			{"IFA_GREE_CorporalSMG", 6},
			{"IFA_GREE_MGAssistant", 6},
            {"IFA_GREE_Grenadier_Rifleman", 6}
		};
	};
	class IFA_GREE_Sniper {
		displayName = "Sniper";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_GREE_Sniper", 1}
		};
	};
	class IFA_GREE_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_GREE_HAT", 1},
			{"IFA_GREE_HAT_Ammo",2}
		};
	};
	class IFA_UK_Commandos {
		displayName = "Commandos";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 5;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"IFA_UK_Com_SquadLeader", 1},
			{"IFA_UK_Com_Medic", 2},
			{"IFA_UK_Com_Corporal", 3},
			{"IFA_UK_Com_MG", 3},
			{"IFA_UK_Com_Engineer", 3}
		};
	};
};
