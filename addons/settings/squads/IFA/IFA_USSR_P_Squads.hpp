
class IFA_USSR_P {

	staticCompositions[] = {
		"CUP",
		"IFA",
		"IFA_SUMMER",
		"USSR_ATCrate",
		"USSR_HMGS",
		"US_IFA_MORTAR",
		"USSR_SUMMER",
		//"USSR_CANNONS",
		//"USSR_CANNONS_SUMMER",
		//"USSR_AA"
	};

	class IFA_USSR_P_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_USSR_P_SquadLeader", 1},
			{"IFA_USSR_P_Medic", 2},
			{"IFA_USSR_P_Rifleman",-1},
			{"IFA_USSR_P_Rifleman_Semi", 3},
			{"IFA_USSR_P_LAT", 4},
			{"IFA_USSR_P_MG", 6},
			{"IFA_USSR_P_MGAssistant", 6},
            {"IFA_USSR_P_Grenade_Rifleman", 7},
			{"IFA_USSR_P_SMG", 8},
            {"IFA_USSR_Crewman",-1}
		};
	};
	class IFA_USSR_P_Sniper {
		displayName = "Sniper Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_USSR_P_Sniper", 1},
			{"IFA_USSR_P_Spotter", 1}
		};
	};
	class IFA_USSR_P_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_USSR_P_HAT", 1},
			{"IFA_USSR_P_HAT_1",1}
		};
	};
};
