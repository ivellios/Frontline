
class IFA_GER {

	staticCompositions[] = {
		// -- ww-2 baseobjects for everyone
		"CUP",

		"IFA",
		"IFA_SUMMER",
		//"IFA_WINTER",

		"GER_ATCrate",
		"GER_BUNKERS",
		"GER_HMGS",
		"GER_MORTAR",
		"GER_SUMMER",
		//"GER_WINTER",
		"GER_CANONS",
		"GER_CANONS_SUMMER",
		//"GER_CANONS_WINTER",
		"GER_AA"
	};

	class IFA_GER_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_GER_SquadLeader", 1},
			{"IFA_GER_Medic", 2},
			{"IFA_GER_Rifleman",-1},
			{"IFA_GER_Rifleman_Semi", 3},
			{"IFA_GER_LAT", 4},
			{"IFA_GER_MG", 6},
			{"IFA_GER_MGAssistant", 6},
            {"IFA_GER_Grenadier_Rifleman", 7},
            {"IFA_GER_Flamethrower", 8},
			{"IFA_GER_CorporalSMG", 8},
            {"IFA_GER_Crewman",-1}
		};
	};
/*
	class IFA_GER_MortarSmall {
		displayName = "Mortar Squad";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAtArray[] = {18};
		roles[] = {
			{"IFA_GER_SquadLeader", 1},
			{"IFA_GER_Medic", 2},
			{"IFA_GER_Rifleman",-1},
			{"IFA_GER_LAT", 4},
			{"IFA_GER_MG", 6},
			{"IFA_GER_MGAssistant", 6},
            {"IFA_GER_MortarTube", 7},
			{"IFA_GER_MortarAmmo", 7},
			{"IFA_GER_CorporalSMG", 8}
		};
	};
*/
	class IFA_GER_Sniper {
		displayName = "Sniper";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 1;
		scope = 2;
		maxSize = 1;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_GER_Sniper", 1}
		};
	};

	class IFA_GER_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 1;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_GER_HAT", 1},
			{"IFA_GER_HAT_Ammo",2}
		};
	};
	class IFA_GER_AB_Squad {
		displayName = "Fallschirmjager";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 5;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"IFA_GER_AB_SquadLeader", 1},
			{"IFA_GER_AB_Medic", 2},
			{"IFA_GER_AB_CorporalSMG", 3},
			{"IFA_GER_AB_Rifleman_Semi", 3},
			{"IFA_GER_AB_LAT", -1}
		};
	};
    class IFA_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"IFA_GER_Crewman", -1}
		};
	};
/*

	class IFA_GER_HMGSquad {
		displayName = "HMG Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
		side = 1;
		scope = 2;
		maxSize = 3;
		availableAt = -1;
		availableAtArray[] = {30};
		roles[] = {
			{"IFA_GER_MG", 1},
			{"IFA_GER_MGAssistant",2},
			{"IFA_GER_MGAmmo", 2}
		};
	};

	class IFA_StaticMGTeam: HMG_basesquad {
		side = 1;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "LIB_MG34_Lafette_Deployed";
		roles[] = {
			{"IFA_GER_HMGLeader", 1},
			{"IFA_GER_HMGAmmo", -1}
		};
	};
    class IFA_StaticMortarTeam: HMG_basesquad {
        displayName = "Mortar Team";
		side = 1;
        availableAtArray[] = {20};
        maxSize = 2;
		staticWeaponType = "LIB_GrWr34";
		roles[] = {
			{"IFA_GER_HMGLeader", 1},
			{"IFA_GER_HMGAmmo", -1}
		};
	};*/
};
