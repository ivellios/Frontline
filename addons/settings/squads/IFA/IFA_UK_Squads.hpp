
class IFA_UK {

	staticCompositions[] = {
		"CUP",

		"IFA",
		"IFA_SUMMER",

		"UK_ATCrate",
		"UK_HMGS",
		"US_IFA_MORTAR",
		"UK_SUMMER",
		//"UK_WINTER",
		"UK_CANNONS",
		"USSR_AA"
	};

	class IFA_UK_SquadSmall {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"IFA_UK_SquadLeader", 1},
			{"IFA_UK_Medic", 2},
			{"IFA_UK_Rifleman",-1},
			{"IFA_UK_Engineer", 4},
			{"IFA_UK_LAT", 4},
			{"IFA_UK_MG", 4},
			{"IFA_UK_MG_1", 6},
            {"IFA_UK_Grenade_Rifleman", 6},
			{"IFA_UK_Corporal", 7},
            {"IFA_UK_Flamethrower", 8},
            {"IFA_UK_Crewman",-1}
		};
	};
/*
	class IFA_UK_MortarSmall {
		displayName = "Mortar Squad";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAtArray[] = {18};
		roles[] = {
			{"IFA_UK_SquadLeader", 1},
			{"IFA_UK_Medic", 2},
			{"IFA_UK_Rifleman",-1},
			{"IFA_UK_MG", 4},
            {"IFA_UK_MortarTube", 6},
			{"IFA_UK_MortarAmmo", 6}
		};
	};
*/
	class IFA_UK_Sniper {
		displayName = "Sniper Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_snpr.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"IFA_UK_Sniper", 1},
			{"IFA_UK_Spotter", 1}
		};
	};

	class IFA_UK_HATSquad {
		displayName = "AT Team";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk_rl.paa";
		side = 0;
		scope = 2;
		maxSize = 2;
		availableAt = -1;
		availableAtArray[] = {10};
		roles[] = {
			{"IFA_UK_HAT", 1},
			{"IFA_UK_HAT_Ammo",2}
		};
	};

	class IFA_UK_Commandos {
		displayName = "Commandos";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 5;
		availableAt = -1;
		availableAtArray[] = {15};
		roles[] = {
			{"IFA_UK_Com_SquadLeader", 1},
			{"IFA_UK_Com_Medic", 2},
			{"IFA_UK_Com_Corporal", 3},
			{"IFA_UK_Com_MG", 3},
			{"IFA_UK_Com_Engineer", 3}
		};
	};

    class IFA_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"IFA_UK_Crewman", -1}
		};
	};
};
