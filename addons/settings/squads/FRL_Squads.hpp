
#include "basesquads.hpp"

#include "Apex\A3_CSAT.hpp"
#include "Apex\A3_NATO.hpp"
#include "Apex\A3_GREF.hpp"
#include "Apex\A3_GUER.hpp"
#include "Apex\A3_PLA.hpp"
#include "Apex\APEX_CHN_squads.hpp"
#include "Apex\APEX_NATO_squads.hpp"


#include "IFA\IFA_USA_Squads.hpp"
#include "IFA\IFA_USA_P_Squads.hpp"
#include "IFA\IFA_USA_AB_Squads.hpp"
#include "IFA\IFA_USSR_Squads.hpp"
#include "IFA\IFA_USSR_W_Squads.hpp"
#include "IFA\IFA_USSR_P_Squads.hpp"
#include "IFA\IFA_GER_Squads.hpp"
#include "IFA\IFA_GER_W_Squads.hpp"
#include "IFA\IFA_GER_DAK_Squads.hpp"
#include "IFA\IFA_GER_AB_Squads.hpp"
#include "IFA\IFA_JP_Squads.hpp"
#include "IFA\IFA_UK_Squads.hpp"
#include "IFA\IFA_UK_AB_Squads.hpp"
#include "IFA\IFA_UK_D_Squads.hpp"
#include "IFA\IFA_UK_Com_Squads.hpp"
#include "IFA\IFA_GREE_Squads.hpp"



#include "RHS\RHS_RUA_W.hpp"
#include "RHS\RHS_USA_W.hpp"
#include "RHS\RHS_USMC_W.hpp"
#include "RHS\RHS_INS_W.hpp"
#include "RHS\RHS_INS_D.hpp"
#include "RHS\RHS_Iraq.hpp"
#include "RHS\RHS_RUA_W_Blu.hpp"
#include "RHS\RHS_UN_D.hpp"
#include "RHS\RHS_AFP.hpp"

#include "UNS\UNS_USA.hpp"
#include "UNS\UNS_VC.hpp"
