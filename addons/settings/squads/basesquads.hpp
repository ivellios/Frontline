class Crewman_basesquad {
  displayName = "Crewman";
  side = 0;
  scope = 2;
  mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_armd.paa";
  availableAt = 10;
  maxSize = 4;
  roles[] = {
    {"RHS_RUA_D_Crewman", -1}
  };
};

class HMG_basesquad {
  displayName = "HMG Team";
  side = 0;
  scope = 2;
  mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_hmg.paa";
  availableAtArray[] = {15};
  maxSize = 3;
  staticWeaponType = "RHS_M2StaticMG_MiniTripod_WD";
  roles[] = {
    {"RHS_USA_W_HMGLeader", 1},
    {"RHS_USA_W_HMGAmmo", -1}
  };
};
