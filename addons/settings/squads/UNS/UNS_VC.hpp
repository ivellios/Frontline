
class UNS_VC {
	staticCompositions[] = {
		/* Ehrm we dont mix RHS and UNS
		"RHS",

		"HESCOS",
		"RU_MORTAR",
		"RU_HMGS",
		"RU_ATGMS",
		"RU_MANPADS"
		*/
		"UNSUNG",
		"NVA",
		"NVA_AA",
		"NVA_MORTAR",
		"NVA_HAT"
	};

	class UNS_VC_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 0;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"UNS_VC_SquadLeader", 1},
			{"UNS_VC_Rifleman",-1},
			{"UNS_VC_Medic", 2},
			//{"UNS_VC_LAT", 3},
			{"UNS_VC_AR", 4},
			{"UNS_VC_HAT", 4},
			{"UNS_VC_Engineer", 3},
			{"UNS_VC_MG", 5},
			{"UNS_VC_Grenadier", 5},
			{"UNS_VC_AA", 3},
			{"UNS_VC_Crewman", -1}
		};
	};

	class UNS_VC_Squad4: UNS_VC_Squad1 {
		displayName = "NVA";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {15};
		roles[] = {
			{"UNS_VC_NVA_TeamLeader", 1},
			{"UNS_VC_NVA_Rifleman",-1},
			{"UNS_VC_NVA_HAT", 3},
			{"UNS_VC_NVA_Engineer", 3}
		};
	};
    
/*
	class UNS_VC_Squad2: UNS_VC_Squad1 {
		displayName = "Regular (Marksman)";
		availableAt = -1;
		availableAtArray[] = {25};
		maxSize = 6;
		roles[] = {
			{"UNS_VC_SquadLeader", 1},
			{"UNS_VC_Rifleman",-1},
			{"UNS_VC_Medic", 2},
			//{"UNS_VC_LAT", 3},
			{"UNS_VC_AR", 4},
			{"UNS_VC_HAT", 4},
			{"UNS_VC_Engineer", 4},
			{"UNS_VC_MG", 5},
			{"UNS_VC_Marksman", 5},
			{"UNS_VC_AA", 3},
			{"UNS_VC_Crewman", -1}
		};
	};
    
	class UNS_VC_SquadMG {
		displayName = "MG Team";
		side = 0;
		scope = 2;
		maxSize = 3;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {16,40};
		roles[] = {
			{"UNS_VC_TeamLeader_MG", 1},
			{"UNS_VC_MG", 2},
			{"UNS_VC_Rifleman", -1}
		};
	};

	class RHS_StaticMGTeam: HMG_basesquad {
		side = 0;
		availableAt = -1;
		availableAtArray[] = {30};
		staticWeaponType = "rhs_KORD_MSV";
		roles[] = {
			{"UNS_VC_HMGLeader", 1},
			{"UNS_VC_HMGAmmo", -1}
		};
	};
*/
	class UNS_VC_Sniper {
		displayName = "Sniper";
		side = 0;
		scope = 2;
		maxSize = 1;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"UNS_VC_Sniper", 1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 0;
		roles[] = {
			{"UNS_VC_Crewman", -1}
		};
	};
};
