
class UNS_USA {
	staticCompositions[] = {
		/* Ehrm we dont mix RHS and UNS
		"RHS",

		"HESCOS",

		"US_RHS_HMGS",
		"US_RHS_MORTAR",
		"US_ATGMS",
		"US_MANPADS"
		*/

		"UNSUNG",
		"US_UNS_HAT",
		"US_UNS_MORTAR",
		"US_UNS"
	};

	class UNS_USA_Squad1 {
		displayName = "Infantry";
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf.paa";
		side = 1;
		scope = 2;
		maxSize = 8;
		availableAt = -1;
        aiSquadType = 1;
		roles[] = {
			{"UNS_USA_SquadLeader", 1},
			{"UNS_USA_Rifleman",-1},
			{"UNS_USA_Medic", 2},
			{"UNS_USA_LAT", 3},
			{"UNS_USA_AR", 4},
			//{"UNS_USA_HAT", 4},
			{"UNS_USA_Engineer", 3},
			{"UNS_USA_MG", 6},
			{"UNS_USA_Grenadier", 6},
			//{"UNS_USA_AA", 3},
			{"UNS_USA_Crewman", -1}
		};
	};
/*
	class UNS_USA_Squad2: UNS_USA_Squad1 {
		displayName = "Regular (Marksman)";
		availableAt = -1;
		availableAtArray[] = {25};
		maxSize = 6;
		roles[] = {
			{"UNS_USA_SquadLeader", 1},
			{"UNS_USA_Rifleman",-1},
			{"UNS_USA_Medic", 2},
			//{"UNS_USA_LAT", 3},
			{"UNS_USA_AR", 4},
			//{"UNS_USA_HAT", 4},
			{"UNS_USA_Engineer", 4},
			{"UNS_USA_MG", 5},
			{"UNS_USA_Marksman", 5},
			//{"UNS_USA_AA", 3},
			{"UNS_USA_Crewman", -1}
		};
	};
*/
	class UNS_USA_Squad4: UNS_USA_Squad1 {
		displayName = "Recon";
		availableAt = -1;
		maxSize = 4;
		availableAtArray[] = {15};
		roles[] = {
			{"UNS_USA_RECON_TeamLeader", 1},
			{"UNS_USA_RECON_Rifleman",-1},
			{"UNS_USA_RECON_AR", 3},
			{"UNS_USA_RECON_Engineer", 3}
		};
	};

	class UNS_USA_Sniper {
		displayName = "Sniper Team";
		side = 1;
		scope = 2;
		maxSize = 2;
		mapIcon = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_mg.paa";
		availableAt = -1;
		availableAtArray[] = {12};
		roles[] = {
			{"UNS_USA_Sniper", 1},
			{"UNS_USA_Spotter", -1}
		};
	};

	class RHS_Crewman: Crewman_basesquad {
		side = 1;
		roles[] = {
			{"UNS_USA_Crewman", -1}
		};
	};
};
