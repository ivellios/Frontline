#include "macros.hpp"

class CfgPatches {
	class ADDON {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};

		requiredAddons[] = {"FRL_Main"};
	};
};

class FRL {
    #include "cfgNametags.hpp"
    #include "cfgDynamicFrontline.hpp"
    #include "cfgRespawn.hpp"
    #include "cfgSpawnpoints.hpp"
    #include "cfgVoting.hpp"
    #include "cfgMedical.hpp"
    #include "cfgMissionDuration.hpp"
    #include "cfgRoles.hpp"
    #include "cfgVehicles.hpp"
    #include "cfgTeamBalance.hpp"
    #include "cfgStatics.hpp"
    #include "cfgSquads.hpp"
    #include "cfgTickets.hpp"
    #include "cfgCommander.hpp"
    #include "CfgSearchDestroy.hpp"
    #include "factions\CfgFactions.hpp"
};
