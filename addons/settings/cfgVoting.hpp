class cfgVoting {
	class switchMission {
		scope = 2;
		showToUser = 1;
		condition = "true";
		requiredRatio = 0.65;
		displayName = "Switch Mission";
		voteYesText = "The current mission will end in 10s.";
		voteNoText = "The current mission will continue.";
		voteNoCode = "true";
		questionText = "Do you want to end the current mission?";
		voteTarget = "all";
		voteYesCode = "[_this, 'Vote to end mission was passed', 'gamemode'] call FRL_voting_fnc_endMissionVote";
	};

	class pauseBattleprep: switchMission {
		scope = 0;
		condition = "true";
		displayName = "Pause battleprep";
		voteYesText = "Battleprep will be paused.";
		voteNoText = "Battleprep won't be paused.";
		questionText = "Do you want to pause battleprep?";
		voteYesCode = "[true] call FRL_prep_fnc_setPausedPrep;";
	};

	class continueBattleprep: pauseBattleprep {
		condition = "false";
		displayName = "Continue battleprep";
		voteYesText = "Battleprep will be continued.";
		voteNoText = "Battleprep won't be continued.";
		questionText = "Do you want to resume battleprep?";
		voteYesCode = "[false] call FRL_prep_fnc_setPausedPrep;";
	};

	class extendTime: switchMission {
		condition = "([false] call FRL_duration_fnc_getMissionDuration) >= 90;";
		displayName = "Extend duration (30m)";
		voteYesText = "The mission duration will be extended 30 minutes.";
		voteYesCode = "[(30*60), true] call frl_duration_fnc_changeMissionDuration;";
		voteNoText = "The mission duration will remain the same.";
		questionText = "Do you want to extend mission duration 30 minutes?";
	};

	class reduceTime: extendTime {
		condition = "([true] call FRL_duration_fnc_getMissionDuration) >= 1890;";
		displayName = "Reduce duration (30m)";
		voteYesText = "The mission duration will be reduced 30 minutes.";
		voteYesCode = "[-(30*60), true] call frl_duration_fnc_changeMissionDuration;";
		questionText = "Do you want to reduce mission duration 30 minutes?";
	};

	class raiseTickets: switchMission {
		condition = "true";
		scope = 0;
		displayName = "Increase tickets (30)";
		voteYesText = "Tickets for both sides increased by 30.";
		voteYesCode = "[30] call FRL_voting_fnc_adjustTickets;";
		voteNoText = "Tickets will remain unchanged.";
		questionText = "Do you want to increase tickets by 30?";
	};

	class lowerTickets: raiseTickets {
		scope = 2;
		displayName = "Reduce tickets (30)";
		voteYesText = "Tickets for both sides lowered by 30.";
		voteYesCode = "[-30] call FRL_voting_fnc_adjustTickets;";
		questionText = "Do you want to lower tickets by 30?";
	};

	class surrenderVote: switchMission {
		scope = 2;
		voteTarget = "callerside";
		displayName = "Surrender";
		questionText = "Do you want to surrender and end the current mission?";
		voteYesCode = "[_this, 'Vote to surrender was passed', 'callerside'] call FRL_voting_fnc_endMissionVote";
	};

};
