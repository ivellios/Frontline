#define DEFAULTBKP  "B_Kitbag_cbr"
#define RDS  		"optic_Holosight_khk_F"
#define SCOPE		"optic_Arco_AK_lush_F"
#define RDSAPEX 	"optic_Holosight_arid_F"
#define SCOPEAPEX	"optic_Arco_AK_arid_F"


class A3_GUER_Default {
	scope = 0;
	ROLE_RIFLEMAN


	class Clothing {
		uniform   = "U_BG_Guerrilla_6_1";
		headgear  = "H_Bandanna_khk";
		goggles   = "";
		vest      = "V_PlateCarrier1_rgr_noflag_FRL";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_TRG21_F";
				rail        = "";
				optics      = RDS;
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			class Pistol {
				weapon      = "hgun_ACPC2_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"9Rnd_45ACP_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Apex";

			class Primary: Primary {
				weapon      = "arifle_AKM_F";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_762x39_Mag_F", 9}};
			};
		};

	};
};

class A3_GUER_Rifleman : A3_GUER_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {

			class Primary : Primary {
				optics      = RDS;
			};

			items[]       = {COMMON_RIFLEMAN_CQB};
		};
		class Variant2 : Variant1 {
			displayName = "APEX AK12";
			class Primary : Primary {
				weapon      = "arifle_AK12_arid_F";
				optics      = RDSAPEX;
				magazines[] = {{"30rnd_762x39_AK12_Arid_Mag_F", 9}};
			};
			items[] = {COMMON_RIFLEMAN_CQB};
		};

		class Variant3 : Variant1 {
			displayName = "APEX AKM";
			class Primary : Primary {
				weapon      = "arifle_AKM_F";
				optics      = "";
				magazines[] = {{"30Rnd_762x39_Mag_F", 9}};
			};
			items[] = {COMMON_RIFLEMAN_CQB};
		};
	};
};

class A3_GUER_SquadLeader : A3_GUER_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Clothing: Clothing {
		uniform = "U_I_G_resistanceLeader_F";
		headgear = "H_Beret_blk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary: Primary {
				optics      = SCOPE;
			};
			items[]       = {COMMON_TEAMLEADER};
		};
		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				optics      = RDS;
			};
			items[] = {COMMON_TEAMLEADER_CQB};
		};
		class Variant3 : Variant1 {
			displayName = "APEX";
			class Primary: Primary {
				weapon      = "arifle_AK12_arid_F";
				optics      = SCOPEAPEX;
				magazines[] = {{"30rnd_762x39_AK12_Arid_Mag_F", 9}};
			};
		};
		class Variant4 : Variant3 {
			displayName = "CQB APEX";
			class Primary : Primary {
				optics      = RDSAPEX;
			};
			items[] = {COMMON_TEAMLEADER_CQB};
		};
	};
};

class A3_GUER_Spotter : A3_GUER_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_O_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_khk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class A3_GUER_Medic : A3_GUER_Default {
	scope = 2;
	ROLE_MEDIC

	class Clothing: Clothing {
		uniform = "U_BG_Guerilla3_1";
		headgear = "H_Booniehat_khk_hs";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_medic_f0";
			class Primary: Primary {
				weapon      = "arifle_TRG20_F";
				magazines[] = {{"30Rnd_556x45_Stanag", 6}};
			};
			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {COMMON_MEDIC};
			};
		};
		class Variant2 : Variant1 {
			class Primary: Primary {
				weapon      = "arifle_AKS_F";
				optics      = "";
				magazines[] = {{"30Rnd_545x39_Mag_F", 6}};
			};
		};
	};
};

class A3_GUER_AR : A3_GUER_Default {
	scope = 2;
	ROLE_AR
	class Clothing: Clothing {
		uniform = "U_I_G_resistanceLeader_F";
		headgear = "H_Cap_headphones";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Zafir";

			class Primary : Primary {
				weapon      = "LMG_Zafir_F";
				magazines[] = {{"150Rnd_762x54_Box", 3}};
				optics      = "optic_MRCO";
				rail 		= "";
			};
			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"150Rnd_762x54_Box", 2}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "APEX";

			class Primary : Primary {
				weapon      = "LMG_03_F";
				magazines[] = {{"200Rnd_556x45_Box_F", 3}};
				optics      = "optic_Holosight_blk_F";
				rail 		= "";
			};
			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"200Rnd_556x45_Box_F", 2}};
			};
		};
	};
};

class A3_GUER_MG : A3_GUER_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "MMG";

			class Primary : Primary {
				weapon      = "MMG_01_hex_F";
				magazines[] = {{"150Rnd_93x64_Mag", 1, 1}};
				optics      = "optic_MRCO";
				bipod       = "bipod_02_F_hex";
				rail        = "";
			};
			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"150Rnd_93x64_Mag", 2}};
			};
		};
		class Variant2 : Variant2 {
			displayName = "Zafir";

			class Primary : Primary {
				weapon      = "LMG_Zafir_F";
				magazines[] = {{"150Rnd_762x54_Box", 3}};
				rail        = "";
				optics      = "optic_MRCO";
			};
			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"150Rnd_762x54_Box", 2}};
			};
		};
	};
};

class A3_GUER_Grenadier : A3_GUER_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {

		class Variant1 : Variant1 {
			displayName = "str_b_soldier_gl_f0";

			class Primary: Primary {
				weapon      = "arifle_TRG21_GL_F";
				optics 		= RDS;
				magazines[] = {{"30Rnd_556x45_Stanag", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};

		class Variant2 : Variant1 {
			displayName = "APEX";

			class Primary: Primary {
				weapon      = "arifle_AK12_GL_arid_F";
				optics 		= RDSAPEX;
				magazines[] = {{"30rnd_762x39_AK12_Arid_Mag_F", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};
		};

	};
};

class A3_GUER_LAT : A3_GUER_Default {
	scope = 2;
	ROLE_LAT

	class Clothing: Clothing {
		uniform = "U_C_HunterBody_grn";
		headgear = "H_Shemag_olive_hs";
	};


	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_lat_f0";

			class Primary : Primary {
				weapon      = "arifle_TRG20_F";
				optics      = RDS;
				magazines[] = {{"30Rnd_556x45_Stanag", 6}};
			};

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"RPG32_F", 1}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "RPG-32 (APEX)";
			class Primary: Primary {
				weapon      = "arifle_AKS_F";
				optics      = "";
				magazines[] = {{"30Rnd_545x39_Mag_F", 6}};
			};
			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};
		};

		class Variant3 : Variant1 {
			displayName = "RPG7 (APEX)";

			class Primary: Primary {
				weapon      = "arifle_AKS_F";
				optics      = "";
				magazines[] = {{"30Rnd_545x39_Mag_F", 6}};
			};

			class Secondary {
				weapon      = "launch_RPG7_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG7_F", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"RPG7_F", 2}};
			};
		};
	};
};



class A3_GUER_HAT : A3_GUER_Default {
	scope = 2;
	ROLE_HAT

	class Clothing: Clothing {
		uniform = "U_BG_Guerilla1_1";
		headgear = "H_Shemag_olive_hs";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Primary : Primary {
				weapon      = "arifle_TRG20_F";
				optics      = RDS;
				magazines[] = {{"30Rnd_556x45_Stanag", 6}};
			};

			class Secondary {
				weapon      = "launch_O_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"Titan_AT", 1}};
			};
		};

		class Variant2 : Variant1 {
			displayName = "RPG-32";

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};

		class Variant3 : Variant1 {
			displayName = "RPG7 (APEX)";

			class Primary: Primary {
				weapon      = "arifle_AKS_F";
				optics      = "";
				magazines[] = {{"30Rnd_545x39_Mag_F", 6}};
			};

			class Secondary {
				weapon      = "launch_RPG7_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG7_F", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"RPG7_F", 2}};
			};
		};

	};
};

class A3_GUER_AA : A3_GUER_Default {
	scope = 2;
	ROLE_AA

	class Clothing: Clothing {
		uniform = "U_BG_Guerilla1_1";
		headgear = "H_Shemag_olive_hs";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_aa_f0";

			class Primary : Primary {
				weapon      = "arifle_TRG20_F";
				optics      = RDS;
				magazines[] = {{"30Rnd_556x45_Stanag", 6}};
			};

			class Secondary {
				weapon      = "launch_O_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack {
				backpack = DEFAULTBKP;
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_GUER_Engineer: A3_GUER_Default {
	scope = 2;
	ROLE_ENGINEER

	class Clothing: Clothing {
		uniform = "U_BG_Guerilla3_1";
		headgear = "H_Watchcap_khk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Demolition Specialist";

			class Primary : Primary {
				weapon      = "arifle_TRG20_F";
				optics      = RDS;
				magazines[] = {{"30Rnd_556x45_Stanag", 6}};
			};

			class Backpack {
				backpack = DEFAULTBKP;
				content[]   = {{"FRL_ExplosiveCharge_Wpn", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Demolition Specialist (APEX)";
			class Primary: Primary {
				weapon      = "arifle_AKS_F";
				optics      = "";
				magazines[] = {{"30Rnd_545x39_Mag_F", 6}};
			};
		};
	};
};

class A3_GUER_Marksman : A3_GUER_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "srifle_DMR_01_F";
				optics      = "optic_DMS";
				rail        = "";
				bipod       = "bipod_02_F_hex";
				magazines[] = {{"10Rnd_762x54_Mag", 9}};
			};
		};
	};
};

class A3_GUER_Crewman : A3_GUER_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing: Clothing {
		headgear = "";
		vest      = "V_HarnessO_brn";
	};
    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary: Primary {
				weapon      = "arifle_TRG20_F";
			    magazines[] = {{"30Rnd_556x45_Stanag", 6}};
			};
			items[]       = {COMMON_TEAMLEADER};
		};
	};
};

class A3_GUER_Sniper : A3_GUER_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_camo_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};
