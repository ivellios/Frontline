// NATO APEX

class APEX_NATO_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "U_B_T_Soldier_F";
		headgear  = "H_HelmetB_tna_F";
		goggles   = "";
		vest      = "V_PlateCarrier2_tna_F";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_SPAR_01_khk_F";
				rail        = "acc_flashlight";
				optics      = "optic_Holosight_khk_F";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_khk_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_AssaultPack_khk";
			};
		};
	};
};

class APEX_NATO_Rifleman : APEX_NATO_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {

			class Primary : Primary {
				optics      = "optic_MRCO";
			};

			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
        class Variant2: Variant1 {
            displayName = "Alternative kit";
            class Backpack {
                backpack = "B_AssaultPack_dgtl";
            };
        };
        class Variant3: Variant1 {
            displayName = "Alternative kit 2";
            class Backpack {
                backpack = "B_AssaultPack_rgr";
            };
        };
	};
};

class APEX_NATO_SquadLeader : APEX_NATO_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr_khk_F";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class APEX_NATO_TeamLeader_MG : APEX_NATO_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"150Rnd_556x45_Drum_Mag_F", 5}};
			};
		};
	};
};

class APEX_NATO_TeamLeader_AA : APEX_NATO_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class APEX_NATO_TeamLeader_HAT : APEX_NATO_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class APEX_NATO_Spotter : APEX_NATO_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_B_T_Sniper_F";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_oli";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class APEX_NATO_Medic : APEX_NATO_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[] = {COMMON_NVG_VANILLA};
		};
	};
};

class APEX_NATO_AR : APEX_NATO_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "arifle_SPAR_02_khk_F";
				bipod       = "bipod_01_F_khk";
				magazines[] = {{"150Rnd_556x45_Drum_Mag_F", 5}};
			};
		};
	};
};

class APEX_NATO_MG : APEX_NATO_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "arifle_SPAR_02_khk_F";
				bipod       = "bipod_01_F_khk";
				magazines[] = {{"150Rnd_556x45_Drum_Mag_F", 5}};
			};
		};
	};
};

class APEX_NATO_Grenadier : APEX_NATO_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_SPAR_01_GL_khk_F";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class APEX_NATO_LAT : APEX_NATO_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class APEX_NATO_HAT : APEX_NATO_Default {
	scope = 0;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_B_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}};
			};
		};
	};
};

class APEX_NATO_AA : APEX_NATO_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_B_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class APEX_NATO_Marksman : APEX_NATO_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "arifle_SPAR_03_khk_F";
				optics      = "optic_DMS";
				bipod       = "bipod_01_F_khk";
				magazines[] = {{"20Rnd_762x51_Mag", 9}};
			};
		};
	};
};

class APEX_NATO_Engineer : APEX_NATO_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class APEX_NATO_Sniper : APEX_NATO_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";

			class Primary {
				weapon      = "srifle_LRR_tna_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS_tna_F";
				bipod       = "";
				magazines[] = {{"7Rnd_408_Mag", 10}};
			};
		};
	};
};

class APEX_NATO_RECON_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

	class Clothing {
		uniform   = "U_B_T_Soldier_AR_F";
		headgear  = "H_Booniehat_tna_F";
		goggles   = "";
		vest      = "V_PlateCarrier1_tna_F";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_SPAR_01_khk_F";
				muzzle      = "muzzle_snds_m_khk_F";
				rail        = "acc_flashlight";
				optics      = "optic_ERCO_khk_F";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_khk_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_AssaultPack_khk";
			};
		};
	};
};

class APEX_NATO_RECON_Rifleman : APEX_NATO_Recon_Default {
	scope = 2;

	class Variants {
		class Variant1 {
			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
	};
};

class APEX_NATO_RECON_TeamLeader : APEX_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class APEX_NATO_RECON_Medic : APEX_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack{
				content[] = {COMMON_MEDIC_NVG_VANILLA};
			};
		};
	};
};

class APEX_NATO_RECON_Engineer : APEX_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class APEX_NATO_RECON_LAT : APEX_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};


class APEX_NATO_Crewman : APEX_NATO_Default {
	ROLE_CREWMAN
	scope = 2;
	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "SMG_05_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_9x21_Mag_SMG_02", 2}};
			};
			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class APEX_NATO_HMGLeader : APEX_NATO_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class APEX_NATO_HMGAmmo : APEX_NATO_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};
