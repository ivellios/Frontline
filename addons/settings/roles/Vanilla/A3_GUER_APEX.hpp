// GUER APEX

class G_A3_GUER_APEX_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "U_I_C_Soldier_Camo_F";
		headgear  = "H_Shemag_olive";
		goggles   = "";
		vest      = "V_PlateCarrier1_rgr_noflag_F";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_AK12_F";
				rail        = "acc_flashlight";
				optics      = "optic_ACO_grn";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_762x39_Mag_F", 9}};
			};

			class Pistol {
				weapon      = "hgun_Pistol_heavy_02_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"6Rnd_45ACP_Cylinder", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_FieldPack_khk";
			};
		};
	};
};

class G_A3_GUER_APEX_Rifleman : G_A3_GUER_APEX_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {

			class Primary : Primary {
				optics      = "optic_MRCO";
			};

			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
	};
};

class G_A3_GUER_APEX_SquadLeader : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class G_A3_GUER_APEX_TeamLeader_MG : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"200Rnd_556x45_Box_F", 5}};
			};
		};
	};
};

class G_A3_GUER_APEX_TeamLeader_AA : G_A3_GUER_APEX_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class G_A3_GUER_APEX_TeamLeader_HAT : G_A3_GUER_APEX_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class G_A3_GUER_APEX_Spotter : G_A3_GUER_APEX_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_I_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_khk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class G_A3_GUER_APEX_Medic : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[] = {COMMON_NVG_VANILLA};
		};
	};
};

class G_A3_GUER_APEX_AR : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "LMG_03_F";
				magazines[] = {{"200Rnd_556x45_Box_F", 5}};
			};
		};
	};
};

class G_A3_GUER_APEX_MG : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "LMG_03_F";
				magazines[] = {{"200Rnd_556x45_Box_F", 5}};
			};
		};
	};
};

class G_A3_GUER_APEX_Grenadier : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_AK12_GL_F";
				magazines[] = {{"30Rnd_762x39_Mag_F", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class G_A3_GUER_APEX_LAT : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_RPG7_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG7_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG7_F", 1}};
			};
		};
	};
};

class G_A3_GUER_APEX_HAT : G_A3_GUER_APEX_Default {
	scope = 0;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_O_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}};
			};
		};
	};
};

class G_A3_GUER_APEX_AA : G_A3_GUER_APEX_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_O_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class G_A3_GUER_APEX_Marksman : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				optics      = "optic_DMS";
				bipod       = "bipod_03_F_blk";
			};
		};
	};
};

class G_A3_GUER_APEX_Engineer : G_A3_GUER_APEX_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class G_A3_GUER_APEX_Sniper : G_A3_GUER_APEX_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};

class G_A3_GUER_APEX_RECON_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

	class Clothing {
		uniform   = "U_I_C_Soldier_Para_1_F";
		headgear  = "H_Shemag_olive";
		goggles   = "";
		vest      = "V_PlateCarrier2_rgr_noflag_F";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_AKM_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_762x39_Mag_F", 9}};
			};

			class Pistol {
				weapon      = "hgun_Pistol_heavy_02_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"6Rnd_45ACP_Cylinder", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_FieldPack_khk";
			};
		};
	};
};

class G_A3_GUER_APEX_RECON_Rifleman : G_A3_GUER_APEX_Recon_Default {
	scope = 2;

	class Variants {
		class Variant1 {
			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
	};
};

class G_A3_GUER_APEX_RECON_TeamLeader : G_A3_GUER_APEX_Recon_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class G_A3_GUER_APEX_RECON_Medic : G_A3_GUER_APEX_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC_NVG_VANILLA};
			};
		};
	};
};

class G_A3_GUER_APEX_RECON_Engineer : G_A3_GUER_APEX_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class G_A3_GUER_APEX_RECON_LAT : G_A3_GUER_APEX_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_RPG7_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG7_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG7_F", 1}};
			};
		};
	};
};
