// GREF

class A3_GREF_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "U_I_CombatUniform";
		headgear  = "H_HelmetIA";
		goggles   = "";
		vest      = "V_FRL_PlateCarrierIA1_dgtl";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_Mk20_F";
				rail        = "acc_flashlight";
				optics      = "optic_ACO_grn";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			class Pistol {
				weapon      = "hgun_ACPC2_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"9Rnd_45ACP_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2: Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack = "B_AssaultPack_dgtl";
			};
		};
	};
};

class A3_GREF_Rifleman: A3_GREF_Default {
	scope = 2;

	class Variants: Variants {
		class Variant1 :Variant1 {

			class Primary: Primary {
				optics      = "optic_MRCO";
			};

			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
		class Variant2: Variant1 {
			displayName = "CQB";
			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};
			items[] = {COMMON_RIFLEMAN_CQB_NVG_VANILLA};
		};
	};
};

class A3_GREF_SquadLeader: A3_GREF_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants: Variants {
		class Variant1: Variant1 {
			displayName = "Standard";

			class Primary: Primary {
				optics = "optic_MRCO";
			};

			items[] = {COMMON_TEAMLEADER_NVG_VANILLA};
		};

		class Variant2: Variant1 {
			displayName = "CQB";
			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};
			items[] = {COMMON_TEAMLEADER_NVG_CQB_VANILLA};
		};
	};
};



class A3_GREF_TeamLeader_MG: A3_GREF_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "AR Support";

			class Backpack: Backpack {
				content[] = {{"150Rnd_762x54_Box", 5}};
			};
		};
	};
};

class A3_GREF_TeamLeader_AA: A3_GREF_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "AA Support";

			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};

			class Backpack: Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_GREF_TeamLeader_HAT: A3_GREF_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "HAT Support";

			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};

			class Backpack: Backpack {
				content[] = {{"Titan_AT", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class A3_GREF_Spotter: A3_GREF_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_I_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_oli";
	};

	class Variants: Variants {
		class Variant1: Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class A3_GREF_Medic: A3_GREF_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack: Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[] = {COMMON_NVG_VANILLA};
		};
	};
};

class A3_GREF_AR: A3_GREF_Default {
	scope = 2;
	ROLE_AR

	class Variants: Variants {
		class Variant1: Variant1 {
			displayName = "Mk200";

			class Primary: Primary {
				weapon      = "LMG_Mk200_F";
				magazines[] = {{"200Rnd_65x39_cased_Box", 5}};
				optics      = "optic_MRCO";
			};
		};
		class Variant2: Variant1 {
			displayName = "Mk200 CQB";

			class Primary: Primary {
				weapon      = "LMG_Mk200_F";
				magazines[] = {{"200Rnd_65x39_cased_Box", 5}};
				optics      = "optic_ACO_grn";
			};
		};
	};
};

class A3_GREF_MG: A3_GREF_Default {
	scope = 2;
	ROLE_MG

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "MMG";

			class Primary: Primary {
				weapon      = "MMG_02_black_F";
				magazines[] = {{"130Rnd_338_Mag", 1, 1}};
				optics      = "optic_MRCO";
				bipod       = "bipod_03_F_blk";
				rail        = "";
			};
			class Backpack: Backpack {
				content[] = {{"130Rnd_338_Mag", 2}};
			};
		};
		class Variant2: Variant2 {
			displayName = "Mk200";

			class Primary: Primary {
				weapon      = "LMG_Mk200_F";
				magazines[] = {{"200Rnd_65x39_cased_Box", 5}};
				optics      = "optic_MRCO";
			};
		};
	};
};

class A3_GREF_Grenadier: A3_GREF_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary: Primary {
				weapon      = "arifle_Mk20_GL_F";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack: Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class A3_GREF_LAT: A3_GREF_Default {
	scope = 2;
	ROLE_LAT

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class A3_GREF_HAT: A3_GREF_Default {
	scope = 2;
	ROLE_HAT

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_I_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"Titan_AT", 1}};
			};
		};
		class Variant2: Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class A3_GREF_AA: A3_GREF_Default {
	scope = 2;
	ROLE_AA

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_I_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_GREF_Marksman: A3_GREF_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants: Variants {
		class Variant1: Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary: Primary {
				weapon      = "srifle_DMR_01_F";
				optics      = "optic_DMS";
				rail        = "";
				bipod       = "bipod_02_F_hex";
				magazines[] = {{"10Rnd_762x54_Mag", 9}};
			};
		};
	};
};

class A3_GREF_Engineer: A3_GREF_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "Demolition Specialist";

			class Primary: Primary {
				weapon      = "arifle_Mk20C_F";
				optics      = "optic_ACO_grn";
			};

			class Backpack: Backpack {
				content[]   = {{"FRL_ExplosiveCharge_Wpn", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class A3_GREF_Crewman: A3_GREF_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing: Clothing {
		headgear = "H_HelmetCrew_I";
		vest      = "V_Chestrig_oli";
	};
    class Variants: Variants {
		class Variant1: Variant1 {
			class Primary {
				weapon      = "arifle_Mk20C_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 6}};
			};
			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class A3_GREF_Sniper: A3_GREF_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants: Variants {
		class Variant1: Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_camo_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};

/*
too lazy to fix for the time being
class A3_GREF_RECON_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

	class Clothing {
		uniform   = "U_O_SpecopsUniform_ocamo";
		headgear  = "H_HelmetSpecO_ocamo";
		goggles   = "G_Bandanna_khk";
		vest      = "V_HarnessO_brn";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_Katiba_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_flashlight";
				optics      = "optic_MRCO";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}};
			};

			class Pistol {
				weapon      = "hgun_ACPC2_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"9Rnd_45ACP_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
	};
};

class A3_GREF_RECON_Rifleman: A3_GREF_Recon_Default {
	scope = 2;

	class Variants {
		class Variant1 {
			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
	};
};

class A3_GREF_RECON_TeamLeader: A3_GREF_Recon_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants: Variants {
		class Variant1: Variant1 {
			displayName = "Standard";

			class Primary: Primary {
				optics      = "optic_MRCO";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class A3_GREF_RECON_Medic: A3_GREF_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants: Variants {
		class Variant1: Variant1 {
			displayName = "str_b_recon_medic_f0";

			class Backpack: Backpack{
				content[] = {COMMON_MEDIC_NVG_VANILLA};
			};
		};
	};
};

class A3_GREF_RECON_Engineer: A3_GREF_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "Demolition Specialist";

			class Backpack: Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class A3_GREF_RECON_LAT: A3_GREF_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants: Variants {
		class Variant1: Variant2 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack: Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};
*/

/*

class A3_GREF_HMGLeader: A3_GREF_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants: Variants {
		class Variant1: Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class A3_GREF_HMGAmmo: A3_GREF_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants: Variants {
		class Variant1: Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};

*/
