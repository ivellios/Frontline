// CSAT

class A3_CSAT_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "U_O_CombatUniform_ocamo";
		headgear  = "H_HelmetO_ocamo";
		goggles   = "";
		vest      = "V_FRL_TacVest_khk";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_Katiba_F";
				rail        = "acc_flashlight";
				optics      = "optic_ACO_grn";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}};
			};

			class Pistol {
				weapon      = "hgun_Rook40_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack = "B_FieldPack_ocamo";
			};
		};
	};
};

class A3_CSAT_Rifleman : A3_CSAT_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {

			class Primary : Primary {
				optics      = "optic_MRCO";
			};

			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};
			items[] = {COMMON_RIFLEMAN_CQB_NVG_VANILLA};
		};
	};
};

class A3_CSAT_SquadLeader : A3_CSAT_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};
			items[] = {COMMON_TEAMLEADER_NVG_CQB_VANILLA};
		};
	};
};

class A3_CSAT_TeamLeader_MG : A3_CSAT_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"150Rnd_762x54_Box", 5}};
			};
		};
	};
};

class A3_CSAT_TeamLeader_AA : A3_CSAT_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_CSAT_TeamLeader_HAT : A3_CSAT_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AT", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class A3_CSAT_Spotter : A3_CSAT_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_O_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_khk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class A3_CSAT_Medic : A3_CSAT_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[] = {COMMON_NVG_VANILLA};
		};
	};
};

class A3_CSAT_AR : A3_CSAT_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Zafir";

			class Primary : Primary {
				weapon      = "LMG_Zafir_F";
				magazines[] = {{"150Rnd_762x54_Box", 5}};
				optics      = "optic_MRCO";
			};
		};
		class Variant2 : Variant1 {
			displayName = "Mk200 CQB";

			class Primary : Primary {
				weapon      = "LMG_Mk200_F";
				magazines[] = {{"200Rnd_65x39_cased_Box", 5}};
				optics      = "optic_ACO_grn";
			};
		};
	};
};

class A3_CSAT_MG : A3_CSAT_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "MMG";

			class Primary : Primary {
				weapon      = "MMG_01_hex_F";
				magazines[] = {{"150Rnd_93x64_Mag", 1, 1}};
				optics      = "optic_MRCO";
				bipod       = "bipod_02_F_hex";
				rail        = "";
			};
			class Backpack : Backpack {
				content[] = {{"150Rnd_93x64_Mag", 2}};
			};
		};
		class Variant2 : Variant2 {
			displayName = "Zafir";

			class Primary : Primary {
				weapon      = "LMG_Zafir_F";
				magazines[] = {{"150Rnd_762x54_Box", 5}};
				rail        = "";
			};
		};
	};
};

class A3_CSAT_Grenadier : A3_CSAT_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_GL_F";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class A3_CSAT_LAT : A3_CSAT_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};
	};
};

class A3_CSAT_HAT : A3_CSAT_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_O_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AT", 1}};
			};
		};
		class Variant2 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};
	};
};

class A3_CSAT_AA : A3_CSAT_Default {
	scope = 2;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};

			class Secondary {
				weapon      = "launch_O_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_CSAT_Marksman : A3_CSAT_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "srifle_DMR_01_F";
				optics      = "optic_DMS";
				rail        = "";
				bipod       = "bipod_02_F_hex";
				magazines[] = {{"10Rnd_762x54_Mag", 9}};
			};
		};
	};
};

class A3_CSAT_Engineer : A3_CSAT_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "optic_ACO_grn";
			};

			class Backpack : Backpack {
				content[]   = {{"FRL_ExplosiveCharge_Wpn", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class A3_CSAT_Crewman : A3_CSAT_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing: Clothing {
		headgear = "H_HelmetCrew_O";
		vest      = "V_HarnessO_brn";
	};
    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 6}};
			};
			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class A3_CSAT_Sniper : A3_CSAT_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_camo_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};
/*
too lazy to fix for the time being
class A3_CSAT_RECON_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

	class Clothing {
		uniform   = "U_O_SpecopsUniform_ocamo";
		headgear  = "H_HelmetSpecO_ocamo";
		goggles   = "G_Bandanna_khk";
		vest      = "V_HarnessO_brn";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_Katiba_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_flashlight";
				optics      = "optic_MRCO";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}};
			};

			class Pistol {
				weapon      = "hgun_Rook40_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
	};
};

class A3_CSAT_RECON_Rifleman : A3_CSAT_Recon_Default {
	scope = 2;

	class Variants {
		class Variant1 {
			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
	};
};

class A3_CSAT_RECON_TeamLeader : A3_CSAT_Recon_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class A3_CSAT_RECON_Medic : A3_CSAT_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack{
				content[] = {COMMON_MEDIC_NVG_VANILLA};
			};
		};
	};
};

class A3_CSAT_RECON_Engineer : A3_CSAT_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class A3_CSAT_RECON_LAT : A3_CSAT_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};
	};
};
*/

class A3_CSAT_HMGLeader : A3_CSAT_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class A3_CSAT_HMGAmmo : A3_CSAT_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};
