// CSAT Apex

class APEX_CHN_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "O_CombatUniform_FRL_chn_07universal";
		headgear  = "H_HelmetIA_chn_07universal";
		goggles   = "";
		vest      = "V_TacVest_chn_07universal";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_CTAR_blk_F";
				rail        = "acc_flashlight";
				optics      = "optic_Aco";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_580x42_Mag_F", 9}};
			};

			class Pistol {
				weapon      = "hgun_Rook40_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_FieldPack_ghex_F";
			};
		};
	};
};

class APEX_CHN_Baserole {
	scope = 2;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "O_CombatUniform_FRL_chn_07universal";
		headgear  = "";
		goggles   = "";
		vest      = "V_TacVest_chn_07universal";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_CTAR_blk_F";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_580x42_Mag_F", 2}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
	};
};

class APEX_CHN_Rifleman : APEX_CHN_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {

			class Primary : Primary {
				optics      = "optic_MRCO";
			};

			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
        class Variant2: Variant1 {
            displayName = "Alternative kit";
            class Backpack {
                backpack = "B_FieldPack_khk";
            };
        };
        class Variant3: Variant1 {
            displayName = "Alternative kit 2";
            class Backpack {
                backpack = "B_FieldPack_cbr";
            };
        };
	};
};

class APEX_CHN_SquadLeader : APEX_CHN_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_ERCO_blk_F";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class APEX_CHN_TeamLeader_MG : APEX_CHN_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"100Rnd_580x42_Mag_F", 5}};
			};
		};
	};
};

class APEX_CHN_TeamLeader_AA : APEX_CHN_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class APEX_CHN_TeamLeader_HAT : APEX_CHN_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class APEX_CHN_Spotter : APEX_CHN_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_O_T_FullGhillie_tna_F";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_khk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class APEX_CHN_Medic : APEX_CHN_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[] = {COMMON_NVG_VANILLA};
		};
	};
};

class APEX_CHN_AR : APEX_CHN_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "arifle_CTARS_blk_F";
				magazines[] = {{"100Rnd_580x42_Mag_F", 5}};
			};
		};
	};
};

class APEX_CHN_MG : APEX_CHN_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "arifle_CTARS_blk_F";
				magazines[] = {{"100Rnd_580x42_Mag_F", 5}};
			};
		};
	};
};

class APEX_CHN_Grenadier : APEX_CHN_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_CTAR_GL_blk_F";
				magazines[] = {{"30Rnd_580x42_Mag_F", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class APEX_CHN_LAT : APEX_CHN_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_RPG32_ghex_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_HE_F", 1}};
			};
		};
	};
};

class APEX_CHN_HAT : APEX_CHN_Default {
	scope = 0;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_B_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}};
			};
		};
	};
};

class APEX_CHN_AA : APEX_CHN_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_B_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class APEX_CHN_Marksman : APEX_CHN_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "srifle_DMR_07_blk_F";
				optics      = "optic_DMS";
				bipod       = "bipod_02_F_hex";
				magazines[] = {{"20Rnd_650x39_Cased_Mag_F", 9}};
			};
		};
	};
};

class APEX_CHN_Engineer : APEX_CHN_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class APEX_CHN_Sniper : APEX_CHN_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_ghex_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_SOS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};

class APEX_CHN_RECON_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

	class Clothing {
		uniform   = "U_O_SpecopsUniform_ocamo";
		headgear  = "H_HelmetSpecO_ocamo";
		goggles   = "G_Bandanna_khk";
		vest      = "V_PlateCarrierIA1_dgtl";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_CTAR_blk_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_flashlight";
				optics      = "optic_ERCO_blk_F";
				bipod       = "";
				magazines[] = {{"30Rnd_580x42_Mag_F", 9}};
			};

			class Pistol {
				weapon      = "hgun_Rook40_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_FieldPack_ghex_F";
			};
		};
	};
};

class APEX_CHN_RECON_Rifleman : APEX_CHN_Recon_Default {
	scope = 2;

	class Variants {
		class Variant1 {
			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
	};
};

class APEX_CHN_RECON_TeamLeader : APEX_CHN_Recon_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_ERCO_blk_F";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class APEX_CHN_RECON_Medic : APEX_CHN_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack{
				content[] = {COMMON_MEDIC_NVG_VANILLA};
			};
		};
	};
};

class APEX_CHN_RECON_Engineer : APEX_CHN_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class APEX_CHN_RECON_LAT : APEX_CHN_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_RPG32_ghex_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_HE_F", 1}};
			};
		};
	};
};

class APEX_CHN_Crewman : APEX_CHN_Default {
	ROLE_CREWMAN
	scope = 2;
	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "SMG_01_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_45ACP_Mag_SMG_01", 2}};
			};
			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class APEX_CHN_HMGLeader : APEX_CHN_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class APEX_CHN_HMGAmmo : APEX_CHN_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};
