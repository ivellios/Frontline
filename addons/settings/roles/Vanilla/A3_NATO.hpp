// NATO

class A3_NATO_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "U_B_CombatUniform_mcam";
		headgear  = "H_HelmetB";
		goggles   = "";
		vest      = "V_FRL_PlateCarrier2_rgr";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_MX_F";
				rail        = "acc_flashlight";
				optics      = "optic_ACO";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_AssaultPack_mcamo";
			};
		};
	};
};

class A3_NATO_Rifleman : A3_NATO_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				optics      = "optic_MRCO";
			};

			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};
			items[] = {COMMON_RIFLEMAN_CQB_NVG_VANILLA};
		};
	};
};

class A3_NATO_SquadLeader : A3_NATO_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};
			items[] = {COMMON_TEAMLEADER_NVG_CQB_VANILLA};
		};
	};
};

class A3_NATO_TeamLeader_MG : A3_NATO_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"100Rnd_65x39_caseless_mag", 5}};
			};
		};
	};
};

class A3_NATO_TeamLeader_AA : A3_NATO_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_NATO_TeamLeader_HAT : A3_NATO_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AT", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class A3_NATO_Spotter : A3_NATO_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_B_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class A3_NATO_Medic : A3_NATO_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[] = {COMMON_NVG_VANILLA};
		};
	};
};

class A3_NATO_AR : A3_NATO_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "CQB";

			class Primary : Primary {
				weapon      = "arifle_MX_SW_F";
				bipod       = "bipod_01_F_snd";
				magazines[] = {{"100Rnd_65x39_caseless_mag", 5}};
			};
		};
		class Variant2 : Variant1 {
		displayName = "Long Range";

			class Primary : Primary {
				weapon      = "arifle_MX_SW_F";
				bipod       = "bipod_01_F_snd";
				optics      = "optic_MRCO";
				magazines[] = {{"100Rnd_65x39_caseless_mag", 5}};
			};
		};
	};
};

class A3_NATO_MG : A3_NATO_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "MMG";

			class Primary : Primary {
				weapon      = "MMG_02_camo_F";
				magazines[] = {{"130Rnd_338_Mag", 1, 1}};
				bipod       = "bipod_01_F_mtp";
				optics 		= "optic_MRCO";
				rail        = "";
			};
			class Backpack  {
				backpack    = "B_AssaultPack_mcamo";
				content[] = {{"130Rnd_338_Mag", 2}};
			};
		};
		class Variant2 : Variant1 {
		displayName = "Long Range";

			class Primary : Primary {
				weapon      = "arifle_MX_SW_F";
				magazines[] = {{"100Rnd_65x39_caseless_mag", 5}};
				bipod       = "bipod_01_F_snd";
				optics      = "optic_MRCO";
				rail        = "";
			};
		};
	};
};

class A3_NATO_Grenadier : A3_NATO_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_MX_GL_F";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class A3_NATO_LAT : A3_NATO_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class A3_NATO_HAT : A3_NATO_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};

			class Secondary {
				weapon      = "launch_B_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AT", 1}};
			};
		};
		class Variant2 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class A3_NATO_AA : A3_NATO_Default {
	scope = 2;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};

			class Secondary {
				weapon      = "launch_B_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_NATO_Marksman : A3_NATO_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "MXM";

			class Primary : Primary {
				weapon      = "arifle_MXM_F";
				optics      = "optic_DMS";
				bipod       = "bipod_01_F_snd";
			};
		};
		class Variant2 : Variant1 {
			displayName = "EBR 7.62mm";

			class Primary : Primary {
				weapon      = "srifle_EBR_F";
				magazines[] = {{"20Rnd_762x51_Mag", 5}};
				optics      = "optic_DMS";
				bipod       = "bipod_01_F_snd";
			};
		};
	};
};

class A3_NATO_Engineer : A3_NATO_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Primary : Primary {
				weapon      = "arifle_MXC_F";
				optics      = "optic_Aco";
			};

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class A3_NATO_Sniper : A3_NATO_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_LRR_camo_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"7Rnd_408_Mag", 10}};
			};
		};
	};
};

class A3_NATO_Crewman : A3_NATO_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing: Clothing{
		headgear = "H_HelmetCrew_B";
		vest = "V_Chestrig_rgr";
	};
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "arifle_MXC_F";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 6}};
			};
			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class A3_NATO_RECON_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN
	class Clothing {
		uniform   = "U_B_CombatUniform_mcam_vest";
		headgear  = "H_Booniehat_mcamo";
		goggles   = "G_Bandanna_khk";
		vest      = "V_PlateCarrier1_rgr";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_MX_Black_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_flashlight";
				optics      = "optic_MRCO";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_AssaultPack_rgr";
			};
		};
	};
};

class A3_NATO_RECON_Rifleman : A3_NATO_Recon_Default {
	scope = 2;

	class Variants {
		class Variant1 {
			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};
	};
};

class A3_NATO_RECON_TeamLeader : A3_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
	};
};

class A3_NATO_RECON_Medic : A3_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack{
				content[] = {COMMON_MEDIC_NVG_VANILLA};
			};
		};
	};
};

class A3_NATO_RECON_Engineer : A3_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class A3_NATO_RECON_LAT : A3_NATO_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};
