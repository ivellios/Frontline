// PLA

#define RDS 	"optic_ACO_grn"
#define SCOPE 	"optic_MRCO"
#define BKP 	"B_FieldPack_taiga_F"

class A3_PLA_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
	    uniform   = "O_CombatUniform_FRL_chn_07universal";
	    headgear  = "H_HelmetIA_chn_07universal";
	    goggles   = "";
	    vest      = "V_TacVest_chn_07universal";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_Katiba_F";
				rail        = "acc_flashlight";
				optics      = RDS;
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}};
			};

			class Pistol {
				weapon      = "hgun_Rook40_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG_VANILLA};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2: Variant1 {
			displayName = "Apex";
			class Primary: Primary{
				weapon      = "arifle_CTAR_blk_F";
				rail        = "acc_flashlight";
				optics      = RDS;
				muzzle      = "";
				bipod       = "";
				magazines[] 	= {{"30Rnd_580x42_Mag_F", 9}};
			};
		};
	};

};

class A3_PLA_Rifleman: A3_PLA_Default {
	scope = 2;

	class Variants: Variants {

		class Variant1: Variant1 {
			class Primary: Primary {
				optics      = SCOPE;
			};
			items[]       = {COMMON_RIFLEMAN_NVG_VANILLA};
		};

		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = RDS;
			};
			items[] = {COMMON_RIFLEMAN_CQB_NVG_VANILLA};
		};

		class Variant3: Variant1 {
			displayName = "APEX";
			class Primary: Primary {
				weapon      = "arifle_CTAR_blk_F";
				optics      = SCOPE;
				magazines[] 	= {{"30Rnd_580x42_Mag_F", 9}};
			};
		};

		class Variant4: Variant2 {
			displayName = "APEX CQB";
			class Primary : Primary {
				weapon      = "arifle_CTAR_blk_F";
				optics      = RDS;
				magazines[] 	= {{"30Rnd_580x42_Mag_F", 9}};
			};
		};
	};
};

class A3_PLA_SquadLeader : A3_PLA_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = SCOPE;
			};
			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = RDS;
			};
			items[] = {COMMON_TEAMLEADER_NVG_CQB_VANILLA};
		};
		class Variant3 : Variant1 {
			displayName = "APEX";
			class Primary : Primary {
				weapon      = "arifle_CTAR_blk_F";
				optics      = SCOPE;
				magazines[]	= {{"30Rnd_580x42_Mag_F", 9}};
			};
		};
		class Variant4 : Variant1 {
			displayName = "APEX CQB";
			class Primary : Primary {
				weapon      = "arifle_CTAR_blk_F";
				optics      = RDS;
				magazines[] = {{"30Rnd_580x42_Mag_F", 9}};
			};
		};
	};
};

class A3_PLA_Spotter : A3_PLA_Default {
	scope = 2;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_O_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_TacVest_chn_07universal";
	};

	class Variants: Variants {

		class Variant1: Variant1 {
			displayName = "str_b_spotter_f0";
		};
		/*
		class Variant2 : Variant1 {
			displayName = "Apex";
			class Primary : Primary {
				weapon      = "arifle_CTAR_blk_F";
				magazines[] = {{"30Rnd_580x42_Mag_F", 9}};
			};
		};
		*/
	};

};

class A3_PLA_Medic : A3_PLA_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_medic_f0";

			class Backpack {
				backpack 	= BKP;
				content[] 	= {COMMON_MEDIC};
			};
			items[] = {COMMON_NVG_VANILLA};
		};
		class Variant2 : Variant1 {
			displayName = "Apex";
			class Primary : Primary {
				weapon      = "arifle_CTAR_blk_F";
				magazines[]	= {{"30Rnd_580x42_Mag_F", 9}};
			};
		};
	};
};

// todo backpacks
class A3_PLA_AR : A3_PLA_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Zafir";

			class Primary : Primary {
				weapon      = "LMG_Zafir_F";
				magazines[] = {{"150Rnd_762x54_Box", 5}};
				rail 		= "";
				optics      = SCOPE;
			};
		};
		class Variant2 : Variant1 {
			displayName = "APEX";

			class Primary : Primary {
				weapon      = "arifle_CTARS_blk_F";
				magazines[] = {{"100Rnd_580x42_Mag_F", 6}};
				rail 		= "";
				optics      = RDS;
			};
		};
	};
};

class A3_PLA_MG : A3_PLA_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "MMG";

			class Primary : Primary {
				weapon      = "MMG_01_hex_F";
				magazines[] = {{"150Rnd_93x64_Mag", 1, 1}};
				optics      = SCOPE;
				bipod       = "bipod_02_F_hex";
				rail        = "";
			};
			class Backpack {
				backpack 	= BKP;
				content[] = {{"150Rnd_93x64_Mag", 2}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Zafir";

			class Primary : Primary {
				weapon      = "LMG_Zafir_F";
				magazines[] = {{"150Rnd_762x54_Box", 3}};
				rail        = "";
			};
			class Backpack : Backpack {
				content[] = {{"150Rnd_762x54_Box", 2}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "APEX";

			class Primary : Primary {
				weapon      = "arifle_CTARS_blk_F";
				magazines[] = {{"100Rnd_580x42_Mag_F", 3}};
				rail 		= "";
				optics      = RDS;
			};

			class Backpack : Backpack {
				content[] = {{"100Rnd_580x42_Mag_F", 3}};
			};
		};
	};
};

class A3_PLA_Grenadier : A3_PLA_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_GL_F";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack {
				backpack 	= BKP;
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Apex";
			class Primary : Primary {
				weapon      = "arifle_CTAR_GL_blk_F";
				magazines[]	= {{"30Rnd_580x42_Mag_F", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};
		};

	};
};

class A3_PLA_LAT : A3_PLA_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_lat_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = RDS;
			};

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack {
				backpack 	= BKP;
				content[] = {{"NLAW_F", 1}};
			};
		};
		class Variant2 : Variant1 {
			class Primary : Primary {
				weapon      = "arifle_CTAR_blk_F";
				optics      = RDS;
			};
		};
	};
};

class A3_PLA_HAT : A3_PLA_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = RDS;
			};

			class Secondary {
				weapon      = "launch_I_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack {
				backpack 	= BKP;
				content[] = {{"Titan_AT", 1}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "str_b_soldier_lat_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = RDS;
			};

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};
	};
};

class A3_PLA_AA : A3_PLA_Default {
	scope = 2;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_aa_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = RDS;
			};

			class Secondary {
				weapon      = "launch_I_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack {
				backpack 	= BKP;
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class A3_PLA_Marksman : A3_PLA_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "srifle_DMR_01_F";
				optics      = "optic_DMS";
				rail        = "";
				bipod       = "bipod_02_F_hex";
				magazines[] = {{"10Rnd_762x54_Mag", 9}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Apex";

			class Primary : Primary {
				weapon      = "srifle_DMR_07_blk_F";
				optics      = "optic_DMS";
				rail        = "";
				bipod       = "";
				magazines[] = {{"20Rnd_650x39_Cased_Mag_F", 6}};
			};
		};
	};
};

class A3_PLA_Engineer : A3_PLA_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Demolition Specialist";

			class Primary : Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = RDS;
			};

			class Backpack {
				backpack 	= BKP;
				content[]   = {{"FRL_ExplosiveCharge_Wpn", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Demolition Specialist (APEX)";
			class Primary: Primary {
				weapon      = "arifle_CTAR_blk_F";
				magazines[]	= {{"30Rnd_580x42_Mag_F", 9}};
			};
		};
	};
};

class A3_PLA_Crewman : A3_PLA_Default {
	scope = 2;
	ROLE_CREWMAN
    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "arifle_Katiba_C_F";
				optics      = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 6}};
			};
			items[]       = {COMMON_TEAMLEADER_NVG_VANILLA};
		};
		class Variant2 : Variant1 {
			class Primary {
				weapon      = "arifle_CTAR_blk_F";
				optics      = "";
				magazines[]	= {{"30Rnd_580x42_Mag_F", 6}};
			};
		};
	};
};

class A3_PLA_Sniper : A3_PLA_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_camo_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};

class A3_PLA_HMGLeader : A3_PLA_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class A3_PLA_HMGAmmo : A3_PLA_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};
