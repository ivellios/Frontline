#define COMMON_ITEMS {"SmokeShell", 1}, {"HandGrenade", 1}
#define COMMON_ITEMS_CQB {"SmokeShell", 1}, {"HandGrenade", 2}
#define COMMON_ITEMSHIDDEN {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}, {"itemGPS", 1}
#define COMMON_FIRSTAID {"FRL_fieldDressing", 3}, {"FRL_epinephrine", 2}
#define COMMON_MEDIC {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}
#define COMMON_SUPPORT {"SmokeShell", 1}
#define COMMON_RIFLEMAN {"SmokeShell", 2}, {"HandGrenade", 2}
#define COMMON_RIFLEMAN_CQB {"SmokeShell", 2}, {"HandGrenade", 3}
#define COMMON_LEADER {"SmokeShell", 2}, {"HandGrenade", 1}, {"itemGPS", 1}, {"Rangefinder", 1}
#define COMMON_LEADER_CQB {"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"Rangefinder", 1}
#define COMMON_TEAMLEADER {"SmokeShell", 1}, {"HandGrenade", 1}, {"Rangefinder", 1}
#define COMMON_STATICTEAM {"SmokeShell", 1}, {"HandGrenade", 1}, {"Binocular", 1}

#define COMMON_ITEMS {"SmokeShell", 1}, {"HandGrenade", 1}
#define COMMON_ITEMS_NVG {"SmokeShell", 1}, {"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}
#define COMMON_ITEMS_NVG_VANILLA {"SmokeShell", 1}, {"HandGrenade", 1}, {"NVGoggles_OPFOR", 1}
#define COMMON_MEDIC {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}
#define COMMON_MEDIC_NVG {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}, {"rhsusf_ANPVS_15", 1}
#define COMMON_MEDIC_NVG_VANILLA {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}, {"NVGoggles_OPFOR", 1}
#define COMMON_SUPPORT_NVG {"SmokeShell", 1}, {"rhsusf_ANPVS_15", 1}
#define COMMON_SUPPORT_NVG_VANILLA {"SmokeShell", 1}, {"NVGoggles_OPFOR", 1}
#define COMMON_RIFLEMAN {"SmokeShell", 2}, {"HandGrenade", 2}
#define COMMON_RIFLEMAN_NVG {"SmokeShell", 2}, {"HandGrenade", 2}, {"rhsusf_ANPVS_15", 1}
#define COMMON_RIFLEMAN_NVG_VANILLA {"SmokeShell", 2}, {"HandGrenade", 2}, {"NVGoggles_OPFOR", 1}
#define COMMON_RIFLEMAN_CQB {"SmokeShell", 2}, {"HandGrenade", 3}
#define COMMON_RIFLEMAN_CQB_NVG_VANILLA {"SmokeShell", 2}, {"HandGrenade", 3}, {"NVGoggles_OPFOR", 1}
#define COMMON_TEAMLEADER {"SmokeShell", 1}, {"HandGrenade", 1}, {"Rangefinder", 1}
#define COMMON_TEAMLEADER_CQB {"SmokeShell", 1}, {"HandGrenade", 2}, {"Rangefinder", 1}
#define COMMON_TEAMLEADER_NVG {"SmokeShell", 1}, {"HandGrenade", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}
#define COMMON_TEAMLEADER_NVG_VANILLA {"SmokeShell", 1}, {"HandGrenade", 1}, {"Rangefinder", 1}, {"NVGoggles_OPFOR", 1}
#define COMMON_TEAMLEADER_NVG_CQB_VANILLA {"SmokeShell", 1}, {"HandGrenade", 2}, {"Rangefinder", 1}, {"NVGoggles_OPFOR", 1}
#define COMMON_STATICTEAM_NVG {"SmokeShell", 1}, {"HandGrenade", 1}, {"Binocular", 1}, {"rhsusf_ANPVS_15", 1}
#define COMMON_STATICTEAM_NVG_VANILLA {"SmokeShell", 1}, {"HandGrenade", 1}, {"Binocular", 1}, {"NVGoggles_OPFOR", 1}

#define COMMON_NVG_VANILLA {"NVGoggles_OPFOR", 1}

#include "FRL_Roles.hpp"

#include "Vanilla\A3_CSAT.hpp"
#include "Vanilla\A3_GREF.hpp"
#include "Vanilla\A3_NATO.hpp"
#include "Vanilla\A3_PLA.hpp"
#include "Vanilla\A3_GUER.hpp"
#include "Vanilla\APEX_NATO.hpp" // M416
#include "Vanilla\APEX_CHN.hpp" // Chinese

#include "RHS\RHS_USA_W.hpp"
#include "RHS\RHS_USMC_W.hpp"
#include "RHS\RHS_RUA_W.hpp"
#include "RHS\RHS_INS_W.hpp"
#include "RHS\RHS_INS_D.hpp"
#include "RHS\RHS_Iraq.hpp"
#include "RHS\RHS_UN_D.hpp"
#include "RHS\RHS_AFP.hpp"

#include "UNS\UNS_USA.hpp"
#include "UNS\UNS_VC.hpp"


#define COMMON_ITEMSHIDDEN_IFA {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1}
#include "IFA\IFA_GER.hpp"
#include "IFA\IFA_GER_W.hpp"
#include "IFA\IFA_GER_DAK.hpp"
#include "IFA\IFA_GER_AB.hpp"
#include "IFA\IFA_USSR.hpp"
#include "IFA\IFA_USSR_W.hpp"
#include "IFA\IFA_USSR_P.hpp"
#include "IFA\IFA_USA.hpp"
#include "IFA\IFA_USA_P.hpp"
#include "IFA\IFA_USA_AB.hpp"
#include "IFA\IFA_JP.hpp"
#include "IFA\IFA_UK.hpp"
#include "IFA\IFA_UK_AB.hpp"
#include "IFA\IFA_UK_D.hpp"
#include "IFA\IFA_UK_Com.hpp"
#include "IFA\IFA_GREE.hpp"
//#include "Turkey\FRL_TURK_D.hpp"
//#include "Turkey\FRL_TURK_W.hpp"
