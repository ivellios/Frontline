// SW3 - Wehrmacht default woodland

class SW_CW_Default {
	scope = 0;
	ROLE_CW_RIFLEMAN

	class Clothing {
		uniform   = "SWOP_Clonetrooper_F_CombatUniform";
		headgear  = "SWOP_Clonetrooper_helmet";
		goggles   = "";
		vest      = "SWOP_Clonetrooper_armor";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "SWOP_DC15ABlasterRifle";
				rail        = "";
				optics      = "SWOP_DC15A_HoloScope";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"SWOP_DC15ABlasterRifle_Full_Mag", 6}};
			};

			items[]       = {{"SmokeShell", 1}, {"SWOP_termDet_Gm", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_CW, COMMON_FIRSTAID};
		};
	};
};

//class b imploder
//SWOP_B_CloneBackpack
//squad energy shield

class SW_CW_Rifleman : SW_CW_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			items[]       = {{"SWOP_termDet_Gm", 3}, {"SmokeShell", 1}};
		};
	};
};

class SW_CW_Rifleman_Semi : SW_CW_Default {
	scope = 2;
	ROLE_SW_RIFLEMAN_1STCLASS
	

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Clonetrooper Carbine";
			class Primary {
				weapon      = "SWOP_DC15_GRIP";
				optics      = "SWOP_DC15S_HoloScope";
				magazines[] = {{"SWOP_DC15_Mag", 3}};
			};
			items[]       = {{"SmokeShell", 1}};
		};
	};
};

class SW_CW_Grenadier_Rifleman : SW_CW_Default {
	scope = 2;
    ROLE_SW_GRENADIER

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifle Grenades";
            class Primary {
				weapon      = "LIB_K98";
				rail        = "";
				optics      = "";
				muzzle      = "LIB_ACC_GW_SB_Empty";
				bipod       = "";
				magazines[] = {{"SWOP_DC15ABlasterRifle_Full_Mag", 6}};
			};
			items[]       = {{"SmokeShell", 1}, {"5Rnd_HE_Grenade_DC15A", 3}};
		};
        /*class Variant2: Variant1 {
            displayName = "Bundle Grenades";
            class Primary {
				weapon      = "SWOP_DC15AGL";
				rail        = "";
				optics      = "SWOP_DC15S_HoloScope";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"SWOP_DC15_Mag", 3}};
			};
			items[]       = {{"lib_nb39", 1}};
            class Backpack {
                backpack = "fow_b_grenadebag";
				content[] = {{"LIB_shg24x7", 3}};
			};
        };*/
	};
};

class SW_CW_SquadLeader : SW_CW_Default {
	scope = 2;
	ROLE_SW_OFFICER


    class Clothing: Clothing {
        uniform   = "SWOP_Clonetrooper_501jesse_F_CombatUniform";
		headgear  = "SWOP_Clonetrooper_501dino_helmet";
		vest      = "SWOP_Clonetrooper_501_armor_recon2";
		goggles   = "";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Squad Leader";


      class Primary {
				weapon      = "SWOP_DC15ABlasterRifle";
				magazines[] = {{"SWOP_DC15ABlasterRifle_Full_Mag", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"5Rnd_HE_Grenade_DC15A", 1}, {"scoutElectroBinoculars_F", 1}};

		};
		/*class Variant2: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "fow_w_g43";
				magazines[] = {{"fow_10nd_792x57", 6}};
			};

            class Pistol {
                weapon      = "fow_w_p08";
				magazines[] = {{"fow_8Rnd_9x19", 3}};
            };
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_CW", 1}};
		};
		class Variant3: Variant1 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 8}};
			};
            class Pistol {
                weapon      = "fow_w_ppk";
				magazines[] = {{"fow_7Rnd_765x17", 3}};
            };
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_CW", 1}};
		};*/
	};
};

class SW_CW_Medic : SW_CW_Default {
	scope = 2;
	ROLE_SW_COMBAT_MEDIC

    class Clothing {
		uniform   = "SWOP_Clonetrooper_P1_F_CombatUniform";
		headgear  = "SWOP_Clonetrooper_501jay_helmet";
		goggles   = "";
		vest      = "SWOP_Clonetrooper_9med_armor";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rifle";

			class Backpack {
                backpack = "SWOP_B_CloneBackpack_med";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"SmokeShell", 7}};
			};
		};
	};
};

class SW_CW_MG : SW_CW_Default {
	scope = 2;
	ROLE_SW_LMG

    class Clothing: Clothing {
        uniform   = "fow_u_CW_m43_01_frag_private";
		vest      = "fow_v_heer_mg";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rotary Blaster";

			class Primary : Primary {
				weapon      = "SWOP_Z6Blaster";
				magazines[] = {{"300Rnd_BlasterLaser_Belt", 3}};
			};

            class Backpack {
				backpack    = "SWOP_B_CloneBackpack";
                content[]   = {};
			};
			items[]       = {{"SmokeShell", 1}};
		};

		class Variant2 : Variant1 {
			displayName = "LMG";

			class Primary : Primary {
				weapon      = "SWOP_DLT19BlasterRifle";
				magazines[] = {{"SWOP_DLT19BlasterRifle_Mag", 5}};
			};
			items[]       = {{"SmokeShell", 1}};
		};
	};
};

// -- Assistant gunner
/*class SW_CW_MGAssistant : SW_CW_Default {
  scope = 2;
	ROLE_SW_ASSISTANT_GUNNER

	class Variants {
		class Variant1 {
			displayName = "Rifle";
			class Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 7}};
			};

			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 2}, {"LIB_Binocular_CW", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_SW, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "fow_b_ammoboxes";
                content[]   = {{"LIB_50Rnd_792x57", 5}};
				};
			};
			class Variant2 : Variant1 {
			displayName = "Pistol";

			class Primary {
				weapon      = "";
				magazines[] = {{"fow_8Rnd_9x19", 1}};
			};
            class Pistol {
                weapon      = "fow_w_p08";
				magazines[] = {{"fow_8Rnd_9x19", 5}};
            };

			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 1}, {"LIB_Binocular_CW", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_SW, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "fow_b_ammoboxes";
                content[]   = {{"LIB_50Rnd_792x57", 5}};
			};
		};
	};
};

class SW_CW_MGAmmo : SW_CW_Default {
  scope = 2;
	ROLE_SW_LMG_AMMO_BEARER

	class Variants {
		class Variant1 {
			displayName = "Rifle";

            class Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 6}};
			};

			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_SW, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_CW_Backpack";
                content[]   = {{"LIB_50Rnd_792x57", 6}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Pistol";

			class Primary {
				weapon      = "";
				magazines[] = {{"fow_8Rnd_9x19", 1}};
			};
            class Pistol {
                weapon      = "fow_w_p08";
				magazines[] = {{"fow_8Rnd_9x19", 5}};
            };


			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_SW, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_CW_Backpack";
                content[]   = {{"LIB_50Rnd_792x57", 6}};
			};
		};
	};
};*/

/*class SW_CW_CorporalSMG: SW_CW_Default {
  scope = 2;
	ROLE_SW_CORPORAL

    class Clothing : Clothing {
		uniform   = "U_LIB_CW_Gefreiter";
        headgear  = "fow_h_CW_feldmutze";
        vest    = "V_LIB_CW_VestMP40";
    };
    class Variants : Variants {
		class Variant1 : Variant1 {
            displayName = "SMG";
      class Primary {
          weapon      = "LIB_MP40";
          rail        = "";
          optics      = "";
          muzzle      = "";
          bipod       = "";
          magazines[] = {{"LIB_32Rnd_9x19", 6}};
      };
			items[]       = {{"lib_nb39", 2}, {"LIB_Binocular_CW", 1}};
		};
	};
};*/

/*class SW_CW_LAT : SW_CW_Default {
	scope = 2;
	ROLE_SW_RIFLE_LAT

  class Clothing: Clothing {
      uniform = "AT Grenades";
  };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "STR_LIB_CW_AT_grenadier";

			class Backpack {
        		backpack = "B_LIB_CW_Backpack";
				content[] = {{"lib_pwm", 3}};
			};
		};
		class Variant2: Variant1 {
			displayName = "Panzerfaust";
			class Primary : Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 6}};
			};
			class Secondary {
				weapon      = "RocketHH15Clone";
				magazines[] = {{"fow_1Rnd_pzfaust_30", 1, 1}};
			};
			class Backpack {
        		backpack = "";
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}};
		};*/
	};
};


class SW_CW_HAT : SW_CW_Default {
	scope = 2;
	ROLE_SW_RAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "RocketHH15Clone";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RocketHH15CloneHEATG_Mag", 1,1}};
			};
			class Backpack {
                backpack = "SWOP_B_CloneBackpack_dem";
				content[] = {{"RocketHH15CloneHEATFF_Mag", 1},{"RocketHH15CloneHEATG_Mag", 1}};
			};
		};
	};
};
/*class SW_CW_Sniper : SW_CW_Default {
    scope = 2;
		ROLE_SW_SNIPER

    class Clothing {
        uniform     = "U_LIB_CW_Scharfschutze";
        headgear    = "fow_h_CW_feldmutze";
        vest        = "V_LIB_WP_SniperBela";
        goggles     = "";
    };

    class Variants : Variants {
        class Variant1 : Variant1 {
            displayName = "STR_LIB_CW_scout_sniper";

            class Primary {
                weapon      = "LIB_K98ZF39";
                magazines[] = {{"LIB_5Rnd_792x57", 8}};
            };
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_CW", 1}};
        };
    };
};*/

/*class SW_CW_MortarTube : SW_CW_Default {
	scope = 2;
	ROLE_MORTARMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar tube";

			class Secondary {
				weapon      = "LIB_GrWr34_Barrel";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_81mmHE_GRWR34", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_CW_Backpack";
				content[] = {{"LIB_1rnd_81mmHE_GRWR34", 8}};
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_CW", 1}};
		};
	};
};

class SW_CW_MortarAmmo : SW_CW_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar Ammo";
			class Secondary {
				weapon      = "LIB_GrWr34_Tripod";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_81mmHE_GRWR34", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_CW_Backpack";
				content[] = {{"LIB_1rnd_81mmHE_GRWR34", 8}};
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_CW", 1}};
		};
	};
};
class SW_CW_HMGLeader : SW_CW_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "";
			};

            class Pistol {
                weapon      = "fow_w_p08";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"fow_8Rnd_9x19", 5}};
            };
			class Backpack {
                backpack = "frl_staticammo_bag";
				content[] = {{"LIB_50Rnd_792x57", 3}};
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_CW", 1}};
		};
	};
};

class SW_CW_HMGAmmo : SW_CW_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_CW", 1}};
		};
	};
};*/

class SW_CW_Crewman : SW_CW_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing {
		uniform   = "fow_u_CW_tankcrew_01_obergefreiter";
		headgear  = "H_LIB_CW_TankPrivateCap2";
		goggles   = "";
		vest      = "fow_v_heer_tankcrew_p38";
	};
	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "LIB_MP40";
				magazines[] = {{"LIB_32Rnd_9x19", 3}};
			};
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_CW", 1}};
            itemshidden[] = {COMMON_ITEMSHIDDEN_SW, COMMON_FIRSTAID};
		};
	};
};
/*class SW_CW_Crewman_1 : SW_CW_Crewman {
	ROLE_CREWMAN
	scope = 2;
	class Clothing {
		uniform   = "fow_u_CW_tankcrew_01";
		headgear  = "fow_h_CW_m38_feldmutze_panzer";
		goggles   = "";
		vest      = "fow_v_heer_tankcrew_p38";
	};
	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "";
				magazines[] = {{"fow_8Rnd_9x19", 1}};
			};
            class Pistol {
                weapon      = "fow_w_p08";
				magazines[] = {{"fow_8Rnd_9x19", 3}};
            };
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_CW", 1}};
            itemshidden[] = {COMMON_ITEMSHIDDEN_SW, COMMON_FIRSTAID};
		};
	};
};*/

/*class SW_CW_Flamethrower : SW_CW_Default {
    scope = 2;
    ROLE_FLAMETHROWER

    class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Flamethrower";
            class Primary {
				weapon      = "LIB_M2_Flamethrower";
				magazines[] = {};
			};
			class Backpack {
                backpack = "B_LIB_US_M2Flamethrower";
				content[] = {{"LIB_M2_Flamethrower_Mag", 1}};
			};

			items[]       = {{"LIB_US_M18", 1}};
		};
	};
};*/
