// RHS - UN Desert

class RHS_UN_D_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "rhs_uniform_g3_mc";
		headgear  = "rhsgref_helmet_pasgt_un";
		goggles   = "";
		vest      = "rhsusf_mbav_rifleman";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "rhs_weap_ak74mr";
				rail        = "";
				optics      = "rhsusf_acc_eotech_xps3";
				muzzle      = "rhs_acc_uuk";
				bipod       = "";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};

			class Pistol {
				weapon      = "rhs_weap_makarov_pm";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_9x18_8_57N181S", 3}};
			};

			items[]       = {COMMON_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_Kitbag_mcamo";
			};
		};
	};
};

class RHS_UN_D_Rifleman : RHS_UN_D_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			displayName = "Standard";
			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG2_USMC_3d";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"Binocular", 1}};
		};
		class Variant2: Variant1 {
            displayName = "CQB";
			class Primary : Primary {
				optics      = "rhsusf_acc_eotech_xps3";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			items[]       = {COMMON_RIFLEMAN};
		};
	};
};

class RHS_UN_D_SquadLeader : RHS_UN_D_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Clothing {
		uniform   = "rhs_uniform_g3_mc";
		headgear  = "rhsgref_un_beret";
		vest      = "rhsusf_mbav_rifleman";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG_3d";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}, {"rhs_30Rnd_545x39_AK_green", 1}};
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
		};
		class Variant2: Variant1 {
            displayName = "CQB";
			class Primary : Primary {
				optics      = "rhsusf_acc_T1_low_fwd";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
        };
	};
};

class RHS_UN_D_TeamLeader_MG : RHS_UN_D_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"rhs_100Rnd_762x54mmR", 5}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};


class RHS_UN_D_TeamLeader_AA : RHS_UN_D_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};

class RHS_UN_D_TeamLeader_HAT : RHS_UN_D_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"rhs_rpg7_PG7VL_mag", 1}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};

class RHS_UN_D_Medic : RHS_UN_D_Default {
	scope = 2;
	ROLE_MEDIC

	class Clothing {
		headgear  = "rhsgref_helmet_pasgt_un";
		vest      = "rhsusf_mbav_medic";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"HandGrenade", 1}};
		};
	};
};

class RHS_UN_D_AR : RHS_UN_D_Default {
	scope = 2;
	ROLE_AR

	class Clothing {
		uniform   = "rhs_uniform_g3_mc";
		headgear  = "rhsgref_helmet_pasgt_un";
		vest      = "rhsusf_mbav_mg";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "PKP";

			class Primary : Primary {
				weapon      = "rhs_weap_pkm";
				rail        = "";
				optics      = "";
				magazines[] = {{"rhs_100Rnd_762x54mmR", 4}};
			};
			items[]       = {COMMON_SUPPORT};
		};
		class Variant2 : Variant1 {
			displayName = "RPK";

			class Primary : Primary {
				weapon      = "rhs_weap_ak74mr";
				rail        = "";
				optics      = "rhsusf_acc_eotech_552";
				muzzle      = "rhs_acc_uuk1";
				bipod       = "rhsusf_acc_grip1";
				magazines[] = {{"rhs_45Rnd_545X39_7N22_AK", 10}};
			};
			items[]       = {COMMON_ITEMS};
		};
	};
};

class RHS_UN_D_MG : RHS_UN_D_Default {
	scope = 2;
	ROLE_MG

	class Clothing {
		uniform   = "rhs_uniform_g3_mc";
		headgear  = "rhsgref_helmet_pasgt_un";
		vest      = "rhsusf_mbav_mg";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "PKP";

			class Primary : Primary {
				weapon      = "rhs_weap_pkp";
				rail        = "";
				optics      = "rhsusf_acc_ACOG2_USMC_3d";
				magazines[] = {{"rhs_100Rnd_762x54mmR", 5}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_UN_D_Grenadier : RHS_UN_D_Default {
	scope = 2;
	ROLE_GRENADIER

	class Clothing {
		uniform   = "rhs_uniform_g3_mc";
		headgear  = "rhsgref_helmet_pasgt_un";
		vest      = "rhsusf_mbav_grenadier";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_ak74mr_gp25";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}, {"rhs_VOG25", 8}};
			};
			class Backpack : Backpack{
				content[] = {{"rhs_VG40OP_white", 4}, {"rhs_GRD40_White", 4}};
			};
			items[]       = {COMMON_SUPPORT};
		};
		class Variant2: Variant1 {
        displayName = "RPG HE";
			class Primary : Primary {
				weapon      = "rhs_weap_ak74mr";
				rail        = "";
				muzzle      = "rhs_acc_uuk";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			class Secondary {
				weapon      = "rhs_weap_M136_hedp";
				magazines[] = {{"rhs_m136_mag_hedp", 1, 1}};
			};
			class Backpack : Backpack{
				content[] = {};
			};
		items[]       = {{"SmokeShell", 5}};
		};
	};
};

class RHS_UN_D_LAT : RHS_UN_D_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "rhs_weap_M136";
				magazines[] = {{"rhs_m136_mag", 1, 1}};
			};

			class Backpack {
				backpack = "";
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_UN_D_HAT : RHS_UN_D_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "RPG";

			class Secondary {
				weapon      = "rhs_weap_rpg7";
				optics      = "rhs_acc_pgo7v3";
				magazines[] = {{"rhs_rpg7_PG7V_mag", 2}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_rpg7_PG7V_mag", 1}};
			};
			items[]       = {COMMON_SUPPORT};
		};

		class Variant2 : Variant2 {
			displayName = "MAAWS";

			class Secondary {
				weapon      = "rhs_weap_maaws";
				optics      = "rhs_optic_maaws";
				magazines[] = {{"rhs_mag_maaws_HEAT", 2}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_mag_maaws_HEAT", 1}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_UN_D_AA : RHS_UN_D_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "rhs_weap_igla";
				magazines[] = {{"rhs_mag_9k38_rocket", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_UN_D_Marksman : RHS_UN_D_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_svds_npz";
				optics      = "rhsusf_acc_ACOG_MDO";
				magazines[] = {{"rhs_10Rnd_762x54mmR_7N1", 8}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_UN_D_Engineer : RHS_UN_D_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Breacher";

			class Primary {
				weapon      = "rhs_weap_M590_5RD";
				magazines[] = {{"rhsusf_5Rnd_Slug", 4}, {"rhsusf_5Rnd_00Buck", 4}};
			};
			class Backpack : Backpack {
				content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_mk84", 2}};
		};
		class Variant2: Variant1 {
            displayName = "Demo Charges";
            class Primary {
                weapon      = "rhs_weap_ak74mr";
                rail        = "";
                optics      = "rhsusf_acc_eotech_552";
				muzzle      = "rhs_acc_uuk";
                magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
            };
			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}};
        };
		class Variant3: Variant2 {
            displayName = "Claymores";
			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}};
        };
	};
};

class RHS_UN_D_Spotter : RHS_UN_D_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_B_FullGhillie_ard";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
			class Primary {
				weapon      = "rhs_weap_ak74m_zenitco01_b33";
				rail        = "";
				optics      = "rhsusf_acc_T1_low_fwd_ak";
				muzzle      = "rhs_acc_dtk4short";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			items[]       = {{"SmokeShell", 1}, {"lerca_1200_tan", 1}};
		};
	};
};

class RHS_UN_D_Sniper : RHS_UN_D_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M2010 ESR";
			class Primary {
				weapon      = "rhs_weap_XM2010_d";
				muzzle      = "";
				rail        = "";
				optics      = "rhsusf_acc_M8541_low_d";
				bipod       = "rhsusf_acc_harris_swivel";
				magazines[] = {{"rhsusf_5Rnd_300winmag_xm2010", 8}};
			};
			items[]       = {{"Leupold_Mk4", 1}};
		};
		class Variant2 : Variant1 {
			displayName = "SVD";

			class Primary : Primary {
				weapon      = "rhs_weap_svds_npz";
				optics      = "rhsusf_acc_M8541_low";
				bipod       = "";
				magazines[] = {{"rhs_10Rnd_762x54mmR_7N1", 10}};
			};
		};
	};
};

class RHS_UN_D_Crewman : RHS_UN_D_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing {
		uniform   = "rhs_uniform_g3_mc";
		headgear  = "rhsusf_cvc_green_ess";
		vest      = "rhsusf_spc_crewman";
	};
	class Variants : Variants {
		class Crewman : Variant1 {
 			displayName = "Crewman";
			class Primary {
				weapon      = "rhs_weap_aks74un";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 2}};
			};
			class Pistol {
				weapon      = "";
				magazines[] = {};
			};
			items[]       = {{"itemGPS", 1}, {"lerca_1200_tan", 1}};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "U_B_HeliPilotCoveralls";
                headgear  = "H_PilotHelmetHeli_O";
                goggles   = "";
                vest      = "V_PlateCarrierSpec_rgr";
            };
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
			class Pistol {
				weapon      = "rhs_weap_makarov_pm";
				magazines[] = {{"rhs_mag_9x18_8_57N181S", 3}};
			};
			class Backpack {
                backpack = "B_Parachute";
				content[] = {};
			};	
		items[]       = {{"itemGPS", 1}, {"rhsusf_ANPVS_15", 1}};
        };
	};
};

class RHS_UN_D_HMGLeader : RHS_UN_D_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class RHS_UN_D_HMGAmmo : RHS_UN_D_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};

class RHS_UN_D_UAVOperator : RHS_UN_D_Default {
	scope = 2;
	UAV_OPERATOR

	class Variants : Variants {
		class Variant1 : Variant1 {

			class Backpack {
				backpack    = "B_UAV_01_backpack_F";
			};
		items[]       = {{"B_UavTerminal", 1}, {"SmokeShell", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		itemshidden[] = {{"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}, COMMON_FIRSTAID};
		};
	};
};