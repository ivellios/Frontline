#define UNIFORM_RHS_IRAQ_SF class Clothing {\
		uniform   = "rhs_uniform_g3_blk";\
		headgear  = "rhsusf_opscore_bk_pelt";\
		goggles   = "G_Bandanna_beast";\
		vest      = "rhsusf_mbav_rifleman";\
	};

class RHS_Iraq_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "rhsgref_uniform_3color_desert";
		headgear  = "rhsusf_ach_bare_tan";
		goggles   = "";
		vest      = "V_TacVest_camo";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "rhs_weap_m16a4_carryhandle";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 7}};
			};

			class Pistol {
				weapon      = "rhsusf_weap_m9";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhsusf_mag_15Rnd_9x19_JHP", 3}};
			};

			items[]       = {COMMON_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_TacticalPack_mcamo";
			};
		};
	};
};

class RHS_Iraq_Rifleman : RHS_Iraq_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			displayName = "Standard";
			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG2_USMC_3d";
				magazines[] = {{"rhs_mag_30Rnd_556x45_Mk318_Stanag", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"Binocular", 1}};
		};

		class Variant2: Variant1 {

		displayName = "CQB";
			class Primary : Primary {
				weapon      = "rhs_weap_savz58p";
				optics      = "";
				magazines[] = {{"rhs_30Rnd_762x39mm_Savz58", 6}};
			};
			items[]       = {COMMON_RIFLEMAN};
		};
	};
};

class RHS_Iraq_SquadLeader : RHS_Iraq_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Clothing: Clothing {
		headgear  = "rhsusf_ach_bare";
		vest      = "V_TacVest_khk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Long Range";

			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG_d";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
		};
		class Variant2: Variant1 {
        displayName = "CQB";
			class Primary {
				weapon      = "rhs_weap_m4a1_d";
				rail        = "";
				optics      = "rhsusf_acc_eotech_552_d";
				magazines[] = {{"rhs_mag_30Rnd_556x45_Mk318_Stanag", 7}};
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
		};
	};
};

class RHS_Iraq_TeamLeader_MG : RHS_Iraq_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"rhsusf_100Rnd_762x51_m61_ap", 5}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};

class RHS_Iraq_TeamLeader_AA : RHS_Iraq_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};

class RHS_Iraq_TeamLeader_HAT : RHS_Iraq_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"rhs_rpg7_PG7VL_mag", 1}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};

class RHS_Iraq_Medic : RHS_Iraq_Default {
	scope = 2;
	ROLE_MEDIC

	class Clothing: Clothing {
		vest      = "V_TacVest_oli";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"HandGrenade", 1}};
		};
	};
};


class RHS_Iraq_AR : RHS_Iraq_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M249";

			class Primary : Primary {
				weapon      = "rhs_weap_m249";
				rail        = "";
				optics      = "";
				magazines[] = {{"rhs_200rnd_556x45_M_SAW", 3}};
			};
			items[]       = {COMMON_SUPPORT};
		};
		class Variant2: Variant2 {
        displayName = "PKM";
			class Primary {
				weapon      = "rhs_weap_pkm";
				rail        = "";
				magazines[] = {{"rhs_100Rnd_762x54mmR", 4}};
			};
		};
		class Variant3: Variant2 {
			displayName = "AK";

			class Primary {
				weapon      = "rhs_weap_ak74m_2mag_camo";
				magazines[] = {{"rhs_45Rnd_545X39_7N22_AK", 10}};
				optics      = "rhs_acc_1p78_3d";
			};
        };
	};
};

class RHS_Iraq_MG : RHS_Iraq_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_m240G";
				rail        = "";
				optics      = "rhsusf_acc_elcan_ard_3d";
				magazines[] = {{"rhsusf_100Rnd_762x51_m61_ap", 3},{"rhsusf_100Rnd_762x51_m62_tracer", 1}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_Iraq_Grenadier : RHS_Iraq_Default {
	scope = 2;
	ROLE_GRENADIER

	class Clothing: Clothing {
		vest      = "V_HarnessOGL_brn";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M16";

			class Primary : Primary {
				weapon      = "rhs_weap_m16a4_carryhandle_M203";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
			items[]       = {COMMON_SUPPORT};
		};

		class Variant2 : Variant1 {
			displayName = "AK";

			class Primary : Primary {
				weapon      = "rhs_weap_akms_gp25";
				magazines[] = {{"rhs_30Rnd_762x39mm", 5}, {"rhs_VOG25", 6}};
			};
			class Backpack : Backpack{
				content[] = {{"rhs_VG40OP_white", 4}, {"rhs_GRD40_White", 4}};
			};
			items[]       = {COMMON_SUPPORT};
		};
		class Variant3: Variant1 {
        displayName = "RPG HE";
			class Primary : Primary {
				weapon      = "rhs_weap_pm63";
				magazines[] = {{"rhs_30Rnd_762x39mm", 6}};
			};
			class Secondary {
				weapon      = "rhs_weap_rshg2";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_rshg2_mag", 1, 1}};
			};

			class Pistol {
				weapon      = "rhs_weap_M320";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_M441_HE", 4}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		items[]       = {{"SmokeShell", 5}, {"HandGrenade", 6}};
		};
	};
};

class RHS_Iraq_LAT : RHS_Iraq_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M72";

			class Secondary {
				weapon      = "rhs_weap_m72a7";
				magazines[] = {{"rhs_m72a7_mag", 1, 1}};
			};

			class Backpack {
				backpack = "";
			};
			items[]       = {COMMON_SUPPORT};
		};
		class Variant2: Variant1 {
        displayName = "AT4";
				class Secondary {
					weapon      = "rhs_weap_M136";
					magazines[] = {{"rhs_m136_mag", 1, 1}};
				};

			class Backpack {
				backpack = "";
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_Iraq_HAT : RHS_Iraq_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "rhs_weap_rpg7";
				optics      = "rhs_acc_pgo7v3";
				magazines[] = {{"rhs_rpg7_PG7V_mag", 2}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_rpg7_PG7V_mag", 1}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_Iraq_AA : RHS_Iraq_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "rhs_weap_igla";
				magazines[] = {{"rhs_mag_9k38_rocket", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_Iraq_Marksman : RHS_Iraq_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Clothing: Clothing {
		vest      = "V_BandollierB_cbr";
	};

	class Variants : Variants {

		class Variant1: Variant1 {
            displayName = "SVD";
            class Primary {
				weapon      = "rhs_weap_svdp_npz";
				optics      = "rhsusf_acc_ACOG_MDO";
				bipod       = "bipod_01_F_blk";
				magazines[] = {{"rhs_10Rnd_762x54mmR_7N1", 8}};
            };
			items[]       = {COMMON_SUPPORT};
        };

		class Variant2 : Variant1 {
			displayName = "M-76";

			class Primary : Primary {
				weapon      = "rhs_weap_m76";
				optics      = "rhs_acc_pso1m2";
				magazines[] = {{"rhsgref_10Rnd_792x57_m76", 8}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_Iraq_Engineer : RHS_Iraq_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Breacher";

			class Primary {
				weapon      = "rhs_weap_M590_5RD";
				magazines[] = {{"rhsusf_5Rnd_Slug", 4}, {"rhsusf_5Rnd_00Buck", 4}};
			};
			class Backpack : Backpack {
				content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_f1", 2}};
		};
		class Variant2: Variant1 {
            displayName = "Demo Charges";
            class Primary {
                weapon      = "rhs_weap_m4a1_carryhandle";
                rail        = "";
                magazines[] = {{"rhs_mag_30Rnd_556x45_Mk318_Stanag", 6}};
            };
			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}};
		};
		class Variant3: Variant2 {
            displayName = "Claymores";
			class Primary {
				weapon      = "rhs_weap_savz61";
				magazines[] = {{"rhsgref_20rnd_765x17_vz61", 10}};
			};
			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}};
        };
	};
};

class RHS_Iraq_Spotter : RHS_Iraq_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		headgear  = "";
		vest      = "V_Chestrig_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
			class Primary {
				weapon      = "rhs_weap_m92";
				magazines[] = {{"rhs_30Rnd_762x39mm", 6}};
			};
			items[]       = {{"SmokeShell", 1}, {"lerca_1200_tan", 1}};
		};
	};
};

class RHS_Iraq_Sniper : RHS_Iraq_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Clothing {
		uniform   = "rhsgref_uniform_3color_desert";
		headgear  = "";
		vest      = "V_BandollierB_cbr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SVD";
			class Primary {
				weapon      = "rhs_weap_svdp_wd_npz";
				optics      = "rhs_acc_dh520x56";
				magazines[] = {{"rhs_10Rnd_762x54mmR_7N1", 10}};
			};
			items[]       = {{"Leupold_Mk4", 1}};
		};
		class Variant2 : Variant1 {
			displayName = "M24";
			class Primary {
				weapon      = "rhs_weap_m24sws";
				muzzle      = "";
				rail        = "";
				optics      = "rhsusf_acc_M8541";
				bipod       = "rhsusf_acc_harris_swivel";
				magazines[] = {{"rhsusf_5Rnd_762x51_m118_special_Mag", 10}};
			};
			items[]       = {{"Leupold_Mk4", 1}};
		};
	};
};

class RHS_Iraq_RECON_Default : RHS_USA_W_RECON_Default {
	UNIFORM_RHS_IRAQ_SF
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"lerca_1200_tan", 1}};
        };  
		class Variant2 : Variant2 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
			class Backpack {
				backpack    = "rhsusf_falconii";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"rhs_mag_mk84", 1}, {"lerca_1200_tan", 1}};
        }; 
    };
};

class RHS_Iraq_RECON_Rifleman : RHS_USA_W_RECON_Rifleman {
	UNIFORM_RHS_IRAQ_SF
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"lerca_1200_tan", 1}};
        }; 
		class Variant2 : Variant2 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
			class Backpack {};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"rhs_mag_mk84", 1}, {"lerca_1200_tan", 1}};
        }; 
    };
};



class RHS_Iraq_RECON_TeamLeader : RHS_Iraq_RECON_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
        };  
		class Variant2 : Variant2 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
			class Backpack {};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 2}, {"rhs_mag_mk84", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
        }; 
    };
};

class RHS_Iraq_RECON_Engineer : RHS_USA_W_RECON_Engineer {
	UNIFORM_RHS_IRAQ_SF
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
			class Backpack : Backpack{
				backpack    = "rhsusf_falconii";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"lerca_1200_tan", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
        };
		class Variant2 : Variant2 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"rhs_mag_mk84", 1}, {"lerca_1200_tan", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			class Backpack : Backpack{
				backpack    = "rhsusf_falconii";
			};
        }; 
		class Variant3 : Variant3 {
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_mk84", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
			class Backpack : Backpack{
				backpack    = "rhsusf_falconii";
			};
        };
    };
};

class RHS_Iraq_RECON_LAT : RHS_USA_W_RECON_LAT {
	UNIFORM_RHS_IRAQ_SF
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"lerca_1200_tan", 1}};
        }; 
		class Variant2 : Variant2 {
			displayName = "CQB";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle";
				bipod       = "rhsusf_acc_grip3";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"rhs_mag_mk84", 1}, {"lerca_1200_tan", 1}};
			class Backpack {};
        }; 
    };
};

class RHS_Iraq_Crewman : RHS_Iraq_Default {
	ROLE_CREWMAN
	class Clothing {
		uniform   = "rhsgref_uniform_3color_desert_olive";
		headgear  = "rhs_tsh4";
		goggles   = "";
		vest      = "V_Rangemaster_belt";
	};
	scope = 2;
	class Variants : Variants {
		class Crewman : Variant1 {
			displayName = "Crewman";
			class Primary {
				weapon      = "rhs_weap_m4_carryhandle";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 2}};
			};
			class Pistol {
				weapon      = "";
				magazines[] = {};
			};
		items[]       = {{"rhs_mag_m18_purple", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "rhsgref_uniform_ERDL";
                headgear  = "rhsusf_hgu56p";
                goggles   = "";
                vest      = "V_PlateCarrier1_blk";
            };
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
			class Pistol {
				weapon      = "rhsusf_weap_m9";
				magazines[] = {{"rhsusf_mag_15Rnd_9x19_FMJ", 3}};
			};
			class Backpack {
                backpack = "B_Parachute";
				content[] = {};
			};	
		items[]       = {{"itemGPS", 1}, {"rhsusf_ANPVS_15", 1}};
        };
	};
};

class RHS_Iraq_HMGLeader : RHS_Iraq_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class RHS_Iraq_HMGAmmo : RHS_Iraq_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};

class RHS_Iraq_UAVOperator : RHS_Iraq_Default {
	scope = 2;
	UAV_OPERATOR

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Backpack {
				backpack    = "B_UAV_01_backpack_F";
			};
		items[]       = {{"B_UavTerminal", 1}, {"SmokeShell", 2}, {"lerca_1200_tan", 1}};
		itemshidden[] = {{"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}, COMMON_FIRSTAID};
		};
	};
};