// RHS - USA Woodland

class RHS_USA_W_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "rhs_uniform_cu_ocp";
		headgear  = "rhsusf_ach_helmet_ocp";
		goggles   = "rhs_googles_black";
		vest      = "rhsusf_iotv_ocp_Rifleman";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "rhs_weap_m4a1_blockII_bk";
				rail        = "";
				optics      = "rhsusf_acc_compm4";
				muzzle      = "";
				bipod       = "rhsusf_acc_grip3";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}};
			};

			class Pistol {
				weapon      = "rhsusf_weap_m9";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhsusf_mag_15Rnd_9x19_FMJ", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "rhsusf_assault_eagleaiii_ocp";
			};
		};
	};
};

class RHS_USA_W_Rifleman : RHS_USA_W_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG2_USMC_3d";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"Binocular", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2 :Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				optics      = "rhsusf_acc_compm4";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}};
			};
			items[]       = {COMMON_RIFLEMAN_NVG};
		};
	};
};

class RHS_USA_W_SquadLeader : RHS_USA_W_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG_3d";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}, {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 1}};
			};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2: Variant1 {
        displayName = "CQB";
			class Primary : Primary {
				optics      = "rhsusf_acc_eotech_xps3";
			};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 2}, {"itemGPS", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USA_W_TeamLeader_MG : RHS_USA_W_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"rhsusf_100Rnd_762x51", 5}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};

class RHS_USA_W_TeamLeader_AA : RHS_USA_W_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"rhs_fim92_mag", 1}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};

class RHS_USA_W_TeamLeader_HAT : RHS_USA_W_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"rhs_mag_smaw_HEDP", 1}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};

		};
	};
};

class RHS_USA_W_Medic : RHS_USA_W_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USA_W_AR : RHS_USA_W_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M249";

			class Primary : Primary {
				weapon      = "rhs_weap_m249_pip_L";
				magazines[] = {{"rhs_200rnd_556x45_M_SAW", 3}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
		class Variant2 : Variant1 {
			displayName = "M240G";

			class Primary : Primary {
				weapon      = "rhs_weap_m240G";
				magazines[] = {{"rhsusf_100Rnd_762x51_m61_ap", 4}};
			};
		};
	};
};

class RHS_USA_W_MG : RHS_USA_W_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_m240G";
				rail        = "";
				optics      = "rhsusf_acc_elcan_ard_3d";
				magazines[] = {{"rhsusf_100Rnd_762x51_m61_ap", 3},{"rhsusf_100Rnd_762x51_m62_tracer", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USA_W_Grenadier : RHS_USA_W_Default {
	scope = 2;
	ROLE_GRENADIER
    
	class Clothing : Clothing {
		vest      = "rhsusf_iotv_ocp_Grenadier";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M203";

			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_carryhandle_m203";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}, {"1Rnd_HE_Grenade_shell", 8}};
			};
			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}, {"rhs_mag_M433_HEDP", 2}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USA_W_LAT : RHS_USA_W_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HEAT";

			class Secondary {
				weapon      = "rhs_weap_M136";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_m136_mag", 1, 1}};
			};

			class Backpack {
				backpack = "";
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USA_W_HAT : RHS_USA_W_Default {
	scope = 0;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "rhs_weap_smaw_green";
				muzzle      = "";
				rail        = "";
				optics      = "rhs_weap_optic_smaw";
				bipod       = "";
				magazines[] = {{"rhs_mag_smaw_HEDP", 1, 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_mag_smaw_HEAA", 1}, {"rhs_mag_smaw_SR", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USA_W_AA : RHS_USA_W_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "rhs_weap_fim92";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_fim92_mag", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_fim92_mag", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USA_W_Marksman : RHS_USA_W_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M14";

			class Primary : Primary {
				weapon      = "rhs_weap_m14ebrri";
				optics      = "rhsusf_acc_ACOG_MDO";
				bipod       = "bipod_01_F_blk";
				magazines[] = {{"rhsusf_20Rnd_762x51_m118_special_Mag", 6}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};

		class Variant2: Variant1 {
            displayName = "SR25";
            class Primary {
                weapon      = "rhs_weap_sr25";
				optics      = "rhsusf_acc_ACOG_MDO";
                bipod       = "rhsusf_acc_harris_bipod";
                magazines[] = {{"rhsusf_20Rnd_762x51_m118_special_Mag", 6}};
            };

        };
	};
};

class RHS_USA_W_Engineer : RHS_USA_W_Default {
    scope = 2;
    ROLE_ENGINEER

    class Variants : Variants {
         class Variant1: Variant2 {
            displayName = "Demo Charges";
			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
        };
		class Variant2: Variant1 {
            displayName = "Claymores";
			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
        };
		class Variant3 : Variant1 {
            displayName = "Breacher";
            class Primary {
                weapon      = "rhs_weap_M590_5RD";
                rail        = "";
                optics      = "";
                muzzle      = "";
                bipod       = "";
                magazines[] = {{"rhsusf_5Rnd_Slug", 4}, {"rhsusf_5Rnd_00Buck", 4}};
            };
            class Backpack : Backpack {
                content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
            };
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_mk84", 2}, {"rhsusf_ANPVS_15", 1}};
        };
    };
};

class RHS_USA_W_Spotter : RHS_USA_W_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_B_FullGhillie_sard";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_blockII_grip2_KAC_d";
				muzzle      = "rhsusf_acc_nt4_tan";
				bipod       = "rhsusf_acc_grip2";
			};
			items[]       = {{"SmokeShell", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USA_W_Sniper : RHS_USA_W_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M24";
			class Primary {
				weapon      = "rhs_weap_m24sws_ghillie";
				muzzle      = "";
				rail        = "";
				optics      = "rhsusf_acc_M8541_low_d";
				bipod       = "rhsusf_acc_harris_swivel";
				magazines[] = {{"rhsusf_5Rnd_762x51_m993_Mag", 8}};
			};
			items[]       = {{"Leupold_Mk4", 1}, {"rhsusf_ANPVS_15", 1}};
		};

		class Variant2 : Variant1 {
			displayName = "M2010 ESR";
			class Primary {
				weapon      = "rhs_weap_XM2010_d";
				muzzle      = "";
				rail        = "";
				optics      = "rhsusf_acc_M8541_low_d";
				bipod       = "rhsusf_acc_harris_swivel";
				magazines[] = {{"rhsusf_5Rnd_300winmag_xm2010", 8}};
			};
		};
	};
};

class RHS_USA_W_RECON_Default : RHS_USA_W_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

		class Clothing {
		uniform   = "rhs_uniform_g3_mc";
		headgear  = "rhsusf_opscore_mc_cover_pelt_nsw";
		goggles   = "rhs_googles_orange";
		vest      = "rhsusf_mbav_rifleman";
		};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M4 (Long Range)";
			class Primary {
				weapon      = "rhs_weap_m4a1_blockII_grip2_KAC_d";
				muzzle      = "";
				rail        = "rhsusf_acc_anpeq15_bk";
				optics      = "rhsusf_acc_g33_xps3";
				bipod       = "rhsusf_acc_grip2";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}};
			};

			class Pistol {
				weapon      = "rhsusf_weap_glock17g4";
				muzzle      = "rhsusf_acc_omega9k";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhsusf_mag_17Rnd_9x19_JHP", 3}};
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};

		class Variant2 : Variant1 {
			displayName = "HK416 (CQB, Para)";
			class Primary : Primary {
				weapon      = "rhs_weap_hk416d10_LMT_d";
				optics      = "rhsusf_acc_eotech_552_d";
				muzzle      = "rhsusf_acc_nt4_tan";
			};
			class Backpack {
				backpack    = "B_Parachute";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"rhs_mag_mk84", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USA_W_RECON_Rifleman : RHS_USA_W_RECON_Default {
	scope = 2;
};

class RHS_USA_W_RECON_TeamLeader : RHS_USA_W_RECON_Rifleman {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants: Variants {
		class Variant1 : Variant1 {
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2 : Variant2 {
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 2}, {"rhs_mag_mk84", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USA_W_RECON_Engineer : RHS_USA_W_RECON_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

    class Variants: Variants {
		class Variant1: Variant1 {
            displayName = "Claymores (Long Range)";
			class Backpack {
				backpack    = "rhsusf_assault_eagleaiii_ocp";
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
        };
         class Variant2: Variant2 {
            displayName = "Demo Charges (CQB, Para)";
			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
        };
		class Variant3 : Variant2 {
            displayName = "Breacher";
            class Primary {
                weapon      = "rhs_weap_M590_8RD";
                rail        = "";
                optics      = "";
                muzzle      = "";
                bipod       = "";
                magazines[] = {{"rhsusf_8Rnd_Slug", 4}, {"rhsusf_8Rnd_00Buck", 4}};
            };
            class Backpack : Backpack {
                content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
            };
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_mk84", 4}, {"rhsusf_ANPVS_15", 1}};
        };
    };
};

class RHS_USA_W_RECON_LAT : RHS_USA_W_RECON_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Secondary {
				weapon      = "rhs_weap_M136";
				magazines[] = {{"rhs_m136_mag", 1, 1}};
			};
		};
		class Variant2 : Variant2 {
			class Secondary {
				weapon      = "rhs_weap_M136";
				magazines[] = {{"rhs_m136_mag", 1, 1}};
			};
		};
	};
};

class RHS_USA_W_Crewman : RHS_USA_W_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing {
		uniform   = "rhs_uniform_cu_ocp";
		headgear  = "rhsusf_cvc_green_ess";
		goggles   = "";
		vest      = "rhsusf_spc_crewman";
	};

	class Variants : Variants {
		class Crewman : Variant1 {
			displayName = "Crewman";
			class Primary {
				weapon      = "rhs_weap_m4_carryhandle";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 2}};
			};
			class Pistol {
				weapon      = "";
				magazines[] = {};
			};
		items[]       = {{"rhs_mag_m18_purple", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "U_B_HeliPilotCoveralls";
                headgear  = "H_PilotHelmetHeli_O";
                goggles   = "";
                vest      = "V_PlateCarrierSpec_rgr";
            };
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
			class Pistol {
				weapon      = "rhsusf_weap_m9";
				magazines[] = {{"rhsusf_mag_15Rnd_9x19_FMJ", 3}};
			};
			class Backpack {
                backpack = "B_Parachute";
				content[] = {};
			};	
		items[]       = {{"itemGPS", 1}, {"rhsusf_ANPVS_15", 1}};
        };
	};
};

class RHS_USA_W_HMGLeader : RHS_USA_W_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class RHS_USA_W_HMGAmmo : RHS_USA_W_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};

class RHS_USA_W_UAVOperator : RHS_USA_W_Default {
	scope = 2;
	UAV_OPERATOR

	class Variants : Variants {
		class Variant1 : Variant1 {

			class Backpack {
				backpack    = "B_UAV_01_backpack_F";
			};
		items[]       = {{"B_UavTerminal", 1}, {"SmokeShell", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		itemshidden[] = {{"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}, COMMON_FIRSTAID};
		};
	};
};
