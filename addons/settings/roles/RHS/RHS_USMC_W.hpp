// RHS - USMC

#define UNIFORM_RHS_USMC_W_SF class Clothing {\
		uniform   = "rhs_uniform_g3_rgr";\
		headgear  = "rhsusf_opscore_rg_cover_pelt";\
		goggles   = "rhs_googles_orange";\
		vest      = "rhsusf_mbav_rifleman";\
	};
    
#define UNIFORM_RHS_USMC_W class Clothing {\
		uniform   = "rhs_uniform_FROG01_wd";\
		headgear  = "rhsusf_lwh_helmet_marpatwd";\
		goggles   = "rhs_googles_black";\
		vest      = "rhsusf_spc_rifleman";\
	};

class RHS_USMC_W_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "rhs_uniform_FROG01_wd";
		headgear  = "rhsusf_lwh_helmet_marpatwd";
		goggles   = "rhs_googles_black";
		vest      = "rhsusf_spc_rifleman";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "rhs_weap_m4a1_blockII_bk";
				rail        = "";
				optics      = "rhsusf_acc_eotech_xps3";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 7}};
			};

			class Pistol {
				weapon      = "rhsusf_weap_m9";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhsusf_mag_15Rnd_9x19_FMJ", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "rhsusf_assault_eagleaiii_coy";
			};
		};
	};
};

class RHS_USMC_W_Rifleman : RHS_USMC_W_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG2_USMC_3d";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"Binocular", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2 :Variant1 {
			displayName = "CQB";
			class Primary : Primary {
				optics      = "rhsusf_acc_compm4";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}};
			};
			items[]       = {COMMON_RIFLEMAN_NVG};
		};
	};
};

class RHS_USMC_W_SquadLeader : RHS_USMC_W_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Clothing: Clothing {
		headgear  = "rhsusf_lwh_helmet_marpatwd_headset_blk";
		vest      = "rhsusf_spc_squadleader";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG_3d";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 6}, {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 1}};
			};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2: Variant1 {
        displayName = "CQB";
			class Primary : Primary {
				optics      = "rhsusf_acc_eotech_xps3";
			};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 2}, {"itemGPS", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USMC_W_TeamLeader_MG : RHS_USMC_W_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"rhsusf_100Rnd_762x51_m61_ap", 5}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};

class RHS_USMC_W_TeamLeader_AA : RHS_USMC_W_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"rhs_fim92_mag", 1}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};

class RHS_USMC_W_TeamLeader_HAT : RHS_USMC_W_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"rhs_mag_smaw_HEDP", 1}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};

class RHS_USMC_W_Medic : RHS_USMC_W_Default {
	scope = 2;
	ROLE_MEDIC

	class Clothing: Clothing {
		vest      = "rhsusf_spc_corpsman";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USMC_W_AR : RHS_USMC_W_Default {
	scope = 2;
	ROLE_AR

	class Clothing: Clothing {
		vest      = "rhsusf_spc_mg";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M249";
			class Primary : Primary {
				weapon      = "rhs_weap_m249_pip_L";
				magazines[] = {{"rhs_200rnd_556x45_M_SAW", 3}};
			};
		};
		class Variant2: Variant1 {
        displayName = "IAR";
			class Primary {
				weapon      = "rhs_weap_m27iar_grip";
				rail        = "";
				optics      = "rhsusf_acc_ACOG2_USMC_3d";
				muzzle      = "";
				bipod       = "rhsusf_acc_harris_bipod";
				magazines[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag", 10}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "M240G";
			class Primary : Primary {
				weapon      = "rhs_weap_m240G";
				magazines[] = {{"rhsusf_100Rnd_762x51_m61_ap", 4}};
			};
		};
	};
};

class RHS_USMC_W_MG : RHS_USMC_W_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_m240G";
				rail        = "";
				optics      = "rhsusf_acc_elcan_ard_3d";
				magazines[] = {{"rhsusf_100Rnd_762x51_m61_ap", 3},{"rhsusf_100Rnd_762x51_m62_tracer", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USMC_W_Grenadier : RHS_USA_W_Grenadier {
	class Clothing {
		uniform   = "rhs_uniform_FROG01_wd";
		headgear  = "rhsusf_lwh_helmet_marpatwd";
		goggles   = "rhs_googles_black";
		vest      = "rhsusf_spc_teamleader";
	};
};

class RHS_USMC_W_LAT : RHS_USMC_W_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HEAT";

			class Secondary {
				weapon      = "rhs_weap_M136";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_m136_mag", 1, 1}};
			};

			class Backpack {
				backpack = "";
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USMC_W_HAT : RHS_USMC_W_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "rhs_weap_smaw";
				muzzle      = "";
				rail        = "";
				optics      = "rhs_weap_optic_smaw";
				bipod       = "";
				magazines[] = {{"rhs_mag_smaw_HEDP", 1, 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_mag_smaw_HEAA", 1},{"rhs_mag_smaw_SR", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USMC_W_AA : RHS_USMC_W_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "rhs_weap_fim92";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_fim92_mag", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_fim92_mag", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_USMC_W_Marksman : RHS_USMC_W_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Clothing: Clothing {
		headgear  = "rhs_booniehat2_marpatwd";
		vest      = "rhsusf_spc_marksman";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M14";

			class Primary : Primary {
				weapon      = "rhs_weap_m14ebrri";
				optics      = "rhsusf_acc_ACOG_MDO";
				bipod       = "bipod_01_F_blk";
				magazines[] = {{"rhsusf_20Rnd_762x51_m118_special_Mag", 6}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};

		class Variant2: Variant1 {
            displayName = "SR25";
            class Primary {
                weapon      = "rhs_weap_sr25";
				optics      = "rhsusf_acc_ACOG_MDO";
                bipod       = "rhsusf_acc_harris_bipod";
                magazines[] = {{"rhsusf_20Rnd_762x51_m118_special_Mag", 6}};
            };

        };
	};
};

class RHS_USMC_W_Engineer : RHS_USMC_W_Default {
	scope = 2;
	ROLE_ENGINEER

    class Variants : Variants {
         class Variant1: Variant2 {
            displayName = "Demo Charges";
			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
        };
		class Variant2: Variant1 {
            displayName = "Claymores";
			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
        };
		class Variant3 : Variant1 {
            displayName = "Breacher";
            class Primary {
                weapon      = "rhs_weap_M590_5RD";
                rail        = "";
                optics      = "";
                muzzle      = "";
                bipod       = "";
                magazines[] = {{"rhsusf_5Rnd_Slug", 4}, {"rhsusf_5Rnd_00Buck", 4}};
            };
            class Backpack : Backpack {
                content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
            };
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_mk84", 2}, {"rhsusf_ANPVS_15", 1}};
        };
    };
};

class RHS_USMC_W_Spotter : RHS_USMC_W_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_B_FullGhillie_lsh";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
			class Primary : Primary {
				weapon      = "rhs_weap_m4a1_blockII_grip2_KAC_d";
				muzzle      = "rhsusf_acc_nt4_tan";
				bipod       = "rhsusf_acc_grip2";
			};
			items[]       = {{"SmokeShell", 1}, {"lerca_1200_tan", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_USMC_W_Sniper : RHS_USMC_W_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M24";
			class Primary {
				weapon      = "rhs_weap_m24sws_ghillie";
				muzzle      = "";
				rail        = "";
				optics      = "rhsusf_acc_M8541_low_wd";
				bipod       = "rhsusf_acc_harris_swivel";
				magazines[] = {{"rhsusf_5Rnd_762x51_m993_Mag", 8}};
			};
			items[]       = {{"Leupold_Mk4", 1}, {"rhsusf_ANPVS_15", 1}};
		};

		class Variant2 : Variant1 {
			displayName = "M40";
			class Primary {
				weapon      = "rhs_weap_m40a5_wd";
				muzzle      = "";
				rail        = "";
				optics      = "rhsusf_acc_M8541_low_wd";
				bipod       = "rhsusf_acc_harris_swivel";
				magazines[] = {{"rhsusf_10Rnd_762x51_m993_Mag", 8}};
			};
		};
	};
};

class RHS_USMC_W_RECON_Default : RHS_USA_W_RECON_Default {
	UNIFORM_RHS_USMC_W_SF
};

class RHS_USMC_W_RECON_Rifleman : RHS_USA_W_RECON_Rifleman {
	UNIFORM_RHS_USMC_W_SF
};

class RHS_USMC_W_RECON_TeamLeader : RHS_USA_W_RECON_TeamLeader {
	UNIFORM_RHS_USMC_W_SF
};

class RHS_USMC_W_RECON_Engineer : RHS_USA_W_RECON_Engineer {
	UNIFORM_RHS_USMC_W_SF
};

class RHS_USMC_W_RECON_LAT : RHS_USA_W_RECON_LAT {
	UNIFORM_RHS_USMC_W_SF
};

class RHS_USMC_W_Crewman : RHS_USA_W_Crewman {
	class Variants : Variants {
		class Crewman : Crewman {
            class Clothing : Clothing {
                uniform   = "rhs_uniform_FROG01_wd";
            };
        };
        class Pilot: Pilot {};
    };
};

class RHS_USMC_W_HMGLeader : RHS_USA_W_HMGLeader {
    UNIFORM_RHS_USMC_W
};

class RHS_USMC_W_HMGAmmo : RHS_USA_W_HMGAmmo {
    UNIFORM_RHS_USMC_W
};

class RHS_USMC_W_UAVOperator : RHS_USA_W_UAVOperator {
    UNIFORM_RHS_USMC_W
};
