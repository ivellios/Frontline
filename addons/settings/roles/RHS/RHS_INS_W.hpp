// RHS - INS

class RHS_INS_W_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "rhsgref_uniform_ttsko_forest";
		headgear  = "rhs_beanie_green";
		goggles   = "G_Bandanna_oli";
		vest      = "rhsgref_TacVest_ERDL";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "rhs_weap_akm";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"rhs_30Rnd_762x39mm", 5}};
			};

			class Pistol {
				weapon      = "rhs_weap_makarov_pm";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_9x18_8_57N181S", 3}};
			};

			class Backpack {
				backpack    = "";
			};

			items[]       = {COMMON_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_Carryall_khk";
			};
		};
	};
};

class RHS_INS_W_Rifleman : RHS_INS_W_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "7.62";
			class Primary : Primary {
				weapon      = "rhs_weap_akm";
				magazines[] = {{"rhs_30Rnd_762x39mm", 5}};
			};
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 1}, {"Binocular", 1}};
		};
		class Variant2: Variant1 {
            displayName = "5.45";
            class Primary {
				weapon      = "rhs_weap_ak74m_2mag_camo";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 8}};
			};
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 1}, {"Binocular", 1}};
		};
		class Variant3: Variant1 {
            displayName = "CQB";
            class Primary {
				weapon      = "rhs_weap_Izh18";
				magazines[] = {{"rhsgref_1Rnd_00Buck", 20}, {"rhsgref_1Rnd_Slug", 20}};
			};
			class Pistol {
				weapon      = "rhs_weap_pp2000_folded";
				magazines[] = {{"rhs_mag_9x19mm_7n31_44", 5}};
			};
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 2}, {"Binocular", 1}};
		};
		class Variant4: Variant1 {
            displayName = "Suppressed";
            class Primary {
				weapon      = "rhs_weap_mosin_sbr";
				muzzle      = "rhsgref_sdn6_suppressor";
				optics      = "rhs_acc_rakursPM";
				magazines[] = {{"rhsgref_5Rnd_762x54_m38", 10}};
			};
			class Pistol {
				weapon      = "rhs_weap_pb_6p9";
				muzzle      = "rhs_acc_6p9_suppressor";
				magazines[] = {{"rhs_mag_9x18_8_57N181S", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 2}, {"Binocular", 1}};
		};
		class Variant5: Variant1 {
			displayName = "Precision Rifle";
			class Primary {
				weapon      = "rhs_weap_kar98k";
				magazines[] = {{"rhsgref_5Rnd_792x57_kar98k", 10}};
			};
			class Pistol {
				weapon      = "rhs_weap_tt33";
				magazines[] = {{"rhs_mag_762x25_8", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 2}, {"Binocular", 1}};
		};
	};
};


class RHS_INS_W_SquadLeader : RHS_INS_W_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Clothing {
		uniform   = "rhsgref_uniform_flecktarn";
		headgear  = "rhsusf_Bowman";
		vest      = "rhsgref_TacVest_ERDL";
		goggles   = "G_Balaclava_blk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "CQB";

			class Primary {
				weapon      = "rhs_weap_ak74m_zenitco01_b33";
				optics      = "rhs_acc_rakursPM";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 8}};
			};

			class Pistol {
				weapon      = "rhsusf_weap_glock17g4";
				magazines[] = {{"rhsusf_mag_17Rnd_9x19_JHP", 3}};
			};

			items[]       = {COMMON_LEADER};
		};
		class Variant2: Variant1 {
            displayName = "Long Range";
            class Primary {
				weapon      = "rhs_weap_ak74m_2mag";
				optics      = "rhs_acc_1p78_3d";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 8}};
			};

			class Pistol {
				weapon      = "rhs_weap_tt33";
				magazines[] = {{"rhs_mag_762x25_8", 3}};
			};
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 2}, {"itemGPS", 1}, {"Rangefinder", 1}};
        };
	};
};


class RHS_INS_W_TeamLeader_MG : RHS_INS_W_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"rhs_100Rnd_762x54mmR", 5}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};


class RHS_INS_W_TeamLeader_AA : RHS_INS_W_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};

class RHS_INS_W_TeamLeader_HAT : RHS_INS_W_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"rhs_rpg7_PG7VL_mag", 1}};
			};
			items[] = {COMMON_TEAMLEADER};
		};
	};
};

class RHS_INS_W_Medic : RHS_INS_W_Default {
	scope = 2;
	ROLE_MEDIC

		class Clothing {
			uniform   = "U_BG_Guerilla2_3";
			headgear  = "H_Bandanna_khk";
			goggles   = "";
			vest      = "rhsgref_TacVest_ERDL";
		};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Primary {
				weapon      = "rhs_weap_aks74u";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"rhs_mag_f1", 1}};
		};

		class Variant2 : Variant1 {
			displayName = "str_b_medic_f0";

			class Primary {
				weapon      = "rhs_weap_savz58p";
				magazines[] = {{"rhs_30Rnd_762x39mm_Savz58", 6}};
			};

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
		};
	};
};

class RHS_INS_W_AR : RHS_INS_W_Default {
	scope = 2;
	ROLE_AR

	class Clothing {
		uniform   = "rhsgref_uniform_tigerstripe";
		headgear  = "H_Watchcap_camo";
		goggles   = "";
		vest      = "rhs_6b23_vydra_3m";
	};
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";
			class Backpack {
				backpack    = "B_Kitbag_rgr";
			};
			class Primary {
				weapon      = "rhs_weap_pkm";
				magazines[] = {{"rhs_100Rnd_762x54mmR_7N26", 5}};
			};
			items[]       = {COMMON_SUPPORT};
		};
		class Variant2: Variant1 {
			displayName = "AK";
			class Backpack {
				backpack    = "B_Kitbag_rgr";
			};
			class Primary {
				weapon      = "rhs_weap_ak74m_2mag";
				magazines[] = {{"rhs_45Rnd_545X39_7N22_AK", 10}};
				rail        = "";
				optics      = "rhs_acc_1p78_3d";
				muzzle      = "rhs_acc_dtk2";
				bipod       = "";
			};
        };
	};
};

class RHS_INS_W_MG : RHS_INS_W_Default {
	scope = 2;
	ROLE_MG

	class Clothing {
		uniform   = "rhsgref_uniform_tigerstripe";
		headgear  = "H_Watchcap_camo";
		goggles   = "";
		vest      = "rhs_6b23_vydra_3m";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_pkp";
				optics      = "rhs_acc_1p78_3d";
				magazines[] = {{"rhs_100Rnd_762x54mmR", 5}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_INS_W_Grenadier : RHS_INS_W_Default {
	scope = 2;
	ROLE_GRENADIER

	class Clothing {
		uniform   = "rhsgref_uniform_woodland_olive";
		headgear  = "H_Bandanna_khk";
		vest      = "V_HarnessOGL_brn";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "GL";

			class Primary : Primary {
				weapon      = "rhs_weap_ak74m_fullplum_gp25";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}, {"rhs_VOG25", 8}};
			};
			class Backpack : Backpack{
				content[] = {{"rhs_VG40OP_white", 4}, {"rhs_GRD40_White", 4}};
			};
			items[]       = {COMMON_SUPPORT};
		};

		class Variant2 : Variant2 {
			displayName = "RPG HE";

			class Primary : Primary {
				weapon      = "rhs_weap_aks74u";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			class Secondary {
				weapon      = "rhs_weap_rshg2";
				magazines[] = {{"rhs_rshg2_mag", 1}};
			};
			items[]       = {{"SmokeShell", 5}, {"rhs_mag_rgn", 5}};
		};
	};
};

class RHS_INS_W_LAT : RHS_INS_W_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "RPG 26";
			class Secondary {
				weapon      = "rhs_weap_rpg26";
				magazines[] = {{"rhs_rpg26_mag", 1}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_INS_W_HAT : RHS_INS_W_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_aks74u";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};

			class Secondary {
				weapon      = "rhs_weap_rpg7";
				optics      = "rhs_acc_pgo7v3";
				magazines[] = {{"rhs_rpg7_PG7V_mag", 3}};
			};

			class Backpack : Backpack {
				content[] = {};
			};
		};
	};
};

class RHS_INS_W_AA : RHS_INS_W_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "rhs_weap_igla";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_9k38_rocket", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
		};
	};
};

class RHS_INS_W_Marksman : RHS_INS_W_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Clothing {
		uniform   = "U_BG_Guerrilla_6_1";
		headgear  = "H_Booniehat_oli";
		vest      = "V_BandollierB_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SVD";

			class Primary : Primary {
				weapon      = "rhs_weap_svdp_wd";
				optics      = "rhs_acc_pso1m2";
				magazines[] = {{"rhs_10Rnd_762x54mmR_7N1", 8}};
			};
			items[]       = {COMMON_SUPPORT};
		};

		class Variant2 : Variant1 {
			displayName = "Zastava m21";

			class Primary : Primary {
				weapon      = "rhs_weap_m21a";
				optics      = "rhs_acc_pso1m2";
				magazines[] = {{"rhsgref_30rnd_556x45_m21", 8}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_INS_W_Engineer : RHS_INS_W_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Breacher";

			class Primary {
				weapon      = "rhs_weap_M590_5RD";
				magazines[] = {{"rhsusf_5Rnd_Slug", 4}, {"rhsusf_5Rnd_00Buck", 4}};
			};
			class Backpack : Backpack {
				content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_f1", 2}};
		};
		class Variant2: Variant1 {
            displayName = "IED";
            class Primary {
                weapon      = "rhs_weap_aks74u";
                magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
            };
			class Backpack : Backpack {
				content[]   = {{"IEDLandBig_Remote_Mag", 1},{"IEDUrbanBig_Remote_Mag", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_f1", 1}};
		};
		class Variant3: Variant2 {
            displayName = "Claymores";
			class Primary {
				weapon      = "rhs_weap_savz61";
				magazines[] = {{"rhsgref_20rnd_765x17_vz61", 8}};
			};
			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_f1", 1}};
        };
	};
};

class RHS_INS_W_Spotter : RHS_INS_W_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_BG_Guerrilla_6_1";
		headgear  = "H_Booniehat_oli";
		vest      = "V_BandollierB_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
		items[]       = {{"SmokeShell", 1}, {"rhs_pdu4", 1}};
		};
	};
};

class RHS_INS_W_Sniper : RHS_INS_W_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Clothing {
		uniform   = "U_BG_Guerrilla_6_1";
		headgear  = "rhs_Booniehat_flora";
		goggles   = "G_Aviator";
		vest      = "V_BandollierB_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "rhs_weap_m38_rail";
				optics      = "rhs_acc_dh520x56";
				magazines[] = {{"rhsgref_5Rnd_762x54_m38", 8}};
			};
			items[]       = {COMMON_SUPPORT};
		};
	};
};

class RHS_INS_W_FF_Default : RHS_INS_W_Default {
	scope = 0;
	ROLE_FF_RIFLEMAN

		class Clothing {
		uniform   = "rhsgref_uniform_dpm_olive";
		headgear  = "H_Cap_blk";
		goggles   = "G_Bandanna_beast";
		vest      = "V_TacVest_oli";
		};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M21 (Long Range)";
			class Primary : Primary {
				weapon      = "rhs_weap_m21s_pr";
				optics      = "optic_MRCO";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"rhsgref_30rnd_556x45_m21", 6}};
			};
			class Pistol {
				weapon      = "rhsusf_weap_glock17g4";
				muzzle      = "rhsusf_acc_omega9k";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhsusf_mag_17Rnd_9x19_JHP", 3}};
			};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 1}, {"lerca_1200_tan", 1}};
		};

		class Variant2 : Variant1 {
			displayName = "AKM (CQB)";
			class Primary {
				weapon      = "rhs_weap_akm_zenitco01_b33";
				muzzle      = "rhs_acc_pbs1";
				rail        = "";
				optics      = "rhsusf_acc_RM05";
				bipod       = "rhsusf_acc_grip2_tan";
				magazines[] = {{"rhs_30Rnd_762x39mm", 6}};
			};
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 2}, {"lerca_1200_tan", 1}};
		};
	};
};

class RHS_INS_W_FF_Rifleman : RHS_INS_W_FF_Default {
	scope = 2;
};

class RHS_INS_W_FF_TeamLeader : RHS_INS_W_FF_Rifleman {
	scope = 2;
	ROLE_FF_TEAMLEADER

		class Clothing :Clothing {
		uniform   = "rhsgref_uniform_olive";
		headgear  = "H_Cap_oli_hs";
		goggles   = "rhsusf_shemagh2_grn";
		};

	class Variants: Variants {
		class Variant1 : Variant1 {
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"rhs_mag_f1", 1}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
		};
		class Variant2 : Variant2 {
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"rhs_mag_f1", 2}, {"itemGPS", 1}, {"lerca_1200_tan", 1}};
		};
	};
};

class RHS_INS_W_FF_Engineer : RHS_INS_W_FF_Default {
	scope = 2;
	ROLE_FF_ENGINEER

		class Clothing :Clothing {
		uniform   = "rhsgref_uniform_dpm_olive";
		headgear  = "H_Bandanna_gry";
		goggles   = "G_Bandanna_beast";
		};

    class Variants: Variants {
		class Variant1: Variant2 {
            displayName = "Claymores (CQB)";
            class Primary {
                weapon      = "rhsusf_weap_MP7A2_desert";
                rail        = "";
                optics      = "rhsusf_acc_RM05";
                muzzle      = "rhsusf_acc_rotex_mp7_desert";
                bipod       = "rhsusf_acc_grip3";
                magazines[] = {{"rhsusf_mag_40Rnd_46x30_FMJ", 8}};
            };
			class Backpack {
				backpack    = "B_Kitbag_rgr";
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4}, {"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
        };
         class Variant2: Variant1 {
            displayName = "IEDs (Long Range)";
			class Primary : Primary {
				weapon      = "rhs_weap_m21s_pr";
				optics      = "optic_MRCO";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"rhsgref_30rnd_556x45_m21", 6}};
			};
			class Backpack : Backpack {
				backpack    = "B_Kitbag_rgr";
				content[]   = {{"IEDLandBig_Remote_Mag", 2}, {"IEDUrbanBig_Remote_Mag", 1}, {"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
        };
		class Variant3 : Variant1 {
            displayName = "Breacher";
            class Primary {
                weapon      = "rhs_weap_M590_8RD";
                rail        = "";
                optics      = "";
                muzzle      = "";
                bipod       = "";
                magazines[] = {{"rhsusf_8Rnd_Slug", 4}, {"rhsusf_8Rnd_00Buck", 4}};
            };
			class Pistol {
				weapon      = "rhs_weap_savz61_folded";
				magazines[] = {{"rhsgref_20rnd_765x17_vz61", 6}};
            };
            class Backpack : Backpack {
                content[]   = {{"FRL_BreachingCharge_Wpn", 5}, { "FRL_ExplosiveCharge_Wpn", 1}};
            };
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_f1", 4}};
        };
    };
};

class RHS_INS_W_FF_LAT : RHS_INS_W_FF_Default {
	scope = 2;
	ROLE_FF_LAT

		class Clothing :Clothing {
		uniform   = "rhsgref_uniform_dpm_olive";
		headgear  = "H_Watchcap_blk";
		goggles   = "G_Aviator";
		};

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Secondary {
				weapon      = "rhs_weap_rpg7";
				optics      = "rhs_acc_pgo7v3";
				magazines[] = {{"rhs_rpg7_PG7V_mag", 3}};
			};
			class Backpack {
                backpack = "B_Kitbag_rgr";
				content[] = {};
			};
		};
		class Variant2 : Variant2 {
			class Secondary {
				weapon      = "rhs_weap_rpg7";
				optics      = "rhs_acc_pgo7v3";
				magazines[] = {{"rhs_rpg7_PG7V_mag", 3}};
			};
			class Backpack {
                backpack = "B_Kitbag_rgr";
				content[] = {};
			};
		};
	};
};

class RHS_INS_W_Crewman : RHS_INS_W_Default {
	ROLE_CREWMAN
	scope = 2;

	class Variants : Variants {
		class Crewman : Variant1 {
			displayName = "Crewman";
			class Primary {
				weapon      = "rhs_weap_aks74u";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 2}};
			};
			class Pistol {
				weapon      = "";
				magazines[] = {};
			};
			items[]       = {{"SmokeShell", 1}, {"itemGPS", 1}, {"rhs_pdu4", 1}};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
			class Pistol {
				weapon      = "rhs_weap_tt33";
				magazines[] = {{"rhs_mag_762x25_8", 3}};
			};
			class Backpack {
                backpack = "rhs_d6_Parachute_backpack";
				content[] = {};
			};
		items[]       = {{"itemGPS", 1}};
        };
	};
};

class RHS_INS_W_HMGLeader : RHS_INS_W_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class RHS_INS_W_HMGAmmo : RHS_INS_W_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};

class RHS_INS_W_UAVOperator : RHS_INS_W_Default {
	scope = 2;
	UAV_OPERATOR

	class Variants : Variants {
		class Variant1 : Variant1 {

			class Backpack {
				backpack    = "O_UAV_01_backpack_F";
			};
		items[]       = {{"O_UavTerminal", 1}, {"SmokeShell", 1}, {"rhs_pdu4", 1}};
		itemshidden[] = {{"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}, COMMON_FIRSTAID};
		};
	};
};

class RHS_INS_W_Baserole {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "U_BG_Guerilla2_3";
		headgear  = "H_Bandanna_khk";
		goggles   = "";
		vest      = "";
	};

	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "rhs_weap_m38";
				magazines[] = {{"rhsgref_5Rnd_762x54_m38", 4}};
			};
			class Backpack {
                backpack = "B_FieldPack_khk";
				content[] = {};
			};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
	};
};
