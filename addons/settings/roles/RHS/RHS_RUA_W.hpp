// RHS - RUA Woodland

class RHS_RUA_W_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "rhs_uniform_emr_patchless";
		headgear  = "rhs_6b7_1m_emr";
		goggles   = "";
		vest      = "rhs_6b23_6sh116";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "rhs_weap_ak74m";
				rail        = "";
				optics      = "";
				muzzle      = "rhs_acc_dtk";
				bipod       = "";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};

			class Pistol {
				weapon      = "rhs_weap_pya";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_9x19_17", 3}};
			};

			items[]       = {COMMON_ITEMS_NVG};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "rhs_assault_umbts";
			};
		};
	};
};

class RHS_RUA_W_Rifleman : RHS_RUA_W_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {

			class Primary : Primary {
				optics      = "rhs_acc_1p78_3d";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"Binocular", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2: Variant1 {
            displayName = "CQB";
			class Primary : Primary {
				optics      = "";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			items[]       = {COMMON_RIFLEMAN_NVG};
		};
	};
};

class RHS_RUA_W_SquadLeader : RHS_RUA_W_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "rhs_acc_1p78_3d";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}, {"rhs_30Rnd_545x39_AK_green", 1}};
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"itemGPS", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2: Variant1 {
            displayName = "CQB";
			class Primary : Primary {
				optics      = "";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
        };
	};
};

class RHS_RUA_W_TeamLeader_MG : RHS_RUA_W_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"rhs_100Rnd_762x54mmR", 5}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};


class RHS_RUA_W_TeamLeader_AA : RHS_RUA_W_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};

class RHS_RUA_W_TeamLeader_HAT : RHS_RUA_W_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"rhs_rpg7_PG7VL_mag", 1}};
			};
			items[] = {COMMON_TEAMLEADER_NVG};
		};
	};
};

class RHS_RUA_W_Medic : RHS_RUA_W_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_RUA_W_AR : RHS_RUA_W_Default {
	scope = 2;
	ROLE_AR

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "PKP";

			class Primary : Primary {
				weapon      = "rhs_weap_pkp";
				rail        = "";
				optics      = "";
				magazines[] = {{"rhs_100Rnd_762x54mmR", 4}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
		class Variant2 : Variant1 {
			displayName = "RPK";

			class Primary : Primary {
				weapon      = "rhs_weap_ak74m";
				rail        = "";
				optics      = "rhs_acc_1p78_3d";
				muzzle      = "rhs_acc_dtk";
				bipod       = "";
				magazines[] = {{"rhs_45Rnd_545X39_7N22_AK", 10}};
			};
			items[]       = {COMMON_ITEMS_NVG};
		};
	};
};

class RHS_RUA_W_MG : RHS_RUA_W_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "PKP";

			class Primary : Primary {
				weapon      = "rhs_weap_pkp";
				rail        = "";
				optics      = "rhs_acc_1p78_3d";
				magazines[] = {{"rhs_100Rnd_762x54mmR", 5}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_RUA_W_Grenadier : RHS_RUA_W_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_ak74m_gp25";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}, {"rhs_VOG25", 8}};
			};
			class Backpack : Backpack{
				content[] = {{"rhs_VG40OP_white", 4}, {"rhs_GRD40_White", 4}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_RUA_W_LAT : RHS_RUA_W_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "rhs_weap_rpg26";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_rpg26_mag", 1, 1}};
			};

			class Backpack {
				backpack = "";
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_RUA_W_HAT : RHS_RUA_W_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "rhs_weap_rpg7";
				muzzle      = "";
				rail        = "";
				optics      = "rhs_acc_pgo7v3";
				bipod       = "";
				magazines[] = {{"rhs_rpg7_PG7V_mag", 2}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_rpg7_PG7V_mag", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_RUA_W_AA : RHS_RUA_W_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "rhs_weap_igla";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_9k38_rocket", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"rhs_mag_9k38_rocket", 1}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_RUA_W_Marksman : RHS_RUA_W_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "rhs_weap_svdp";
				muzzle      = "";
				rail        = "";
				optics      = "rhs_acc_pso1m2";//
				bipod       = "";
				magazines[] = {{"rhs_10Rnd_762x54mmR_7N1", 8}};
			};
			items[]       = {COMMON_SUPPORT_NVG};
		};
	};
};

class RHS_RUA_W_Engineer : RHS_RUA_W_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Breacher";

			class Primary {
				weapon      = "rhs_weap_M590_5RD";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"rhsusf_5Rnd_Slug", 4}, {"rhsusf_5Rnd_00Buck", 4}};
			};
			class Backpack : Backpack {
				content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"rhs_mag_zarya2", 2}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2: Variant1 {
            displayName = "Demo Charges";
            class Primary {
                weapon      = "rhs_weap_ak74m";
                rail        = "";
                optics      = "";
				muzzle      = "rhs_acc_dtk";
                bipod       = "";
                magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
            };
			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
        };
		class Variant3: Variant2 {
            displayName = "Claymores";
			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4},{"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"HandGrenade", 1}, {"rhsusf_ANPVS_15", 1}};
        };
	};
};

class RHS_RUA_W_Spotter : RHS_RUA_W_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_I_FullGhillie_lsh";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
			class Primary {
				weapon      = "rhs_weap_ak74m_zenitco01_b33";
				rail        = "";
				optics      = "rhs_acc_rakursPM";
				muzzle      = "rhs_acc_dtk4short";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 6}};
			};
			items[]       = {{"SmokeShell", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_RUA_W_Sniper : RHS_RUA_W_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "T5000";
			class Primary {
				weapon      = "rhs_weap_t5000";
				muzzle      = "";
				rail        = "";
				optics      = "rhs_acc_dh520x56";
				bipod       = "rhs_acc_harris_swivel";
				magazines[] = {{"rhs_5Rnd_338lapua_t5000", 8}};
			};
			items[]       = {{"Leupold_Mk4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2 : Variant1 {
			displayName = "SVD";

			class Primary : Primary {
				weapon      = "rhs_weap_svds";
				muzzle      = "";
				rail        = "";
				optics      = "rhs_acc_pso1m2";
				bipod       = "";
				magazines[] = {{"rhs_10Rnd_762x54mmR_7N1", 10}};
			};
		};
	};
};

class RHS_RUA_W_RECON_Default : RHS_RUA_W_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

		class Clothing {
		uniform   = "rhs_uniform_gorka_r_y";
		headgear  = "rhs_altyn_novisor_ess_bala";
		goggles   = "";
		vest      = "rhs_6b13_6sh92";
		};


	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "VSS Vintorez (Long Range)";
			class Primary {
				weapon      = "rhs_weap_vss_npz";
				muzzle      = "";
				rail        = "";
				optics      = "optic_DMS";
				bipod       = "";
				magazines[] = {{"rhs_20rnd_9x39mm_SP5", 9}};
			};
			class Pistol {
				weapon      = "rhs_weap_tr8";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2 : Variant1 {
			displayName = "AS VAL (CQB, Para)";
           class Primary {
				weapon      = "rhs_weap_asval_grip";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_20rnd_9x39mm_SP5", 9}};
            };
			class Backpack {
				backpack    = "rhs_d6_Parachute_backpack";
			};
		items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"rhs_mag_zarya2", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_RUA_W_RECON_Rifleman : RHS_RUA_W_Recon_Default {
	scope = 2;
};

class RHS_RUA_W_RECON_TeamLeader : RHS_RUA_W_RECON_Rifleman {
	scope = 2;
	ROLE_RECON_TEAMLEADER


    class Variants : Variants {
		class Variant1 : Variant1 {
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 1}, {"itemGPS", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
		class Variant2 : Variant2 {
		items[]       = {{"SmokeShell", 2}, {"rhs_mag_m18_purple", 1}, {"HandGrenade", 2}, {"rhs_mag_zarya2", 1}, {"itemGPS", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_RUA_W_RECON_LAT : RHS_RUA_W_RECON_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Secondary {
				weapon      = "rhs_weap_rpg26";
				magazines[] = {{"rhs_rpg26_mag", 1, 1}};
			};
		};
		class Variant2 : Variant2 {
			class Secondary {
				weapon      = "rhs_weap_rpg26";
				magazines[] = {{"rhs_rpg26_mag", 1, 1}};
			};
		};
	};
};

class RHS_RUA_W_RECON_Engineer : RHS_RUA_W_RECON_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants : Variants {
		class Variant1: Variant1 {
            displayName = "Claymores (Long Range)";
			class Backpack {
				backpack    = "rhs_assault_umbts";
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4}, {"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
        };
		class Variant2: Variant2 {
            displayName = "Demo Charges (CQB, Para)";
			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 1}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
        };
		class Variant3 : Variant1 {
			displayName = "Breacher";
			class Primary {
				weapon      = "rhs_weap_pp2000";
				rail        = "";
				optics      = "rhs_acc_ekp8_18";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"rhs_mag_9x19mm_7n31_44", 8}};
			};
			class Backpack : Backpack {
				backpack    = "rhs_assault_umbts";
				content[]   = {{"FRL_BreachingCharge_Wpn", 4}, { "FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"SmokeShell", 2}, {"rhs_mag_zarya2", 4}, {"rhsusf_ANPVS_15", 1}};
		};
	};
};

class RHS_RUA_W_Crewman : RHS_RUA_W_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing {
		uniform   = "rhs_uniform_m88_patchless";
		headgear  = "rhs_tsh4";
		goggles   = "rhs_scarf";
		vest      = "rhs_vest_commander";
	};
	class Variants : Variants {
		class Crewman : Variant1 {
			displayName = "Crewman";
			class Primary {
				weapon      = "rhs_weap_aks74un";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"rhs_30Rnd_545x39_AK", 2}};
			};
			class Pistol {
				weapon      = "";
				magazines[] = {};
			};
			items[]       = {{"itemGPS", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "rhs_uniform_df15_tan";
                headgear  = "rhs_zsh7a_mike_alt";
                goggles   = "";
                vest      = "rhs_vest_pistol_holster";
            };
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
			class Pistol {
				weapon      = "rhs_weap_pya";
				magazines[] = {{"rhs_mag_9x19_17", 3}};
			};
			class Backpack {
                backpack = "rhs_d6_Parachute_backpack";
				content[] = {};
			};	
		items[]       = {{"itemGPS", 1}, {"rhsusf_ANPVS_15", 1}};
        };
	};
};

class RHS_RUA_W_HMGLeader : RHS_RUA_W_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM_NVG};
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class RHS_RUA_W_HMGAmmo : RHS_RUA_W_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM_NVG};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};

class RHS_RUA_W_UAVOperator : RHS_RUA_W_Default {
	scope = 2;
	UAV_OPERATOR

	class Variants : Variants {
		class Variant1 : Variant1 {

			class Backpack {
				backpack    = "O_UAV_01_backpack_F";
			};
		items[]       = {{"O_UavTerminal", 1}, {"SmokeShell", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		itemshidden[] = {{"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}, COMMON_FIRSTAID};
		};
	};
};

class RHS_RUA_W_Blu_UAVOperator : RHS_RUA_W_UAVOperator {
	scope = 2;
	UAV_OPERATOR

	class Variants : Variants {
		class Variant1 : Variant1 {

			class Backpack {
				backpack    = "B_UAV_01_backpack_F";
			};
		items[]       = {{"B_UavTerminal", 1}, {"SmokeShell", 1}, {"rhs_pdu4", 1}, {"rhsusf_ANPVS_15", 1}};
		itemshidden[] = {{"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}, COMMON_FIRSTAID};
		};
	};
};