#define UNIFORM_RHS_AFP class Clothing {\
		uniform   = "rhsgref_uniform_dpm";\
		headgear  = "rhsusf_ach_bare";\
		goggles   = "";\
		vest      = "V_I_G_resistanceLeader_F";\
	};
    
#define UNIFORM_RHS_AFP_SF class Clothing {\
		uniform   = "rhsgref_uniform_tigerstripe";\
		headgear  = "rhsusf_opscore_mar_fg_pelt";\
		goggles   = "G_Bandanna_beast";\
		vest      = "V_I_G_resistanceLeader_F";\
	};

class RHS_AFP_Default : RHS_Iraq_Default {
    UNIFORM_RHS_AFP
};

class RHS_AFP_Rifleman : RHS_Iraq_Rifleman {
    UNIFORM_RHS_AFP
	class Variants : Variants {
		class Variant1 :Variant1 {
			displayName = "Long Range";
			class Primary : Primary {
				optics      = "rhsusf_acc_ACOG2_USMC_3d";
				magazines[] = {{"rhs_mag_30Rnd_556x45_Mk318_Stanag", 6}};
			};
			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 1}, {"Binocular", 1}};
		};
		class Variant2: Variant1 {
		displayName = "CQB";
			class Primary : Primary {
				weapon      = "rhs_weap_m4_carryhandle";
				optics      = "";
			};
			items[]       = {COMMON_RIFLEMAN};
		};
		class Variant3: Variant1 {
		displayName = "M14";
			class Primary : Primary {
				weapon      = "srifle_DMR_06_olive_F";
				optics      = "optic_Aco";
				magazines[] = {{"20Rnd_762x51_Mag", 9}};
			};
			items[]       = {COMMON_RIFLEMAN};
		};
	};
};

class RHS_AFP_SquadLeader : RHS_Iraq_SquadLeader {
    UNIFORM_RHS_AFP
};

class RHS_AFP_TeamLeader_MG : RHS_Iraq_TeamLeader_MG {
    UNIFORM_RHS_AFP
};

class RHS_AFP_TeamLeader_AA : RHS_Iraq_TeamLeader_AA {
    UNIFORM_RHS_AFP
};

class RHS_AFP_TeamLeader_HAT : RHS_Iraq_TeamLeader_HAT {
    UNIFORM_RHS_AFP
};

class RHS_AFP_Medic : RHS_Iraq_Medic {
    UNIFORM_RHS_AFP
};

class RHS_AFP_AR : RHS_Iraq_AR {
    UNIFORM_RHS_AFP
	class Variants : Variants {
		class Variant1 :Variant1 {};
    };
};


class RHS_AFP_MG : RHS_Iraq_MG {
    UNIFORM_RHS_AFP
};

class RHS_AFP_Grenadier : RHS_Iraq_Grenadier {
    UNIFORM_RHS_AFP
	class Variants : Variants {
		class Variant1 :Variant1 {};
    };
};

class RHS_AFP_LAT : RHS_Iraq_LAT {
    UNIFORM_RHS_AFP
};

class RHS_AFP_HAT : RHS_Iraq_HAT {
    UNIFORM_RHS_AFP
};

class RHS_AFP_AA : RHS_Iraq_AA {
    UNIFORM_RHS_AFP
};

class RHS_AFP_Marksman : RHS_Iraq_Marksman {
    UNIFORM_RHS_AFP
};

class RHS_AFP_Engineer : RHS_Iraq_Engineer {
    UNIFORM_RHS_AFP
	class Variants : Variants {
		class Variant1 :Variant1 {};
		class Variant2 :Variant2 {};
		class Variant3 :Variant3 {
            class Primary {
                weapon      = "rhs_weap_m4a1_carryhandle";
                magazines[] = {{"rhs_mag_30Rnd_556x45_Mk318_Stanag", 6}};
            };
        };
    };
};

class RHS_AFP_Spotter : RHS_Iraq_Spotter {
    UNIFORM_RHS_AFP
	class Variants : Variants {
		class Variant1 :Variant1 {
            class Primary {
                weapon      = "rhs_weap_m4a1_carryhandle";
                magazines[] = {{"rhs_mag_30Rnd_556x45_Mk318_Stanag", 6}};
            };
        };
    };
};

class RHS_AFP_Sniper : RHS_Iraq_Sniper {
    UNIFORM_RHS_AFP
	class Variants : Variants {
		class Variant1 :Variant2 {};
    };
};

class RHS_AFP_RECON_Default : RHS_Iraq_RECON_Default {
    UNIFORM_RHS_AFP_SF
};

class RHS_AFP_RECON_Rifleman : RHS_Iraq_RECON_Rifleman {
    UNIFORM_RHS_AFP_SF
};

class RHS_AFP_RECON_TeamLeader : RHS_Iraq_RECON_TeamLeader {
    UNIFORM_RHS_AFP_SF
};

class RHS_AFP_RECON_Engineer : RHS_Iraq_RECON_Engineer {
    UNIFORM_RHS_AFP_SF
};

class RHS_AFP_RECON_LAT : RHS_Iraq_RECON_LAT {
    UNIFORM_RHS_AFP_SF
};

class RHS_AFP_Crewman : RHS_UN_D_Crewman {};

class RHS_AFP_HMGLeader : RHS_Iraq_HMGLeader {
    UNIFORM_RHS_AFP
};

class RHS_AFP_HMGAmmo : RHS_Iraq_HMGAmmo {
    UNIFORM_RHS_AFP
};

class RHS_AFP_UAVOperator : RHS_Iraq_UAVOperator {
    UNIFORM_RHS_AFP
};