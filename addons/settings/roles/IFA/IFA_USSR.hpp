// IFA3 - USSR default woodland

class IFA_USSR_Default {
	scope = 0;
	ROLE_IFA_RIFLEMAN

	class Clothing {
		uniform   = "U_LIB_SOV_Strelok_summer";
		headgear  = "H_LIB_SOV_RA_PrivateCap";
		goggles   = "";
		vest      = "V_LIB_SOV_RA_MosinBelt";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "LIB_M9130";
				rail        = "";
				optics      = "";
				muzzle 		= "LIB_ACC_M1891_Bayo";
				bipod       = "";
				magazines[] = {{"LIB_5Rnd_762x54", 6}};
			};

			items[]       = {{"LIB_RDG", 1}, {"LIB_rg42", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

			class Backpack {
				backpack = "B_LIB_SOV_RA_Shinel";
			};
		};
	};
};

class IFA_USSR_Rifleman : IFA_USSR_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			items[]       = {{"LIB_rg42", 3}, {"LIB_RDG", 1}};
		};
	};
};

class IFA_USSR_Rifleman_Semi : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_RIFLEMAN_1STCLASS

    class Clothing: Clothing {
		headgear  = "H_LIB_SOV_RA_Helmet";
	};

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifleman semi auto";
			class Primary {
				weapon      = "LIB_SVT_40";
				magazines[] = {{"LIB_10Rnd_762x54", 6}};
			};
			items[]       = {{"LIB_RDG", 1}};
		};
	};
};

class IFA_USSR_Grenade_Rifleman : IFA_USSR_Default {
	scope = 2;
    ROLE_IFA_GRENADIER

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifle Grenades";
            class Primary {
				weapon      = "LIB_M9130";
				rail        = "";
				optics      = "";
				muzzle      = "LIB_ACC_GL_DYAKONOV_Empty";
				bipod       = "";
				magazines[] = {{"LIB_5Rnd_762x54", 5}};
			};
			items[]       = {{"LIB_RDG", 1}, {"LIB_1Rnd_G_DYAKONOV", 4}};
		};
       class Variant2: Variant1 {
            displayName = "Molotov";
            class Primary {
				weapon      = "LIB_M9130";
				rail        = "";
				optics      = "";
				muzzle 		= "LIB_ACC_M1891_Bayo";
				bipod       = "";
				magazines[] = {{"LIB_5Rnd_762x54", 5}};
			};
			items[]       = {{"LIB_RDG", 1}};
            class Backpack {
                backpack = "B_LIB_SOV_RA_Rucksack2_Green";
				content[] = {{"LIB_Molotov_Mag", 3}};
			};
        };
	};
};
class IFA_USSR_SquadLeader : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_OFFICER


    class Clothing: Clothing {
        uniform   = "U_LIB_SOV_Sergeant";
		headgear  = "H_LIB_SOV_RA_OfficerCap";
		vest      = "V_LIB_SOV_RA_OfficerVest";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";

			class Primary {
				weapon      = "LIB_PPSh41_m";
				magazines[] = {{"LIB_35Rnd_762x25", 5}};
			};
			items[]       = {{"LIB_RDG", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_SU", 1}};
			class Backpack {
				backpack = "";
			};
		};
		class Variant2: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_SVT_40";
				magazines[] = {{"LIB_10Rnd_762x54", 6}};
			};

            class Pistol {
                weapon      = "LIB_M1896";
				magazines[] = {{"LIB_10Rnd_9x19_M1896", 3}};
            };
			items[]       = {{"LIB_RDG", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_SU", 1}};
		};
		class Variant3: Variant1 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "LIB_M9130";
				muzzle 		= "LIB_ACC_M1891_Bayo";
				magazines[] = {{"LIB_5Rnd_762x54", 8}};
			};
            class Pistol {
                weapon      = "LIB_TT33";
				magazines[] = {{"LIB_8Rnd_762x25", 3}};
            };
			items[]       = {{"LIB_rg42", 2}, {"LIB_RDG", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_SU", 1}};
		};
	};
};

class IFA_USSR_Medic : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rifle";

			class Backpack {
                backpack = "B_LIB_SOV_RA_MedicalBag";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_RDG", 7}};
			};
		items[]       = {{"LIB_rg42", 2}};
		};
		class Variant2: Variant1 {
			displayName = "Pistol";
			class Primary : Primary {
				weapon      = "";
			};

			class Pistol {
				weapon      = "LIB_M1895";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_7Rnd_762x38", 8}};
            };
			class Backpack {
                backpack = "B_LIB_SOV_RA_MedicalBag";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_RDG", 7}};
			};
		items[]       = {{"LIB_rg42", 1}};
		};
	};
};

class IFA_USSR_MG : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
      uniform   = "U_LIB_SOV_Efreitor";
  		vest      = "V_LIB_SOV_RA_MGBelt";
  		headgear	= "H_LIB_SOV_RA_Helmet";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "LMG DP";

			class Primary : Primary {
				weapon      = "LIB_DP28";
				magazines[] = {{"LIB_47Rnd_762x54", 3}};
			};
            class Pistol {
              weapon      = "LIB_TT33";
      				magazines[] = {{"LIB_8Rnd_762x25", 3}};
            };

			class Backpack {
				backpack = "B_LIB_SOV_RA_MGAmmoBag";
				content[] = {};
			};
			items[]       = {{"LIB_RDG", 1}};
		};
		class Variant2: Variant1 {
			displayName = "LMG DT";

			class Primary : Primary {
				weapon      = "LIB_DT";
				magazines[] = {{"LIB_63Rnd_762x54", 3}};
			};
			items[]       = {{"LIB_RDG", 1}};

            class Pistol {
                weapon      = "LIB_M1896";
				magazines[] = {{"LIB_10Rnd_9x19_M1896", 3}};
            };

			class Backpack {
				backpack = "B_LIB_SOV_RA_MGAmmoBag";
				content[] = {};
			};
		};
	};
};

// -- Assistant gunner
class IFA_USSR_MGAssistant : IFA_USSR_Default {
  scope = 2;
	ROLE_IFA_ASSISTANT_GUNNER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Assistant DP";

			items[]       = {{"LIB_RDG", 1}, {"LIB_rg42", 2}, {"LIB_Binocular_SU", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

      class Backpack {
				backpack    = "B_LIB_SOV_RA_MGAmmoBag";
				content[]   = {{"LIB_47Rnd_762x54",5}};
			};
		};
		class Variant2: Variant1 {
			displayName = "Assistant DT";

			class Backpack {
				backpack    = "B_LIB_SOV_RA_MGAmmoBag";
                content[]   = {{"LIB_63Rnd_762x54",5}};
			};
		};
	};
};


class IFA_USSR_HMG : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
      uniform   = "U_LIB_SOV_Efreitor";
  		vest      = "V_LIB_SOV_RA_MGBelt";
  		headgear	= "H_LIB_SOV_RA_Helmet";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HMG DT";

			class Primary : Primary {
				weapon      = "LIB_DT_OPTIC";
				magazines[] = {{"LIB_63Rnd_762x54", 3}};
			};

            class Pistol {
				weapon      = "LIB_M1896";
      			magazines[] = {{"LIB_10Rnd_9x19_M1896", 5}};
            };

			class Backpack {
				backpack = "B_LIB_SOV_RA_MGAmmoBag";
				content[] = {};
			};
			items[]       = {{"LIB_RDG", 1}};
		};
	};
};
// -- Assistant gunner
class IFA_USSR_HMGAssistant : IFA_USSR_Default {
  scope = 2;
	ROLE_IFA_ASSISTANT_GUNNER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rifle";

			items[]       = {{"LIB_RDG", 1}, {"LIB_rg42", 2}, {"LIB_Binocular_SU", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_SOV_RA_MGAmmoBag";
                content[]   = {{"LIB_8Rnd_762x25", 4},{"LIB_63Rnd_762x54",5}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Pistol";
			class Primary : Primary {
				weapon      = "";
			};

			class Pistol {
				weapon      = "LIB_M1896";
				magazines[] = {{"LIB_10Rnd_9x19_M1896", 5}};
            };
		};
	};
};

class IFA_USSR_MGAmmo : IFA_USSR_Default {
  scope = 2;
	ROLE_IFA_LMG_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Ammo Bearer";

			items[]       = {{"LIB_RDG", 1}, {"LIB_rg42", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_SOV_RA_MGAmmoBag";
                content[]   = {{"LIB_63Rnd_762x54",6}};
			};
		};
	};
};

class IFA_USSR_LAT : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_RIFLE_LAT
	class Clothing : Clothing {
		headgear	= "H_LIB_SOV_RA_Helmet";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AT Grenades";

			class Backpack {
				backpack = "B_LIB_SOV_RA_GasBag";
				content[] = {{"LIB_rpg6", 3}};
			};
		};
		class Variant2: Variant1 {
			displayName = "AT Rifle";
			class Primary {
				weapon      = "LIB_PTRD";
				magazines[] = {{"LIB_1Rnd_145x114", 16}};
			};
			class Pistol {
				weapon      = "LIB_M1895";
				magazines[] = {{"LIB_7Rnd_762x38", 8}};
			};
			class Backpack {};
		};
	};
};

class IFA_USSR_HAT : IFA_USSR_Default {
	scope = 2;

  ROLE_IFA_RAT
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "STR_LIB_SOV_AT_soldier";

			class Secondary {
				weapon      = "LIB_M1A1_Bazooka";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1Rnd_60mm_M6", 1, 1}};
			};

			class Backpack {
			backpack = "B_LIB_US_RocketBag_Empty";
			content[] = {{"LIB_1Rnd_60mm_M6", 1}};
			};
		};
	};
};
class IFA_USSR_HAT_Ammo : IFA_USSR_Default {
	scope = 2;

  ROLE_IFA_RAT_AMMO
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "STR_LIB_SOV_AT_soldier";

			class Backpack {
			backpack = "B_LIB_US_RocketBag_Empty";
			content[] = {{"LIB_1Rnd_60mm_M6", 4}};
			};
		};
	};
};
class IFA_USSR_HAT_Ammo_1 : IFA_USSR_HAT_Ammo {
};

class IFA_USSR_Sniper : IFA_USSR_Default {
    scope = 2;
    ROLE_IFA_SNIPER
    class Clothing {
        uniform     = "U_LIB_SOV_Sniper";
        vest        = "V_LIB_SOV_RA_SniperVest";
        goggles     = "";
        headgear    = "";
    };

    class Variants : Variants {
      class Variant1 : Variant1 {
        displayName = "Standard";
        class Primary {
            weapon      = "LIB_M9130PU";
            magazines[] = {{"LIB_5Rnd_762x54", 8}};
        };
			class Backpack {
			backpack = "";
			};
        items[]       = {{"LIB_RDG", 1}};
      };
    };
};

class IFA_USSR_Spotter : IFA_USSR_Default {
	scope = 2;
    ROLE_IFA_SPOTTER

    class Clothing {
        uniform     = "U_LIB_SOV_Sniper";
        vest        = "V_LIB_SOV_RA_SniperVest";
        goggles     = "";
        headgear    = "";
    };

	class Variants : Variants {
		class Variant1 :Variant1 {
			class Backpack {
			backpack = "";
			};
			items[]       = {{"LIB_rg42", 3}, {"LIB_RDG", 1}, {"LIB_Binocular_SU", 1}};
		};
	};
};

class IFA_USSR_SMG: IFA_USSR_Default {
  scope = 2;
  ROLE_IFA_SMG
  class Clothing {
      uniform     = "U_LIB_SOV_Efreitor_summer";
      vest        = "V_LIB_SOV_RA_PPShBelt";
      goggles     = "";
      headgear    = "H_LIB_SOV_RA_Helmet";
  };

  class Variants : Variants {
    class Variant1 : Variant1 {
    	class Primary {
    		weapon      = "LIB_PPSh41_d";
    		magazines[] = {{"LIB_71Rnd_762x25", 5}};
    	};
        items[]       = {{"LIB_RDG", 1}};
    };
  };
};

class IFA_USSR_PART_SquadLeader : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_OFFICER


    class Clothing: Clothing {
        uniform   = "U_LIB_CIV_Woodlander_4";
		headgear  = "H_LIB_GER_Ushanka";
		vest      = "V_LIB_GER_OfficerVest";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";

			class Primary {
				weapon      = "LIB_MP38";
				magazines[] = {{"LIB_32Rnd_9x19", 6}};
			};

			class Pistol {
                weapon      = "LIB_M1895";
				magazines[] = {{"LIB_7Rnd_762x38", 3}};
            };
			items[]       = {{"LIB_rg42", 1}, {"LIB_RDG", 2}, {"LIB_Binocular_SU", 1}};

			class Backpack {
			backpack = "B_LIB_SOV_RA_Rucksack2";
			content[] = {};
			};
			};
		class Variant2 : Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary {
				weapon      = "LIB_SVT_40";
				magazines[] = {{"LIB_10Rnd_762x54", 6}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "Rifle";

			class Primary {
				weapon      = "LIB_M9130";
				magazines[] = {{"LIB_5Rnd_762x54", 8}};
				muzzle 		= "LIB_ACC_M1891_Bayo";
			};
            class Pistol {
                weapon      = "LIB_M1896";
				magazines[] = {{"LIB_10Rnd_9x19_M1896", 4}};
            };
			items[]       = {{"LIB_rg42", 3}, {"LIB_RDG", 2}, {"LIB_Binocular_SU", 1}};
		};
	};
};

class IFA_USSR_PART_Medic : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC
	    class Clothing: Clothing {
		uniform   = "U_LIB_CIV_Woodlander_3";
  		vest      = "V_LIB_SOV_RA_Belt";
  		headgear	= "H_LIB_SOV_Ushanka2";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Vladimir Druzhynin";

			class Primary {
				weapon      = "LIB_SVT_40";
				magazines[] = {{"LIB_10Rnd_762x54", 6}};
			};

			class Backpack {
			backpack = "B_LIB_SOV_RA_MedicalBag";
			content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_RDG", 7}};
			};
		items[]       = {{"LIB_rg42", 1}};
		};
	};
};
class IFA_USSR_PART_LMG : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
      uniform   = "U_LIB_CIV_Villager_3";
  		vest      = "V_LIB_SOV_RA_Belt";
  		headgear	= "H_LIB_SOV_Ushanka";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Yitzhak Arad";

			class Primary : Primary {
				weapon      = "LIB_DP28";
				magazines[] = {{"LIB_47Rnd_762x54", 7}};
			};
			class Backpack {
				backpack = "B_LIB_SOV_RA_GasBag";
			};
			items[]       = {{"LIB_rg42", 1}, {"LIB_RDG", 1}};
		};
	};
};
class IFA_USSR_PART_SMG: IFA_USSR_Default {
  scope = 2;
  ROLE_IFA_SMG
  class Clothing {
      uniform     = "U_LIB_CIV_Woodlander_2";
      vest        = "V_LIB_SOV_RAZV_PPShBelt";
      goggles     = "";
      headgear    = "H_LIB_CIV_Worker_Cap_4";
  };

  class Variants : Variants {
    class Variant1 : Variant1 {
		displayName = "PPSh";
    	class Primary {
    		weapon      = "LIB_PPSh41_d";
    		magazines[] = {{"LIB_71Rnd_762x25", 5}};
			};
			items[]       = {{"LIB_rg42", 1}, {"LIB_RDG", 1}};
    	};
		class Variant2 : Variant1 {
			displayName = "MP38";

			class Primary {
				weapon      = "LIB_MP38";
				magazines[] = {{"LIB_32Rnd_9x19", 5}};
			};
		};
	};
};

class IFA_USSR_PART_LAT : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_RIFLE_LAT
	class Clothing {
      uniform     = "U_LIB_CIV_Woodlander_4";
      vest        = "V_LIB_SOV_RA_Belt";
      goggles     = "";
      headgear    = "H_LIB_SOV_Ushanka2";
  };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AT Grenades";
			class Primary {
    		weapon      = "LIB_G43";
    		magazines[] = {{"LIB_10Rnd_792x57", 6}};
			};
			class Secondary {
				weapon      = "LIB_PzFaust_30m";
				magazines[] = {{"LIB_1Rnd_PzFaust_30m", 1, 1}};
			};
			class Backpack {
				backpack = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {{"LIB_rpg6", 3}};
			};
			items[]       = {{"LIB_RDG", 1}};
		};
		class Variant2: Variant1 {
			displayName = "AT Rifle";
			class Primary {
				weapon      = "LIB_PTRD";
				magazines[] = {{"LIB_1Rnd_145x114", 10}};
			};
			class Pistol {
                weapon      = "LIB_M1895";
				magazines[] = {{"LIB_7Rnd_762x38", 3}};
            };
			class Backpack {
				backpack = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {{"LIB_1Rnd_145x114", 6}};
			};
		};
	};
};
class IFA_USSR_MortarTube : IFA_USSR_Default {
	scope = 2;
	ROLE_MORTARMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar tube";
			class Secondary {
				weapon      = "LIB_BM37_Barrel";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_82mmHE_BM37", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {{"LIB_1rnd_82mmHE_BM37", 6}};
			};
		items[]       = {{"LIB_RDG", 1}, {"LIB_Binocular_SU", 1}};
        itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};

class IFA_USSR_MortarAmmo : IFA_USSR_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar Ammo";
			class Secondary {
				weapon      = "LIB_BM37_Tripod";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_82mmHE_BM37", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {{"LIB_1rnd_82mmHE_BM37", 6}};
			};
		items[]       = {{"LIB_RDG", 1}, {"LIB_Binocular_SU", 1}};
        itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};
class IFA_USSR_HMGLeader : IFA_USSR_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Backpack {
				backpack    = "frl_staticfortify_bag";
			};
		};
	};
};

class IFA_USSR_HMGAmmo : IFA_USSR_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		items[]       = {{"LIB_RDG", 1}, {"LIB_Binocular_SU", 1}};
        itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};

class IFA_USSR_Crewman : IFA_USSR_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing {
		uniform   = "U_LIB_SOV_Tank_ryadovoi";
		headgear  = "H_LIB_SOV_TankHelmet";
		goggles   = "";
		vest      = "V_LIB_SOV_RA_TankOfficerSet";
	};

	class Variants {
		class Crewman {
			displayName = "Crewman";
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
            class Pistol {
				weapon      = "LIB_M1895";
				magazines[] = {{"LIB_7Rnd_762x38", 3}};
            };
		items[]       = {{"LIB_RDG", 1}, {"LIB_Binocular_SU", 1}};
        itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "U_LIB_SOV_Pilot";
                headgear  = "H_LIB_SOV_PilotHelmet";
                goggles   = "";
                vest      = "V_LIB_DAK_PrivateBelt";
            };
			class Backpack {
                backpack = "B_LIB_US_TypeA3";
				content[] = {};
			};
		items[]       = {};
        };
	};
};

class IFA_USSR_Flamethrower : IFA_USSR_Default {
    scope = 2;
    ROLE_FLAMETHROWER

    class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Flamethrower";
            class Primary {
				weapon      = "LIB_M2_Flamethrower";
				magazines[] = {};
			};
			class Backpack {
                backpack = "B_LIB_US_M2Flamethrower";
				content[] = {{"LIB_M2_Flamethrower_Mag", 1}};
			};

			items[]       = {{"LIB_US_M18", 1}};
		};
	};
};
