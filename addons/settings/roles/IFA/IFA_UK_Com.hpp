// IFA3 - UK Commandos

#define UNIFORM_UK_Com class Clothing {\
		uniform   = "U_LIB_UK_P37JERKINS";\
		headgear  = "H_LIB_UK_Beret_Commando";\
		vest      = "V_LIB_UK_P37_STEN_BLANCO";\
		goggles   = "";\
	};

class IFA_UK_Com_Default : IFA_UK_Default {
	UNIFORM_UK_Com
};

class IFA_UK_Com_Rifleman: IFA_UK_Rifleman {
	UNIFORM_UK_Com

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Pistol {
                weapon      = "LIB_Welrod_mk1";
				magazines[] = {{"LIB_6Rnd_9x19_Welrod", 4}};
            };
			items[]       = {{"LIB_MILLSBOMB", 3}, {"LIB_No77", 2}};
        };
		class Variant2: Variant1 {
			displayName = "Suppressed Carabine";

			class Primary : Primary {
				weapon      = "LIB_DELISLE";
				magazines[] = {{"LIB_7Rnd_45ACP", 8}};
			};
			class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 4}};
            };
			items[]       = {{"LIB_MILLSBOMB", 1}, {"LIB_No77", 2}};
		};
    };
};


class IFA_UK_Com_SquadLeader: IFA_UK_SquadLeader {
    class Clothing {
        uniform     = "U_LIB_UK_P37JERKINS";
        vest        = "V_LIB_UK_P37_Officer_Blanco";
        headgear    = "H_LIB_UK_Beret_Commando";
        goggles     = "";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 4}};
            };
			items[]       = {{"LIB_No77", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_UK", 1}};
        };

		class Variant2: Variant2 {
			class Pistol {
                weapon      = "LIB_Welrod_mk1";
				magazines[] = {{"LIB_6Rnd_9x19_Welrod", 4}};
            };
			items[]       = {{"LIB_MILLSBOMB", 3}, {"LIB_No77", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_UK", 1}};
        };
		class Variant3: Variant1 {
			displayName = "Suppressed Carabine";
			class Primary {
				weapon      = "LIB_DELISLE";
				magazines[] = {{"LIB_7Rnd_45ACP", 8}};
			};
			class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 4}};
            };
			items[]       = {{"LIB_MILLSBOMB", 1}, {"LIB_No77", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_UK", 1}};
        };
		class Variant4: Variant1 {
			displayName = "SMG";
			class Primary {
				weapon      = "LIB_M1A1_Thompson";
				magazines[] = {{"LIB_30Rnd_45ACP", 5}};
			};
        };
	};
};

class IFA_UK_Com_Medic: IFA_UK_Com_Rifleman {
    ROLE_IFA_COMBAT_MEDIC

    class Variants : Variants {
		class Variant1 : Variant1 {
		items[]       = {{"LIB_MILLSBOMB", 3}, {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_No77", 7}};			
        };
		class Variant2 : Variant2 {
		items[]       = {{"LIB_MILLSBOMB", 1}, {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_No77", 7}};			
        };
	};
};

class IFA_UK_Com_MG: IFA_UK_MG {

    class Clothing {
        uniform     = "U_LIB_UK_P37JERKINS";
        vest        = "V_LIB_UK_P37_HEAVY_green";
        headgear    = "H_LIB_UK_Beret_Commando";
        goggles     = "";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			items[]       = {{"LIB_No77", 2}};
        };
    };
};


class IFA_UK_Com_MG_1 : IFA_UK_Com_MG {
};

class IFA_UK_Com_Corporal: IFA_UK_Com_Rifleman {
	ROLE_IFA_CORPORAL
     class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";
			class Primary {
				weapon      = "LIB_STEN_MK2";
				magazines[] = {{"LIB_32Rnd_9x19_Sten", 6}};
			};
			class Pistol {
                weapon      = "";
            };
			items[]       = {{"LIB_No77", 2}};
		};
		class Variant2 : Variant2 {
		};
    };
};


class IFA_UK_Com_Engineer : IFA_UK_Com_Rifleman {
    ROLE_ENGINEER
	
    class Variants : Variants {
 		class Variant1: Variant1 {
			class Backpack {
                backpack = "B_LIB_UK_HSACK_AT";
				content[] = {{"FRL_ExplosiveCharge_Wpn", 1},{"LIB_No82", 2},{"LIB_StickyNade_Mag", 3}};
			};
		items[]       = {{"LIB_No77", 2}};
		};
		class Variant2: Variant2 {
        };
	};
};

class IFA_UK_Com_Grenade_Rifleman : IFA_UK_Grenade_Rifleman {
    UNIFORM_UK_Com
    class Variants : Variants {
 		class Variant1: Variant1 {
			
			class Pistol {
                weapon      = "LIB_Welrod_mk1";
				magazines[] = {{"LIB_6Rnd_9x19_Welrod", 4}};
			};
		items[]       = {{"LIB_No77", 2}};
		};
	};
};


class IFA_UK_Com_LAT : IFA_UK_LAT {
	UNIFORM_UK_Com
   class Variants : Variants {
 		class Variant1: Variant1 {
			class Pistol {
                weapon      = "LIB_Welrod_mk1";
				magazines[] = {{"LIB_6Rnd_9x19_Welrod", 4}};
            };
		items[]       = {{"LIB_MILLSBOMB", 1}, {"LIB_No77", 2}};
		};
 		class Variant2: Variant2 {
			class Pistol {
                weapon      = "LIB_Welrod_mk1";
				magazines[] = {{"LIB_6Rnd_9x19_Welrod", 4}};
            };
		items[]       = {{"LIB_MILLSBOMB", 1}, {"LIB_No77", 2}};
		};
	};
};

class IFA_UK_Com_HAT : IFA_UK_HAT {
	UNIFORM_UK_Com
   class Variants : Variants {
 		class Variant1: Variant1 {
			class Pistol {
                weapon      = "LIB_Welrod_mk1";
				magazines[] = {{"LIB_6Rnd_9x19_Welrod", 4}};
            };
		items[]       = {{"LIB_MILLSBOMB", 1}, {"LIB_No77", 2}};
		};
	};
};

class IFA_UK_Com_HAT_Ammo : IFA_UK_HAT_Ammo {
	UNIFORM_UK_Com
   class Variants : Variants {
 		class Variant1: Variant1 {
			class Pistol {
                weapon      = "LIB_Welrod_mk1";
				magazines[] = {{"LIB_6Rnd_9x19_Welrod", 4}};
            };
		items[]       = {{"LIB_MILLSBOMB", 1}, {"LIB_No77", 2}};
		};
	};
};

class IFA_UK_Com_Sniper : IFA_UK_Sniper {
    class Clothing : Clothing {
        headgear    = "H_LIB_UK_Beret_Commando";
    };
};


class IFA_UK_Com_Spotter : IFA_UK_Com_Rifleman {
   class Variants : Variants {
		class Variant1 : Variant1 {
			items[]       = {{"LIB_MILLSBOMB", 3}, {"LIB_No77", 2}, {"LIB_BINOCULAR_UK", 1}};
        };
		class Variant2: Variant2 {
			items[]       = {{"LIB_MILLSBOMB", 1}, {"LIB_No77", 2}, {"LIB_BINOCULAR_UK", 1}};
		};
    };
};

class IFA_UK_Com_HMGLeader : IFA_UK_HMGLeader {
	UNIFORM_UK_Com
};

class IFA_UK_Com_HMGAmmo : IFA_UK_HMGAmmo {
	UNIFORM_UK_Com
};

class IFA_UK_Com_Flamethrower: IFA_UK_Flamethrower {
	UNIFORM_UK_Com
};

class IFA_UK_Com_MortarTube: IFA_UK_MortarTube {
	UNIFORM_UK_Com
};

class IFA_UK_Com_MortarAmmo: IFA_UK_MortarAmmo {
	UNIFORM_UK_Com
};