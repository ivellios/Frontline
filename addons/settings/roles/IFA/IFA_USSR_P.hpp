// IFA3 - USSR default partisans

#define UNIFORM_USSR_P class Clothing {\
		uniform   = "U_LIB_CIV_Woodlander_4";\
		headgear  = "H_LIB_SOV_Ushanka2";\
		goggles   = "";\
		vest      = "V_LIB_SOV_RA_MosinBelt";\
	};

class IFA_USSR_P_Default : IFA_USSR_Default {
	UNIFORM_USSR_P
};

class IFA_USSR_P_Rifleman : IFA_USSR_Rifleman {
	UNIFORM_USSR_P
};

class IFA_USSR_P_Rifleman_Semi : IFA_USSR_Rifleman_Semi {
	UNIFORM_USSR_P
};

class IFA_USSR_P_Grenade_Rifleman : IFA_USSR_Grenade_Rifleman {
    UNIFORM_USSR_P
};

class IFA_USSR_P_SquadLeader : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_OFFICER

    class Clothing: Clothing {
        uniform   = "U_LIB_CIV_Woodlander_4";
		headgear  = "H_LIB_GER_Ushanka";
		vest      = "V_LIB_GER_OfficerVest";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";

			class Primary {
				weapon      = "LIB_MP38";
				magazines[] = {{"LIB_32Rnd_9x19", 6}};
			};

			class Pistol {
                weapon      = "LIB_M1895";
				magazines[] = {{"LIB_7Rnd_762x38", 3}};
            };
			items[]       = {{"LIB_RDG", 2}, {"LIB_Binocular_SU", 1}};

			class Backpack {
			backpack = "B_LIB_SOV_RA_Rucksack2";
			content[] = {};
			};
			};
		class Variant2 : Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary {
				weapon      = "LIB_SVT_40";
				magazines[] = {{"LIB_10Rnd_762x54", 6}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "Rifle";

			class Primary {
				weapon      = "LIB_M9130";
				muzzle 		= "LIB_ACC_M1891_Bayo";
				magazines[] = {{"LIB_5Rnd_762x54", 8}};
			};
            class Pistol {
                weapon      = "LIB_M1896";
				magazines[] = {{"LIB_10Rnd_9x19_M1896", 4}};
            };
			items[]       = {{"LIB_Molotov_Mag", 3}, {"LIB_RDG", 2}, {"LIB_Binocular_SU", 1}};
		};
	};
};

class IFA_USSR_P_Medic : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC
	    class Clothing: Clothing {
		uniform   = "U_LIB_CIV_Woodlander_3";
  		vest      = "V_LIB_SOV_RA_Belt";
  		headgear	= "H_LIB_SOV_Ushanka2";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Vladimir Druzhynin";

			class Primary {
				weapon      = "LIB_SVT_40";
				magazines[] = {{"LIB_10Rnd_762x54", 6}};
			};

			class Backpack {
			backpack = "B_LIB_SOV_RA_MedicalBag";
			content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_RDG", 7}};
			};
		items[]       = {{"LIB_Molotov_Mag", 1}};
		};
	};
};

class IFA_USSR_P_MG : IFA_USSR_MG {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
      uniform   = "U_LIB_CIV_Villager_3";
  		vest      = "V_LIB_SOV_RA_Belt";
  		headgear	= "H_LIB_SOV_Ushanka";
    };
};

class IFA_USSR_P_MGAssistant : IFA_USSR_MGAssistant {
	UNIFORM_USSR_P
};

class IFA_USSR_P_HMGAssistant : IFA_USSR_HMGAssistant {
	UNIFORM_USSR_P
};

class IFA_USSR_P_MGAmmo : IFA_USSR_MGAmmo {
	UNIFORM_USSR_P
};

class IFA_USSR_P_LAT : IFA_USSR_Default {
	scope = 2;
	ROLE_IFA_RIFLE_LAT
	class Clothing {
      uniform     = "U_LIB_CIV_Woodlander_4";
      vest        = "V_LIB_SOV_RA_Belt";
      goggles     = "";
      headgear    = "H_LIB_SOV_Ushanka2";
  };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AT Grenades";
			class Secondary {
				weapon      = "LIB_PzFaust_60m";
				magazines[] = {{"LIB_1Rnd_PzFaust_60m", 1, 1}};
			};
			class Backpack {
				backpack = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {{"LIB_rpg6", 3}};
			};
			items[]       = {{"LIB_RDG", 1}};
		};
		class Variant2: Variant1 {
			displayName = "AT Rifle";
			class Primary {
				weapon      = "LIB_PTRD";
				magazines[] = {{"LIB_1Rnd_145x114", 10}};
			};
			class Pistol {
                weapon      = "LIB_M1895";
				magazines[] = {{"LIB_7Rnd_762x38", 3}};
            };
			class Backpack {
				backpack = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {{"LIB_1Rnd_145x114", 6}};
			};
		};
	};
};

class IFA_USSR_P_HAT : IFA_USSR_HAT {
	UNIFORM_USSR_P
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "LIB_PTRD";
				magazines[] = {{"LIB_1Rnd_145x114", 15}};
			};
			class Secondary {
				weapon      = "LIB_PzFaust_60m";
				magazines[] = {{"LIB_1Rnd_PzFaust_60m", 1, 1}};
			};
			class Pistol {
                weapon      = "LIB_M1895";
				magazines[] = {{"LIB_7Rnd_762x38", 3}};
            };
			class Backpack {
				backpack = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {{"LIB_rpg6", 3}};
			};
			items[]       = {{"LIB_RDG", 1}};
		};
	};
};

class IFA_USSR_P_HAT_1 : IFA_USSR_P_HAT {};

class IFA_USSR_P_Sniper : IFA_USSR_Sniper {
	UNIFORM_USSR_P
};

class IFA_USSR_P_Spotter : IFA_USSR_Spotter {
	UNIFORM_USSR_P
};

class IFA_USSR_P_SMG: IFA_USSR_Default {
  scope = 2;
  ROLE_IFA_SMG
  class Clothing {
      uniform     = "U_LIB_CIV_Woodlander_2";
      vest        = "V_LIB_SOV_RAZV_PPShBelt";
      goggles     = "";
      headgear    = "H_LIB_CIV_Worker_Cap_4";
  };

  class Variants : Variants {
    class Variant1 : Variant1 {
		displayName = "PPSh";
    	class Primary {
    		weapon      = "LIB_PPSh41_d";
    		magazines[] = {{"LIB_71Rnd_762x25", 5}};
			};
			items[]       = {{"LIB_RDG", 1}};
    	};
		class Variant2 : Variant1 {
			displayName = "MP38";

			class Primary {
				weapon      = "LIB_MP38";
				magazines[] = {{"LIB_32Rnd_9x19", 5}};
			};
		};
	};
};

class IFA_USSR_P_MortarTube : IFA_USSR_MortarTube {
	UNIFORM_USSR_P
};

class IFA_USSR_P_MortarAmmo : IFA_USSR_MortarAmmo {
	UNIFORM_USSR_P
};

class IFA_USSR_P_HMGLeader : IFA_USSR_HMGLeader {
	UNIFORM_USSR_P
};

class IFA_USSR_P_HMGAmmo : IFA_USSR_HMGAmmo {
	UNIFORM_USSR_P
};
class IFA_USSR_P_Crewman : IFA_USSR_Crewman {
    UNIFORM_USSR_P
};

class IF_USSR_Baserole : IFA_USSR_Crewman {
	scope = 0;
	ROLE_IFA_RIFLEMAN

    UNIFORM_USSR_P

	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "LIB_M9130";
				muzzle 		= "LIB_ACC_M1891_Bayo";
				magazines[] = {{"LIB_5Rnd_762x54", 3}};
			};
			class Backpack {
                backpack = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {};
			};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};
