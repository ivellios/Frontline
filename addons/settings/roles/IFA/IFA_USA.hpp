// IFA3 - USA default woodland

class IFA_USA_Default {
	scope = 0;
	ROLE_IFA_RIFLEMAN

	class Clothing {
		uniform   = "U_LIB_US_Rangers_Private_1st";
		headgear  = "H_LIB_US_Rangers_Helmet_os";
		goggles   = "";
		vest      = "V_LIB_US_Vest_Garand";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "LIB_M1_Garand";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"LIB_8Rnd_762x63", 6}};
			};

			items[]       = {{"LIB_US_M18", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};

class IFA_USA_Rifleman : IFA_USA_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_US_M18", 1}};
		};
	};
};

class IFA_USA_Grenade_Rifleman : IFA_USA_Default {
	scope = 2;
    ROLE_IFA_GRENADIER

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifle Grenades";
            class Primary {
				weapon      = "LIB_M1_Garand";
				rail        = "";
				optics      = "";
				muzzle      = "LIB_ACC_GL_M7";
				bipod       = "";
				magazines[] = {{"LIB_8Rnd_762x63", 5}};
			};
			items[]       = {{"LIB_US_M18", 1}, {"LIB_1Rnd_G_Mk2", 4}};
		};
	};
};

class IFA_USA_SquadLeader : IFA_USA_Default {
	scope = 2;
	ROLE_IFA_OFFICER


    class Clothing: Clothing {
        uniform   = "U_LIB_US_Rangers_Uniform";
		headgear  = "H_LIB_US_Rangers_Helmet_First_lieutenant";
		vest      = "V_LIB_US_Vest_Thompson_nco";
		goggles   = "G_LIB_Binoculars";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";

			class Primary {
				weapon      = "LIB_M1A1_THOMPSON";
				magazines[] = {{"LIB_30RND_45ACP", 5}};
			};
			items[]       = {{"LIB_US_M18", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_US", 1}};
		};
		class Variant2: Variant1 {
			displayName = "Carabine";

			class Primary : Primary {
				weapon      = "LIB_M1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};

            class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 2}};
            };
			items[]       = {{"LIB_US_M18", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_US", 1}};
		};
		class Variant3: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_M1_Garand";
				magazines[] = {{"LIB_8Rnd_762x63", 6}};
			};

            class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 2}};
            };
			items[]       = {{"LIB_US_M18", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_US", 1}};
		};
		class Variant4: Variant2 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "LIB_M1903A3_Springfield";
				magazines[] = {{"LIB_5Rnd_762x63", 8}};
				muzzle      = "LIB_ACC_M1_Bayo";
			};
			items[]       = {{"LIB_US_Mk_2", 2}, {"LIB_US_M18", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};

class IFA_USA_Medic : IFA_USA_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC

	class Clothing: Clothing {
        uniform   = "U_LIB_US_Rangers_Med";
		headgear  = "H_LIB_US_Helmet_Med_os";
		vest      = "V_LIB_US_Vest_Medic2";
    };


	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Carabine";

			class Primary : Primary {
				weapon      = "LIB_M1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};
			items[]       = {};

			class Backpack {
                backpack = "B_LIB_US_MedicBackpack_Empty";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_US_M18", 7}};
			};
		};
				class Variant2: Variant1 {
			displayName = "Pistol";

			class Primary : Primary {
				weapon      = "";
			};
			items[]       = {};

            class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 5}};
            };
			class Backpack {
                backpack = "B_LIB_US_MedicBackpack_Empty";
				content[] = {{"LIB_US_Mk_2", 1}, {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_US_M18", 7}};
			};
		};
	};
};

class IFA_USA_MG : IFA_USA_Default {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
        uniform   = "U_LIB_US_Rangers_Private_1st";
		vest      = "V_LIB_US_Vest_Bar";
		headgear	= "H_LIB_US_Rangers_Helmet_os";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Auto Rifleman";

			class Primary : Primary {
				weapon      = "LIB_M1918A2_BAR";
				magazines[] = {{"LIB_20RND_762X63", 12}};
			};
			items[]       = {{"LIB_US_M18", 1}};

			class Backpack {
				backpack = "B_LIB_US_Backpack";
				content[] = {};
			};
		};
	};
};

// -- Assistant gunner / ammo carrier
class IFA_USA_HMG : IFA_USA_Default {
  scope = 2;
	ROLE_IFA_HMG

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HMG";
			class Primary : Primary {
				weapon      = "LIB_M1919A6";
				magazines[] = {{"LIB_50Rnd_762x63", 4}};
			};
			items[]       = {{"LIB_US_M18", 1}};

      class Backpack {
				backpack    = "B_LIB_US_Backpack";
				content[]   = {};
			};
		};
	};
};
class IFA_USA_MGAssistant : IFA_USA_Default {
  scope = 2;
	ROLE_IFA_ASSISTANT_GUNNER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HMG";
			class Primary : Primary {
				weapon      = "LIB_M1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};

      class Backpack {
				backpack    = "B_LIB_US_Backpack";
				content[]   = {{"LIB_50Rnd_762x63", 2}};
			};
			items[]       = {{"LIB_US_M18", 2}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};
class IFA_USA_MGAssistant_1 : IFA_USA_Default {
  scope = 2;
	ROLE_IFA_ASSISTANT_GUNNER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HMG";
			class Primary : Primary {
				weapon      = "LIB_M1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};

      class Backpack {
				backpack    = "B_LIB_US_Backpack";
				content[]   = {{"LIB_50Rnd_762x63", 2}};
			};
			items[]       = {{"LIB_US_M18", 2}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};
/*
class IFA_USA_LAT : IFA_USA_Default {
	scope = 2;
	ROLE_RIFLE_LAT
	class Clothing : Clothing {
		headgear	= "H_LIB_SOV_RA_Helmet";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "STR_LIB_SOV_AT_grenadier";

				class Backpack {
					backpack = "B_LIB_SOV_RA_GasBag";
					content[] = {{"LIB_rpg6", 3};
				};
			};
		};
	};
};*/

class IFA_USA_HAT : IFA_USA_Default {
	scope = 2;

  ROLE_IFA_RAT
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Anti-Tank";
			class Primary : Primary {
				weapon      = "LIB_M1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};

			class Secondary {
				weapon      = "LIB_M1A1_Bazooka";
				magazines[] = {{"LIB_1Rnd_60mm_M6", 1,1}};
			};
		  class Backpack {
					backpack = "B_LIB_US_Backpack_RocketBag";
			content[] = {{"LIB_1Rnd_60mm_M6", 1}};
		  };
		};
	};
};
class IFA_USA_HAT_Ammo : IFA_USA_Default {
  scope = 2;

    ROLE_IFA_RAT_AMMO
  class Variants : Variants {
    class Variant1 : Variant1 {
      displayName = "Anti-Tank Ammo";

      class Primary : Primary {
        weapon      = "LIB_M1_CARBINE";
        magazines[] = {{"LIB_15RND_762X33", 9}};
      };

      class Backpack {
                backpack = "B_LIB_US_Backpack_RocketBag";
        content[] = {{"LIB_1Rnd_60mm_M6", 4}};
      };
    };
  };
};
class IFA_USA_HAT_Ammo_1 : IFA_USA_HAT_Ammo {
};

class IFA_USA_Engineer : IFA_USA_Default {
    scope = 2;
    ROLE_ENGINEER_AT

	class Clothing {
		uniform   = "U_LIB_US_Rangers_Private_1st";
		headgear  = "H_LIB_US_Rangers_Helmet_os";
		goggles   = "";
		vest      = "V_LIB_US_Vest_Carbine_eng";
	};

    class Variants : Variants {
		class Variant1: Variant1 {
			displayName = "SMG";
			class Primary {
				weapon      = "LIB_M3_GREASEGUN";
				magazines[] = {{"LIB_30RND_M3_GREASEGUN_45ACP", 5}};
			};
			class Backpack {
                backpack = "B_LIB_US_Backpack";
				content[] = {{"FRL_ExplosiveCharge_Wpn", 1},{"LIB_Ladung_Big_MINE_mag", 1},{"LIB_StickyNade_Mag", 3}};
			};
			items[]       = {{"LIB_US_M18", 1}};
		};
	};
};


class IFA_USA_Sniper : IFA_USA_Default {
    scope = 2;
    ROLE_IFA_SNIPER
    class Clothing {
        uniform     = "U_LIB_US_Snipe";
        vest        = "V_LIB_US_Vest_Garand";
        goggles     = "";
    };

    class Variants : Variants {
        class Variant1 : Variant1 {
            displayName = "Sniper";

            class Primary {
                weapon      = "LIB_M1903A4_Springfield";
                magazines[] = {{"LIB_5Rnd_762x63", 8}};
            };
			items[]       = {{"LIB_US_M18", 1}};
        };
    };
};

class IFA_USA_Spotter : IFA_USA_Default {
	scope = 2;
    ROLE_IFA_SPOTTER

	class Variants : Variants {
		class Variant1 :Variant1 {
			class Primary : Primary {
				weapon      = "LIB_M1903A3_Springfield";
				magazines[] = {{"LIB_5Rnd_762x63", 8}};
				muzzle      = "LIB_ACC_M1_Bayo";
			};
			items[]       = {{"LIB_US_Mk_2", 3}, {"LIB_US_M18", 1}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};

class IFA_USA_Corporal: IFA_USA_Default {
	scope = 2;
    ROLE_IFA_CORPORAL
    class Clothing {
        uniform     = "U_LIB_US_Rangers_Sergant";
        vest        = "V_LIB_US_Vest_Thompson";
        goggles     = "";
        headgear    = "H_LIB_US_Rangers_Helmet_ns";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "LIB_M1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};
			items[]       = {{"LIB_US_M18", 1}, {"LIB_BINOCULAR_US", 1}};
        };
    };
};

class IFA_USA_MortarTube : IFA_USA_Default {
	scope = 2;
	ROLE_MORTARMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar tube";
			class Secondary {
				weapon      = "LIB_M2_60_Barrel";
				magazines[] = {{"LIB_1rnd_60mmHE_M2", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_US_Backpack";
				content[] = {{"LIB_1rnd_60mmHE_M2", 9}};
			};	
		items[]       = {{"LIB_US_M18", 1}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};

class IFA_USA_MortarAmmo : IFA_USA_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar Ammo";
			class Secondary {
				weapon      = "LIB_M2_60_Tripod";
				magazines[] = {{"LIB_1rnd_60mmHE_M2", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_US_Backpack";
				content[] = {{"LIB_1rnd_60mmHE_M2", 9}};
			};
		items[]       = {{"LIB_US_M18", 1}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};
class IFA_USA_HMGLeader : IFA_USA_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
            class Primary : Primary {
				weapon      = "";
			};

            class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 5}};
            };
			class Backpack {
                backpack = "frl_staticfortify_bag";
				content[] = {{"LIB_50Rnd_762x63", 3}};
			};	
		items[]       = {{"LIB_US_M18", 1}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};
class IFA_USA_HMGAmmo : IFA_USA_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		items[]       = {{"LIB_US_M18", 1}, {"LIB_BINOCULAR_US", 1}};
		};
	};
};
class IFA_USA_Crewman : IFA_USA_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing {
		uniform   = "U_LIB_US_Tank_Crew";
		headgear  = "H_LIB_US_Helmet_Tank";
		goggles   = "G_LIB_Binoculars";
		vest      = "V_LIB_US_Vest_45";
	};

	class Variants {
		class Crewman {
			displayName = "Crewman";
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
            class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 3}};
            };
		items[]       = {{"LIB_US_M18", 1}, {"LIB_BINOCULAR_US", 1}};
        itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "U_LIB_US_Pilot_2";
                headgear  = "H_LIB_US_Helmet_Pilot_Glasses_Up";
                goggles   = "";
                vest      = "V_LIB_US_LifeVest";
            };
			class Backpack {
                backpack = "B_LIB_US_TypeA3";
				content[] = {};
			};
		items[]       = {};
        };
	};
};

class IFA_USA_Flamethrower : IFA_USA_Default {
    scope = 2;
    ROLE_FLAMETHROWER

    class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Flamethrower";
            class Primary {
				weapon      = "LIB_M2_Flamethrower";
				magazines[] = {};
			};
			class Backpack {
                backpack = "B_LIB_US_M2Flamethrower";
				content[] = {{"LIB_M2_Flamethrower_Mag", 1}};
			};

			items[]       = {{"LIB_US_M18", 1}};
		};
	};
};
