// IFA3 - USA default pacific

#define UNIFORM_UK_AB class Clothing {\
		uniform   = "U_LIB_UK_DENISONSMOCK";\
		headgear  = "H_LIB_UK_PARA_HELMET_MK2_CAMO";\
		goggles   = "";\
		vest      = "V_LIB_UK_P37_RIFLEMAN_BLANCO";\
	};

class IFA_UK_AB_Default : IFA_UK_Default {
	UNIFORM_UK_AB
};

class IFA_UK_AB_Rifleman : IFA_UK_Rifleman {
	UNIFORM_UK_AB
};

class IFA_UK_AB_Grenade_Rifleman : IFA_UK_Grenade_Rifleman {
    UNIFORM_UK_AB
};

class IFA_UK_AB_SquadLeader : IFA_UK_SquadLeader {
    class Clothing: Clothing {
        uniform   = "U_LIB_UK_DENISONSMOCK";
		headgear  = "H_LIB_UK_Para_Beret";
		vest      = "V_LIB_UK_P37_STEN_BLANCO";
		goggles   = "";
    };
};

class IFA_UK_AB_Medic : IFA_UK_Medic {
	UNIFORM_UK_AB
};

class IFA_UK_AB_Engineer : IFA_UK_Engineer {
    class Clothing: Clothing {
		uniform   = "U_LIB_UK_DENISONSMOCK";
		headgear  = "H_LIB_UK_PARA_HELMET_MK2_CAMO";
		vest      = "V_LIB_UK_P37_RIFLEMAN_BLANCO";
		goggles   = "";
	};
};

class IFA_UK_AB_MG : IFA_UK_MG {
    class Clothing: Clothing {
        uniform   = "U_LIB_UK_DENISONSMOCK";
		vest      = "B_LIB_SD_UK_ARMY_LMG_BLANCO";
		headgear	= "H_LIB_UK_PARA_HELMET_MK2_CAMO";
		goggles   = "";
    };
};

class IFA_UK_AB_MG_1 : IFA_UK_AB_MG {
};

/*
class IFA_UK_Para_MGAssistant : IFA_UK_MGAssistant {
	UNIFORM_UK_AB
};


class IFA_UK_Para_HMG : IFA_UK_HMG {
	UNIFORM_UK_AB
};

class IFA_UK_Para_HMGAssistant : IFA_UK_HMGAssistant {
	UNIFORM_UK_AB
};

class IFA_UK_Para_MGAmmo : IFA_UK_MGAmmo {
	UNIFORM_UK_AB
};
*/

class IFA_UK_AB_LAT : IFA_UK_LAT {
	UNIFORM_UK_AB
};

class IFA_UK_AB_HAT : IFA_UK_HAT {
	UNIFORM_UK_AB
};

class IFA_UK_AB_HAT_Ammo : IFA_UK_HAT_Ammo {
	UNIFORM_UK_AB
};

class IFA_UK_AB_Sniper : IFA_UK_Sniper {
    class Clothing {
        uniform     = "U_LIB_UK_DENISONSMOCK";
        vest        = "V_LIB_UK_P37_Officer_Blanco";
        goggles     = "";
        headgear    = "H_LIB_UK_Beret";
    };
};

class IFA_UK_AB_Spotter : IFA_UK_Spotter {
    class Clothing {
        uniform     = "U_LIB_UK_DENISONSMOCK";
        vest        = "V_LIB_UK_P37_Officer_Blanco";
        goggles     = "";
        headgear    = "H_LIB_UK_Beret";
    };
};

class IFA_UK_AB_Corporal: IFA_UK_Corporal {
	UNIFORM_UK_AB
};

class IFA_UK_AB_HMGLeader : IFA_UK_HMGLeader {
	UNIFORM_UK_AB
};

class IFA_UK_AB_HMGAmmo : IFA_UK_HMGAmmo {
	UNIFORM_UK_AB
};

class IFA_UK_AB_Crewman : IFA_UK_Crewman {
};

class IFA_UK_AB_Flamethrower: IFA_UK_Flamethrower {
	UNIFORM_UK_AB
};

class IFA_UK_AB_MortarTube: IFA_UK_MortarTube {
	UNIFORM_UK_AB
};

class IFA_UK_AB_MortarAmmo: IFA_UK_MortarAmmo {
	UNIFORM_UK_AB
};