// IFA3 - Wehrmacht default winter

#define UNIFORM_GER_DAK class Clothing {\
		uniform   = "U_LIB_DAK_Soldier_3";\
		headgear  = "H_LIB_DAK_Helmet_2";\
		goggles   = "G_LIB_Dust_Goggles";\
		vest      = "V_LIB_DAK_VestG43";\
	};

class IFA_GER_DAK_Default: IFA_GER_Default {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_Rifleman: IFA_GER_Rifleman {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_Rifleman_Semi: IFA_GER_Rifleman_Semi {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_Grenadier_Rifleman: IFA_GER_Grenadier_Rifleman {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_SquadLeader: IFA_GER_SquadLeader {
	class Clothing : Clothing {
        uniform   = "U_LIB_DAK_lieutenant";
		headgear  = "H_LIB_DAK_OfficerCap";
    };
};

class IFA_GER_DAK_Medic: IFA_GER_Medic {
	class Clothing : Clothing {
        uniform   = "U_LIB_DAK_Medic";
    };
};

class IFA_GER_DAK_MG: IFA_GER_MG {
	class Clothing : Clothing {
        uniform   = "U_LIB_DAK_Soldier_3";
    };
};

class IFA_GER_DAK_MGAssistant: IFA_GER_MGAssistant {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_MGAmmo: IFA_GER_MGAmmo {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_CorporalSMG: IFA_GER_CorporalSMG {
	class Clothing : Clothing {
		uniform   = "U_LIB_DAK_Soldier_3";
        headgear  = "H_LIB_DAK_Cap";
    };
};

class IFA_GER_DAK_LAT: IFA_GER_LAT {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_HAT: IFA_GER_HAT {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_HAT_Ammo: IFA_GER_HAT_Ammo {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_HAT_Ammo_1: IFA_GER_HAT_Ammo {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_Sniper: IFA_GER_Sniper {
};

class IFA_GER_DAK_HMGLeader : IFA_GER_HMGLeader {
	UNIFORM_GER_DAK
};

class IFA_GER_DAK_HMGAmmo : IFA_GER_HMGAmmo {
	UNIFORM_GER_DAK
};
class IFA_GER_DAK_Crewman : IFA_GER_Crewman {
	class Clothing : Clothing {
        uniform     = "U_LIB_DAK_Spg_crew_unterofficer";
		vest      	= "V_LIB_DAK_OfficerVest";
        headgear    = "H_LIB_DAK_Cap";
		goggles  	= "G_LIB_Scarf2_G";
    };
};
/*
class IFA_GER_DAK_Crewman_1 : IFA_GER_Crewman_1 {
	class Clothing : Clothing {
        uniform     = "U_LIB_DAK_Spg_crew_private";
		vest      	= "V_LIB_DAK_PrivateBelt";
        headgear    = "H_LIB_DAK_Cap";
		goggles  	= "G_LIB_Scarf2_G";
    };
};
*/
class IFA_GER_DAK_Flamethrower: IFA_GER_Flamethrower {
	UNIFORM_GER_DAK
};
