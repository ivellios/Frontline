// IFA3 - Wehrmacht default winter

#define UNIFORM_GER_W class Clothing {\
		uniform   = "U_LIB_GER_Soldier3_w";\
		headgear  = "H_LIB_GER_HelmetCamo2_w";\
		goggles   = "G_LIB_Headwrap";\
		vest      = "V_LIB_GER_VestKar98";\
	};

class IFA_GER_W_Default: IFA_GER_Default {
	UNIFORM_GER_W
};

class IFA_GER_W_Rifleman: IFA_GER_Rifleman {
	UNIFORM_GER_W
};

class IFA_GER_W_Rifleman_Semi: IFA_GER_Rifleman_Semi {
	class Clothing : Clothing {
        uniform     = "U_LIB_GER_Soldier3_w";
        headgear    = "H_LIB_GER_Fieldcap";
    };
};

class IFA_GER_W_Grenadier_Rifleman: IFA_GER_Grenadier_Rifleman {
	UNIFORM_GER_W
};

class IFA_GER_W_SquadLeader: IFA_GER_SquadLeader {
	class Clothing : Clothing {
        uniform   = "U_LIB_GER_Soldier_camo_w";
		headgear  = "H_LIB_GER_Fieldcap";
    };
};

class IFA_GER_W_Medic: IFA_GER_Medic {
	UNIFORM_GER_W
};

class IFA_GER_W_MG: IFA_GER_MG {
	class Clothing : Clothing {
        uniform   = "U_LIB_GER_Soldier3_w";
        headgear  = "H_LIB_GER_HelmetCamo2_w";
		goggles   = "G_LIB_Headwrap";
    };
};

class IFA_GER_W_MGAssistant: IFA_GER_MGAssistant {
	UNIFORM_GER_W
};

class IFA_GER_W_MGAmmo: IFA_GER_MGAmmo {
	UNIFORM_GER_W
};

class IFA_GER_W_CorporalSMG: IFA_GER_CorporalSMG {
	class Clothing : Clothing {
		uniform   = "U_LIB_GER_Soldier3_w";
        headgear  = "H_LIB_GER_Fieldcap";
		goggles   = "G_LIB_Headwrap";
    };
};

class IFA_GER_W_LAT: IFA_GER_LAT {
	UNIFORM_GER_W
};

/*
class IFA_GER_W_LATG43: IFA_GER_LATG43 {
	UNIFORM_GER_W
};
*/

class IFA_GER_W_HAT: IFA_GER_HAT {
	UNIFORM_GER_W
};

class IFA_GER_W_HAT_Ammo: IFA_GER_HAT_Ammo {
	UNIFORM_GER_W
};

class IFA_GER_W_HAT_Ammo_1: IFA_GER_HAT_Ammo {
	UNIFORM_GER_W
};

class IFA_GER_W_Sniper: IFA_GER_Sniper {
	class Clothing : Clothing {
        uniform     = "U_LIB_GER_Scharfschutze_2_w";
		goggles   	= "G_LIB_GER_Gloves2";
    };
};
/*
class IFA_GER_W_OberSchutze: IFA_GER_OberSchutze {
	class Clothing : Clothing {
        uniform     = "U_LIB_GER_Soldier3_w";
        headgear    = "H_LIB_GER_Fieldcap";
    };
};
*/
class IFA_GER_W_MortarTube : IFA_GER_MortarTube {
	UNIFORM_GER_W
};

class IFA_GER_W_MortarAmmo : IFA_GER_MortarAmmo {
	UNIFORM_GER_W
};

class IFA_GER_W_HMGLeader : IFA_GER_HMGLeader {
	UNIFORM_GER_W
};

class IFA_GER_W_HMGAmmo : IFA_GER_HMGAmmo {
	UNIFORM_GER_W
};
class IFA_GER_W_Crewman : IFA_GER_Crewman {
    UNIFORM_GER_W
};

class IFA_GER_W_Flamethrower: IFA_GER_Flamethrower {
	UNIFORM_GER_W
};