// IFA3 - Wehrmacht default woodland

class IFA_GER_Default {
	scope = 0;
	ROLE_IFA_RIFLEMAN

	class Clothing {
		uniform   = "U_LIB_GER_Schutze";
		headgear  = "H_LIB_GER_Helmet";
		goggles   = "";
		vest      = "V_LIB_GER_VestKar98";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "LIB_K98";
				rail        = "";
				optics      = "";
				muzzle      = "LIB_ACC_K98_Bayo";
				bipod       = "";
				magazines[] = {{"LIB_5Rnd_792x57", 7}};
			};

			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};

class IFA_GER_Rifleman : IFA_GER_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			items[]       = {{"LIB_shg24", 3}, {"lib_nb39", 1}};
		};
	};
};

class IFA_GER_Rifleman_Semi : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_RIFLEMAN_1STCLASS

    class Clothing: Clothing {
		uniform   = "U_LIB_GER_Schutze";
		headgear  = "H_LIB_GER_Helmet";
		vest      = "V_LIB_WP_G43Vest";
		goggles   = "";
    };

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifleman semi auto";
			class Primary {
				weapon      = "LIB_G43";
				magazines[] = {{"LIB_10Rnd_792x57", 6}};
			};
			items[]       = {{"lib_nb39", 1}};
		};
	};
};

class IFA_GER_Grenadier_Rifleman : IFA_GER_Default {
	scope = 2;
    ROLE_IFA_GRENADIER

	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifle Grenades";
            class Primary {
				weapon      = "LIB_K98";
				rail        = "";
				optics      = "";
				muzzle      = "LIB_ACC_GW_SB_Empty";
				bipod       = "";
				magazines[] = {{"LIB_5Rnd_792x57", 5}};
			};
			items[]       = {{"lib_nb39", 1}, {"LIB_1Rnd_G_SPRGR_30", 3}, {"LIB_1Rnd_G_PZGR_40", 1}, {"LIB_1Rnd_G_PZGR_30", 1}};
		};
        class Variant2: Variant1 {
            displayName = "Bundle Grenades";
            class Primary {
				weapon      = "LIB_K98";
				rail        = "";
				optics      = "";
				muzzle      = "LIB_ACC_K98_Bayo";
				bipod       = "";
				magazines[] = {{"LIB_5Rnd_792x57", 5}};
			};
			items[]       = {{"lib_nb39", 1}};
            class Backpack {
                backpack = "B_LIB_GER_Backpack";
				content[] = {{"LIB_shg24x7", 3},{"FRL_ExplosiveCharge_Wpn", 2}};
			};
        };
	};
};

class IFA_GER_SquadLeader : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_OFFICER


    class Clothing: Clothing {
        uniform   = "U_LIB_GER_Unterofficer";
		headgear  = "H_LIB_GER_OfficerCap";
		vest      = "V_LIB_GER_FieldOfficer";
		goggles   = "";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";


      class Primary {
				weapon      = "LIB_MP40";
				magazines[] = {{"LIB_32Rnd_9x19", 5}};
			};
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};

		};
		class Variant2: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_G43";
				magazines[] = {{"LIB_10Rnd_792x57", 6}};
			};

            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 3}};
            };
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
		};
		class Variant3: Variant1 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 8}};
				muzzle      = "LIB_ACC_K98_Bayo";
			};
            class Pistol {
                weapon      = "LIB_WaltherPPK";
				magazines[] = {{"LIB_7Rnd_765x17_PPK", 3}};
            };
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GER_Medic : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC

    class Clothing: Clothing {
        uniform   = "U_LIB_GER_Medic";
		goggles   = "G_LIB_Dienst_Brille2";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rifle";

			class Backpack {
                backpack = "B_LIB_GER_MedicBackpack_Empty";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"lib_nb39", 7}};
			};
			items[]       = {{"LIB_shg24", 2}};
		};
		class Variant2: Variant1 {
			displayName = "Pistol";

			class Primary : Primary {
				weapon      = "";
			};

            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 5}};
            };
			class Backpack {
                backpack = "B_LIB_GER_MedicBackpack_Empty";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"lib_nb39", 7}};
			};
			items[]       = {{"LIB_shg24", 1}};
		};
	};
};

class IFA_GER_MG : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
        uniform   = "U_LIB_GER_Schutze";
		vest      = "V_LIB_GER_VestMG";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "MG42";

			class Primary : Primary {
				weapon      = "LIB_MG42";
				muzzle      = "";
				magazines[] = {{"LIB_50Rnd_792x57", 2}};
			};

            class Backpack {
				backpack    = "B_LIB_GER_A_frame";
                content[]   = {};
			};
			items[]       = {{"lib_nb39", 1}};
		};

		class Variant2 : Variant1 {
			displayName = "MG34";

			class Primary : Primary {
				weapon      = "LIB_MG34";
				magazines[] = {{"LIB_50Rnd_792x57", 2}};
			};
			items[]       = {{"lib_nb39", 1}};

		};
	};
};

// -- Assistant gunner
class IFA_GER_MGAssistant : IFA_GER_Default {
  scope = 2;
	ROLE_IFA_ASSISTANT_GUNNER

	class Variants {
		class Variant1 {
			displayName = "Rifle";
			class Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 7}};
				muzzle      = "LIB_ACC_K98_Bayo";
			};

			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 2}, {"LIB_Binocular_GER", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_GER_Backpack";
                content[]   = {{"LIB_50Rnd_792x57", 5}};
				};
			};
			class Variant2 : Variant1 {
			displayName = "Pistol";

			class Primary {
				weapon      = "";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 1}};
			};
            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 5}};
            };

			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 1}, {"LIB_Binocular_GER", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_GER_Backpack";
                content[]   = {{"LIB_50Rnd_792x57", 5}};
			};
		};
	};
};

class IFA_GER_MGAmmo : IFA_GER_Default {
  scope = 2;
	ROLE_IFA_LMG_AMMO_BEARER

	class Variants {
		class Variant1 {
			displayName = "Rifle";

            class Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 6}};
				muzzle      = "LIB_ACC_K98_Bayo";
			};

			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_GER_Backpack";
                content[]   = {{"LIB_50Rnd_792x57", 6}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Pistol";

			class Primary {
				weapon      = "";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 1}};
			};
            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 5}};
            };


			items[]       = {{"lib_nb39", 1}, {"LIB_shg24", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "B_LIB_GER_Backpack";
                content[]   = {{"LIB_50Rnd_792x57", 6}};
			};
		};
	};
};

class IFA_GER_CorporalSMG: IFA_GER_Default {
  scope = 2;
	ROLE_IFA_CORPORAL

    class Clothing : Clothing {
		uniform   = "U_LIB_GER_Gefreiter";
        headgear  = "H_LIB_GER_HelmetCamo";
        vest    = "V_LIB_GER_VestMP40";
    };
    class Variants : Variants {
		class Variant1 : Variant1 {
            displayName = "SMG";
      class Primary {
          weapon      = "LIB_MP40";
          rail        = "";
          optics      = "";
          muzzle      = "";
          bipod       = "";
          magazines[] = {{"LIB_32Rnd_9x19", 6}};
      };
			items[]       = {{"lib_nb39", 2}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GER_LAT : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_RIFLE_LAT

  class Clothing: Clothing {
      uniform = "AT Grenades";
  };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "STR_LIB_GER_AT_grenadier";

			class Backpack {
        		backpack = "B_LIB_GER_Backpack";
				content[] = {{"lib_pwm", 3}, {"FRL_ExplosiveCharge_Wpn", 2}};
			};
		};
		class Variant2: Variant1 {
			displayName = "Panzerfaust";
			class Primary : Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 6}};
				muzzle      = "LIB_ACC_K98_Bayo";
			};
			class Secondary {
				weapon      = "LIB_PzFaust_60m";
				magazines[] = {{"LIB_1Rnd_PzFaust_60m", 1, 1}};
			};
			class Backpack {
        		backpack = "B_LIB_GER_Backpack";
				content[] = {{"LIB_1Rnd_PzFaust_60m", 1}};
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}};
		};
	};
};

class IFA_GER_HAT : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_RAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "LIB_RPzB";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1Rnd_RPzB", 1,1}};
			};
			class Backpack {
                backpack = "B_LIB_GER_Panzer";
				content[] = {{"LIB_1Rnd_RPzB", 1}};
			};
		};
	};
};

class IFA_GER_HAT_Ammo : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_RAT_AMMO

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Backpack {
                backpack = "B_LIB_GER_Panzer";
				content[] = {{"LIB_1Rnd_RPzB", 4}};
			};
		};
	};
};

class IFA_GER_HAT_Ammo_1 : IFA_GER_HAT_Ammo {
};

class IFA_GER_Sniper : IFA_GER_Default {
    scope = 2;
		ROLE_IFA_SNIPER

    class Clothing {
        uniform     = "U_LIB_GER_Scharfschutze";
        headgear    = "H_LIB_GER_HelmetCamo";
        vest        = "V_LIB_WP_SniperBela";
        goggles     = "";
    };

    class Variants : Variants {
        class Variant1 : Variant1 {
            displayName = "STR_LIB_GER_scout_sniper";

            class Primary {
                weapon      = "LIB_K98ZF39";
                magazines[] = {{"LIB_5Rnd_792x57", 8}};
            };
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
        };
    };
};

class IFA_GER_Storm_SquadLeader : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_OFFICER


    class Clothing: Clothing {
        uniform   = "U_LIB_GER_Soldier_camo4";
		headgear  = "H_LIB_GER_HelmetCamo2";
		vest      = "V_LIB_WP_G43Vest";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rifle";


			class Primary : Primary {
				weapon      = "LIB_G43";
				magazines[] = {{"LIB_10Rnd_792x57", 6}};

			};
			items[]       = {{"LIB_shg24", 1}, {"lib_nb39", 2}, {"LIB_Binocular_GER", 1}};


		};
		class Variant2: Variant1 {
			displayName = "SMG";

			 class Primary {
				weapon      = "LIB_MP40";
				magazines[] = {{"LIB_32Rnd_9x19", 6}};
			};

            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 3}};
            };
			items[]       = {{"LIB_shg24", 1}, {"lib_nb39", 2}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GER_Storm_Rifleman : IFA_GER_Default {
	scope = 2;
	class Clothing: Clothing {
        uniform   = "U_LIB_GER_Soldier_camo4";
		headgear  = "H_LIB_GER_HelmetCamo2";
		vest      = "V_LIB_WP_Kar98Vest";
    };

	class Variants : Variants {
		class Variant1 :Variant1 {
			class Backpack {
			backpack = "B_LIB_GER_Backpack";
				content[] = {{"LIB_shg24", 1}, {"LIB_shg24x7", 2}};
			};
			items[]       = {{"LIB_shg24", 1}, {"lib_nb39", 1}};
		};
	};
};

class IFA_GER_Storm_Medic : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC

    class Clothing: Clothing {
        uniform     = "U_LIB_GER_Soldier_camo4";
        headgear    = "H_LIB_GER_HelmetCamo2";
        vest        = "V_LIB_WP_Kar98Vest";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "STR_LIB_GER_medic";

			class Backpack {
                backpack = "B_LIB_GER_MedicBackpack";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"lib_nb39", 7}};
			};
			items[]       = {{"LIB_shg24", 2}};
		};
	};
};

class IFA_GER_Storm_STG: IFA_GER_Default {
	scope = 2;
  ROLE_IFA_LMG

    class Clothing: Clothing {
        uniform     = "U_LIB_GER_Soldier_camo4";
        headgear    = "H_LIB_GER_HelmetCamo2";
        vest        = "V_LIB_WP_STGVest";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "LIB_MP44";
				magazines[] = {{"LIB_30Rnd_792x33", 8}};
			};
        };
    };
};

class IFA_GER_Storm_STG_1: IFA_GER_Default {
	scope = 2;
  ROLE_IFA_LMG

    class Clothing: Clothing {
        uniform     = "U_LIB_GER_Soldier_camo4";
        headgear    = "H_LIB_GER_HelmetCamo2";
        vest        = "V_LIB_WP_STGVest";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "LIB_MP44";
				magazines[] = {{"LIB_30Rnd_792x33", 8}};
			};
        };
    };
};

class IFA_GER_Storm_STG_2: IFA_GER_Default {
	scope = 2;
  ROLE_IFA_LMG

    class Clothing: Clothing {
        uniform     = "U_LIB_GER_Soldier_camo4";
        headgear    = "H_LIB_GER_HelmetCamo2";
        vest        = "V_LIB_WP_STGVest";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "LIB_MP44";
				magazines[] = {{"LIB_30Rnd_792x33", 8}};
			};
        };
    };
};

class IFA_GER_Storm_LAT : IFA_GER_Default {
	scope = 2;
	ROLE_IFA_RIFLE_LAT

  class Clothing: Clothing {
      uniform = "U_LIB_GER_Schutze";
  };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Panzerfaust";
			class Primary : Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 6}};
			};
			class Secondary {
				weapon      = "LIB_PzFaust_60m";
				magazines[] = {{"LIB_1Rnd_PzFaust_60m", 1, 1}};
			};

			class Backpack {
			backpack = "B_LIB_GER_Backpack";
				content[] = {{"lib_pwm", 3}};
			};
		};
	};
};

class IFA_GER_MortarTube : IFA_GER_Default {
	scope = 2;
	ROLE_MORTARMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar tube";

			class Secondary {
				weapon      = "LIB_GrWr34_Barrel";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_81mmHE_GRWR34", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_GER_Backpack";
				content[] = {{"LIB_1rnd_81mmHE_GRWR34", 8}};
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GER_MortarAmmo : IFA_GER_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar Ammo";
			class Secondary {
				weapon      = "LIB_GrWr34_Tripod";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_81mmHE_GRWR34", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_GER_Backpack";
				content[] = {{"LIB_1rnd_81mmHE_GRWR34", 8}};
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GER_HMGLeader : IFA_GER_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "";
			};

            class Pistol {
                weapon      = "LIB_P08";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 5}};
            };
			class Backpack {
                backpack = "frl_staticammo_bag";
				content[] = {{"LIB_50Rnd_792x57", 3}};
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GER_HMGAmmo : IFA_GER_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
			items[]       = {{"LIB_shg24", 2}, {"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GER_Crewman : IFA_GER_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing {
		uniform   = "U_LIB_GER_Tank_crew_leutnant";
		headgear  = "H_LIB_GER_TankPrivateCap2";
		goggles   = "";
		vest      = "V_LIB_GER_TankPrivateBelt";
	};
	class Variants {
		class Crewman {
			displayName = "Crewman";
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 3}};
            };
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
            itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "U_LIB_GER_LW_pilot";
                headgear  = "H_LIB_GER_LW_PilotHelmet";
                goggles   = "";
                vest      = "V_LIB_GER_OfficerBelt";
            };
			class Backpack {
                backpack = "B_LIB_US_TypeA3";
				content[] = {};
			};
		items[]       = {};
        };
	};
};

class IFA_GER_Flamethrower : IFA_GER_Default {
    scope = 2;
    ROLE_FLAMETHROWER

    class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Flamethrower";
            class Primary {
				weapon      = "LIB_M2_Flamethrower";
				magazines[] = {};
			};
			class Backpack {
                backpack = "B_LIB_US_M2Flamethrower";
				content[] = {{"LIB_M2_Flamethrower_Mag", 1}};
			};
			items[]       = {{"LIB_US_M18", 1}};
		};
	};
};

class IF_GER_Baserole : IFA_GER_Default {
    scope = 0;
    ROLE_IFA_RIFLEMAN

    uniform   = "U_LIB_GER_Schutze";

    class Variants {
        class Variant1 {
            class Primary {
                weapon      = "LIB_K98";
                magazines[] = {{"LIB_5Rnd_792x57", 3}};
				muzzle      = "LIB_ACC_K98_Bayo";
            };
			class Backpack {
                backpack = "B_LIB_GER_Backpack";
				content[] = {};
			};
            itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
        };
    };
};
