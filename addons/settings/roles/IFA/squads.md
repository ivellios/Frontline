## Wehrmacht

By JK on discord:
https://youtu.be/-rKRt5zVZgw?t=40s
https://goo.gl/TLq1OW

http://www.avalanchepress.com/BehindCounters2.php
http://www.avalanchepress.com/BehindCounters3.php

#### 1941 (Original)
- 10 Man Sizes
  - SL (Sergeant)
    - MP40 (6 magazines)
  - MG
    - MG42, with 1 drum
    - Pistol
  - Assistant Gunner
    - Pistol Only
    - 4 Drums, 1 box 300
  - Ammo Carrier
    - K98Krz (5 Magazines)
    - 2 box 300
  - 5x Rifleman (Schutze)
    - K98Krz (5 magazines)
    - 2 Grenades
  - Unteroffizier
    - K98Krz (5 magazines)

#### 1941 (Frontline Setup)
- 10 Man Size
  - SL (Sergeant)
    - MP40 (6 magazines)
    - Binoculars
  - MG
    - MG42, with 2 drum
    - Pistol
  - Assistant Gunner
    - Pistol Only
    - Binoculars
    - 4 Drums, 1 box 300
  - Medic
    - K98Krz (5 Magazines)
    - Backpack ammo
  - 4x Rifleman (Schutze)
    - K98Krz (5 magazines)
    - 2 Grenades
  - Unteroffizier
    - K98Krz (5 magazines)
    * Radio guy

## RUSKI RUSKI
- 9 Man Sizes
  - SL (Sergeant)
    - Mosin
  - MG
    - DPT With Drum
    - Pistol
  - Assistant Gunner
    - Mosin
    - 4 Drum
  - 2 X Submachine Gunner
    - ppsSH
    - 4 Mags
  - 4x Rifleman
    - Mosin
    - 2 Grenades
  - 1x Medic
    - Mosin






#### 1944
  - 10 Man Sizes
