// IFA3 - Wehrmacht default winter

#define UNIFORM_GER_AB class Clothing {\
		uniform   = "U_LIB_FSJ_Soldier_camo";\
		headgear  = "H_LIB_GER_FSJ_M38_Helmet_os";\
		goggles   = "";\
		vest      = "V_LIB_GER_FSJ_VestKar98";\
	};

class IFA_GER_AB_Default: IFA_GER_Default {
	UNIFORM_GER_AB
};

class IFA_GER_AB_Rifleman: IFA_GER_Rifleman {
	UNIFORM_GER_AB
};

class IFA_GER_AB_Rifleman_Semi: IFA_GER_Rifleman_Semi {
    UNIFORM_GER_AB
};

class IFA_GER_AB_Grenadier_Rifleman: IFA_GER_Grenadier_Rifleman {
	UNIFORM_GER_AB
};

class IFA_GER_AB_SquadLeader: IFA_GER_SquadLeader {
	class Clothing : Clothing {
        uniform   = "U_LIB_FSJ_Soldier_camo";
		headgear  = "H_LIB_GER_FSJ_M38_Helmet_os";
		};
	class Variants : Variants {
		class Variant1 :Variant1 {
		};
		class Variant2 :Variant2 {
		};
		class Variant3 :Variant3 {
		};
        class Variant4: Variant1 {
            displayName = "Assault Rifle";
            class Primary {
				weapon      = "LIB_MP44";
				magazines[] = {{"LIB_30Rnd_792x33", 5}};
			};
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
        };
        class Variant5: Variant1 {
            displayName = "Automatic Rifle";
            class Primary {
				weapon      = "LIB_FG42G";
				magazines[] = {{"LIB_20Rnd_792x57", 6}};
			};
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
        };
	};
};

class IFA_GER_AB_Medic: IFA_GER_Medic {
	UNIFORM_GER_AB
};

class IFA_GER_AB_MG: IFA_GER_MG {
	class Clothing : Clothing {
        uniform   = "U_LIB_FSJ_Soldier_camo";
		headgear  = "H_LIB_GER_FSJ_M38_Helmet_os";
		vest      = "V_LIB_GER_VestMG";
    };
};

class IFA_GER_AB_MGAssistant: IFA_GER_MGAssistant {
	UNIFORM_GER_AB
};

class IFA_GER_AB_MGAmmo: IFA_GER_MGAmmo {
	UNIFORM_GER_AB
};

class IFA_GER_AB_CorporalSMG: IFA_GER_CorporalSMG {
	class Clothing : Clothing {
        uniform   = "U_LIB_FSJ_Soldier_camo";
		headgear  = "H_LIB_GER_FSJ_M38_Helmet_os";
		vest      = "V_LIB_GER_VestSTG";
    };
	class Variants : Variants {
		class Variant1 :Variant1 {
		};
        class Variant2: Variant1 {
            displayName = "Assault Rifle";
            class Primary {
				weapon      = "LIB_MP44";
				magazines[] = {{"LIB_30Rnd_792x33", 5}};
			};
        };
        class Variant3: Variant1 {
            displayName = "Automatic Rifle";
            class Primary {
				weapon      = "LIB_FG42G";
				magazines[] = {{"LIB_20Rnd_792x57", 6}};
			};
        };
	};
};

class IFA_GER_AB_LAT: IFA_GER_LAT {
	UNIFORM_GER_AB
};

class IFA_GER_AB_HAT: IFA_GER_HAT {
	UNIFORM_GER_AB
};

class IFA_GER_AB_HAT_Ammo: IFA_GER_HAT_Ammo {
	UNIFORM_GER_AB
};

class IFA_GER_AB_HAT_Ammo_1: IFA_GER_HAT_Ammo {
	UNIFORM_GER_AB
};

class IFA_GER_AB_Sniper: IFA_GER_Sniper {
	class Clothing : Clothing {
        uniform     = "U_LIB_FSJ_Soldier_camo";
    };
};

class IFA_GER_AB_HMGLeader : IFA_GER_HMGLeader {
	UNIFORM_GER_AB
};

class IFA_GER_AB_HMGAmmo : IFA_GER_HMGAmmo {
	UNIFORM_GER_AB
};

class IFA_GER_AB_Flamethrower: IFA_GER_Flamethrower {
	UNIFORM_GER_AB
};