// IFA3 - UK desert, ca mid 1942 setup

#define UNIFORM_UK_D class Clothing {\
	uniform   = "U_LIB_UK_KhakiDrills";\
	headgear  = "H_LIB_UK_Helmet_Mk2_Desert_Bowed";\
	vest      = "V_LIB_UK_P37_RIFLEMAN";\
	goggles   = "";\
};


class IFA_UK_D_Default : IFA_UK_Default {
	UNIFORM_UK_D;
};

class IFA_UK_D_Rifleman : IFA_UK_Rifleman {
	UNIFORM_UK_D;
};

class IFA_UK_D_Grenade_Rifleman : IFA_UK_Grenade_Rifleman {
    UNIFORM_UK_D;
};

class IFA_UK_D_SquadLeader : IFA_UK_SquadLeader {
    class Clothing: Clothing {
		uniform   = "U_LIB_UK_KhakiDrills";
		headgear  = "H_LIB_UK_Beret";
		vest      = "V_LIB_UK_P37_Officer";
		goggles   = "G_LIB_Binoculars";
    };
};

class IFA_UK_D_Medic : IFA_UK_Medic {
	class Clothing: Clothing {
		headgear  = "H_LIB_UK_Helmet_Mk2_Cover";
		uniform   = "U_LIB_UK_KhakiDrills";
		vest      = "V_LIB_UK_P37_RIFLEMAN";
		goggles   = "";
	};
};

class IFA_UK_D_Engineer : IFA_UK_Engineer {
	UNIFORM_UK_D;
};

class IFA_UK_D_MG : IFA_UK_MG {
    class Clothing: Clothing {
		vest = "V_LIB_UK_P37_Heavy";
		headgear  = "H_LIB_UK_Helmet_Mk2_Desert_Bowed";
		uniform   = "U_LIB_UK_KhakiDrills";
		goggles   = "";
    };
};

class IFA_UK_D_MG_1 : IFA_UK_D_MG {
	class Clothing: Clothing {
		vest = "V_LIB_UK_P37_Heavy";
		headgear  = "H_LIB_UK_Helmet_Mk2_Desert_Bowed";
		uniform   = "U_LIB_UK_KhakiDrills";
		goggles   = "";
    };
};

/*
class IFA_UK_Para_MGAssistant : IFA_UK_MGAssistant {
	UNIFORM_UK_D
};


class IFA_UK_Para_HMG : IFA_UK_HMG {
	UNIFORM_UK_D
};

class IFA_UK_Para_HMGAssistant : IFA_UK_HMGAssistant {
	UNIFORM_UK_D
};

class IFA_UK_Para_MGAmmo : IFA_UK_MGAmmo {
	UNIFORM_UK_D
};
*/

class IFA_UK_D_LAT : IFA_UK_LAT {
	UNIFORM_UK_D;
};

class IFA_UK_D_HAT: IFA_UK_D_LAT {};

class IFA_UK_D_HAT_Ammo : IFA_UK_HAT_Ammo {
	UNIFORM_UK_D;
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Anti-Tank Ammo";
			class Backpack {
				backpack = "B_LIB_UK_HSACK_AT";
				content[] = {{"LIB_1Rnd_89m_PIAT", 3}};
			};
		};
	};
};

class IFA_UK_D_Sniper : IFA_UK_Sniper {
    class Clothing: Clothing {
        vest        = "V_LIB_UK_P37_Officer";
        goggles     = "G_LIB_Scarf2_G";
        headgear    = "H_LIB_UK_Beret";
		uniform   	= "U_LIB_UK_KhakiDrills";
    };
};

class IFA_UK_D_Spotter : IFA_UK_Spotter {
    class Clothing: Clothing {
		vest 		= "V_LIB_UK_P37_Officer";
		goggles 	= "G_LIB_Scarf2_G";
		headgear 	= "H_LIB_UK_Beret";
		uniform  	= "U_LIB_UK_KhakiDrills";
    };
};

class IFA_UK_D_Corporal: IFA_UK_Corporal {
	UNIFORM_UK_D;
};

class IFA_UK_D_HMGLeader : IFA_UK_HMGLeader {
	UNIFORM_UK_D;
};

class IFA_UK_D_HMGAmmo : IFA_UK_HMGAmmo {
	UNIFORM_UK_D;
};

class IFA_UK_D_Crewman : IFA_UK_Crewman {
	class Clothing: Clothing {
		goggles = "G_LIB_Dust_Goggles";
		uniform = "U_LIB_UK_KhakiDrills";
		vest	= "V_LIB_UK_P37_Officer";
	};
};

class IFA_UK_D_Flamethrower: IFA_UK_Flamethrower {
	UNIFORM_UK_D;
};

class IFA_UK_D_MortarTube: IFA_UK_MortarTube {
	UNIFORM_UK_D;
};

class IFA_UK_D_MortarAmmo: IFA_UK_MortarAmmo {
	UNIFORM_UK_D;
};
