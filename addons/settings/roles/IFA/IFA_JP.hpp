// IFA3 - Wehrmacht default woodland

class IFA_JP_Default {
	scope = 0;
	ROLE_IFA_RIFLEMAN

	class Clothing {
		uniform   = "fow_u_ija_type98_foliage";
		headgear  = "fow_h_ija_type90_foliage";
		goggles   = "";
		vest      = "fow_v_ija_rifle";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "fow_w_type99";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"fow_5Rnd_77x58", 7}};
			};

			items[]       = {{"lib_nb39", 1}, {"fow_e_type97", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};

class IFA_JP_Rifleman : IFA_JP_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			items[]       = {{"fow_e_type97", 3}, {"lib_nb39", 1}};
		};
	};
};

class IFA_JP_Rifleman_Semi : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_RIFLEMAN_1STCLASS
	
	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifleman semi auto";
			class Primary {
				weapon      = "LIB_G43";
				magazines[] = {{"LIB_10Rnd_792x57", 6}};
			};
			items[]       = {{"lib_nb39", 1}};
		};
	};
};

class IFA_JP_Grenadier_Rifleman : IFA_JP_Default {
	scope = 2;
    ROLE_IFA_GRENADIER
	
    class Clothing: Clothing {
		vest      = "fow_v_ija_grenadier";
    };
	
	class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Rifle Grenades";
            class Primary {
				weapon      = "fow_w_type99";
				magazines[] = {{"fow_5Rnd_77x58", 5}};
			};
			items[]       = {{"lib_nb39", 1}, {"fow_1Rnd_type2_40", 4}};
		};
	};
};

class IFA_JP_SquadLeader : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_OFFICER


    class Clothing: Clothing {
        uniform   = "fow_u_ija_type98_khakibrown";
		headgear  = "fow_h_ija_fieldcap";
		vest      = "fow_v_ija_nco";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";


      class Primary {
				weapon      = "fow_w_type100";
				magazines[] = {{"fow_32Rnd_8x22", 5}};
			};
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
		};
		class Variant2: Variant1 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "fow_w_type99";
				magazines[] = {{"fow_5Rnd_77x58", 8}};
			};
            class Pistol {
                weapon      = "fow_w_type14";
				magazines[] = {{"fow_8Rnd_8x22", 3}};
            };
			items[]       = {{"fow_e_type97", 2}, {"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_JP_Medic : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC

    class Clothing: Clothing {
		goggles   = "G_LIB_Dienst_Brille2";
		vest      = "fow_v_ija_medic";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rilfe";

			class Backpack {
                backpack = "fow_b_ija_backpack_foliage";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"lib_nb39", 7}};
			};
			items[]       = {{"fow_e_type97", 2}};
		};
		class Variant2: Variant1 {
			displayName = "Pistol";

			class Primary : Primary {
				weapon      = "";
			};

            class Pistol {
                weapon      = "fow_w_type14";
				magazines[] = {{"fow_8Rnd_8x22", 5}};
            };
			class Backpack {
                backpack = "fow_b_ija_backpack_foliage";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"lib_nb39", 7}};
			};
			items[]       = {{"fow_e_type97", 1}};
		};
	};
};

class IFA_JP_MG : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
		vest      = "fow_v_ija_mg";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Type99";

			class Primary : Primary {
				weapon      = "fow_w_type99_lmg";
				magazines[] = {{"fow_30Rnd_77x58", 10}};
			};
			items[]       = {{"lib_nb39", 1}};
		};
	};
};
class IFA_JP_MG_1 : IFA_JP_MG {};

// -- Assistant gunner
class IFA_JP_MGAssistant : IFA_JP_Default {
  scope = 2;
	ROLE_IFA_ASSISTANT_GUNNER

	class Variants {
		class Variant1 {
			displayName = "Rifle";
			class Primary {
				weapon      = "fow_w_type99";
				magazines[] = {{"fow_5Rnd_77x58", 7}};
			};
			
			items[]       = {{"lib_nb39", 1}, {"fow_e_type97", 2}, {"LIB_Binocular_GER", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "fow_b_ija_ammobox_wood";
                content[]   = {{"fow_30Rnd_77x58", 12}};
				};
			};
			class Variant2 : Variant1 {
			displayName = "Pistol";

			class Primary {
				weapon      = "";
				magazines[] = {{"fow_8Rnd_8x22", 1}};
			};
            class Pistol {
                weapon      = "fow_w_type14";
				magazines[] = {{"fow_8Rnd_8x22", 5}};
            };

			items[]       = {{"lib_nb39", 1}, {"fow_e_type97", 1}, {"LIB_Binocular_GER", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "fow_b_ija_ammobox_wood";
                content[]   = {{"fow_30Rnd_77x58", 12}};
			};
		};
	};
};
class IFA_JP_MGAmmo : IFA_JP_Default {
  scope = 2;
	ROLE_IFA_LMG_AMMO_BEARER

	class Variants {
		class Variant1 {
			displayName = "Rifle";

            class Primary {
				weapon      = "fow_w_type99";
				magazines[] = {{"fow_5Rnd_77x58", 6}};
			};

			items[]       = {{"lib_nb39", 1}, {"fow_e_type97", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "fow_b_ija_backpack_foliage";
                content[]   = {{"fow_30Rnd_77x58", 6}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Pistol";

			class Primary {
				weapon      = "";
				magazines[] = {{"fow_8Rnd_8x22", 1}};
			};
            class Pistol {
                weapon      = "fow_w_type14";
				magazines[] = {{"fow_8Rnd_8x22", 5}};
            };


			items[]       = {{"lib_nb39", 1}, {"fow_e_type97", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};

            class Backpack {
				backpack    = "fow_b_ija_backpack_foliage";
                content[]   = {{"fow_30Rnd_77x58", 6}};
			};
		};
	};
};
class IFA_JP_CorporalSMG: IFA_JP_Default {
  scope = 2;
	ROLE_IFA_CORPORAL

    class Variants : Variants {
		class Variant1 : Variant1 {
      class Primary {
          weapon      = "fow_w_type100";
          magazines[] = {{"fow_32Rnd_8x22", 6}};
      };
			items[]       = {{"lib_nb39", 2}, {"LIB_Binocular_GER", 1}};
		};
	};
};
class IFA_JP_LAT : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_RIFLE_LAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "STR_LIB_GER_AT_grenadier";

			class Backpack {
                backpack = "fow_b_ija_backpack_foliage";
				content[] = {{"lib_pwm", 3}, {"FRL_ExplosiveCharge_Wpn", 2}};
			};
			items[]       = {{"lib_nb39", 1}};
		};
	};
};
class IFA_JP_HAT : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_RAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "LIB_RPzB";
				magazines[] = {{"LIB_1Rnd_RPzB", 1, 1}};
			};
			class Backpack {
                backpack = "fow_b_ija_backpack_foliage";
				content[] = {{"LIB_1Rnd_RPzB", 1}};
			};
		};
	};
};
class IFA_JP_HAT_Ammo : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_RAT_AMMO

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Backpack {
                backpack = "fow_b_ija_ammobox_wood";
				content[] = {{"LIB_1Rnd_RPzB", 4}};
			};
		};
	};
};
class IFA_JP_HAT_Ammo_1 : IFA_JP_Default {
	scope = 2;
	ROLE_IFA_RAT_AMMO

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_at_f0";

			class Backpack {
                backpack = "fow_b_ija_ammobox_wood";
				content[] = {{"LIB_1Rnd_RPzB", 5}};
			};
		};
	};
};
class IFA_JP_Sniper : IFA_JP_Default {
    scope = 2;
		ROLE_IFA_SNIPER

    class Clothing {
        uniform     = "fow_u_ija_type98_foliage";
        vest        = "fow_v_ija_bayonet";
        goggles     = "";
        headgear    = "fow_h_ija_type90_foliage";
    };

    class Variants : Variants {
        class Variant1 : Variant1 {
            displayName = "STR_LIB_GER_scout_sniper";

            class Primary {
                weapon      = "fow_w_type99_sniper";
                magazines[] = {{"fow_5Rnd_77x58", 8}};
            };
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
        };
    };
};

class IFA_JP_MortarTube : IFA_JP_Default {
	scope = 2;
	ROLE_MORTARMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar tube";
			items[]       = {{"LIB_Binocular_GER", 1}};
			class Secondary {
				weapon      = "LIB_GrWr34_Barrel";
				magazines[] = {{"LIB_1rnd_81mmHE_GRWR34", 1}};
			};
			class Backpack {
				backpack    = "fow_b_ija_ammobox_wood";
				content[] = {{"LIB_1rnd_81mmHE_GRWR34", 8}};
			};		
		};
	};
};

class IFA_JP_MortarAmmo : IFA_JP_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar Ammo";
			class Secondary {
				weapon      = "LIB_GrWr34_Tripod";
				magazines[] = {{"LIB_1rnd_81mmHE_GRWR34", 1}};
			};
			items[]       = {{"LIB_Binocular_GER", 1}};
			class Backpack {
				backpack    = "fow_b_ija_ammobox_wood";
				content[] = {{"LIB_1rnd_81mmHE_GRWR34", 8}};
			};
			
		};
	};
};

class IFA_JP_HMGLeader : IFA_JP_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[]       = {{"fow_e_type97", 1}, {"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
			class Primary : Primary {
				weapon      = "";
			};

            class Pistol {
                weapon      = "fow_w_type14";
				magazines[] = {{"fow_8Rnd_8x22", 5}};
            };
			class Backpack {
                backpack = "frl_staticammo_bag";
				content[] = {{"fow_30Rnd_77x58", 3}};
			};		
		};
	};
};

class IFA_JP_HMGAmmo : IFA_JP_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[]       = {{"fow_e_type97", 1}, {"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};

class IFA_JP_Crewman : IFA_JP_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing {
		uniform   = "fow_u_ija_pilot";
		headgear  = "fow_h_ija_tank_helmet";
		goggles   = "";
		vest      = "V_LIB_GER_OfficerBelt";
	};
	class Variants {
		class Crewman {
            displayName = "Crewman";
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
            class Pistol {
                weapon      = "fow_w_type14";
				magazines[] = {{"fow_8Rnd_8x22", 3}};
            };
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
            itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "fow_u_ija_pilot";
                headgear  = "fow_h_ija_flight_helmet";
                goggles   = "";
                vest      = "V_LIB_GER_OfficerBelt";
            };
			class Backpack {
                backpack = "B_LIB_US_TypeA3";
				content[] = {};
			};	
		items[]       = {};
        };
	};
};

class IFA_JP_Crewman_1 : IFA_JP_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing {
		uniform   = "fow_u_ija_pilot";
		headgear  = "fow_h_ija_tank_helmet";
		goggles   = "";
		vest      = "";
	};
	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"fow_8Rnd_8x22", 1}};
			};
            class Pistol {
                weapon      = "fow_w_type14";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"fow_8Rnd_8x22", 3}};
            };
			items[]       = {{"lib_nb39", 1}, {"LIB_Binocular_GER", 1}};
            itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};