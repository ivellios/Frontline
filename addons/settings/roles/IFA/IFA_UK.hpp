// IFA3 - USA default woodland

class IFA_UK_Default {
	scope = 0;
	ROLE_IFA_RIFLEMAN

	class Clothing {
		uniform   = "U_LIB_UK_P37";
		headgear  = "H_LIB_UK_HELMET_MK2";
		goggles   = "";
		vest      = "V_LIB_UK_P37_RIFLEMAN";
	};

	class Variants {
		class Variant1 {
			displayName = "Rifle";

			class Primary {
				weapon      = "LIB_LEEENFIELD_NO4";
				rail        = "";
				optics      = "";
				muzzle      = "LIB_ACC_NO4_MK2_BAYO";
				bipod       = "";
				magazines[] = {{"LIB_10Rnd_770x56", 6}};
			};

			items[]       = {{"LIB_MILLSBOMB", 2}, {"LIB_No77", 1}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
	};
};

class IFA_UK_Rifleman : IFA_UK_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {
			items[]       = {{"LIB_MILLSBOMB", 3}, {"LIB_No77", 1}};
		};
	};
};

class IFA_UK_Grenade_Rifleman : IFA_UK_Default {
	scope = 2;
    ROLE_IFA_GRENADIER
	class Variants: Variants {
		class Variant1: Variant1 {
	        displayName = "Rifle grenades";

			class Primary {
				weapon      = "LIB_LEEENFIELD_NO4";
				muzzle      = "LIB_ACC_GL_Enfield_CUP_Empty";
				magazines[] = {{"LIB_1Rnd_G_MillsBomb", 5}, {"LIB_10Rnd_770x56", 6}};
			};
		};
	};
};


class IFA_UK_SquadLeader : IFA_UK_Default {
	scope = 2;
	ROLE_IFA_OFFICER

    class Clothing: Clothing {
        uniform   = "U_LIB_UK_P37JERKINS_SERGEANT";
		headgear  = "H_LIB_UK_HELMET_MK2_CAMO";
		vest      = "V_LIB_UK_P37_OFFICER";
		goggles   = "";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SMG";

			class Primary {
				weapon      = "LIB_STEN_MK2";
				magazines[] = {{"LIB_32Rnd_9x19_Sten", 5}};
			};

			class Pistol {
                weapon      = "LIB_WEBLEY_MK6";
				magazines[] = {{"LIB_6Rnd_455", 3}};
            };
			items[]       = {{"LIB_No77", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_UK", 1}};
		};

		class Variant2: Variant1 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "LIB_LEEENFIELD_NO4";
				muzzle      = "LIB_ACC_NO4_MK2_BAYO";
				magazines[] = {{"LIB_10Rnd_770x56", 8}};
			};
			items[]       = {{"LIB_MILLSBOMB", 2}, {"LIB_No77", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_UK", 1}};
		};
	};
};

class IFA_UK_Medic : IFA_UK_Default {
	scope = 2;
	ROLE_IFA_COMBAT_MEDIC


	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "LIB_LEEENFIELD_NO4";
				magazines[] = {{"LIB_10Rnd_770x56", 8}};
			};
			items[]       = {};

			class Backpack {
                backpack = "B_LIB_UK_HSACK";
				content[] = {{"LIB_MILLSBOMB", 2}, {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_No77", 7}};
			};
		};
				class Variant2: Variant1 {
			displayName = "Pistol";

			class Primary : Primary {
				weapon      = "";
			};
			items[]       = {};

            class Pistol {
                weapon      = "LIB_WEBLEY_MK6";
				magazines[] = {{"LIB_6Rnd_455", 5}};
            };
			class Backpack {
                backpack = "B_LIB_UK_HSACK";
				content[] = {{"LIB_MILLSBOMB", 1}, {"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_No77", 7}};
			};
		};
	};
};

class IFA_UK_MG : IFA_UK_Default {
	scope = 2;
	ROLE_IFA_LMG

    class Clothing: Clothing {
        uniform   = "U_LIB_UK_P37";
		vest      = "V_LIB_UK_P37_HEAVY";
		headgear	= "H_LIB_UK_HELMET_MK2";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Auto Rifleman";

			class Primary : Primary {
				weapon      = "LIB_BREN_MK2";
				muzzle      = "";
				magazines[] = {{"LIB_30Rnd_770x56", 12}};
			};
			items[]       = {{"LIB_No77", 1}};

			class Backpack {
				backpack = "B_LIB_UK_HSACK";
				content[] = {};
			};
		};
	};
};
class IFA_UK_MG_1 : IFA_UK_MG {
};

/*
class IFA_UK_HMG : IFA_UK_Default {
  scope = 2;
	ROLE_IFA_HMG

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HMG";
			items[]       = {{"LIB_No77", 1}};

      class Backpack {
				backpack    = "";
				content[]   = {};
			};
		};
	};
};

// -- Assistant gunner / ammo carrier
class IFA_UK_MGAssistant : IFA_UK_Default {
  scope = 2;
	ROLE_IFA_ASSISTANT_GUNNER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HMG Ammo";

      class Backpack {
				backpack    = "B_LIB_UK_HSACK";
				content[]   = {{"LIB_50Rnd_762x63", 2}};
			};
			items[]       = {{"LIB_No77", 2}, {"LIB_BINOCULAR_UK", 1}};
		};
	};
};
*/
class IFA_UK_MGAssistant_1 : IFA_UK_Default {
};



class IFA_UK_LAT : IFA_UK_Default {
	scope = 2;
	ROLE_IFA_RIFLE_LAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AT Explosives";

			class Backpack {
				backpack = "B_LIB_UK_HSACK";
				content[] = {{"FRL_ExplosiveCharge_Wpn", 1}};
			};
			items[]       = {{"LIB_No82", 5}, {"LIB_No77", 1}};
		};
		class Variant2 : Variant1 {
			displayName = "PIAT";

			class Secondary {
				weapon      = "LIB_PIAT";
				magazines[] = {{"LIB_1Rnd_89m_PIAT", 1}};
			};

      class Backpack {
                backpack = "B_LIB_UK_HSACK_AT";
                content[] = {{"LIB_1Rnd_89m_PIAT", 2}};
            };
		};
	};
};


class IFA_UK_HAT : IFA_UK_Default {
	scope = 2;

  ROLE_IFA_RAT
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Anti-Tank";

			class Secondary {
				weapon      = "LIB_M1A1_Bazooka";
				magazines[] = {{"LIB_1Rnd_60mm_M6", 1,1}};
			};
			class Pistol {
				weapon      = "LIB_WEBLEY_MK6";
				magazines[] = {{"LIB_6Rnd_455", 5}};
			};
      class Backpack {
                backpack = "B_LIB_UK_HSACK_AT";
				content[] = {{"LIB_1Rnd_60mm_M6", 2}};
			};
		};
	};
};
class IFA_UK_HAT_Ammo : IFA_UK_Default {
  scope = 2;

    ROLE_IFA_RAT_AMMO
  class Variants : Variants {
    class Variant1 : Variant1 {
      displayName = "Anti-Tank Ammo";

      class Backpack {
                backpack = "B_LIB_UK_HSACK_AT";
				content[] = {{"LIB_1Rnd_60mm_M6", 4}};
      };
    };
  };
};
class IFA_UK_HAT_Ammo_1 : IFA_UK_HAT_Ammo {
};

class IFA_UK_Engineer : IFA_UK_Default {
    scope = 2;
    ROLE_ENGINEER

	class Clothing {
		uniform   = "U_LIB_UK_P37";
		headgear  = "H_LIB_UK_HELMET_MK2";
		goggles   = "";
		vest      = "V_LIB_UK_P37_RIFLEMAN";
	};

    class Variants : Variants {
 		class Variant1: Variant1 {
			displayName = "Combat Engineer";

			class Backpack {
                backpack = "B_LIB_UK_HSACK_AT";
				content[] = {{"FRL_ExplosiveCharge_Wpn", 3},{"LIB_Ladung_Big_MINE_mag", 1},{"LIB_No82", 3}};
			};
		};
	};
};

class IFA_UK_Sniper : IFA_UK_Default {
    scope = 2;
    ROLE_IFA_SNIPER
    class Clothing {
        uniform     = "U_LIB_UK_P37";
        vest        = "V_LIB_UK_P37_Officer_Blanco";
        goggles     = "";
        headgear    = "H_LIB_UK_Beret";
    };

    class Variants : Variants {
        class Variant1 : Variant1 {
            displayName = "Sniper";

            class Primary {
                weapon      = "LIB_LeeEnfield_No4_Scoped";
                magazines[] = {{"LIB_10Rnd_770x56", 4}};
            };
			items[]       = {{"LIB_No77", 1}};
        };
    };
};

class IFA_UK_Spotter : IFA_UK_Default {
	scope = 2;
    ROLE_IFA_SPOTTER
    class Clothing {
        uniform     = "U_LIB_UK_P37";
        vest        = "V_LIB_UK_P37_Officer_Blanco";
        goggles     = "";
        headgear    = "H_LIB_UK_Beret";
    };

	class Variants : Variants {
		class Variant1 :Variant1 {
			items[]       = {{"LIB_MILLSBOMB", 3}, {"LIB_No77", 1}, {"LIB_BINOCULAR_UK", 1}};
		};
	};
};

class IFA_UK_Corporal: IFA_UK_Default {
	scope = 2;
    ROLE_IFA_CORPORAL

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "LIB_STEN_MK2";
				magazines[] = {{"LIB_32Rnd_9x19_Sten", 6}};
			};
			items[]       = {{"LIB_No77", 1}};
        };
    };
};

class IFA_UK_MortarTube : IFA_UK_Default {
	scope = 2;
	ROLE_MORTARMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar tube";
			class Secondary {
				weapon      = "LIB_M2_60_Barrel";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_60mmHE_M2", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_UK_HSACK_AT";
				content[] = {{"LIB_1rnd_60mmHE_M2", 9}};
			};
			items[]       = {{"LIB_No77", 1}};
		};
	};
};

class IFA_UK_MortarAmmo : IFA_UK_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mortar Ammo";
			class Secondary {
				weapon      = "LIB_M2_60_Tripod";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"LIB_1rnd_60mmHE_M2", 1}};
			};
			class Backpack {
				backpack    = "B_LIB_UK_HSACK_AT";
				content[] = {{"LIB_1rnd_60mmHE_M2", 9}};
			};
			items[]       = {{"LIB_No77", 1}};
		};
	};
};
class IFA_UK_HMGLeader : IFA_UK_Default {
	scope = 2;
	ROLE_STATICLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
            class Primary : Primary {
				weapon      = "";
			};

            class Pistol {
                weapon      = "LIB_WEBLEY_MK6";
				magazines[] = {{"LIB_6Rnd_455", 5}};
            };
			class Backpack {
                backpack = "frl_staticfortify_bag";
				content[] = {{"LIB_50Rnd_762x63", 3}};
			};
		};
	};
};
class IFA_UK_HMGAmmo : IFA_UK_Default {
	scope = 2;
	ROLE_AMMO_BEARER

	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {COMMON_STATICTEAM};
			class Backpack {
				backpack    = "frl_staticammo_bag";
			};
		};
	};
};
class IFA_UK_Crewman : IFA_UK_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing {
		uniform   = "U_LIB_UK_P37_CORPORAL";
		headgear  = "H_LIB_UK_BERET_HEADSET";
		goggles   = "";
		vest      = "V_LIB_UK_P37_OFFICER";
	};

	class Variants {
		class Crewman {
            displayName = "Crewman";
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
            class Pistol {
                weapon      = "LIB_WEBLEY_MK6";
				magazines[] = {{"LIB_6Rnd_455", 3}};
            };
		items[]       = {{"LIB_No77", 1}, {"LIB_BINOCULAR_UK", 1}};
        itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing {
                uniform   = "U_LIB_US_Pilot";
                headgear  = "H_LIB_US_Helmet_Pilot_Glasses_Up";
                goggles   = "";
                vest      = "V_LIB_US_LifeVest";
            };
			class Backpack {
                backpack = "B_LIB_US_TypeA3";
				content[] = {};
			};
		items[]       = {};
        };
	};
};
class IFA_UK_Flamethrower : IFA_UK_Default {
    scope = 2;
    ROLE_FLAMETHROWER

    class Variants : Variants {
		class Variant1 :Variant1 {
            displayName = "Flamethrower";
            class Primary {
				weapon      = "LIB_M2_Flamethrower";
				magazines[] = {};
			};
			class Backpack {
                backpack = "B_LIB_US_M2Flamethrower";
				content[] = {{"LIB_M2_Flamethrower_Mag", 1}};
			};

			items[]       = {{"LIB_No77", 1}};
		};
	};
};
