// IFA3 - USSR default winter

#define UNIFORM_USSR_W class Clothing {\
		uniform   = "U_LIB_SOV_Strelok_w";\
		headgear  = "H_LIB_SOV_RA_Helmet_w";\
		goggles   = "";\
		vest      = "V_LIB_SOV_RA_MosinBelt";\
	};

class IFA_USSR_W_Default : IFA_USSR_Default {
	UNIFORM_USSR_W
};

class IFA_USSR_W_Rifleman : IFA_USSR_Rifleman {
	UNIFORM_USSR_W
};

class IFA_USSR_W_Rifleman_Semi : IFA_USSR_Rifleman_Semi {
	UNIFORM_USSR_W
};

class IFA_USSR_W_Grenade_Rifleman : IFA_USSR_Grenade_Rifleman {
    UNIFORM_USSR_W
};

class IFA_USSR_W_SquadLeader : IFA_USSR_SquadLeader {
	class Clothing : Clothing {
        uniform   = "U_LIB_SOV_Strelok_w";
		headgear  = "H_LIB_SOV_RA_Helmet_w";
    };
};

class IFA_USSR_W_Medic : IFA_USSR_Medic {
	UNIFORM_USSR_W
};

class IFA_USSR_W_MG : IFA_USSR_MG {
	class Clothing: Clothing {
    	uniform   = "U_LIB_SOV_Strelok_w";
  		headgear  = "H_LIB_SOV_RA_Helmet_w";
    };
};

class IFA_USSR_W_MGAssistant : IFA_USSR_MGAssistant {
	UNIFORM_USSR_W
};

class IFA_USSR_W_HMG : IFA_USSR_HMG {
	class Clothing: Clothing {
    	uniform   = "U_LIB_SOV_Strelok_w";
    	headgear  = "H_LIB_SOV_RA_Helmet_w";
    };
};

class IFA_USSR_W_HMGAssistant : IFA_USSR_HMGAssistant {
	UNIFORM_USSR_W
};

class IFA_USSR_W_MGAmmo : IFA_USSR_MGAmmo {
	UNIFORM_USSR_W
};

class IFA_USSR_W_LAT : IFA_USSR_LAT {
	UNIFORM_USSR_W
};

class IFA_USSR_W_HAT : IFA_USSR_HAT {
	UNIFORM_USSR_W
};

class IFA_USSR_W_HAT_Ammo : IFA_USSR_HAT_Ammo {
	UNIFORM_USSR_W
};

class IFA_USSR_W_HAT_Ammo_1 : IFA_USSR_HAT_Ammo {
	UNIFORM_USSR_W
};

class IFA_USSR_W_Sniper : IFA_USSR_Sniper {
	class Clothing : Clothing {
        uniform     = "U_LIB_SOV_Sniper_w";
    };
};

class IFA_USSR_W_Spotter : IFA_USSR_Spotter {
	class Clothing : Clothing {
        uniform     = "U_LIB_SOV_Sniper_w";
    };
};

class IFA_USSR_W_SMG: IFA_USSR_SMG {
	class Clothing : Clothing {
      uniform     = "U_LIB_SOV_Strelok_w";
      headgear    = "H_LIB_SOV_RA_Helmet_w";
  };
};

class IFA_USSR_W_MortarTube : IFA_USSR_MortarTube {
	UNIFORM_USSR_W
};

class IFA_USSR_W_MortarAmmo : IFA_USSR_MortarAmmo {
	UNIFORM_USSR_W
};

class IFA_USSR_W_HMGLeader : IFA_USSR_HMGLeader {
	UNIFORM_USSR_W
};

class IFA_USSR_W_HMGAmmo : IFA_USSR_HMGAmmo {
	UNIFORM_USSR_W
};
class IFA_USSR_W_Crewman : IFA_USSR_Crewman {
    UNIFORM_USSR_W
};

class IFA_USSR_W_Flamethrower: IFA_USSR_Flamethrower {
	UNIFORM_USSR_W
};