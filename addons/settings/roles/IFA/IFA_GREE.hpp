// IFA3 - Greek Partisans

#define UNIFORM_GREE class Clothing {\
		uniform   = "U_LIB_CIV_Citizen_2";\
		headgear  = "H_StrawHat_dark";\
		goggles   = "";\
		vest      = "V_LIB_WP_Kar98Vest";\
	};

class IFA_GREE_Default: IFA_GER_Default {
	UNIFORM_GREE
};

class IFA_GREE_Rifleman: IFA_GER_Rifleman {
	UNIFORM_GREE
};

class IFA_GREE_Rifleman_Semi: IFA_GER_Rifleman_Semi {
	UNIFORM_GREE
};

class IFA_GREE_Grenadier_Rifleman: IFA_GER_Grenadier_Rifleman {
	UNIFORM_GREE
};

class IFA_GREE_SquadLeader: IFA_GER_SquadLeader {
	class Clothing {
		uniform   = "U_LIB_CIV_Citizen_6";
		headgear  = "H_Beret_blk";
		goggles   = "G_LIB_Watch2";
		vest      = "V_LIB_GER_FieldOfficer";
	};
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "MP40";

      class Primary {
				weapon      = "LIB_MP40";
				magazines[] = {{"LIB_32Rnd_9x19", 5}};
			};
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};

		};
		class Variant2 : Variant1 {
            displayName = "Sten";
			class Primary {
				weapon      = "LIB_STEN_MK2";
				magazines[] = {{"LIB_32Rnd_9x19_Sten", 6}};
			};
        };
		class Variant3: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_G43";
				magazines[] = {{"LIB_10Rnd_792x57", 6}};
			};
            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 3}};
            };
			items[]       = {{"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
		};
		class Variant4: Variant1 {
			displayName = "Rifle";

			class Primary : Primary {
				weapon      = "LIB_K98";
				magazines[] = {{"LIB_5Rnd_792x57", 8}};
				muzzle      = "LIB_ACC_K98_Bayo";
			};
            class Pistol {
                weapon      = "LIB_WaltherPPK";
				magazines[] = {{"LIB_7Rnd_765x17_PPK", 3}};
            };
			items[]       = {{"LIB_Molotov_Mag", 2}, {"lib_nb39", 2}, {"LIB_US_M18_Red", 1}, {"LIB_Binocular_GER", 1}};
		};
	};
};

class IFA_GREE_Medic: IFA_GER_Medic {
	class Clothing {
		uniform   = "U_LIB_CIV_Schoolteacher";
		headgear  = "H_Hat_brown";
		goggles   = "G_LIB_Dienst_Brille";
		vest      = "V_LIB_DAK_PioneerVest";
	};
};

class IFA_GREE_MG: IFA_GER_MG {
	class Clothing {
		uniform   = "U_LIB_CIV_Citizen_1";
		headgear  = "H_LIB_CIV_Worker_Cap_4";
		goggles   = "";
		vest      = "V_LIB_WP_MGVest";
	};
};

class IFA_GREE_MGAssistant: IFA_GER_MGAssistant {
	UNIFORM_GREE
};

class IFA_GREE_MGAmmo: IFA_GER_MGAmmo {
	UNIFORM_GREE
};

class IFA_GREE_CorporalSMG: IFA_GER_CorporalSMG {
    ROLE_IFA_SMG
	class Clothing {
		uniform   = "U_LIB_CIV_Citizen_3";
		headgear  = "H_LIB_CIV_Villager_Cap_1";
		goggles   = "";
		vest      = "V_LIB_WP_MP40Vest";
    };
        
    class Variants : Variants {
		class Variant1 : Variant1 {
            displayName = "MP40";
          class Primary {
              weapon      = "LIB_MP40";
              magazines[] = {{"LIB_32Rnd_9x19", 6}};
          };
			items[]       = {{"lib_nb39", 2}};
		};
		class Variant2 : Variant1 {
            displayName = "Sten";
			class Primary {
				weapon      = "LIB_STEN_MK2";
				magazines[] = {{"LIB_32Rnd_9x19_Sten", 6}};
			};
        };
	};
};

class IFA_GREE_LAT: IFA_GER_LAT {
	UNIFORM_GREE
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AT Rifle + AT grenades";
            
			class Primary {
				weapon      = "LIB_PTRD";
				magazines[] = {{"LIB_1Rnd_145x114", 16}};
			};
            class Pistol {
                weapon      = "LIB_P08";
				magazines[] = {{"LIB_8Rnd_9x19_P08", 3}};
            };
			class Backpack {
        		backpack = "B_LIB_GER_Backpack";
				content[] = {{"lib_pwm", 3}};
			};
		};
		class Variant2: Variant1 {
			displayName = "AT Rifle + Panzerfaust";

			class Secondary {
				weapon      = "LIB_PzFaust_60m";
				magazines[] = {{"LIB_1Rnd_PzFaust_60m", 1, 1}};
			};
			class Backpack {
        		backpack = "";
			};
			items[]       = {{"LIB_Molotov_Mag", 2}, {"lib_nb39", 1}};
		};
	};
};


class IFA_GREE_LAT_1: IFA_GREE_LAT {};

class IFA_GREE_HAT: IFA_GER_HAT {
	UNIFORM_GREE
};

class IFA_GREE_HAT_Ammo: IFA_GER_HAT_Ammo {
	UNIFORM_GREE
};

class IFA_GREE_HAT_Ammo_1: IFA_GER_HAT_Ammo {
	UNIFORM_GREE
};

class IFA_GREE_Sniper: IFA_GER_Sniper {
	UNIFORM_GREE
};

class IFA_GREE_HMGLeader : IFA_GER_HMGLeader {
	UNIFORM_GREE
};

class IFA_GREE_HMGAmmo : IFA_GER_HMGAmmo {
	UNIFORM_GREE
};

class RHS_GREE_Baserole : IFA_GREE_Default {
	scope = 0;
	ROLE_RIFLEMAN
    class Clothing {
        uniform     = "U_LIB_CIV_Citizen_2";
        vest        = "";
        goggles     = "";
        headgear    = "H_StrawHat_dark";
    };
	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "LIB_LEEENFIELD_NO4";
				magazines[] = {{"LIB_10Rnd_770x56", 3}};
			};
			class Backpack {
                backpack = "B_LIB_SOV_RA_Rucksack_Green";
				content[] = {};
			};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
	};
};
/*
class IFA_GREE_Crewman : IFA_GER_Crewman {
	class Clothing : Clothing {
        uniform     = "U_LIB_DAK_Spg_crew_unterofficer";
		vest      	= "V_LIB_DAK_OfficerVest";
        headgear    = "H_LIB_DAK_Cap";
		goggles  	= "G_LIB_Scarf2_G";
    };
};

class IFA_GREE_Crewman_1 : IFA_GER_Crewman_1 {
	class Clothing : Clothing {
        uniform     = "U_LIB_DAK_Spg_crew_private";
		vest      	= "V_LIB_DAK_PrivateBelt";
        headgear    = "H_LIB_DAK_Cap";
		goggles  	= "G_LIB_Scarf2_G";
    };
};
*/