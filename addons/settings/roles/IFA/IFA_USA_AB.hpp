// IFA3 - USA Airborne

#define UNIFORM_USA_AB class Clothing {\
		uniform   = "U_LIB_US_AB_Uniform_M42";\
		headgear  = "H_LIB_US_AB_Helmet_5";\
		goggles   = "G_LIB_GER_Gloves5";\
		vest      = "V_LIB_US_AB_Vest_Garand";\
	};

class IFA_USA_AB_Default : IFA_USA_Default {
	UNIFORM_USA_AB
};

class IFA_USA_AB_Rifleman: IFA_USA_Default {
	scope = 2;
    ROLE_RIFLEMAN
    class Clothing {
        uniform     = "U_LIB_US_AB_Uniform_M42";
        vest        = "V_LIB_US_AB_Vest_Carbine";
        headgear    = "H_LIB_US_AB_Helmet_2";
        goggles     = "G_LIB_GER_Gloves5";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Carabine";
			class Primary {
				weapon      = "LIB_M1A1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};
			class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 3}};
            };
/*
			class Backpack {
                backpack = "B_LIB_US_Type5";
			};
*/
			items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_US_M18", 2}};
			itemshidden[] = {COMMON_ITEMSHIDDEN_IFA, COMMON_FIRSTAID};
        };
		class Variant2: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_M1_Garand";
				magazines[] = {{"LIB_8Rnd_762x63", 6}};
			};
		};
    };
};


class IFA_USA_AB_SquadLeader: IFA_USA_AB_Rifleman {
	scope = 2;
    ROLE_IFA_OFFICER
    class Clothing {
        uniform     = "U_LIB_US_AB_Uniform_M42";
        vest        = "V_LIB_US_AB_Vest_Thompson_nco";
        headgear    = "H_LIB_US_AB_Helmet_CO_1";
        goggles     = "G_LIB_GER_Gloves5";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Carabine";
			
			items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_US_M18", 2}, {"LIB_US_M18_Red", 1}, {"LIB_BINOCULAR_UK", 1}};
        };
		class Variant2: Variant1 {
			displayName = "SMG";
			class Primary {
				weapon      = "LIB_M1A1_THOMPSON";
				magazines[] = {{"LIB_30RND_45ACP", 5}};
			};
        };
		class Variant3: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_M1_Garand";
				magazines[] = {{"LIB_8Rnd_762x63", 6}};
			};
		};
	};
};

class IFA_USA_AB_Medic: IFA_USA_AB_Rifleman {
	scope = 2;
    ROLE_IFA_COMBAT_MEDIC
    class Clothing {
        uniform     = "U_LIB_US_AB_Uniform_M42";
        vest        = "V_LIB_US_AB_Vest_Garand";
        headgear    = "H_LIB_US_AB_Helmet_Medic_1";
        goggles     = "G_LIB_GER_Gloves5";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {

		items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_BINOCULAR_UK", 1}};
			class Backpack {
                backpack = "B_LIB_US_MedicBackpack_Empty";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_epinephrine", 10}, {"LIB_US_M18", 7}};
			};
        };
	};
};

class IFA_USA_AB_MG: IFA_USA_AB_Rifleman {
	scope = 2;
    ROLE_IFA_LMG
    class Clothing {
        uniform     = "U_LIB_US_AB_Uniform_M42";
        vest        = "V_LIB_US_AB_Vest_M1919";
        headgear    = "H_LIB_US_AB_Helmet_5";
        goggles     = "G_LIB_GER_Gloves5";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "LIB_M1918A2_BAR";
				magazines[] = {{"LIB_20RND_762X63", 10}};
			};

			items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_US_M18", 1}, {"LIB_BINOCULAR_UK", 1}};
        };
    };
};

class IFA_USA_AB_Corporal: IFA_USA_AB_Rifleman {
	scope = 2;
    ROLE_IFA_CORPORAL
    class Clothing {
        uniform     = "U_LIB_US_AB_Uniform_M42";
        vest        = "V_LIB_US_AB_Vest_Thompson";
        headgear    = "H_LIB_US_AB_Helmet";
        goggles     = "G_LIB_GER_Gloves5";
    };

   class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Carabine";
			
			items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_US_M18", 2}};
        };
		class Variant2: Variant1 {
			displayName = "SMG";
			class Primary {
				weapon      = "LIB_M1A1_THOMPSON";
				magazines[] = {{"LIB_30RND_45ACP", 5}};
			};
        };
		class Variant3: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_M1_Garand";
				magazines[] = {{"LIB_8Rnd_762x63", 6}};
			};
		};
	};
};

class IFA_USA_AB_Engineer : IFA_USA_AB_Rifleman {
    scope = 2;
    ROLE_ENGINEER
	
    class Clothing {
        uniform     = "U_LIB_US_AB_Uniform_M42";
        vest        = "V_LIB_US_AB_Vest_Garand_eng";
        headgear    = "H_LIB_US_AB_Helmet_Jump_1";
        goggles     = "G_LIB_GER_Gloves5";
    };

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Carabine";
            class Pistol {
                weapon      = "LIB_Colt_M1911";
				magazines[] = {{"LIB_7Rnd_45ACP", 2}};
			};
			class Backpack {
                backpack = "B_LIB_US_Backpack";
				content[] = {{"FRL_ExplosiveCharge_Wpn", 1},{"LIB_Ladung_Big_MINE_mag", 1},{"LIB_StickyNade_Mag", 3}};
			};
			items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_US_M18", 1}};
		};
		class Variant2: Variant1 {
			displayName = "Semi-auto Rifle";

			class Primary : Primary {
				weapon      = "LIB_M1_Garand";
				magazines[] = {{"LIB_8Rnd_762x63", 6}};
			};
		};
    };
};

class IFA_USA_AB_Grenade_Rifleman : IFA_USA_Grenade_Rifleman {
    UNIFORM_USA_AB
};

class IFA_USA_AB_HAT : IFA_USA_HAT {
	UNIFORM_USA_AB
	/*
  class Variants : Variants {
    class Variant1 : Variant1 {
			class Backpack {
            backpack = "B_LIB_US_Type5";
			};
		};
	};
	*/
};

class IFA_USA_AB_HAT_Ammo : IFA_USA_HAT_Ammo {
	UNIFORM_USA_AB
	/*
  class Variants : Variants {
    class Variant1 : Variant1 {
			class Backpack {
            backpack = "B_LIB_US_Type5";
			content[] = {{"LIB_1Rnd_60mm_M6", 4}};
			};
		};
	};
	*/
};

class IFA_USA_AB_Sniper : IFA_USA_Sniper {
	UNIFORM_USA_AB
	/*
  class Variants : Variants {
    class Variant1 : Variant1 {
			class Backpack {
            backpack = "B_LIB_US_Type5";
			};
		};
	};
	*/
};

class IFA_USA_AB_Spotter : IFA_USA_Spotter {
	UNIFORM_USA_AB
};


class IFA_USA_AB_HMGLeader : IFA_USA_HMGLeader {
	UNIFORM_USA_AB
};

class IFA_USA_AB_HMGAmmo : IFA_USA_HMGAmmo {
	UNIFORM_USA_AB
};

class IFA_USA_AB_MortarTube: IFA_USA_MortarTube {
	UNIFORM_USA_AB
};

class IFA_USA_AB_MortarAmmo: IFA_USA_MortarAmmo {
	UNIFORM_USA_AB
};