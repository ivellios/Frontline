// IFA3 - USA default pacific
// IFA3 - USA default pacific

#define UNIFORM_USA_P class Clothing {\
		uniform   = "fow_u_usmc_p42_01_camo02_1_private";\
		headgear  = "fow_h_usmc_m1_camo_02";\
		goggles   = "";\
		vest      = "fow_v_usmc_garand";\
	};

class IFA_USA_P_Default : IFA_USA_Default {
	UNIFORM_USA_P
	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "fow_w_m1903A1";
				magazines[] = {{"fow_5Rnd_762x63", 8}};
			};
		};
	};
};

class IFA_USA_P_Rifleman : IFA_USA_Rifleman {
	UNIFORM_USA_P
   class Variants : Variants {
 		class Variant1: Variant1 {
      displayName = "Rifle";
			class Primary : Primary {
				weapon      = "fow_w_m1903A1";
				magazines[] = {{"fow_5Rnd_762x63", 8}};
			};
			items[]       = {{"LIB_US_Mk_2", 3}, {"LIB_US_M18", 1}};
		};
 		class Variant2: Variant1 {
      displayName = "Shotgun";
			class Primary : Primary {
				weapon      = "fow_w_M1912";
				magazines[] = {{"fow_6Rnd_12G_Pellets", 8}};
			};
			items[]       = {{"LIB_US_Mk_2", 1}, {"LIB_US_M18", 1}};
		};
	};
};

class IFA_USA_P_Rifleman_Semi : IFA_USA_P_Rifleman {
	ROLE_IFA_RIFLEMAN_1STCLASS
	UNIFORM_USA_P
  class Variants : Variants {
 		class Variant1: Variant1 {
      displayName = "Rifle";
			class Primary : Primary {
				weapon      = "LIB_M1_Garand";
				magazines[] = {{"LIB_8Rnd_762x63", 6}};
			};
			items[]       = {{"LIB_US_M18", 1}};
		};
 		class Variant2: Variant1 {
      displayName = "Carabine";
			class Primary : Primary {
				weapon      = "LIB_M1A1_CARBINE";
				magazines[] = {{"LIB_15RND_762X33", 9}};
			};
		};
	};
};
/*
class IFA_USA_P_Grenade_Rifleman : IFA_USA_Grenade_Rifleman {
    UNIFORM_USA_P
};
*/
class IFA_USA_P_SquadLeader : IFA_USA_SquadLeader {
	class Clothing : Clothing {
        uniform   = "fow_u_usmc_p42_01_camo02_1_private";
		headgear  = "fow_h_usmc_m1";
    };
};

class IFA_USA_P_Medic : IFA_USA_Medic {
	UNIFORM_USA_P
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "fow_w_m1903A1";
				magazines[] = {{"fow_5Rnd_762x63", 8}};
			};
			items[]       = {{"LIB_US_Mk_2", 1}};
		};
	};
};

class IFA_USA_P_Engineer : IFA_USA_Engineer {
	UNIFORM_USA_P
};

class IFA_USA_P_MG : IFA_USA_MG {
	class Clothing: Clothing {
    	uniform   = "fow_u_usmc_p42_01_camo02_1_private";
  		headgear  = "fow_h_usmc_m1_camo_02";
    };
};

class IFA_USA_P_MGAssistant : IFA_USA_MGAssistant {
	UNIFORM_USA_P
};


class IFA_USA_P_HMG : IFA_USA_HMG {
	class Clothing: Clothing {
    	uniform   = "fow_u_usmc_p42_01_camo02_1_private";
    	headgear  = "fow_h_usmc_m1_camo_02";
    };
};
/*
class IFA_USA_P_HMGAssistant : IFA_USA_HMGAssistant {
	UNIFORM_USA_P
};

class IFA_USA_P_MGAmmo : IFA_USA_MGAmmo {
	UNIFORM_USA_P
};

class IFA_USA_P_LAT : IFA_USA_LAT {
	UNIFORM_USA_P
};
*/
class IFA_USA_P_HAT : IFA_USA_HAT {
	UNIFORM_USA_P
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "fow_w_m1903A1";
				magazines[] = {{"fow_5Rnd_762x63", 8}};
			};

			class Secondary {
				weapon      = "LIB_M1A1_Bazooka";
				magazines[] = {{"LIB_1Rnd_60mm_M6", 1,1}};
			};

		};
	};
};

class IFA_USA_P_HAT_Ammo : IFA_USA_HAT_Ammo {
	UNIFORM_USA_P
  class Variants : Variants {
    class Variant1 : Variant1 {
      displayName = "Anti-Tank Ammo";

      class Primary : Primary {
        weapon      = "fow_w_m1903A1";
        magazines[] = {{"fow_5Rnd_762x63", 8}};
      };

      class Backpack {
                backpack = "B_LIB_US_RocketBag_Empty";
        content[] = {{"LIB_1Rnd_60mm_M6", 4}};
      };
    };
  };
};
class IFA_USA_P_HAT_Ammo_1 : IFA_USA_HAT_Ammo {
	UNIFORM_USA_P
  class Variants : Variants {
    class Variant1 : Variant1 {
      displayName = "Anti-Tank Ammo";

      class Primary : Primary {
        weapon      = "fow_w_m1903A1";
        magazines[] = {{"fow_5Rnd_762x63", 8}};
      };

      class Backpack {
                backpack = "B_LIB_US_RocketBag_Empty";
        content[] = {{"LIB_1Rnd_60mm_M6", 4}};
      };
    };
  };
};

class IFA_USA_P_Sniper : IFA_USA_Sniper {
	class Clothing : Clothing {
        uniform     = "fow_u_usmc_p42_01_camo02_1_private";
        headgear    = "fow_h_us_daisy_mae_01";
    };
};


class IFA_USA_P_Spotter : IFA_USA_Spotter {
	class Clothing : Clothing {
        uniform     = "fow_u_usmc_p42_01_camo02_1_private";
        headgear    = "fow_h_us_daisy_mae_01";
    };
};

class IFA_USA_P_Corporal: IFA_USA_Corporal {
	class Clothing : Clothing {
        uniform   = "fow_u_usmc_p42_01_camo02_1_private";
		headgear  = "fow_h_usmc_m1_camo_02";
    };

    class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "fow_w_m55_reising";
				magazines[] = {{"fow_20Rnd_45acp", 6}};
			};
        };
    };
};

class IFA_USA_P_HMGLeader : IFA_USA_HMGLeader {
	UNIFORM_USA_P
};

class IFA_USA_P_HMGAmmo : IFA_USA_HMGAmmo {
	UNIFORM_USA_P
};
class IFA_USA_P_Crewman : IFA_USA_Crewman {
    UNIFORM_USA_P
};

class IFA_USA_P_Flamethrower: IFA_USA_Flamethrower {
	UNIFORM_USA_P
};
class IFA_USA_P_MortarTube: IFA_USA_MortarTube {
	UNIFORM_USA_P
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "fow_w_m1903A1";
				magazines[] = {{"fow_5Rnd_762x63", 8}};
			};
			class Secondary {
				weapon      = "LIB_M2_60_Barrel";
				magazines[] = {{"LIB_1rnd_60mmHE_M2", 1}};
			};
			items[]       = {{"LIB_US_M18", 1}};
			class Backpack {
				backpack    = "B_LIB_US_Backpack";
				content[] = {{"LIB_1rnd_60mmHE_M2", 9}};
			};			
		};
	};
};

class IFA_USA_P_MortarAmmo: IFA_USA_MortarAmmo {
	UNIFORM_USA_P
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary {
				weapon      = "fow_w_m1903A1";
				magazines[] = {{"fow_5Rnd_762x63", 8}};
			};
			class Secondary {
				weapon      = "LIB_M2_60_Tripod";
				magazines[] = {{"LIB_1rnd_60mmHE_M2", 1}};
			};
			items[]       = {{"LIB_US_M18", 1}};
			class Backpack {
				backpack    = "B_LIB_US_Backpack";
				content[] = {{"LIB_1rnd_60mmHE_M2", 9}};
			};			
		};
	};
};