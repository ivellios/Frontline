// UNS Vietcong + NVA  
#define VC_ITEMS {"SmokeShell", 1}, {"uns_rgd33gren", 1}
#define VC_SUPPORT {"SmokeShell", 1}
#define VC_CREWMAN {"SmokeShell", 1}, {"uns_binocular_army", 1}
#define VC_RIFLEMAN {"SmokeShell", 1}, {"uns_rgd33gren", 2}
#define VC_LEADER {"SmokeShell", 2}, {"uns_m18Purple", 1}, {"uns_m18red", 1}, {"uns_m18Green", 1}, {"uns_m18Yellow", 1}, {"uns_rgd33gren", 1}, {"uns_binocular_army", 1}
#define VC_TEAMLEADER {"SmokeShell", 1}, {"uns_rgd33gren", 1}, {"uns_binocular_army", 1}


class UNS_VC_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "UNS_VC_S";
		vest      = "UNS_VC_A3";
		headgear  = "UNS_Boonie4_VC";
		goggles   = "UNS_Scarf_Red";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "uns_ak47";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"uns_ak47mag", 6}};
			};
			items[]       = {VC_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "UNS_VC_R1";
			};
		};
	};
};

class UNS_VC_SquadLeader : UNS_VC_Default {
	scope = 2;
	ROLE_SQUADLEADER
	
	class Clothing : Clothing {
		uniform   = "UNS_VC_U";
		vest      = "UNS_NVA_A3";
		headgear  = "UNS_Boonie2_VC";
		goggles   = "UNS_Scarf_ARVN";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AK";

			class Primary : Primary {
				weapon      = "uns_ak47";
				magazines[] = {{"uns_ak47mag", 7}};
			};
			class Pistol {
				weapon      = "uns_38spec";
				magazines[] = {{"uns_38specmag", 3}};
			};
			class Secondary {
				weapon      = "uns_m127a1_flare";
				magazines[] = {{"uns_1Rnd_M127_mag", 6}};
			};
			class Backpack : Backpack {
				backpack    = "UNS_NVA_RTO";
			};
			items[]       = {VC_LEADER};
		};
		class Variant2: Variant1 {
            displayName = "Ppsh";
            class Primary {
				weapon      = "uns_ppsh41";
				magazines[] = {{"uns_ppshmag", 5}};
			};
        };
	};
};

class UNS_VC_Rifleman : UNS_VC_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant2 {
            displayName = "AK";
			class Backpack : Backpack {
				backpack    = "UNS_NVA_RC";
			};
			items[]       = {{"SmokeShell", 1}, {"uns_traps_punj4_mag", 1}};
		};
		class Variant2 :Variant1 {
            displayName = "SKS + Scorpion";
			class Primary : Primary {
				weapon      = "uns_sks";
				magazines[] = {{"uns_sksmag", 15}};
			};
			class Pistol {
				weapon      = "uns_sa61_p";
				magazines[] = {{"uns_20Rnd_sa61_pa", 4}};
			};
			items[]       = {{"SmokeShell", 1}, {"uns_rgd33gren", 2}, {"uns_traps_punj4_mag", 1}};
		};
		class Variant3: Variant1 {
            displayName = "Shotgun";
			class Primary : Primary {
				weapon      = "uns_baikal";
				magazines[] = {{"uns_12gaugemag_2", 11}};
			};
			items[]       = {{"SmokeShell", 1}, {"uns_rgd33gren", 3}, {"uns_traps_punj4_mag", 1}};
		};
		class Variant4: Variant1 {
            displayName = "Type50";
			class Primary : Primary {
				weapon      = "uns_type50";
				magazines[] = {{"uns_k50mag_NT", 6}};
			};
			items[]       = {{"SmokeShell", 1}, {"uns_rgd33gren", 1}, {"uns_traps_punj4_mag", 1}};
		};
		class Variant5: Variant1 {
            displayName = "STG44";
			class Primary : Primary {
				weapon      = "uns_STG_44";
				magazines[] = {{"uns_30Rnd_kurtz_stg", 6}};
			};
			items[]       = {{"SmokeShell", 1}, {"uns_traps_punj4_mag", 1}};
		};
		class Variant6: Variant1 {
            displayName = "Kar98 + MAT49";
			class Primary : Primary {
				weapon      = "uns_kar98k";
				magazines[] = {{"uns_kar98kmag", 12}};
			};
			class Pistol {
				weapon      = "uns_mat49p";
				magazines[] = {{"uns_mat49mag_NT", 5}};
			};
			items[]       = {{"SmokeShell", 1}, {"uns_rgd33gren", 1}, {"uns_traps_punj4_mag", 1}};
		};
	};
};



class UNS_VC_Medic : UNS_VC_Default {
	scope = 2;
	ROLE_MEDIC
	
	class Clothing : Clothing {
		uniform   = "UNS_VC_U";
		goggles   = "UNS_Scarf_Red";
		vest      = "UNS_VC_B1";
	};
	class Variants : Variants {
		class Variant1 :Variant2 {
            displayName = "PPSH";
			class Primary : Primary {
				weapon      = "uns_type50";
				magazines[] = {{"uns_k50mag", 6}};
			};

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"uns_rgd33gren", 1}};
		};
		class Variant2: Variant1 {
            displayName = "mp40";
			class Primary : Primary {
				weapon      = "uns_mp40";
				magazines[] = {{"uns_mp40mag", 6}};
			};
		};
	};
};

class UNS_VC_AR : UNS_VC_Default {
	scope = 2;
	ROLE_AR
	
	class Clothing : Clothing {
		uniform   = "UNS_VC_U";
		vest      = "UNS_VC_MG";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "DP";

			class Primary : Primary {
				weapon      = "uns_DP28_base";
				magazines[] = {{"uns_47Rnd_DP28", 10}};
			};
			items[]       = {VC_SUPPORT};
		};
		
		class Variant2: Variant1 {
            displayName = "RPK";
            class Primary {
				weapon      = "uns_RPK_40";
				magazines[] = {{"uns_rpkmag", 11}};
			};
		};
	};
};

class UNS_VC_MG : UNS_VC_Default {
	scope = 2;
	ROLE_MG
	
	class Clothing : Clothing {
		uniform   = "UNS_VC_U";
		vest      = "UNS_VC_MG";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "UK vz.59";

			class Primary : Primary {
				weapon      = "uns_ukvz59";
				magazines[] = {{"uns_100Rnd_762x54_ukvz59", 4}};
			};
			items[]       = {VC_SUPPORT};
		};
		class Variant2: Variant1 {
            displayName = "RPD";
            class Primary {
				weapon      = "uns_rpd";
				magazines[] = {{"uns_rpdmag", 4}};
			};
		};
		class Variant3: Variant1 {
            displayName = "PK";
            class Primary {
				weapon      = "uns_PK";
				magazines[] = {{"uns_100Rnd_762x54_PK", 4}};
			};
		};
	};
};

class UNS_VC_Grenadier : UNS_VC_Default {
	scope = 2;
	ROLE_GRENADIER
    
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Mas36";
			class Primary : Primary {
				weapon      = "uns_mas36_gl";
				magazines[] = {{"uns_mas36mag", 12}};
			};
            class Backpack : Backpack{
                    content[] = {{"Uns_1Rnd_22mm_FRAG", 8}, {"Uns_1Rnd_22mm_smoke", 10}, {"uns_1Rnd_22mm_lume", 8}, {"Uns_1Rnd_22mm_AT", 2}};
            };
		};
		class Variant2 : Variant1 {
			displayName = "type99";
			class Primary : Primary {
				weapon      = "uns_type99_gl";
				magazines[] = {{"uns_type99mag", 12}};
			};
			class Pistol {
				weapon      = "uns_mkvFlarePistol";
				magazines[] = {};
			};
            class Backpack : Backpack{
                    content[] = {{"Uns_1Rnd_30mm_FRAG", 10}, {"uns_1Rnd_Smoke_MKV", 6}, {"uns_1Rnd_SmokeRed_MKV", 4}, {"uns_40mm_mkv_Green", 8}};
            };
		};
		class Variant3: Variant2 {
        displayName = "Hand Grenades";
	class Clothing : Clothing {
		vest      = "UNS_NVA_GR";
	};
			class Primary : Primary {
				weapon      = "uns_k50m";
				magazines[] = {{"uns_k50mag", 6}};
			};
            class Backpack : Backpack{
                    content[] = {{"uns_molotov_mag", 1}, {"uns_1Rnd_Smoke_MKV", 6}, {"uns_1Rnd_SmokeRed_MKV", 4}, {"uns_40mm_mkv_Green", 8}};
            };
			items[]       = {{"uns_rgd5gren", 9}};
		};
	};
};

class UNS_VC_LAT : UNS_VC_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "RPG-2";
			class Secondary {
				weapon      = "uns_rpg2";
				magazines[] = {{"uns_rpg2grenade", 1}};
			};
			class Backpack {
				backpack = "";
			};
			items[]       = {VC_SUPPORT};
		};
		class Variant2: Variant1 {
            displayName = "AT grenades";
            class Primary {
                weapon      = "uns_sa58v"; 
                magazines[] = {{"uns_sa58mag", 6}};
            };
			class Secondary {
				weapon      = "";
				magazines[] = {};
			};
			items[]       = {{"uns_rkg3gren", 3}};
        };
	};
};

class UNS_VC_HAT : UNS_VC_Default {
	scope = 2;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_RPG7_F";
				magazines[] = {{"RPG7_F", 1}};
			};
			class Backpack : Backpack {
				backpack    = "UNS_VC_R1_RPG";
				content[] = {{"RPG7_F", 2}};
			};
			items[]       = {VC_ITEMS};
		};
	};
};

class UNS_VC_AA : UNS_VC_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
                weapon      = "uns_sa7b"; 
                magazines[] = {{"uns_sa7bmag", 1}}; 
			};

			class Backpack : Backpack {
				backpack    = "UNS_NVA_R1";
                content[] = {{"uns_sa7bmag", 1}}; 
			};
			items[]       = {VC_ITEMS};
		};
	};
};

class UNS_VC_Engineer : UNS_VC_Default {
	scope = 2;
	ROLE_ENGINEER
	
	class Clothing : Clothing {
		uniform   = "UNS_VC_S";
		vest      = "UNS_VC_SP";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "IEDs";
			class Primary {
				weapon      = "uns_baikal";
				magazines[] = {{"uns_12gaugemag_2", 15}};
			};
			class Backpack : Backpack {
				content[]   = {{"IEDLandBig_Remote_Mag", 1}};
			};
			items[]       = {{"SmokeShell", 1}, {"uns_rgd33gren", 3}, {"UNS_TrapKit", 1}, {"ToolKit", 1}};
		};
		class Variant2: Variant1 {
            displayName = "Punji traps";
            class Primary {
                weapon      = "uns_svt";
                magazines[] = {{"uns_svtmag", 8}};
            };
			class Backpack : Backpack {
				content[]   = {{"uns_traps_punj2_mag", 2}};
			};
			items[]       = {{"SmokeShell", 1}, {"uns_rgd33gren", 2}, {"UNS_TrapKit", 1}, {"ToolKit", 1}};
        };
	};
};

class UNS_VC_Marksman : UNS_VC_Default {
	scope = 2;
	ROLE_MARKSMAN
	
	class Clothing : Clothing {
		uniform   = "UNS_VC_S";
		vest      = "UNS_VC_S2";
	};
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "SVD";
			class Primary {
				weapon      = "uns_SVD_CAMO_base";
				optics      = "uns_o_PSO1_camo";
				magazines[] = {{"uns_svdmag", 10}};
			};
			class Pistol {
				weapon      = "uns_p64";
				magazines[] = {{"uns_6Rnd_czak", 3}};
			};
			items[]       = {VC_SUPPORT};
        };
		class Variant2 : Variant1 {
			displayName = "MAS49/56";
			class Primary {
				weapon      = "uns_mas4956";
				optics      = "uns_o_APXSOM";
				magazines[] = {{"uns_mas4956mag", 10}};
			};
        };
    };
};

class UNS_VC_Spotter : UNS_VC_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing : Clothing {
		headgear  = "UNS_PAVN_HC";
		vest      = "UNS_VC_B1";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
			class Primary {
				weapon      = "uns_mas4956";
				magazines[] = {{"uns_mas4956mag", 5}};
			};
			items[] = {VC_TEAMLEADER};
		};
	};
};

class UNS_VC_Sniper : UNS_VC_Spotter {
	scope = 0;
	ROLE_SNIPER
	
	class Clothing : Clothing {
		vest      = "UNS_VC_S2";
	};
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Mosin";
			class Primary : Primary {
				weapon      = "uns_mosin";
				optics      = "uns_o_PU";
				magazines[] = {{"uns_mosinmag", 10}};
			};
			class Pistol {
				weapon      = "uns_nagant_m1895";
				magazines[] = {{"uns_nagant_m1895mag", 5}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Kar98";
			class Primary : Primary {
				weapon      = "uns_kar98k";
				optics      = "uns_o_zf41";
				magazines[] = {{"uns_kar98kmag", 10}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "type99";
			class Primary : Primary {
				weapon      = "uns_type99";
				optics      = "uns_o_Akatihi4x";
				magazines[] = {{"uns_type99mag", 10}};
			};
		};
	};
};

class UNS_VC_NVA_Default : UNS_VC_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing : Clothing {
		uniform   = "UNS_NVA_GS";
		headgear  = "UNS_NVA_HG";
		goggles   = "";
		vest      = "UNS_VC_A3";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "uns_akm";
				magazines[] = {{"uns_ak47mag", 7}};
			};
			items[]       = {VC_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
		class Variant2 : Variant1 {
			displayName = "Backpack";
			class Backpack {
				backpack    = "UNS_NVA_R1";
			};
		};
	};
};

class UNS_VC_NVA_Rifleman : UNS_VC_NVA_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 : Variant2 {
			items[]       = {VC_RIFLEMAN};
			class Backpack {};
		};
	};
};

class UNS_VC_NVA_TeamLeader : UNS_VC_NVA_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Pistol {
				weapon      = "uns_APS";
				magazines[] = {{"uns_20Rnd_APS", 5}};
			};
			class Backpack {
				backpack    = "UNS_NVA_RTO";
			};
			items[]       = {VC_LEADER};
		};
	};
};

class UNS_VC_NVA_Medic : UNS_VC_NVA_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_medic_f0";
			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
		};
	};
};

class UNS_VC_NVA_Engineer : UNS_VC_NVA_Default {
	scope = 2;
	ROLE_RECON_ENGINEER
	class Clothing : Clothing {
		vest      = "UNS_VC_SP";
	};
    
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";
			class Backpack : Backpack {
				content[]   = {{"SatchelCharge_Remote_Mag", 1}, {"ATMine_Range_Mag", 1}};
			};
		};
	};
};

class UNS_VC_NVA_HAT : UNS_VC_NVA_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "RPG7";
			class Secondary {
				weapon      = "launch_RPG7_F";
				magazines[] = {{"RPG7_F", 1}};
			};
			class Backpack : Backpack {
				backpack    = "UNS_NVA_RPG";
				content[] = {{"RPG7_F", 3}};
			};
			items[]       = {VC_SUPPORT};
		};
		class Variant2: Variant1 {
            displayName = "AT grenades";
            class Primary {
                weapon      = "uns_sa58v"; 
                magazines[] = {{"uns_sa58mag", 6}};
            };
			class Secondary {
				weapon      = "";
				magazines[] = {};
			};
			class Backpack {};
			items[]       = {{"uns_rkg3gren", 6}};
        };
	};
};

class UNS_VC_NVA_AR : UNS_VC_Default {
	scope = 2;
	ROLE_AR
	class Clothing : Clothing {
		vest      = "UNS_VC_MG";
	};
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "PK";

			class Primary : Primary {
				weapon      = "uns_PK";
				magazines[] = {{"uns_100Rnd_762x54_PK", 6}};
			};
			items[]       = {VC_SUPPORT};
		};
	};
};

class UNS_VC_Crewman : UNS_VC_Default {
	ROLE_CREWMAN
	scope = 2;
	class Clothing : Clothing {
		uniform   = "UNS_NVA_CC";
		headgear  = "UNS_NVA_CHBG";
		vest      = "";
	};
	class Variants : Variants {
		class Crewman : Variant1 {
			displayName = "Crewman";
			class Primary {
				weapon      = "uns_sa61";
				magazines[] = {{"uns_20Rnd_sa61", 3}};
			};
			items[]       = {VC_CREWMAN};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing : Clothing {
                uniform   = "UNS_NVA_CG";
                headgear  = "UNS_NVA_PL";
                vest      = "";
            };
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
			class Pistol {
				weapon      = "uns_38spec";
				magazines[] = {{"uns_38specmag", 3}};
			};
			class Backpack {
				backpack    = "UNS_BA22_Para";
			};
			items[]       = {};
		};
	};
};

class UNS_VC_Baserole {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "UNS_VC_S";
		vest      = "";
		headgear  = "UNS_Conehat_VC";
		goggles   = "";
	};

	class Variants {
		class Variant1 {
			class Primary {
				weapon      = "uns_kar98k";
				magazines[] = {{"uns_kar98kmag", 4}};
			};
			class Backpack {
                backpack = "UNS_CIV_R1";
				content[] = {};
			};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
    };
};