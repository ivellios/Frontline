// UNS - USA Army

#define US_ITEMS {"SmokeShell", 1}, {"uns_m67gren", 1}
#define US_SUPPORT {"SmokeShell", 1}
#define US_SNIPER {"SmokeShell", 1}, {"uns_binocular_army", 1}
#define US_RIFLEMAN {"SmokeShell", 1}, {"uns_m67gren", 2}
#define US_LEADER {"SmokeShell", 2}, {"uns_m18Purple", 1}, {"uns_m18red", 1}, {"uns_m18Green", 1}, {"uns_m18Yellow", 1}, {"uns_m67gren", 1}, {"uns_binocular_army", 1}, {"uns_item_zippo", 1}
#define US_TEAMLEADER {"SmokeShell", 1}, {"uns_m67gren", 1}, {"uns_binocular_army", 1}, {"uns_item_zippo", 1}


class UNS_USA_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "UNS_ARMY_BDU_1stCavSubdpv1";
		headgear  = "UNS_M1_1";
		goggles   = "UNS_Peace";
		vest      = "UNS_M1956_A9";
	};

	class Variants {
		class Variant1 {
			displayName = "m16";
			class Primary {
				weapon      = "uns_m16";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"uns_20Rnd_556x45_Stanag", 10}};
			};
			items[]       = {US_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
		class Variant2 : Variant1 {
			displayName = "Backpack";
			class Backpack {
				backpack    = "UNS_Alice_F7";
			};
		};
	};
};

class UNS_USA_SquadLeader : UNS_USA_Default {
	scope = 2;
	ROLE_SQUADLEADER
	
	class Clothing : Clothing {
		uniform   = "UNS_ARMY_BDU_1stCavSubdsgt";
		headgear  = "UNS_M1_3A";
		vest      = "UNS_M1956_A2";
		goggles   = "UNS_Towel";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M16";
			class Primary : Primary {
				weapon      = "uns_M16A1";
				magazines[] = {{"uns_30Rnd_556x45_Stanag", 7}};
			};
			class Pistol {
				weapon      = "uns_m79p";
				magazines[] = {{"uns_1Rnd_SmokeRed_40mm", 2}, {"uns_40mm_red", 4}};
			};
			class Backpack : Backpack {
				backpack    = "UNS_ARMY_RTO2";
			};
			items[]       = {US_LEADER};
		};
		class Variant2: Variant1 {
        displayName = "Shotgun";
        class Primary {
				weapon      = "uns_m870_mk1";
				magazines[] = {{"uns_m870mag", 10}};
			};
		};
		class Variant3: Variant1 {
        displayName = "M14";
        class Primary {
				weapon      = "uns_m14";
				magazines[] = {{"uns_m14mag_NT", 11}};
			};
		};
	};
};

class UNS_USA_Rifleman : UNS_USA_Default {
	scope = 2;
	
	class Clothing : Clothing {
		uniform   = "UNS_ARMY_BDU_1stCavSubdpv1";
		headgear  = "UNS_M1_1B";
		vest      = "UNS_M1956_A7";
	};
	
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M16";
			items[]       = {US_RIFLEMAN};
		};
		class Variant2: Variant1 {
        displayName = "M14";
        class Primary {
				weapon      = "uns_m14";
				magazines[] = {{"uns_m14mag_NT", 9}};
			};
		};
	};
};

class UNS_USA_Medic : UNS_USA_Default {
	scope = 2;
	ROLE_MEDIC
	
	class Clothing : Clothing {
		goggles   = "UNS_Peace";
		vest      = "UNS_M1956_A9";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M2 Carabine";
			
			class Primary {
				weapon      = "uns_m2carbine";
				magazines[] = {{"uns_m2carbinemag", 6}};
			};
			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
			items[]       = {{"uns_m67gren", 1}};
        };
        class Variant2: Variant1 {
            displayName = "Thompson";
                class Primary {
                    weapon      = "uns_thompson";
                    magazines[] = {{"uns_thompsonmag_30", 6}};
                };
            };
	};
};

class UNS_USA_AR : UNS_USA_Default {
	scope = 2;
	ROLE_AR
	
	class Clothing : Clothing {
		uniform   = "UNS_ARMY_BDU_1stCavSubdsgt";
		headgear  = "UNS_M1_3A";
		vest      = "UNS_M1956_A7";
	};
	
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";
			class Primary : Primary {
				weapon      = "uns_M16A1_HBAR";
				magazines[] = {{"uns_30Rnd_556x45_Stanag", 15}};
			};
			items[]       = {US_SUPPORT};
		};
	};
};

class UNS_USA_MG : UNS_USA_Default {
	scope = 2;
	ROLE_MG

	class Clothing : Clothing {
		headgear  = "UNS_M1_2A";
		goggles   = "UNS_Bullets";
		vest      = "UNS_M1956_A6";
	};
	
	class Variants : Variants {
		class Variant1 : Variant2 {
			class Primary : Primary {
				weapon      = "uns_m60";
				magazines[] = {{"uns_m60mag", 4}};
			};
			items[]       = {US_SUPPORT};
		};
	};
};

class UNS_USA_Grenadier : UNS_USA_Default {
	scope = 2;
	ROLE_GRENADIER
	
	class Clothing : Clothing {
		headgear  = "UNS_M1_8A";
		vest      = "UNS_M1956_A4";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M79";
			class Primary : Primary {
				weapon      = "uns_m79";
				magazines[] = {{"Uns_1Rnd_HE_M381", 12}};
			};
            class Backpack : Backpack{
                    content[] = {{"uns_1Rnd_Smoke_40mm", 6}, {"uns_1Rnd_SmokeRed_40mm", 4}, {"uns_40mm_mkv_Red", 8}, {"uns_1Rnd_HEDP_M433", 2}};
            };
			class Pistol {
				weapon      = "uns_m1911";
				magazines[] = {{"uns_m1911mag", 6}};
			};
		};
        class Variant2: Variant2 {
            displayName = "M203";
            class Primary {
                    weapon      = "uns_m16_m203";
                    magazines[] = {{"uns_20Rnd_556x45_Stanag", 6}, {"Uns_1Rnd_HE_M381", 6}};
                };
            class Backpack : Backpack{
                    content[] = {{"uns_1Rnd_Smoke_40mm", 6}, {"uns_1Rnd_SmokeRed_40mm", 4}, {"uns_40mm_red", 8}, {"uns_1Rnd_HEDP_M433", 2}};
            };
			items[]       = {US_SUPPORT};
        };
	};
};

class UNS_USA_LAT : UNS_USA_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HEAT";
			class Secondary {
				weapon      = "uns_m72";
				magazines[] = {{"uns_m72rocket", 1, 1}};
			};
			items[]       = {US_SUPPORT};
		};
	};
};

class UNS_USA_HAT : UNS_USA_Default {
	scope = 0;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";
			class Secondary {
				weapon      = "uns_m20_bazooka";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"uns_M28A2_mag", 1, 1}};
			};
			class Backpack : Backpack {
				content[] = {{"uns_M28A2_mag", 2}};
			};
			items[]       = {US_ITEMS};
		};
	};
};

class UNS_USA_AA : UNS_USA_Default {
	scope = 0;
	ROLE_AA
    
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";
			class Secondary {
				weapon      = "uns_sa7b";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"uns_sa7bmag", 1}};
			};
			class Backpack : Backpack {
				content[] = {{"uns_sa7bmag", 1}};
			};
			items[]       = {US_ITEMS};
		};
	};
};

class UNS_USA_Marksman : UNS_USA_Default {
	scope = 2;
	ROLE_MARKSMAN
    
	class Clothing : Clothing {
		headgear  = "UNS_M1_9A";
		vest      = "UNS_M1956_A8";
	};
    
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "m14";
			class Primary : Primary {
				weapon      = "uns_m14";
				optics      = "uns_o_LeatherwoodART_m14";
				magazines[] = {{"uns_m14mag_NT", 8}};
			};
			items[]       = {US_SNIPER};
		};
		class Variant2: Variant1 {
            displayName = "m16";
            class Primary {
                weapon      = "uns_m16";
				optics      = "uns_o_LeatherwoodART_m16";
                magazines[] = {{"uns_20Rnd_556x45_Stanag", 8}};
            };
        };
	};
};

class UNS_USA_Engineer : UNS_USA_Default {
    scope = 2;
    ROLE_ENGINEER
    
	class Clothing : Clothing {
		vest      = "UNS_M1956_A9";
	};
    
    class Variants : Variants {
        class Variant1 : Variant2 {
            displayName = "Demo Charges";
            class Primary {
				weapon      = "uns_m870_mk1";
				magazines[] = {{"uns_m870mag", 10}};
            };
            class Backpack : Backpack {
                content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
            };
			items[]       = {{"SmokeShell", 1}, {"uns_m67gren", 2}, {"UNS_TrapKit", 1}, {"ToolKit", 1}};
        };
        class Variant2: Variant1 {
            displayName = "Claymores";
            class Primary {
                weapon      = "uns_thompson";
                magazines[] = {{"uns_thompsonmag_30", 6}};
            };

			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 4}, {"FRL_BreachingCharge_Wpn", 4}};
            };
        };
    };
};

class UNS_USA_Spotter : UNS_USA_Default {
	scope = 0;
	ROLE_SPOTTER
    
	class Clothing : Clothing {
		uniform   = "UNS_ARMY_BDU_1stCavSubdpv2";
		headgear  = "UNS_Boonie_OD2";
		goggles   = "";
		vest      = "UNS_M1956_A9";
	};
    
    
	class Variants : Variants {
		class Variant1 : Variant1 {
			items[] = {US_TEAMLEADER};
		};
	};
};

class UNS_USA_Sniper : UNS_USA_Spotter {
	scope = 0;
	ROLE_SNIPER
    
	class Clothing : Clothing {
		vest      = "UNS_M1956_A1";
	};
    
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "M40";
			class Primary {
				weapon      = "uns_M40_base";
				optics      = "uns_o_RedfieldART";
				magazines[] = {{"uns_m40mag_T", 8}};
			};
			class Pistol {
				weapon      = "uns_m1911";
				magazines[] = {{"uns_m1911mag", 3}};
			};
			items[]       = {};
		};
		class Variant2 : Variant1 {
			displayName = "Winchester m70";
			class Primary {
				weapon      = "uns_model70_iron";
				optics      = "uns_o_RedfieldART_m70";
				magazines[] = {{"uns_model70mag", 8}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "Springfield";
			class Primary {
				weapon      = "uns_m1903";
				optics      = "uns_o_Unertl8x";
				magazines[] = {{"uns_springfieldmag", 8}};
			};
			items[]       = {};
		};
	};
};

class UNS_USA_RECON_Default : UNS_USA_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN
    
	class Clothing : Clothing {
		uniform   = "UNS_SOG_BDU_TS";
		headgear  = "UNS_Headband_OD2";
		goggles   = "";
		vest      = "UNS_M1956_S3";
	};
    
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Stoner M63";
			class Primary {
				weapon      = "uns_M63a_AR_base";
				magazines[] = {{"uns_30Rnd_556x45_Stanag", 7}};
			};
			class Pistol {
				weapon      = "uns_m1911";
				magazines[] = {{"uns_m1911mag", 3}};
			};
			items[]       = {US_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "UNS_Alice_SOG";
			};
		};
	};
};

class UNS_USA_RECON_Rifleman : UNS_USA_Recon_Default {
	scope = 2;
	class Variants : Variants {
		class Variant1 : Variant1 {
			items[]       = {US_RIFLEMAN};
		};
	};
};

class UNS_USA_RECON_TeamLeader : UNS_USA_Recon_Default {
	scope = 2;
	ROLE_SQUADLEADER
    
	class Clothing : Clothing {
		headgear  = "UNS_Boonie_TIGF4";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
        displayName = "Stoner M63";
			items[]       = {US_LEADER};
		};
		class Variant2: Variant1 {
        displayName = "Shotgun";
        class Primary {
				weapon      = "uns_m870_mk1";
				magazines[] = {{"uns_m870mag", 10}};
			};
		};
	};
};

class UNS_USA_RECON_Medic : UNS_USA_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC
    
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_medic_f0";
			class Primary {
				weapon      = "uns_mac10";
				muzzle      = "uns_s_MAC10";
				magazines[] = {{"uns_mac10mag", 8}};
			class Backpack : Backpack{
				content[] = {COMMON_MEDIC};
				};
			};
		};
	};
};

class UNS_USA_RECON_Grenadier : UNS_USA_Recon_Default {
	scope = 2;
	ROLE_GRENADIER
    
	class Clothing : Clothing {
		uniform   = "UNS_SOG_BDU_TS";
		headgear  = "UNS_Boonie_TIGF4";
		goggles   = "";
		vest      = "UNS_M1956_S1";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "M79";
			class Primary : Primary {
				weapon      = "uns_m79";
				magazines[] = {{"Uns_1Rnd_HE_M381", 8}};
			};
		};
		class Variant2: Variant1 {
            displayName = "M203";
			class Primary {
				weapon      = "uns_xm177e2_xm148";
				magazines[] = {{"uns_30Rnd_556x45_Stanag", 4}, {"Uns_1Rnd_HE_M381", 4}};
            class Backpack : Backpack{
                content[] = {{"uns_1Rnd_Smoke_40mm", 4}, {"uns_40mm_green", 4}};
				};
			};
		};
	};
};

class UNS_USA_RECON_AR : UNS_USA_Default {
	scope = 2;
	ROLE_AR
    
	class Clothing : Clothing {
		uniform   = "UNS_SEAL_BDU_ET";
		headgear  = "UNS_Bandana_OD3";
		goggles   = "UNS_Ear";
		vest      = "UNS_M1956_N2";
	};
    
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Stoner";
			class Primary {
				weapon      = "uns_M63a_LMG";
				magazines[] = {{"uns_m63abox", 5}};
			};
		};
		class Variant2 : Variant2 {
			displayName = "M60";
			
			class Primary {
				weapon      = "uns_m60grip";
				magazines[] = {{"uns_m60mag", 5}};
			};
		};
	};
};

class UNS_USA_RECON_Engineer : UNS_USA_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER
    
	class Clothing : Clothing {
		headgear  = "UNS_Boonie_TIG2";
		goggles   = "UNS_Finger";
		vest      = "UNS_M1956_S1";
	};
    
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";
			class Primary {
				weapon      = "uns_m870_mk1";
				magazines[] = {{"uns_m870mag", 10}};
			};
			class Backpack : Backpack {
				content[]   = {{"SatchelCharge_Remote_Mag", 1}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
		class Variant2 : Variant1 {
			displayName = "Claymores";
			class Primary {
				weapon      = "uns_thompson";
				magazines[] = {{"uns_thompsonmag_30", 5}};
			};
			class Backpack : Backpack {
				content[]   = {{"ClaymoreDirectionalMine_Remote_Mag", 5}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class UNS_USA_RECON_LAT : UNS_USA_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT
    
	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_lat_f0";
			class Secondary {
				weapon      = "uns_m72";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"uns_m72rocket", 1}};
			};
			class Backpack : Backpack {
				content[] = {{"uns_m72rocket", 3}};
			};
		};
	};
};

class UNS_USA_Crewman : UNS_USA_Default {
	scope = 2;
	ROLE_CREWMAN
	class Clothing : Clothing {
		uniform   = "UNS_ARMY_BDU_ROKArmyCMIDFlakipatch";
		headgear  = "UNS_TC_2";
		goggles   = "UNS_Towel";
		vest      = "UNS_M1956_T1";
	};

	class Variants : Variants {
		class Crewman : Variant1 {
			displayName = "Crewman";
			class Primary {
				weapon      = "uns_m3a1";
				magazines[] = {{"uns_m3a1mag", 3}};
			};
			items[]       = {US_SNIPER};
		};
        class Pilot: Crewman {
			displayName = "Pilot";
            class Clothing : Clothing {
                uniform   = "UNS_Pilot_BDU";
                headgear  = "UNS_HP_Helmet_REBEL";
                goggles   = "G_Aviator";
                vest      = "UNS_FLAK";
            };
			class Primary {
				weapon      = "";
				magazines[] = {};
			};
			class Pistol {
				weapon      = "uns_bhp";
				magazines[] = {{"uns_13Rnd_hp", 3}};
			};
			class Backpack {
				backpack    = "UNS_BA22_Para";
			};
			items[]       = {};
        };
	};
};

class UNS_USA_Baserole {
	scope = 2;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "UNS_ARMY_BDU_1stCavSubdpv1";
		headgear  = "UNS_M1_1";
		goggles   = "UNS_Peace";
		vest      = "UNS_M1956_A9";
	};

	class Variants {
		class Variant1 {
        displayName = "M14";
        class Primary {
				weapon      = "uns_m14";
				magazines[] = {{"uns_m14mag_NT", 4}};
			};
			class Backpack {
                backpack = "UNS_Alice_2";
				content[] = {};
			};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};
    };
};