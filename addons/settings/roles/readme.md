# How Does One Gear?

There's two sample files included with comments on what each value does. Generally speaking you should only need the export function in Arsenal, but if you want to make some manual edits use that for explanation.

## Export

Recommended to use Arsenal. When Frontline is loaded up there's a fancy red button at the bottom. That'll copy the gear to clipboard.
Role name and classnames need to be manually adjusted. The same goes for things like abilities, marker and icon.
Config inheritance isn't used by default in the export function and isn't really neccesary, but can make updating of shared gear easier

## Factions

Right now there's no ez splitting of factions within the mission, so I recommend putting different factions in different files for now.
Classnames need to be unique, so I recommend a Mod and faction identifier. Some examples
 ```
(B_CUP_US_Rifleman // -- Blufor US Rifleman for CUP
(B_CUP_USWd_Rifleman // -- Blufor US woodland Rifleman for CUP
(B_IFA_Wehr_SubmachineGunner // -- Blufor wehrmacht MP40 dude for Iron Front
(B_IFA_Sturm_SubmachineGunner // -- Blufor sturmtrooper MP40 dude for Iron Front
```
You can use imaginary names for this, just whatever makes some sense. 

## Inheritance

In the end the goal is to have relatively clean configs that use a lot of inheritance. 
The advantage of this is that in the example of a woodland rifleman versus a rifleman in desert uniform, is that we only need to change the uniform on one of them
Whenever we update the gear of the other one, it'll be automatically updated for the other role as well.
It means you can do a lot more effecient updating for different factions, without overriding specifics such as Uniform or type of scope