#define ROLE_CREWMAN displayName = "str_b_crew_f0";\
	abilities[] = {"Crewman"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Crewman.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_RIFLEMAN displayName = "str_a3_cfgvehicles_b_soldier_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\rifleman_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_SQUADLEADER displayName = "str_b_soldier_sl_f0";\
	abilities[] = {"RP", "Leader"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_TEAMLEADER displayName = "str_b_soldier_tl_f0";\
	abilities[] = {"Leader"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_SPOTTER displayName = "str_b_spotter_f0";\
	abilities[] = {"Leader"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_MEDIC displayName = "Combat Lifesaver";\
	abilities[] = {"Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\medic_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMedic_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};\
	highlightVacant = 1;

#define ROLE_AR displayName = "str_b_soldier_ar_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\autorifleman_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMG_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_MG displayName = "Machine Gunner";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\autorifleman_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMG_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_GRENADIER displayName = "str_b_soldier_gl_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\grenadier_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManExplosive_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
	
#define ROLE_MORTARMAN displayName = "Mortar Operator";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\grenadier_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManExplosive_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};	

#define ROLE_LAT displayName = "Light Anti-Tank (LAT)";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\LAT.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};\
	highlightVacant = 1;

#define ROLE_HAT displayName = "Heavy Anti-Tank (HAT)";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\HAT.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_AA displayName = "str_b_soldier_aa_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\AA.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_MARKSMAN displayName = "str_b_soldier_m_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Marksman.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManRecon_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_ENGINEER displayName = "str_b_engineer_f0";\
	abilities[] = {"Engineer"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
    
#define ROLE_ENGINEER_AT displayName = "Engineer (Anti-Tank)";\
	abilities[] = {"Engineer"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_SNIPER displayName = "str_b_sniper_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sniper_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManRecon_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_RECON_RIFLEMAN	displayName = "str_b_recon_f0";\
	abilities[] = {"Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\rifleman_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_RECON_TEAMLEADER displayName = "str_b_recon_tl_f0";\
	abilities[] = {"RP", "Leader","Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManLeader_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_RECON_MEDIC displayName = "str_b_recon_medic_f0";\
	abilities[] = {"Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\medic_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMedic_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_RECON_ENGINEER displayName = "str_b_recon_exp_f0";\
	abilities[] = {"Engineer","Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_RECON_LAT displayName = "str_b_recon_lat_f0";\
	abilities[] = {"Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\LAT.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
    
#define ROLE_FF_RIFLEMAN	displayName = "Rifleman";\
	abilities[] = {"Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\rifleman_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_FF_TEAMLEADER displayName = "Team Leader";\
	abilities[] = {"RP", "Leader","Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManLeader_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_FF_ENGINEER displayName = "Demo Specialist";\
	abilities[] = {"Engineer","Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_FF_LAT displayName = "Ani-Tank";\
	abilities[] = {"Medic"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\LAT.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_AMMO_BEARER displayName = "Ammo Bearer";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Rifleman_alt.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_STATICLEADER displayName = "str_b_soldier_tl_f0";\
	abilities[] = {"Static", "Leader"};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
	
#define UAV_OPERATOR displayName = "UAV Operator";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

// ----------------------------
// IRON FRONT

#define ROLE_IFA_RIFLEMAN displayName = "str_a3_cfgvehicles_b_soldier_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Rifleman_alt.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_RIFLEMAN_1STCLASS displayName = "Rifleman Semi-auto";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Rifleman_semi.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_OFFICER displayName = "Sergeant";\
  abilities[] = {"RP", "Leader"};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\SL_alt.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_ASSISTANT_GUNNER displayName = "MG Assistant (Ammo)";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\Assistant_gunner_1.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_LMG_AMMO_BEARER displayName = "Ammo Bearer";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\Rifleman_alt.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_RAT displayName = "Rocket Specialist (AT)";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\Rocket_specialist.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_RAT_AMMO displayName = "Rocket Ammo Bearer";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\Rocket_specialist.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_CORPORAL displayName = "Corporal";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\SMG.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_SMG displayName = "Submachinegunner";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\SMG.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_RIFLE_LAT displayName = "str_b_soldier_lat_f0";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\AT_Grenadier_2.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_LMG displayName = "Light Machine Gunner";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\autorifleman_small_88.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMG_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_HMG displayName = "Machine Gunner";\
  abilities[] = {};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\autorifleman_small_88.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMG_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};

#define ROLE_IFA_COMBAT_MEDIC displayName = "Combat Medic";\
  abilities[] = {"Medic"};\
  UIIcon = "\pr\frl\addons\client\ui\media\roles\medic_small_88.paa";\
  mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMedic_ca.paa";\
  compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};\
  highlightVacant = 1;

#define ROLE_IFA_SNIPER displayName = "str_b_sniper_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sniper_alt.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManRecon_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
    
#define ROLE_IFA_SPOTTER displayName = "Spotter";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManLeader_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
    
#define ROLE_IFA_GRENADIER displayName = "str_b_soldier_gl_f0";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\grenadier_small_88.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManExplosive_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
	
#define ROLE_FLAMETHROWER displayName = "Flamethrower";\
	abilities[] = {};\
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Flamethrower.paa";\
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";\
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
