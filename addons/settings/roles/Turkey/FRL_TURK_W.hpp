// TURK - Woodland

class B_TURK_W_Default {
	scope = 0;
	ROLE_RIFLEMAN

	class Clothing {
		uniform   = "TSF_Uniform2";
		headgear  = "TSF_kask";
		goggles   = "";
		vest      = "TSF_Vest_2";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "TSF_HK33";
				rail        = "";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"tsf_30rnd_762x51_b_hk33", 7}};
			};

			class Pistol {
				weapon      = "TSF_Zigana_T";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"TSF_15Rnd_9x19_Zigana", 3}};
			};

			items[]       = {COMMON_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "TSF_Canta_2";
			};
		};
	};
};

class B_TURK_W_Rifleman : B_TURK_W_Default {
	scope = 2;

	class Variants : Variants {
		class Variant1 :Variant1 {

			class Primary : Primary {
				optics      = "";
			};

			items[]       = {COMMON_RIFLEMAN};
		};
	};
};

class B_TURK_W_SquadLeader : B_TURK_W_Default {
	scope = 2;
	ROLE_SQUADLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				weapon      = "Tsf_g3s";
				magazines[] = {{"tsf_20rnd_762x51_b_G3", 7}};
			};

			items[]       = {COMMON_LEADER};
		};
	};
};

class B_TURK_W_TeamLeader_MG : B_TURK_W_Default {
	scope = 2;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AR Support";

			class Backpack : Backpack {
				content[] = {{"TSF_200Rnd_762x51_MG3", 5}};
			};
		};
	};
};

class B_A3_TURK_TeamLeader_AA : B_A3_TURK_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "AA Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class B_A3_TURK_TeamLeader_HAT : B_A3_TURK_Default {
	scope = 0;
	ROLE_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "HAT Support";

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class B_A3_TURK_Spotter : B_A3_TURK_Default {
	scope = 0;
	ROLE_SPOTTER

	class Clothing {
		uniform   = "U_B_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_rgr";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class B_TURK_W_Medic : B_TURK_W_Default {
	scope = 2;
	ROLE_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack {
				content[] = {COMMON_MEDIC};
			};
		};
	};
};

class B_TURK_W_MG : B_TURK_W_Default {
	scope = 2;
	ROLE_MG

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "TSF_mg3";
				magazines[] = {{"TSF_200Rnd_762x51_MG3", 3}};
			};
		};
	};
};

class B_TURK_W_Grenadier : B_TURK_W_Default {
	scope = 2;
	ROLE_GRENADIER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "TSF_HK33_T40";
				magazines[] = {{"tsf_30rnd_762x51_b_hk33", 7}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class B_A3_TURK_LAT : B_A3_TURK_Default {
	scope = 2;
	ROLE_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class B_A3_TURK_HAT : B_A3_TURK_Default {
	scope = 0;
	ROLE_HAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_B_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}};
			};
		};
	};
};

class B_A3_TURK_AA : B_A3_TURK_Default {
	scope = 0;
	ROLE_AA

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_B_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class B_A3_TURK_Marksman : B_A3_TURK_Default {
	scope = 2;
	ROLE_MARKSMAN

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "arifle_MXM_F";
				optics      = "optic_DMS";
				bipod       = "bipod_01_F_snd";
			};
		};
	};
};

class B_A3_TURK_Engineer : B_A3_TURK_Default {
	scope = 2;
	ROLE_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class B_A3_TURK_Sniper : B_A3_TURK_Spotter {
	scope = 0;
	ROLE_SNIPER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_LRR_camo_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"7Rnd_408_Mag", 10}};
			};
		};
	};
};

class B_A3_TURK_RECON_Default {
	scope = 0;
	ROLE_RECON_RIFLEMAN

	class Clothing {
		uniform   = "U_B_CombatUniform_mcam_vest";
		headgear  = "H_Booniehat_mcamo";
		goggles   = "G_Bandanna_khk";
		vest      = "V_PlateCarrier1_rgr";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_MX_Black_F";
				muzzle      = "muzzle_snds_H";
				rail        = "";
				optics      = "optic_MRCO";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {COMMON_ITEMS};
			itemshidden[] = {COMMON_ITEMSHIDDEN, COMMON_FIRSTAID};
		};

		class Variant2 : Variant1 {
			displayName = "Backpack";

			class Backpack {
				backpack    = "B_AssaultPack_rgr";
			};
		};
	};
};

class B_A3_TURK_RECON_Rifleman : B_A3_TURK_Recon_Default {
	scope = 2;

	class Variants {
		class Variant1 {
			items[]       = {COMMON_RIFLEMAN};
		};
	};
};

class B_A3_TURK_RECON_TeamLeader : B_A3_TURK_Recon_Default {
	scope = 2;
	ROLE_RECON_TEAMLEADER

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {COMMON_LEADER};
		};
	};
};

class B_A3_TURK_RECON_Medic : B_A3_TURK_Recon_Default {
	scope = 2;
	ROLE_RECON_MEDIC

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack{
				content[] = {COMMON_MEDIC};
			};
		};
	};
};

class B_A3_TURK_RECON_Engineer : B_A3_TURK_Recon_Default {
	scope = 2;
	ROLE_RECON_ENGINEER

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 4}};
			};
		};
	};
};

class B_A3_TURK_RECON_LAT : B_A3_TURK_Recon_Default {
	scope = 2;
	ROLE_RECON_LAT

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};
