class cfgDynamicFrontline {
    percentageToEnd = 0.75;
    timeRequired = 90; // 5 minutes
    preventDestroyRangeFO = 150; // Don't allow FO destroy if further than x meters from frontline (-1 to disable)
    frontlineUpdateSpeed = 10; // 10 seconds
    frontlineFreezetime = 300; // -- time the frontline remains inactive after preptime
    positionsUpdateSpeed = 2.5;

    class Settings { // -- These are passed to python as dictionary
        square_size = 75;
        max_capture = 20;
    };
};
