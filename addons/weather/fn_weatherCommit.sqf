/*
 *	File: fn_weatherCommit.sqf
 *	Author: Adanteh
 *	Commits current weather settings.
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call EFUNC(main,weatherCommit);
 */

#include "macros.hpp"

params [["_broadcast", false], ["_weatherSettings", []]];

if (isServer && _broadcast) then {

    // For each weather setting,
    {
        VAR(_type) = _x;
        VAR(_value) =  (_weatherSettings select 1) select _forEachIndex;

        // Execute the function associated with setting the current weather type (eg: QFUNC(setRain) _type = "Rain")
        VAR(_function) = missionNamespace getVariable [(QFUNC(set) + _type), ""];
        if (typeName _function == "CODE") then {
            [_value, false, false] call _function;
        };

        // Create list of changed settings to notify user of
    } forEach (_weatherSettings select 0);

    // Notify all clients via a message of change setting names.
    private _types = ((_weatherSettings select 0) joinString ", ");
    (format ["<ADMIN> Weather type: %1 at %2", _types, serverTime]) call MFUNC(diagLog);
    if (time > 20) then { 	// -- Don't give notifications like this at mission start -- //
        _types remoteExec [QFUNC(weatherNotify), 0, false];
    };
} else {
    closeDialog 0;
    _weatherSettings = missionNamespace getVariable [QGVAR(preview), [[], []]];
    if !(_weatherSettings isEqualTo [[], []]) then {
        [true, _weatherSettings] remoteExec [_fnc_scriptName, 2, false];
        missionNamespace setVariable [QGVAR(preview), nil];
    };
};
