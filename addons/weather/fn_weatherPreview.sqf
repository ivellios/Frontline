/*
 *	File: fn_weatherPreview.sqf
 *	Author: Adanteh
 *	Previews weather changes (Largely broken now because server auto-syncs weather, so it'll override this stuff)
 *	It still allows us to queue multiple effects and commit in one go
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call EFUNC(main,weatherPreview);
 */

#include "macros.hpp"

params [
	["_type", "", ['', []]],
	["_value", 0, [[], 0]],
	["_offset", false]];

// -- Runs function named after the input type -- //
VAR(_function) = missionNamespace getVariable [(QFUNC(set) + _type), ""];
if (typeName _function == "CODE") then {
	// -- Save the set weather settings to an array. Overwrite previously set settings -- //
	VAR(_previewArray) =+ (missionNamespace getVariable [QGVAR(preview), [[], []]]);
	VAR(_typeIndex) = ((_previewArray select 0) find _type);
	if (_typeIndex == -1) then {
		_typeIndex = (_previewArray select 0) pushBack _type;
	};

	VAR(_value) = [_value, _offset, true] call _function;
	(_previewArray select 1) set [_typeIndex, _value];
	GVAR(preview) = _previewArray;
};

