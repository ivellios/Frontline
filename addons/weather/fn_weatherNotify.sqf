/*
 *	File: fn_weatherNotify.sqf
 *	Author: Adanteh
 *	Shows updated weather notfications on clients
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call EFUNC(main,weatherNotify);
 */

#include "macros.hpp"

// -- Only give notificaton on clients -- //
if (hasInterface) then {
	params ["_environmentType"];

	VAR(_msg) = format ["%1 was adjusted.", _environmentType];
	[QGVAR(WeatherChanged) , ["", _msg]] call BIS_fnc_showNotification;
};
