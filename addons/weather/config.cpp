#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\weather";

            class init;
            class serverInit;
            class setDate;
            class setFog;
            class setOvercast;
            class setRain;
            class setWind;
            class stopRain;
            class weatherCommit;
            class weatherNotify;
            class weatherPreview;
            class canChange;
        };
    };
};

class cfgNotifications {
    class TaskSucceededIcon;
    class GVAR(WeatherChanged): TaskSucceededIcon {
        iconPicture = "\a3\ui_f\data\gui\Rsc\RscDisplayArcadeMap\cloudly_ca.paa";
        title = "Weather Change";
    };
};
