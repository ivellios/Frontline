/*
 *	File: fn_stopRain.sqf
 *	Author: Adanteh
 *	Function to force stop rain, which is automagically reenabled on higher overcast settings by BIS weather gods
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call EFUNC(main,stopRain);
 */

#include "macros.hpp"

if !(missionNamespace getVariable [QGVAR(allowRain), false]) then {
	if (rain != 0) then {
		0 setRain 0;
	};
};

