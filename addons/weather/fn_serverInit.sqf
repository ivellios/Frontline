/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Loads parameters and applies server weather settings globally
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call EFUNC(main,serverInit);
 */

#include "macros.hpp"

[QGVAR(Settings), missionConfigFile >> "FRL" >> "CfgWeather"] call MFUNC(cfgSettingLoad);

// -- Add extra delay. Otherwise syncing weather sometimes break -- //
[{
	// -- Load mission weather params -- //
	VAR(_randomizeOvercast) = ([QGVAR(Settings_overcastRandomize), 0] call MFUNC(cfgSetting)) > 0;
	VAR(_randomizeRain) = ([QGVAR(Settings_rainRandomize), 0] call MFUNC(cfgSetting)) > 0;
	VAR(_randomizeFog) = ([QGVAR(Settings_fogRandomize), 0] call MFUNC(cfgSetting)) > 0;
	VAR(_timeMultiplier) = (0.1 max ([QGVAR(Settings_timeMultiplier), 1] call MFUNC(cfgSetting))) min 120;

	if (_timeMultiplier != 1) then {
		setTimeMultiplier _timeMultiplier;
	};

	// -- Randomize time of day within the accepted time frame -- //
	private ["_hour", "_minute"];
	VAR(_randomizeTime) = ([QGVAR(Settings_timeRandomize), 0] call MFUNC(cfgSetting)) > 0;
	if (_randomizeTime) then {
		// -- Pick a mission in the allowed time frame set by the mission maker -- //
		VAR(_randomizedTimeframe) = [QGVAR(Settings_timeFrame), [4, 22]] call MFUNC(cfgSetting);
		VAR(_randomizedTime) = (random ((_randomizedTimeframe select 1) - (_randomizedTimeframe select 0))) +  (_randomizedTimeframe select 0);
		_minute = 60 * (_randomizedTime - (floor _randomizedTime));
		_hour = floor _randomizedTime;
	} else {
		_hour = date select 3;
		_minute = date select 4;
	};

	private ["_rain"];
	if (_randomizeRain) then {
		private _rainFrame = [QGVAR(Settings_rainFrame), [0, 0.5]] call MFUNC(cfgSetting);
		_rain = (random ((_rainFrame select 1) - (_rainFrame select 0))) +  (_rainFrame select 0);
		[_rain, false, false] call FUNC(setRain);
	} else {
		_rain = rain;
	};

	GVAR(allowRain) = _rain != 0;
	publicVariable QGVAR(allowRain);

	private _fog = 0;
	if (_randomizeFog) then {
		private _fogFrameFrame = [QGVAR(Settings_fogFrame), [0, 0.2]] call MFUNC(cfgSetting);
		_fog = (random ((_fogFrameFrame select 1) - (_fogFrameFrame select 0))) +  (_fogFrameFrame select 0);
		[_fog, false, false] call FUNC(setFog);
	};

	// -- check if a fog is being set and the weather conditions may results in silly sunshine + fog
	private _compensateForFog = (_fog > 0.1 && {_hour > 8 && _hour < 18});

	// -- Set all variables to server and client -- //
	if (_randomizeOvercast || _compensateForFog) then {
		private _overcastFrame = [QGVAR(Settings_overcastFrame), [0, 0.5]] call MFUNC(cfgSetting);
		if (_compensateForFog) then {
			_overcastFrame = [0.6,1];
		};
		private _overcast = (random ((_overcastFrame select 1) - (_overcastFrame select 0))) +  (_overcastFrame select 0);
		[_overcast, false, false] call FUNC(setOvercast);
	};
	// -- Always set date -- //
	[[_hour, _minute], false, false] call FUNC(setDate);


}, 5, []] call CFUNC(wait);


true;
