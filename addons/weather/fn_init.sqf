/*
 *	File: fn_init.sqf
 *	Author: Adanteh
 *	Describe your function
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[] call EFUNC(main,init);
 */

#include "macros.hpp"

// -- Stops the rain from autostarting on high overcast settings -- //
[{ call FUNC(stopRain) }, 5, []] call MFUNC(addPerFramehandler);

// -- Add all the debug settings -- //
if (hasInterface) then {

	['add', ["admin", 'Admin Settings', 'Weather', [
		'Time',
		'Sets the time of day',
		QMVAR(CtrlSlider),
		{ (date select 3) },
		{ [Clib_Player] call FUNC(canChange); },
		(format ["['date', [(_this select 1)]] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 24], [0.05, 1]]
	]]] call MFUNC(settingsWindow);

	// -- Volume levels -- //
	['add', ["admin", 'Admin Settings', 'Weather', [
		'Overcast',
		'Set the amount of overcast',
		QMVAR(CtrlSlider),
		{ overcast },
		{ [Clib_Player] call FUNC(canChange); },
		(format ["['overcast', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call MFUNC(settingsWindow);

	['add', ["admin", 'Admin Settings', 'Weather', [
		'Rain',
		'Set the amount of rain',
		QMVAR(CtrlSlider),
		{ rain },
		{ [Clib_Player] call FUNC(canChange); },
		(format ["['rain', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call MFUNC(settingsWindow);

	['add', ["admin", 'Admin Settings', 'Weather', [
		'Fog',
		'Set the amount of fog',
		QMVAR(CtrlSlider),
		{ fog },
		{ [Clib_Player] call FUNC(canChange); },
		(format ["['fog', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call MFUNC(settingsWindow);

	['add', ["admin", 'Admin Settings', 'Weather', [
		'Wind',
		'Set the amount of wind',
		QMVAR(CtrlSlider),
		{ windStr },
		{ [Clib_Player] call FUNC(canChange); },
		(format ["['wind', (_this select 1)] call %1; false;", QFUNC(weatherPreview)]),
		[[0, 1], [0.05, 1]]
	]]] call MFUNC(settingsWindow);

	['add', ["admin", 'Admin Settings', 'Weather', [
		'Broadcast',
		'Broadcasts all the changed weather settings',
		QMVAR(CtrlButton),
		true,
		{ [Clib_Player] call FUNC(canChange); },
		(format ["[false, []] call %1; false;", QFUNC(weatherCommit)]),
		["BROADCAST"]
	]]] call MFUNC(settingsWindow);
};
