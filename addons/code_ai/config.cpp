#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 0;
            dependencies[] = {"Main", "Rolesspawns"};
            path = "pr\frl\addons\code_ai\functions";

            class aiKilled;
            class clientInit;
            class createGroup;
            class getRole;
            class getSpawnPosition;
            class getWaypointPosition;
            class handleSquads;
            class resetAI;
            class serverInit;
            class setUnitName;
            class setWaypoint;
            class unstuckRetardedPathfinding;
            class updateWaypoint;
        };
    };

    class cfgAI {
        enabled = 1;
        strengthMinimum = 3; // Ignore squads below this strength
        strengthMax = 9;
        overLimitSquads = 2; // Don't create more squads than required (from preferUnitCount / (units per squad)), whether they are above or below strength
        preferedUnitCount = 32; // How much units we want (AI - pla) 3x6 man squads
        waypointRefreshRate = 30; // How often to set a new waypoint
        aiSkillBase = 0.45; // Unit ability
        aiSkillRandom = 0.2; // Randomized onto base ability (So random between 0.25 and 0.65)
        squadCreateCooldown = 135; // Time between squad creation
        distanceFilter = 1500; // Only allow spawning AI if spawnpoint is within this distance
        mergeDistance = 250;
    };
};
