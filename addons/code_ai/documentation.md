## AI

Does AI stuff

## Squad creation
Creates squads based on requested player number. It'll create squads based on 6 active units

## Pathfinding
Waypoints are based on the average position of squad (All positions added, divided by amount of units)
Then a small static waypoint bias is added (Generated randomly each for a squad) so not all squads go to the same position
Then whatever point within the frontline is closest, is where they will be send.

If it's a big Frontline (+10 points), then the corners are removed, so they don't go to close to the edge of map
If they're very close to their waypoint, they will switch combat mode so they go prone/ in cover more
