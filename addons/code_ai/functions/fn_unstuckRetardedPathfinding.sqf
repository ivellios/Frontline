/*
    Function:       FRL_AI_fnc_unstuckRetardedPathfinding
    Author:         Adanteh
    Description:    Handles checking if AI is stuck, and if so move them around
*/
#include "macros.hpp"

params ["_group"];

if (isNull _group) exitWith { false };

private _units = units _group;
if (_units isEqualTo []) exitWith { false };

// -- Get average position
private _averagePosition = [_group] call MFUNC(getGroupPosition);
private _lastPosition = _group getVariable [QGVAR(lastPosition), [0, 0, 0]];
private _stuck = false;

// -- Don't unstuck if group is near where they are supposed to be
private _currentWPIndex = currentWaypoint _group;
if (count waypoints _group <= _currentWPIndex) exitwith {
    _group setVariable [QGVAR(stuckTicks), 0, false];
    _group setVariable [QGVAR(lastPosition), _averagePosition, false];
    _stuck;
};
// -- do not make them count as stuck when they go prone and fight
private _nearestEnemy = units _group select (units _group findIf {alive _x}) findNearestEnemy _averagePosition;
if (!isNull _nearestEnemy) exitwith {
    _group setVariable [QGVAR(stuckTicks), 0, false];
    _group setVariable [QGVAR(lastPosition), _averagePosition, false];
    _stuck;
};

private _ticks = _group getVariable [QGVAR(stuckTicks), 0];

// -- Check if position is close to the last one (Exact position might differ a little due to units turning or whatever)
if (_averagePosition distance2D _lastPosition < 5) then {
    _stuck = true;
    _ticks = _ticks + 1;

    // -- If units have been stuck a longer time, just kill all of them so a new squad can spawn
    if (_ticks > 10) then {
        {
            _x setDamage 1;
        } forEach _units;
    } else {
        // -- Else, find a safe'ish position (All the commands for that are barely useable) and telepor them there.
        if (_ticks > 5) then {
            {
                private _pos = AGLtoASL ([getPos _x, 10, 0, typeOf _x] call CFUNC(findSavePosition));
                _x setPosASL _pos;
            } forEach _units;
        };
    };

} else {
    // -- avoid deleting AI mid combat by removing ticks after movement
    // -- AI barely moves at all when in combat
    _ticks = 0;
};

_group setVariable [QGVAR(stuckTicks), _ticks, false];
_group setVariable [QGVAR(lastPosition), _averagePosition, false];
_stuck;
