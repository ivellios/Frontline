/*
    Function:       FRL_AI_fnc_resetAI
    Author:         Adanteh
    Description:    Deletes all current AI squads, cleans them up and spawns in new ones
*/
#include "macros.hpp"

private _allSquads = GVAR(namespace) getVariable ["groups", []];
{
    private _group = _x;
    private _units = units _group;
    { deleteVehicle _x } forEach _units;

    private _marker = _group getVariable [QGVAR(waypointMarker), ""];
    deleteMarker _marker;
    deleteGroup _group;

} forEach _allSquads;

GVAR(namespace) setVariable ["groups", []];
