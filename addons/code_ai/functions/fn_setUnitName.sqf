/*
    Function:       FRL_AI_fnc_setUnitName
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_unit", ["_nameClass", "BritishMen"]];

private _cfg = configFile >> "CfgWorlds" >> "GenericNames" >> _nameClass;
private _firstName = selectRandom ((configProperties [(_cfg >> "FirstNames"), "true", true]) apply { getText _x });
private _lastName = selectRandom ((configProperties [(_cfg >> "LastNames"), "true", true]) apply { getText _x });

_unit setVariable ["clib_core_playerName", [_firstName, _lastName] joinString " ", true];
