/*
    Function:       FRL_AI_fnc_getSpawnPosition
    Author:         Adanteh
    Description:    Figures out where to spawn a group
*/
#include "macros.hpp"

params ["_side"];

private _aiSpawnPos = [];
["AI_spawnPosition", [_side]] call MFUNC(checkCondition);

// -- Pick a random spawnpoint (Only direct spawns, so base, zone, forward, etc)
private _distanceFilter = [QGVAR(Settings_distanceFilter), 500] call MFUNC(cfgSetting);
private _spawnpoints = [];
[EGVAR(rolesspawns,spawnpoints), {
    if !(_value getVariable ["deleted", false]) then {

        private _availableFor = _value getVariable ["availableFor", sideUnknown];
        if (_availableFor isEqualTo _side) then {

            private _type = _value getVariable ["type", ""];
            private _position = _value getVariable ["position", [0, 0, 0]];
            if (_type in ["fo", "zone", "forward", "base"]) then {

                private _aiCanSpawn = (["ai_canSpawn", [_value, _position, _distanceFilter, _type], true]) call MFUNC(checkConditions);
                if (_aiCanSpawn) then {
                    // -- Check if it isn't blocked or anything like that
                    private _canSpawn = ([_type + "_canSpawn", [_value, _position, _side], true] call MFUNC(checkConditions));

                    // -- Don't allow spawning if there's ANY enemies inside (no matter how many friendlies or wahtever)
                    if (count (_value getVariable ["enemyInside", []]) > 0) exitWith { };
                    if (_canSpawn) then {
                        if (_value getVariable ["aiSpawnAllow", true]) then {
                            _spawnpoints pushBack _value;
                        };
                    };
                };
            };
        };
    };
}] call MFUNC(forEachVariable);

// -- Figure out where to spawn (Adjust position if neccesary)
if !(_spawnpoints isEqualTo []) then {
    // -- If non-base spawns are available, ignore all base spawns
    private _spawnpointsExcludeBase = _spawnpoints select { (_x getVariable ["type", "base"]) != "base" };
    if !(_spawnpointsExcludeBase isEqualTo []) then {
        _spawnpoints = _spawnpointsExcludeBase;
    };
    private _spawnpoint = (selectRandom _spawnpoints);
    private _posAdjust = [_spawnpoint getVariable "type", "spawnPos"] call EFUNC(rolesspawns,spawnTypeData);
    private _positionAdjust = call _posAdjust; // -- Add registered mouseover code
    if !(isNil "_positionAdjust") then {
        _aiSpawnPos = _positionAdjust;
    } else {
        _aiSpawnPos = _spawnpoint getVariable ["AISpawnpos", _spawnpoint getVariable ["position", []]];
    };
};

_aiSpawnPos;
