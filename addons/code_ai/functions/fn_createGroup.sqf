/*
    Function:       FRL_AI_fnc_createGroup
    Author:         Adanteh
    Description:    A Function That Does stuff
    Example:        [playerSide, getpos player] call FRL_AI_fnc_createGroup;
*/
#include "macros.hpp"

#ifdef DEBUGFULL
[[_fnc_scriptNameShort, _this], "red"] call MFUNC(debugMessage);
#endif

params ["_side", ["_spawnPosition", []], ["_type", ""]];

// -- Create the first unit as squad leader, then add the rest of the units
if (_spawnPosition isEqualTo []) then {
    _spawnPosition = [_side, grpNull] call FUNC(getSpawnPosition);
};

// -- No spawn location available
if (_spawnPosition isEqualTo []) exitWith { };

private _className = [_side, "playerclass"] call MFUNC(getSideData);
if !(isClass (configFile >> "cfgVehicles" >> _className)) then {
    private _sideID = [_side] call BIS_fnc_sideID;
    _className = ["O_Soldier_F", "B_Soldier_F", "I_soldier_F"] param [_sideID, "B_Soldier_F"];
};

// -- Create a new group, create a squad leader for it and register it
if (_type == "") then {
    _type = [_side, "aisquad"] call MFUNC(getSideData);
    if (_type == "") then {
        ["debugMessage", [[_fnc_scriptNameShort, 'No squad with aiSquadType = 1; setting for side', _side], "red", 20, -1]] call CFUNC(globalEvent);
    };
};

private _group = createGroup [_side, false];
private _leader = _group createUnit [_className, _spawnPosition, [], 0, "CAN_COLLIDE"];
if (isDedicated) then {
    ["call", ["AI Squad", _type, _leader, _side, false]] call EFUNC(rolesspawns,squadCreate);
} else {
    ["mutex", ["AI Squad", _type, _leader, _side, false]] call EFUNC(rolesspawns,squadCreate);
};


_leader setRank "SERGEANT";
_group setVariable [QMVAR(gType), _type, true]; // -- This is set by squadCreate, but might be delayed due to mutex

// -- Create units and add them to the group
private _units = [_leader];
private _size = EGVAR(rolesspawns,squadNamespace) getVariable [format ["%1_%2", _type, "maxSize"], 6];

for "_i" from 2 to _size do {
    private _pos = _spawnPosition getPos [1, _i * 40];
    private _unit = _group createUnit [_className, _pos, [], 0, "CAN_COLLIDE"];
    _unit setRank "PRIVATE";
    _units pushBack _unit;
};
_units joinSilent _group;

// -- Set ability to everyone, add some randomization and give them a role
private _skillBase = [QGVAR(settings_aiSkillBase), 0.5] call MFUNC(cfgSetting);
private _skillRandom = [QGVAR(settings_aiSkillRandom), 0.2] call MFUNC(cfgSetting);
private _nameClass = [_side, "unitNames"] call MFUNC(getSideData);

{
    _x setDir (random 360);
    _x setUnitAbility (RANDOMOFFSET(_skillBase,_skillRandom) min 1);
    _x addEventHandler ["Killed", { _this call FUNC(aiKilled) }];
    [_x, _nameClass] call FUNC(setUnitName);


    private _role = [_x, _type] call FUNC(getRole);
    _x setVariable [QSVAR(rkit), _role, true];
    [_x, _role] call EFUNC(rolesspawns,roleEquip);
    [_x, _role] call EFUNC(rolesspawns,roleEquipData);
} forEach _units;


// -- Keep track of groups
private _allSquads = GVAR(namespace) getVariable ["groups", []];
_allSquads pushBack _group;
GVAR(namespace) setVariable ["groups", _allSquads];

#ifdef DEBUGFULL
[[_fnc_scriptNameShort, _group, _leader, _side, diag_tickTime], "blue"] call MFUNC(debugMessage);
#endif

_group deleteGroupWhenEmpty true;
_group
