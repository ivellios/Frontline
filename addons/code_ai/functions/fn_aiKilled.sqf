/*
    Function:       FRL_AI_fnc_aiKilled
    Author:         Adanteh
    Description:    EH for killed AI (SERVER ONLY)
*/
#include "macros.hpp"

params ["_unit", "_killer"];
if !(isPlayer _unit) then {

    private _killers = [];
    call {
        if (_killer isKindOf "CaManBase") exitWith { _killers = [_killer] };
        // -- If not soldier check crew of source -- //
        {
        	_x params ["_unit", "_role"];
        	if (toLower _role in ["commander", "gunner", "turret", "driver"]) then {
        		_killers pushBack _unit;
        	};
        	nil
        } count (fullCrew _killer);
    };

    ["KilledAI", [_unit, _killers]] call CFUNC(localEvent);
};
