/*
    Function:       FRL_AI_fnc_serverInit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

GVAR(namespace) = false call MFUNC(createNamespace);
[QGVAR(settings), configFile >> "FRL" >> "CfgAI"] call MFUNC(cfgSettingLoad);
[QGVAR(settings), missionConfigFile >> "FRL" >> "CfgAI"] call MFUNC(cfgSettingLoad);

[{
    [{ _this call FUNC(handleSquads) }, 5] call MFUNC(addPerFrameHandler);
}, 10] call CFUNC(wait);

// [QGVAR(settings_enabled), 0] call MFUNC(cfgSettingSet);
