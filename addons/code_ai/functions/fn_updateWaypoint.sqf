/*
    Function:       FRL_AI_fnc_updateWaypoint
    Author:         Adanteh
    Description:    Checks where to send this squad
*/
#include "macros.hpp"

params ["_group"];

private _waypoint = [_group] call FUNC(getWaypointPosition);
if !(_waypoint isEqualTo [-1]) then {
    [_group, _waypoint] call FUNC(setWaypoint);
};
