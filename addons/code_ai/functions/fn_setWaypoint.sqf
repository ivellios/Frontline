/*
    Function:       FRL_AI_fnc_setWaypoint
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_group", "_position", ["_type", "MOVE"], ["_behaviour", "AWARE"], ["_formation", "##"], ["_speed", "FULL"], ["_fireMode", "YELLOW"]];

while {(count (waypoints _group)) > 0} do {
    deleteWaypoint ((waypoints _group) select 0);
};

private _waypoint = _group addWaypoint [_position, 0];
if (_formation isEqualTo "##") then {
    _formation = selectRandom [
    //    "COLUMN",
        "STAG COLUMN",
        "WEDGE",
        "ECH LEFT",
        "ECH RIGHT",
        "VEE",
        "LINE",
        //"FILE",
        "DIAMOND"
    ];
};

#ifdef DEBUGFULL
["debugMessage", [[_fnc_scriptNameShort, _group, _type, _behaviour, _formation, _speed], "purple"]] call CFUNC(globalEvent);
#endif

// -- ADVANCED BEHAVIOUR STARTS ONLY IF WE'RE CLOSE TO THE WAYPOINT (Enter buildings, go in cover, enter combat mode and so on).
private _fnc_groupPosition = {
    params ["_group"];
    private _units = units _group;
    private _averagePosition = [0, 0, 0];
    _units apply { _averagePosition = _averagePosition vectorAdd (getPosATL _x ) };
    (_averagePosition vectorMultiply (1 / (count _units)));
};

private _groupPosition = [_group] call _fnc_groupPosition;
private _distanceBehaviourChange = 100;
private _distanceToWaypoint = _groupPosition distance2D _position;
if (_distanceToWaypoint < _distanceBehaviourChange) then {
    _behaviour = "COMBAT";
};

if (_distanceToWaypoint < 50) then {
    #ifdef DEBUGFULL
        [[_fnc_scriptNameShort, "Frontline behaviour for:", _group, _distanceToWaypoint], "purple"] call MFUNC(debugMessage);
    #endif
    _behaviour = "COMBAT";
    _speed = "NORMAL";
    _combatMode = "RED";
    _type = "SAD";
    //_formation = "NO CHANGE"; // This crashes the game for some reason
};



// -- Set AI data
_group setFormation _formation;
_group setBehaviour _behaviour;
_group setCombatMode _combatMode;
_waypoint setWaypointType _type;
_waypoint setWaypointBehaviour _behaviour;
_waypoint setWaypointFormation _formation;
_waypoint setWaypointSpeed _speed;
_waypoint setWaypointCombatMode _combatMode;
