/*
    Function:       FRL_AI_fnc_handleSquads
    Author:         Adanteh
    Description:    Main loop to take care of spawning in AI
*/
#include "macros.hpp"

private _refreshRate = [QGVAR(settings_waypointRefreshRate), 30] call MFUNC(cfgSetting);
//private _strengthMinimum = [QGVAR(settings_strengthMinimum), 3] call MFUNC(cfgSetting);
private _strengthMaximum = [QGVAR(settings_strengthMax), 9] call MFUNC(cfgSetting);
private _mergeDistance = [QGVAR(settings_mergeDistance), 250] call MFUNC(cfgSetting);
private _allSquads = GVAR(namespace) getVariable ["groups", []];
private _totalUnits = 0;

#ifdef DEBUGFULL
    _refreshRate = 5;
#endif

// -- We do this even if automagic creation is disabled
{
    if (count ((units _x) select { alive _x }) > 0) then {
        // -- Update the waypoint every now and then
        // -- TODO: Don't override commander waypoints
        private _waypointUpdateAt = _x getVariable [QGVAR(waypointUpdateAt), -1e9];
        if (time > _waypointUpdateAt) then {

            // -- Disregard if commander waypoint is set
            if ((_x getVariable [QEGVAR(Commander,currentOrder), []]) isEqualTo []) then {
                [_x] call FUNC(updateWaypoint);
                _x setVariable [QGVAR(waypointUpdateAt), time + _refreshRate];
            };

            // -- Checks if squad is stuck (Position is still the same as before), and if it is, teleport units around a little
            private _stuck = [_x] call FUNC(unstuckRetardedPathfinding);
            // -- Give all units Usain Bolt capabilities
            #ifdef DEBUGFULL
                { _x setAnimSpeedCoef _runspeedModifier } forEach (units _x);
            #endif

        };
    };
} forEach _allSquads;

private _squadsSorted = [];
private _enabled = ([QGVAR(settings_enabled), 0] call MFUNC(cfgSetting)) > 0;
if (_enabled) then {
    private _strengthRequired = [QGVAR(settings_strengthMinimum), 3] call MFUNC(cfgSetting);
    private _overlimit = [QGVAR(settings_overLimitSquads), 3] call MFUNC(cfgSetting);
    private _expectedUnitCount = [QGVAR(settings_preferedUnitCount), 50] call MFUNC(cfgSetting);

    private _requiredAI = _expectedUnitCount - (count allPlayers);
    missionNamespace setVariable [QSVAR(aiPerSide), round (_requiredAI / 2), true];

    private _requiredGroups = round ((_requiredAI / 7) / 2); // -- Expect about 7 AI units in a squad, divide per 2 sides);
    private _maxSquads = _requiredGroups + _overlimit;

    // -- create arrays to keep track of how many squads there are
    private _sides = call MFUNC(getSides);
    private _squadsOnSide = [];
    _squadsOnSide resize (count _sides);
    _squadsOnSide = _squadsOnSide apply { 0 };

    private _squadsOnSideFull = [];
    private _sideCount = count _sides;
    _squadsOnSideFull resize _sideCount;
    _squadsOnSideFull = _squadsOnSideFull apply { 0 };

    {
        private _currentSide = _x;
        private _tempArr = [];
        {
            private _group = _x;
            if (side _group == _currentSide) then {
                _tempArr append [[_group, {alive _x} count units _group]];
            };
        } forEach _allSquads;

        // sort according to length
        private _result = [];
        private _l = count _tempArr;

        for [{_i = 0}, {_i < _l}, {_i = _i + 1}] do {
            private _smallest = _tempArr select _i;
            for [{_k = _i + 1}, {_k <= _l}, {_k = _k + 1}] do {
                private _element = _tempArr select _k;
                if ( (_smallest select 1) > (_element select 1) ) then {
                    _smallest = _element;
                };
            };
            _result append [_smallest];
        };
        _squadsSorted append [_result];

    } foreach _sides;

    {
        private _group = _x;
        private _side = side _group;
        private _units = units _group;
        if (_units isEqualTo []) then {
            // -- Cleanup debug waypoint
            #ifdef ISDEV
                private _marker = _group getVariable [QGVAR(waypointMarker), ""];
                deleteMarker _marker;
            #endif
            deleteGroup _group;
        } else {

            // -- If squad is above our required strength, count is as an AI squad on this side
            // TODO: Consider merging squads if squads are close together and at low strentgh
            private _sideIndex = _sides find _side;
            _totalUnits = _totalUnits + (count (_units select { alive _x }));
            if (_sideIndex >= 0) then {
                if (count _units > _strengthRequired) then {
                    _squadsOnSide set [_sideIndex, (_squadsOnSide select _sideIndex) + 1];
                };
                _squadsOnSideFull set [_sideIndex, (_squadsOnSideFull select _sideIndex) + 1];
            };
        };

    } forEach _allSquads;

    // -- Check if any new squads are required (But don't do that if there's still a whole bunch of small squads)
    {
        if (_x < _requiredGroups) then {
            if ((_squadsOnSideFull select _forEachIndex) < _maxSquads) then {
                private _side = _sides select _forEachIndex;

                // -- Cooldown between squad creation
                private _nextSquadCreateAt = GVAR(namespace) getVariable [format ["%1_nextCreateAt", _side], -1e9];
                if (time > _nextSquadCreateAt) then {
                    private _cooldown = [QGVAR(settings_squadCreateCooldown), 90] call MFUNC(cfgSetting);
                    GVAR(namespace) setVariable [format ["%1_nextCreateAt", _side], time + _cooldown];
                    [_side] call FUNC(createGroup);
                };
            };
        };
    } forEach _squadsOnSide;

    private _allSquads = GVAR(namespace) getVariable ["groups", []];
    GVAR(namespace) setVariable ["groups", _allSquads - [grpNull]];

    #ifdef DEBUGFULL
    [[_fnc_scriptNameShort, "Squads:", _squadsOnSideFull, _squadsOnSide, "Units:", _totalUnits], "lime"] call MFUNC(debugMessage);
    #endif
};

{
    scopeName "side";
    private _squadsSide =+ _x;
    private _squadsCt = count _squadsSide;
    for [{_i = 0}, {_i < _squadsCt}, {_i = _i + 1}] do {
        (_squadsSide select _i) params ["_grpJoining", "_grpJoiningCt"];
        // -- if a smaller squad combined wont fit the bill, no need to check further with bigger ones
        private _checkLimit = _squadsCt;
        scopeName "squads";
        for [{_k = _i + 1}, {_k < _checkLimit}, {_k = _k + 1}] do {
            (_squadsSide select _k) params ["_grpToJoin", "_grpToJoinCt"];
            // -- check if squads combined do no exceed a certain size
            if (_grpToJoinCt + _grpToJoinCt > _strengthMaximum) then {
                // -- no further need to look for groups as they are sorted by size
                breakTo "squads";
                _checkLimit = _forEachIndex;
            } else {

                _grpJoiningPos = [_grpJoining] call MFUNC(getGroupPosition);
                _grpToJoinPos = [_grpToJoin] call MFUNC(getGroupPosition);
                if (_mergeDistance < (_grpJoiningPos distance2D _grpToJoinPos)) then {
                    units _grpJoining joinSilent _grpToJoin;
                    _squadsSide set [_k, [_grpToJoin, ({alive _x} count units _grpToJoin)] ];
                    breakTo "squads";
                };
                
            };
        };
    };
} forEach _squadsSorted;
