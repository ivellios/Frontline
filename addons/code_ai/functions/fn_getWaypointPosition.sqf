/*
    Function:       FRL_AI_fnc_getWaypointPosition
    Author:         Adanteh
    Description:    Figures out where to send given group
*/
#include "macros.hpp"

params ["_group"];

private _units = (units _group select { alive _x });
if (_units isEqualTo []) then { _units = leader _group };

// -- Start with a bit random position, so we don't send squads to the same position
private _groupBias = _group getVariable [QGVAR(waypointBias), []];
if (_groupBias isEqualTo []) then {
    _groupBias = [0, 0, 0] apply { RANDOMOFFSET(0,250) + (selectRandom [-100, 100]) };
    _groupBias set [2, 0];
    _group setVariable [QGVAR(waypointBias), _groupBias];
};

private _averagePosition = ([_group] call MFUNC(getGroupPosition)) vectorAdd (_groupBias vectorMultiply 1);
private _AI_waypointPosition = _averagePosition;
private _side = side _group;

// -- Return [-1] to not update the waypoint
["AI_waypointPosition", [_side, _averagePosition, _group], true] call MFUNC(checkConditions);

#ifdef ISDEV
    if !(_AI_waypointPosition isEqualTo [-1]) then {
        _marker = _group getVariable [QGVAR(waypointMarker), ""];
        if (_marker == "") then {
            _marker = createMarker [str _group, _AI_waypointPosition];
            _group setVariable [QGVAR(waypointMarker), _marker];
        };

        _marker setMarkerShape "ICON";
        _marker setMarkerType "mil_dot";
        _marker setMarkerColor (["ColorEAST", "ColorWEST"] param [[_side] call BIS_fnc_sideID, "ColorYellow"]);
        _marker setMarkerPos _AI_waypointPosition;
        _marker setMarkerText format ["%1: %2", name leader _group, groupID _group];
    };

#endif

// TODO: Change to combat mode is waypoint is close
// TOOD: If units are very spead out, change to combat mode and don't move waypoint to the frontline

_AI_waypointPosition
