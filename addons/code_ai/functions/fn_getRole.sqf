/*
    Function:       FRL_AI_fnc_getRole
    Author:         Adanteh
    Description:    Gets role to equip to the unit
*/
#include "macros.hpp"

params ["_unit", "_groupType"];

private _roles = EGVAR(Rolesspawns,squadNamespace) getVariable [(format ["%1_roles", _groupType]), []];


// -- Reverse role check (Start at the fanciest kits), figure out which kits are available.
private _rolesPossible = [];
private _rolesLeader = [];
{
    _x params ["_roleTag"];
    if ([_unit, _roleTag] call EFUNC(rolesspawns,roleCanUse)) then {

        // -- Filter out crewman kits
        if !([_roleTag, "crewman"] call EFUNC(rolesspawns,roleHasAbility)) then {
            _rolesPossible pushBack _roleTag;

            if ([_roleTag, "leader"] call EFUNC(rolesspawns,roleHasAbility)) then {
                _rolesLeader pushBack _roleTag;
            };
        };
    };
} forEach _roles;

// TODO: Error reporting if no kit is found
if (_rolesPossible isEqualTo []) exitWith { "" };
private _leader = leader _unit isEqualTo _unit;

if (_leader && { !(_rolesLeader isEqualTo []) }) exitWith { selectRandom _rolesLeader };
private _role = selectRandom _rolesPossible;


_role
