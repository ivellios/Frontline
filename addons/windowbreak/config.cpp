#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            path = "\pr\FRL\addons\windowbreak";
            dependencies[] = {"Main"}; // -- Requires keybind system in main

            class clientInit;
            class breakWindow;
            class findBuilding;
        };
    };
};
