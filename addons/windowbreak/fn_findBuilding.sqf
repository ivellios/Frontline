/*
    Function:       FRL_WindowBreak_fnc_findBuilding
    Author:         N3croo
    Description:    find building the player is inside or is looking at
    returns:        object
    [getpos player] call FRL_WindowBreak_fnc_findBuilding
*/
#include "macros.hpp"

if (Clib_player != vehicle CLib_Player) exitwith {};

params ["_playerPos"];
_playerPos = _playerPos vectorAdd [0,0,getTerrainHeightASL _playerPos];

private _buildings = [];

{
  private _endPos = (_playerPos vectorAdd _x);
  private _intersectedObjs = lineIntersectsWith [_playerPos, _endPos, objNull, objNull, true];
  if ({_x iskindOf "Building"} count _intersectedObjs > 0) exitWith {
    _buildings = _intersectedObjs;
  };
}foreach [[0,0,-3],[0,0,3],((eyeDirection Clib_Player) vectormultiply 20)];

_buildings;
