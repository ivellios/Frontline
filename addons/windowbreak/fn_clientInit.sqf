/*
    Function:       FRL_WindowBreak_fnc_clientInit
    Author:         N3croo
    Description:    Adds keybind for window breaking
*/
#include "macros.hpp"

[	"General", // -- Keybind category
	"WindowBreak", // -- Keybind slug (Unique name within category)
	["Break Window", "Smashes the window if it's in front of you"], // -- Display name and tooltip
	{ _this call FUNC(breakWindow); false; }, // -- Keydown code
	{ false }, // -- Keyup code
	"deployWeaponAuto", // -- Keybind to use by default (Can also use [DIK_CODE, [_shift, _ctrl, _alt]])
	false, // -- CBA Compat (Ignore)
	false // -- If keyup code should respect same modifier keys (Usually this is off)
] call MFUNC(addKeybind);
