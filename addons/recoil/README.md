## Contents

Vanilla recoil configs. Makes guns not behave like laser pointers.


if its a 4 array value its for differing stances, support, deployment and stance provide bonuses check recoildecrease for that

FRLRecoilMax maximum multiplier reachable
FRLRecoilTimeconstant number of shots needed till 2/3 of the maximum recoil coeff is reached
FRLRecoilDrift [0.25,0.3] first param is the max driftrate per short  ranges from 0-1, second param is the   driftpower applied as a multiplier to current recoil factor
FRLRecoilResetTime time needed for the multiplier to fully drop
FRLRecoilShakePwr camera shake effect I'd recommend not going above 4, a good reference are the ironsights
