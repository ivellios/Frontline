class CfgWeaponHandling{
	class Stabilization{
			characterPoints[] = {"lElbow", "rElbow"};
			weaponPoints[] = {"Usti hlavne", "Konec hlavne"};
			groundLimits[] = {0.339626, 0.414159};//			groundLimits[] = {0.139626, 0.314159};
			objectLimits[] = {0.349066, 0.837758};//			objectLimits[] = {0.349066, 0.837758};
			upperBodyRadius = 0.3;//upperBodyRadius = 0.12;
			weaponRadius = 0.4;//weaponRadius = 0.4;
			restingCoef = 0.2;//0.4;
			restingProneCoef = 0.06;//0.08;
			restingRecoil = 0.28;//restingRecoil = 0.5;
			restingRecoilPersistent = 0.28;//restingRecoilPersistent = 0.5;
			deployedCoef = 0.1;
			deployedProneCoef = 0.02;
			deployedRecoil = 0.2;//deployedRecoil = 0.5;//
			deployedRecoilPersistent = 0;
			deployTime = 0.1;//deployTime = 0.2;
			undeployTime = 0.1;//undeployTime = 0.2;
			deployBipodTime = 0.4;
			undeployBipodTime = 0.3;
	};
};