#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};

        requiredAddons[] = {"FRL_Main"};
    };
};

class FRL {
    class Modules {
        class MODULE {
            path = "\pr\FRL\addons\recoil\functions";
            dependencies[] = {"Main"};

            class clientInit;
            class recoilDecrease;
            class updateRecoilParameters;
        };
    };
};


#include "recoils.hpp"
