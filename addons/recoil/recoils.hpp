#include "macros.hpp"

//muzzleOuter[] = {Right/left , Up/down ,  Random left/right , Random up/down};
//permanant = Climb;
//temporary = Short term;
//kickback = {min,max};

#define rifleKickback65 {0.0375,0.0433}
#define carabineKickback65 {0.04,0.0455}

#define rifleKickback556 {0.033,0.04}
#define carabineKickback556 {0.0366,0.0425}

#define LMG_MMKickback65 {0.0375,0.0415}
#define DMR_Kickback762 {0.065,0.08}

class CfgRecoils {
    class Default;
    class recoil_default : Default {
        muzzleOuter[] = {0.3, 1, 1.1, 0.75};// muzzleOuter[] = {0.3, 1, 0.3, 0.2};
        muzzleInner[] = {0, 0, 0.1, 0.1};
        kickBack[] = {0.03, 0.06};
        permanent = 0.1;
        temporary = 0.0125;
    };
    class recoil_ebr: recoil_default {
        muzzleOuter[] = {0.6,2,0.6,0.4};
        muzzleInner[] = {0, 0, 0.1, 0.1};
        kickBack[] = {0.06,0.07};   // its a bit heavier of a DMR so less visual kickBack
        permanent = 0.166;
        temporary = 0.01;
    };
    // mmgs get a copy paste of the LMG recoil from IFA
    class recoil_mmg_01: recoil_default {
        muzzleOuter[] = {0.515, 1.25, 0.55 , 0.66};
        muzzleInner[] = {0, 0, 0, 0};
        kickBack[] = {0.0375, 0.045};
        permanent = 0.225;
        temporary = 0.0166;
    };
    class recoil_mmg_02: recoil_default {
        muzzleOuter[] = {0.515, 1.25, 0.55 , 0.66};
        muzzleInner[] = {0, 0, 0, 0};
        kickBack[] = {0.0375, 0.045};
        permanent = 0.225;
        temporary = 0.0166;
    };
    // rahim
    class recoil_dmr_01 : recoil_default {
        muzzleOuter[] = {0.66,1.85,0.575,0.45};// muzzleOuter[] = {0.3, 1, 0.3, 0.2};
        muzzleInner[] = {0, 0, 0.1, 0.1};
        kickBack[] = DMR_Kickback762;
        permanent = 0.225;
        temporary = 0.0125;
    };
    // mk20 series, derivated from katiba recoil
    class recoil_mk20 : recoil_default {
        kickBack[] = rifleKickback556;
        muzzleInner[] = {0,0,0.1,0.1};
        muzzleOuter[] = {0.366,1.15,0.32,0.315};
        permanent = 0.1;
        temporary = 0.013;
    };
    class recoil_mk20c : recoil_default {
        kickBack[] = carabineKickback556;
        muzzleInner[] = {0,0,0.1,0.1};
        muzzleOuter[] = {0.375,1.15,0.33,0.315};
        permanent = 0.115;
        temporary = 0.015;
    };
    class recoil_trg20: recoil_mk20c {

    };
    class recoil_trg21: recoil_mk20 {

    };
    class recoil_mk200 : recoil_default {
        kickBack[] = {0.033,0.0366};
        muzzleInner[] = {0,0,0.1,0.1};
        muzzleOuter[] = {0.4,0.86,0.46,0.366};
        permanent = 0.09;
        temporary = 0.0085;
    };
    // -- mx series
    class recoil_mx : recoil_default {
        kickBack[] = rifleKickback65;
        muzzleInner[] = {0,0,0.1,0.1};
        muzzleOuter[] = {0.415,1.1,0.266,0.33};
        permanent = 0.133;
        temporary = 0.0125;
    };
    class recoil_mxc : recoil_mx {
        kickBack[] = carabineKickback65;
        muzzleOuter[] = {0.415,1.1,0.266,0.33};
        permanent = 0.14;
        temporary = 0.013;
    };
    class recoil_mxm : recoil_mx {
        kickBack[] = LMG_MMKickback65;
        muzzleOuter[] = {0.415,1.1,0.266,0.3};
        permanent = 0.125;
        temporary = 0.01;
    };
    class recoil_sw : recoil_default {
        kickBack[] = LMG_MMKickback65;
        muzzleInner[] = {0,0,0.1,0.1};
        muzzleOuter[] = {0.375,0.875,0.25,0.28};
        permanent = 0.11;
        temporary = 0.0115;
    };
    class recoil_ktb : recoil_default {
        kickBack[] = rifleKickback65;
        muzzleInner[] = {0,0,0.1,0.1};
        muzzleOuter[] = {0.325,1.25,0.26,0.315};
        permanent = 0.12;
        temporary = 0.0125;
    };
    class recoil_ktbc : recoil_ktb {
        kickBack[] = carabineKickback65;
        muzzleOuter[] = {0.325,1.25,0.26,0.315};
        permanent = 0.15;
        temporary = 0.0133;
    };
    class recoil_zafir : recoil_default {
        kickBack[] = {0.03,0.0375};
        muzzleInner[] = {0,0,0.1,0.1};
        muzzleOuter[] = {0.5,1,0.475,0.3};
        permanent = 0.1;
        temporary = 0.01;
    };

    // -- Apex
    class recoil_ak12: recoil_default {
        muzzleOuter[] = {0.315, 0.65, 0.325, 0.3};
		kickBack[] = {0.04, 0.045};
		permanent = 0.32;
		temporary = 0.005;
    };
    class recoil_akm: recoil_default {
        muzzleOuter[] = {0.315, 0.65, 0.325, 0.3};
		kickBack[] = {0.05, 0.06};
		permanent = 0.32;
		temporary = 0.005;
    };
    class recoil_aks: recoil_default {
        muzzleOuter[] = {0.366, 0.8, 0.25, 0.3};
		kickBack[] = {0.0575, 0.07};
		permanent = 0.25;
        temporary = 0.005;
    };
    // chinese things
    class recoil_car: recoil_default {
        muzzleOuter[] = {0.28, 0.733, 0.25, 0.3};
        kickBack[] = {0.0266, 0.0366};
        permanent = 0.2;
    };
    class recoil_car_lsw: recoil_car {
        temporary = 0.0115;
    };
    class recoil_car_dmr: recoil_car {
        muzzleOuter[] = {0.5,2,0.5,0.5};
        kickBack[] = {0.0425, 0.05};
        permanent = 0.2;
        temporary = 0.015;
    };
    // P90 thingy
    class recoil_smg_03: recoil_default {
        muzzleOuter[] = {0.2,1.7,0.525,0.3}; //-- {0.2,1.5,0.2,0.3}; vanilla
        kickBack[] = {0.07, 0.09};
        permanent = 0.2;
        temporary = 0.0125;
    };
    // 416s
    class recoil_spar: recoil_default {
        muzzleOuter[] = {0.285, 0.725, 0.275 , 0.325};
        kickBack[] = {0.03, 0.04};
        permanent = 0.2;
    };
    class recoil_spar_lsw: recoil_spar {
        muzzleOuter[] = {0.275, 0.715, 0.25, 0.3};
        kickBack[] = {0.025, 0.0325};
        permanent = 0.175;
        temporary = 0.0075;
    };
    class recoil_spar_dmr: recoil_spar {
        muzzleOuter[] = {0.5,2,0.5,0.5};
        kickBack[] = {0.0425, 0.05};
        permanent = 0.2;
        temporary = 0.015;
    };
};

#define AR556Shake 2.1
#define AR65Shake 2.6
#define LMG65Shake 1.75
#define LMG762Shake 3.25
#define DMR_Shake762 4.5

class CfgWeapons {
    class Default;
    class RifleCore : Default {};
    class Rifle : RifleCore {};
    /*
    if its a 4 array value its for differing stances, support, deployment and stance provide bonuses check recoildecrease for that

    FRLRecoilMax maximum multiplier reachable
    FRLRecoilTimeconstant number of shots needed till 2/3 of the maximum recoil coeff is reached
    FRLRecoilDrift [0.25,0.3] first param is the max driftrate per shot  ranges from 0-1, second param is the driftpower applied as a multiplier to current recoil factor
    FRLRecoilResetTime time needed for the multiplier to fully drop
    FRLRecoilShakePwr camera shake effect I'd recommend not going above 4, a good reference are the ironsights
    */
    class Rifle_Base_F : Rifle {
        FRLRecoilMax[] = {1.55,1.4,1.35,1.25};
        FRLRecoilMin = 1;
        FRLRecoilTimeconstant[] = {4.5,6,7,11};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.25,0.1}; //reserved for MG or mby prone
        FRLRecoilShakePwr = 2.75;
    };
    class Rifle_Long_base_F : Rifle_Base_F {
        FRLRecoilMax[] = {1.4,1.35,1.3,1.3};
        FRLRecoilResetTime[] = {0.175,0.15,0.135,0.125};
        FRLRecoilShakePwr = 2.5;
        RLRecoilTimeconstant[] = {6,9,11,15};
        FRLRecoilDrift[] = {0.2, 0.3}; //reserved for MG or mby prone
    };
    // -- vanilla weapons
    // -- katiba series
    class arifle_AK12_base_F: Rifle_Base_F {
        inertia = 0.9;
        FRLRecoilTimeconstant[] = {5.5,7,9,11};
        FRLRecoilShakePwr = 2.75;
    };
    class arifle_AK12_GL_base_F: arifle_AK12_base_F {
        inertia = 0.95;
    };
    class arifle_AKM_base_F: Rifle_Base_F {
        inertia = 0.8;
        FRLRecoilShakePwr = 3;
    };
    class arifle_AKS_base_F: Rifle_Base_F {
        FRLRecoilMax[] = {1.45,1.4,1.35,1.333};
		FRLRecoilShakePwr = 3;
    };
    class arifle_ARX_base_F: Rifle_Base_F {
        inertia = 0.65;
        FRLRecoilMax[] = {1.6,1.4,1.35,1.25};
        FRLRecoilTimeconstant[] = {3.75,6,7,11};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.22,0.075}; //reserved for MG or mby prone
        FRLRecoilShakePwr = AR65Shake;
    };
    class arifle_CTAR_base_F: Rifle_Base_F {
        FRLRecoilMax[] = {1.45,1.33,1.3,1.25};
        FRLRecoilTimeconstant[] = {4.5,7,8,12};
        FRLRecoilResetTime[] = {0.25,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.22,0.075}; //reserved for MG or mby prone
        FRLRecoilShakePwr = AR556Shake;
    };
    class arifle_CTAR_GL_base_F: arifle_CTAR_base_F {
        inertia = 0.6;
    };
    class arifle_Katiba_Base_F : Rifle_Base_F {
        FRLRecoilMax[] = {1.6,1.4,1.35,1.25};
        FRLRecoilTimeconstant[] = {3.75,6,7,11};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.22,0.075}; //reserved for MG or mby prone
        FRLRecoilShakePwr = AR65Shake;
    };
    class arifle_Katiba_C_F : arifle_Katiba_Base_F {

    };
    class arifle_Katiba_GL_F : arifle_Katiba_Base_F {

    };

    // bohemia you turds could you actually have a consistent naming sheme in the game?
    class mk20_base_F: Rifle_Base_F {
        FRLRecoilMax[] = {1.55,1.4,1.3,1.25};
        FRLRecoilMin = 1;
        FRLRecoilTimeconstant[] = {4.5,7,8,12};
        FRLRecoilResetTime[] = {0.25,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.22,0.075}; //reserved for MG or mby prone
        FRLRecoilShakePwr = AR556Shake;
    };

    // -- mx-series
    class arifle_MX_Base_F : Rifle_Base_F {
        FRLRecoilMax[] = {1.675,1.45,1.45,1.25};
        FRLRecoilMin = 1;
        FRLRecoilTimeconstant[] = {4.5,6,7,11};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.2,0.075}; //reserved for MG or mby prone
        FRLRecoilShakePwr = AR65Shake;
    };
    class arifle_MXC_F : arifle_MX_Base_F {

    };
    class arifle_MXM_F : arifle_MX_Base_F {
        inertia = 0.9;
        FRLRecoilShakePwr = LMG65Shake;
    };
    class SDAR_base_F: Rifle_Base_F {};
    // SPAR LMG
    class arifle_SPAR_02_base_F: Rifle_Base_F {
        inertia = 0.7;
        FRLRecoilTimeconstant[] = {5.5,7,9,11};
    };
    class arifle_SPAR_03_base_F: Rifle_Base_F {
        inertia = 1;
        FRLRecoilDrift[] = {0.2, 0.3}; //reserved for MG or mby prone
        FRLRecoilMax[] = {1.5,1.45,1.3,1.3};
        FRLRecoilShakePwr = 3;
        RLRecoilTimeconstant[] = {4,6,8,9};
    };
    // # Naming sheme
    // copy of the Mk20 recoil but well can't just copy the mk20 with all params
    class Tavor_base_F: Rifle_Base_F {
        FRLRecoilMax[] = {1.55,1.4,1.3,1.25};
        FRLRecoilMin = 1;
        FRLRecoilTimeconstant[] = {4.5,7,8,12};
        FRLRecoilResetTime[] = {0.25,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.22,0.075}; //reserved for MG or mby prone
        FRLRecoilShakePwr = AR556Shake;
    };
    // LMGs
    class arifle_MX_SW_F : arifle_MX_Base_F {
        FRLRecoilMax[] = {1.425,1.266,0.9,0.9};
        FRLRecoilMin = 1;
        FRLRecoilTimeconstant[] = {5.5,6.5,7,11};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.25,0.35};
        FRLRecoilShakePwr = LMG65Shake;
        inertia = 1.5;
    };
    class DMR_01_base_F: Rifle_Long_base_F {
        FRLRecoilMax[] = {1.6,1.5,1.3,1.3};
        FRLRecoilTimeconstant[] = {3,4.5,6,7};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.25,0.35};
        FRLRecoilShakePwr = DMR_Shake762;
    };
    class DMR_07_base_F: Rifle_Long_Base_F {
        inertia = 1.6;
        FRLRecoilShakePwr = DMR_Shake762;
    };
    class EBR_base_F: Rifle_Long_base_F {
        FRLRecoilMax[] = {1.6,1.5,1.3,1.3};
        FRLRecoilTimeconstant[] = {3,4.5,6,7};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.25,0.35};
        FRLRecoilShakePwr = DMR_Shake762;
        inertia = 1.8;
    };
    class LMG_Mk200_F : Rifle_Long_base_F {
        FRLRecoilMax[] = {1.33,1.15,0.9,0.85};
        FRLRecoilMin = 1;
        FRLRecoilTimeconstant[] = {5,9,12,15};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.25,0.26};
        FRLRecoilShakePwr = LMG65Shake;
        inertia = 1.8;
    };
    class LMG_Zafir_F : Rifle_Long_Base_F {
        inertia = 2.7;
        FRLRecoilMax[] = {1.6,1.2,0.9,0.9};
        FRLRecoilMin = 1;
        FRLRecoilTimeconstant[] = {4,6,12,15};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        FRLRecoilDrift[] = {0.25,0.3};
        FRLRecoilShakePwr = LMG762Shake;
    };
};

class CfgWeaponHandling {

    class Recoil {
        kickVisual  = 0.5;  //multiplier for extra camera-only movement for weapon kickback
        impulseCoef  = 1;  //common coefficient for weapon-based recoil configuration values, increase to make all recoils stronger, decrease to make them all weaker
        prone    = 10;  //coefficient of recoil forces while in prone (this is further multiplied by weapon resting coefficients)
    };


    class Stabilization {
        characterPoints[] = {"lElbow", "rElbow"};
        weaponPoints[] = {"Usti hlavne", "Konec hlavne"};
        groundLimits[] = {0.339626, 0.414159};//			groundLimits[] = {0.139626, 0.314159};
        objectLimits[] = {0.349066, 0.837758};//			objectLimits[] = {0.349066, 0.837758};
        upperBodyRadius = 0.3;//upperBodyRadius = 0.12;
        weaponRadius = 0.4;//weaponRadius = 0.4;
        restingCoef = 0.25;//0.4;
        restingProneCoef = 0.08;//0.08;
        restingRecoil = 0.35;//restingRecoil = 0.5;
        restingRecoilPersistent = 0.28;//restingRecoilPersistent = 0.5;
        deployedCoef = 0.1;
        deployedProneCoef = 0.05;
        deployedRecoil = 0.28;//deployedRecoil = 0.5;//
        deployedRecoilPersistent = 0.28;
        deployTime = 0.1;//deployTime = 0.2;
        undeployTime = 0.1;//undeployTime = 0.2;
        deployBipodTime = 0.4;
        undeployBipodTime = 0.3;
    };
};
