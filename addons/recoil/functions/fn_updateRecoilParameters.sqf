/*
    Function:       FRL_Recoil_fnc_updateRecoilParameters
    Author:         N3Croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _weapon = primaryWeapon Clib_Player;

private _temp =  getArray (configfile >> "CfgWeapons" >> _weapon >> "FRLRecoilMax");
if (count _temp == 0) then {
	_temp = [2.65,2.45,2.34,2.3]; //maximum multiplier reachable
};
GVAR(maxRecoil) = _temp;

_temp = getArray (configfile >> "CfgWeapons" >> _weapon >> "FRLRecoilTimeconstant");
if (count _temp == 0) then {
	_temp = [3,4,6,9];
};
GVAR(timeConstant) = _temp;

_temp = getArray (configfile >> "CfgWeapons" >> _weapon >> "FRLRecoilResetTime");
if (count _temp == 0) then {
	_temp = [0.35,0.25,0.25,0.2];
};
GVAR(resetTime) = _temp;

_temp = getArray (configfile >> "CfgWeapons" >> _weapon >> "FRLRecoilDrift");
if (count _temp == 0) then {
	_temp = [0.25,0.3];
};
GVAR(recoilDriftParams) = _temp;

_temp = getNumber (configfile >> "CfgWeapons" >> _weapon >> "FRLRecoilMin");
if (_temp == 0) then {
	_temp = 1;
};
GVAR(minRecoil) = _temp;

_temp = getNumber (configfile >> "CfgWeapons" >> _weapon >> "FRLRecoilShakePwr");
if (_temp == 0) then {
	_temp = 5;
};
GVAR(camShakePwr) = _temp;

GVAR(ROF) = 60/700;
private _rof = 0;
{
	_rof = getNumber (configfile >> "CfgWeapons" >> _weapon >> _x >> "reloadTime");
	if (_rof > 0) exitWith {
		GVAR(ROF) = _rof;
	};
} foreach ["FullAuto", "Single"];
GVAR(ROF) = (60/400) min GVAR(ROF); // for seriously low ROF weapons;

_maxRecoil = GVAR(maxRecoil);
GVAR(maxRecoil) = _maxRecoil apply { _x - GVAR(minRecoil) }; //implementation of ROF affecting maxrecoil

//manual override for testing purposes
#ifdef DEBUGFULL
	if ( false ) then {
		systemChat "overriding recoilscript with parameters";
		//[player,5] call bis_fnc_tracebullets;
		GVAR(maxRecoil) = [1.55,1.4,1.35,1.25];
		GVAR(timeConstant) = [4.5,6,7,11];
		GVAR(resetTime) = [0.3,0.25,0.2,0.18];
		GVAR(recoilDriftParams) = [0.25,0.1];
		GVAR(minRecoil) = 1;
		GVAR(camShakePwr) = 8;
	} else {
		systemChat "default recoilscript params";
	};

	systemChat str GVAR(maxRecoil);
	systemChat str GVAR(timeConstant);
	systemChat str GVAR(resetTime);
	systemChat str GVAR(recoilDriftParams);
	systemChat str GVAR(minRecoil);
	systemChat str GVAR(camShakePwr);

#endif
