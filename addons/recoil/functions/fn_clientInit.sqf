/*
    Function:       FRL_Recoil_fnc_clientInit
    Author:         N3Croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

// -- Load settings on start
GVAR(recoilDrift) = 0;
GVAR(decreasePFH) = -1;
GVAR(current) = 0;
call FUNC(updateRecoilParameters);

//universal parameters to be Triggered upon gearchange
["currentWeaponChanged", {
	call FUNC(updateRecoilParameters);
}] call CFUNC(addEventHandler);

private _code = {
	params ["_unit", "_weapon", "", "", "", "", "", "_vehicle"];
	if !(isNull _vehicle) exitWith { }; // -- Firing vehicle weapon (Not FVF)
	if (_weapon == primaryweapon _unit) then {

		GVAR(recoilDrift) = GVAR(recoilDrift) - (GVAR(recoilDriftParams) select 0) + random (2 * (GVAR(recoilDriftParams) select 0));
		GVAR(recoilDrift) = (0 max GVAR(recoilDrift)) min 1;

		GVAR(current) = GVAR(current) + 1;
		if (GVAR(decreasePFH == -1)) then {
			enableCamShake true;
			GVAR(decreasePFH) = [FUNC(recoilDecrease), GVAR(ROF), [0, 1]] call MFUNC(addPerFrameHandler);
		};
		if ( 60/400 > GVAR(ROF) && player ammo primaryWeapon player > 0) then {
			addcamshake [GVAR(camShakePwr), GVAR(ROF), 2/GVAR(ROF)];	// -- 2 cycles
		} else {	// -- simulate last rounds bolt holdopen or manually operated guns, only one cycle
			addcamshake [GVAR(camShakePwr), 0.15, 5];
		};

	};
};

// -- Make sure we remove old EH, in case we're adding a new one (and the EH carries over)
// -- Some EHs carry over, others don't. Completely unpredictable, plus this has support for team switching
private _index = Clib_Player addEventHandler ["firedMan", _code];
["playerChanged", {
    params ["_data", "_params"];
    _data params ["_currentPlayer", "_oldPlayer"];
    _params params ["_name", "_code", "_index"];

    _oldPlayer removeEventHandler [_name, _index];
    _currentPlayer removeEventHandler [_name, _index];
    _params set [2, _currentPlayer addEventHandler [_name, _code]];
}, ["firedMan", _code, _index]] call CFUNC(addEventHandler);

GVAR(hazeParticleSettings) = [];
if ( isClass (configFile >> "RHS_HeatHaze") ) then {
	GVAR(hazeParticleSettings) = [ [5,"RHS_HeatHaze"], [20,"RHS_HeatHaze1"] ];
} else {
	 if ( isClass (configFile >> "LIB_RifleSmokeTrail") ) then {
		 GVAR(hazeParticleSettings) = [ [10,"LIB_RifleSmokeTrail"] ];
	 };
};
