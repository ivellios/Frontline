/*
    Function:       FRL_Recoil_fnc_recoilDecrease
    Author:         N3croo
    Description:    Decreases recoil over time
*/
#include "macros.hpp"

params ["_args", "_handle"];
_args params ["_oldRecoil", "_maxCurrent"];

// -- Determine how much recoil should be lowered (Not sure why this is in a call <adanteh>)
private _recoilType = 0;
if (isWeaponRested player) then {
	_recoilType = _recoilType + 1;
};

if (isWeaponDeployed player) then {
	_recoilType = _recoilType + 2;
};

if (stance player == "PRONE") then {
	_recoilType = _recoilType + 2;
};
if (stance player == "CROUCH") then {
	_recoilType = _recoilType + 1;
};
_recoilType = _recoilType min 3;

private _fact = ( GVAR(maxRecoil) select _recoilType) * (1 - exp - (GVAR(current) / (GVAR(timeConstant) select _recoilType)));
_fact = GVAR(minRecoil) + _fact * (1 + (GVAR(recoilDriftParams) select 1) * GVAR(recoilDrift) );

Clib_Player setUnitRecoilCoefficient _fact;

{
	_x params ["_minCount", "_particle"];
	private _roundedMaxCurrent = floor _maxCurrent;
	if ( _roundedMaxCurrent == _minCount) exitWith {
		if ( isNil QGVAR(HazeParticle) )  then {
			GVAR(HazeParticle) = "#particlesource" createVehicleLocal [0,0,0];
			GVAR(HazeParticle) attachto [player, [-0.05, 0.4, 0.02], "rwrist"];
		};
		GVAR(HazeParticle) setParticleClass _particle;
	};
} forEach GVAR(hazeParticleSettings);

if (_oldRecoil == GVAR(current) ) then { // no "fullauto ROF" starting to decrease
	GVAR(current) = GVAR(current) - (_maxCurrent *  ( GVAR(ROF) / (GVAR(resetTime) select _recoilType) ) );
} else {
	_args set [1, GVAR(current) max _maxCurrent];
	#ifdef DEBUGFULL
		systemChat format ["recoilType: %1 | factor: %2 | ", _recoilType, _fact];
		// systemChat format ["coeff %1 | recoildrift %2 in percent", unitRecoilCoefficient player, ( 100 * ( (GVAR(recoilDriftParams) select 1) * GVAR(recoilDrift) )) ];
	#endif
};


if (GVAR(current) <= 0) exitWith {
	if !(isNil QGVAR(HazeParticle) ) then {
		deleteVehicle GVAR(HazeParticle);
		GVAR(HazeParticle) = nil;
	};
	[_handle] call MFUNC(removePerFrameHandler);
	GVAR(current) = 0;
	GVAR(decreasePFH) = -1;
	GVAR(recoilDrift) = 0;
	Clib_Player setUnitRecoilCoefficient GVAR(minRecoil);
};

_args  set [0, GVAR(current)]; // -- Update last recoil
