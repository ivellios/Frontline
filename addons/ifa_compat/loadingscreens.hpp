class loadingScreen {
  default = "\pr\frl\addons\ifa_compat\images\winter_01.paa";
  MCN_Neaville[] = {
    "\pr\frl\addons\ifa_compat\images\neaville_01.paa"
  };

  MCN_Neaville_Winter[] = {
    "\pr\frl\addons\ifa_compat\images\winter_01.paa"
  };

  PLR_Bulge[] = {
    "\pr\frl\addons\ifa_compat\images\bulge_01.paa"
  };

  I44_Omaha_v2[] = {
    "\pr\frl\addons\ifa_compat\images\omaha_01.paa"
  };

  Panovo[] = {
    "\pr\frl\addons\ifa_compat\images\panovo_01.paa"
  };

  ivachev[] = {
    "\pr\frl\addons\ifa_compat\images\ivachev_01.paa"
  };

  Baranow[] = {
    "\pr\frl\addons\ifa_compat\images\baranow_01.paa",
    "\pr\frl\addons\ifa_compat\images\baranow_02.paa"
  };

  staszow[] = {
    "\pr\frl\addons\ifa_compat\images\staszow_01.paa",
    "\pr\frl\addons\ifa_compat\images\staszow_02.paa"
  };
};
