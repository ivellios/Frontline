#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(61K) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class CfgMagazines {
    class VehicleMagazine;
    class LIB_5x_61k: VehicleMagazine {
        scope = 2;
        displayName = "61-K";
        ammo = "LIB_B_37mm_AA";
        count = 5;
        initSpeed = 1880;
        maxLeadSpeed = 500;
        tracersEvery = 1;
        nameSound = "Magazine";
    };
};

class CfgAmmo {
    class LIB_Bullet_AA_base;
    class LIB_B_37mm_AA: LIB_Bullet_AA_base {
        model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_red.p3d";
        tracerScale = 6.85;
        tracerEndTime = 4.6;
    };
};


// This has a habit of fucking up something (Scripts, some weird behaviour) with configs,
// You're supposed to track inherited subclasses to their origin
class NewTurret;
class Turrets;
class MainTurret;
class ViewOptics;
class CommanderOptics;
class ViewGunner;
class ViewCargo;

class cfgVehicles {
    class Land;
    class LandVehicle: Land {
        class CommanderOptics: NewTurret { };
        class ViewOptics: ViewOptics { };
        class ViewGunner: ViewCargo { };
    };

    class StaticWeapon: LandVehicle {};
    class StaticCannon: StaticWeapon {};
    class LIB_StaticCannon_base: StaticCannon {};
    class LIB_61k_base: LIB_StaticCannon_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                weapons[] = {"LIB_61k"};
                magazines[] = {mag_48("LIB_5x_61k")};
                maxHorizontalRotSpeed = 1.45;
                maxVerticalRotSpeed = 1.45;
                class ViewOptics {
                    initAngleX = 0;
                    minAngleX = ntru-100;
                    maxAngleX = 100;
                    initAngleY = 0;
                    minAngleY = -100;
                    maxAngleY = 100;
                    initFov = 0.093;
                    minFov = 0.093;
                    maxFov = 0.093;
                };

                class OpticsIn {
                    class Wide: ViewOptics {
                        initAngleX = 0;
                        minAngleX = -100;
                        maxAngleX = 100;
                        initAngleY = 0;
                        minAngleY = -100;
                        maxAngleY = 100;
                        initFov = 0.093;
                        minFov = 0.093;
                        maxFov = 0.093;
                    };
                    class Medium: Wide {
                        initAngleY = 0;
                        initFov = 0.7;
                        minFov = 0.7;
                        maxFov = 0.7;
                        gunnerOpticsModel = "\WW2\Assets_m\Vehicles\Optics_m\IF_Optika_61k.p3d";
                    };

                    class Narrow: Medium {
                        gunnerOpticsModel = "\WW2\Assets_m\Vehicles\Optics_m\IF_Optika_61k.p3d";
                        initFov = 0.155;
                        minFov = 0.155;
                        maxFov = 0.155;
                    };
                };
            };
        };
    };
};
