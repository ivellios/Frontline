#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(Recoil) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

#define MC	0.0089
#define RSD	0.06031
#define RCT	-0.0028

/*class CfgRecoils
{
//	recoil_single_m1a1[] = {0.04,0.00625,0.007,0.01,0.0125,0,0.045,0,-0.0035};
//	recoil_single_prone_m1a1[] = {0.035,0.003125,0.0049,0.01,0.00625,0,0.045,0,-0.00175,0.01,0,-0.00175};
//	LIB_mp40Recoil[] = {0,0,0,0.06,0.01,0.012,0.1,0,-0.02,0.1,-0.01,0.01,0.05,0,0};
//	LIB_mp40RecoilProne[] = {0,0,0,0.06,0.01,0.012,0.1,0,-0.02,0.1,-0.01,0.01,0.05,0,0};
//	LIB_PPSh41Recoil[] = {0,0,0,0.05,0.002,0.005,0.1,-0.01,-0.001,0.05,0,0};
//	LIB_PPSh41RecoilProne[] = {0,0,0,0.05,0.005,0.001,0.1,-0.005,0,0.1,0,0,0.05,0,0};
//	LIB_ppshrec[] = {0,0,0,0.06,0.01,0.012,0.1,0,-0.02,0.1,-0.01,0.01,0.05,0,0};
//	LIB_dp28Recoil[] = {0,0,0,0.05,0.002,0.005,0.1,-0.01,-0.001,0.05,0,0};
//	LIB_dp28RecoilProne[] = {0,0,0,0.05,0.005,0.001,0.1,-0.005,0,0.1,0,0,0.05,0,0};
//	LIB_mg42Recoil[] = {0,0,0,0.05,0.002,0.005,0.1,-0.01,-0.001,0.05,0,0};
//	LIB_mg42RecoilProne[] = {0,0,0,0.05,0.005,0.001,0.1,-0.005,0,0.1,0,0,0.05,0,0};
//	LIB_mgrecoil[] = {0.05,0.02,0.008,0.08,0,0};

//	recoil_riffle[] =			{0,0.018,0.019+MC,		0.043+RSD,0,0,		0.083+RSD,0,-0.0065+RCT,		0.132+RSD,0,0};
//	recoil_riffle_prone[] =			{0,0.017,0.010+MC,		0.043+RSD,0,0,		0.083+RSD,0,-0.0035+RCT,		0.132+RSD,0,0};
//	recoil_riffle2[] =			{0,0.02,0.025+MC,		0.04+RSD,0,0,		0.08+RSD,0,-0.007+RCT,			0.129+RSD,0,0};
//	recoil_riffle2_prone[] =		{0,0.019,0.015+MC,		0.04+RSD,0,0,		0.08+RSD,0,-0.004+RCT,			0.129+RSD,0,0};
//	recoil_single_kurz[] =			{0,0.010,0.015+MC,		0.042+RSD,0,0,		0.073+RSD,0,-0.0060+RCT,		0.127+RSD,0,0};
//	recoil_single_kurz_prone[] =		{0,0.009,0.008+MC,		0.042+RSD,0,0,		0.073+RSD,0,-0.0031+RCT,		0.127+RSD,0,0};
//	recoil_single_m1[] =			{0,0.007,0.017+MC,		0.04+RSD,0,0,		0.071+RSD,0,-0.0054+RCT,		0.127+RSD,0,0};
//	recoil_single_m1_prone[] =		{0,0.006,0.008+MC,		0.039+RSD,0,0,		0.071+RSD,0,-0.0028+RCT,		0.127+RSD,0,0};

//	recoil_1outof3[] = {0.04,"0.006*((3/7))","0.05*.5*((3/7))",0.01,"0.009*((3/7))",0,0.045,0,"-0.025*.5*((3/7))"};
//	recoil_2outof3[] = {0.04,"0.006*((5/7))","0.05*.5*((4/7))",0.03,"0.006*((5/7))","0.05*.5*((5/7))",0.04,"0.006*((5/7))","-0.05*.5*((5/7))",0.01,"0.009*((5/7))",0,0.045,0,"-0.025*.5*((4/7))"};
//	recoil_3outof3[] = {0.04,"0.006*((7/7))","0.05*.5*((7/7))",0.01,"0.009*((7/7))",0,0.045,0,"-0.025*.5*((7/7))"};
//	recoil_prone_1outof3[] = {0.035,"0.004*((4/7))","0.035*.5*((4/7))",0.01,"0.006*((4/7))",0,0.045,0,"-0.025*.25*((4/7))",0.01,0,"-0.025*.25*((4/7))"};
//	recoil_prone_2outof3[] = {0.035,"0.004*((6/7))","0.035*.5*((6/7))",0.01,"0.006*((6/7))",0,0.045,0,"-0.030*.25*((6/7))",0.01,0,"-0.030*.25*((6/7))"};
//	recoil_prone_3outof3[] = {0.035,"0.004*((8/7))","0.035*.5*((8/7))",0.01,"0.006*((8/7))",0,0.045,0,"-0.025*.25*((8/7))",0.01,0,"-0.025*.25*((8/7))"};
//	recoil_9x19_Pistol[] = {0,0.007,0.05,0.005,0.007,0.05,0.09,0,-0.01,0.1,0,0};
//	recoil_762x25_Pistol[] = {0,0.007,0.05,0.005,0.007,0.05,0.09,0,-0.01,0.1,0,0};
//	recoil_45ACP_Pistol[] = {0,0.007,0.06,0.005,0.007,0.06,0.09,0,-0.01,0.1,0,0};
//	recoil_mp40[] = {0,0,0,0.03,"36.943*((0.001))*(0.3)","3.587*((0.004))*(1.5)",0.03,"31.817*((0.001))*(0.5)","1.251*((0.004))*(1.7)",0.03,"19.755*((0.001))*(0.7)","0.764*((0.004))*(1.9)",0.06,"7.388*((0.001))*(0.9)","0.285*((0.004))*(2.1)",0.06,"-2.402*((0.001))*(0.3)","-0.096*((0.004))*(3.5)",0.06,"-3.53*((0.001))*(0.5)","-0.141*((0.004))*(2.5)",0.06,"-3.677*((0.001))*(0.5)","-0.147*((0.004))*(1.5)",0.06,"-3.138*((0.001))*(0.3)","-0.125*((0.004))*(0.5)",0.06,0,0};
//	recoil_prone_mp40[] = {0,0,0,0.03,"36.943*((0.001))*(0.3)","3.587*((0.004))*(0.35)",0.03,"31.817*((0.001))*(0.5)","1.251*((0.004))*(0.55)",0.03,"19.755*((0.001))*(0.7)","0.764*((0.004))*(0.75)",0.06,"7.388*((0.001))*(0.9)","0.285*((0.004))*(0.95)",0.06,"-2.402*((0.001))*(0.3)","-0.096*((0.004))*(1)",0.06,"-3.53*((0.001))*(0.5)","-0.141*((0.004))*(0.5)",0.06,"-3.677*((0.001))*(0.5)","-0.147*((0.004))*(0.25)",0.06,"-3.138*((0.001))*(0.3)","-0.125*((0.004))*(0.15)",0.06,0,0};

    class recoil_default;
    class recoil_mp40_1: recoil_default
    {
        muzzleOuter[] = {0.4,0.7,0.5,0.5};
        muzzleInner[] = {0.2,0.2,0.5,0.7};
        kickBack[] = {0.06,0.085};
        permanent = 0.1;
        temporary = 0.03;
    };
    class recoil_ppshM_1: recoil_default
    {
        muzzleOuter[] = {0.38,0.68,0.47,0.47};
        muzzleInner[] = {0.18,0.18,0.48,0.68};
        kickBack[] = {0.041,0.081};
        permanent = 0.1;
        temporary = 0.03;
    };
    class recoil_ppshD_1: recoil_default
    {
        muzzleOuter[] = {0.37,0.67,0.46,0.46};
        muzzleInner[] = {0.17,0.17,0.47,0.67};
        kickBack[] = {0.04,0.08};
        permanent = 0.1;
        temporary = 0.03;
    };
    class recoil_thompson_1: recoil_default
    {
        muzzleOuter[] = {0.2,0.2,0.3,0.3};
        muzzleInner[] = {0.03,0.03,0.2,0.2};
        kickBack[] = {0.04,0.08};
        permanent = 0.25;
        temporary = 0.02;
    };
    class recoil_rifle_1: recoil_default
    {
        muzzleOuter[] = {0.7,4,1,1};
        kickBack[] = {0.06,0.08};
        temporary = 0.01;
    };
    // class recoil_rifle_s_1: recoil_default
    // {
        // muzzleOuter[] = {0.7,4,0.3,0.9};
        // muzzleInner[] = {0.03,0.03,0.2,0.2};
        // kickBack[] = {0.06,0.08};
        // temporary = 0.02;
    // };
    // ^ is too low
    class recoil_rifle_s_1: recoil_default
    {
        muzzleOuter[] = {0.7,4,1.25,1.25};
        muzzleInner[] = {0.03,0.03,0.2,0.2};
        kickBack[] = {0.06,0.08};
        permanent = 0.2;
        temporary = 0.02;
    };
    class recoil_rifle_m1_1: recoil_default
    {
        muzzleOuter[] = {0.6,2.3,0.7,0.7};
        kickBack[] = {0.02,0.05};
        temporary = 0.04;
    };
    class recoil_rifle_kurtz_1: recoil_default
    {
        muzzleOuter[] = {1.19,1.59,0.24,0.54};//muzzleOuter[] = {0.39,0.99,0.54,0.54};
        muzzleInner[] = {0,0,0.21,0.61};
        kickBack[] = {0.18,0.2};
        permanent = 0.08;
        temporary = 0.011;
    };
    class recoil_mg_1: recoil_default
    {
        muzzleOuter[] = {0.3,0.9,0.5,0.5};
        muzzleInner[] = {0,0,0.11,0.51};
        kickBack[] = {0.023,0.04};
        permanent = 0.12;//permanent = 0.08;
        temporary = 0.011;
    };
    class recoil_mg_2: recoil_default
    {
        muzzleOuter[] = {0.3,1.2,0.3,0.5};//muzzleOuter[] = {0.3,0.9,0.5,0.5};
        muzzleInner[] = {0,0,0.11,0.51};
        kickBack[] = {0.043,0.08};//kickBack[] = {0.023,0.04};
        permanent = 0.3;//permanent = 0.08;
        temporary = 0.011;
    };
};
class cfgMagazines{
    class CA_LauncherMagazine;
    class LIB_1Rnd_60mm_M6: CA_LauncherMagazine
    {
        mass = 30;
    };
    class LIB_1Rnd_RPzB: CA_LauncherMagazine
    {
        mass = 35.09;
    };
    class LIB_1Rnd_PzFaust_30m: CA_LauncherMagazine
    {
        mass = 14.37;
    };
};*/


/*class cfgWeapons
{
    class Mode_SemiAuto{
    multiplier = 1;
    burst = 1;
    burstRangeMax = -1;
    dispersion = 0.0002;
    sound[] = {"",10,1};
    soundBegin[] = {"sound",1};
    soundBeginWater[] = {"sound",1};
    soundClosure[] = {"sound",1};
    soundEnd[] = {};
    soundLoop[] = {};
    soundContinuous = 0;
    weaponSoundEffect = "";
    reloadTime = 0.1;
    ffCount = 1;
    ffMagnitude = 0.5;
    ffFrequency = 11;
    flash = "gunfire";
    flashSize = 0.1;
    autoFire = 0;
    useAction = 0;
    useActionTitle = "";
    showToPlayer = 1;
    minRange = 30;
    minRangeProbab = 0.25;
    midRange = 300;
    midRangeProbab = 0.58;
    maxRange = 600;
    maxRangeProbab = 0.04;
    artilleryDispersion = 1;
    artilleryCharge = 1;
    canShootInWater = 0;
    sounds[] = {"StandardSound","SilencedSound"};
    displayName = "Semi";
    textureType = "semi";
    recoil = "recoil_single_primary_3outof10";
    recoilProne = "recoil_single_primary_prone_3outof10";
    aiDispersionCoefY = 1.7;
    aiDispersionCoefX = 1.4;
    soundBurst = 0;
    requiredOpticType = -1;
    aiRateOfFire = 2;
    aiRateOfFireDispersion = 1;
    aiRateOfFireDistance = 500;
    };
    class Mode_FullAuto: Mode_SemiAuto{
    dispersion = 0.0005;
    sound[] = {"",10,1};
    soundEnd[] = {"sound",1};
    soundContinuous = 0;
    reloadTime = 0.08;
    autoFire = 1;
    minRange = 1;
    minRangeProbab = 0.2;
    midRange = 30;
    midRangeProbab = 0.58;
    maxRange = 80;
    maxRangeProbab = 0.04;
    displayName = "Full";
    textureType = "fullAuto";
    recoil = "recoil_auto_primary_3outof10";
    recoilProne = "recoil_auto_primary_prone_3outof10";
    aiDispersionCoefY = 3;
    aiDispersionCoefX = 2;
    soundBurst = 0;
    };
    class Pistol_Base_F;
    class Rifle_Base_F;
    class Rifle_Long_Base_F;
    class Rifle_Short_Base_F;
    class Launcher_Base_F;

    class LIB_PISTOL: Pistol_Base_F
    {
    };
    class LIB_P38: LIB_PISTOL
    {
        recoil = "recoil_pistol_p07";
    };
    class LIB_P08: LIB_P38
    {
    };
    class LIB_M1908: LIB_P08
    {
    };
    class LIB_TT33: LIB_PISTOL
    {
        recoil = "recoil_pistol_rook40";
    };
    class LIB_M1895: LIB_PISTOL
    {
        recoil = "recoil_pistol_p07";
    };
    class LIB_Colt_M1911: LIB_PISTOL
    {
        recoil = "recoil_pistol_p07";
    };
    class LIB_FLARE_PISTOL: LIB_PISTOL
    {
        recoil = "recoil_pistol_p07";
    };

    class LIB_SMG: Rifle_Short_Base_F
    {
    };
    class LIB_MP40: LIB_SMG
    {
        recoil = "recoil_mp40_1";
    };
    class LIB_PPSh41_m: LIB_SMG
    {
        recoil = "recoil_ppshM_1";
    };
    class LIB_PPSh41_d: LIB_PPSh41_m
    {
        recoil = "recoil_ppshD_1";
    };
    class LIB_M1A1_Thompson: LIB_SMG
    {
        discreteDistanceInitIndex = 1;
        recoil = "recoil_thompson_1";
    };

    class LIB_RIFLE: Rifle_Base_F
    {
    };
    class LIB_K98: LIB_RIFLE
    {
        recoil = "recoil_rifle_1";
    };
    class LIB_G3340: LIB_K98
    {
    };
    class LIB_M9130: LIB_RIFLE
    {
        recoil = "recoil_rifle_1";
    };
    class LIB_M38: LIB_M9130
    {
    };
    class LIB_M44: LIB_M9130
    {
    };
    class LIB_M1903A3_Springfield: LIB_RIFLE
    {
        recoil = "recoil_rifle_1";
    };
    class LIB_MP44: LIB_RIFLE
    {
        recoil = "recoil_rifle_kurtz_1";
    };
    class LIB_SVT_40: LIB_RIFLE
    {
        recoil = "recoil_rifle_s_1";
    };
    class LIB_G43: LIB_RIFLE
    {
        recoil = "recoil_rifle_s_1";
    };
    class LIB_M1_Garand: LIB_RIFLE
    {
        recoil = "recoil_rifle_s_1";
    };
    class LIB_M1_Carbine: LIB_RIFLE
    {
        recoil = "recoil_rifle_m1_1";
    };
    class LIB_PTRD: LIB_RIFLE
    {
        recoil = "recoil_gm6";
        inertia = 5.2;
    };

    class LIB_LMG: Rifle_Long_Base_F
    {
    };
    class LIB_DP28: LIB_LMG
    {
        recoil = "recoil_mg_2";
        inertia = 4.2;
    };
    class LIB_DT: LIB_LMG
    {
        recoil = "recoil_mg_2";
        inertia = 4.2;
    };
    class LIB_DT_OPTIC: LIB_DT
    {
        inertia = 4.2;
    };
    class LIB_MG42: LIB_LMG
    {
        recoil = "recoil_mg_1";
        inertia = 4.8;
    };
    class LIB_M1918A2_BAR: LIB_LMG
    {
        discreteDistanceInitIndex = 1;
        recoil = "recoil_mg_1";
        inertia = 1.5;
        modes[] = {"Full","Full_Rapid","Far","Medium","Short"};
        class Full: Mode_FullAuto
        {
            displayName = "Full (300)";
            textureType = "fullAuto";
            reloadTime = 0.2;
            dispersion = 0.000875;
            aiDispersionCoefX = 5;
            aiDispersionCoefY = 5;
            aiRateOfFire = 0.001;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.2;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.05;
            sounds[] = {"StandardSound"};

        };
        class Full_Rapid: Full
        {
            textureType = "burst";
            displayName = "Full (650)";
            reloadTime = 0.12;
        };
        class Far: Full
        {
            textureType = "semi";
            showToPlayer = 0;
            displayName = "Semi";
            autoFire = 0;
            aiRateOfFireDistance = 500;
            aiRateOfFire = 5;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.5;
            maxRange = 500;
            maxRangeProbab = 0.04;
        };
        class Medium: Full
        {
            showToPlayer = 0;
            burst = 3;
            ffCount = 3;
            autoFire = 0;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 300;
            minRange = 100;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.5;
            maxRange = 300;
            maxRangeProbab = 0.04;
        };
    };

    class LIB_SRIFLE: Rifle_Long_Base_F
    {
    };
    class LIB_K98ZF39: LIB_SRIFLE
    {
        recoil = "recoil_rifle_1";
    };
    class LIB_M9130PU: LIB_SRIFLE
    {
        recoil = "recoil_rifle_1";
    };
    class LIB_M1903A4_Springfield: LIB_SRIFLE
    {
        recoil = "recoil_rifle_1";
    };

    class LIB_LAUNCHER: Launcher_Base_F
    {
    };
    class LIB_PzFaust_30m: LIB_LAUNCHER
    {
        recoil = "recoil_single_law";
    };
    class LIB_RPzB: LIB_LAUNCHER
    {
        recoil = "recoil_single_law";
    };
    class LIB_M1A1_Bazooka: LIB_LAUNCHER
    {
        recoil = "recoil_single_law";
    };
    class FF_lmg_BAR: Rifle_Base_F {
        recoil = "recoil_mg_1";
        inertia = 1.5;
    };
    class FF_rifle_Carbine: Rifle_Base_F {
        recoil = "recoil_rifle_m1_1";
    };
    class FF_srifle_G41: Rifle_Base_F {
        recoil = "recoil_rifle_s_1";
    };
    class FF_srifle_Garand: Rifle_Base_F {
        recoil = "recoil_rifle_s_1";
    };
    class FF_rifle_K98K: Rifle_Base_F {
        recoil = "recoil_rifle_1";
    };
    class FF_smg_M3: Rifle_Base_F {
        recoil = "recoil_thompson_1";
    };
    class FF_lmg_M1919A6: Rifle_Base_F {
        inertia = 4.8;
    };
    class FF_sgun_M198712G: Rifle_Base_F {
        recoil = "recoil_rifle_s_1";
    };
    class FF_lmg_MG34 : Rifle_Base_F {
        recoil = "recoil_mg_1";
        inertia = 4.8;
    };
    class FF_lmg_MG42: Rifle_Base_F {
        recoil = "recoil_mg_1";
        inertia = 4.8;
    };
    class FF_smg_MP40: Rifle_Base_F {
        recoil = "recoil_mp40_1";
    };
    class FF_rifle_Springfield: Rifle_Base_F {
        recoil = "recoil_rifle_1";
    };
    class FF_smg_STG44: Rifle_Base_F {
        recoil = "recoil_rifle_kurtz_1";
    };
    class FF_smg_M1A1_Thompson: Rifle_Base_F {
        recoil = "recoil_thompson_1";
    };
    class FF_smg_M1_Thompson: Rifle_Base_F {
        recoil = "recoil_thompson_1";
    };
};*/

/*class CfgAmmo {
    class BulletBase;
    class LIB_Bullet_base: BulletBase {};
    class LIB_B_762x63_Ball: LIB_Bullet_base
    {
        class HitEffects
            {
                hitVirtual = "ImpactMetalSabotBig";
                hitMetal="ImpactMetalSabotSmall50";
                hitMetalPlate="ImpactMetalSabotSmall50";
                hitBuilding="BCImpactConcrete_50cal";
                hitConcrete="BCImpactConcrete_50cal";
                hitGroundSoft="BCImpactDirt_50cal";
                hitGroundHard="BCImpactDirt_50cal";
                default_mat="BCImpactDirt_50cal";
                Hit_Foliage_green = "BCImpactDirt_50cal";
                Hit_Foliage_Dead = "BCImpactDirt_50cal";
                Hit_Foliage_Green_big = "BCImpactDirt_50cal";
                Hit_Foliage_Palm = "BCImpactDirt_50cal";
                Hit_Foliage_Pine = "BCImpactDirt_50cal";
        };
    };
};
