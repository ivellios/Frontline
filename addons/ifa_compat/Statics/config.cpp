#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(Canons) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class CfgAmmo {
    class Default;

    class ShellCore: Default{};
    class ShellBase: ShellCore{};

    class BulletCore: Default{};
    class BulletBase: BulletCore{};

    class LIB_Bullet_AA_base: BulletBase {};
    class LIB_4x_SprGr_FlaK_38: LIB_Bullet_AA_base {
        LIB_FLAK_ExplosionTypeAmmo = "";
    };
};

class CfgMagazines {
    class LIB_HE_VehicleMagazine_base;
    class LIB_AP_VehicleMagazine_base;

    class LIB_SprGr34_KWK40_HE;
    class LIB_50x_SprGr34_Pak40_HE: LIB_SprGr34_KWK40_HE {
        count = 25;
    };
    class LIB_PzGr39_KWK40_AP;
    class LIB_40x_PzGr39_Pak40_AP: LIB_PzGr39_KWK40_AP{
        count = 35;
    };

    class LIB_50x_OF350_ZiS3_HE: LIB_HE_VehicleMagazine_base {
        count = 25;
    };
    class LIB_40x_BR350B_ZiS3_AP: LIB_AP_VehicleMagazine_base {
        count = 35;
    };
};
class Mode_SemiAuto {};
class CfgWeapons {
	class CannonCore {};
	class LIB_MortarCannon_base: CannonCore {};
	class LIB_M2_60: LIB_MortarCannon_base {
		class Single1: Mode_SemiAuto
			{
				artilleryCharge = 0.35;
			};
			class Single2: Single1
			{
				artilleryCharge = 0.38;
			};
			class Single3: Single1
			{
				artilleryCharge = 0.42;
			};
	};
	class LIB_BM37: LIB_MortarCannon_base
		{
			class Single1: Mode_SemiAuto
			{
				artilleryCharge = 0.28;
			};
			class Single2: Single1
			{
				artilleryCharge = 0.32;
			};
			class Single3: Single1
			{
				artilleryCharge = 0.39;
			};
		};
	class LIB_GRWR34: LIB_MortarCannon_base
		{
			class Single1: Mode_SemiAuto
			{
				artilleryCharge = 0.32;
			};
			class Single2: Single1
			{
				artilleryCharge = 0.36;
			};
			class Single3: Single1
			{
				artilleryCharge = 0.41;
			};
		};
};

class CfgVehicles {
    class All {};
    class AllVehicles: All {
        class NewTurret {};
    };
    class Land: AllVehicles {};
    class LandVehicle: Land {};
    class StaticWeapon: LandVehicle {
        class Turrets {
            class MainTurret: NewTurret {};
        };
    };
    class StaticMGWeapon: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {};
        };
    };
    class StaticMortar: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {};
        };
    };
    class StaticCanon: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {};
        };
    };
    class LIB_StaticMGWeapon_base: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {};
        };
    };
    class LIB_StaticCanon_base: StaticCanon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {};
        };
    };
    class fow_w_mortar_base: StaticMortar {
        class Turrets: Turrets {
            class MainTurret: MainTurret {};
        };
    };
    class fow_w_vickers: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                magazines[] = {"fow_250Rnd_vickers","fow_250Rnd_vickers","fow_250Rnd_vickers","fow_250Rnd_vickers"};
            };
        };
    };
    class fow_w_type92_tripod: StaticMGWeapon {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                magazines[] = {
                    "fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92",
                    "fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92",
                    "fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92",
                    "fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92",
                    "fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92",
                    "fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92","fow_30Rnd_77x58_Type92"
                };
            };
        };
    };

    class fow_w_type97_mortar_ija: fow_w_mortar_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                magazines[] = {"fow_8Rnd_81mm_type97_HE"};
            };
        };
    };
    class LIB_Flakvierling_38_base: LIB_StaticCanon_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                maxHorizontalRotSpeed = 1.1;
                maxVerticalRotSpeed = 0.65;
            };
        };
    };
    class LIB_FlaK_38_base: LIB_StaticCanon_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                maxHorizontalRotSpeed = 1.1;
                maxVerticalRotSpeed = 0.65;
            };
        };
    };
    class LIB_61k_base: LIB_StaticCanon_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                maxHorizontalRotSpeed = 1.1;
                maxVerticalRotSpeed = 0.65;
            };
        };
    };

    class LIB_MG34_Lafette: LIB_StaticMGWeapon_base {};
    class LIB_MG34_Lafette_Deployed: LIB_MG34_Lafette {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                magazines[] = {"LIB_250Rnd_792x57","LIB_250Rnd_792x57","LIB_250Rnd_792x57","LIB_250Rnd_792x57"};
            };
        };
    };
    class LIB_MG42_Lafette: LIB_StaticMGWeapon_base {};
    class LIB_MG42_Lafette_Deployed: LIB_MG42_Lafette {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                magazines[] = {"LIB_250Rnd_792x57","LIB_250Rnd_792x57","LIB_250Rnd_792x57","LIB_250Rnd_792x57"};
            };
        };
    };
    class LIB_MG42_Lafette_trench: LIB_MG42_Lafette{};
    class LIB_MG42_Lafette_low: LIB_MG42_Lafette_trench{};
    class LIB_MG42_Lafette_low_Deployed: LIB_MG42_Lafette_low{
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                magazines[] = {"LIB_250Rnd_792x57","LIB_250Rnd_792x57","LIB_250Rnd_792x57","LIB_250Rnd_792x57"};
            };
        };
    };

};
