#include "..\macros.hpp"

class CfgPatches {
	class ADDON {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        requiredAddons[] = {QUOTE(ADDON)};
	};
};
#define KH_GRASS_CUT_COEF 0.55

class CfgWorlds {

    class DefaultClutter;
    class DefaultWorld;
    class CAWorld;
    class LIBWorld;
    class I44World;
    class Staszow;
    

	class Colleville: Staszow {
        
			class Clutter
			{
				class LIB_StasoGrassBase: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 0.8 * KH_GRASS_CUT_COEF;
				};
				class LIB_GrassTall2: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				};
				class LIB_FernTall: DefaultClutter
				{
					scaleMin = 0.75 * KH_GRASS_CUT_COEF;
					scaleMax = 1.3 * KH_GRASS_CUT_COEF;
				};
				class LIB_GrassAutumnBrown: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				};
				class LIB_France_Greenweat: DefaultClutter
				{
					scaleMin = 0.5 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
				class LIB_France_Caluna: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
			};
        };
/*
	class fow_map_Henderson: CAWorld {
        
			class Clutter {
				class c_GrassBunch_HI: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.3 * KH_GRASS_CUT_COEF;
				};
				class c_GrassTropic: DefaultClutter
				{
					scaleMin = 0.65 * KH_GRASS_CUT_COEF;
					scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				};
				class c_forest_fern: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				};
				class StrGrassGreenGroup: DefaultClutter
				{
					scaleMin = 0.7 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
				class pinanga01: DefaultClutter
				{
					scaleMin = 0.6 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
				class pinanga03: DefaultClutter
				{
					scaleMin = 0.6 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
				class young_tree03: DefaultClutter
				{
					scaleMin = 0.6 * KH_GRASS_CUT_COEF;
					scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				};
		};
    };
*/
	class ruha: CAWorld {
			class Clutter
			{
				class ruha_sammal1_Clutter: DefaultClutter
				{
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
				class ruha_pelto6_Clutter: DefaultClutter
				{
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1.7 * KH_GRASS_CUT_COEF;
				};
				class ruha_pelto7_Clutter: DefaultClutter
				{
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				};
				class ruha_vaalea_heina: DefaultClutter
				{
					scaleMin = 0.9 * KH_WHEAT_CUT_COEF;
					scaleMax = 1.1 * KH_WHEAT_CUT_COEF;
				};
				class ruha_WeedDead: DefaultClutter
				{
					scaleMin = 0.75 * KH_GRASS_CUT_COEF;
					scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				};
				class ruha_GrassDesert: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.8 * KH_GRASS_CUT_COEF;
				};
				class ruha_vaalea_ruohoa: DefaultClutter
				{
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.8 * KH_GRASS_CUT_COEF;
				};
			};
		};
    };