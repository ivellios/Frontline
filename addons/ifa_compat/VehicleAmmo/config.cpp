#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(VehicleAmmo) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class CfgAmmo {
    class Default {};
    class BombCore;
    class RocketBase;
    class ShellCore;
    class SubmunitionBase;
        class LIB_R_150mm_WGr41: SubmunitionBase {  //waffel
        hit = 230;
        indirectHit = 120; //125
        indirectHitRange = 14; //19
    };
    class LIB_R_BM13: RocketBase {  //katyusha

        hit = 240;
        indirectHit = 60; //100
        indirectHitRange = 7; //30
    };

    // -- Lib bombs
    class LIB_Bomb_base: BombCore
    {
        CraterEffects = "BombCrater";
        explosionEffects = "BombExplosion";
        simulation = "shotGrenade";
        soundFly[] = {"",3.16,1,1000};
    };
    class LIB_SC50_Bomb: LIB_Bomb_base
    {
        CraterEffects = "80mm_Smoke";
        explosionEffects = "155mm_Explode";
        explosionTime = 2;
    };
    class LIB_US_500lb_Bomb: LIB_Bomb_base
    {
        CraterEffects = "BombCrater";
        explosionEffects = "MK82_Explode";
        explosionTime = 5;
    };
    class LIB_FAB250_Bomb: LIB_Bomb_base
    {
        CraterEffects = "BombCrater";
        explosionTime = 3;
    };
    class LIB_SC250_Bomb: LIB_FAB250_Bomb
    {
        CraterEffects = "BombCrater";
        explosionTime = 3;
    };
    class LIB_FAB500_Bomb: LIB_Bomb_base
    {
        CraterEffects = "BombCrater";
        explosionEffects = "MK82_Explode";
        explosionTime = 5;
    };
    class LIB_SC500_Bomb: LIB_FAB500_Bomb
    {
        CraterEffects = "BombCrater";
        explosionTime = 5;
    };

    // -- FRL fuse replacements

    class FRL_FUSED_SC50: LIB_SC50_Bomb {
        CraterEffects = "";
        craterWaterEffects = "";
        explosionEffects = "";
        explosionSoundEffect = "";
        indirectHit = 0;
        indirectHitRange = 0;
        multiSoundHit[] = {};
        simulation = "shotMissile";
        soundFly[] = {"\WW2\Assets_s\Weapons\Misc_s\Mortars_Bm37\Bm_37_SoundFly.wss",3.16,1,1000};
        SoundSetExplosion[] = {};

        class eventHandlers {
            fired = "( ['LIB_SC50_Bomb'] + _this) call FRL_Compat_fnc_bombFuses";
        };
    };
    class FRL_FUSED_SC250: LIB_SC250_Bomb {
        CraterEffects = "";
        craterWaterEffects = "";
        explosionEffects = "";
        explosionSoundEffect = "";
        indirectHit = 0;
        indirectHitRange = 0;
        simulation = "shotMissile";
        soundFly[] = {"\WW2\Assets_s\Weapons\Misc_s\Mortars_Bm37\Bm_37_SoundFly.wss",3.16,1,1000};
        SoundSetExplosion[] = {};

        class eventHandlers {
            fired = "( ['LIB_SC250_Bomb'] + _this) call FRL_Compat_fnc_bombFuses";
        };
    };
    class FRL_FUSED_SC500: LIB_SC500_Bomb {
        CraterEffects = "";
        craterWaterEffects = "";
        explosionEffects = "";
        explosionSoundEffect = "";
        indirectHit = 0;
        indirectHitRange = 0;
        simulation = "shotMissile";
        soundFly[] = {"\WW2\Assets_s\Weapons\Misc_s\Mortars_Bm37\Bm_37_SoundFly.wss",3.16,1,1000};
        SoundSetExplosion[] = {};

        class eventHandlers {
            fired = "( ['LIB_SC500_Bomb'] + _this) call FRL_Compat_fnc_bombFuses";
        };
    };
    class FRL_FUSED_500lb: LIB_US_500lb_Bomb {
        CraterEffects = "";
        craterWaterEffects = "";
        explosionEffects = "";
        explosionSoundEffect = "";
        indirectHit = 0;
        indirectHitRange = 0;
        simulation = "shotMissile";
        soundFly[] = {"\WW2\Assets_s\Weapons\Misc_s\Mortars_Bm37\Bm_37_SoundFly.wss",3.16,1,1000};
        SoundSetExplosion[] = {};

        class eventHandlers {
            fired = "( ['LIB_US_500lb_Bomb'] + _this) call FRL_Compat_fnc_bombFuses";
        };
    };
    class FRL_FUSED_FAB250: LIB_FAB250_Bomb {
        CraterEffects = "";
        craterWaterEffects = "";
        explosionEffects = "";
        explosionSoundEffect = "";
        indirectHit = 0;
        indirectHitRange = 0;
        simulation = "shotMissile";
        soundFly[] = {"\WW2\Assets_s\Weapons\Misc_s\Mortars_Bm37\Bm_37_SoundFly.wss",3.16,1,1000};
        SoundSetExplosion[] = {};

        class eventHandlers {
            fired = "( ['LIB_FAB250_Bomb'] + _this) call FRL_Compat_fnc_bombFuses";
        };
    };
    class FRL_FUSED_FAB500: LIB_FAB500_Bomb {
        CraterEffects = "";
        craterWaterEffects = "";
        explosionEffects = "";
        explosionSoundEffect = "";
        indirectHit = 0;
        indirectHitRange = 0;
        simulation = "shotMissile";
        soundFly[] = {"\WW2\Assets_s\Weapons\Misc_s\Mortars_Bm37\Bm_37_SoundFly.wss",3.16,1,1000};
        SoundSetExplosion[] = {};

        class eventHandlers {
            fired = "( ['LIB_FAB500_Bomb'] + _this) call FRL_Compat_fnc_bombFuses";
        };
    };

    // -- Shells
    class ShellBase: ShellCore {};
    // -- FOW 37mm
    class fow_Sh_37mm_APHE: ShellBase { //Type 95 Ha-Go 37mm gun
        hit = 130; //110
        tracerEndTime = 3;
        tracerScale = 2;
        tracerStartTime = 0.1;
    };
    class fow_Sh_37mm_m6: ShellBase {
        tracerEndTime = 3;
        tracerScale = 2;
        tracerStartTime = 0.1;
    };
    class fow_Sh_37mm_M74_AP: fow_Sh_37mm_m6 {
        tracerEndTime = 3;
        tracerScale = 2;
        tracerStartTime = 0.1;
    };
    class fow_Sh_37mm_M51_APC: fow_Sh_37mm_m6 {
        tracerEndTime = 3;
        tracerScale = 2;
        tracerStartTime = 0.1;
    };
    // -- FOW 57mm
    class fow_Sh_57mm_HE: ShellBase {
        tracerEndTime = 3;
        tracerScale = 2.25;
        tracerStartTime = 0.1;
    };
    class fow_Sh_57mm_AP: fow_Sh_57mm_HE {

    };
    class fow_Sh_57mm_APCBC: fow_Sh_57mm_HE {

    };
    class fow_Sh_57mm_APCR: fow_Sh_57mm_HE {

    };
    class fow_Sh_57mm_APDS: fow_Sh_57mm_HE {

    };
    // -- FOW 75mm
    class fow_Sh_75mm_m3: ShellBase {
        tracerEndTime = 3;
        tracerScale = 3;
        tracerStartTime = 0.1;
    };
    class fow_Sh_75mm_M61_APCBC: fow_Sh_75mm_m3 {
        tracerEndTime = 3;
        tracerScale = 3;
        tracerStartTime = 0.1;
    };
    class fow_Sh_75mm_M72_AP: fow_Sh_75mm_m3 {
        tracerEndTime = 3;
        tracerScale = 3;
        tracerStartTime = 0.1;
    };
    class fow_Sh_75mm_sprg42_HE: ShellBase {
        tracerEndTime = 3;
        tracerScale = 3;
        tracerStartTime = 0.1;
    };
    class fow_Sh_75mm_pzgr3942_APCBC_HE: fow_Sh_75mm_sprg42_HE {

    };
    class fow_Sh_75mm_pzgr4042_APCR: fow_Sh_75mm_sprg42_HE {

    };
    // -- FOW 88mm
    class fow_Sh_88mm_HE: ShellBase {
        tracerEndTime = 3;
        tracerScale = 3.5;
        tracerStartTime = 0.1;
    };
    class fow_Sh_88mm_AP: fow_Sh_88mm_HE {

    };
    // 75mm m61 caliber = "(88/((15*619)/1000))";
    // T45 APCR used in Jumbos and sherman II used by UK and M4A2s
    // caliber = "(95/((15*1045)/1000))";
};

class cfgMagazines {
    class Default;
    class CA_Magazine;
    class VehicleMagazine: CA_Magazine {};
    class fow_10Rnd_57mm_APCBC: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_10Rnd_57mm_APCR: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_10Rnd_57mm_APDS: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_20Rnd_20mm_AP: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_20Rnd_57mm_AP: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_27Rnd_37mm_M51_APC: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_27Rnd_75mm_M72_AP: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_30Rnd_37mm_M74_AP: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_30Rnd_75mm_M61_APCBC: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_30Rnd_75mm_pzgr3942_APCBC_HE: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_30Rnd_75mm_pzgr4042_APCR: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_30Rnd_88mm_AP: VehicleMagazine {
        tracersEvery = 1;
    };
    class fow_49Rnd_37mm_APHE: VehicleMagazine {
        tracersEvery = 1;
    };
    class LIB_Bomb_VehicleMagazine_base: VehicleMagazine {};
    class LIB_1Rnd_SC250: LIB_Bomb_VehicleMagazine_base {
        ammo = "FRL_FUSED_SC250";
    };
    class LIB_1Rnd_SC50: LIB_Bomb_VehicleMagazine_base {
        ammo = "FRL_FUSED_SC50";
    };
    class LIB_1Rnd_SC500: LIB_Bomb_VehicleMagazine_base {
        ammo = "FRL_FUSED_SC500";
    };
    class LIB_1Rnd_FAB250: LIB_Bomb_VehicleMagazine_base {
        ammo = "FRL_FUSED_FAB250";
    };
    class LIB_1Rnd_FAB500: LIB_Bomb_VehicleMagazine_base {
        ammo = "FRL_FUSED_FAB500";
    };
    class LIB_1Rnd_US_500lb: LIB_Bomb_VehicleMagazine_base {
        ammo = "FRL_FUSED_500lb";
    };
};
