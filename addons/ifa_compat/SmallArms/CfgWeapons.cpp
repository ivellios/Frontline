#include "..\macros.hpp"

class Mode_SemiAuto {};
class Mode_FullAuto : Mode_SemiAuto {};

class CfgWeapons{
    class Default;
    class LauncherCore: Default {};
    class Launcher: LauncherCore {};
    class Launcher_Base_F: Launcher {};
    class fow_w_m1a1_bazooka: Launcher_Base_F
    {
        maxZeroing = 100;
    };

    // -- vests

    class ItemCore;
    class InventoryItem_Base_F;
    class VestItem;

    class Vest_Camo_Base : ItemCore {
        class ItemInfo: VestItem {};
    };


    class fow_v_base : Vest_Camo_Base {
        class ItemInfo: ItemInfo {
            class HitpointsProtectionInfo {
                class Chest {
                    HitpointName = "HitChest";
                    armor = 6;
                    PassThrough = 0.75;
                };
                class Abdomen {
                    hitpointName = "HitAbdomen";
                    armor = 6;
                    PassThrough = 0.75;
                };
                class Diaphragm {
                    HitpointName = "HitDiaphragm";
                    armor = 4;
                    PassThrough = 0.75;
                };
                /*
                class Body
                {
                    hitpointName = "HitBody";
                    PassThrough = 0.7;
                };
                */
            };
        };
    };
    class V_LIB_Vest_Camo_Base : Vest_Camo_Base {
        class ItemInfo: ItemInfo {
            class HitpointsProtectionInfo {
                class Chest {
                    HitpointName = "HitChest";
                    armor = 6;
                    PassThrough = 0.75;
                };
                class Abdomen {
                    hitpointName = "HitAbdomen";
                    armor = 6;
                    PassThrough = 0.75;
                };
                class Diaphragm {
                    HitpointName = "HitDiaphragm";
                    armor = 4;
                    PassThrough = 0.75;
                };
                /*
                class Body
                {
                    hitpointName = "HitBody";
                    PassThrough = 0.7;
                };
                */
            };
        };
    };

    class V_LIB_SOV_IShBrVestMG : V_LIB_Vest_Camo_Base {
        class ItemInfo: ItemInfo {
            class HitpointsProtectionInfo {

                class Chest {
                    HitpointName = "HitChest";
                    armor = 6;
                    PassThrough = 0.25;
                };
                class Abdomen {
                    hitpointName = "HitAbdomen";
                    armor = 6;
                    PassThrough = 0.25;
                };
                class Diaphragm {
                    HitpointName = "HitDiaphragm";
                    armor = 4;
                    PassThrough = 0.25;
                };

                /*
                class Body {
                    armor = 5 + 3;
                    hitpointName = "HitBody";
                    PassThrough = 0.7;
                };
                */

            };
        };
    };

    class V_LIB_SOV_IShBrVestPPShMag : V_LIB_SOV_IShBrVestMG {};
    class V_LIB_SOV_IShBrVestPPShDisc : V_LIB_SOV_IShBrVestMG {};

    // -- weapons

    class GrenadeLauncher: Default {};
    class Throw: GrenadeLauncher {
			muzzles[] = {"HandGrenade_Stone", "HandGrenadeMuzzle", "MiniGrenadeMuzzle", "SmokeShellMuzzle", "SmokeShellYellowMuzzle", "SmokeShellGreenMuzzle", "SmokeShellRedMuzzle", "SmokeShellPurpleMuzzle", "SmokeShellOrangeMuzzle", "SmokeShellBlueMuzzle", "ChemlightGreenMuzzle", "ChemlightRedMuzzle", "ChemlightYellowMuzzle", "ChemlightBlueMuzzle", "IRGrenade", "LIB_SmokeShellMuzzle", "LIB_Shg24Muzzle", "LIB_Shg24x7Muzzle", "LIB_F1Muzzle", "LIB_Rg42Muzzle", "LIB_M39Muzzle", "LIB_US_Mk_2Muzzle", "LIB_Rpg6Muzzle", "LIB_PwmMuzzle", "LIB_MillsBombMuzzle", "LIB_No82Muzzle", "LIB_stickyNadeMuzzle", "LIB_MolotovMuzzle"};
        class ThrowMuzzle: GrenadeLauncher {};
        class HandGrenadeMuzzle: ThrowMuzzle {};
        class LIB_stickyNadeMuzzle: HandGrenadeMuzzle
        {
            author = "Frontline";
            magazines[] = {"LIB_StickyNade_Mag"};
            displayName = "Sticky Grenade";
            minRange = 15;
            minRangeProbab = 0.5;
            midRange = 25;
            midRangeProbab = 0.8;
            maxRange = 40;
            maxRangeProbab = 0.3;
        };
        class LIB_MolotovMuzzle: HandGrenadeMuzzle
        {
            author = "Frontline";
            magazines[] = {"LIB_Molotov_Mag"};
            displayName = "Molotov";
            minRange = 15;
            minRangeProbab = 0.5;
            midRange = 25;
            midRangeProbab = 0.8;
            maxRange = 40;
            maxRangeProbab = 0.3;
        };
    };
    // -- defining baseclasses
    class PistolCore: Default {};
    class Pistol: PistolCore {};
    class Pistol_Base_F: Pistol {};
    class RifleCore: Default {};
    class Rifle: RifleCore {};
    class Rifle_Base_F: Rifle {
        dexterity = 1.9;
        intertia = 0.7;
    };
    class Rifle_Long_Base_F: Rifle_Base_F {
        recoil = "recoil_LMG";
        inertia = 2.9;
        dexterity = 1;
        FRLRecoilResetTime[] = {0.175,0.15,0.135,0.125};
        FRLRecoilShakePwr = 2.5;
        FRLRecoilMax[] = {1.4,1.35,1.3,1.3};
        RLRecoilTimeconstant[] = {6,9,11,15};
        FRLRecoilDrift[] = {0.2, 0.3}; //reserved for MG or mby prone
    };
    class Rifle_Short_Base_F: Rifle_Base_F {};

    // -- LIB baseclasses
    class LIB_PISTOL: Pistol_Base_F {
        inertia = 1.15;
        maxRecoilSway = 0.02;
        //swayDecaySpeed = 1.25;
        dexterity = 1.5;
        recoil = "recoil_pistol_acpc2";
    };
    class LIB_RIFLE: Rifle_Base_F {
        recoil = "recoil_SemiautoRifle";
        FRLRecoilShakePwr = 10;
        maxRecoilSway = 0.02;
    };
    class LIB_LAUNCHER: Launcher_Base_F {};
    class LIB_LMG: Rifle_Long_Base_F {
        recoil = "recoil_LMG";
        inertia = 2.9;
        dexterity = 1;
        maxRecoilSway = 0.05;
    };
    class LIB_SMG: Rifle_Short_Base_F {
        recoil = "recoil_SMG";
        FRLRecoilShakePwr = 1.5;
        FRLRecoilDrift[] = {0.1, 0.15};
        inertia = 0.5;
        dexterity = 2.5;
        maxRecoilSway = 0.015;
    };
    // -- FOW baseclasses
    class fow_rifle_base: Rifle_Base_F {
        recoil = "recoil_SemiautoRifle";
        FRLRecoilMax[] = {1.6,1.5,1.425,1.4};
        RLRecoilTimeconstant[] = {2,4,5,5.5};
        FRLRecoilShakePwr = 12;
        FRLRecoilDrift[] = {0.2, 0.3}; //reserved for MG or mby prone
        maxRecoilSway = 0.02;
    };
    class fow_rifleBolt_base: fow_rifle_base{};
    class fow_shotgun_base: Rifle_Base_F {
        recoil = "recoil_shotgun";
        FRLRecoilShakePwr = 6;
        maxRecoilSway = 0.03;
    };
    class fow_smg_base: fow_rifle_base {
        recoil = "recoil_SMG";
        inertia = 0.5;
        dexterity = 2.5;
        FRLRecoilMax[] = {1.4,1.35,1.35,1.35};
        FRLRecoilResetTime[] = {0.2,0.175,0.15,0.125};
        FRLRecoilShakePwr = 2.5;
        FRLRecoilDrift[] = {0.1, 0.15}; //reserved for MG or mby prone
        maxRecoilSway = 0.015;
    };
    class fow_lmg_base: fow_rifle_base {
        recoil = "recoil_LMG";
        inertia = 2.9;
        dexterity = 1;
        FRLRecoilMax[] = {1.55,1.5,1.35,1.3};
        FRLRecoilResetTime[] = {0.175,0.15,0.135,0.125};
        FRLRecoilShakePwr = 2.5;
        RLRecoilTimeconstant[] = {6,9,11,15};
        FRLRecoilDrift[] = {0.15, 0.2}; //reserved for MG or mby prone
        maxRecoilSway = 0.05;
        swayDecaySpeed = 1.25;  // -- vanilla GM6
    };
    // -- end of baseclasses

    // -- FOW Boltguns
    class fow_w_leeenfield_no4mk1: fow_rifleBolt_base {
        discreteDistance[] = {300,600};
        discreteDistanceInitIndex = 0;      // -- 300m
    };

    // -- LIB LMGs
    class LIB_Bren_Mk2: LIB_LMG {
        // okay why is the bren 6x as inaccurate as the MG42 with dispersion of 0.006???
        class Single: Mode_SemiAuto {
            dispersion = 0.001;
        };
        class Full: Mode_FullAuto{
            dispersion = 0.001;
        };
        inertia = 2; // like our old one and inline with the other AR we have
    };
    class LIB_DT: LIB_LMG {
        recoil = "recoil_LMG";
    };
    class LIB_MG34: LIB_LMG {
        recoil = "recoil_LMG";
    };
    class LIB_MG42: LIB_LMG {
        recoil = "recoil_LMG";
    };
    class LIB_M1919A4: LIB_LMG {
        recoil = "recoil_LMG";
    };
    class LIB_M1918A2_BAR: LIB_LMG {
        FRLRecoilMax[] = {1.55,1.5,1.45,1.4};
        RLRecoilTimeconstant[] = {2,4,5,5.5};
        recoil = "recoil_AR";
        inertia = 2;
        dexterity = 1.3;
    };
    // -- FOW-LMGs
    class fow_w_m1918a2: fow_lmg_base {
        FRLRecoilMax[] = {1.55,1.5,1.45,1.4};
        RLRecoilTimeconstant[] = {2,4,5,5.5};
        recoil = "recoil_AR";
        inertia = 2;
        dexterity = 1.3;
    };
    class fow_w_m1919: fow_lmg_base {       // has fucked up zeroing
        recoil = "recoil_LMG";
    };
    class fow_w_bren: fow_lmg_base {
        FRLRecoilMax[] = {1.55,1.5,1.45,1.4};
        RLRecoilTimeconstant[] = {2,4,5,5.5};
        dexterity = 1.4;
        inertia = 2;
        recoil = "recoil_AR";
    };
    class fow_w_mg42: fow_lmg_base {
        recoil = "recoil_LMG";
    };
    class fow_w_mg34: fow_lmg_base {
        recoil = "recoil_LMG";
    };
    class fow_w_type99_lmg: fow_lmg_base {
        class FullAuto: Mode_FullAuto {
            dispersion = 0.001;      // -- LIB-MG42
        };
        FRLRecoilMax[] = {1.55,1.5,1.45,1.4};
        RLRecoilTimeconstant[] = {2,4,5,5.5};
        dexterity = 1.3;
        inertia = 2.5;
        recoil = "recoil_AR";
    };
    // -- Lib Pistols
    class LIB_M1895: LIB_PISTOL {
        recoil = "recoil_pistol_acpc2";
    };
    class LIB_M1896: LIB_PISTOL {
        recoil = "recoil_pistol_p07";
    };
    class LIB_P38: LIB_PISTOL {
        recoil = "recoil_pistol_p07";
    };
    class LIB_P08: LIB_P38 {
        recoil = "recoil_pistol_p07";
    };
    class LIB_TT33: LIB_PISTOL {
        recoil = "recoil_pistol_acpc2";
    };
    class LIB_WaltherPPK: LIB_PISTOL {
        recoil = "recoil_pistol_p07";
    };
    class LIB_Colt_M1911: LIB_PISTOL {
        recoil = "recoil_pistol_acpc2";
    };
    // -- LIB SMGs
    class LIB_M3_GreaseGun: LIB_SMG {
        recoil = "recoil_SMG";
        FRLRecoilDrift[] = {0.1, 0.05};
        FRLRecoilMin = 0.9;
    };
    class LIB_M1A1_Thompson: LIB_SMG {
        dexterity = 2;
        recoil = "recoil_SMG";
        FRLRecoilDrift[] = {0, 0};
        FRLRecoilMin = 0.8;
    };
    class LIB_MP40: LIB_SMG {
        FRLRecoilMax[] = {1.3,1.325,1.3,1.3};
        recoil = "recoil_SMG";
    };
    class LIB_PPSh41_m: LIB_SMG {
        dexterity = 2;
        recoil = "recoil_SMG";
        FRLRecoilDrift[] = {0, 0};
        FRLRecoilMin = 0.8;
    };
    class LIB_Sten_Mk2: LIB_SMG {
        FRLRecoilMax[] = {1.3,1.325,1.3,1.3};
    };
    class LIB_Sten_Mk5: LIB_Sten_Mk2 {
        FRLRecoilMax[] = {1.3,1.325,1.3,1.3};
    };
    // -- PDWs
    class LIB_M1_Carbine: LIB_RIFLE {
        // recoil = "recoil_SMG";
        inertia = 0.4;
        FRLRecoilMin = 2;
        FRLRecoilShakePwr = 2.5;
        FRLRecoilMax[] = {1.65,1.4,1.35,1.25};
        FRLRecoilTimeconstant[] = {3.5,5,6,9};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
        class Single: Mode_SemiAuto {
            dispersion = 0.0021;
        };
    };

    // -- misc
    class LIB_PIAT: LIB_LAUNCHER {
        recoil = "recoil_ptrd";
    };
    // -- FOW SMG
    class fow_w_m3: fow_smg_base {
        modes[] = {"Full","Far","Medium","Short"};
        recoil = "recoil_SMG";
        inertia = 0.4;
        FRLRecoilDrift[] = {0.1, 0.05};
        FRLRecoilMin = 0.9;
    };
    class fow_w_mp40: fow_smg_base {
        recoil = "recoil_SMG";
        inertia = 0.4;
        discreteDistance[] = {100, 200};    // like IFA and forgotten weapons pics show
        discreteDistanceInitIndex = 0;      // -- 100m
    };
    class fow_w_type100: fow_smg_base {
        recoil = "recoil_SMG";
        inertia = 0.4;
        FRLRecoilDrift[] = {0.1, 0.15};
    };
    class fow_w_sten_mk2: fow_smg_base {
        recoil = "recoil_SMG";
        inertia = 0.4;
    };
    class fow_w_m1a1_thompson: fow_smg_base {
        dexterity = 2;
        recoil = "recoil_SMG";
        FRLRecoilDrift[] = {0, 0};
        FRLRecoilMin = 0.8;
    };
    class fow_w_m1_thompson: fow_w_m1a1_thompson {
        dexterity = 2;
        recoil = "recoil_SMG";
        FRLRecoilDrift[] = {0, 0};
    };
    class fow_w_m55_reising: fow_smg_base {
        recoil = "recoil_SMG";
    };

    // -- Autorifles
    class LIB_M1_Garand: LIB_RIFLE{
        FRLRecoilShakePwr = 8;
        recoil = "recoil_SemiautoRifle";
    };
    class LIB_G43: LIB_RIFLE{
        FRLRecoilShakePwr = 8;
        recoil = "recoil_SemiautoRifle";
    };
    class LIB_SVT40: LIB_RIFLE{
        FRLRecoilShakePwr = 8;
        recoil = "recoil_SemiautoRifle";
    };

    // -- misc
    class LIB_M2_Flamethrower: LIB_RIFLE{
        FRLRecoilShakePwr = 1;
    };

    // -- FOW Autorifles
    class fow_w_fg42: fow_rifle_base {
        recoil = "recoil_LMG";
        FRLRecoilMin = 1;
        FRLRecoilMax[] = {1.55,1.45,1.425,1.4};
        RLRecoilTimeconstant[] = {2,4,5,5.5};
        FRLRecoilResetTime[] = {0.175,0.15,0.135,0.125};
        FRLRecoilShakePwr = 6;
        FRLRecoilDrift[] = {0.2, 0.3}; //reserved for MG or mby prone
        maxRecoilSway = 0.02;
    };
    class fow_w_g43: fow_rifle_base {
        FRLRecoilShakePwr = 8;
        recoil = "recoil_SemiautoRifle";
    };
    class fow_w_m1_garand: fow_rifle_base {
        FRLRecoilShakePwr = 8;
        recoil = "recoil_SemiautoRifle";
        discreteDistance[] = {100,200,300,400,500};
        discreteDistanceInitIndex = 1;  // -- 200m like IFA used to be 300m
    };

    // -- FOW Pistols
    class fow_w_webley: Pistol_Base_F {
        recoil = "recoil_pistol_acpc2";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };
    class fow_w_welrod_mkii: Pistol_Base_F {
        recoil = "recoil_pistol_p07";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };
    class fow_w_p08: Pistol_Base_F {
        recoil = "recoil_pistol_p07";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };
    class fow_w_p35: Pistol_Base_F {
        recoil = "recoil_pistol_acpc2";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };
    class fow_w_p640p: Pistol_Base_F {
        recoil = "recoil_pistol_acpc2";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };
    class fow_w_pHiPower: fow_w_p640p {
        displayName = "[UK] Browning HP";
    };
    class fow_w_ppk: Pistol_Base_F {
        recoil = "recoil_pistol_p07";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };
    class fow_w_m1911: Pistol_Base_F {
        recoil = "recoil_pistol_acpc2";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };
    class fow_w_type14: Pistol_Base_F {
        recoil = "recoil_pistol_p07";
        inertia = 1.15;
        maxRecoilSway = 0.02;
        dexterity = 1.5;
    };

    // -- PDWs :-)
    class fow_w_m1_carbine: fow_smg_base {
        recoil = "recoil_SMG";
        inertia = 0.4;
        FRLRecoilMin = 2;
        FRLRecoilShakePwr = 2.5;
        FRLRecoilMax[] = {1.65,1.4,1.35,1.25};
        FRLRecoilTimeconstant[] = {3.5,5,6,9};
        FRLRecoilResetTime[] = {0.3,0.25,0.2,0.18};
    };
    // -- shotguns
    class fow_w_ithaca37: fow_shotgun_base{
        recoil = "recoil_shotgun";
        discreteDistance[] = {50,100};      // -- unspecified before resulting in 300m zeroying with pellets o.O?>
        discreteDistanceInitIndex = 0;      // -- 50m
    };
    // -- unicorns
    class fow_w_stg44: fow_rifle_base {
        class FullAuto: Mode_FullAuto {
            dispersion = 0.0008;        // -- Lib_mp44 levels
        };
        class Single: Mode_SemiAuto {
            dispersion = 0.0008;
        };
        FRLRecoilMax[] = {1.575,1.5,1.45,1.44};
        FRLRecoilShakePwr = 4;
        recoil = "recoil_STG";
        inertia = 0.65;
        dexterity = 2;
    };
    class LIB_MP44: LIB_RIFLE {
        FRLRecoilMax[] = {1.575,1.5,1.45,1.44};
        FRLRecoilShakePwr = 6;
        recoil = "recoil_STG";
        inertia = 0.65;
        dexterity = 2;
        discreteDistanceInitIndex = 1;  // -- 200m
        class Single: Mode_SemiAuto {
            dispersion = 0.0021;        // copied from fullauto was dispersion = 0.0045;
        };
    };


    class LIB_K98: LIB_RIFLE
    {
        class Mode_SemiAuto;
        class Single: Mode_SemiAuto
        {
            multiplier = 1;
        burst = 1;
        burstRangeMax = -1;
        sound[] = {"",10,1};
        soundBegin[] = {"sound",1};
        soundBeginWater[] = {"sound",1};
        soundClosure[] = {"sound",1};
        soundEnd[] = {};
        soundLoop[] = {};
        soundContinuous = 0;
        weaponSoundEffect = "";
        ffCount = 1;
        ffMagnitude = 0.5;
        ffFrequency = 11;
        flash = "gunfire";
        flashSize = 0.1;
        autoFire = 0;
        useAction = 0;
        useActionTitle = "";
        showToPlayer = 1;
        artilleryDispersion = 1;
        artilleryCharge = 1;
        canShootInWater = 0;
        displayName = "Semi";
        textureType = "semi";
        recoil = "recoil_single_primary_3outof10";
        recoilProne = "recoil_single_primary_prone_3outof10";
        aiDispersionCoefY = 1.7;
        aiDispersionCoefX = 1.4;
        soundBurst = 0;
        requiredOpticType = -1;
        aiRateOfFireDispersion = 1;
            reloadTime = 0.9;
            dispersion = 0.0008;
            aiRateOfFire = 0.001;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.2;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.05;
            sounds[] = {"StandardSound"};
        };
        /*class Eventhandlers
        {
            class LIB_BoltAction
            {
                fired = "[_this select 0,_this select 1,_this select 2] call FRL_fnc_handleBoltAction;";
            };
            class LIB_RifleGrenades
            {
                fired = "if (local (_this select 0)) then {_this call LIB_System_fnc_rifleGrenadeFire};";
            };
        };*/
    };
    class LIB_LeeEnfield_No4: LIB_RIFLE {

    };

    class LIB_M1903A3_Springfield: LIB_RIFLE
    {
        class Mode_SemiAuto;
        class Single: Mode_SemiAuto
        {
            multiplier = 1;
        burst = 1;
        burstRangeMax = -1;
        sound[] = {"",10,1};
        soundBegin[] = {"sound",1};
        soundBeginWater[] = {"sound",1};
        soundClosure[] = {"sound",1};
        soundEnd[] = {};
        soundLoop[] = {};
        soundContinuous = 0;
        weaponSoundEffect = "";
        ffCount = 1;
        ffMagnitude = 0.5;
        ffFrequency = 11;
        flash = "gunfire";
        flashSize = 0.1;
        autoFire = 0;
        useAction = 0;
        useActionTitle = "";
        showToPlayer = 1;
        artilleryDispersion = 1;
        artilleryCharge = 1;
        canShootInWater = 0;
        displayName = "Semi";
        textureType = "semi";
        recoil = "recoil_single_primary_3outof10";
        recoilProne = "recoil_single_primary_prone_3outof10";
        aiDispersionCoefY = 1.7;
        aiDispersionCoefX = 1.4;
        soundBurst = 0;
        requiredOpticType = -1;
        aiRateOfFireDispersion = 1;
            reloadTime = 0.9;
            dispersion = 0.0008;
            aiRateOfFire = 0.001;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.2;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.05;
            sounds[] = {"StandardSound"};
        };
    };
    class LIB_SRIFLE: Rifle_Long_Base_F{};
    class LIB_K98ZF39: LIB_SRIFLE
    {
        class Mode_SemiAuto;
        class Single: Mode_SemiAuto
        {
            multiplier = 1;
        burst = 1;
        burstRangeMax = -1;
        sound[] = {"",10,1};
        soundBegin[] = {"sound",1};
        soundBeginWater[] = {"sound",1};
        soundClosure[] = {"sound",1};
        soundEnd[] = {};
        soundLoop[] = {};
        soundContinuous = 0;
        weaponSoundEffect = "";
        ffCount = 1;
        ffMagnitude = 0.5;
        ffFrequency = 11;
        flash = "gunfire";
        flashSize = 0.1;
        autoFire = 0;
        useAction = 0;
        useActionTitle = "";
        showToPlayer = 1;
        artilleryDispersion = 1;
        artilleryCharge = 1;
        canShootInWater = 0;
        displayName = "Semi";
        textureType = "semi";
        recoil = "recoil_single_primary_3outof10";
        recoilProne = "recoil_single_primary_prone_3outof10";
        aiDispersionCoefY = 1.7;
        aiDispersionCoefX = 1.4;
        soundBurst = 0;
        requiredOpticType = -1;
        aiRateOfFireDispersion = 1;
            reloadTime = 0.9;
            dispersion = 0.0007;
            aiRateOfFire = 0.001;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.2;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.05;
            sounds[] = {"StandardSound"};
        };
    };
    class LIB_M1903A4_Springfield: LIB_SRIFLE
    {
        class Mode_SemiAuto;
        class Single: Mode_SemiAuto
        {
            multiplier = 1;
        burst = 1;
        burstRangeMax = -1;
        sound[] = {"",10,1};
        soundBegin[] = {"sound",1};
        soundBeginWater[] = {"sound",1};
        soundClosure[] = {"sound",1};
        soundEnd[] = {};
        soundLoop[] = {};
        soundContinuous = 0;
        weaponSoundEffect = "";
        ffCount = 1;
        ffMagnitude = 0.5;
        ffFrequency = 11;
        flash = "gunfire";
        flashSize = 0.1;
        autoFire = 0;
        useAction = 0;
        useActionTitle = "";
        showToPlayer = 1;
        artilleryDispersion = 1;
        artilleryCharge = 1;
        canShootInWater = 0;
        displayName = "Semi";
        textureType = "semi";
        recoil = "recoil_single_primary_3outof10";
        recoilProne = "recoil_single_primary_prone_3outof10";
        aiDispersionCoefY = 1.7;
        aiDispersionCoefX = 1.4;
        soundBurst = 0;
        requiredOpticType = -1;
        aiRateOfFireDispersion = 1;
            reloadTime = 0.9;
            dispersion = 0.0007;
            aiRateOfFire = 0.001;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.2;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.05;
            sounds[] = {"StandardSound"};
        };
    };
    class LIB_M9130: LIB_RIFLE
    {
        class Mode_SemiAuto;
        class Single: Mode_SemiAuto
        {
            multiplier = 1;
        burst = 1;
        burstRangeMax = -1;
        sound[] = {"",10,1};
        soundBegin[] = {"sound",1};
        soundBeginWater[] = {"sound",1};
        soundClosure[] = {"sound",1};
        soundEnd[] = {};
        soundLoop[] = {};
        soundContinuous = 0;
        weaponSoundEffect = "";
        ffCount = 1;
        ffMagnitude = 0.5;
        ffFrequency = 11;
        flash = "gunfire";
        flashSize = 0.1;
        autoFire = 0;
        useAction = 0;
        useActionTitle = "";
        showToPlayer = 1;
        artilleryDispersion = 1;
        artilleryCharge = 1;
        canShootInWater = 0;
        displayName = "Semi";
        textureType = "semi";
        recoil = "recoil_single_primary_3outof10";
        recoilProne = "recoil_single_primary_prone_3outof10";
        aiDispersionCoefY = 1.7;
        aiDispersionCoefX = 1.4;
        soundBurst = 0;
        requiredOpticType = -1;
        aiRateOfFireDispersion = 1;
            reloadTime = 0.9;
            dispersion = 0.0008;
            aiRateOfFire = 0.001;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.2;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.05;
            sounds[] = {"StandardSound","SilencedSound"};
        };
    };

    class LIB_M9130PU: LIB_SRIFLE
    {
        class Mode_SemiAuto;
        class Single: Mode_SemiAuto
        {
            multiplier = 1;
        burst = 1;
        burstRangeMax = -1;
        sound[] = {"",10,1};
        soundBegin[] = {"sound",1};
        soundBeginWater[] = {"sound",1};
        soundClosure[] = {"sound",1};
        soundEnd[] = {};
        soundLoop[] = {};
        soundContinuous = 0;
        weaponSoundEffect = "";
        ffCount = 1;
        ffMagnitude = 0.5;
        ffFrequency = 11;
        flash = "gunfire";
        flashSize = 0.1;
        autoFire = 0;
        useAction = 0;
        useActionTitle = "";
        showToPlayer = 1;
        artilleryDispersion = 1;
        artilleryCharge = 1;
        canShootInWater = 0;
        displayName = "Semi";
        textureType = "semi";
        recoil = "recoil_single_primary_3outof10";
        recoilProne = "recoil_single_primary_prone_3outof10";
        aiDispersionCoefY = 1.7;
        aiDispersionCoefX = 1.4;
        soundBurst = 0;
        requiredOpticType = -1;
        aiRateOfFireDispersion = 1;
            reloadTime = 0.9;
            dispersion = 0.0007;
            aiRateOfFire = 0.001;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.2;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.05;
            sounds[] = {"StandardSound","SilencedSound"};
        };
    };

    class LIB_PTRD: LIB_RIFLE {
        flashSize = 0.5;
        FRLRecoilShakePwr = 22;
        fireLightDuration = 0.1;
        fireLightIntensity = 0.3;
        maxRecoilSway = 1;
        swayDecaySpeed = 0.1;
        dexterity = 0.4;
        inertia = 10;
        dispersion = 0.00029; // used to be 0.00029;
        /*
        class WeaponSlotsInfo {
            mass = 500; // from 375 which is correct, we just need to make the guy carrying feel it
        };
        */
    };
	class fow_w_piat: fow_rifle_base {
        FRLRecoilShakePwr = 22;
		inertia = 4; // 1.5
        recoil = "recoil_ptrd";
        class WeaponSlotsInfo {
            mass = 330; //100
        };
	};
    // Lib rifle grenade launchers
    class UGL_F: GrenadeLauncher {};
    class LIB_RifleGrenade_Muzzle: UGL_F {
        FRLRecoilShakePwr = 25;
        recoil = "recoil_rifle_GL";
    };

    /*
    class LIB_K98_GW: LIB_K98 {
        class LIB_K98_RG: LIB_RifleGrenade_Muzzle {};
    };
    class LIB_LeeEnfield_No4_CUP: LIB_LeeEnfield_No4 {};
    class LIB_M1_Garand_M7: LIB_M1_Garand {};
    */
};
