#include "..\macros.hpp"

class CfgMagazines{
    class Default;
    class CA_Magazine: Default {};
    class CA_LauncherMagazine: CA_Magazine{};


	
    class fow_1Rnd_m6a1: CA_LauncherMagazine //bazooka rocket
    {
        mass = 82.17; //25 // increased so that ammo carrier couldn't just give his backpack and go on his way
    };
	class LIB_1Rnd_RPzB: CA_LauncherMagazine
	{
		mass = 82.09; // 72.09
	};
	
	class 1Rnd_HE_Grenade_shell: CA_Magazine{};
	class fow_1Rnd_piat_HE: 1Rnd_HE_Grenade_shell
	{
		mass = 82; //25
	};
	
	
    class HandGrenade: CA_Magazine {};
    class HandGrenade_West: HandGrenade {};
    class LIB_StickyNade_Mag: HandGrenade_West
    {
        author = "Frontline";
        displayName = "Sticky Grenade (AT)";
        displayNameShort = "Stickynade (AT)";
        descriptionShort = "Explosives Set to stick and boom";
        ammo = "LIB_StickyNade";
        model = "\WW2\Assets_m\Weapons\Mines_m\Inv\IF_Ladung_Inv.p3d";
        picture = "\WW2\Assets_t\Weapons\Equipment_t\Mines\Gear_Ladung_Wire_ca.paa";
        initSpeed = 15;
        mass = 30.42;
    };
    class LIB_Molotov_Mag: HandGrenade_West
    {
        author = "Frontline";
        displayName = "Molotov";
        displayNameShort = "Molotov";
        descriptionShort = "A juicy cocktail of flaming death";
        ammo = "LIB_MolotovNade";
        model = "\WW2\Assets_m\Weapons\Mines_m\Inv\IF_Ladung_Inv.p3d";
        picture = "\WW2\Assets_t\Weapons\Equipment_t\Mines\Gear_Ladung_Wire_ca.paa";
        initSpeed = 15;
        mass = 30.42;
    };
    class LIB_M2_Flamethrower_Mag: CA_Magazine
    {
        //count = 200;
        initSpeed = 58;
    };

    class VehicleMagazine: CA_Magazine{};
    class LIB_8Rnd_82mmHE_BM37: VehicleMagazine{};
    class LIB_1rnd_82mmHE_BM37: LIB_8Rnd_82mmHE_BM37
    {
        mass = 14;
    };
    class LIB_8Rnd_81mmHE_GRWR34: LIB_8Rnd_82mmHE_BM37{};
    class LIB_1rnd_81mmHE_GRWR34: LIB_8Rnd_81mmHE_GRWR34
    {
        mass = 13;
    };
    class LIB_8Rnd_60mmHE_M2: LIB_8Rnd_82mmHE_BM37{};
    class LIB_1rnd_60mmHE_M2: LIB_8Rnd_60mmHE_M2
    {
        mass = 9;
    };
	
	class LIB_4000Rnd_M2_P47: VehicleMagazine
	{
		count = 1700; //3400
	};

        class LIB_7Rnd_762x38: CA_Magazine //nagant// changed to use new cartridge
        {
            ammo = "LIB_B_762x38_Ball";
        };
        class fow_6Rnd_455: CA_Magazine //webley// changed to use IF 45 caliber
        {
            ammo = "LIB_B_45ACP_Ball"; //B_45ACP_Ball
        };
};
