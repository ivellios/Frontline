/*
	Bolt action animations handler

	i:
		_unit - unit which fired
		_weapon - weapon which was fired
		_muzzle - muzzle whcih was fired
		_anim - bolt action animation
	o:
		---

	a: reyhard
*/
params["_unit","_weapon","_muzzle"];

// exit for non local units
if (!(local _unit)) exitWith {};

// exit if it was last round
if ((_unit ammo _weapon) isEqualTo 0) exitWith {};

private _playAction =
{
	private _soundName = getText (configFile/"cfgWeapons"/_weapon/"LIB_boltActionSound");
	private _sound = getArray (configFile/"CfgSounds"/_soundName/"sound");
	private _anim = getText (configFile/"cfgWeapons"/_weapon/"LIB_boltActionAnim");

	playSound3D [_sound select 0,_this,false,ATLToASL (_this modelToWorldVisual (_this selectionPosition "leftHand")),_sound select 1,_sound select 2,_sound select 3];

	// workaround for strange reload timing for AI & remote controled units
	if (!(isPlayer _this) || (missionNamespace getVariable ["bis_fnc_moduleRemoteControl_unit",objNull] == player)) then
	{
		[_this,_anim] spawn
		{
			params["_u","_anim"];

			sleep 0.22;//sleep 0.22;

			_u playAction _anim;
		};
	}
	else
	{
		_this playAction _anim;
	};
};

// action for players
if (isPlayer (_unit getVariable ["bis_fnc_moduleremotecontrol_owner",_unit])) then
{
	["LIB_pfh_boltAction","onEachFrame",
	{
		params["_unit","_weapon","_muzzle","_condition","_playAction","_timeOut"];

		if ((currentMuzzle _unit) isEqualTo _muzzle) then
		{
			if ((inputAction "DefaultAction") isEqualTo _condition) then
			{
				if (_condition isEqualTo 0) then
				{
					if ((profileNameSpace getVariable ['LIB_manualBolting',0]) isEqualTo 1) then
					{
						_this set [3,1];
					}
					else
					{
						["LIB_pfh_boltAction","onEachFrame"] call BIS_fnc_removeStackedEventHandler;
						_unit call _playAction;
					};
				}
				else
				{
					["LIB_pfh_boltAction","onEachFrame"] call BIS_fnc_removeStackedEventHandler;
					_unit call _playAction;
				};
			};
			_unit setWeaponReloadingTime [_unit,_muzzle,1];
		}
		else
		{
			// 300 seconds time out handling - if unit doesn't have weapon anymore, in vehicle or dead
			if (time > _timeOut) then
			{
				["LIB_pfh_boltAction","onEachFrame"] call BIS_fnc_removeStackedEventHandler;
			};
		};
	},(_this + [0,_playAction,time + 300])] call BIS_fnc_addStackedEventHandler;
}
else
{
	// action for AI
	_unit call _playAction;
};
