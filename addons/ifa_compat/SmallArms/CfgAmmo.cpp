#include "..\macros.hpp"

#define smgHit 4.05
#define smgPen 1.2
#define pistolHit 4.5
#define rifleHit 10

/*
LIB_10Rnd_762x54 840-820
LIB_10Rnd_792x57 755
LIB_10Rnd_9x19_M1896 425
LIB_20Rnd_762x63 835
LIB_30Rnd_792x33 685

LIB_7Rnd_45ACP 262
LIB_7Rnd_762x38 300
LIB_7Rnd_9x19 365
LIB_35Rnd_762x25 490 --ppsh
LIB_8Rnd_762x25 450 tokarev

fow_30Rnd_77x58 700
fow_8Rnd_8x22 290
fow_10Rnd_303 750
fow_50Rnd_792x57 760
fow_5Rnd_762x63 860


LIB_B_763x25_Ball
typicalSpeed = 440;

LIB_15Rnd_762x33 600

doublecheck LIB_B_45ACP_Ball

*/

class CfgAmmo{
    class Default;

    class ShellCore: Default{};
    class ShellBase: ShellCore{};

    class BulletCore: Default{};
    class BulletBase: BulletCore{};

	class ShotgunCore: Default{};
	class ShotgunBase: ShotgunCore{};

    class ammo_Penetrator_Base: ShellBase {};

    class RocketCore: Default {};
    class RocketBase: RocketCore {};

    class LIB_Rocket_base: RocketBase {};
    class LIB_Bullet_base: BulletBase {};

	// -- Shotguns
	class B_12Gauge_Pellets: ShotgunBase{};
	class B_12Gauge_Slug: BulletBase{};

	class B_45ACP_Ball: BulletBase { 		 	//thompson, colt 1911, webley
        airFriction = -0.002;
        caliber = smgPen;
        hit = smgHit;	//5
        typicalSpeed = 255;
	};
	class B_9x18_Ball : BulletBase {
		hit = smgHit;	//5
		caliber = smgPen;
        typicalSpeed = 315; // -- from 900 ...
	};
	class fow_B_12Gauge_Slug: B_12Gauge_Slug{
		airFriction = -0.002042;
		caliber = 0.3048;
		hit = 34.51;
	};
	class fow_B_12Gauge_Pellets: B_12Gauge_Pellets{
		 airFriction = -0.00634;
		 caliber = 0.6;
		 hit = smgHit;							// from RHS shotguns
		 initSpeed = 342.9; 					// according to US Combat Shotguns
	};
	class fow_B_45_acp: B_45ACP_Ball {
		airFriction = -0.002;
		caliber = smgPen;
		hit = smgHit;	//5
	};
	class fow_B_455: B_45ACP_Ball {
		airFriction = -0.002;
		caliber = 5;
		hit = 25;      // really needs different damage values since velocity is almost half of regular pistols
        typicalSpeed = 185; // -- down from 285, used to calculate hitpower
	};
	class fow_B_65x52_Ball: BulletBase {		// -- 6.5 Jap
		hit = rifleHit;
		caliber = 1;
	};
    /*
	class fow_B_762x33_Ball: BulletBase { 		//FoW m1 carabine
		airFriction = -0.001513;
		caliber = 0.9;
		hit = smgHit;
        typicalSpeed = 675;
	};
    */
	class fow_B_762x63_Ball: BulletBase {		// -- .30-06
		airFriction = -0.00101;
		caliber = 1.065;
		hit = rifleHit;
        typicalSpeed = 835; // --down from 860
	};
	class fow_B_765x17_Ball: BulletBase {		// -- 32 caliber from welrod and ppk
		hit = smgHit;
		caliber = smgPen;
	};
	class fow_B_77x58_Ball: BulletBase { 		// 7.7jap
		hit = rifleHit;
		caliber = 1.065;						// inline with LIB 7.62*54mmR
	};
	class fow_B_303_Ball: fow_B_77x58_Ball {	// -- .303 and 7.7 very identical cartridges
		hit = rifleHit;
		caliber = 1.065;
	};
	class fow_B_792x33_Ball: BulletBase {		// 8mm kurz copied from LIB eseentially
		airFriction = -0.0018;
		hit = rifleHit;
		caliber = 1;
        typicalSpeed = 675;
	};
	class fow_B_792x57_Ball: BulletBase {		// 8mm
		airFriction = -0.00105;
		hit = rifleHit;
		caliber = 1.105;
	};
	class fow_B_8x22_Ball: BulletBase {			//type99, type14 (pistol)
		airFriction = -0.0022;					// from LIB 7.62*25mm Tokarev
		hit = smgHit;	//6.5
		caliber = smgPen;
        typicalSpeed = 280;     // -- initspeed on fow_8Rnd_8x22 (type14) is 290
	};
	class fow_B_9x19_Ball: B_9x18_Ball {			//type99, type14 (pistol)
		airFriction = -0.0022;					// from LIB 7.62*25mm Tokarev
		hit = smgHit;	//6.5
		caliber = smgPen;
        typicalSpeed = 355; //-- 365 in both LIB and FOW mags, dowm from 380
	};

    // -- pistol ammo, hitting a bit harder than SMGs other

    class LIB_89mm_PIAT: LIB_Rocket_base {
        indirectHitRange = 3;
    };

    class LIB_Molotov_Ammo: BulletBase{
		craterEffects = "";
		soundHit[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hit = 1;
		indirectHit = 0.5;
		indirectHitRange = 0.8;
		cartridge = "";
		cost = 2;
		visibleFire = 32;
		audibleFire = 12;
		visibleFireTime = 20;
		airFriction = 0;
		timetolive = 5;
		typicalSpeed = 30;
		deflecting = 90;
        deflectionSlowDown = 0.01;
		explosive = 0.5;
		fuseDistance = 2;
		caliber = 1.5;
		explosionEffects = "LIB_Moltov_Impact_Flame";
		model = "\A3\Weapons_f\Data\bullettracer\tracer_yellow";
		tracerScale = 1;
		tracerStartTime = 0;
		tracerEndTime = 5;
		soundEmpty[] = {"",1,1,1};
		/*hitGroundSoft[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitGroundHard[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitMan[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitArmor[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitIron[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitBuilding[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitFoliage[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitWood[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitGlass[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitGlassArmored[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitConcrete[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitRubber[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitPlastic[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitDefault[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitMetal[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};
		hitMetalplate[] = {"\a3\sounds_f\sfx\fire1_loop.wss",0.01,1};*/
	};
	class LIB_B_45ACP_Ball: LIB_Bullet_base
	{
		hit = smgHit;
		caliber = smgPen;
        typicalSpeed = 440;
	};
	class LIB_B_762x25_Ball: LIB_Bullet_base //ppsh, tokarev
	{
		hit = smgHit;	//3.2
		caliber = smgPen;
        typicalSpeed = 440;
	};

	class LIB_B_9x18_Ball: LIB_Bullet_base{
		hit = smgHit;
		caliber = smgPen;
        typicalSpeed = 305; // -- not exactly sure what uses 9mm makarov, it but its certainly not sth with 900m/s V0
	};

	class LIB_B_9x19_Ball: LIB_B_9x18_Ball // walther PPK P08
	{
		hit = smgHit;	//3.2
		caliber = smgPen;
        typicalSpeed = 355; // -- down from 440 which is blazing fast for 9mm
	};
	class LIB_B_762x38_Ball: LIB_B_762x25_Ball {	//new cartridge for nagant
        caliber = smgPen;
		hit = smgHit;
        typicalSpeed = 290; // -- down from 500 blin
	};
    class LIB_B_762x33_Ball: LIB_Bullet_base {		//IF m1 carabine, got fucked by FOW
        airFriction = -0.0018;    // IF value
        hit = rifleHit;
		caliber = 1;
        typicalSpeed = 675;
        // typicalSpeed = 675; // -- down from 750 smells like inheritance from 8x57, IF value
    };
    class LIB_B_792x33_Ball: LIB_Bullet_base {
        hit = rifleHit;
        typicalSpeed = 675; // -- down from 710
    };

	class LIB_B_145x144_Ball: LIB_Bullet_base {
		explosionForceCoef = 0;
	    hit = 50; //was at 140 as a reference LiB 12.7*114mm has 35 vanilla 12.7*109mm AP has 60
		caliber = 3.3;	// was 2.4 so ... 3.3 seems to barely pen panthers turret at 50m
	    indirectHit = 0.3;
	    indirectHitRange = 1;
	    soundDefault1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_01", 6.99526, 0.5, 120};
	    soundDefault2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_02", 6.99526, 0.5, 120};
	    soundDefault3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_03", 6.99526, 0.5, 120};
	    soundDefault4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_04", 6.99526, 0.5, 120};
	    soundDefault5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_05", 6.99526, 0.5, 120};
	    soundDefault6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_06", 6.99526, 0.5, 120};
	    soundDefault7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_07", 6.99526, 0.5, 120};
	    soundDefault8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_08", 6.99526, 0.5, 120};
	    soundGroundSoft1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_01", 6.99526, 0.5, 120};
	    soundGroundSoft2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_02", 6.99526, 0.5, 120};
	    soundGroundSoft3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_03", 6.99526, 0.5, 120};
	    soundGroundSoft4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_04", 6.99526, 0.5, 120};
	    soundGroundSoft5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_05", 6.99526, 0.5, 120};
	    soundGroundSoft6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_06", 6.99526, 0.5, 120};
	    soundGroundSoft7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_07", 6.99526, 0.5, 120};
	    soundGroundSoft8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\soft_ground_08", 6.99526, 0.5, 120};
	    soundGroundHard1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_01", 6.41254, 0.5, 160};
	    soundGroundHard2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_02", 6.41254, 0.5, 160};
	    soundGroundHard3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_03", 6.41254, 0.5, 160};
	    soundGroundHard4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_04", 6.41254, 0.5, 160};
	    soundGroundHard5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_05", 6.41254, 0.5, 160};
	    soundGroundHard6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_06", 6.41254, 0.5, 160};
	    soundGroundHard7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_07", 6.41254, 0.5, 160};
	    soundGroundHard8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\hard_ground_08", 6.41254, 0.5, 160};
	    soundGlass1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_01", 6.25893, 0.5, 200};
	    soundGlass2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_02", 6.25893, 0.5, 200};
	    soundGlass3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_03", 6.25893, 0.5, 200};
	    soundGlass4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_04", 6.25893, 0.5, 200};
	    soundGlass5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_05", 6.25893, 0.5, 200};
	    soundGlass6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_06", 6.25893, 0.5, 200};
	    soundGlass7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_07", 6.25893, 0.5, 200};
	    soundGlass8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_08", 6.25893, 0.5, 200};
	    soundGlassArmored1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_01", 6.58489, 0.5, 120};
	    soundGlassArmored2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_02", 6.58489, 0.5, 120};
	    soundGlassArmored3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_03", 6.58489, 0.5, 120};
	    soundGlassArmored4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_04", 6.58489, 0.5, 120};
	    soundGlassArmored5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_05", 6.58489, 0.5, 120};
	    soundGlassArmored6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_06", 6.58489, 0.5, 120};
	    soundGlassArmored7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_07", 6.58489, 0.5, 120};
	    soundGlassArmored8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_arm_08", 6.58489, 0.5, 120};
	    soundMetal1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_01", 6.58489, 0.5, 160};
	    soundMetal2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_02", 6.58489, 0.5, 160};
	    soundMetal3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_03", 6.58489, 0.5, 160};
	    soundMetal4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_04", 6.58489, 0.5, 160};
	    soundMetal5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_05", 6.58489, 0.5, 160};
	    soundMetal6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_06", 6.58489, 0.5, 160};
	    soundMetal7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_07", 6.58489, 0.5, 160};
	    soundMetal8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_08", 6.58489, 0.5, 160};
	    soundVehiclePlate1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_01", 6.58489, 0.5, 160};
	    soundVehiclePlate2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_02", 6.58489, 0.5, 160};
	    soundVehiclePlate3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_03", 6.58489, 0.5, 160};
	    soundVehiclePlate4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_04", 6.58489, 0.5, 160};
	    soundVehiclePlate5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_05", 6.58489, 0.5, 160};
	    soundVehiclePlate6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_06", 6.58489, 0.5, 160};
	    soundVehiclePlate7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_07", 6.58489, 0.5, 160};
	    soundVehiclePlate8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\metal_plate_08", 6.58489, 0.5, 160};
	    soundWood1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_01", 6.99526, 0.5, 120};
	    soundWood2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_02", 6.99526, 0.5, 120};
	    soundWood3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_03", 6.99526, 0.5, 120};
	    soundWood4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_04", 6.99526, 0.5, 120};
	    soundWood5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_05", 6.99526, 0.5, 120};
	    soundWood6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_06", 6.99526, 0.5, 120};
	    soundWood7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_07", 6.99526, 0.5, 120};
	    soundWood8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\wood_08", 6.99526, 0.5, 120};
	    soundHitBody1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_01", 6.58489, 0.5, 10};
	    soundHitBody2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_02", 6.58489, 0.5, 10};
	    soundHitBody3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_03", 6.58489, 0.5, 10};
	    soundHitBody4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_04", 6.58489, 0.5, 10};
	    soundHitBody5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_05", 6.58489, 0.5, 10};
	    soundHitBody6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_06", 6.58489, 0.5, 10};
	    soundHitBody7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_07", 6.58489, 0.5, 10};
	    soundHitBody8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\body_08", 6.58489, 0.5, 10};
	    soundHitBuilding1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_01", 6.23872, 0.5, 130};
	    soundHitBuilding2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_02", 6.23872, 0.5, 130};
	    soundHitBuilding3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_03", 6.23872, 0.5, 130};
	    soundHitBuilding4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_04", 6.23872, 0.5, 130};
	    soundHitBuilding5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_05", 6.23872, 0.5, 130};
	    soundHitBuilding6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_06", 6.23872, 0.5, 130};
	    soundHitBuilding7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_07", 6.23872, 0.5, 130};
	    soundHitBuilding8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\building_08", 6.23872, 0.5, 130};
	    soundHitFoliage1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_01", 3.707946, 0.5, 60};
	    soundHitFoliage2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_02", 3.707946, 0.5, 60};
	    soundHitFoliage3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_03", 3.707946, 0.5, 60};
	    soundHitFoliage4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_04", 3.707946, 0.5, 60};
	    soundHitFoliage5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_05", 3.707946, 0.5, 60};
	    soundHitFoliage6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_06", 3.707946, 0.5, 60};
	    soundHitFoliage7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_07", 3.707946, 0.5, 60};
	    soundHitFoliage8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\foliage_08", 3.707946, 0.5, 60};
	    soundPlastic1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_01", 1, 1, 140};
	    soundPlastic2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_02", 1, 1, 140};
	    soundPlastic3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_03", 1, 1, 140};
	    soundPlastic4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_04", 1, 1, 140};
	    soundPlastic5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_05", 1, 1, 140};
	    soundPlastic6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_06", 1, 1, 140};
	    soundPlastic7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_07", 1, 1, 140};
	    soundPlastic8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\plastic_08", 1, 1, 140};
	    soundConcrete1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_01", 6.41254, 0.5, 140};
	    soundConcrete2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_02", 6.41254, 0.5, 140};
	    soundConcrete3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_03", 6.41254, 0.5, 140};
	    soundConcrete4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_04", 6.41254, 0.5, 140};
	    soundConcrete5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_05", 6.41254, 0.5, 140};
	    soundConcrete6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_06", 6.41254, 0.5, 140};
	    soundConcrete7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_07", 6.41254, 0.5, 140};
	    soundConcrete8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\concrete_08", 6.41254, 0.5, 140};
	    soundRubber1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_01", 3.891251, 1, 50};
	    soundRubber2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_02", 3.891251, 1, 50};
	    soundRubber3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_03", 3.891251, 1, 50};
	    soundRubber4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_04", 3.891251, 1, 50};
	    soundRubber5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_05", 3.891251, 1, 50};
	    soundRubber6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_06", 3.891251, 1, 50};
	    soundRubber7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_07", 3.891251, 1, 50};
	    soundRubber8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\tyre_08", 3.891251, 1, 50};
	    soundWater1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_01", 1, 1, 40};
	    soundWater2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_02", 1, 1, 40};
	    soundWater3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_03", 1, 1, 40};
	    soundWater4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_04", 1, 1, 40};
	    soundWater5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_05", 1, 1, 40};
	    soundWater6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_06", 1, 1, 40};
	    soundWater7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_07", 1, 1, 40};
	    soundWater8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\water_08", 1, 1, 40};
	    soundMetalInt1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_01", 6.58489, 0.5, 160};
	    soundMetalInt2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_02", 6.58489, 0.5, 160};
	    soundMetalInt3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_03", 6.58489, 0.5, 160};
	    soundMetalInt4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_04", 6.58489, 0.5, 160};
	    soundMetalInt5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_05", 6.58489, 0.5, 160};
	    soundMetalInt6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_06", 6.58489, 0.5, 160};
	    soundVehiclePlateInt1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_01", 6.58489, 0.5, 160};
	    soundVehiclePlateInt2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_02", 6.58489, 0.5, 160};
	    soundVehiclePlateInt3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_03", 6.58489, 0.5, 160};
	    soundVehiclePlateInt4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_04", 6.58489, 0.5, 160};
	    soundVehiclePlateInt5[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_05", 6.58489, 0.5, 160};
	    soundVehiclePlateInt6[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_06", 6.58489, 0.5, 160};
	    soundVehiclePlateInt7[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_07", 6.58489, 0.5, 160};
	    soundVehiclePlateInt8[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_08", 6.58489, 0.5, 160};
	    soundVehiclePlateInt9[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_09", 6.58489, 0.5, 160};
	    soundVehiclePlateInt10[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_10", 6.58489, 0.5, 160};
	    soundVehiclePlateInt11[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_11", 6.58489, 0.5, 160};
	    soundVehiclePlateInt12[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_12", 6.58489, 0.5, 160};
	    soundVehiclePlateInt13[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_13", 6.58489, 0.5, 160};
	    soundVehiclePlateInt14[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_14", 6.58489, 0.5, 160};
	    soundVehiclePlateInt15[] = {"A3\Sounds_F\arsenal\sfx\bullet_hitsInt\metal_plate_15", 6.58489, 0.5, 160};

	};
    class LIB_B_Bayonet: BulletBase {
        caliber = 1;
        hit = 15;
    };
	class LIB_M2_Flamethrower_Ammo: BulletBase
		{
            deflecting = 120;
            deflectionSlowDown = 0.6;
            deflectionDirDistribution=0.5;
			hit = 20; //5
			indirectHit = 8; //4
			//indirectHitRange = 1; //0.8
		};

    class Grenade: Default {};
    class GrenadeHand: Grenade {};
    class LIB_GrenadeHand_base: GrenadeHand {};
    class SmokeShell: GrenadeHand {};
    class Chemlight_base: SmokeShell {};
    class Chemlight_green: Chemlight_base
	{
		model = "\A3\Weapons_f\chemlight\chemlight_green_lit";
		effectsSmoke = "ChemlightLight_green";
		typicalspeed = 14;
	};
    class Molotov_Fire: Chemlight_green {
        effectsSmoke = "LIB_Moltov_Impact_Flame";
        explosionTime = 0;
    };
    class LIB_StickyNade: LIB_GrenadeHand_base
	{

		hit = 400;
		indirectHit = 5;
		indirectHitRange = 3.6;
		caliber = "(70/((15*1000)/1000))";
		aiAmmoUsageFlags = "128 + 512";
		visibleFire = 2;
		audibleFire = 0.25;
		visibleFireTime = 5;
		model = "\WW2\Assets_m\Weapons\Mines_m\IF_Ladung.p3d";
		whistleDist = 15;
		CraterEffects = "ATMissileCrater";
		explosionEffects = "ATMissileExplosion";
		deflecting = 0;
		fuseDistance = 0;
		explosionTime = 0;
		explosionForceCoef = 20;
		simulation = "shotShell";
		LIB_caliber = 96;
		LIB_mass = 1.13;
		LIB_massEM = 0.58;
		LIB_kPenetration = 120;
		LIB_initSpeed = 2800;
		LIB_quality = 4;


	};
    class LIB_MolotovNade: LIB_GrenadeHand_base
	{

		hit = 20;
		indirectHit = 3;
		indirectHitRange = 3.6;
		caliber = "(70/((15*1000)/1000))";
		aiAmmoUsageFlags = "128 + 512";
		visibleFire = 2;
		audibleFire = 0.25;
		visibleFireTime = 5;
		model = "\A3\Structures_F_Heli\Items\Food\Ketchup_01_F.p3d";
		whistleDist = 15;
		CraterEffects = "";
		explosionEffects = "MolotovExplosionEffects";
		deflecting = 0;
		fuseDistance = 0;
		explosionTime = 0;
		explosionForceCoef = 20;
		simulation = "shotShell";
		LIB_caliber = 96;
		LIB_mass = 1.13;
		LIB_massEM = 0.58;
		LIB_kPenetration = 120;
		LIB_initSpeed = 2800;
		LIB_quality = 4;
        soundHit1[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_01", 6.25893, 0.96, 200};
		soundHit2[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_02", 6.25893, 0.96, 200};
		soundHit3[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_03", 6.25893, 0.96, 200};
		soundHit4[] = {"A3\Sounds_F\arsenal\sfx\bullet_hits\glass_04", 6.25893, 0.96, 200};
	};

		class GrenadeBase;
		class fow_g_piat_HE: GrenadeBase
		{
			indirectHitRange = 3;
		};

		class LIB_Shell_base: ShellBase {
			class HitEffects
			{
				default_mat = "BCImpactDirt_30mm";
				Hit_Foliage_Dead = "ImpactLeavesDead";
				Hit_Foliage_green = "ImpactLeavesGreen";
				Hit_Foliage_Green_big = "ImpactLeavesGreenBig";
				Hit_Foliage_Palm = "ImpactLeavesPalm";
				Hit_Foliage_Pine = "ImpactLeavesPine";
				hitBuilding = "ImpactConcreteSabot";
				hitConcrete = "ImpactConcreteSabot";
				hitFoliage = "ImpactLeaves";
				hitGlass = "ImpactGlass";
				hitGlassArmored = "ImpactGlassThin";
				hitGroundHard = "ImpactEffectsGroundSabotBig";
				hitGroundSoft = "ImpactEffectsGroundSabotBig";
				hitMan = "ImpactEffectsBlood";
				hitMetal = "ImpactMetalSabotBig";
				hitMetalPlate = "ImpactMetalSabotBig";
				hitPlastic = "ImpactPlastic";
				hitRubber = "ImpactRubber";
				hitWater = "ImpactEffectsWater";
				hitWood = "ImpactWood";
				object = "ImpactConcrete";
				vehicle = "ImpactEffectsGroundSabotBig";
			};
		};
		class LIB_Bullet_Vehicle_base: BulletBase {};
		class LIB_B_127x99_Ball: LIB_Bullet_Vehicle_base {
			class HitEffects
			{
			hitVirtual = "ImpactMetalSabotBig";
			hitMetal="ImpactMetalSabotSmall50";
			hitMetalPlate="ImpactMetalSabotSmall50";
			hitBuilding="BCImpactConcrete_50cal";
			hitConcrete="BCImpactConcrete_50cal";
			hitGroundSoft="BCImpactDirt_50cal";
			hitGroundHard="BCImpactDirt_50cal";
			default_mat="BCImpactDirt_50cal";
			Hit_Foliage_green = "BCImpactDirt_50cal";
			Hit_Foliage_Dead = "BCImpactDirt_50cal";
			Hit_Foliage_Green_big = "BCImpactDirt_50cal";
			Hit_Foliage_Palm = "BCImpactDirt_50cal";
			Hit_Foliage_Pine = "BCImpactDirt_50cal";
			};
		};
};
