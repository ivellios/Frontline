#include "..\macros.hpp"

class CfgRecoils {
    class Default;
    class recoil_default : Default {};
    class recoil_LMG: recoil_default {
        muzzleOuter[] = {0.515, 1.25, 0.55 , 0.66};
        muzzleInner[] = {0, 0, 0, 0};
        kickBack[] = {0.0375, 0.045};
        permanent = 0.225;
        temporary = 0.0125;
    };
    class recoil_AR: recoil_LMG {
        muzzleOuter[] = {0.515, 1.5, 0.333 , 0.66};
        kickBack[] = {0.045, 0.05};
        permanent = 0.225;
        temporary = 0.01;
    };
    class recoil_SemiautoRifle: recoil_default {
        muzzleOuter[] = {0.9, 3.75, 0.8, 0.6};
        //muzzleInner[] = {0.465, 0.55, 0.175, 0.3};
        kickBack[] = {0.07, 0.075};
        permanent = 0.2;
        temporary = 0.02;
    };
    class recoil_SMG: recoil_default {
        muzzleOuter[] = {0.215, 0.7, 0.22 , 0.275};
        kickBack[] = {0.015, 0.0175};
        permanent = 0.33;
        temporary = 0.0125;
    };
    class recoil_pistol_acpc2: recoil_default {
        muzzleInner[] = {0, 0, 0, 0};
        muzzleOuter[] = {0.2,1.8,0.6,0.3};    // -- {0.2,1.5,0.2,0.3}; vanilla
        kickBack[] = {0.08, 0.1};
        permanent = 0.2;
        temporary = 0.02;
    };
    class recoil_pistol_p07: recoil_default {
        muzzleInner[] = {0, 0, 0, 0};
        muzzleOuter[] = {0.2,1.7,0.525,0.3}; //-- {0.2,1.5,0.2,0.3}; vanilla
        kickBack[] = {0.07, 0.09};
        permanent = 0.2;
        temporary = 0.02;
    };

    class recoil_shotgun : recoil_default {
        muzzleOuter[] = {0.5, 2.5, 1.6, 0.9};//muzzleOuter[] = {0.5, 2.5, 0.8, 0.6};
        kickBack[] = {0.08, 0.09};
        permanent = 0.9;
        temporary = 0.002;
    };
    class recoil_rifle_GL: recoil_default {
        muzzleOuter[] = {1, 4.5, 1.6, 0.9};//muzzleOuter[] = {0.5, 2.5, 0.8, 0.6};
        kickBack[] = {0.4, 0.45};
        permanent = 0.5;
        temporary = 0.03;
    };
    // -- FOW recoils override
    class fow_w_mg_recoil: recoil_LMG {};
    class fow_w_smg_recoil: recoil_SMG {};
    class fow_w_rifle_recoil: recoil_SemiautoRifle {};


    class recoil_STG: recoil_default {  // -- yeah gunther you liked that kickback if anything
        // -- using AKM recoil doesnt really work bec ROF is different also IFA STG shoots faster than FOW
        // muzzleOuter[] = {0.3,0.8,0.54,0.54}; "recoil_rifle_kurtz_1"
		muzzleOuter[] = {0.45, 0.875, 0.4, 0.45};
		//muzzleInner[] = {0.465, 0.55, 0.175, 0.3};
		kickBack[] = {0.06, 0.0675};
        // kickBack[] = {0.1,0.1};
		permanent = 0.32;
        //permanent = 0.08;
        //temporary = 0.011;
	};

};
