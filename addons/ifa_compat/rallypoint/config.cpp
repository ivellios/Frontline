#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(Rally) {
        units[] = {"FRL_Rally_Wehrmacht"};
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON), "FRL_Obj_rallypoint"};
    };
};

class CfgVehicles {
    // -- These are made by Audicustoms
    class FRL_Backpacks_Base;
    class FRL_Rally_Wehrmacht: FRL_Backpacks_Base {
        scope = 2;
        author = "Audiocustoms";
        displayName = "Rallypoint (Wehrmacht)";
        model = "\pr\frl\addons\ifa_compat\rallypoint\frl_rally_ger.p3d";
    };
};
