#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(Mortars) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class CfgMagazines {
    class VehicleMagazine;
    class LIB_8Rnd_82mmHE_BM37: VehicleMagazine {
        count = 4;
    };
};

class CfgAmmo {
    class Sh_82mm_AMOS;
    class LIB_Sh_82_HE: Sh_82mm_AMOS {
        CraterEffects = "ArtyShellCrater";
        ExplosionEffects = "MortarExplosion";
    };
    class LIB_Sh_81_HE: LIB_Sh_82_HE {
        CraterEffects = "ArtyShellCrater";
        ExplosionEffects = "MortarExplosion";
    };
    class LIB_Sh_60_HE: LIB_Sh_82_HE
    {
        CraterEffects = "ArtyShellCrater";
        ExplosionEffects = "MortarExplosion";
    };
};
