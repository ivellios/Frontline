// -- This makes sure chat doesn't get spammed with AI messages (No REAL way to turn this off in Arma)

class CfgVoice {
  class EN;
  class LIB_Ger: EN {
    protocol = "FRL_RadioProtocolNoRadio";
  };
  class LIB_SU: EN {
    protocol = "FRL_RadioProtocolNoRadio";
  };
};
