#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(Planes) {
        units[] = {"LIB_US_P39_2", "LIB_P47"};
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class cfgVehicles {
    class LIB_US_Plane_base;
    class LIB_US_P39;

    class LIB_US_P39_2: LIB_US_P39 {
        weapons[] = {};
        magazines[] = {};
    };
/*
    class LIB_P47: LIB_US_Plane_base {
        magazines[] = {"LIB_4000Rnd_M2_P47", "LIB_1Rnd_US_500lb", "LIB_6Rnd_M8_P47"};
    };
*/
};
