#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(ammocrates) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class CfgVehicles {
    class All {};
    class Thing: All {};
    class ThingX: Thing {};
    class ReammoBox_F: ThingX {};
    class LIB_ReammoBox_base: ReammoBox_F {};
    class LIB_AmmoCrate_Arty_SU: LIB_ReammoBox_base {};
    class LIB_4Rnd_RPzB: LIB_ReammoBox_base {};
    // -- end of baseclasses
    class FRL_ATCrate_GER: LIB_4Rnd_RPzB {

        class TransportMagazines {
            class _xx_LIB_5Rnd_RPzB {
                magazine = "LIB_Pwm";
                count = 5;
            };
            class PzFaust60Ammo {
                magazine = "LIB_1Rnd_PzFaust_60m";
                count = 4;
            };
        };
        class TransportWeapons {
            class PzFaust60 {
                weapon = "LIB_PzFaust_60m";
                count = 4;
            };
        };
        class TransportBackpacks {
            class _xx_RocketBackpacks {
                backpack = "B_LIB_GER_SapperBackpack_empty";
                count = 3;
            };
        };
    };
    class FRL_ATCrate_SU: LIB_AmmoCrate_Arty_SU {

        class TransportMagazines {
            class _xx_LIB_5Rnd_RPzB {
                magazine = "LIB_Pwm";
                count = 6;
            };
            class PzFaust60Ammo {
                magazine = "LIB_1Rnd_PzFaust_60m";
                count = 2;
            };
        };
        class TransportWeapons {
            class PzFaust60 {
                weapon = "LIB_PzFaust_60m";
                count = 2;
            };
        };
        class TransportBackpacks {
            class _xx_RocketBackpacks {
                backpack = "B_LIB_SOV_RA_Rucksack_Green";
                count = 3;
            };
        };

    };
    class FRL_ATCrate_US: LIB_AmmoCrate_Arty_SU {
        class TransportMagazines {
            class _xx_LIB_5Rnd_RPzB {
                magazine = "LIB_StickyNade_Mag";
                count = 4;
            };
            class Bazooka_ammo {
                magazine = "LIB_1Rnd_60mm_M6";
                count = 6;
            };
        };
        class TransportWeapons {
            class Bazooka {
                weapon = "LIB_M1A1_Bazooka";
                count = 2;
            };
        };
        class TransportBackpacks {
            class _xx_RocketBackpacks {
                backpack = "B_LIB_US_RocketBag_Empty";
                count = 3;
            };
        };
    };
    class FRL_ATCrate_UK: LIB_AmmoCrate_Arty_SU {
        class TransportMagazines {
            class _xx_LIB_5Rnd_RPzB {
                magazine = "LIB_StickyNade_Mag";
                count = 4;
            };
            class piat_ammo {
                magazine = "LIB_1Rnd_89m_PIAT";
                count = 6;
            };
        };
        class TransportWeapons {
            class Piat {
                weapon = "LIB_PIAT";
                count = 2;
            };
        };
        class TransportBackpacks {
            class _xx_RocketBackpacks {
                backpack = "B_LIB_US_RocketBag_Empty";
                count = 3;
            };
        };
    };
    class FRL_ATCrate_JP: LIB_AmmoCrate_Arty_SU {
        class TransportMagazines {
            class _xx_LIB_5Rnd_RPzB {
                magazine = "LIB_Pwm";
                count = 8;
            };
        };
    };

};

/*
["LIB_BasicAmmunitionBox_US","LIB_ReammoBox_base","ReammoBox_F","ThingX","Thing","All"]
["LIB_AmmoCrate_Arty_SU","LIB_ReammoBox_base","ReammoBox_F","ThingX","Thing","All"]
["LIB_4Rnd_RPzB","LIB_ReammoBox_base","ReammoBox_F","ThingX","Thing","All"]

[["LIB_Molotov_Mag",1],["LIB_StickyNade_Mag",1],
["fow_e_tnt_halfpound",1],["LIB_Rpg6",1],["LIB_Pwm",1]]
[["LIB_Molotov_Mag",1],["LIB_Molotov_Mag",1],["LIB_Molotov_Mag",1],["LIB_Shg24x7",1],["fow_e_no82",1],["LIB_US_TNT_4pound_mag",1]]

"LIB_PTRD"

["LIB_1Rnd_145x114","LIB_1Rnd_145x114","LIB_Ladung_Big_MINE_mag",
"LIB_Ladung_Small_MINE_mag","LIB_Ladung_Big_MINE_mag","LIB_Ladung_Small_MINE_mag",
"LIB_STMI_MINE_mag","LIB_STMI_MINE_mag","LIB_1Rnd_Faustpatrone","LIB_1Rnd_145x114","LIB_1Rnd_145x114","LIB_1Rnd_145x114"]

"LIB_PzFaust_60m"
"LIB_Faustpatrone"

"fow_w_piat"
[["fow_1Rnd_piat_HEAT",1],["LIB_Ladung_Big_MINE_mag",1],["LIB_Ladung_Small_MINE_mag",1],
["LIB_Ladung_Big_MINE_mag",1],["LIB_Ladung_Small_MINE_mag",1],["LIB_STMI_MINE_mag",1],
["LIB_STMI_MINE_mag",1],["LIB_1Rnd_PzFaust_60m",1]]

"TransportMagazines" >> "_xx_LIB_5Rnd_RPzB"
magazine = "LIB_1Rnd_RPzB";
weapon = "";
count = 5;
"TransportWeapons"
*/
