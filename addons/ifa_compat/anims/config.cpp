#include "..\macros.hpp"

class CfgPatches {
    class IFACONFIG(AnimReconfig) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
    };
};

class CfgMovesBasic;
class CfgMovesMaleSdr: CfgMovesBasic {
    class StandBase;
    class Default;
    class States {
        class AmovPercMstpSrasWrflDnon;
        class AmovPercMstpSlowWrflDnon;
        class AmovPpneMstpSrasWrflDnon;
        class SprintBaseDf;

/*
        class AmovPercMrunSrasWrflDf: AmovPercMstpSrasWrflDnon {	// run weapon up
            speed = 0.684541; //Vanilla and RHS 0.684541  14km
        };
*/
        class AmovPercMrunSlowWrflDf: AmovPercMstpSlowWrflDnon {   // run weapon lowered
            speed = 0.77; //Vanilla 0.715882  12km// RHS0.77 15km
        };
        class AmovPercMevaSrasWrflDf: SprintBaseDf {  // sprint weapon up
            speed = 1.6; //Vanilla 1.60971 16km//RHS 1.8 20km
        };
        class AmovPercMevaSlowWrflDf: AmovPercMevaSrasWrflDf {}; // sprint weapon down
    };
};
