//#include "macros.hpp"

class CfgPatches {
    class FRL_NoZoom_ifa {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.70;
        requiredAddons[] = {
            "FRL_NoZoom",
            "FRL_IFA_Compat"
        };
    };
};

#define __ZOOMMIN 0.75
#define __ZOOMMAX 0.375
#define __ZOOMINIT 0.75

class CfgVehicles {
    class All {
        class ViewOptics;
    };
    class AllVehicles: All {
        class NewTurret {
            class ViewGunner: ViewOptics {
            };
            class ViewOptics {
            };
        };
        class ViewPilot {
        };
        class ViewOptics {
        };
    };
    class Land: AllVehicles {
    };
    class LandVehicle: Land {
    };
    class Car: LandVehicle {
        class ViewPilot: ViewPilot {
        };
    };
    class Tank: LandVehicle {
        class ViewOptics;
    };
    class Air: AllVehicles {
    };
    class Helicopter: Air {
        class ViewPilot: ViewPilot {
        };
    };
    class Plane: Air {
        class ViewPilot: ViewPilot {
        };
        class ViewOptics: ViewOptics {
        };
    };
    class Ship: AllVehicles {
    };
    class UAV: Plane {
    };
    class Tank_F: Tank {
        class Turrets {
            class MainTurret: NewTurret {
            };
        };
    };
    class Car_F: Car {
        class ViewPilot: ViewPilot {
        };
        class NewTurret: NewTurret {
        };
        class Turrets {
            class MainTurret: NewTurret {
                class ViewOptics: ViewOptics {
                };
                class ViewGunner: ViewOptics {
                };
            };
        };
    };
    class Truck_F: Car_F {
        class Turrets: Turrets {
            class ViewGunner;
        };
    };
    class Helicopter_Base_F: Helicopter {
    };
    class Ship_F: Ship {
    };
    class Boat_F: Ship_F {
    };
    class UAV_01_base_F: Helicopter_Base_F {
        class PilotCamera {
            class OpticsIn {
                class Wide {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class UAV_05_Base_F: UAV {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
        class Viewoptics: ViewOptics {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class UAV_06_base_F: Helicopter_Base_F {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class LIB_Truck_base: Truck_F {
        class ViewPilot: ViewPilot {
        };
        class Turrets: Turrets {
            class MainTurret: MainTurret {
            };
        };
    };
    class LIB_SdKfz222_base: LIB_Truck_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner: ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class LIB_SdKfz234_base: LIB_Truck_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewGunner: ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class LIB_Boat_base: Boat_F {
    };
    class LIB_Car_base: Car_F {
        class ViewPilot: ViewPilot {
        };
    };
    class LIB_WheeledTracked_APC_base: Tank_F {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
                class ViewGunner: ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class LIB_US6_base: LIB_Truck_base {
    };
    class LIB_US6_BM13: LIB_US6_base {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class LIB_LCM3: LIB_Boat_base {
    };
    class LIB_LCM3_Armed: LIB_LCM3 {
        class Turrets {
            class LeftTurret: NewTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class LIB_Kfz1_base: LIB_Car_base {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class LIB_Scout_M3_base: LIB_Truck_base {
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                class ViewOptics: ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
                class ViewGunner: ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
    class LIB_SdKfz251_base: LIB_WheeledTracked_APC_base {
        class Turrets: Turrets {
        };
    };
    class LIB_GazM1_base: LIB_Car_base {
        class ViewPilot: ViewPilot {
            minFov = __ZOOMMAX;
            maxFov = __ZOOMMIN;

        };
    };
    class LIB_SdKfz251_FFV_base: LIB_SdKfz251_base {
        class Turrets: Turrets {
            class RearTurret: NewTurret {
                class ViewOptics {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
                class ViewGunner {
                    minFov = __ZOOMMAX;
                    maxFov = __ZOOMMIN;

                };
            };
        };
    };
};
