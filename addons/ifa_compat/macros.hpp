// -- Module defines -- //
#define MODULE IFA_Compat

// -- Global defines -- //
#include "..\main\macros_local.hpp"


#define IFACONFIG(var1) TRIPLE(PREFIX,MODULE,var1)

#define mag_2(a) a, a
#define mag_5(a) a, a, a, a, a
#define mag_20(a) mag_5(a), mag_5(a), mag_5(a), mag_5(a)
#define mag_48(a) mag_20(a), mag_20(a), mag_5(a), mag_2(a), mag_2(a)
