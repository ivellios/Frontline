#include "macros.hpp"

class CfgPatches {
	class ADDON {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
		requiredAddons[] = {"FRL_Main"};
	};
};

class CfgAmmo {
    class SmokeShell;
    class FRL_FO_Explosion: SmokeShell {
        CraterEffects = "NoCrater";
        explosionEffects = "NoExplosion";
        cost = 150;
        timeToLive = 25;
        effectsSmoke = "FO_Explosion";
        // model = "\A3\Weapons_f\empty";
        SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1", 0.125893, 1, 16.5};
        SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2", 0.125893, 1, 16.5};
        SmokeShellSoundLoop3[] = {"A3\Sounds_F\sfx\fire2_loop", 0.125893, 1, 13};
        SmokeShellSoundLoop4[] = {"A3\Sounds_F\sfx\special_sfx\sparkles_wreck_5_loop", 0.125893, 1, 8};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1", 0.5, "SmokeShellSoundLoop2", 0.5, "SmokeShellSoundLoop3", 0.5, "SmokeShellSoundLoop4", 0.5};
        aiAmmoUsageFlags = "1 + 2 + 64";
    };
	class FRL_FO_Explosion1: SmokeShell {
        CraterEffects = "ScudEffect";
        explosionEffects = "ScudEffect";
        cost = 150;
        timeToLive = 25;
        effectsSmoke = "ScudEffect";
        // model = "\A3\Weapons_f\empty";
        SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1", 0.125893, 1, 16.5};
        SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2", 0.125893, 1, 16.5};
        SmokeShellSoundLoop3[] = {"A3\Sounds_F\sfx\fire2_loop", 0.125893, 1, 13};
        SmokeShellSoundLoop4[] = {"A3\Sounds_F\sfx\special_sfx\sparkles_wreck_5_loop", 0.125893, 1, 8};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1", 0.5, "SmokeShellSoundLoop2", 0.5, "SmokeShellSoundLoop3", 0.5, "SmokeShellSoundLoop4", 0.5};
        aiAmmoUsageFlags = "1 + 2 + 64";
    };
	class FRL_FO_Explosion2: SmokeShell {
        CraterEffects = "Blastcore_Shockwave_Large_Dust";
        explosionEffects = "Blastcore_Shockwave_Large_Dust";
        cost = 150;
        timeToLive = 25;
        effectsSmoke = "Blastcore_Shockwave_Large_Dust";
        // model = "\A3\Weapons_f\empty";
        SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1", 0.125893, 1, 16.5};
        SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2", 0.125893, 1, 16.5};
        SmokeShellSoundLoop3[] = {"A3\Sounds_F\sfx\fire2_loop", 0.125893, 1, 13};
        SmokeShellSoundLoop4[] = {"A3\Sounds_F\sfx\special_sfx\sparkles_wreck_5_loop", 0.125893, 1, 8};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1", 0.5, "SmokeShellSoundLoop2", 0.5, "SmokeShellSoundLoop3", 0.5, "SmokeShellSoundLoop4", 0.5};
        aiAmmoUsageFlags = "1 + 2 + 64";
    };
	class FRL_FO_Explosion3: SmokeShell {
        CraterEffects = "Blastcore_Vehicle_Explosion_Emit";
        explosionEffects = "Blastcore_Vehicle_Explosion_Emit";
        cost = 150;
        timeToLive = 25;
        effectsSmoke = "Blastcore_Vehicle_Explosion_Emit";
        // model = "\A3\Weapons_f\empty";
        SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1", 0.125893, 1, 16.5};
        SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2", 0.125893, 1, 16.5};
        SmokeShellSoundLoop3[] = {"A3\Sounds_F\sfx\fire2_loop", 0.125893, 1, 13};
        SmokeShellSoundLoop4[] = {"A3\Sounds_F\sfx\special_sfx\sparkles_wreck_5_loop", 0.125893, 1, 8};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1", 0.5, "SmokeShellSoundLoop2", 0.5, "SmokeShellSoundLoop3", 0.5, "SmokeShellSoundLoop4", 0.5};
        aiAmmoUsageFlags = "1 + 2 + 64";
    };
	class FRL_FO_Explosion4: SmokeShell {
        CraterEffects = "MineUnderwaterPDMExplosion";
        explosionEffects = "MineUnderwaterPDMExplosion";
        cost = 150;
        timeToLive = 25;
        effectsSmoke = "MineUnderwaterPDMExplosion";
        // model = "\A3\Weapons_f\empty";
        SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1", 0.125893, 1, 16.5};
        SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2", 0.125893, 1, 16.5};
        SmokeShellSoundLoop3[] = {"A3\Sounds_F\sfx\fire2_loop", 0.125893, 1, 13};
        SmokeShellSoundLoop4[] = {"A3\Sounds_F\sfx\special_sfx\sparkles_wreck_5_loop", 0.125893, 1, 8};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1", 0.5, "SmokeShellSoundLoop2", 0.5, "SmokeShellSoundLoop3", 0.5, "SmokeShellSoundLoop4", 0.5};
        aiAmmoUsageFlags = "1 + 2 + 64";
    };
	class FRL_FO_Explosion5: SmokeShell {
        CraterEffects = "IEDMineFlame";
        explosionEffects = "IEDMineFlame";
        cost = 150;
        timeToLive = 25;
        effectsSmoke = "IEDMineFlame";
        // model = "\A3\Weapons_f\empty";
        SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1", 0.125893, 1, 16.5};
        SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2", 0.125893, 1, 16.5};
        SmokeShellSoundLoop3[] = {"A3\Sounds_F\sfx\fire2_loop", 0.125893, 1, 13};
        SmokeShellSoundLoop4[] = {"A3\Sounds_F\sfx\special_sfx\sparkles_wreck_5_loop", 0.125893, 1, 8};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1", 0.5, "SmokeShellSoundLoop2", 0.5, "SmokeShellSoundLoop3", 0.5, "SmokeShellSoundLoop4", 0.5};
        aiAmmoUsageFlags = "1 + 2 + 64";
    };
	class FRL_FO_Explosion6: SmokeShell {
        CraterEffects = "WeaponCloudsMGun";
        explosionEffects = "WeaponCloudsMGun";
        cost = 150;
        timeToLive = 25;
        effectsSmoke = "WeaponCloudsMGun";
        // model = "\A3\Weapons_f\empty";
        SmokeShellSoundLoop1[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop1", 0.125893, 1, 16.5};
        SmokeShellSoundLoop2[] = {"A3\Sounds_F\weapons\smokeshell\smoke_loop2", 0.125893, 1, 16.5};
        SmokeShellSoundLoop3[] = {"A3\Sounds_F\sfx\fire2_loop", 0.125893, 1, 13};
        SmokeShellSoundLoop4[] = {"A3\Sounds_F\sfx\special_sfx\sparkles_wreck_5_loop", 0.125893, 1, 8};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1", 0.5, "SmokeShellSoundLoop2", 0.5, "SmokeShellSoundLoop3", 0.5, "SmokeShellSoundLoop4", 0.5};
        aiAmmoUsageFlags = "1 + 2 + 64";
    };
};

class CfgVehicles {
	class ThingX;
    // -- These are made by Chairborne
	class FRL_Backpacks_Base: ThingX {
	    animated = 0;
			destrType = "DestructNo"; //cant be destroyed
			simulation = "ThingX";
	    icon = "iconCrate";
			accuracy = 0.2;
	  	typicalCargo[] = {};
	    editorCategory = "EdCat_Supplies";
	    vehicleClass = "Ammo";
	    maximumLoad = 0;
	    transportMaxWeapons = 0;
	    transportMaxMagazines = 0;
	    transportMaxBackpacks = 0;
	    transportAmmo = 0;
	    transportRepair = 0;
	    transportFuel = 0;
	    supplyRadius = 0;
	    cost = 0;
	    armor = 999999; //shouldn't do anything since we declared DestructNo above
	    mapSize = 2;
	    waterLinearDampingCoefY = 1;
	    waterAngularDampingCoef = 0.1;
	    scope = 1;
	    curatorInfoTypeEmpty = "RscDisplayAttributesInventory";
	};
	class FRL_Backpacks_West: FRL_Backpacks_Base {
	    scope = 2;
	    author = "Chairborne";
			displayName = "Rallypoint (West)";
			model = "\pr\frl\addons\obj_rallypoint\frl_Backpacks_West.p3d";
	};
	class FRL_Backpacks_East: FRL_Backpacks_Base {
	    scope = 2;
	    author = "Chairborne";
			displayName = "Rallypoint (East)";
			model = "\pr\frl\addons\obj_rallypoint\frl_Backpacks_East.p3d";
	};

    class FRL_FO_Box1: FRL_Backpacks_Base {
      scope = 2;
      author = "Adanteh";
      displayName = "FOB";
      editorCategory = "EdCat_Supplies";
      model = "\A3\Weapons_F_Bootcamp\Ammoboxes\Weaponsbox_02_F.p3d";
    };
};
