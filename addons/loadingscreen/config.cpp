#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
  class loadingScreen {
    default = "\pr\frl\addons\loadingscreen\images\default_01.paa";
    altis[] = {
      "\pr\frl\addons\loadingscreen\images\altis_01.paa",
      "\pr\frl\addons\loadingscreen\images\altis_02.paa"
    };
    frl_bornholmwest[] = {
      "\pr\frl\addons\loadingscreen\images\bornholm.paa"
    };
    bozcaada[] = {
      "\pr\frl\addons\loadingscreen\images\bozcaada_01.paa"
    };
    CUP_kunduz[] = {
      "\pr\frl\addons\loadingscreen\images\kunduz_01.paa",
      "\pr\frl\addons\loadingscreen\images\kunduz_02.paa"
    };
    stratis[] = {
      "\pr\frl\addons\loadingscreen\images\stratis_01.paa"
    };
    tanoa[] = {
      "\pr\frl\addons\loadingscreen\images\tanoa_01.paa",
      "\pr\frl\addons\loadingscreen\images\tanoa_02.paa"
    };
  };
};
