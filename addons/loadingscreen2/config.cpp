#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
  class loadingScreen {
    ProvingGrounds_PMC[] = {
      "\pr\frl\addons\loadingscreen2\images\Proving_Grounds.paa"
    };
    Bootcamp_ACR[] = {
      "\pr\frl\addons\loadingscreen2\images\Bukovina.paa"
    };
    Malden[] = {
      "\pr\frl\addons\loadingscreen2\images\Malden.paa"
    };
    Woodland_ACR[] = {
      "\pr\frl\addons\loadingscreen2\images\Bystrica.paa"
    };
    Chernarus[] = {
      "\pr\frl\addons\loadingscreen2\images\Chernarus.paa"
    };
    Chernarus_Summer[] = {
      "\pr\frl\addons\loadingscreen2\images\Chernarus_Summer.paa"
    };
    Porto[] = {
      "\pr\frl\addons\loadingscreen2\images\Porto.paa"
    };
    Sara[] = {
      "\pr\frl\addons\loadingscreen2\images\Sahrani.paa"
    };
    Sara_dbe1[] = {
      "\pr\frl\addons\loadingscreen2\images\Sahrani.paa"
    };
    Shapur_BAF[] = {
      "\pr\frl\addons\loadingscreen2\images\Shapur.paa"
    };
    SaraLite[] = {
      "\pr\frl\addons\loadingscreen2\images\Southern_Sahrani.paa"
    };
    Mountains_ACR[] = {
      "\pr\frl\addons\loadingscreen2\images\Takistan.paa"
    };
    Takistan[] = {
      "\pr\frl\addons\loadingscreen2\images\Takistan.paa"
    };
    Utes[] = {
      "\pr\frl\addons\loadingscreen2\images\Utes.paa"
    };
    Zargabad[] = {
      "\pr\frl\addons\loadingscreen2\images\Zargabad.paa"
    };
  };
};
