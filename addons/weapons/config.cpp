#include "macros.hpp"

class CfgPatches {
	class ADDON {
		units[] = {};
		weapons[] = {"FRL_BreachingCharge_Mag"};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};

		requiredAddons[] = {"FRL_Main"};
	};
};

class CfgWeapons {
    class ItemCore;
    class InventoryOpticsItem_Base_F;

    class InventoryItem_Base_F;
    class InventoryFlashLightItem_Base_F: InventoryItem_Base_F { };
    class acc_flashlight: ItemCore {
        class ItemInfo: InventoryFlashLightItem_Base_F  {
            class FlashLight
            {
                color[] = {1900,1800,1700};
                coneFadeCoef = 10;
                flareSize = 8.4;
                flareMaxDistance = "100.0f";
                innerAngle = 35;
                intensity = 48;
                outerAngle = 95;
                class Attenuation
                {
                    start = 40;
                    constant = 210;
                    linear = 40;
                    quadratic = 20;
                    hardLimitStart = 320;
                    hardLimitEnd = 490;
                };
            };
        };
    };
	class Default;
	class LauncherCore: Default{ };
	class Launcher: LauncherCore { };
	class Launcher_Base_F: Launcher
	{
		reloadAction = "GestureReloadRPG7";
	};
	class launch_Titan_base: Launcher_Base_F
	{
		reloadAction = "GestureReloadRPG7";
	};
	class launch_RPG32_F: Launcher_Base_F
	{
		reloadAction = "GestureReloadRPG7";
	};
	class launch_NLAW_F: Launcher_Base_F
	{
		reloadAction = "GestureReloadRPG7";
	};

    class optic_Hamr : ItemCore {
        displayName = "HAMR 4x";
        descriptionShort = "High Accuracy Multi-Range Optic<br />Magnification: 4x";

        class ItemInfo : InventoryOpticsItem_Base_F {
            mass = 4;
            optics = 1;
            optictype = 1;
            modelOptics = "\A3\Weapons_f\acc\reticle_HAMR";

            class OpticsModes {
                class Hamr2Scope {
                    opticsid = 2;
                    usemodeloptics = 0;
                    opticsppeffects[] = {"OpticsCHAbera5", "OpticsBlur1"};
                    opticszoominit = 0.13;
                    opticszoommax = 0.13;
                    opticszoommin = 0.13;
                    memorypointcamera = "opticView";
                    //visionmode[] = {"Normal"};
                    opticsflare = 1;
                    opticsdisableperipherialvision = 1;
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                    cameradir = "";
                    discretefov[] = {0.125};
                    discreteinitindex = 0;
                };
            };
        };
    };

    class optic_Arco : ItemCore {
        displayname = "ARCO 6x";
        descriptionshort = "Advanced Rifle Combat Optic<br />Magnification: 6x";
        class ItemInfo: InventoryOpticsItem_Base_F {
            mass = 4;
            optics = 1;
            optictype = 1;
            modelOptics = "\A3\Weapons_f\acc\reticle_arco_F";

            class OpticsModes {
                class ARCO2collimator {
                    cameradir = "";
                    distancezoommax = 300;
                    distancezoommin = 300;
                    memorypointcamera = "eye";
                    opticsdisableperipherialvision = 0;
                    opticsdisplayname = "CQB";
                    opticsflare = 0;
                    opticsid = 1;
                    opticsppeffects[] = {""};
                    opticszoominit = 0.75;
                    opticszoommax = 1.1;
                    opticszoommin = 0.375;
                    usemodeloptics = 0;
                    visionmode[] = {};
                };
                class ARCO2scope: ARCO2collimator {
                    opticsID = 2;
                    useModelOptics = 0;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsZoomMin = 0.125;
                    opticsZoomMax = 0.125;
                    opticsZoomInit = 0.125;
                    memoryPointCamera = "opticView";
                    opticsFlare = 1;
                    opticsDisablePeripherialVision = 1;
                };
            };
        };
    };
    class optic_ERCO_blk_F : ItemCore {
        class ItemInfo: InventoryOpticsItem_Base_F {
            mass = 4;
            optics = 1;
            optictype = 1;
            modelOptics = "\A3\Weapons_f\acc\reticle_arco_F";

            class OpticsModes {
                class ARCO2collimator {
                    cameradir = "";
                    distancezoommax = 300;
                    distancezoommin = 300;
                    memorypointcamera = "eye";
                    opticsdisableperipherialvision = 0;
                    opticsdisplayname = "CQB";
                    opticsflare = 0;
                    opticsid = 1;
                    opticsppeffects[] = {""};
                    opticszoominit = 0.75;
                    opticszoommax = 1.1;
                    opticszoommin = 0.375;
                    usemodeloptics = 0;
                    visionmode[] = {};
                };
                class ARCO2scope: ARCO2collimator {
                    cameradir = "";
                    distancezoommax = 300;
                    distancezoommin = 300;
                    memoryPointCamera = "opticView";
                    opticsDisablePeripherialVision = 1;
                    opticsFlare = 1;
                    opticsID = 2;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsZoomMin = 0.14;
                    opticsZoomMax = 0.14;
                    opticsZoomInit = 0.14;
                    useModelOptics = 0;
                };
            };
        };
    };
	class optic_DMS: ItemCore
	{
		class ItemInfo: InventoryOpticsItem_Base_F
			{
				class OpticsModes
				{
					class Snip
					{
						opticsID = 1;
						useModelOptics = 1;
						opticsPPEffects[] = {"OpticsCHAbera2", "OpticsBlur3"};
						opticsZoomMin = 0.1875;
						opticsZoomMax = 0.125;
						opticsZoomInit = 0.125;
						discreteDistance[] = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200};
						discreteDistanceInitIndex = 1;
						distanceZoomMin = 300;
						distanceZoomMax = 1200;
						discretefov[] = {0.125, 0.0625};
						discreteInitIndex = 0;
						memoryPointCamera = "opticView";
						modelOptics[] = {"\A3\Weapons_F_EPA\acc\reticle_marksman_F", "\A3\Weapons_F_EPA\acc\reticle_marksman_z_F"};
						visionMode[] = {"Normal"};
						opticsFlare = 1;
						opticsDisablePeripherialVision = 1;
						cameraDir = "";
					};
					class Iron: Snip
					{
						opticsID = 2;
						useModelOptics = 0;
						opticsPPEffects[] = {"", ""};
						opticsFlare = 0;
						opticsDisablePeripherialVision = 0;
						opticsZoomMin = 0.25;
						opticsZoomMax = 1.25;
						opticsZoomInit = 0.75;
						memoryPointCamera = "eye";
						visionMode[] = {};
						discretefov[] = {};
						distanceZoomMin = 200;
						distanceZoomMax = 200;
						discreteDistance[] = {200};
						discreteDistanceInitIndex = 0;
					};
				};
			};
	};
	class optic_LRPS : ItemCore {
        author = "Bohemia Interactive";
        _generalMacro = "optic_LRPS";
        scope = 2;
        displayName = "LRPS";
        picture = "\A3\Weapons_F_EPB\Acc\Data\UI\gear_acco_sniper02_CA.paa";
        model = "\A3\Weapons_F_EPB\Acc\acco_sniper02_F.p3d";
        descriptionShort = "Long-Range Precision Scope<br />Magnification: 6x–25x";
        class ItemInfo : InventoryOpticsItem_Base_F {
            mass = 16;
            opticType = 2;
            weaponInfoType = "RscWeaponRangeZeroingFOV";
            optics = 1;
            modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
            class OpticsModes {
                class Snip {
                    opticsID = 1;
                    opticsDisplayName = "WFOV";
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera1", "OpticsBlur1"};
                    opticsZoomMin = 0.09375;
					opticsZoomMax = 0.0625;
					opticsZoomInit = 0.0625;
                    discreteDistance[] = {300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400};
                    discreteDistanceInitIndex = 2;
                    distanceZoomMin = 300;
                    distanceZoomMax = 2400;
                    discretefov[] = {0.042, 0.01};
                    discreteInitIndex = 0;
                    memoryPointCamera = "opticView";
                    modelOptics[] = {"\A3\Weapons_F\acc\reticle_lrps_F", "\A3\Weapons_F\acc\reticle_lrps_z_F"};
                    visionMode[] = {"Normal"};
                    opticsFlare = 1;
                    opticsDisablePeripherialVision = 1;
                    cameraDir = "";
                };
            };
        };
        inertia = 0.2;
    };
	class optic_SOS: ItemCore
		{
			class ItemInfo: InventoryOpticsItem_Base_F
			{
				class OpticsModes
				{
					class Snip
					{
						opticsID = 1;
						opticsDisplayName = "WFOV";
						useModelOptics = 1;
						opticsPPEffects[] = {"OpticsCHAbera1", "OpticsBlur1"};
						opticsZoomMin = 0.13;
						opticsZoomMax = 0.107;
						opticsZoomInit = 0.107;
						discreteDistance[] = {300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600};
						discreteDistanceInitIndex = 0;
						distanceZoomMin = 300;
						distanceZoomMax = 1600;
						discretefov[] = {0.107, 0.05};
						discreteInitIndex = 0;
						memoryPointCamera = "opticView";
						modelOptics[] = {"\A3\Weapons_F\acc\reticle_sniper_F", "\A3\Weapons_F\acc\reticle_sniper_z_F"};
						visionMode[] = {"Normal"};
						opticsFlare = 1;
						opticsDisablePeripherialVision = 1;
						cameraDir = "";
					};
					class Iron
					{
						opticsID = 2;
						opticsDisplayName = "";
						useModelOptics = 0;
						opticsPPEffects[] = {"", ""};
						opticsFlare = 0;
						opticsDisablePeripherialVision = 0;
						opticsZoomMin = 0.25;
						opticsZoomMax = 1.25;
						opticsZoomInit = 0.75;
						memoryPointCamera = "eye";
						visionMode[] = {};
						discretefov[] = {};
						discreteDistance[] = {200};
						discreteDistanceInitIndex = 0;
						distanceZoomMin = 200;
						distanceZoomMax = 200;
						discreteInitIndex = 0;
					};
				};
			};
	};
    class optic_MRCO : ItemCore {
        displayName = "MRCO 1x/3x";
        descriptionShort = "Medium Range Combat Optic<br />Magnification: 1x/3x";
        class ItemInfo : InventoryOpticsItem_Base_F {
            opticType = 1;
            mass = 4;
            optics = 1;
            modelOptics = "\A3\Weapons_f_beta\acc\reticle_MRCO_F";

            class OpticsModes {
                class MRCOcq {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsPPEffects[] = {""};
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 1;
                    opticsZoomMin = 0.375;
                    opticsZoomMax = 1.1;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    visionMode[] = {};
                    distanceZoomMin = 100;
                    distanceZoomMax = 100;
                };

                class MRCOscope {
                    cameradir = "";
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                    memorypointcamera = "opticView";
                    opticsdisableperipherialvision = 0;
                    opticsdisplayname = "MRCO";
                    opticsflare = 1;
                    opticsid = 2;
                    opticsppeffects[] = {"OpticsCHAbera2", "OpticsBlur2"};
                    opticszoominit = 0.1875;
                    opticszoommax = 0.1875;
                    opticszoommin = 0.1875;
                    discretefov[] = {0.1875};
                    discreteinitindex = 0;
                    usemodeloptics = 0;
                    visionmode[] = {};
                };
            };
        };
    };

    class CUP_optic_ACOG : ItemCore {
        displayName = "Trijicon ACOG TA31F";
        descriptionShort = "Advanced Combat Optical Bubblegum";
        class ItemInfo : InventoryOpticsItem_Base_F {
            opticType = 1;
            mass = 7;
            optics = 1;
            modelOptics = "CUP\Weapons\CUP_Weapons_West_Attachments\ACOG_Generic\CUP_ACOG_optic.p3d";
            class OpticsModes {
                class ACOG {
                cameradir = "";
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                    memorypointcamera = "opticView";
                    opticsdisableperipherialvision = 1;
                    opticsflare = 1;
                    opticsid = 1;
                    opticsppeffects[] = {"OpticsCHAbera1", "OpticsBlur1"};
                    opticszoominit = 0.1875;
                    opticszoommax = 0.1875;
                    opticszoommin = 0.1875;
                    usemodeloptics = 1;
                    visionmode[] = {"Normal"};
                };
                class Kolimator {
                    cameradir = "";
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                    memorypointcamera = "eye";
                    opticsdisableperipherialvision = 0;
                    opticsflare = 0;
                    opticsid = 2;
                    opticsppeffects[] = {};
                    opticszoominit = 0.75;
                    opticszoommax = 1.25;
                    opticszoommin = 0.25;
                    usemodeloptics = 0;
                    visionmode[] = {};
                };
            };
        };
    };
    class CUP_optic_ElcanM145 : ItemCore {
        class ItemInfo : InventoryOpticsItem_Base_F {
            class OpticsModes {
                class M145 {
                    opticsZoomInit = 0.1875;
                    opticsZoomMax = 0.1875;
                    opticsZoomMin = 0.1875;
                };
            };
        };
    };



    class FRL_ItemCore;
    class FRL_BreachingCharge_Wpn: FRL_ItemCore {
        scope = 2;
        displayName = "Breaching Charge";
        picture = "\A3\Weapons_F\Data\UI\gear_c4_charge_small_CA.paa";
        model = "\A3\Weapons_F\Explosives\c4_charge_small";
        descriptionShort = "Type: Charge<br />Used By: Engineer<br />Used on: Walls";
        descriptionUse = "Place Breaching Charge";
        allowedSlots[] = {901, 701};
        class ItemInfo: InventoryItem_Base_F {
            mass = 10;
        };
    };
    class FRL_ExplosiveCharge_Wpn: FRL_ItemCore {
        scope = 2;
        displayName = "Explosive Charge";
        picture = "\A3\Weapons_F\Data\UI\gear_c4_charge_small_CA.paa";
        model = "\A3\Weapons_F\Explosives\c4_charge_small";
        descriptionShort = "Type: Charge<br />Used By: Engineer<br />Used on: Walls";
        descriptionUse = "Place Explosive Charge";
        allowedSlots[] = {901, 701};
        class ItemInfo: InventoryItem_Base_F {
            mass = 10;
        };
    };

};

class CfgAmmo {
    class DemoCharge_Remote_Ammo;
	class FRL_ExplosiveCharge_Ammo: DemoCharge_Remote_Ammo {
		triggerWhenDestroyed = 1;
	};
    class FRL_BreachingCharge_Ammo: DemoCharge_Remote_Ammo {
        hit = 10;
        indirectHit = 50;
        indirectHitRange = 2.5;
        ExplosionEffects = "BreachingExplosion";
        CraterEffects = "MineNondirectionalCraterSmall";
        triggerWhenDestroyed = 1;
        SoundSetExplosion[] = {"ExplosiveCharge_Exp_SoundSet", "ExplosiveCharge_Tail_SoundSet", "Explosion_Debris_SoundSet"};
    };
};

class CfgVehicles {
	class All {};
	class AllVehicles: All {
		class NewTurret {};
	};
	class Land: AllVehicles {};
	class LandVehicle: Land {};
	class StaticWeapon: LandVehicle {
		class Turrets {
			class MainTurret: NewTurret {};
		};
	};
	class StaticMortar : StaticWeapon {
		class Turrets : Turrets {
			class MainTurret: MainTurret {};
		};
	};
	class Mortar_01_base_F : StaticMortar {
		class Turrets : Turrets {
			class MainTurret: MainTurret {
				magazines[] = {"8Rnd_82mm_Mo_shells","8Rnd_82mm_Mo_Flare_white","8Rnd_82mm_Mo_Smoke_white","8Rnd_82mm_Mo_Smoke_white"};
			};
		};
	};
};
