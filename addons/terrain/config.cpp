#include "macros.hpp"


class CfgPatches {
	class ADDON {
		units[] = {};
		weapons[] = {};
		requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};

		requiredAddons[] = {"FRL_Main"};
	};
};
#define KH_GRASS_CUT_COEF 0.55
#define KH_WHEAT_CUT_COEF 0.75

class CfgWorlds {

  class DefaultClutter;
  class DefaultWorld;
  class CAWorld: DefaultWorld {};
	class Tanoa: CAWorld {
		NeedsApex = 1;
		
		class clutter {

					class c_forest_BiglLeaves : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_BiglLeaves.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.4 * KH_GRASS_CUT_COEF;
						scaleMin = 0.5 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_forest_BiglLeaves2 : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_BiglLeaves2.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.4 * KH_GRASS_CUT_COEF;
						scaleMin = 0.5 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_forest_fern : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_fern.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.65 * KH_GRASS_CUT_COEF;
						scaleMin = 0.8 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class c_forest_roots : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_roots.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 2.2 * KH_GRASS_CUT_COEF;
						scaleMin = 0.4 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class c_forest_violet_leaves : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_violet_leaves.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.05 * KH_GRASS_CUT_COEF;
						scaleMin = 0.7 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_forest_violet_leaves2 : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_violet_leaves2.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.15 * KH_GRASS_CUT_COEF;
						scaleMin = 0.5 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class c_forest_violet_single : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_violet_single.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.9 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_Grass_leaf_big : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_leaf_big.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.4 * KH_GRASS_CUT_COEF;
						scaleMin = 0.5 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_Grass_Leaves_coltsfoot : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_Leaves_coltsfoot.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.4 * KH_GRASS_CUT_COEF;
						scaleMin = 0.5 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class c_Grass_mimosa : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_mimosa.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.3 * KH_GRASS_CUT_COEF;
						scaleMin = 0.4 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class c_Grass_nettle : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_nettle.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.6 * KH_GRASS_CUT_COEF;
						scaleMin = 0.8 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class c_Grass_short_bunch : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_short_bunch.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.5 * KH_GRASS_CUT_COEF;
						scaleMin = 1 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_Grass_short_leaf : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_short_leaf.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.6 * KH_GRASS_CUT_COEF;
						scaleMin = 1 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_Grass_short_mimosa : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_short_mimosa.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.5 * KH_GRASS_CUT_COEF;
						scaleMin = 1 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_Grass_short_nettle : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_short_nettle.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.5 * KH_GRASS_CUT_COEF;
						scaleMin = 1 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_Grass_short_small : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_short_small.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.9 * KH_GRASS_CUT_COEF;
						scaleMin = 1 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_GrassBunch_HI : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_GrassBunch_HI.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.3 * KH_GRASS_CUT_COEF;
						scaleMin = 0.8 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_GrassBunch_LO : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_GrassBunch_LO.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.4 * KH_GRASS_CUT_COEF;
						scaleMin = 0.65 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_GrassTropic : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_Tropic.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.4 * KH_GRASS_CUT_COEF;
						scaleMin = 0.65 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class c_red_dirt_leaves : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Red_dirt\c_red_dirt_leaves.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.2 * KH_GRASS_CUT_COEF;
						scaleMin = 1 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class Coral1Exp : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Seabed\c_Coral1_exp.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.3 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class Coral2Exp : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Seabed\c_Coral2_exp.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.3 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class Coral3Exp : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Seabed\c_Coral3_exp.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.3 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class Coral4Exp : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Seabed\c_Coral4_exp.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.3 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class Coral5Exp : DefaultClutter {
						affectedByWind = 0.05;
						model = "A3\Vegetation_F_Exp\Clutter\Seabed\c_Coral5_exp.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.2 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
					class large_stones : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Red_dirt\c_red_dirt_large_stones.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.8 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class mimosa : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Red_dirt\c_red_dirt_mimosa.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.5 * KH_GRASS_CUT_COEF;
						scaleMin = 1 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class rock_stones : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Cliff\c_rock_stones.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1.18 * KH_GRASS_CUT_COEF;
						scaleMin = 0.2 * KH_GRASS_CUT_COEF;
						swLighting = 1;
					};
					class SeaWeed1Exp : DefaultClutter {
						affectedByWind = 0;
						model = "A3\Vegetation_F_Exp\Clutter\Seabed\c_SeaWeed1_exp.p3d";
						relativeColor[] = {1,1,1,1};
						scaleMax = 1 * KH_GRASS_CUT_COEF;
						scaleMin = 0.25 * KH_GRASS_CUT_COEF;
						swLighting = 0;
					};
			};
	};
	class Altis: CAWorld {
		class clutter {
			class Coral1: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral1.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral2: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral2.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral3: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral3.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.5 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral4: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral4.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral5: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\Plants_F\Clutter\c_Coral5.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.6 * KH_GRASS_CUT_COEF;
				scaleMin = 0.2 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class FlowerCakile: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Flower_Cakile.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.8 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class FlowerLowYellow2: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Flower_Low_Yellow2.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassBrushHighGreen: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_BrushHigh_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassBunchSmall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Bunch_Small.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.8 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassCrookedDead: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_GrassCrooked.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassDesertGroupSoft: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_GrassGreen_GroupSoft.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassDry: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Dry.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassGreen: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.85 * KH_GRASS_CUT_COEF;
				surfaceColor[] = {0.431,0.475,0.267};
				swLighting = 1;
			};
			class GrassLong_DryBunch: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_GrassLong_DryBunch.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassTall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Tall_Dead.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassTalltwo: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Tall_Dead.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.15 * KH_GRASS_CUT_COEF;
				scaleMin = 0.75 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class PlantGreenSmall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Plant_Green_Small.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class SeaWeed1: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_SeaWeed1.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class SeaWeed2: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_SeaWeed2.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrBigFallenBranches_pine: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrBigFallenBranches_pine02: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine02.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrBigFallenBranches_pine03: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine03.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrGrassDry: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrGrassDry.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrGrassDryGroup: DefaultClutter
			{
				affectedByWind = 0.65;
				model = "A3\plants_f\Clutter\c_StrGrassDry_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.65 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrGrassDryMediumGroup: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrGrassDryMedium_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrGrassGreenGroup: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrGrassGreen_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrPlantGermaderGroup: DefaultClutter
			{
				affectedByWind = 0.35;
				model = "A3\plants_f\Clutter\c_StrPlantGermader_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.5 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrPlantGreenShrub: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrPlantGreenShrub.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrPlantMullein: DefaultClutter
			{
				affectedByWind = 0.35;
				model = "A3\plants_f\Clutter\c_StrPlantMullein.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.15 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThistlePurpleSmall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrThistlePurple_small.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThistleSmallYellow: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrThistleSmallYellow.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThistleYellowShrub: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrThistleYellowShrub.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThornGrayBig: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGray.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornGraySmall: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGray.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornGreenBig: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGreen.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornGreenSmall: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGreen.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.5 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornKhakiBig: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornKhaki.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornKhakiSmall: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornKhaki.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrWeedBrownTallGroup: DefaultClutter
			{
				affectedByWind = 0.03;
				model = "A3\plants_f\Clutter\c_StrWeedBrownTall_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.25 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrWeedGreenTall: DefaultClutter
			{
				affectedByWind = 0.03;
				model = "A3\plants_f\Clutter\c_StrWeedGreenTall.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleHigh: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_High.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleHighDead: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_High_Dead.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.8 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleSmallYellow: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Small_Yellow.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleThornBrown: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Brown.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornBrownSmall: DefaultClutter
			{
				affectedByWind = 0.25;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Brown.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornDesert: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Desert.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornGray: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Gray.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 1.1 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornGreen: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornGreenSmall: DefaultClutter
			{
				affectedByWind = 0.25;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
		};
	};
	class Stratis: CAWorld {
		class clutter {
			class Coral1: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral1.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral2: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral2.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral3: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral3.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.5 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral4: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_Coral4.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class Coral5: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\Plants_F\Clutter\c_Coral5.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.6 * KH_GRASS_CUT_COEF;
				scaleMin = 0.2 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class FlowerCakile: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Flower_Cakile.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.8 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class FlowerLowYellow2: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Flower_Low_Yellow2.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassBrushHighGreen: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_BrushHigh_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassBunchSmall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Bunch_Small.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.8 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassCrookedDead: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_GrassCrooked.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassDesertGroupSoft: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_GrassGreen_GroupSoft.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassDry: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Dry.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassGreen: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.85 * KH_GRASS_CUT_COEF;
				surfaceColor[] = {0.431,0.475,0.267};
				swLighting = 1;
			};
			class GrassLong_DryBunch: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_GrassLong_DryBunch.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassTall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Tall_Dead.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class GrassTalltwo: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Grass_Tall_Dead.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.15 * KH_GRASS_CUT_COEF;
				scaleMin = 0.75 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class PlantGreenSmall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Plant_Green_Small.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class SeaWeed1: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_SeaWeed1.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class SeaWeed2: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_SeaWeed2.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrBigFallenBranches_pine: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrBigFallenBranches_pine02: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine02.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrBigFallenBranches_pine03: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine03.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrGrassDry: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrGrassDry.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrGrassDryGroup: DefaultClutter
			{
				affectedByWind = 0.65;
				model = "A3\plants_f\Clutter\c_StrGrassDry_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.65 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrGrassDryMediumGroup: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrGrassDryMedium_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrGrassGreenGroup: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrGrassGreen_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrPlantGermaderGroup: DefaultClutter
			{
				affectedByWind = 0.35;
				model = "A3\plants_f\Clutter\c_StrPlantGermader_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.5 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrPlantGreenShrub: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrPlantGreenShrub.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrPlantMullein: DefaultClutter
			{
				affectedByWind = 0.35;
				model = "A3\plants_f\Clutter\c_StrPlantMullein.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.15 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThistlePurpleSmall: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrThistlePurple_small.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThistleSmallYellow: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrThistleSmallYellow.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThistleYellowShrub: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_StrThistleYellowShrub.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrThornGrayBig: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGray.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornGraySmall: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGray.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornGreenBig: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGreen.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornGreenSmall: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornGreen.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.5 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornKhakiBig: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornKhaki.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.7 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrThornKhakiSmall: DefaultClutter
			{
				affectedByWind = 0.05;
				model = "A3\plants_f\Clutter\c_StrThornKhaki.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class StrWeedBrownTallGroup: DefaultClutter
			{
				affectedByWind = 0.03;
				model = "A3\plants_f\Clutter\c_StrWeedBrownTall_group.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.25 * KH_GRASS_CUT_COEF;
				scaleMin = 0.9 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class StrWeedGreenTall: DefaultClutter
			{
				affectedByWind = 0.03;
				model = "A3\plants_f\Clutter\c_StrWeedGreenTall.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.8 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleHigh: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_High.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.6 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleHighDead: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_High_Dead.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.8 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleSmallYellow: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Small_Yellow.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.9 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 1;
			};
			class ThistleThornBrown: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Brown.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.2 * KH_GRASS_CUT_COEF;
				scaleMin = 0.5 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornBrownSmall: DefaultClutter
			{
				affectedByWind = 0.25;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Brown.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornDesert: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Desert.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornGray: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Gray.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				scaleMin = 1.1 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornGreen: DefaultClutter
			{
				affectedByWind = 0;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 1 * KH_GRASS_CUT_COEF;
				scaleMin = 0.3 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
			class ThistleThornGreenSmall: DefaultClutter
			{
				affectedByWind = 0.25;
				model = "A3\plants_f\Clutter\c_Thistle_Thorn_Green.p3d";
				relativeColor[] = {1,1,1,1};
				scaleMax = 0.7 * KH_GRASS_CUT_COEF;
				scaleMin = 0.4 * KH_GRASS_CUT_COEF;
				swLighting = 0;
			};
		};
	};
			class Clutter
			{
				class ruha_sammal1_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_GrassCrookedGreen_summer.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1 * KH_GRASS_CUT_COEF;
				};
				class ruha_sammal2_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_blueBerry_summer.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal4_Clutter: DefaultClutter
				{
					model = "CA\plants2\clutter\c_picea.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal5_Clutter: DefaultClutter
				{
					model = "a3\plants_f\Clutter\c_Grass_Dry.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal6_Clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\forest\c_forest_roots.p3d";
					affectedByWind = 0;
					swLighting = 0;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal7_Clutter: DefaultClutter
				{
					model = "A3\Vegetation_F_Exp\Clutter\Forest\c_forest_violet_leaves.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal9_Clutter: DefaultClutter
				{
					model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine.p3d";
					affectedByWind = 0;
					swLighting = 0;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal10_Clutter: DefaultClutter
				{
					model = "a3\plants_f\Clutter\c_StrGrassGreen_group.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal13_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_raspBerry_summer.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal14_Clutter: DefaultClutter
				{
					model = "CA\plants_e2\Clutter\c_caluna_summer.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_sammal15_Clutter: DefaultClutter
				{
					model = "CA\plants2\Clutter\c_fern.p3d";
					affectedByWind = 0.2;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_kukka4_Clutter: DefaultClutter
				{
					model = "ca\plants\bolsevnik_group.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_pelto5_Clutter: DefaultClutter
				{
					model = "A3\Plants_F\Clutter\c_Grass_Green.p3d";
					affectedByWind = 0.4;
					swLighting = 1;
					scaleMin = 0.3;
					scaleMax = 0.9;
				};
				class ruha_pelto6_Clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\grass\c_Grass_Tropic.p3d";
					affectedByWind = 0.4;
					swLighting = 1;
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1.7 * KH_GRASS_CUT_COEF;
				};
				class ruha_pelto7_Clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\grass\c_GrassBunch_HI.p3d";
					affectedByWind = 0.4;
					swLighting = 1;
					scaleMin = 1 * KH_GRASS_CUT_COEF;
					scaleMax = 1.4 * KH_GRASS_CUT_COEF;
				};
				class ruha_uusi_grass_long_Clutter1: DefaultClutter
				{
					model = "ca\plants\clutter_grass_long.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_vaalea_heina: DefaultClutter
				{
					model = "ca\plants2\clutter\c_stubble.p3d";
					affectedByWind = 0.1;
					swLighting = 1;
					scaleMin = 0.9 * KH_WHEAT_CUT_COEF;
					scaleMax = 1.1 * KH_WHEAT_CUT_COEF;
				};
				class ruha_WeedDead: DefaultClutter
				{
					model = "ca\plants2\clutter\c_WeedDead.p3d";
					affectedByWind = 0.3;
					swLighting = 1;
					scaleMin = 0.75 * KH_GRASS_CUT_COEF;
					scaleMax = 1.1 * KH_GRASS_CUT_COEF;
				};
				class ruha_GrassDesert: DefaultClutter
				{
					model = "ca\plants_pmc\Clutter\c_GrassDesert_GroupSoft_PMC.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.8 * KH_GRASS_CUT_COEF;
				};
				class ruha_vaalea_ruohoa: DefaultClutter
				{
					model = "a3\plants_f\clutter\c_Grass_TuftDry.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 0.8 * KH_GRASS_CUT_COEF;
					scaleMax = 1.8 * KH_GRASS_CUT_COEF;
				};
				class ruha_FernAutumnTall_clutter: DefaultClutter
				{
					model = "ca\plants2\clutter\c_fernTall.p3d";
					affectedByWind = 0.15;
					scaleMin = 0.7;
					scaleMax = 1.2;
					swLighting = 1;
				};
				class ruha_RaspBerry_clutter: DefaultClutter
				{
					model = "ca\plants2\clutter\c_raspBerry.p3d";
					affectedByWind = 0;
					swLighting = 1;
					scaleMin = 0.5;
					scaleMax = 1;
				};
				class ruha_suokukka1_Clutter: DefaultClutter
				{
					model = "CA\plants_e\clutter\c_papaver_06_ep1.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_suokukka2_Clutter: DefaultClutter
				{
					model = "CA\plants\bodlak_group.p3d";
					affectedByWind = 0.6;
					swLighting = 1;
					scaleMin = 0.7;
					scaleMax = 1;
				};
				class ruha_suokukka3_clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\volcano\c_volcano_grassmix.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 1;
					scaleMax = 1.5;
				};
				class ruha_suokukka4_clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\grass\c_grass_tropic.p3d";
					affectedByWind = 1;
					swLighting = 1;
					scaleMin = 1;
					scaleMax = 1.5;
				};
				class ruha_suokukka5_clutter: DefaultClutter
				{
					model = "a3\vegetation_f_exp\clutter\forest\c_forest_fern.p3d";
					affectedByWind = 1;
					swLighting = 0;
					scaleMin = 1;
					scaleMax = 1.5;
				};
			};
		};
};
