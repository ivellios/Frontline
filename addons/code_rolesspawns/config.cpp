#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 0;
            dependencies[] = {"Main"};
            path = "pr\frl\addons\code_rolesspawns\functions";

            class canSpawn;
            class clientInit;
            class getRespawnTime;
            class init;
            class postInit;

            #include "functions\interface\module.hpp"

            class Roles {
                class clientInitRoles;
                class roleAssign;
                class roleCanUse;
                class roleEquip;
                class roleEquipCrewman;
                class roleEquipData;
                class roleHasAbility;
                class roleInfo;
                class roleRearm;
                class rolesParse;

                class Limitations {
                    class clientInitLimitations;
                    class validateClothing;
                };
            };

            class Sides {
                class clientInitSides;
                class serverInitSides;
                class sideAssign;
                class sideAutoAssign;
                class sideCanSwitch;
                class sideSavePlayers;
                class sideSwitch;
            };

            #include "functions\spawnpoints\module.hpp"

            class Squads {
                class clientInitLeader;
                class squadCreate;
                class squadNextID;
                class squadJoin;
                class squadKickMember;
                class squadParse;
                class squadTypeAvailable;
                class Invite {
                    class clientInitSquadInvite;
                    class squadInvite;
                };
            };
        };
    };
};
