/*
    Function:       FRL_Rolesspawns_fnc_postInit
    Author:         Adanteh
    Description:    Does postinit things
*/
#include "macros.hpp"

[QGVAR(postInit)] call MFUNC(persistentEvent);
