/*
    Function:       FRL_Rolesspawns_fnc_getRespawnTime
    Author:         Adanteh
    Description:    Gets respawn time for player
*/
#include "macros.hpp"
#ifdef DEBUGFULL
	if (true) exitWith { 10 };
#endif

if (missionNamespace getVariable [QGVAR(debugSpawn), false]) exitWith { 5 };
if (missionNamespace getVariable [QGVAR(firstRespawn), true]) exitWith {
	GVAR(firstRespawn) = false;
	private _timeSinceStart = (serverTime - (missionNamespace getVariable [QMVAR(missionStartTime), serverTime])) max 0;
	private _respawnTime = (([QGVAR(RespawnSettings_battleprepDuration), 0] call MFUNC(cfgSetting)) - _timeSinceStart) max 5;
	_respawnTime
};

private _respawnTime = ([QGVAR(RespawnSettings_respawnCountdown), 0] call MFUNC(cfgSetting));
private _incappedTime = missionNamespace getVariable [QEGVAR(Medical,timeIncapped), 0];
_respawnTime = (_respawnTime - _incappedTime) max ([QGVAR(RespawnSettings_minimumRespawnTime), 10] call MFUNC(cfgSetting));
_respawnTime
