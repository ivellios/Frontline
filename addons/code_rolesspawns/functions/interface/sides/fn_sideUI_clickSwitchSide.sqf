/*
    Function:       FRL_Rolesspawns_fnc_clickSwitchSide
    Author:         Adanteh
    Description:    Called when you click team switch button (Will close any open UI and reset your spawn timer)
*/
#include "macros.hpp"

call FUNC(sideSwitch);

// -- If half-size squad screen is open close that, but not if the full is open
if !(isNull (uiNamespace getVariable [QGVAR(squadDisplay), displayNull])) then {
    if (isNull (uiNamespace getVariable [QGVAR(respawnDisplay), displayNull])) then {
        GVAR(firstRespawn) = true; // -- Give low spawn timer
        (uiNamespace getVariable [QGVAR(squadDisplay), displayNull]) closeDisplay 1;
    };
};
// -- If respawn screen wasn't already open, open it now (Switch side while alive)
[{
    if (isNull (uiNamespace getVariable [QGVAR(respawnDisplay), displayNull])) then {
        (findDisplay 46) createDisplay QSVAR(RespawnScreen);
    };
}] call CFUNC(execNextFrame);
