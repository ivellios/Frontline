/*
    Function:       FRL_Rolesspawns_fnc_clientInitTeamUI
    Author:         Adanteh
    Description:    Inits the topleft section of respawn screen, showing players, tickets and flags
*/
#include "macros.hpp"


[QSVAR(sideUI_onLoad), {
    (_this select 0) params ["_control"];
    private _display = ctrlParent _control;
    uiNamespace setVariable [QGVAR(sideInfoDisplay), _display];

    [_display] call FUNC(sideUI_onLoad);
}] call CFUNC(addEventHandler);


// -- Update flag of your own team
[QSVAR(sideUI_update), { _this call FUNC(sideUI_updateSideInfo) }] call CFUNC(addEventHandler);


// -- Update ticket info
[QSVAR(sideUI_ticketUpdate), { _this call FUNC(sideUI_ticketInfo) }] call CFUNC(addEventHandler);


// -- Clicking SWITCH TEAM button
[QSVAR(sideUI_buttonClick), { _this call FUNC(sideUI_clickSwitchSide) }] call CFUNC(addEventHandler);


// -- Call ticket info change when tickets change
["ticketsChanged", { [QSVAR(sideUI_ticketUpdate)] call CFUNC(localEvent) }] call CFUNC(addEventHandler);
