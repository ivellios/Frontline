/*
    Function:       FRL_Rolessspawns_fnc_updateSideInfo
    Author:         Adanteh
    Description:    Updates the side info (Which team you are on + Flag)
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(sideInfoDisplay), displayNull];
if (isNull _display) exitWith {};

// Update the flag and text
(_display displayCtrl 102) ctrlSetText ([playerSide, "flag"] call MFUNC(getSideData));
(_display displayCtrl 103) ctrlSetText ([playerSide, "name"] call MFUNC(getSideData));
