/*
    Function:       FRL_Rolesspawns_fnc_sideUI_ticketInfo
    Author:         Adanteh
    Description:    Updates the ticket info in side UI
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(sideInfoDisplay), displayNull];
if (isNull _display) exitWith {};

{
    (_display displayCtrl (601 + _forEachIndex)) ctrlSetText ([_x, 'flag'] call MFUNC(getSideData));
    (_display displayCtrl (603 + _forEachIndex)) ctrlSetText ([_x, 'name'] call MFUNC(getSideData));

    private _tickets = str floor ([_x] call EFUNC(tickets,getSideTickets));
    private _pointsNeeded = missionNamespace getVariable [QEGVAR(tickets,ticketsToWin), -1];
    if (_pointsNeeded != -1) then {
        _tickets = format ["%1 / %2", _tickets, _pointsNeeded];
    };
    (_display displayCtrl (605 + _forEachIndex)) ctrlSetText _tickets;
} forEach (call MFUNC(getSides));
