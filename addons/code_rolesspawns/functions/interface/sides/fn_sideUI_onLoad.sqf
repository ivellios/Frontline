/*
    Function:       FRL_Rolesspawns_fnc_sideUI_onLoad
    Author:         Adanteh
    Description:    Loads the 'side' section of respawn screen, show ticket info and which team you are on
*/
#include "macros.hpp"

params ["_display"];
[{
    params ["_display"];

    [QSVAR(sideUI_update)] call CFUNC(localEvent); // -- Flag indicating your own team
    [QSVAR(sideUI_ticketUpdate)] call CFUNC(localEvent); // -- Tickets info for all teams

    // -- Update in loop (Don't have a global team switch/disconnect event) -- //
    [{
        private _display = uiNamespace getVariable [QGVAR(sideInfoDisplay), displayNull];
        if (isNull _display) exitWith {
            [(_this select 1)] call MFUNC(removePerFrameHandler);
        };

        private _allPlayers = +allPlayers;

        {
            private _side = _x;
            private _count = { side group _x == _side; } count _allPlayers;
            private _text = format ["%1 Player%2", _count, ["s", ""] select (_count == 1)];
            private _aiCount = missionNamespace getVariable [QSVAR(aiPerSide), 0];
            if (_aiCount > 0) then {
                _text = format ["%1 (+ %2 AI)", _text, _aiCount];
            };
            (_display displayCtrl (607 + _forEachIndex)) ctrlSetText _text;
        } forEach (call MFUNC(getSides));
     }, 2] call MFUNC(addPerFramehandler);
}, _display] call CFUNC(execNextFrame);
