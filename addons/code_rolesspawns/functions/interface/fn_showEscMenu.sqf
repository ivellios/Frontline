/*
    Function:       FRL_Rolesspawns_fnc_showEscMenu
    Author:         Adanteh
    Description:    Creates the esc interrupt menu
*/
#include "macros.hpp"

params ["_display", "_dikCode"];

if (_dikCode != 1) exitWith {false};

for "_i" from 1 to 9 do {
    private _controlGroup = _display displayCtrl (_i * 100);
    _controlGroup ctrlShow false;
};

createDialog (["RscDisplayInterrupt", "RscDisplayMPInterrupt"] select isMultiplayer);
private _dialog = findDisplay 49;
private _keepEnabled = [
    //122, // Field manual
    //101 // Options
];
// -- Script for those buttons doesn't work, so just keep them disabled for now i guess

for "_i" from 100 to 2000 do {
    if !(_i in _keepEnabled) then {
        (_dialog displayCtrl _i) ctrlEnable false;
        (_dialog displayCtrl _i) ctrlSetTooltip "";
    };
};


private _control = _dialog displayCtrl 104;
_control ctrlSetText "ABORT";
_control ctrlEnable true;
_control ctrlSetEventHandler ["buttonClick", {
    (uiNamespace getVariable [QGVAR(respawnDisplay), displayNull]) closeDisplay 2;
    closeDialog 0;
    failMission "LOSER";
} call MFUNC(codeToString)];

[{
    params ["_display"];

    for "_i" from 1 to 9 do {
        private _controlGroup = _display displayCtrl (_i * 100);
        _controlGroup ctrlShow true;
    };
}, {!dialog}, _display] call CFUNC(waitUntil);

true
