/*
    Function:       FRL_Rolesspawns_fnc_roleUI_addListN
    Author:         Adanteh
    Description:    Updates the ListNBox
*/
#include "macros.hpp"

params ["_control", "_lnbData", "_selectedValue"];

if (isNil "_selectedValue") then {
    private _selectedEntry = lnbCurSelRow _control;
    _selectedValue = [[_control, [_selectedEntry, 0]] call CFUNC(lnbLoad), nil] select (_selectedEntry == -1);
};

private _addedData = [];
lnbClear _control;
{
    _x params ["_textRows", "_data", ["_icon", "##"], ["_color", [1, 1, 1, 1]], ["_tooltip", ""], ["_enabled", true]];

    private _rowNumber = _control lnbAddRow _textRows;
    [_control, [_rowNumber, 0], _data] call CFUNC(lnbSave);
    _addedData pushBack _data;

    if !(_icon isEqualTo "##") then {
        _control lnbSetPicture [[_rowNumber, 0], _icon];
    };

    if (_data isEqualTo _selectedValue) then {
        _control lnbSetCurSelRow _rowNumber;
    };

    _control lnbSetColor [[_rowNumber, 0], _color];
    if (_tooltip != "") then {
        _control lbSetTooltip [_rowNumber * 2, format ["%1 (%2)", _tooltip, _rowNumber]];
    } else {
        _control lbSetTooltip [_rowNumber * 2, ""];
    };

    _control lbSetPictureColor [_rowNumber * 2, _color];
    _control lbSetPictureColorSelected [_rowNumber * 2, _color];
} forEach _lnbData;


if ((lnbSize _control select 0) == 0) then {
    _control lnbSetCurSelRow -1;
    _selectedValue = nil;
} else {
    if (isNil "_selectedValue" || {!(_selectedValue in _addedData)}) then {
        _control lnbSetCurSelRow 0;
        _selectedValue = [_control, [0, 0]] call CFUNC(lnbLoad);
    };
};

_selectedValue
