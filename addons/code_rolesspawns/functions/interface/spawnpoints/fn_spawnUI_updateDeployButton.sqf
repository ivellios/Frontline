/*
    Function:       FRL_Rolesspawns_fnc_spawnUI_updateDeployButton
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(deploymentDisplay), displayNull];
if (isNull _display) exitWith {};

private _control = _display displayCtrl 404;

// Calculate the respawn timer if necessary
if (!(alive Clib_Player) || (Clib_Player getVariable [QSVAR(tempUnit), false])) then {
    // Disable the button and start the timer
    _control ctrlEnable false;
    private _respawnTime = diag_tickTime + (call FUNC(getRespawnTime));
    [{
        params ["_params", "_id"];
        _params params ["_control", "_respawnTime"];

        // If the display has closed exit the PFH
        if (isNull GVAR(deploymentDisplay)) exitWith {
            _id call MFUNC(removePerFrameHandler);
        };

        // If the timer is up enabled deploying
        if (diag_tickTime >= _respawnTime) exitWith {
            _control ctrlSetText " DEPLOY";
            _control ctrlEnable true;

            _id call MFUNC(removePerFrameHandler);
        };

        // Update the text on the button
        private _time = _respawnTime - diag_tickTime;
        _control ctrlSetText format [" %1.%2s", floor _time, floor ((_time % 1) * 10)];
    }, 0.1, [_control, _respawnTime]] call MFUNC(addPerFramehandler);
} else {
    _control ctrlSetText "Close";
};
