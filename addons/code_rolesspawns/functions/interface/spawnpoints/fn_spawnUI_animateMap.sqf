/*
    Function:       FRL_RolesSpawns_fnc_animateMap
    Author:         Adanteh
    Description:    Focussed the map on newly selected spawnpoint
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(deploymentDisplay), displayNull];
if (isNull _display) exitWith {};

// Get the selected value
private _control = _display displayCtrl 403;
private _selectedEntry = lnbCurSelRow _control;
if (_selectedEntry == -1) exitWith {};
private _spawnpoint = [_control, [_selectedEntry, 0]] call CFUNC(lnbLoad);

private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
private _vehicle = _spawnpoint getVariable ["vehicle", objNull];
if !(isNull _vehicle) then {
    _position = getPosWorld _vehicle;
};
preloadCamera _position;

// Animate the map
private _controlMap = _display displayCtrl 800;
private _mapScale = (ctrlMapScale _controlMap) min 0.15;
_controlMap ctrlMapAnimAdd [0.5, _mapScale, _position]; // Dialog syntax can not be used
ctrlMapAnimCommit _controlMap;
