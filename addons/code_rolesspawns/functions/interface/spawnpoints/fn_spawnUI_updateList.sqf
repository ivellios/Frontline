/*
    Function:       FRL_Rolesspawns_fnc_spawnUI_updateList
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(deploymentDisplay), displayNull];
if (isNull _display) exitWith {};


private _lnbData = [];
private _spawnpoints = [false] call FUNC(spawnGetAvailable);
{
    private _spawnpoint = _x;
    private _aiSpawnOnly = _spawnpoint getVariable ["aiSpawnOnly", false];
    if !(_aiSpawnOnly) then {
        private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
        private _name = _spawnpoint getVariable ["name", sideUnknown];
        private _type = _spawnpoint getVariable ["type", sideUnknown];
        private _position = _spawnpoint getVariable ["position", sideUnknown];
        private _icon = _spawnpoint getVariable ["icon", sideUnknown];

        private _listcode = [_type, "list_entry"] call FUNC(spawnTypeData);
        private _listEntry = [_spawnpoint] call _listCode;
        if !(_listentry isEqualTo "##") then {
            _lnbData pushBack [[_listEntry], _spawnpoint, _icon];
        };
    };
} forEach _spawnpoints;

GVAR(listReselect) = GVAR(listReselect) + 1;
[_display displayCtrl 403, _lnbData] call FUNC(UI_updateList); // This may trigger an lbSelChanged event
