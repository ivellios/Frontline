/*
    Function:       FRL_Rolesspawns_fnc_spawnUI_onLoad
    Author:         Adanteh
    Description:    When the spawnlist section of respawn UI is loaded
*/
#include "macros.hpp"

(_this select 0) params ["_control"];
private _display = ctrlParent _control;
uiNamespace setVariable [QGVAR(deploymentDisplay), _display];

[{
    params ["_display"];
    [QSVAR(DeployScreen_buttonUpdate)] call CFUNC(localEvent);
    [QSVAR(DeployScreen_update)] call CFUNC(localEvent);

    [{
        (_this select 0) params ["_display"];
        if (isNull _display) exitWith {
            [_this select 1] call MFUNC(removePerFrameHandler);
        };

        QSVAR(DeployScreen_update) call CFUNC(localEvent);
    }, 1, [_display]] call MFUNC(addPerFramehandler);

}, _display] call CFUNC(execNextFrame);
