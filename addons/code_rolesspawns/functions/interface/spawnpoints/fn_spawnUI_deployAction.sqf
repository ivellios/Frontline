/*---------------------------------------------------------------------------
    This is a modified version of AAW RespawnUI_fnc_clientInitDeployment, July 2nd 2016 version
    Released under APL license by the AAW Team.

    Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
    Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
---------------------------------------------------------------------------*/
/*
    Function:       FRL_Rolesspawns_fnc_deployAction
    Author:         Adanteh
    Description:    Action connected to pressing the deploy button
*/
#include "macros.hpp"

private _deploymentDisplay = uiNamespace getVariable [QGVAR(deploymentDisplay), displayNull];
private _roleDisplay = uiNamespace getVariable [QGVAR(roleDisplay), displayNull];
if (isNull _deploymentDisplay) exitWith {};

// -- Spawning mutex (This will make sure players are spawned in one by one. Needed if conditions can change, based on someone else spawning in (Tickets))
[{
    private _returnMessage = "";
    private _spawnpointSelected = objNull;
    private _redeploy = false;
    private _canSpawn = [false] call FUNC(canSpawn);
    if (isNil "_canSpawn") then {
        _returnMessage = "Error occured";
        _canSpawn = false;
    };
    
    if !(_canSpawn) exitWith {
        [_returnMessage] call MFUNC(notificationShow);
        false
    };

    [_spawnpointSelected, _redeploy] call FUNC(spawnDeploy);

    (uiNamespace getVariable [QGVAR(deploymentDisplay), displayNull]) closeDisplay 1;
}, [], "respawn"] call CFUNC(mutex);
