 /*---------------------------------------------------------------------------
     This is a modified version of AAW RespawnUI, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"




GVAR(listReselect) = 0;

[QSVAR(DeploymentScreen_onLoad), { _this call FUNC(spawnUI_onLoad) }] call CFUNC(addEventHandler);


// -- Activate/deactive the deploy button and adjust the time on it
[QSVAR(DeployScreen_buttonUpdate), { _this call FUNC(spawnUI_updateDeployButton) }] call CFUNC(addEventHandler);

// -- Do stuff
[QSVAR(DeployScreen_update), { _this call FUNC(spawnUI_updateList) }] call CFUNC(addEventHandler);

// -- Do stuff
[QSVAR(DeployScreen_buttonAction), { _this call FUNC(spawnUI_deployAction) }] call CFUNC(addEventHandler);


[QSVAR(spawnsChanged), {
    [QSVAR(DeployScreen_update)] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


// -- Stop refocussing on the same spawnpoint -- //
[QSVAR(DeployScreen_listOnLBSelChanged), {
    // -- Bypass scripted list select doing the animate on map -- //
    if (GVAR(listReselect) > 1) then {
        GVAR(listReselect) = 1;
    } else {
        [QSVAR(DeployScreen_animateMap)] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);


[QSVAR(DeployScreen_animateMap), { _this call FUNC(spawnUI_animateMap) }] call CFUNC(addEventHandler);
