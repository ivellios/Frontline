class Interface {
    class clientInitUI;
    class clientInitUIEvents;
    class showEscMenu;
    class UI_updateList;

    class Roles {
        class clientInitRolesUI;
        class roleUI_changePreset;
        class roleUI_selChanged;
        class roleUI_showPreset;
        class roleUI_updateRoleList;
    };

    class Sides {
        class clientInitSideUI;
        class sideUI_clickSwitchSide;
        class sideUI_onLoad;
        class sideUI_ticketInfo;
        class sideUI_updateSideInfo;
    };

    class Spawnpoints {
        class clientInitSpawnUI;
        class spawnUI_animateMap;
        class spawnUI_deployAction;
        class spawnUI_onLoad;
        class spawnUI_updateDeployButton;
        class spawnUI_updateList;
    };

    class Squads {
        class clientInitSquadUI;
        class squadUI_listSquadMembers;
        class squadUI_listSquads;
        class squadUI_listSquadTypes;

        class Buttons {
            class clientInitSquadButtons;
            class squadUI_clickSquadType;
            class squadUI_handleButton;
            class squadUI_updateButtons;
        };
    };
};
