/*
    Function:       FRL_Rolesspawns_fnc_squadUI_listSquadTypes
    Author:         Adanteh
    Description:    Lists the available squad types
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
if (isNull _display) exitWith { };

params [["_mode", "update"]];

private _exit = false;
private _ctrls = [];
if (toLower _mode in ["toggle", "close"]) then {
    private _createCtrl = _display displayCtrl 206;
    if (_mode == "close" || (ctrlText _createCtrl == "CANCEL")) exitWith {
        ctrlDelete (uiNamespace getVariable [QGVAR(squadSelector), controlNull]);
        _createCtrl ctrlSetText "CREATE";
        _exit = true;
    };

    _createCtrl ctrlSetText "CANCEL";
    private _yPos = 0;
    private _ctrlBase = _display ctrlCreate [QSVAR(SquadSelector), 600];

    private _groupTypes = +(GVAR(squadNamespace) getVariable ["frl_squads", []]);
    private _index = 0;
    private _sideID = [playerSide] call BIS_fnc_sideID;

    {
        private _groupSlug = _x;
        private _groupSide = GVAR(squadNamespace) getVariable (_groupSlug + "_side");
        if (_groupSide == _sideID) then {
            private _groupName = GVAR(squadNamespace) getVariable (_groupSlug + "_name");
            private _marker = GVAR(squadNamespace) getVariable (_groupSlug + "_marker");

            private _ctrl = _display ctrlCreate [QSVAR(squadTypeEntry), 6000 + (_index * 100), _ctrlBase];
            _ctrl setVariable ["class", _groupSlug];

            (_ctrl controlsGroupCtrl 6001) ctrlSetText _marker;
            (_ctrl controlsGroupCtrl 6002) ctrlSetStructuredText parseText _groupName;
            (_ctrl controlsGroupCtrl 6004) ctrlSetTooltip format ["%1\n\n%2", _groupName, "Number requirements are based on the smallest team (AI count half)"];
            (_ctrl controlsGroupCtrl 6004) buttonSetAction format ["['%1', ['%2']] call %3", QSVAR(squadUI_selectorClick), _groupSlug, QCFUNC(localEvent)];
            ctrlSetFocus (_ctrl controlsGroupCtrl 6004);

            _ctrl ctrlSetPosition [0, _yPos];
            _ctrl ctrlCommit 0;
            _yPos = _yPos + _SQUADSELECTOR_ROWHEIGHT;
            _ctrls pushBack _ctrl;
            _index = _index + 1;
        };

    } forEach _groupTypes;
    _yPos = _yPos + _SQUADSELECTOR_SPACING_Y;

    uiNamespace setVariable [QGVAR(squadSelector), _ctrlBase];
    uiNamespace setVariable [QGVAR(squadSelectorTypes), _ctrls];

    private _controlPos = ctrlPosition _createCtrl;
    private _controlParent = ctrlParentControlsGroup _createCtrl;
    while { !isNull _controlParent } do {
        private _parentPos = ctrlPosition _controlParent;
        _controlPos set [0, (_controlPos select 0) + (_parentPos select 0)];
        _controlPos set [1, (_controlPos select 1) + (_parentPos select 1)];
        _controlParent = ctrlParentControlsGroup _control;
    };

    _controlPos params ["_xOrigin", "_yOrigin", "", "_yOriginHeight"];

    // -- Animate open
    //([_createCtrl] call MFUNC(getCtrlPositionReal)) params ["_xOrigin", "_yOrigin", "", "_yOriginHeight"];
    _ctrlBase ctrlSetPosition [_xOrigin, (_yOrigin + _yOriginHeight), (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE + _SQUADSELECTOR_SPACING_X), 0];
    _ctrlBase ctrlCommit 0;
    (_ctrlBase controlsGroupCtrl 601) ctrlSetPosition [0, 0, (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE + _SQUADSELECTOR_SPACING_X), 0];
    (_ctrlBase controlsGroupCtrl 601) ctrlCommit 0;

    _ctrlBase ctrlSetPosition [_xOrigin, (_yOrigin + _yOriginHeight), (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE + _SQUADSELECTOR_SPACING_X), _yPos];
    _ctrlBase ctrlCommit 0.1;
    (_ctrlBase controlsGroupCtrl 601) ctrlSetPosition [0, 0, (_SQUADSELECTOR_WIDTH_CONDITION + _SQUADSELECTOR_WIDTH_TYPE + _SQUADSELECTOR_SPACING_X), _yPos];
    (_ctrlBase controlsGroupCtrl 601) ctrlCommit 0.1;

    ctrlSetFocus _ctrlBase;
} else {
    _ctrls = uiNamespace getVariable [QGVAR(squadSelectorTypes), []];
};

if (_exit) exitWith { [] };

// -- Update this. This is mainly the color highlighting of conditions, the rest of info won't change
private _playersOnTeam = [false] call MFUNC(getSideSmallest);
{
    private _ctrl = _x;
    private _groupSlug = _ctrl getVariable "class";
    private _conditionText = "<t size='0.8'>";
    private _maxSize = GVAR(squadNamespace) getVariable (_groupSlug + "_maxSize");
    if (_maxSize > 0) then {
        _conditionText = format ["%1Max Members: %2<br/>", _conditionText, _maxSize];
    } else {
        _conditionText = format ["%1Max Members: %2<br/>", _conditionText, "∞"];
    };

    // -- This shows which number the next squad of this types becomes available
    private _requiredPlayers = GVAR(squadNamespace) getVariable (_groupSlug + "_availableAt");
    if (_requiredPlayers > 0) then {
        private _color = ["#ff4040", "#ffffff"] select (_playersOnTeam >= _requiredPlayers);
        _conditionText = format ["%1Available At: <t color='%3'>%2 Players</t><br/>", _conditionText, _requiredPlayers, _color];
    };

    private _requiredPlayersArray = GVAR(squadNamespace) getVariable (_groupSlug + "_availableAtArray");
    if !(_requiredPlayersArray isEqualTo []) then {
        private _arrayText = "";
        {
            // -- Add comma with exception of the first entry
            if (_forEachIndex != 0) then {
                _arrayText = _arrayText + ", ";
            };
            private _color = ["#ff4040", "#ffffff"] select (_playersOnTeam >= _x);
            _arrayText = format ["%1<t color='%3'>%2</t>", _arrayText, _x, _color];
        } forEach _requiredPlayersArray;
        _conditionText = format ["%1Available At: %2 Players<br/>", _conditionText, _arrayText];
    };

    (_ctrl controlsGroupCtrl 6003) ctrlSetStructuredText parseText (_conditionText + "</t>");
} forEach _ctrls;
