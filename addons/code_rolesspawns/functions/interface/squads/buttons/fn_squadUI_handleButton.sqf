/*
    Function:       FRL_Rolesspawns_fnc_squadUI_handleButton
    Author:         Adanteh
    Description:    Handles one of the squad buttons (Lock, unlock, kick etc)
*/
#include "macros.hpp"

params [["_mode", "start"], ["_args", []]];

private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
if (isNull _display) exitWith {};

private _return = true;
switch (toLower _mode) do {

    // --
    // -- Join/leave button
    // -- Joins the selected squad (If locked, send a request), If in the squad leave it
    case "join": {
        private _control = _display displayCtrl 207;
        private _selectedSquad = [_control, [lnbCurSelRow _control, 0]] call CFUNC(lnbLoad);

        if (_selectedSquad == group Clib_Player) then {
            [] call MFUNC(leaveSquad);
        } else {
            private _isLocked = (_selectedSquad getVariable [QGVAR(Locked), false]);
            if !(_isLocked) then {
                [_selectedSquad] call FUNC(squadJoin);
            } else {
                [_selectedSquad] call FUNC(squadJoinRequest);
            };
        };
    };


    // --
    // -- Kick button
    // -- Kicks the selected unit from the squad
    case "kick": {
        private _control = _display displayCtrl 210;
        private _selectedSquadMember = [_control, [lnbCurSelRow _control, 0]] call CFUNC(lnbLoad);
        [_selectedSquadMember] call FUNC(squadKickMember);
    };


    // --
    // -- Promote button
    // -- Makes the selected unit the squad leader
    case "promote": {
        private _control = _display displayCtrl 210;
        private _selectedSquadMember = [_control, [lnbCurSelRow _control, 0]] call CFUNC(lnbLoad);
        [_selectedSquadMember] call MFUNC(selectLeader);
    };


    // --
    // -- Invite button
    // -- Invites selected unit to your squad (Will give him an F5/F6 option)
    case "invite": {
        private _control = _display displayCtrl 210;
        private _selectedSquadMember = [_control, [lnbCurSelRow _control, 0]] call CFUNC(lnbLoad);
        [_selectedSquadMember, group Clib_Player] call FUNC(squadInvite);
    };

    // --
    // -- Invite button
    // -- Invites the selecte
    case "assume": {
        private _group = group Clib_Player;
        if (_group getVariable [QGVAR(slAssume), false]) then {
            private _groupType = _group getVariable [QMVAR(gType), ""];

            [Clib_Player] call MFUNC(selectLeader);
            _group setVariable [QGVAR(slAssume), nil, true];

            // -- Assign the first kit with 'Leader' ability in the squad, to the player
            private _roleToAssign = "";
            {
                if ([_x select 0, "leader"] call FUNC(roleHasAbility)) exitWith {
                    _roleToAssign = _x select 0;
                };
            } forEach (GVAR(squadNamespace) getVariable [(format ["%1_roles", _groupType]), []]);

            [Clib_Player, _roleToAssign] call FUNC(roleAssign);
        };
    };
};

_return;
