/*
    Function:       FRL_Rolesspawns_fnc_squadUI_clickSquadType
    Author:         Adanteh
    Description:    Handles behaviour when you click an entry in the squad list.
                    Will do nothing if you select not-available squad, will create a new squad if available
*/
#include "macros.hpp"

params ["_squadType"];
if (_squadType == "") exitWith {
    false
};

// -- Get the squad name
private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
private _control = _display displayCtrl 204;
private _squadName = ctrlText _control;
if (count _squadName > 14) then {
    _squadName = _squadName select [0, 14];
};

private _canCreateSquadMessage = "";
if !([_squadType] call FUNC(squadTypeAvailable)) exitWith {
    if (_canCreateSquadMessage != "") then {
        ["showNotification", [_canCreateSquadMessage, "nope"]] call CFUNC(localEvent);
    };
    false
};

ctrlDelete (uiNamespace getVariable [QGVAR(squadSelector), controlNull]);
(_display displayCtrl 206) ctrlSetText "CREATE";
["mutex", [_squadName, _squadType, Clib_Player]] call FUNC(squadCreate);

true;
