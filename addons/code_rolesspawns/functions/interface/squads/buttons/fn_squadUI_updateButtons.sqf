/*
    Function:       FRL_Rolesspawns_fnc_updateButtons
    Author:         Adanteh
    Description:    Updates buttons to lock, kick, etc
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
if (isNull _display) exitWith {};

// Get the controls
private _controlSquadMemberList = _display displayCtrl 210;
private _controlKickButton = _display displayCtrl 212;
private _controlPromoteButton = _display displayCtrl 213;
private _controlLockButton = _display displayCtrl 214;

private _groupPlayer = group Clib_Player;
private _isLeader = (leader _groupPlayer == Clib_Player) && ({groupID _groupPlayer in MVAR(squadIds)});
private _canAssumeSL = _groupPlayer getVariable [QGVAR(slAssume), false];
if (_canAssumeSL) then {
    _controlLockButton ctrlSetText "CLAIM SL";
    _controlLockButton ctrlSetTooltip "Take leadership and equip SL Kit";
    _controlLockButton ctrlShow true;
    _controlLockButton buttonSetAction format ["'%1' call %2;", QSVAR(RespawnScreen_AssumeBtn_onButtonClick), QCFUNC(localEvent)];
} else {
    _controlLockButton ctrlSetText (["LOCK", "UNLOCK"] select (_groupPlayer getVariable [QGVAR(Locked), false]));
    _controlLockButton ctrlSetTooltip "";
    _controlLockButton ctrlShow _isLeader; // -- Show lock always when you're SL
    _controlLockButton buttonSetAction format ["'%1' call %2;", QSVAR(RespawnScreen_LockBtn_onButtonClick), QCFUNC(localEvent)];
};

private _selectedEntry = lnbCurSelRow _controlSquadMemberList;
if (_selectedEntry == -1) then {
    _controlKickButton ctrlShow false;
    _controlPromoteButton ctrlShow false;
} else {
    private _selectedGroupMember = [_controlSquadMemberList, [_selectedEntry, 0]] call CFUNC(lnbLoad);
    private _isSameGroup = (group _selectedGroupMember == _groupPlayer);

    _controlKickButton ctrlShow (_isLeader && (_selectedGroupMember != Clib_Player));
    _controlPromoteButton ctrlShow (_isLeader && _isSameGroup && (_selectedGroupMember != Clib_Player)); // -- Show promote when selecting someone in your own group

    if (_isLeader) then {
        if (_isSameGroup) then { // -- Show kick button if SL and selecting someone in same grup
            _controlKickButton ctrlSetText "KICK";
            _controlKickButton buttonSetAction format ["'%1' call %2;", QSVAR(RespawnScreen_KickBtn_onButtonClick), QCFUNC(localEvent)];
        } else { // -- Show invite button if SL and selecting someone in a different squad -- //
            _controlKickButton ctrlSetText "INVITE";
            _controlKickButton buttonSetAction format ["'%1' call %2;", QSVAR(RespawnScreen_InviteBtn_onButtonClick), QCFUNC(localEvent)];
        };
    };
};
