/*
    Function:       FRL_Rolesspawns_fnc_clientInitSquadButtons
    Author:         Adanteh
    Description:    Inits all the functions and events for the squad-buttons like kick, join, etc
*/
#include "macros.hpp"

[QSVAR(squadUI_buttonsUpdate), { _this call FUNC(squadUI_updateButtons) }] call CFUNC(addEventHandler);
[QSVAR(RespawnScreen_JoinLeaveBtn_onButtonClick), { ["join"] call FUNC(squadUI_handleButton) }] call CFUNC(addEventHandler);
[QSVAR(RespawnScreen_KickBtn_onButtonClick), { ["kick"] call FUNC(squadUI_handleButton) }] call CFUNC(addEventHandler);
[QSVAR(RespawnScreen_PromoteBtn_onButtonClick), { ["promote"] call FUNC(squadUI_handleButton) }] call CFUNC(addEventHandler);
[QSVAR(RespawnScreen_InviteBtn_onButtonClick), { ["invite"] call FUNC(squadUI_handleButton) }] call CFUNC(addEventHandler);
[QSVAR(RespawnScreen_AssumeBtn_onButtonClick), { ["assume"] call FUNC(squadUI_handleButton) }] call CFUNC(addEventHandler);


[QSVAR(RespawnScreen_LockBtn_onButtonClick), {
    private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
    if (isNull _display) exitWith {};

    private _isSquadLocked = ((group Clib_Player) getVariable [QGVAR(Locked), false]);
    (group Clib_Player) setVariable [QGVAR(Locked), !_isSquadLocked, true];

    [QSVAR(squadUI_buttonsUpdate)] call CFUNC(localEvent);
    [QSVAR(squadUI_listUpdate), playerSide] call CFUNC(targetEvent);
}] call CFUNC(addEventHandler);
