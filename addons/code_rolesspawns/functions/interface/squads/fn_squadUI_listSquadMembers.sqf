/*
    Function:       FRL_Rolesspawns_fnc_squadUI_listSquadMembmers
    Author:         Adanteh
    Description:    Lists the units inside a squad, plus their roles
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
if (isNull _display) exitWith {};

// Get the controls
private _controlSquadList = _display displayCtrl 207;
private _controlSquadMemberListHeader = _display displayCtrl 209;
private _controlSquadMemberList = _display displayCtrl 210;
private _controlJoinLeaveButton = _display displayCtrl 211;

// Get the selected value
private _selectedEntry = lnbCurSelRow _controlSquadList;
if (_selectedEntry == -1) exitWith {
    _controlSquadMemberListHeader ctrlSetText "SELECT A SQUAD";
    lnbClear _controlSquadMemberList;
    _controlSquadMemberList lnbSetCurSelRow -1;
    _controlJoinLeaveButton ctrlShow false;
};

private _selectedSquad = [_controlSquadList, [_selectedEntry, 0]] call CFUNC(lnbLoad);

// HeadingSquadDetails
private _playerIndex = -1;
if (isNull _selectedSquad) then {
    _controlSquadMemberListHeader ctrlSetText toUpper "Unassigned";

    private _groupUnits = +GVAR(unassignedUnits);
    private _lnbData = _groupUnits apply {
        private _icon = [_x, "ICON"] call FUNC(roleInfo);
        [[[_x] call CFUNC(name)], _x, _icon]
    };
    _playerIndex = _groupUnits find Clib_Player;
    [_controlSquadMemberList, _lnbData] call FUNC(UI_updateList); // This may trigger an lbSelChanged event
} else {
    _controlSquadMemberListHeader ctrlSetText toUpper (groupId _selectedSquad);
    _controlJoinLeaveButton ctrlShow true;
    // SquadMemberList
    private _groupUnits = ([_selectedSquad] call CFUNC(groupPlayers));
    private _lnbData = _groupUnits apply {
        private _icon = [_x, "ICON"] call FUNC(roleInfo);
        [[[_x] call CFUNC(name)], _x, _icon]
    };
    _playerIndex = _groupUnits find Clib_Player;
    [_controlSquadMemberList, _lnbData] call FUNC(UI_updateList); // This may trigger an lbSelChanged event

    // JoinLeaveBtn
    if (_selectedSquad == group Clib_Player) then {
        _controlJoinLeaveButton ctrlSetText "LEAVE";
        _controlJoinLeaveButton ctrlSetTooltip "Leave the squad";
        _controlJoinLeaveButton ctrlEnable true;
    } else {
        private _groupType = _selectedSquad getVariable [QMVAR(gType), ""];
        private _groupSizeMax = GVAR(squadNamespace) getVariable [(format ["%1_maxSize", _groupType]), 0];
        private _canJoin = true;
        if (_groupSizeMax != -1) then {
            _canJoin = ((count ([_selectedSquad] call CFUNC(groupPlayers))) < _groupSizeMax);
        };

        // TODO Add squad lock button
        private _squadIsLocked = (_selectedSquad getVariable [QGVAR(Locked), false]);
        _controlJoinLeaveButton ctrlSetText (["JOIN", "REQUEST"] select _squadIsLocked);
        _controlJoinLeaveButton ctrlSetTooltip (["Join the group", "Request to join the group"] select _squadIsLocked);
        _controlJoinLeaveButton ctrlEnable _canJoin;
    };
};
if (_playerIndex != -1) then {
    _controlSquadMemberList lbSetPictureColor [_playerIndex, [0.77, 0.51, 0.08, 1]];
    _controlSquadMemberList lbSetPictureColorSelected [_playerIndex, [0.77, 0.51, 0.08, 1]];
    _controlSquadMemberList lnbSetColor [[_playerIndex, 0], [0.77, 0.51, 0.08, 1]];
};
