/*
    Function:       FRL_Rolesspawns_fnc_squadUI_listSquads
    Author:         Adanteh
    Description:    Lists the squads in the respawn UI
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
if (isNull _display) exitWith {};

private _ownGroupIndex = -1;

// Prepare the data for the lnb
private _lnbData = [];
private _unassignedGroups = [];
private _unassignedUnits = [];

{
    if (groupID _x in MVAR(squadIds)) then {
        private _groupCount = count ([_x] call CFUNC(groupPlayers));
        if (_groupCount == 0) exitWith { };
        private _groupId = groupId _x;
        if (_x == group Clib_Player) then {
            _ownGroupIndex = _forEachIndex;
        };

        private _squadDesignator = _groupId select [0, 1];
        private _description = _x getVariable [QMVAR(gName), str _x];
        private _groupType = _x getVariable [QMVAR(gType), ""];
        private _groupTypeName = GVAR(squadNamespace) getVariable [(format ["%1_name", _groupType]), ""];
        private _groupSizeMax = GVAR(squadNamespace) getVariable [(format ["%1_maxSize", _groupType]), 0];
        if (_groupSizeMax == -1) then { "∞" };

        if (_description == "") then {
            _description = _groupId;
        };

        private _squadLockIcon = [nil, (MEDIAPATH + "icons\locked.paa")] select (_x getVariable [QGVAR(Locked), false]);
        private _textEntry = format ["%1 / %2", _groupCount, _groupSizeMax];
        _lnbData pushBack [[_squadDesignator, _description, _groupTypeName, _textEntry], _x, _squadLockIcon];
    } else {
        _unassignedGroups pushBack _x;
        _unassignedUnits append (units _x select { isPlayer _x });
        if (_x == group Clib_Player) then {
            _ownGroupIndex = _forEachIndex;
        };
    };
} forEach (allGroups select {side _x == playerSide});

GVAR(unassignedUnits) = +_unassignedUnits;
// -- Combine all the unassigned groups under one -- //
if (count _unassignedGroups > 0 && (count _unassignedUnits > 0)) then {
    _lnbData pushBack [["Unassigned", " ", " ", str (count _unassignedUnits)], grpNull];
};

private _control = _display displayCtrl 207;
[_control, _lnbData] call FUNC(UI_updateList);

if (_ownGroupIndex != -1) then {
    for "_i" from 0 to 3 do {
        _control lnbSetColor [[_ownGroupIndex, _i], [0.77, 0.51, 0.08, 1]];
    };
};
