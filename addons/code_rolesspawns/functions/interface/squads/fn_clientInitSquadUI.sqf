 /*---------------------------------------------------------------------------
     This is a modified version of AAW RespawnUI, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/
 #include "macros.hpp"

["missionStarted", {
    ["Squad Screen", Clib_Player, 0, {isNull (uiNamespace getVariable [QGVAR(squadDisplay), displayNull])}, {
        (findDisplay 46) createDisplay QSVAR(SquadScreen);
    }, ["ignoredCanInteractConditions",["isNotInVehicle"], "showWindow", false, "unconscious", true]] call CFUNC(addAction);
}] call CFUNC(addEventHandler);

// Create Squad Description Limit
[QSVAR(RespawnScreen_SquadDescriptionInput_TextChanged), {
    private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
    if (isNull _display) exitWith {};

    private _control = _display displayCtrl 204;
    private _description = ctrlText _control;

    if (count _description > 14) then {
        _control ctrlSetBackgroundColor [0.77, 0.51, 0.08, 1];
        _control ctrlCommit 0;
        [{
            params ["_control"];

            _control ctrlSetBackgroundColor [0.4, 0.4, 0.4, 1];
            _control ctrlCommit 0;
        }, 1, _control] call CFUNC(wait);

        _control ctrlSetText (_description select [0, 14]);
    };
}] call CFUNC(addEventHandler);


// -- Squad type selector. This is frontline code
[QSVAR(squadUI_selectorUpdate), {
    private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
    if (isNull _display) exitWith { };
    ["update", _display] call FUNC(squadUI_listSquadTypes);
}] call CFUNC(addEventHandler);

[QSVAR(squadUI_selectorClick), {
    (_this select 0) call FUNC(squadUI_clickSquadType);
}] call CFUNC(addEventHandler);


// -- Close the squad selection if you press switch team button
[QSVAR(squadUI_createClick), { ["toggle"] call FUNC(squadUI_listSquadTypes); }] call CFUNC(addEventHandler);
[QSVAR(sideUI_buttonClick), { ["close"] call FUNC(squadUI_listSquadTypes); }] call CFUNC(addEventHandler);

// -- Show all groups
[QSVAR(squadUI_listUpdate), { _this call FUNC(squadUI_listSquads) }] call CFUNC(addEventHandler);
[QSVAR(squadUI_update), { _this call FUNC(squadUI_listSquadMembers)} ] call CFUNC(addEventHandler);


["leaderChanged", {
    private _display = uiNamespace getVariable [QGVAR(squadDisplay), displayNull];
    if (isNull _display) exitWith {};

    // Get the selected value
    private _control = _display displayCtrl 207;
    private _selectedEntry = lnbCurSelRow _control;
    if (_selectedEntry == -1) exitWith {};
    private _selectedSquad = [_control, [_selectedEntry, 0]] call CFUNC(lnbLoad);

    if (_selectedSquad == group Clib_Player) then {
        QSVAR(squadUI_buttonsUpdate) call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);
