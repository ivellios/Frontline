 /*---------------------------------------------------------------------------
     This is a modified version of AAW RespawnUI, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/
 #include "macros.hpp"

// When player dies show respawn UI
["Killed", {
    if (isMultiplayer) then {
        private _wait = 0;
        if (missionNamespace getVariable [QEGVAR(Medical,instaKill), false]) then {
            missionNamespace setVariable [QEGVAR(Medical,instaKill), false];
            _wait = 3;
        };
        [{
            [[-10000, -10000, 50], true] call MFUNC(Respawn);
            [{
                // Respawn screen may already open by user action
                if (!(isNull (uiNamespace getVariable [QGVAR(respawnDisplay), displayNull]))) exitWith {
                    [QSVAR(RespawnScreen_onLoad), GVAR(respawnDisplay)] call CFUNC(localEvent);
                };

                (findDisplay 46) createDisplay QSVAR(RespawnScreen);
            }] call CFUNC(execNextFrame);
        }, _wait] call CFUNC(wait);
    } else {
        endMission "LOSER";
    };
}] call CFUNC(addEventHandler);


// -- Loads a hint at the bottom middle of respawn screen
[QSVAR(RespawnScreen_showHint), {
    private _display = uiNamespace getVariable [QGVAR(respawnDisplay), displayNull];
    if (isNull _display) exitWith { };
    private _loadingHints = getarray (configFile >> "FRL" >> "loadingTexts");
    if (count _loadingHints > 0) then {
        (_display displayCtrl 500) ctrlSetFade 0;
        (_display displayCtrl 500) ctrlCommit 0;
        (_display displayCtrl 501) ctrlSetText (selectRandom _loadingHints);
    };
}] call CFUNC(addEventHandler);


// -- This is the main respawn screen, showing deployment points and everything
[QSVAR(RespawnScreen_onLoad), {
    (_this select 0) params ["_display"];
    uiNamespace setVariable [QGVAR(respawnDisplay), _display];
    openMap false;

    [QSVAR(RespawnScreen), true, 0.25, 1.4] call MFUNC(blurBackground);
    ['RscDisplayMPInterrupt', false] call MFUNC(blurBackground);

    [{
        params ["_display"];

        _display displayAddEventHandler ["KeyDown", FUNC(showEscMenu)];

        [_display displayCtrl 800] call CFUNC(registerMapControl); // Register the map for the marker system
        ["registerMapClick", [_display displayCtrl 800]] call CFUNC(localEvent);

        ["addKeybinds.display", [_display]] call CFUNC(localEvent); // Register for keybinds
        [QSVAR(RespawnScreen_showHint)] call CFUNC(localEvent);

        // -- Show a message indiciating playtimes
        #ifndef ISDEV
        private _shownPlayMessage = uiNamespace getVariable [QGVAR(shownPlayMessage), false];
        if !(_shownPlayMessage) then {
            if (count allPlayers <= 5) then {
                uiNamespace setVariable [QGVAR(shownPlayMessage), false];
                private _text = format [
                "Hi there, seems like you're joining an empty server<br /><br />If you want to play with more players please join one of our sesssions at:<br /><br />" +
                "<t size=1.25 color='#FDB535'>Sunday 19GMT Iron Front/RHS</t><br /><br />" +
                "Join our <a color='#FDB535' href='http://discord.frontline-mod.com'>Discord</a> for more information"];
                [_display, "Scheduled playtimes", _text, [
                    [],
                    [],
                    ["Ok", "Yeah yeah I get it, i'll be there"]]
                ] call MFUNC(popupDialog);
            };
        };
        #endif
    }, _display] call CFUNC(execNextFrame);
}] call CFUNC(addEventHandler);


// -- This is the 'small' squad screen only showing left half, used when you're alive
[QSVAR(SquadScreen_onLoad), {
    (_this select 0) params ["_control"];
    private _display = ctrlParent _control;
    uiNamespace setVariable [QGVAR(squadDisplay), _display];

    // The dialog needs one frame until access to controls is possible
    [{
        params ["_display"];
        [QSVAR(RespawnScreen_NewSquadDesignator_update)] call CFUNC(localEvent);
        [QSVAR(squadUI_listUpdate)] call CFUNC(localEvent);
        [QSVAR(squadUI_update)] call CFUNC(localEvent);\

        ctrlSetFocus ((_display displayCtrl 200) controlsGroupCtrl 204);
    }, _display] call CFUNC(execNextFrame);
}] call CFUNC(addEventHandler);



[QSVAR(RespawnScreen_onUnload), {
    [QSVAR(RespawnScreen), false, 0.25] call MFUNC(blurBackground);
}] call CFUNC(addEventHandler);

["endMission", {
    if !(isNull (uiNamespace getVariable [QGVAR(respawnDisplay), displayNull])) then {
        (uiNamespace getVariable [QGVAR(respawnDisplay), displayNull]) closeDisplay 2
    };
}] call CFUNC(addEventHandler);
