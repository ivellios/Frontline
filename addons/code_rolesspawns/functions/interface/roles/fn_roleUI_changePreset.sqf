/*
    Function:       FRL_Rolesspawns_fnc_roleUI_changePreset
    Author:         Adanteh
    Description:    Allows picking a different preset for this kit
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(roleDisplay), displayNull];
if (isNull _display) exitWith {};

params ["", "_variant"];

// -- Make sure this doesn't get run double when curselection is triggered script -- //
if ((uiNamespace getVariable [QSVAR(curSel2), ""]) isEqualTo _variant) exitWith {
    uiNamespace setVariable [QSVAR(curSel2), []];
};

// -- Get selected kit
private _control = _display displayCtrl 303;
private _selectedEntry = lnbCurSelRow _control;
if (_selectedEntry == -1) exitWith { };
private _role = [_control, [_selectedEntry, 0]] call CFUNC(lnbLoad);

// -- Removed the disabled tag from kit
private _isDisabled = (toLower _role find "_disabled");
if (_isDisabled != -1) then { _role = _role select [0, _isDisabled]; };

// -- Set selected variant as default for this kit (Don't copy array on purpose)
GVAR(roleNamespace) setVariable [_role + "VARIANT_CURR", _variant];
[_display, _role] call FUNC(roleUI_showPreset);
