/*
    Function:       FRL_rolesspawns_fnc_clienntInitRolesUI
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"


[QSVAR(RoleScreen_onLoad), {
    (_this select 0) params ["_control"];
    private _display = ctrlParent _control;
    uiNamespace setVariable [QGVAR(roleDisplay), _display];

    [{
        params ["_display"];
        QSVAR(roleUI_update) call CFUNC(localEvent);
    }, _display] call CFUNC(execNextFrame);
}] call CFUNC(addEventHandler);


// -- Show roles in squad
[QSVAR(roleUI_update), { _this call FUNC(roleUI_updateRoleList) }] call CFUNC(addEventHandler);


// -- Select kit if it's available
[QSVAR(RoleScreenList_onLBSelChanged), { _this call FUNC(roleUI_selChanged) }] call CFUNC(addEventHandler);


// -- Change preset data if we change it
[QSVAR(RoleScreen_presetChanged), { (_this select 0) call FUNC(roleUI_changePreset) }] call CFUNC(addEventHandler);
