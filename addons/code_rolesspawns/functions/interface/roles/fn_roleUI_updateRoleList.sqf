/*
    Function:       FRL_Rolesspawns_fnc_roleUI_updateRoleList
    Author:         Adanteh
    Description:    Updates the list of roles in role screen
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(roleDisplay), displayNull];
if (isNull _display) exitWith {};

// -- Unassign kit if conditions to use said kit are no longer matched -- //
if !([Clib_Player] call FUNC(roleCanUse)) then {
    Clib_Player setVariable [QSVAR(rkit), "", true];
};

// Prepare the data for the lnb
private _lnbData = [];
private _rolePlayer = Clib_Player getVariable [QSVAR(rkit), ""];
private _playerGroupType = (group Clib_Player) getVariable [QMVAR(gType), ""];
private _playerGroupUnits = units (group Clib_Player);

private _groupRoles = GVAR(squadNamespace) getVariable [(format ["%1_roles", _playerGroupType]), []];
{
    _x params ["_role", ["_requiredGroupMembers", 0]];

    private _unlimitedKit = (_requiredGroupMembers == -1);
    private _playersWithRole =  { (_x getVariable [QSVAR(rkit), ""] == _role) } count _playerGroupUnits;
    private _kitUsable = if (_unlimitedKit) then {
        true;
    } else {
        ((_playersWithRole == 0) && {(count _playerGroupUnits >= _requiredGroupMembers)} && { if ([_role, "leader"] call FUNC(roleHasAbility)) then { (leader Clib_Player isEqualTo Clib_Player) } else { true } });
    };

    private _uiIcon = [_role, "ICON"] call FUNC(roleInfo);
    private _displayName = [_role, "NAME"] call FUNC(roleInfo);

    private _color = [[1,1,1,1], [0.77, 0.51, 0.08, 1]] select (_role == _rolePlayer);
    if (_kitUsable) then {
        // -- Important kits like Light AT and Medic we want to highlight in green if their unoccupied
        if ([_role, "HIGHLIGHT"] call FUNC(roleInfo)) then { _color = [0.33, 0.84, 0.22, 1] }; //vec3(0.33, 0.84, 0.22)
        _lnbData pushBack [[_displayName, format ["%1 / %2", _playersWithRole, (["1", "∞"] select _unlimitedKit)]], _role, _uiIcon, _color, "", nil, _role];
    } else {
        if (_rolePlayer == _role) then {
            _lnbData pushBack [[_displayName, format ["%1 / %2", _playersWithRole, 1]], _role, _uiIcon, _color, "", nil];
        } else {
            if (_playersWithRole != 0) then {
                _lnbData pushBack [[_displayName, format ["%1 / %2", _playersWithRole, 1]], (_role + "_disabled"), _uiIcon, _color, "", nil];
            } else {
                _lnbData pushBack [[_displayName, format ["[%1+]", _requiredGroupMembers]], (_role + "_disabled"), _uiIcon, [1,1,1,0.35], "", nil];
            };
        };
    };

    nil;
} count _groupRoles;

// Update the lnb
[_display displayCtrl 303, _lnbData, _rolePlayer] call FUNC(UI_updateList); // This may trigger an lbSelChanged event
