/*
    Function:       FRL_Rolesspawns_fnc_roleUI_showPreset
    Author:         Adanteh
    Description:    Updates kit preset UI elements
*/
#include "macros.hpp"
#define _ATTACHMENTBG ["\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_muzzle_gs.paa", "\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_bipod_gs.paa", "\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_side_gs.paa", "\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_top_gs.paa"]

params ["_display", "_role"];

private _variantSelected = [_role, "VARIANT_CURR"] call FUNC(roleInfo);
private _variants = [_role, "VARIANTS"] call FUNC(roleInfo);
private _variant = _variants param [_variantSelected, []];

if (_variantSelected isEqualTo []) exitWith { };

private _magsCtrlVar = format ["frl_magCtrls"];
private _magsCtrls = [];

if (true) then {
    private _posY = PRESET_ITEM_SPACING_Y;
    private _variantItemCtrl = _display displayCtrl 3304;

    // -- Preset selection box
    // -- Hide variants selection if only 1 is available -- //
    private _variantsCtrl = _display displayCtrl 3303;
    if (count _variants <= 1) then {
        _variantsCtrl ctrlEnable false;
        _variantsCtrl ctrlShow false;
        _variantItemCtrl ctrlSetPosition [
            0,
            PY(3),
            PRESET_WIDTH,
            PRESET_HEIGHT - PY(3)
        ];
    } else {
        _variantsCtrl ctrlEnable true;
        _variantsCtrl ctrlShow true;
        lbClear _variantsCtrl;
        {
            _variantsCtrl lbAdd (_x select VARIANT_NAME);
        } forEach _variants;
        uiNamespace setVariable [QSVAR(curSel2), _variantSelected];

        _variantsCtrl lbSetCurSel _variantSelected;
        _variantItemCtrl ctrlSetPosition [
            0,
            PY(3) + PRESET_TAB_HEIGHT + PRESET_ITEM_SPACING_Y,
            PRESET_WIDTH,
            PRESET_HEIGHT - PY(3) - PRESET_TAB_HEIGHT - PRESET_ITEM_SPACING_Y
        ];
    };

    _variantItemCtrl ctrlCommit 0;

    {
        _x params ["_typeIDC", "_typeIndex"];
        private _itemGroupCtrl = _display displayCtrl _typeIDC;
        private _itemInfo = _variant param [_typeIndex, []];

        private _magsCtrl = _itemGroupCtrl controlsGroupCtrl 5106;
        // -- Clear out existing magazine entries before adding new -- //

        if !(_typeIndex isEqualTo VARIANT_BACKPACK) then {
            // -- Primary Weapon -- //
            private _weapon = _itemInfo param [0, ""];
            private _magazines = _itemInfo param [1, []];
            private _attachments = _itemInfo param [2, ["", "", "", ""]];

            if !(_weapon isEqualTo "") then {
                if !(ctrlEnabled _itemGroupCtrl) then { _itemGroupCtrl ctrlEnable true; };
                if !(ctrlShown _itemGroupCtrl) then { _itemGroupCtrl ctrlShow true; };

                private _itemCtrl = _itemGroupCtrl controlsGroupCtrl 5101;
                _itemCtrl ctrlSetText getText(configFile >> "CfgWeapons" >> _weapon >> "picture");
                _itemCtrl ctrlSetTooltip getText(configFile >> "CfgWeapons" >> _weapon >> "displayName");

                {
                    private _ctrl = _itemGroupCtrl controlsGroupCtrl (5102 + _forEachIndex);
                    if !(_x isEqualTo "") then {
                        _ctrl ctrlSetText getText(configFile >> "CfgWeapons" >> _x >> "picture");
                        _ctrl ctrlSetTooltip getText(configFile >> "CfgWeapons" >> _x >> "displayName");
                    } else {
                        _ctrl ctrlSetText (_ATTACHMENTBG param [_forEachIndex, ""]);
                        _ctrl ctrlSetTooltip "";
                    }
                } forEach _attachments;

                // -- Add magazines horizontally. Adds scrollbar for overflow by adding it into controlsGroup -- //
                {
                    // -- If control already exists, use it. Workaround for broken ctrlDelete command -- //
                    _x params ["_mag", "_magQty"];
                    private _magCtrl = (_display displayCtrl (10 + _typeIDC + _forEachIndex));
                    if (isNull _magCtrl) then {
                        _magCtrl = _display ctrlCreate ["ctrlStaticPictureKeepAspect", (10 + _typeIDC + _forEachIndex), _magsCtrl];
                    } else {
                        _magCtrl ctrlEnable true;
                        _magCtrl ctrlShow true;
                    };
                    _magsCtrls pushBack _magCtrl;

                    private _magQtyCtrl = (_display displayCtrl (50 + _typeIDC + _forEachIndex));
                    if (isNull _magQtyCtrl) then {
                        _magQtyCtrl = _display ctrlCreate ["ctrlStatic", (50 + _typeIDC + _forEachIndex), _magsCtrl];
                    } else {
                        _magQtyCtrl ctrlEnable true;
                        _magQtyCtrl ctrlShow true;
                    };
                    _magsCtrls pushBack _magQtyCtrl;

                    _magCtrl ctrlSetText getText(configFile >> "CfgMagazines" >> _mag >> "picture");
                    _magQtyCtrl ctrlSetText format ["x %1", _magQty];
                    _magCtrl ctrlSetPosition [(_forEachIndex * PRESET_ITEM_WIDTH) + (_forEachIndex * PRESET_ITEM_SPACING_X), 0, PRESET_ITEM_WIDTH, PRESET_ITEM_HEIGHT];
                    _magQtyCtrl ctrlSetPosition [(_forEachIndex * PRESET_ITEM_WIDTH) + (_forEachIndex * PRESET_ITEM_SPACING_X), 0, PRESET_ITEM_WIDTH, PRESET_ITEM_HEIGHT];
                    _magQtyCtrl ctrlSetTooltip getText(configFile >> "CfgMagazines" >> _mag >> "displayName");
                    _magCtrl ctrlCommit 0;
                    _magQtyCtrl ctrlCommit 0;
                    //_magsCtrls append [_magCtrl, _magQtyCtrl];
                    //_magsCtrls append [(10 + _typeIDC + _forEachIndex), (50 + _typeIDC + _forEachIndex)];
                } forEach _magazines;

                // -- Update position -- //
                private _itemCtrlH = PRESET_WEAPON_HEIGHT;
                _itemGroupCtrl ctrlSetPosition [0, _posY, PRESET_WIDTH, _itemCtrlH];
                _itemGroupCtrl ctrlCommit 0;
                _posY = _posY + _itemCtrlH + PRESET_ITEM_SPACING_Y;

            } else {
                _itemGroupCtrl ctrlEnable false;
                _itemGroupCtrl ctrlShow false;
                _itemGroupCtrl ctrlSetPosition [0, _posY,  PRESET_WIDTH, 0];
                _itemGroupCtrl ctrlCommit 0;
            };
        } else {
            // -- Add backpack picture -- //
            private _uniformCtrl = _itemGroupCtrl controlsGroupCtrl 5102;
            private _itemCtrl = _itemGroupCtrl controlsGroupCtrl 5101;
            private _backpack = _itemInfo param [0, ""];
            private _items =+ (_variant param [VARIANT_ITEMSMAIN, []]);
            //_items append (_variant param [VARIANT_ITEMS, []]); // Don't list base items, such as bandages which are pretty much the same for all roles

            if !(ctrlEnabled _uniformCtrl) then { _uniformCtrl ctrlEnable true; };
            if !(ctrlEnabled _itemGroupCtrl) then { _itemGroupCtrl ctrlEnable true; };
            if !(ctrlShown _uniformCtrl) then { _uniformCtrl ctrlShow true; };
            if !(ctrlShown _itemGroupCtrl) then { _itemGroupCtrl ctrlShow true; };

            if !(_backpack isEqualTo "") then {
                _itemCtrl ctrlSetText getText(configFile >> "CfgVehicles" >> _backpack >> "picture");
                _itemCtrl ctrlSetTooltip getText(configFile >> "CfgVehicles" >> _backpack >> "displayName");
                _items append (_itemInfo param [1, []]);
                _items append (_itemInfo param [2, []]);
            } else {
                // -- If no backpack and no items worth listing, hide this entire row -- //
                if (_items isEqualTo []) then {
                    _itemGroupCtrl ctrlEnable false;
                    _itemGroupCtrl ctrlShow false;
                    _itemGroupCtrl ctrlSetPosition [0, _posY, PRESET_WIDTH, 0];
                    _itemGroupCtrl ctrlCommit 0;
                } else {
                    _itemCtrl ctrlSetText "\A3\ui_f\data\gui\Rsc\RscDisplayGear\ui_gear_backpack_gs.paa";
                    _itemCtrl ctrlSetTooltip "";
                };
            };


            private _top = true;
            private _columnIndex = 0;
            {
                _x params ["_mag", "_magQty"];
                private _magText = "";
                private _magIcon = if (isClass (configFile >> "CfgMagazines" >> _mag)) then {
                    _magText = getText (configFile >> "cfgMagazines" >> _mag >> "displayName");
                    getText (configFile >> "cfgMagazines" >> _mag >> "picture");
                } else {
                    _magText = getText (configFile >> "CfgWeapons" >> _mag >> "displayName");
                    getText (configFile >> "CfgWeapons" >> _mag >> "picture");
                };

                private _magCtrl = (_display displayCtrl (10 + _typeIDC + _forEachIndex));
                if (isNull _magCtrl) then {
                    _magCtrl = _display ctrlCreate ["ctrlStaticPictureKeepAspect", (10 + _typeIDC + _forEachIndex), _magsCtrl];
                } else {
                    _magCtrl ctrlEnable true;
                    _magCtrl ctrlShow true;
                };
                _magsCtrls pushBack _magCtrl;

                private _magQtyCtrl = (_display displayCtrl (50 + _typeIDC + _forEachIndex));
                if (isNull _magQtyCtrl) then {
                    _magQtyCtrl = _display ctrlCreate ["ctrlStatic", (50 + _typeIDC + _forEachIndex), _magsCtrl];
                } else {
                    _magQtyCtrl ctrlEnable true;
                    _magQtyCtrl ctrlShow true;
                };
                _magsCtrls pushBack _magQtyCtrl;

                private _posYItem = [PRESET_ITEM_HEIGHT + PRESET_ITEM_SPACING_Y, 0] select _top;
                _magCtrl ctrlSetText _magIcon;
                _magQtyCtrl ctrlSetTooltip _magText;
                _magQtyCtrl ctrlSetText format ["x %1", _magQty];
                _magCtrl ctrlSetPosition [(_columnIndex * PRESET_ITEM_WIDTH) + (_columnIndex * PRESET_ITEM_SPACING_X), _posYItem, PRESET_ITEM_WIDTH, PRESET_ITEM_HEIGHT];
                _magQtyCtrl ctrlSetPosition [(_columnIndex * PRESET_ITEM_WIDTH) + (_columnIndex * PRESET_ITEM_SPACING_X), _posYItem, PRESET_ITEM_WIDTH, PRESET_ITEM_HEIGHT];
                _magCtrl ctrlCommit 0;
                _magQtyCtrl ctrlCommit 0;

                // -- Create 2-row fill from left to right -- //
                if (_top) then {
                    _top = false;
                } else {
                    _top = true;
                    _columnIndex = _columnIndex + 1;
                };

                //_magsCtrls append [(10 + _typeIDC + _forEachIndex), (50 + _typeIDC + _forEachIndex)];
            } forEach _items;

            // -- Update position -- //
            private _itemCtrlH = PRESET_WEAPON_HEIGHT;
            _itemGroupCtrl ctrlSetPosition [0, _posY, PRESET_WIDTH, _itemCtrlH];
            _itemGroupCtrl ctrlCommit 0;
            _posY = _posY + _itemCtrlH + PRESET_ITEM_SPACING_Y;
        };

    } forEach [[4100, VARIANT_PRIMARY], [4200, VARIANT_BACKPACK], [4300, VARIANT_SECONDARY], [4400, VARIANT_PISTOL]];

    // -- Hide magazine controls that are currently not used (Workaround for broken ctrlDelete) -- //
    private _allCtrls = uiNamespace getVariable [_magsCtrlVar, []];
    private _unusedCtrls = (_allCtrls - _magsCtrls);
    {
        //private _ctrl = _display displayCtrl _x;
        private _ctrl = _x;
        _ctrl ctrlEnable false;
        _ctrl ctrlShow false;
        // -- @todo: Tell BIS to repair ctrlDelete command. It's broken (1.57)
        // -- Ticket: http://feedback.arma3.com/view.php?id=22317
        //private _deleted = ctrlDelete (_ctrl);

        //[format ["_ctrl:%1", _ctrl], "selectionRoles"] call EFUNC(main,logInfo);

    } forEach _unusedCtrls;
    private _allCtrlsNew = (_unusedCtrls + _magsCtrls);
    uiNamespace setVariable [_magsCtrlVar, _allCtrlsNew];
};
