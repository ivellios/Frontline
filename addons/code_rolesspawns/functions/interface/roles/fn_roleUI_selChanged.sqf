/*
    Function:       FRL_Rolesspawns_fnc_selChanged
    Author:         Adanteh
    Description:    Called when you click an entry in the role list
*/
#include "macros.hpp"

private _display = uiNamespace getVariable [QGVAR(roleDisplay), displayNull];
if (isNull _display) exitWith {};

// Get the selected value
private _control = _display displayCtrl 303;
private _selectedEntry = lnbCurSelRow _control;
if (_selectedEntry == -1) exitWith { };

private _previousSelectedKit = Clib_Player getVariable [QSVAR(rkit), ""];
private _role = [_control, [_selectedEntry, 0]] call CFUNC(lnbLoad);

private _isDisabled = (toLower _role find "_disabled");

if ((_previousSelectedKit != _role) && (_isDisabled == -1)) then {
    // -- Double check all requirements WITHIN the mutex, so we don't get people selecting the same role -- //
    [Clib_Player, _role] call FUNC(roleAssign);
};

// -- Strip the disabled tag and show presets
if (_isDisabled != -1) then { _role = _role select [0, _isDisabled]; };
[_display, _role] call FUNC(roleUI_showPreset);
