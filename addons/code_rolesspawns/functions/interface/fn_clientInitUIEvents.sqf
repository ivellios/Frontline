/*
    Function:       FRL_Rolesspawns_fnc_clientInitUIEvents
    Author:         Adanteh
    Description:    Random shit
*/
#include "macros.hpp"

["groupChanged", {
    [QSVAR(roleUI_update), _this select 0] call CFUNC(targetEvent);
    [QSVAR(squadUI_listUpdate), playerSide] call CFUNC(targetEvent);
    [QSVAR(DeployScreen_update)] call CFUNC(localEvent);

    [QSVAR(squadUI_selectorUpdate), playerSide] call CFUNC(targetEvent);

    [QSVAR(SpawnsChanged)] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


["leaderChanged", {
    [QSVAR(roleUI_update)] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


// -- SQUAD MEMBER LIST
[QSVAR(RoleChanged), {
    [QSVAR(squadUI_update), group Clib_Player] call CFUNC(targetEvent);
}] call CFUNC(addEventHandler);


// -- Squad list
[QSVAR(squadUI_squadListSelChanged), {
    QSVAR(squadUI_update) call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


// -- Unit list
[QSVAR(squadUI_memberListSelChanged), {
    QSVAR(squadUI_buttonsUpdate) call CFUNC(localEvent);
}] call CFUNC(addEventHandler);



["playerSideChanged", {
    (_this select 0) params ["_newSIde", "_oldSide"];
    [QSVAR(sideUI_update)] call CFUNC(localEvent);
    [QSVAR(squadUI_listUpdate), _oldSide] call CFUNC(targetEvent);
    [QSVAR(squadUI_selectorUpdate)] call CFUNC(globalEvent);
}] call CFUNC(addEventHandler);
