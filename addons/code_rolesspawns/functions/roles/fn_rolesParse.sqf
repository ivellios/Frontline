/*
    Function:       FRL_Rolesspawn_fnc_rolesParse
    Author:         Adanteh
    Description:    Parses all the roles uses in the mission (Used after squad parsing)
*/
#include "macros.hpp"

GVAR(roleNamespace) = false call CFUNC(createNamespace);

private _fnc_parseWeapon = {
    if (isClass _this && {getText (_this >> "weapon") != ""}) then {
        [
            getText (_this >> "weapon"),
            getArray (_this >> "magazines"),
            [
                getText (_this >> "muzzle"),
                getText (_this >> "bipod"),
                getText (_this >> "rail"),
                getText (_this >> "optics")
            ]
        ]
    } else {
        []
    }
};

private _allSquads = GVAR(squadNamespace) getVariable [QSVAR(Squads), []];
private _fnc_getClothing = {
    params ["_cfg"];
    private _clothingCfg = (_cfg >> "Clothing");
    if !(isClass _clothingCfg) then {
        [] // -- Return empty array if class doesn't exist
    } else {
        private _clothingArray = [];
        {
            if (isArray (_clothingCfg >> _x)) then {
                _clothingArray pushBack (getArray (_clothingCfg >> _x));
            } else {
                _clothingArray pushBack (getText (_clothingCfg >> _x));
            };
        } forEach ["uniform", "headgear", "goggles", "vest"];
        // -- If the entire array is empty, just pass a fully empty array so we don't reset clothing
        if (_clothingArray isEqualTo ["", "", "", ""]) then {
            []
        } else {
            _clothingArray
        };
    };
};

private _fnc_parseRole = {
    params ["_role"];
    private _roleCfg = (missionConfigFile >> "FRL" >> "Roles" >> _role);
    if !(isClass _roleCfg) then {
        // -- If it doesnt exist in the mission, check in mod instead -- //
        _roleCfg = (configFile >> "FRL" >> "Roles" >> _role);
    };

    if (isClass _roleCfg) then {

        GVAR(roleNamespace) setVariable [_role, true];
        private _roleName = getText (_roleCfg >> "displayName");
        if (isLocalized _roleName) then { _roleName = localize _roleName; };

        private _variants = [];

        for "_h" from 0 to (count (_roleCfg >> "Variants") - 1) do {
            private _variant = (_roleCfg >> "Variants") select _h;

            private _pistol  = (_variant >> "Pistol") call _fnc_parseWeapon;
            private _primary = (_variant >> "Primary") call _fnc_parseWeapon;
            private _secondary = (_variant >> "Secondary") call _fnc_parseWeapon;
            private _backpack = if (isClass(_variant >> "Backpack") && {getText (_variant >> "Backpack" >> "backpack") != ""}) then {
                [
                    getText (_variant >> "Backpack" >> "backpack"),
                    getArray (_variant >> "Backpack" >> "content")
                ]
            } else {
                []
            };

            private _variantName = getText (_variant >> "displayName");
            if (isLocalized  _variantName) then { _variantName = localize _variantName };

            _variants pushBack [
    /* VARIANT_CLASS       */ configName _variant,
    /* VARIANT_NAME        */ _variantName,
    /* VARIANT_PISTOL      */ _pistol,
    /* VARIANT_PRIMARY     */ _primary,
    /* VARIANT_SECONDARY   */ _secondary,
    /* VARIANT_BACKPACK    */ _backpack,
    /* VARIANT_ITEMSMAIN   */ getArray (_variant >> "items"),
    /* VARIANT_ITEMS       */ getArray (_variant >> "itemshidden"),
    /* VARIANT_CLOTHING    */ [_variant] call _fnc_getClothing
            ];
        };

        private _clothing = [_roleCfg] call _fnc_getClothing;
        private _values = [];
        {
            _x params ["_key", "_value"];
            _values pushBack _value;
            GVAR(roleNamespace) setVariable [_role + _key, _value];
        } forEach [
            ["NAME", _roleName],
            ["CLOTHING", _clothing],
            ["VARIANTS", _variants],
            ["VARIANT_CURR", 0],
            ["ABILITIES", (getArray (_roleCfg >> "abilities") apply { toLower _x })],
            ["ICON",  getText (_roleCfg >> "UIIcon")],
            ["MARKER", getText (_roleCfg >> "mapIcon")],
            ["HIGHLIGHT", (getNumber (_roleCfg >> "highlightVacant") > 0)]
        ];
    } else {
        // TODO Add error report that a non-existant kit is used in a squad
    };

};

{
    private _groupRoles = GVAR(squadNamespace) getVariable (_x + "_roles");
    {
        _x params ["_role"];
        [_role] call _fnc_parseRole;
        nil;
    } count _groupRoles;
    nil;
} count _allSquads;


// -- Parse baserole, not part of any squads, used for things like spawning in SD without much gear
{
    private _baseRole = [_x, "baserole", ""] call MFUNC(getSideData);
    if (_baseRole != "") then {
        if !(GVAR(roleNamespace) getVariable [_baseRole, false]) then {
            [_baseRole] call _fnc_parseRole;
        };
    };
} forEach (call MFUNC(getSides));
