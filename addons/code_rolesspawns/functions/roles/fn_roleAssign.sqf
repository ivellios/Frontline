/*
    Function:       FRL_Rolesspawns_fnc_roleAssign
    Author:         Adanteh
    Description:    Assigns given kit to given unit (Doesn't equip gear, just sets all vars and updates available kits)
*/
#include "macros.hpp"

params ["_unit", "_role"];

[{
    params ["_unit", "_role"];

    // -- Update the UI if we can't use it
    if !([_unit, _role] call FUNC(roleCanUse)) exitWith {
        [QSVAR(roleUI_update)] call CFUNC(localEvent);
    };


    _unit setVariable [QSVAR(rkit), _role, true];
    [QSVAR(roleUI_update), group _unit] call CFUNC(targetEvent);
    [QSVAR(RoleChanged), [_role]] call CFUNC(localEvent);

}, [_unit, _role], "roles"] call CFUNC(mutex);
