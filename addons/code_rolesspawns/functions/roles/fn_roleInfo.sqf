/*
    Function:       FRL_Rolesspawns_fnc_roleInfo
    Author:         Adanteh
    Description:    Gets a given value for given role tag
*/
#include "macros.hpp"

params ["_role", "_value"];

if (_role isEqualType objNull) then {
    _role = _role getVariable [QSVAR(rkit), ""];
};

private _return = GVAR(roleNamespace) getVariable (_role + _value);

// -- Default return
if (isNil "_return") then {
    _return = switch (toLower _value) do {
        case "name": { "" };
        case "icon": { "\a3\ui_f\data\IGUI\Cfg\Actions\clear_empty_ca.paa" };
        case "abilities": { [] };
        case "clothing": { [] };
        case "marker": { "\A3\ui_f\data\map\vehicleicons\iconMan_ca.paa" };
        case "highlight": { false };
        case "variants": { [] };
        case "variant_curr": { 0 };
        default { "" };
    };
};

_return;
