/*
    Function:       FRL_Rolesspawns_fnc_roleEqupData
    Author:         Adanteh
    Description:    Applies abilities and sets vars
*/
#include "macros.hpp"

params ["_unit", ["_role", "##"]];

if (_role isEqualTo "##") then {
    _role = _unit getVariable [QSVAR(rKit), ""];
};

// -- Applies abilities that a role has (Can put rallypoint, counts as medic, engineer or so on) -- //
// -- Following commands are only used to enable the vanilla systems -- //
// -- This has delayed broadcast by vanilla design (cuz logic yo) -- //

private _abilities = [_role, "abilities"] call FUNC(roleInfo);
if ("medic" in _abilities) then {
	if !(_unit getUnitTrait "Medic") then {
		_unit setUnitTrait ["Medic", true];
	};
} else {
	if (_unit getUnitTrait "Medic") then {
		_unit setUnitTrait ["Medic", false];
	};
};
if ("engineer" in _abilities) then {
	if !(_unit getUnitTrait "Engineer") then {
		_unit setUnitTrait ["Engineer", true];
	};
} else {
	if (_unit getUnitTrait "Engineer") then {
		_unit setUnitTrait ["Engineer", false];
	};
};

private _displayName = [_role, "NAME"] call FUNC(roleInfo);
private _icon = [_role, "ICON"] call FUNC(roleInfo);
private _mapIcon = [_role, "MARKER"] call FUNC(roleInfo);

//unit setVariable [QSVAR(rkit), _role, true]; */
_unit setVariable [QSVAR(rAbilities), _abilities, true];
_unit setVariable [QSVAR(rkitIcon), _icon, true];
_unit setVariable [QSVAR(rMapIcon), _mapIcon, true];
