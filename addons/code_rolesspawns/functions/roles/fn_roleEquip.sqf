/*
    Function:       FRL_Rolesspawns_fnc_roleEqup
    Author:         Adanteh
    Description:    Equips items belonging to role (On respawn/kit switch)
*/
#include "macros.hpp"
#define ASSIGNED_ITEM_TYPE 131072

params ["_unit", ["_role", "##"]];

if (_role isEqualTo "##") then {
    _role = _unit getVariable [QSVAR(rKit), ""];
};

// -- Starts bypass while equipping -- //
_unit setVariable [QGVAR(bypassClothingCheck), true];

removeAllWeapons _unit;
removeAllAssignedItems _unit;
removeAllItems _unit;
removeBackpack _unit;
removeVest _unit;
removeHeadgear _unit;
removeGoggles _unit;

private _variantSelected = [_role, "VARIANT_CURR"] call FUNC(roleInfo);
private _variant = ([_role, "VARIANTS"] call FUNC(roleInfo)) select _variantSelected;

private _clothing = _variant param [VARIANT_CLOTHING, []];
if (_clothing isEqualTo []) then {
    _clothing = [_role, "clothing"] call FUNC(roleInfo);
};
if (_clothing isEqualTo []) exitWith {
	diag_log ["[FRL ERROR] ", "fn_roleEquip unknown kit: ", _role];
	_unit setVariable [QGVAR(bypassClothingCheck), false];
};

private _fnc_selectClothing = {
	params ["_index"];
	private _entry = _clothing select _index;
	if (_entry isEqualType []) then {
		_entry = selectRandom _entry;
	};
	_entry;
};

// -- Save array of equipped clothign, so we can't swap it out
private _clothingArray = [];
{
	_clothingArray pushBack ([_x] call _fnc_selectClothing);
} forEach [CLOTHING_UNIFORM, CLOTHING_HEADGEAR, CLOTHING_GOGGLES, CLOTHING_VEST];
GVAR(clothingArray) = _clothingArray;

// -- This needs to be saved for validating of clothing
if !((_clothingArray select 1) isEqualTo "") then {
    _unit addHeadgear (_clothingArray select 1);
};
if !((_clothingArray select 2) isEqualTo "") then {
    _unit addGoggles (_clothingArray select 2);
};
if !((_clothingArray select 3) isEqualTo "") then {
    _unit addVest (_clothingArray select 3);
};

// -- Only replace uniform if it's not the same as we already have
if (uniform _unit != ((_clothingArray select 0))) then {
	_unit forceAddUniform (_clothingArray select 0);
};

private _variantSelected = [_role, "VARIANT_CURR"] call FUNC(roleInfo);
private _variant = ([_role, "VARIANTS"] call FUNC(roleInfo)) select _variantSelected;


private _fnc_giveWeapon = {
	private _wpn = _variant select _this;
	if (count _wpn != 0) then {
		{
			// -- This is a little option to force add item to vest, it'll bypass container size limitations
			_x params [["_item", "", [""]], ["_itemQty", 1], ["_forceAddToVest", 0]];
			if (_forceAddToVest == 1) then {
				// -- Only add the first magazine to vest
				(vestContainer _unit) addMagazineCargoGlobal [_item, _itemQty];
				_itemQty = _itemQty - 1;
			};
			if (_itemQty > 0) then {
				_unit addMagazines [_item, _itemQty];
			};
		} forEach (_wpn select 1);

		_unit addWeapon (_wpn select 0);

		{
			if (_x != "") then {
				switch (_this) do {
					case VARIANT_PRIMARY: {
						_unit addPrimaryWeaponItem _x;
					};
					case VARIANT_SECONDARY: {
						_unit addSecondaryWeaponItem _x;
					};
					case VARIANT_PISTOL: {
						_unit addHandgunItem _x;
					};
				};
			};
		} forEach (_wpn select 2);
	};
};

private _stuff = _variant select VARIANT_BACKPACK;
if (count _stuff > 0) then {
	_unit addBackpack (_stuff select 0);

	private _backpack = unitBackpack _unit;

	clearWeaponCargoGlobal _backpack;
	clearMagazineCargoGlobal _backpack;
	clearItemCargoGlobal _backpack;

	{
		_x params ["_item", "_itemQty"];
		if (isClass(configFile >> "CfgWeapons" >> _item)) then {
			if (isClass (configFile >> "CfgWeapons" >> _item >> "WeaponSlotsInfo")) then {
				for "_i" from 1 to _itemQty do {
					_unit addWeaponCargoGlobal _item;
				};
			} else {
				for "_i" from 1 to _itemQty do {
					_unit addItemToBackpack _item;
				};
			};
		} else {
			if (isClass(configFile >> "CfgMagazines" >> _item)) then {
				_backpack addMagazineCargoGlobal _x;
			};
		};
	} forEach (_stuff select 1);
};

VARIANT_PRIMARY call _fnc_giveWeapon;
VARIANT_SECONDARY call _fnc_giveWeapon;
VARIANT_PISTOL call _fnc_giveWeapon;

private _fnc_addItems = {
	params [["_itemArray", []]];
	{
		_x params ["_item", "_itemQty"];
		for "_i" from 1 to _itemQty do {
			if (isClass(configFile >> "CfgWeapons" >> _item)) then {
				// -- Get the type of item -- //
				if (isClass(configFile >> "CfgWeapons" >> _item >> "WeaponSlotsInfo")) then {
					_unit addWeapon _item;
				} else {
					_unit addItem (_x select 0);
					if ((getNumber(configFile >> "CfgWeapons" >> _item >> "type")) == ASSIGNED_ITEM_TYPE) then {
						_unit assignItem _item;
					};
				};
			} else {
				_unit addMagazine _item;
			};
		};
	} forEach _itemArray;
};

[(_variant select VARIANT_ITEMSMAIN)] call _fnc_addItems;
[(_variant select VARIANT_ITEMS)] call _fnc_addItems;

// -- Ends bypass, resuming clothing validation (Called when clothing changed) -- //
_unit setVariable [QGVAR(bypassClothingCheck), false];
