/*
    Function:       FRL_Rolesspawns_fnc_clientInitLimitations
    Author:         Adanteh
    Description:    Inits gear limitations (Not allowed to take enemy uniform and so on)
*/
#include "macros.hpp"


// TODO - Make this a setting
GVAR(clothingArray) = [];
["clothingChanged", { _this call FUNC(validateClothing) }] call CFUNC(addEventHandler);
["missionStarted", {
	GVAR(oldPlayerClothing) = ["", "", "", ""];
	[{
		private _data = [uniform CLib_Player, headgear CLib_Player, goggles CLib_Player, vest CLib_Player];
		if !(_data isEqualTo GVAR(oldPlayerClothing)) then {
			["clothingChanged", [CLib_Player, _data, GVAR(oldPlayerClothing)]] call CFUNC(localEvent);
			GVAR(oldPlayerClothing) = _data;
		};
	}, 0] call MFUNC(addPerFramehandler);
}] call CFUNC(addEventHandler);
