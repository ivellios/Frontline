/*
    Function:       FRL_Rolesspawns_fnc_validateClothing
    Author:         Adanteh
    Description:    Checks if our changed uniform is acceptable for us to wear (Makes sure you can't pick up enemy uniform)

     0: The unit <OBJECT>
     1: Looks new <ARRAY>
     	0: New Uniform <STRING>
     	1: New Helmet <STRING>
     	2: New Goggles <STRING>
     	3: New Vest <STRING>
     1: Looks old <ARRAY>
     	0: Old Uniform <STRING>
     	1: Old Helmet <STRING>
     	2: Old Goggles <STRING>
     	3: Old Vest <STRING>
*/
#include "macros.hpp"

(_this select 0) params ["_unit", "_clothingNew", "_clothingOld"];

// -- Allows you to bypass clothes check once (On role equip for example) -- //
// -- Default is true, so we can bypass once when original unit is assigned -- //
if !(_unit getVariable [QGVAR(bypassClothingCheck), true]) then {
	_clothingNew params ["_uniformNew", "_headgearNew", "_gogglesNew", "_vestNew"];
	_clothingOld params ["_uniformOld", "_headgearOld", "_gogglesOld", "_vestOld"];

	private _roleClothing = GVAR(clothingArray);
	private _allowedEmpty = [false, true, true, false];
	private _restoreCode = [
		/* UNIFORM 	*/	{ params ["_unit", "_roleItem"]; _unit forceAddUniform _roleItem; },
		/* HELMET 	*/	{ params ["_unit", "_roleItem"]; _unit addHeadgear _roleItem },
		/* GOGGLES 	*/	{ params ["_unit", "_roleItem"]; _unit addGoggles _roleItem  },
		/* VEST 	*/	{ params ["_unit", "_roleItem"]; _unit addVest _roleItem }
	];

	// -- Validate each item -- //
	{
		// -- Item is not the same as the role item -- //
		_restoreItem = false;
		private _roleItem = (_roleClothing param [_forEachIndex, ""]);
		if (_x != _roleItem) then {
			if (_x == "") then {
				if !(_allowedEmpty select _forEachIndex) then {
					_restoreItem = true;
				};
			} else {
				_restoreItem = true;
			};
		};
		if (_restoreItem) then {
			[_unit, _roleItem] call (_restoreCode select _forEachIndex);
		};
	} forEach _clothingNew;
};
