/*
    Function:       FRL_Rolesspawns_fnc_roleHasAbility
    Author:         Adanteh
    Description:    Checks if
*/
#include "macros.hpp"

params ["_role", "_ability"];

if (_role isEqualType objNull) then {
    _role = _role getVariable [QSVAR(rkit), ""];
};

toLower _ability in (GVAR(roleNamespace) getVariable [_role + "abilities", []]);
