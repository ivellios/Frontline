/*
    Function:       FRL_Rolesspawns_fnc_roleCanUse
    Author:         Adanteh
    Description:    Checks role conditions, to be used for unassign the role when conditions no longer apply (Left/kicked from the squad or not enough players)
*/
#include "macros.hpp"

params [["_unit", CLib_Player], ["_role", "##"]];

private _playerGroup = group _unit;
private _playerGroupType = _playerGroup getVariable [QMVAR(gType), ""];
if (_playerGroupType == "") exitWith {
	false;
};

if (_role isEqualTo "##") then {
    _role = _unit getVariable [QSVAR(rkit), ""];
};

private _canUseRole = true;
private _requiredGroupMembers = -1;
{
    _x params ["_roleTag", ["_requiredForKit", -1]];
    if (_roleTag == _role) exitWith {
        _requiredGroupMembers = _requiredForKit;
        nil;
    };
    nil;
} count (GVAR(squadNamespace) getVariable [(format ["%1_roles", _playerGroupType]), ""]);

if (_requiredGroupMembers != -1) then {

	// -- Check if there are enough people in the squad for this role -- //
	private _playerGroupUnits = units _group;
	if (_requiredGroupMembers > (count _playerGroupUnits)) exitWith {
		_canUseRole = false;
	};

	// -- Check if no one else already has this role in the squad -- //
	if ({ (_x getVariable [QSVAR(rkit), ""] == _selectedRole) } count (_playerGroupUnits - [_unit]) > 0) exitWith {
		_canUseRole = false;
	};
};

_canUseRole;
