/*
    Function:       FRL_Roles_fnc_roleRearm
    Author:         Adanteh
    Description:   	Rearms role and reequips launcher (if it's disposable)
	Example:		[player, player getVariable ["AAW_kit_kit", ""]] call frl_roles_fnc_roleRearm;
*/
#include "macros.hpp"

#define ASSIGNED_ITEM_TYPE 131072

params ["_unit", "_role"];

private _allItemsAndMagazines = magazines _unit;
_allItemsAndMagazines append (weapons _unit);
_allItemsAndMagazines append (items _unit);

private _variantSelected = [_role, "VARIANT_CURR"] call FUNC(roleInfo);
private _variant = ([_role, "VARIANTS"] call FUNC(roleInfo)) select _variantSelected;

private _fnc_restoreAmmo = {
	private _wpn = _variant select _this;
	if (count _wpn != 0) then {
		{
			// -- This is a little option to force add item to vest, it'll bypass container size limitations
			_x params [["_item", "", [""]], ["_itemQty", 1], ["_forceAddToVest", 0]];
			private _existingItemsOfType = ({ _x == _item } count _allItemsAndMagazines);
			if (_existingItemsOfType >= 1) then {
				for "_i" from 1 to _existingItemsOfType do {
					_unit removeMagazineGlobal _item;
				};
			};
			if (_forceAddToVest == 1) then {
				// -- Only add the first magazine to vest
				(vestContainer _unit) addMagazineCargoGlobal [_item, _itemQty];
				_itemQty = _itemQty - 1;
			};
			if (_itemQty > 0) then {
				_unit addMagazines [_item, _itemQty];
			};
		} forEach (_wpn select 1);
	};
};

VARIANT_PRIMARY call _fnc_restoreAmmo;
VARIANT_SECONDARY call _fnc_restoreAmmo;
VARIANT_PISTOL call _fnc_restoreAmmo;

private _stuff = _variant select VARIANT_BACKPACK;
if (count _stuff > 0) then {
	private ["_backpack"];
	if (backpack _unit == "") then {
		_unit addBackpack (_stuff select 0);
		_backpack = unitBackpack _unit;
		clearWeaponCargoGlobal _backpack;
		clearMagazineCargoGlobal _backpack;
	} else {
		_backpack = unitBackpack _unit;
	};

	{
		_x params ["_item", "_itemQty"];
		_itemQty = 0 max (_itemQty - ({ _x == _item } count _allItemsAndMagazines));
		if (_itemQty > 0) then {
			if (isClass(configFile >> "CfgWeapons" >> _item)) then {
				if (isClass (configFile >> "CfgWeapons" >> _item >> "WeaponSlotsInfo")) then {
					for "_i" from 1 to _itemQty do {
						_unit addWeaponCargoGlobal _item;
					};
				} else {
					for "_i" from 1 to _itemQty do {
						_unit addItemToBackpack _item;
					};
				};
			} else {
				if (isClass(configFile >> "CfgMagazines" >> _item)) then {
					_backpack addMagazineCargoGlobal [_item, _itemQty];
				};
			};
		};
	} forEach (_stuff select 1);
};


private _fnc_addItems = {
	params [["_itemArray", []]];
	{
		_x params ["_item", "_itemQty"];
		private _currentFound = "";
		_itemQty = 0 max (_itemQty - ({ _x == _item } count _allItemsAndMagazines));
		for "_i" from 1 to _itemQty do {
			if (isClass(configFile >> "CfgWeapons" >> _item)) then {
				// -- Get the type of item -- //
				if (isClass(configFile >> "CfgWeapons" >> _item >> "WeaponSlotsInfo")) then {
					_unit addWeapon _item;
				} else {
					_unit addItem (_x select 0);
					if ((getNumber(configFile >> "CfgWeapons" >> _item >> "type")) == ASSIGNED_ITEM_TYPE) then {
						_unit assignItem _item;
					};
				};
			} else {
				_unit addMagazine _item;
			};
		};
	} forEach _itemArray;
};

[(_variant select VARIANT_ITEMSMAIN)] call _fnc_addItems;
[(_variant select VARIANT_ITEMS)] call _fnc_addItems;

// -- If the secondary weapon on this kit is a disposable (launcher), and it's used, replace it with a fresh one -- //
private _secondary = (_variant select VARIANT_SECONDARY) param [0, ""];
if (_secondary != "") then {
	if ((getNumber (configFile >> "CfgWeapons" >> _secondary >> "rhs_disposable")) > 0) then {
		private _secondaryCurrent = secondaryWeapon _unit;
		if (_secondaryCurrent != _secondary) then {
			if (_secondaryCurrent != "") then {
				_unit removeWeapon _secondaryCurrent;
			};
			_unit addWeapon _secondary;
		};
	};
};

// -- reloads the currently loaded magazine
{
	Clib_Player setAmmo [_x, 1000000]; //full mag with default ammo count
} foreach [primaryWeapon Clib_Player, secondaryWeapon Clib_Player, handgunWeapon Clib_Player];
