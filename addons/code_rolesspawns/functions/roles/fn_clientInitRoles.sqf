/*
    Function:       FRL_Rolespawns_fnc_clientInitRoles
    Author:         Adanteh
    Description:    Something
*/
#include "macros.hpp"

// -- Require RP ability in the role config if we use our role system
["placeRPonCall", {
	params ["_caller", "_pos"];
	private _canPlace = [_caller getVariable [QSVAR(rkit), ""], "rp"] call FUNC(roleHasAbility);
	if !(_canPlace) then {
		["showNotification", ["You need the squad leader kit to place RPs"]] call CFUNC(localEvent);
	};
	_canPlace
}] call MFUNC(addCondition);


// -- When you create a new group, see if there's any roles to autoassign to -- //
["groupCreated", {
    (_this select 0) params ["_newGroup", "_groupType"];
    if !(Clib_Player in units _newGroup) exitWith { };

    {
        _x params ["_role", ["_requiredForKit", -1], ["_autoAssign", 0]];
        if (_autoAssign > 0) exitWith {
            [Clib_Player, _role] call FUNC(roleAssign);
        };
    } forEach (GVAR(squadNamespace) getVariable [format ["%1_%2", _groupType, "roles"], []]);
}] call CFUNC(addEventHandler);

// -- When you change groups, see if your current group is available, if not, unassign it
["groupChanged", {
    if !([Clib_player] call FUNC(roleCanUse)) then {
        Clib_Player setVariable [QSVAR(rkit), "", true];
    };
}] call CFUNC(addEventHandler);


[QSVAR(AfterSpawn), {
    (_this select 0) params ["_spawnpoint", "_unit", "_redeploy"];
    if (_spawnpoint getVariable ["type", ""] isEqualTo "base") then {
        [_unit] call FUNC(roleEquip);
    } else {
        private _spawnRole = missionNamespace getVariable [QMVAR(roleSpawnOverride), "##"];
        [_unit, _spawnRole] call FUNC(roleEquip);
    };
    [_unit] call FUNC(roleEquipData);
}] call CFUNC(addEventHandler);
