/*
    Function:       FRL_Roles_fnc_roleEquipCrewman
    Author:         Adanteh
    Description:    Equips crewman kit if it's available in the current squad type
    Example:        [Clib_Player] call FRL_Roles_fnc_equipCrewman
*/
#include "macros.hpp"

params ["_unit", ["_vehicle", objNull]];

private _isCrewman = [_unit, "crewman"] call FUNC(roleHasAbility);
if (_isCrewman) exitWith {
    ["You already have a crewman kit equipped", (MEDIAPATH + "roles\crewman.paa"), "green"] call MFUNC(notificationShow);
    true; // -- Close interact menu
};

private _preset = "";
if !(isNull _vehicle) then {
    _preset = ["crewman", "pilot"] select (_vehicle isKindOf "Air");
};

private _playerGroup = group _unit;
private _playerGroupType = _playerGroup getVariable [QMVAR(gType), ""];
private _hasCrewmanAvailable = false;
private _roleToAssign = "";
private _roles = GVAR(squadNamespace) getVariable [(format ["%1_roles", _playerGroupType]), ""];
// TODO: Consider a rewrite to detect if a squad has crewmen on initial parsing
private _crewmanKits = [];
private _presetKit = "";
private _presetIndex = 0;

{
    _x params ["_roleTag", ["_requiredGroupMembers", 0]];
    private _isCrewman = [_roletag, "crewman"] call FUNC(roleHasAbility);
    if (_isCrewman) then {
        if (_requiredGroupMembers == -1) exitWith { _hasCrewmanAvailable = true };
        private _playerGroupUnits = [_playerGroup] call CFUNC(groupPlayers);
        private _playersWithKit =  { (_x getVariable [QSVAR(rkit), ""] == _roleTag) } count _playerGroupUnits;
        _hasCrewmanAvailable = ((_playersWithKit == 0) && {(count _playerGroupUnits >= _requiredGroupMembers)});
    };

    // -- Check if there's a specific type we want to get if possibru
    if (_hasCrewmanAvailable) then {
        private _variantNames = ([_roleTag, "VARIANTS"] call FUNC(roleInfo)) apply { toLower (_x select 0) };
        private _variantIndex = _variantNames find _preset;
        if (_variantIndex >= -1) then {
            _presetKit = _roleTag;
            _presetIndex = _variantIndex;
        };
        _crewmanKits pushBack _roleTag;
    };
    nil;
} count _roles;

if !(_crewmanKits isEqualTo []) then {
    if (_presetKit != "") then { // -- If given prefered preset is found, make sure that one is set as the current variant
        _roleToAssign = _presetKit;
        GVAR(roleNamespace) setVariable [_presetKit + "VARIANT_CURR", _presetIndex];
    } else {
        _roleToAssign = _crewmanKits select 0;
    };
};

if !(_hasCrewmanAvailable) exitWith {
    ["No crewman kit available in your current squad", (MEDIAPATH + "roles\crewman.paa"), "yellow"] call MFUNC(notificationShow);
    true; // -- Close interact menu
};

[_unit, _roleToAssign] call FUNC(roleAssign);
[_unit, _roleToAssign] call FUNC(roleEquip);
[_unit, _roleToAssign] call FUNC(roleEquipData);

["Equipped crewman kit", (MEDIAPATH + "roles\crewman.paa"), "green"] call MFUNC(notificationShow);
true;
