/*
    Function:       FRL_AI_fnc_init
    Author:         Adanteh
    Description:    Inits the role + squad system
*/
#include "macros.hpp"

[QGVAR(RespawnSettings), configFile >> "FRL" >> "CfgRespawn"] call MFUNC(cfgSettingLoad);
[QGVAR(RespawnSettings), missionConfigFile >> "FRL" >> "CfgRespawn"] call MFUNC(cfgSettingLoad);

[] call FUNC(squadParse);
[] call FUNC(rolesParse);

if (isServer) then {
    GVAR(spawnpoints) = true call CFUNC(createNamespace);
    publicVariable QGVAR(spawnpoints);

    GVAR(spawnPointTypes) = true call CFUNC(createNamespace);
    publicVariable QGVAR(spawnpointTypes);
};
