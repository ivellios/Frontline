 /*---------------------------------------------------------------------------
     This is a modified version of AAW, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

 #include "macros.hpp"
/*
    Project Reality ArmA 3

    Author: BadGuy, NetFusion

    Description:
    Creates a squad on players side

    Parameter(s):
    1: Description <STRING>
    2: Type <STRING>

    Returns:
    None
*/

params ["_mode", ["_args", []]];

switch (tolower _mode) do {
    case "call": {

        _args params ["_description", "_type", "_unit", ["_side", sideLogic], ["_createGroup", true]];
        if (_side isEqualTo sideLogic) then { _side = side group _unit };

        // Remove leading whitespace
        private _descriptionArray = toArray _description;
        for "_i" from 0 to (count _descriptionArray - 1) do {
            if (!((_descriptionArray select _i) in [9, 10, 13, 32])) exitWith {_description = _description select [_i]};
        };


        // -- Instead of creating a new group, change the current group
        private _newGroup = group _unit;
        if (_createGroup) then {
            [_unit] call MFUNC(leaveSquad);
            _newGroup = createGroup [_side, true];
        };

        private _squadID = [_unit] call FUNC(squadNextID);
        private _groupSizeMax = GVAR(squadNamespace) getVariable format ["%1_%2", _type, "maxSize"];
        private _mapIcon = GVAR(squadNamespace) getVariable format ["%1_%2", _type, "mapIcon"];


        _newGroup setGroupIdGlobal [_squadID];
        _newGroup setVariable [QMVAR(gName), _description, true];
        _newGroup setVariable [QMVAR(gType), _type, true];
        _newGroup setVariable [QMVAR(gSize), _groupSizeMax, true];
        _newGroup setVariable [QMVAR(gIcon), _mapIcon, true];

        if (_createGroup) then {
            [_unit] join _newGroup;
        };

        ["groupCreated", [_newGroup, _type]] call CFUNC(localEvent);
    };

    case "mutex": {
        [{ ['call', _this] call FUNC(squadCreate); }, _args, "respawn"] call CFUNC(mutex);
    };
};
