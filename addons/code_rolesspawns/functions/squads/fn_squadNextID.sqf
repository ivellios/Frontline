/*---------------------------------------------------------------------------
    This is a modified version of AAW, July 11th 2016 version
    Released under APL license by the AAW Team.

    Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
    Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
---------------------------------------------------------------------------*/

/*
    Function:       FRL_Rolesspawns_fnc_squadNextID
    Author:         NetFusion
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params [["_unit", Clib_Player]];
(MVAR(squadIds) - ((allGroups select {side _x == (side group _unit) && (_x != group _unit || count (_x call CFUNC(groupPlayers)) > 1)}) apply {groupId _x})) select 0;
