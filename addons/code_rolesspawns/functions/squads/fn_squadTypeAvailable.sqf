/*
    Function:       FRL_Rolesspawns_fnc_squadTypeAvailable
    Author:         Adanteh
    Description:    Checks if given squad t ype is available
*/
#include "macros.hpp"
#ifdef DEBUGFULL
    if (true) exitWith { true };
#endif

params ["_groupSlug", "_playersOnTeam"];
if (isNil "_playersOnTeam") then {
    _playersOnTeam = [true] call MFUNC(getSideSmallest);
};

  // -- Unlocks squad unlimited when player count is reached
private _availableAt = GVAR(squadNamespace) getVariable (_groupSlug + "_availableAt");
if (_availableAt > 0 && { (_availableAt > _playersOnTeam) }) exitWith {
    _canCreateSquadMessage = format ["Squad type unlocks %1 players<br />Currently %2 on smallest team (AI count half)", _availableAt, _playersOnTeam];
    false
};

// -- Unlocks squad unlimited when player count is reached
private _availableAtArray = GVAR(squadNamespace) getVariable (_groupSlug + "_availableAtArray");
if (!(_availableAtArray isEqualTo []) && {
    private _groupsOfType = { ((groupID _x in MVAR(squadIds)) && {((_x getVariable [QMVAR(gType), ""]) == _groupSlug)}) } count (allGroups select {side _x == playerSide});
    private _nextUnlockAt = (_availableAtArray param [_groupsOfType, 999]);
    if (_nextUnlockAt isEqualTo 999) then {
        _canCreateSquadMessage = "No more squads of this type available";
    } else {
        _canCreateSquadMessage = format ["Next squad of this type unlocks at %1 players<br />Currently %2 on smallest team (AI count half)", _nextUnlockAt, _playersOnTeam];
    };
    (_nextUnlockAt > _playersOnTeam);
}) exitWith {
    false
};

true;
