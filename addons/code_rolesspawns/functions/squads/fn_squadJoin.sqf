 /*---------------------------------------------------------------------------
     This is a modified version of AAW, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"

[{
    params ["_group"];

    // Check conditions for creation
    if (_group == group Clib_Player) exitWith { };

    private _groupType = _group getVariable [QMVAR(gType), ""];
    private _groupSizeMax = GVAR(squadNamespace) getVariable format ["%1_%2", _groupType, "maxSize"];

    if ((_groupSizeMax != -1) && {(count ([_group] call CFUNC(groupPlayers)) >= _groupSizeMax)}) exitWith {};

    [Clib_Player] join _group;
}, _this, "respawn"] call CFUNC(mutex);
