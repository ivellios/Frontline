/*
    Function:       FRL_Rolesspawns_fnc_clientInitLeader
    Author:         Adanteh
    Description:    Squad leader related stuff
*/
#include "macros.hpp"

// If the leader is changed to someone random (Autoassign arma because disconnect or whatever)
// allow someone new to Assume SL
["leaderChanged", {
    (_this select 0) params ["_newLeader", "_oldLeader"];

    // -- Only do this for the leader
    if (_newLeader != Clib_Player) exitWith { };
    private _group = group Clib_Player;
    if (group _oldLeader isEqualTo _group) exitWith { };

    private _unitsInGroup = +(units _group);
    private _unitsWithLeaderKit = _unitsInGroup select {
        [_x, "leader"] call FUNC(roleHasAbility);
    };

    // -- If someone has leader kit and he's not the leader, make him squad lead
    if (count _unitsWithLeaderKit > 0) then {
        [_unitsWithLeaderKit select 0] call MFUNC(selectLeader);
    } else {
    // -- If no one with leader kit left, add the 'assume' option which allows anyone to click the button and get SL assigned
        _group setVariable [QGVAR(slAssume), true, true];
    };
}] call CFUNC(addEventHandler);



// -- If you are leader and you switch to the SL kit, and Assume is still turned on, turn it off
[QSVAR(RoleChanged), {
    (_this select 0) params ["_assignedKit"];
    if (leader Clib_Player isEqualTo Clib_Player) then {
        private _hasLeaderKit = [_assignedKit, "leader"] call FUNC(roleHasAbility);
        if (_hasLeaderKit && { (group Clib_Player) getVariable [QGVAR(slAssume), false] }) then {
            (group Clib_Player) setVariable [QGVAR(slAssume), nil, true];
        };
    };
}] call CFUNC(addEventHandler);
