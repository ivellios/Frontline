/*
    Function:       FRL_Medical_fnc_squadParse
    Author:         Adanteh
    Description:    Parses all squads in the current mission
*/
#include "macros.hpp"

GVAR(squadNamespace) = call CFUNC(createNamespace);

private _squadSets = (call MFUNC(getSides)) apply { [_x, "squads"] call MFUNC(getSideData) };
private _squadIndex = 0;
private _allSquads = [];

{
    private _squadSet = _x;
    private _squadSetCfg = (missionConfigFile >> "FRL" >> "Squads" >> _squadSet);
    if !(isClass _squadSetCfg) then {
        // -- If it doesnt exist in the mission, check in mod instead -- //
        _squadSetCfg = (configFile >> "FRL" >> "Squads" >> _squadSet);
    };

    if (isClass _squadSetCfg) then {

        private _squads = configProperties [_squadSetCfg, "isClass _x", true];
        {
            private _squadCfg = _x;
            private _scope = getNumber (_squadCfg >> "scope");
            private _roles = getArray (_squadCfg >> "roles");
            if (_scope >= 2) then {
                if (_roles isEqualTo []) exitWith { };
                private _slug = format ["frl_S%1", _squadIndex];

                // -- Dev bypass for role restrictions. Actual selection code is pretty messy so we do it here
                #ifdef DEBUGFULL
                    {
                        _x set [1, -1];
                    } forEach _roles;
                #endif

                private _properties = (configProperties [_squadCfg, "true", false]) apply { toLower configName _x };
                private _isAIsquad = ("aisquadtype" in _properties) && ({ getNumber (_squadCfg >> "aisquadtype") >= 1 });
                private _side = [east, west, independent, civilian] select (getNumber (_squadCfg >> "side"));
                if (_isAIsquad) then {
                    missionNamespace setVariable [format [QSVAR(side_%1_aiSquad), _side], _slug];
                };

                {
                    _x params ["_valueType", "_value"];
                    GVAR(squadNamespace) setVariable [(_slug + "_" + _valueType), _value];
                } forEach ([
                    ["name", getText (_squadCfg >> "displayName")],
                    ["marker", getText (_squadCfg >> "mapIcon")],
                    ["side", getNumber (_squadCfg >> "side")],

                    // -- Restrictions / rules
                    ["maxSize", getNumber (_squadCfg >> "maxSize")],
                    ["alwaysAvailable", (getNumber (_squadCfg >> "alwaysAvailable") > 0)],
                    ["availableAt", getNumber (_squadCfg >> "availableAt")],
                    ["availableAtArray", getArray (_squadCfg >> "availableAtArray")],
                    ["staticWeapon", getText (_squadCfg >> "staticWeaponType")],
                    ["roles", _roles]
                ]);

                _squadIndex = _squadIndex + 1;
                _allSquads pushBack _slug;
            };
        } forEach _squads;

    } else {
        diag_log format ["[%1 LOG] %2", ["WARNING: ", "Missing squad set in mission: ", QUOTE(PREFIX), _squadSet]];
    };
} forEach _squadSets;

GVAR(squadNamespace) setVariable [QSVAR(Squads), _allSquads];

true;
