/*
    Function:       FRL_Rolesspawns_fnc_clientInitSquadInvite
    Author:         Adanteh
    Description:    Handles squad request join/invite system, used for locked squads
*/
#include "macros.hpp"

// -- Select a locked squad that's not your own, and you can request SL to accept you
DFUNC(squadJoinRequest) = {
    params ["_selectedSquad"];
    private _squadLeader = leader _selectedSquad;
    if !(isPlayer _squadLeader) exitWith { };
    [QGVAR(groupRequestReceive), _squadLeader, [_selectedSquad, Clib_Player]] call CFUNC(targetEvent);
};

// -- Player receiving an invite to join a squad
[QGVAR(groupInviteReceive), {
    (_this select 0) params ["_inviteGroup", "_invitedBy"];
    private _groupName = _inviteGroup getVariable [QMVAR(gName), str _inviteGroup];
    private _question = format ["You are invited to Squad %1 by %2. Do you want to join?", _groupName, ([_invitedBy] call CFUNC(name))];
    [[Clib_Player, [_inviteGroup], _question, {
        params ["", "_args", "_answer"];
        _args params ["_inviteGroup"];
        if (_answer == 1) then {
            if !(isNull _inviteGroup) then {
                if (count units _inviteGroup > 0) then {
                    [_inviteGroup] call FUNC(squadJoin);
                };
            };
        };
    }, 30]] call EFUNC(voting,voteHUD);
}] call CFUNC(addEventHandler);

// -- Group leader receiving a request of player to join locked squad
[QGVAR(groupRequestReceive), {
    (_this select 0) params ["_requestedSquad", "_playerRequesting"];
    private _groupLeader = leader _requestedSquad;
    if (_groupLeader isEqualTo Clib_Player) then {
        private _playerName = [_playerRequesting] call CFUNC(name);
        private _question = format ["%1 Is requesting to join your squad. Accept it?", _playerName];
        [[_groupLeader, [_playerRequesting, _requestedSquad], _question, {
            params ["", "_args", "_answer"];
            _args params ["_playerRequesting", "_requestedSquad"];
            if !(isNull _playerRequesting) then {
                [QGVAR(groupRequestAnswer), _playerRequesting, [_playerRequesting, _requestedSquad, _answer]] call CFUNC(targetEvent);
            };
        }, 30]] call EFUNC(voting,voteHUD);
    };
}] call CFUNC(addEventHandler);

// -- Person requesting to join the locked squad receiving an answer to his request
[QGVAR(groupRequestAnswer), {
    (_this select 0) params ["_playerRequesting", "_requestedSquad", "_answer"];
    if !(_playerRequesting isEqualTo Clib_Player) exitWith { };
    if (_answer == 1) then {
        ["showNotification", ["Request to join squad accepted", "ok"]] call CFUNC(localEvent);
        [_requestedSquad] call FUNC(squadJoin);
    } else {
        ["showNotification", ["Request to join squad denied", "nope"]] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);
