/*
    Function:       FRL_Rolesspawns_fnc_squadInvite
    Author:         Adanteh
    Description:    Invites a unit to squad
*/
#include "macros.hpp"

params [["_unit", objNull], ["_group", grpNull]];
[QGVAR(groupInviteReceive), _unit, [_group, Clib_Player]] call CFUNC(targetEvent);

// TODO Add notification, plus spam protection so you can't invite the same dude 1000 times
