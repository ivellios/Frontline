/*---------------------------------------------------------------------------
    This is a modified version of AAW, July 11th 2016 version
    Released under APL license by the AAW Team.

    Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
    Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
---------------------------------------------------------------------------*/
#include "macros.hpp"

[{
    params ["_unit"];

    if (Clib_Player != leader _unit || Clib_Player == _unit) exitWith {};

    [_unit] join grpNull;

}, _this, "respawn"] call CFUNC(mutex);
