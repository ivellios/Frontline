/*
    Function:       FRL_Rolesspawns_fnc_clientInitRearm
    Author:         Adanteh
    Description:    Adds rearm options on RP / FO
*/
#include "macros.hpp"


// -- Add rearm action to the primary rally point object -- //
GVAR(lastRearm) = -1e5;
[QGVAR(rallyPlaced), {
    (_this select 0) params ["_spawnpoint", "_position", "_caller"];
    if !(group CLib_Player isEqualTo (group _caller)) exitWith { false };

    private _objects = _spawnpoint getVariable ["objects", []];
    private _mainObject = _objects param [0, objNull];
    [_mainObject, [0, 0, 0.4], "REARM", { ["rearm", _this] call FUNC(spawnRearm); }, "rearm_option"] call EFUNC(interactThreeD,addInteractObject);
}] call CFUNC(addEventHandler);
