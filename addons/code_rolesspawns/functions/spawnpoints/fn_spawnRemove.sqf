/*
    Function:       FRL_Rolesspawns_fnc_spawnRemove
    Author:         Adanteh
    Description:    Removes a spawnpoint
*/
#include "macros.hpp"

params ["_spawnpoint", ["_removeObjects", false]];

private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
_spawnpoint setVariable ["deleted", true, true];

if (_removeObjects) then {
    {
        deleteVehicle _x;
    } forEach (_spawnpoint getVariable ["objects", []]);
};

if ((_availableFor isEqualType sideUnknown) || {!(isNull _availableFor)}) then {
    [QSVAR(spawnsChanged), _availableFor] call CFUNC(targetEvent);
};
