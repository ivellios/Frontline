/*
    Function:       FRL_Rolesspawns_fnc_clientInitSpawnMarkers
    Author:         Adanteh
    Description:    Inits the markers for spawn points
*/
#include "macros.hpp"

GVAR(spawnMarkers) = [];
[QSVAR(spawnsChanged), { _this call FUNC(spawnMarkerUpdate) }] call CFUNC(addEventHandler);
