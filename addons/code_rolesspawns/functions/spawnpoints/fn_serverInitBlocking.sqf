/*
    Function:       FRL_Rolesspawns_fnc_serverInitBlocking
    Author:         Adanteh
    Description:    Server init for removal and blocking of spawn points
*/
#include "macros.hpp"

[{
    [GVAR(spawnpoints), {
        params ["_varName", "_spawnpoint"];

        if (_spawnpoint getVariable ["deleted", false]) exitWith { };
        if (_spawnpoint getVariable ["destroyed", false]) exitWith { };

        private _spawnblock_return = "";
        private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
        private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
        private _type = _spawnpoint getVariable ["type", ""];
        private _blocked = ([_type + "_spawnBlock", [_spawnpoint, _availableFor, _position], false] call MFUNC(checkConditions));

        switch (_type) do {
            case "rally": {
                [_spawnpoint, _blocked] call FUNC(spawnRallyBlock);
            };

            case "fo": {
                [_spawnpoint, _blocked] call FUNC(spawnFOBlock);
            };

            case "forward": {
                [_spawnpoint, _blocked] call FUNC(spawnForwardBlock);
            };

            case "vehicle": {
                [_spawnpoint, _blocked] call FUNC(spawnVehicleBlock);
            };
        };

    }] call MFUNC(forEachVariable);
}, 0.5] call MFUNC(addPerFramehandler);
