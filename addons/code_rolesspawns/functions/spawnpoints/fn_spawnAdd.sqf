/*
    Function:       FRL_Rolesspawns_fnc_spawnAdd
    Author:         Adanteh
    Description:    Creates a spawn point
*/
#include "macros.hpp"

params ["_name", "_position", "_availableFor", "_type", "_icon", ["_mapIcon", ""], ["_objects", []]];

private _id = format ["%1_%2", _name, _position];
private _spawnpoint = true call MFUNC(createNamespace);

_spawnpoint setVariable ["id", _id, true];
_spawnpoint setVariable ["name", _name, true];
_spawnpoint setVariable ["position", _position, true]; // posATL 
private _aiSpawnPos = ([_position, 25, 5, "B_UGV_01_rcws_F"] call CFUNC(findSavePosition));
_spawnpoint setVariable ["AISpawnpos", _aiSpawnPos]; // posATL
_spawnpoint setVariable ["availableFor", _availableFor, true];
_spawnpoint setVariable ["type", toLower _type, true];
_spawnpoint setVariable ["icon", _icon, true];

if !(_mapIcon isEqualTo "") then {
    _spawnpoint setVariable ["mapicon", _mapIcon, true];
};

if !(_objects isEqualTo []) then {
    _spawnpoint setVariable ["objects", _objects, true];
};

GVAR(spawnpoints) setVariable [_id, _spawnpoint, true];

private _creationCode = GVAR(spawnpointTypes) getVariable [_type + "#oncreate", {}];
[_spawnpoint, _position, _id] call ([_creationCode] call MFUNC(parseToCode));

[QSVAR(SpawnsChanged), _availableFor] call CFUNC(targetEvent);

_spawnpoint
