/*
    Function:       FRL_Rolesspawns_fnc_getNearUnitsBlocking
    Author:         Adanteh
    Description:    Gets nearby units that can block the point
*/
#include "macros.hpp"

params ["_pos", "_range", "_side"];

if (_side isEqualType grpNull) then {
    _side = side _side;
};

private _enemies = {
    private _sideUnit = side group _x;
    ((alive _x) &&
    { !(_sideUnit in [sideUnknown, civilian, _side]) } &&
    {!([_x] call MFUNC(isUnconscious))}
)} count ([_pos, _range] call MFUNC(getNearUnits));
_enemies
