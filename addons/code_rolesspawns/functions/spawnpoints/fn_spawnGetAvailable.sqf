/*
    Function:       FRL_Rolesspawns_fnc_spawnGetAvailable
    Author:         Adanteh
    Description:    Get all available spawn points for player
*/
#include "macros.hpp"

params [["_includeOtherGroups", false]];

private _availablePoints = [];

[GVAR(spawnpoints), {

    if !(_value getVariable ["deleted", false]) then {
        private _availableFor = _value getVariable ["availableFor", sideUnknown];
        if ((_availableFor isEqualType playerSide && {playerSide == _availableFor}) || (_availableFor isEqualType grpNull && {(group CLib_Player == _availableFor) || ({_includeOtherGroups && ((side _availableFor) == playerSide)})})) then {
            _availablePoints pushBack _value;
        };
    };

}] call MFUNC(forEachVariable);

_availablePoints
