/*
    Function:       FRL_Rolesspawns_fnc_initSpawnRegisterZone
    Author:         Adanteh
    Description:    Registers FO spawn mechanics
*/
#include "macros.hpp"
#define __KEY(var1) [_respawnType, var1] joinString "#"

private _respawnType = "zone";
GVAR(spawnpointTypes) setVariable [__KEY("COLOR"), [1, 1, 1, 1]];
GVAR(spawnpointTypes) setVariable [__KEY("Marker_mouseover"), {}];
GVAR(spawnpointTypes) setVariable [__KEY("spawnPos"), {
    [_spawnpoint] call FUNC(spawnRandomize);
}];

GVAR(spawnpointTypes) setVariable [__KEY("list_entry"), { _name; }];
