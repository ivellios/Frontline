/*
    Function:       FRL_Rolesspawns_fnc_spawnUpdateZone
    Author:         Adanteh
    Description:    This checks the zone array and creates/deletes spawn points for controlling side when zone isn't active
*/
#include "macros.hpp"

{
    private _sector = _x;

	// -- Skip bases -- //
	if !(_sector getVariable ["isBase", false]) then {

		private _sectorSide = _sector getVariable ["side", sideUnknown];

		// -- Allow spawn on non-captureable and non-neutral zones for the team that controls it -- //
		if (_sector getVariable ["isSpawn", false]) then {
            private _spawnpoint = _sector getVariable ["spawnpoint", "##"];
			if !(_spawnpoint isEqualTo "##") then {
				private _sectorName = _sector getVariable ["fullName", "Unknown Sector"];
				private _designator = _sector getVariable ["designator", ""];
				if (_designator != "") then {
					_sectorName = format ["[%1] %2", _designator, _sectorName];
				};
				private _sectorSpawnPos = getPos _x;
                private _marker = _sector getVariable ["marker", ""];
				private _spawnpoint = [_sectorName, _sectorSpawnPos, _sectorSide, "zone", "\A3\ui_f\data\igui\cfg\simpleTasks\types\walk_ca.paa"] call FUNC(spawnAdd);
                _sector setVariable ["spawnpoint", _spawnpoint, false];
                _spawnpoint setVariable ["marker", _marker, true];
			};
		} else {

			private _spawnpoint = _sector getVariable ["spawnpoint", "##"];
			if !(_spawnpoint isEqualTo "##") then {
				[_spawnpoint] call FUNC(spawnRemove);
                _sector setVariable ["spawnpoint", nil, false];
			};
		};
	};

    nil

} count (missionNamespace getVariable [QEGVAR(Frontline,allSectorsArray), []]);
