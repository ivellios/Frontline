/*
    Function:       FRL_Rolessspawns_fnc_serverInitSpawnsZone
    Author:         Adanteh
    Description:    Handles spawn mechanics for AAS mode (Spawning on friendly zones not next to the frontline)
*/
#include "macros.hpp"

["createSector", {
    (_this select 0) params ["_sectorconfig", "_sector"];
    private _sectorType = getText (_sectorconfig >> "sectorType");
    if (_sectorType == "mainbase") then {
        private _cfgName = configName _sectorConfig;
        private _sectorMarker = _sector getVariable ["marker", _cfgName];
        private _spawnMarker = ""; // _sectorMarker; (Don't add by default, only add valid marker entries)

        // -- If there is a 'spawnMarker' entry spawn there instead of middle of the circle
        private _sectorSpawn = getText (_sectorconfig >> "spawnMarker");
        if (_sectorSpawn != "") then {
            if !(getMarkerPos _sectorSpawn isEqualTo [0, 0, 0]) then {
                _spawnMarker = _sectorSpawn;
            };
        };

        // -- Autodetect base spawns
        if (_spawnMarker == "") then {
            _spawnMarker = switch (toLower _cfgName) do {
                case "base_west": { "baseSpawn_west" };
                case "base_east": { "baseSpawn_east" };
                case "base_independent": { "baseSpawn_independent" };
                default { _cfgName };
            };
        };

    	if (_spawnMarker != "") then {
            private _markerPosition = getMarkerPos _spawnMarker;
            private _heightOffset = getNumber (_sectorConfig >> "spawnheight");
            private _spawnName = getText (_sectorConfig >> "designator");
            private _aiSpawnAllow = !(isNumber (_sectorConfig >> "aiSpawnAllow")) || (getNumber (_sectorConfig >> "aiSpawnAllow") > 0);
            private _aiSpawnOnly = getNumber (_sectorConfig >> "aiSpawnOnly") > 0;

            _markerPosition set [2, _heightOffset];
            private _side = switch (markerColor _sectorMarker) do {
                case "ColorWEST": {west};
                case "ColorEAST": {east};
                case "ColorGUER": {independent};
                default {sideUnknown};
            };
            if (_spawnName == "") then {
                _spawnName = "BASE";
            };

            _sector setVariable ["isBase", true, true];
            if (_aiSpawnOnly) then {
                _spawnName = format ["%1 [AI]", _spawnName];
            };
            private _spawnpoint = [_spawnName, _markerPosition, _side, "base", "a3\ui_f\data\map\Markers\Military\box_ca.paa"] call FUNC(spawnAdd);
            _spawnpoint setVariable ["marker", _spawnMarker, true];
            _spawnpoint setVariable ["aiSpawnAllow", _aiSpawnAllow, true];
            _spawnpoint setVariable ["aiSpawnOnly", _aiSpawnOnly, true];
    	};
    };
}] call CFUNC(addEventHandler);

if !(MODULELOADED(Frontline)) exitWith { };

[{
    call FUNC(spawnUpdateZone);
    ["frontlineShift", { _this call FUNC(spawnUpdateZone) }] call CFUNC(addEventhandler);
}, { missionNamespace getVariable [QEGVAR(Frontline,ServerInitDone), false] }] call CFUNC(waitUntil);
