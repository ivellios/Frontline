/*
    Function:       FRL_Rolesspawns_fnc_serverInitSpawnForward
    Author:         Adanteh
    Description:    Inits forward spawn. This is a randomized area spawn, placed by mission maker, will block if enemies come close
*/
#include "macros.hpp"

GVAR(forwardSpawnIndex) = 1;
publicVariable QGVAR(forwardSpawnIndex);
// -- This adds a preset spawn, that if it's inside friendly territory you can spawn on it
["missionStarted", {

    private _markerIndex = 0;
    private _markersSkipped = 0;
    while { _markersSkipped < 10 } do {
        private _spawnMarker = format ["frl_presetSpawn_%1", _markerIndex];
        private _markerPos = (markerPos _spawnMarker);
        if (_markerPos isEqualTo [0, 0, 0]) then {
            _markersSkipped = _markersSkipped + 1;
        } else {
            private _spawnSide = markerColor _spawnMarker;
            private _spawnSide = [markerColor _spawnMarker] call MFUNC(getSideFromString);

            if (_spawnSide != sideUnknown) then {
                [_spawnMarker, _spawnSide] call FUNC(spawnForwardCreate);
            };

        };
        _markerIndex = _markerIndex + 1;
    };

}] call CFUNC(addEventHandler);
