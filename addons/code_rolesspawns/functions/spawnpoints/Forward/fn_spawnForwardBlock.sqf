/*
    Function:       FRL_Rolesspawns_fnc_spawnForwardBlock
    Author:         Adanteh
    Description:    Registers the forward spawn type. Can spawn on friendly area, randomized within a zone
*/
#include "macros.hpp"

params ["_spawnpoint", "_blocked"];

if (_blocked) then {
    _spawnpoint setVariable ["disabled", true, true];
} else {
    if (_spawnpoint getVariable ["disabled", false]) then {
        _spawnpoint setVariable ["disabled", false, true];
    };

    // -- Remove if there is a timeout
    private _timeout = _spawnpoint getVariable ["timeout", -1];
    if (_timeout != -1) then {
        if (serverTime > _timeout) then {
            [_spawnpoint] call FUNC(spawnRemove);
        };
    };
};
