/*
    Function:       FRL_Rolesspawns_fnc_spawnForwardCreate
    Author:         Adanteh
    Description:    Creates a (temporary) forward spawn, spawns you randomly inside given marker
*/
#include "macros.hpp"

params [
    "_marker",
    "_side",
    ["_name", "##"],
    ["_timeout", -1],
    ["_iconMap", "\A3\ui_f\data\igui\cfg\simpleTasks\types\walk_ca.paa"]
];

if (_name isEqualTo "##") then {
    _name = format ["Forward Spawn #%1", GVAR(forwardSpawnIndex)];
    GVAR(forwardSpawnIndex) = GVAR(forwardSpawnIndex) + 1;
    publicVariable QGVAR(forwardSpawnIndex);
};

private _position = getMarkerPos _marker;
private _spawnpoint = [_name, _position, _side, "forward", _icon, _iconMap] call FUNC(spawnAdd);
_spawnpoint setVariable ["marker", _marker, true];

// # NOT IMPLEMENTED
if (_timeout > -1) then {
    _spawnpoint setVariable ["timeout", serverTime + _timeout, true];
};

_marker setMarkerAlpha 0;
_spawnpoint
