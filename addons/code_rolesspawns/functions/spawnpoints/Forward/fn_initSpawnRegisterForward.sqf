/*
    Function:       FRL_Rolesspawns_fnc_initSpawnRegisterForward
    Author:         Adanteh
    Description:    Registers forward spawn mechanics
*/
#include "macros.hpp"
#define __KEY(var1) [_spawnType, var1] joinString "#"

private _spawnType = "forward";
GVAR(spawnpointTypes) setVariable [__KEY("COLOR"), [1, 1, 1, 1]];
GVAR(spawnpointTypes) setVariable [__KEY("Marker_mouseover"), {}];


GVAR(spawnpointTypes) setVariable [__KEY("spawnPos"), {
    [_spawnpoint] call FUNC(spawnRandomize);
}];


GVAR(spawnpointTypes) setVariable [__KEY("list_entry"), {
    if (_spawnpoint getVariable ["disabled", false]) then {
        _name = format ["%1 [Disabled]", _name];
    };
    private _timeout = _spawnpoint getVariable ["timeout", -1];
    if (_timeout != -1) then {
        private _timeleft = [(_timeout - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
        _name = format ["%1 [%2 Remaining]", _name, _timeleft];
    };

    _name
}];


["forward_canSpawn", {
    params ["_spawnpoint", "_position"];

    private _disabled = _spawnpoint getVariable ["disabled", false];
    if (_disabled) exitWith {
        _returnMessage = _spawnpoint getVariable ["blocked_reason", ""];
        if (_returnMessage isEqualTo "") then {
            _returnMessage = "This spawn location is disabled.";
        } else {
            _returnMessage = format ["Blocked: %1", _returnMessage];
        };
        false
    };
    true;
}] call MFUNC(addCondition);
