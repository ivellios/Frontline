/*
    Function:       FRL_Rolesspawns_fnc_clientInitSpawnRally
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"


[QGVAR(postInit), {
    ["Create Rally Point", CLib_Player, 0, {
        [QGVAR(canPlaceRally), FUNC(canPlaceRally), [], 5, QGVAR(ClearRallyPlaceable)] call CFUNC(cachedCall);
    }, {
        QGVAR(ClearRallyPlaceable) call CFUNC(localEvent);
        call FUNC(placeRally);
    }, ["showWindow", false]] call CFUNC(addAction);
}] call CFUNC(addEventHandler);


[QGVAR(rallyPlaced), {
    (_this select 0) params ["_spawnpoint", "_position", "_caller"];
    if !(group CLib_Player isEqualTo (group _caller)) exitWith { false };
    ["showNotification", ["Your squadleader placed a rallypoint"]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


[QGVAR(rallyPlaced), {
    (_this select 0) params ["_spawnpoint", "_position", "_caller"];

    if (CLib_Player isEqualTo _caller) then {
        private _rallyUptime = [QGVAR(Rally_uptime), 90] call MFUNC(cfgSetting);
        private _timeEnd = serverTime + _rallyUptime;
        private _timeStart = serverTime;
        ["progressHudShow", [QGVAR(rallyUptime),
        {
            (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\respawn.paa");
            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.222,0.7,0.222,0.6];
        },
        {
            params ["_spawnpoint", "_timeStart", "_rallyUptime"];
            private _deleted = _spawnpoint getVariable ["deleted", false];
            private _id = (group CLib_Player) getVariable [QGVAR(rallyID), objNull];
            if (_deleted || (isNull _spawnPoint) || (_id != _spawnpoint)) then {
                ["progressHudHide", [QGVAR(rallyUptime)]] call CFUNC(localEvent);
            };
            private _timeLeft = ((_timeStart + _rallyUptime) - serverTime) max 0;
            private _percentage = (0 max (1 - ((serverTime - _timeStart) / _rallyUptime))) min 1;
            (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
            (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Rally %1", ([_timeLeft, "MM:SS", false] call BIS_fnc_secondsToString)]);
        },
        [_spawnpoint, _timeStart, _rallyUptime]]] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);

[QGVAR(rallyRemoved), {
    [{
        if (leader CLib_Player isEqualTo CLib_Player) then {
            private _waitTime = [QGVAR(Rally_waitTime), 150] call MFUNC(cfgSetting);
            private _lastRallyPlaced = (group CLib_Player) getVariable [QGVAR(lastRallyPlaced), -_waitTime];
            private _rallyCooldown = (_lastRallyPlaced + _waitTime - serverTime);
            private _timeStart = serverTime;

            if (_rallyCooldown > 0) then {
                ["progressHudShow", [QGVAR(rallyCooldown),
                {
                    (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\respawn.paa");
                    (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.8,0.333,0.333,0.5];
                },
                {
                    params ["_timeStart", "_rallyCooldown", "_group"];
                    private _timeLeft = ((_timeStart + _rallyCooldown) - serverTime) max 0;
                    if (_timeLeft <= 0 || {!(((group CLib_Player) isEqualTo _group) && {((leader (group CLib_Player)) isEqualTo CLib_Player)})}) then {
                        ["progressHudHide", [QGVAR(rallyCooldown)]] call CFUNC(localEvent);
                    };
                    private _percentage = (0 max ((serverTime - _timeStart) / _rallyCooldown)) min 1;
                    (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
                    (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Rally Cooldown %1", ([_timeLeft, "MM:SS", false] call BIS_fnc_secondsToString)]);
                },
                [_timeStart, _rallyCooldown, group Clib_Player]]] call CFUNC(localEvent);
            };
        };
    }, 1.5] call CFUNC(wait);
}] call CFUNC(addEventHandler);

// -- Reset vars -- //
[QGVAR(rallyRemoved), {
    if (leader CLib_Player isEqualTo CLib_Player) then {
        (group CLib_Player) setVariable [QGVAR(rallyTimeout), nil, true];
        (group CLib_Player) setVariable [QGVAR(rallyID), nil, true];
    };
}] call CFUNC(addEventHandler);
