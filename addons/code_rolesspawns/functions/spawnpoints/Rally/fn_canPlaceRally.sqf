/*
    Function:       FRL_RolesSpawns_fnc_canPlaceRally
    Author:         Adanteh
    Description:    Checks if you can place rallypoint down
	Example:		[] call FRL_RolesSpawns_fnc_canPlaceRally


	This is a modified version of AAW_Deployment_fnc_canPlaceRally, July 2nd 2016 version
	Released under APL license by the AAW Team.

	Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
	Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
*/
#include "macros.hpp"

params [["_unit", Clib_Player]];

if (leader _unit != _unit) exitWith { false };

// Check vehicle
if (vehicle _unit != _unit) exitWith { false };

// Check cooldown
private _waitTime = [QGVAR(Rally_waitTime), 10] call MFUNC(cfgSetting);
private _lastRallyPlaced = (group _unit) getVariable [QGVAR(lastRallyPlaced), -_waitTime];
if (serverTime - _lastRallyPlaced < _waitTime) exitWith { false };

// Check modular stack condition (RP role, sector conditions)
private _position = getPosATL _unit;
private _canPlace = ["placeRPRealtime", [_unit, _position], true] call MFUNC(checkConditions);
if !(_canPlace) exitWith { false };

// Check near players
private _nearPlayerToBuild = [QGVAR(Rally_nearPlayerToBuild), -1] call MFUNC(cfgSetting);
if (_nearPlayerToBuild > 0 && {
    private _unitGroup = group _unit;
	private _nearPlayerToBuildRadius = [QGVAR(Rally_nearPlayerToBuildRadius), -1] call MFUNC(cfgSetting);
	private _count = {(group _x) isEqualTo _unitGroup} count ([_unit, _nearPlayerToBuild] call MFUNC(getNearUnits));
	(_count < _nearPlayerToBuild)
}) exitWith {
	false
};

true
