/*
    Function:       FRL_Rolesspawns_fnc_spawnShowRangeRP
    Author:         Adanteh
    Description:    Shows rally point ranges (Indicating where you can place them)
*/
#include "macros.hpp"


private _minDistance = [QGVAR(Rally_minDistanceEnemyZone), 100] call MFUNC(cfgSetting);
private _sectorArray = +(missionNamespace getVariable [QEGVAR(Frontline,allSectorsArray), []]);
{
    private _sector = _x;
    private _sectorMarker = _sector getVariable ["marker", ""];
    private _sectorSize = selectMax (getMarkerSize _sectorMarker);
    
    if !((_sector getVariable ["side", sideUnknown]) in [sideUnknown, playerSide]) then {
        _markerSize = _minDistance + _sectorSize;
        private _marker = createMarkerLocal [_sectorMarker + "_placeCondition", getPosATL _sector];
        _marker setMarkerShapeLocal "ELLIPSE";
        _marker setMarkerColorLocal "ColorRed";
        _marker setMarkerAlphaLocal 0.3;
        _marker setMarkerBrushLocal "SolidBorder";
        _marker setMarkerSizeLocal [_markerSize, _markerSize];
        _markers pushBack _marker;
    };
    nil
} count _sectorArray;
