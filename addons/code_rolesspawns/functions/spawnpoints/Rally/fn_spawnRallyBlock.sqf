/*
    Function:       FRL_Rolesspawns_fnc_spawnRallyBlock
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_spawnpoint", ["_blocked", false]];
private _availableFor = _spawnpoint getVariable ["availableFor", grpNull];

if !(_blocked) then {
    if (isNull _availableFor) exitWith {
        _spawnblock_return = "Squad doesn't exist";
        _blocked = true;
    };

    if (serverTime >= (_availableFor getVariable [QGVAR(rallyTimeout), (serverTime + 3)])) exitWith {
        _spawnblock_return = "RP timed out";
        _blocked = true;
    };

    private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
    private _blockCount = [QGVAR(Rally_maxEnemyCount), 2] call MFUNC(cfgSetting);
    private _blockRadius = [QGVAR(Rally_maxEnemyCountRadius), 40] call MFUNC(cfgSetting);
    private _nearUnits = [_position, _blockRadius, _availableFor] call FUNC(getNearUnitsBlocking);

    _blocked = (_nearUnits >= _blockCount);
    _spawnblock_return = "Enemies nearby";
};

if (_blocked) then {
    [_spawnpoint, _availableFor] call FUNC(spawnRallyDestroy);
};
