/*
    Function:       FRL_Rolesspawns_fnc_initSpawnRegisterRally
    Author:         Adanteh
    Description:    Registers forward spawn mechanics
*/
#include "macros.hpp"
#define __KEY(var1) [_spawnType, var1] joinString "#"

[QGVAR(Rally), configFile >> "FRL" >> "CfgSquadRallyPoint"] call MFUNC(cfgSettingLoad);
[QGVAR(Rally), missionConfigFile >> "FRL" >> "CfgSquadRallyPoint"] call MFUNC(cfgSettingLoad);

private _spawnType = "rally";
GVAR(spawnpointTypes) setVariable [__KEY("COLOR"), [0, 0.87, 0, 1]];
GVAR(spawnpointTypes) setVariable [__KEY("Marker_mouseover"), {
    params ["_spawnpoint"];

    private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
    private _availableForPlayer = group CLib_Player isEqualTo _availableFor;
    if (_availableForPlayer) then {
        private _timeout = (group CLib_Player) getVariable [QGVAR(rallyTimeout), -1];
        private _timeleft = [(_timeout - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
        _text = format ["%1 [%2 Remaining]", _text, _timeleft];
    };
}];

GVAR(spawnpointTypes) setVariable [__KEY("list_entry"), {
    private _timeout = _availableFor getVariable [QGVAR(rallyTimeout), -1];
    private _timeleft = [(_timeout - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
    (format ["%1 [%2]", _name, _timeleft]);
}];
