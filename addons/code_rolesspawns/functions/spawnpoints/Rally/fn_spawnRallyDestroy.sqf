/*
    Function:       FRL_Rolesspawns_fnc_spawnRallyDestroy
    Author:         Adanteh
    Description:    Removes a rally
*/
#include "macros.hpp"

params ["_spawnpoint"];
private _availableFor = _spawnpoint getVariable ["availableFor", grpNull];
private _id = _spawnpoint getVariable ["id", "##"];

[_spawnpoint, true] call FUNC(spawnRemove);

if (_spawnpoint isEqualTo (_availableFor getVariable [QGVAR(rallyId), objNull])) then {
	[QGVAR(rallyRemoved), _availableFor] call CFUNC(targetEvent);
};
