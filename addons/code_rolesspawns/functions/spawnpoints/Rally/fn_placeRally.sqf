 /*---------------------------------------------------------------------------
     This is a modified version of AAW_Deployment_fnc_placeRally, July 2nd 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"

[{
    if (!(call FUNC(canPlaceRally))) exitWith {};

    private _caller = CLib_Player;
    private _targetGroup = group _caller;

     // -- Place it exactly on caller. It might look weird, but doesn't give any problems and makes sure that you can only place it at locations the player can actually reach
    private _position = getPosATL _caller;

    // -- Do non-real time calls. If these conditions fail they will show a notification themselves. -- //
    // -- Action menu option will be shown if these return false for better understanding of the rally point placement conditions -- //
    private _canPlace = ["placeRPonCall", [_caller, _position], true] call MFUNC(checkConditions);
    if !(_canPlace) exitWith { false };

    // -- Change to single obj only and use createSimpleObject instead -- //
    private _pointObject = [side _targetGroup, "rally"] call MFUNC(getSideData);
    _pointObject = createVehicle [_pointObject, _position, [], 0, "CAN_COLLIDE"];
    _pointObject setPosATL (getPosATL _caller);
    ["enableSimulation", [_pointObject, false]] call CFUNC(serverEvent);

    private _rallyUptime = [QGVAR(Rally_uptime), 90] call MFUNC(cfgSetting);
    private _rallyName = format ["RP %1", groupID _targetGroup];
    private _spawnpoint = [_rallyName, _position, _targetGroup, "rally", MEDIAPATH + "icons\rally_ca.paa", MEDIAPATH + "icons\rally_ca.paa", [_pointObject]] call FUNC(spawnAdd);

    _targetGroup setVariable [QGVAR(rallyTimeout), (serverTime + _rallyUptime), true];
    _targetGroup setVariable [QGVAR(lastRallyPlaced), serverTime, true];
    _targetGroup setVariable [QGVAR(rallyId), _spawnpoint, true];

    // -- Use side, broadcasting to group isn't possible from non-server
    [QGVAR(rallyPlaced), [_spawnpoint, _position, _caller]] call CFUNC(globalEvent);
}, [], "respawn"] call CFUNC(mutex);
