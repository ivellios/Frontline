/*
    Function:       FRL_Rolesspawns_fnc_serverInitSpawnTemporary
    Author:         Adanteh
    Description:    Inits Temporary spawn. This is a randomized area spawn, placed by mission maker, will block if enemies come close
*/
#include "macros.hpp"
