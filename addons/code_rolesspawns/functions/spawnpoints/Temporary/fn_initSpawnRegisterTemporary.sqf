/*
    Function:       FRL_Rolesspawns_fnc_initSpawnRegisterTemporary
    Author:         Adanteh
    Description:    Registers Temporary spawn mechanics
*/
#include "macros.hpp"
#define __KEY(var1) [_spawnType, var1] joinString "#"

[QGVAR(Temporary), configFile >> "FRL" >> "CfgTemporarySpawn"] call MFUNC(cfgSettingLoad);
[QGVAR(Temporary), missionConfigFile >> "FRL" >> "CfgTemporarySpawn"] call MFUNC(cfgSettingLoad);

private _spawnType = "Temporary";
GVAR(spawnpointTypes) setVariable [__KEY("COLOR"), [0, 0.87, 0, 1]];
GVAR(spawnpointTypes) setVariable [__KEY("Marker_mouseover"), {
    params ["_spawnpoint"];

    private _timeout = _spawnpoint getVariable ["timeout", -1];
    private _timeleft = [(_timeout - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
    _text = format ["%1 [%2 Remaining]", _text, _timeleft];
}];

//GVAR(spawnpointTypes) setVariable [__KEY("spawnPos"), { }];

GVAR(spawnpointTypes) setVariable [__KEY("list_entry"), {
    private _timeout = _spawnpoint getVariable ["timeout", -1];
    private _timeleft = [(_timeout - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
    (format ["%1 [%2]", _name, _timeleft]);
}];

GVAR(spawnpointTypes) setVariable [__KEY("oncreate"), {
    private _timeout = [QGVAR(temporary_timeout), 120] call MFUNC(settingLoad);
    _spawnpoint setVariable ["timeout", _timeout, true];
}];

["Temporary_canSpawn", {
    params ["_spawnpoint", "_position"];

    private _disabled = _spawnpoint getVariable ["disabled", false];
    if (_disabled) exitWith {
        _returnMessage = _spawnpoint getVariable ["blocked_reason", ""];
        if (_returnMessage isEqualTo "") then {
            _returnMessage = "This spawn location is disabled.";
        } else {
            _returnMessage = format ["Blocked: %1", _returnMessage];
        };
        false
    };
    true;
}] call MFUNC(addCondition);
