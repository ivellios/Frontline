/*
    Function:       FRL_Rolesspawns_fnc_spawnFOPlayNoise
    Author:         Adanteh
    Description:    Plays radio sounds on the FO
*/
#include "macros.hpp"

params ["_spawnpoint"];

if (isNull _spawnpoint) exitWith { };
if (_spawnpoint getVariable ["destroyed", false]) exitWith { };

// -- Make sure the same sound doesn't get played too often -- //
private _lastSounds = _spawnpoint getVariable [QGVAR(soundsPlayed), []];
if (count _lastSounds >= 6) then {
    _lastSounds deleteAt 0;
};

private _soundList = [
    ["radio\ambient_radio2", 10],
    ["radio\ambient_radio3", 11],
    ["radio\ambient_radio4", 07],
    ["radio\ambient_radio5", 09],
    ["radio\ambient_radio6", 07],
    ["radio\ambient_radio7", 05],
    ["radio\ambient_radio8", 12],
    ["radio\ambient_radio9", 08],
    ["radio\ambient_radio10", 11],
    ["radio\ambient_radio11", 06],
    ["radio\ambient_radio13", 06],
    ["radio\ambient_radio14", 07],
    ["radio\ambient_radio15", 08],
    ["radio\ambient_radio16", 11],
    ["radio\ambient_radio17", 06],
    ["radio\ambient_radio18", 10],
    ["radio\ambient_radio19", 10],
    ["radio\ambient_radio20", 06],
    ["radio\ambient_radio21", 04],
    ["radio\ambient_radio22", 05],
    ["radio\ambient_radio23", 08],
    ["radio\ambient_radio24", 08],
    ["radio\ambient_radio25", 10],
    ["radio\ambient_radio26", 08],
    ["radio\ambient_radio30", 09],
    ["UI\uav\UAV_01", 04, 4.5],
    ["UI\uav\UAV_02", 11, 4.5],
    ["UI\uav\UAV_03", 05, 4.5],
    ["UI\uav\UAV_04", 08, 4.5],
    ["UI\uav\UAV_05", 08, 4.5],
    ["UI\uav\UAV_06", 17, 4.5],
    ["UI\uav\UAV_07", 10, 4.5]] - _lastSounds;

private _soundToPlay = selectRandom _soundList;
private _soundPath = format ["a3\sounds_f\sfx\%1.wss", _soundToPlay select 0];
_lastSounds pushBack _soundToPlay;
_spawnpoint setVariable [QGVAR(soundsPlayed), _lastSounds];

private _soundLength = _soundToPlay param [1, 12];
private _soundStrength = _soundToPlay param [2, 1];
private _object = (_spawnpoint getVariable ["objects", []]) param [0, objNull];

playSound3D [_soundPath, _object, false, getPosASL _object, (_soundStrength * 2.5), 1, 100];
[{ _this call FUNC(spawnFOPlayNoise) }, (_soundLength + random 5), [_spawnpoint]] call CFUNC(wait);
