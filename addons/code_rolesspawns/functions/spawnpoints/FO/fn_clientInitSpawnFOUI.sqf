/*
    Function:       FRL_Rolesspawns_fnc_clientInitSpawnFOUI
    Author:         Adanteh
    Description:    FO notifications, messages and other things
*/
#include "macros.hpp"

GVAR(currentFO) = objNull;
// -- NOTIFICATIONS
// -- Disabled
[QGVAR(foRebuild), {
    (_this select 0) params ["_spawnpoint", "_position", "_caller"];
    if (isNil "_position") exitWith { }; // -- JIP support

    private _locationText = [_position] call MFUNC(getNearestName);
    ["showNotification", [format ["%1 rebuild an FO near %2", [_caller] call CFUNC(name), _locationText]]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


[QGVAR(foPlaced), {
    (_this select 0) params ["_spawnpoint", "_position", "_caller"];
    if (isNil "_position") exitWith { }; // -- JIP support
    if (isNull _caller) exitWith { }; // -- preplaced FOBs dont get announced

    private _locationText = [_position] call MFUNC(getNearestName);
    ["showNotification", [format ["%1 placed an FO near %2", [_caller] call CFUNC(name), _locationText]]] call CFUNC(localEvent);
    [_position, _spawnpoint] call FUNC(foPlacedTooltip);
}] call CFUNC(addEventHandler);


[QGVAR(foDestroyed), {
    (_this select 0) params ["_spawnpoint"];
    private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
    private _name = ["Enemy FOB", _spawnpoint getVariable ["name", "Friendly FOB"]] select (_availableFor isEqualTo playerSide);
    ["showNotification", [format["%1 Destroyed", _name]]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


// -- CAPTURE PROGRESS HUD (IN TOP LEFT)
// -- Status SHOW -- //
[QGVAR(foEnter), {
    (_this select 0) params ["_spawnpoint", "_availableFor", "_units"];
    if !(Clib_Player in _units) exitWith {  };

    GVAR(currentFO) = _spawnpoint;

    private _name = ["Enemy FOB", _spawnpoint getVariable ["name", "Friendly FOB"]] select (_availableFor isEqualTo playerSide);

    ["progressHudShow", [
        str _spawnpoint,
        {
            params ["_spawnpoint", "_availableFor", "_name"];
            (_controlsGroup controlsGroupCtrl 2) ctrlSetText ([_availableFor, "flag"] call MFUNC(getSideData));
            (_controlsGroup controlsGroupCtrl 3) ctrlSetText _name;
            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor ([_availableFor, "color"] call MFUNC(getSideData));
            (_controlsGroup controlsGroupCtrl 5) progressSetPosition 1;
        },
        {
            params ["_spawnpoint", "_availableFor", "_name"];
            if (_spawnpoint getVariable ["destroyed", false]) then {
                (_controlsGroup controlsGroupCtrl 3) ctrlSetText (_name + " (Destroyed)");
            } else {

                private _activatedAt = _spawnpoint getVariable ["activateAt", -1];
                if (_activatedAt > serverTime) then {
                    private _blockTime = [(_activatedAt - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
                    _name = format ["%1 [Active in %2]", _name, _blockTime];
                } else {
                    private _blockedTime = _spawnpoint getVariable ["blocked", -1];
                    if (_blockedTime > serverTime) then {
                        private _blockReason = _spawnpoint getVariable ["blocked_reason", ""];
                        _name = format ["%1 [Blocked %2] %3", _name, [ceil (_blockedTime - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString, _blockReason];
                    };
                };
                (_controlsGroup controlsGroupCtrl 3) ctrlSetText _name;
            };

        },
        [_spawnpoint, _availableFor, _name]]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


[QGVAR(foLeave), {
    (_this select 0) params ["_spawnpoint", "_availableFor", "_units"];
    if !(Clib_Player in _units) exitWith {  };

    ["progressHudHide", [str _spawnpoint]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);
