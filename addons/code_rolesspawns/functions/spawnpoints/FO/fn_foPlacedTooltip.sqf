/*
    Function:       FRL_Main_fnc_foPlacedTooltip
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_position", "_spawnpoint"];

// -- no need to disturb unconscious players
if (!(alive CLib_Player) || !(isNil QGVAR(bleedoutInfo)) || [CLib_Player] call FUNC(isUnconscious)) exitWith {};

[{
    params ["_spawnpoint", "_position"];

    if (!(alive CLib_Player) || !(isNil QGVAR(bleedoutInfo)) || [CLib_Player] call FUNC(isUnconscious)) exitWith {};
    private _actitvateAt = _spawnpoint getVariable ["activateAt", -1];
    if (_activatedAt <= serverTime) exitWith {};
    if (_position distance2D getPosASL CLib_Player > 50) exitWith {};

    private _txt = "The FO you placed can be activated!<br/><br/>Doing this will bypass the activation time<br/>and can be done after supplies have been unloaded from vehicles.<br/>This will not consume supplies";
    private _icon = "\pr\frl\addons\main\icons\av\fast_forward.paa";

    /*
    private _suppliesNearby = false;
    private _vehList = (getpos _spawnpoint) nearObjects ["AllVehicles", 50];
    _vehList append ((getpos _spawnpoint) nearObjects [QSVAR(SupplyContainer), 100]);
    {
        private _vehicle = _x;
        _supplyValue = _vehicle getVariable ["suppliesLoaded", 0];
        if (_supplyValue > 0 && (getpos _vehicle) select 2 <= 3) exitWith {
            _suppliesNearby = true;
            _txt = _txt + "<br/>you already have vehicles nearby to unload from at the FO";
        };
    } foreach _vehList;
    */

    [_txt, _icon, _color] call MFUNC(tooltipShow);
}, 15, [_spawnpoint, _position]] call CFUNC(wait);
