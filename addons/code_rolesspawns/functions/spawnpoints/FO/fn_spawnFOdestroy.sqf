/*
    Function:       FRL_RolesSpawns_fnc_spawnFOdestroy
    Author:         Adanteh
    Description:    This destroy FO, creating destruction effect and preparing the cleanup
	Example:		[(cursorTarget getVariable "spawnpoint")] call FRL_RolesSpawns_fnc_spawnFOdestroy;
*/
#include "macros.hpp"

params ["_spawnpoint", ["_destroyedBy", objNull]];

private _pointName = _spawnpoint getVariable ["name", ""];
private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
private _objects = _spawnpoint getVariable ["objects", []];
private _mainObject = _objects param [0, objNull];
private _position = _spawnpoint getVariable ["position", [0, 0, 0]];

// -- Let other scripts know that the object is not an active FO anymore -- //
_spawnpoint setVariable ["destroyed", true, true];
_pos = getPosATL _mainObject;
_pos set [2, (_pos select 2) + 0.4];
_destructEffect = QSVAR(FO_Explosion) createVehicle _pos;
_destructEffect setPosATL _pos;

[QGVAR(foDestroyed), [_spawnpoint, _position, _destroyedBy]] call CFUNC(globalEvent);
