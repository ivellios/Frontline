/*
    Function:       FRL_Rolesspawns_fnc_createFO
    Author:         Adanteh
    Description:    Creates the FO (no checks, should be able to be called through debug and work)
    Example:        [player getPos [5, 0]] call FRL_Rolesspawns_fnc_createFO;
*/
#include "macros.hpp"

params ["_objectPosition", "_position", ["_dir", getDir Clib_Player], "_vectorUp", ["_side", playerSide], ["_caller", Clib_Player], ["_activateTime", -1], ["_supplies", 0]];

if (isNil "_position") then {
    _position = [_objectPosition, [2, 0, 0], _dir + 180] call MFUNC(calcRelativePosition);
};

if (isNil "_vectorUp") then {
    _vectorUp = surfaceNormal _objectPosition;
};

private _pointObjects = [_side, "fo"] call MFUNC(getSideData);
_pointObjects = _pointObjects apply {
    _x params ["_type", "_offset", ["_dirOffset", 0]];

    private _objPosition = _objectPosition vectorAdd _offset;
    private _obj = createVehicle [_type, _objPosition, [], 0, "CAN_COLLIDE"];
    _obj enableSimulation false; // -- Do this instantly local, so we don't need to wait for server
    _obj allowDamage false;
    _obj setDir (_dir + _dirOffset);
    _obj setPosATL _objPosition;
    _obj setVectorUp _vectorUp;
    ["enableSimulation", [_obj, false]] call CFUNC(serverEvent);

    _obj
};

private _foIndex = missionNamespace getVariable [format [QGVAR(foIndex_%1), _side], 0];
_foIndex = _foIndex + 1;
missionNamespace setVariable [format [QGVAR(foIndex_%1), _side], _foIndex];
publicVariable format [QGVAR(foIndex_%1), _side];

private _spawnpoint = [format ["FOB #%1", _foIndex], _position, _side, "fo", MEDIAPATH + "icons\fob_ca.paa", MEDIAPATH + "icons\fob_ca.paa", _pointObjects] call FUNC(spawnAdd);
(_pointObjects select 0) setVariable ["spawnpoint", _spawnpoint, true];
(_pointObjects select 0) setVariable [QSVAR(interactMenuCategory), QSVAR(fo), true];
// -- Link extra objects to the first one, this is used for things like interaction detection
if (count _pointObjects > 1) then {
    for "_i" from 1 to (count _pointObjects - 1) do {
        (_pointObjects select _i) setVariable [QSVAR(proxy), (_pointObjects select 0), true];
    };
};

private _targetEvent = _side;
if !(isServer) then { _targetEvent = [_side, 2]; };
[QGVAR(foPlaced), _targetEvent, [_spawnpoint, _position, _caller, _activateTime, _supplies]] call CFUNC(targetEvent);
