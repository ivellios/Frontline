/*
    Function:       FRL_RolesSpawns_fnc_canDestroyFO
    Author:         Adanteh
    Description:    Checks if FO can be destroyed
*/
#include "macros.hpp"

params ["_foTarget"];
private _canDestroy = ["destroyFO", [_foTarget], true] call MFUNC(checkConditions);
_canDestroy;
