/*
    Function:       FRL_RolesSpawns_fnc_foRebuild
    Author:         Adanteh
    Description:    Rebuilds a destroyed FO (Possible within the 5 minute timer)
    Example:        [cursorTarget] call FRL_RolesSpawns_fnc_foRebuild;
*/
#include "macros.hpp"

params ["_foTarget"];

// -- Get details
private _caller = CLib_Player;
private _spawnpoint = _foTarget getVariable ["spawnpoint", objNull];
private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
private _rebuild = true;
private _canPlace = ["placeFOonCall", [CLib_Player, _position, _rebuild, _foTarget], true] call MFUNC(checkConditions);
if !(_canPlace) exitWith { // -- The FO stack will give a message, so just do a quiet exit
    false
};

// -- Set it to blocked so its not an instant spawn
private _blockDuration = [QGVAR(FO_blockDuration), 60] call MFUNC(cfgSetting);
[_spawnpoint, _blockDuration] call FUNC(spawnDisable);

// -- Restore the settings to a non-destroyed status
_spawnpoint setVariable ["icon", (MEDIAPATH + "icons\fob_ca.paa"), true];
_spawnpoint setVariable ["mapIcon", (MEDIAPATH + "icons\fob_ca.paa"), true];
_spawnpoint setVariable ["destroyed", false, true];

private _targetEvent = playerSide;
if !(isServer) then { _targetEvent = [playerSide, 2]; };

[QGVAR(foRebuild), _targetEvent, [_spawnpoint, _position, _caller]] call CFUNC(targetEvent);
