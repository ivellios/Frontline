/*
    Function:       FRL_Rolesspawns_fnc_spawnFOblockOnIncap
    Author:         Adanteh
    Description:    Blocks FOs if a friendly goes incap (Exclude suicide and teamkill)
    Example:        [player, [objNull]] call FRL_Rolesspawns_fnc_spawnFOblockOnIncap
*/
#include "macros.hpp"

params ["_victim", "_killers"];

//[[_fnc_scriptNameShort, _this], "red"] call MFUNC(debugMessage);
if ([_victim] isEqualTo _killers) exitWith { }; // -- Suicide

private _killerFirst = _killers param [0, objNull];
private _killerSide = side group _killerFirst;
private _friendlySide = (side group _victim);
if (_friendlySide getFriend _killerSide >= 0.6) exitWith { }; // -- Teamkill

// -- Check for nearby FO
call {
    scopeName _fnc_scriptNameShort;
    private _nearbySpawnpoint = objNull;
    private _range = [QGVAR(FO_incapBlockRange), 25] call MFUNC(cfgSetting);
    private _incapPos = getPosATL _victim;
    [GVAR(spawnpoints), {
        if (_value getVariable ["type", "fo"] isEqualTo "fo") then {
            if ((_value getVariable ["availableFor", sideUnknown]) isEqualTo _friendlySide) then {
                if ((_value getVariable ["position", [0, 0, 0]]) distance _incapPos < _range) then {
                    _nearbySpawnpoint = _value;
                    breakTo "spawnFOblockOnIncap";
                };
            };
        };
    }] call MFUNC(forEachVariable);

    //[[_fnc_scriptNameShort, _nearbySpawnpoint, _incapPos, _range], "red"] call MFUNC(debugMessage);

    if !(isNull _nearbySpawnpoint) then {
        private _spawnblock_return = "Friendly got shot";
        private _incapBlockDuration = [QGVAR(FO_incapBlockDuration), 45] call MFUNC(cfgSetting);
        [_nearbySpawnpoint, true, _incapBlockDuration] call FUNC(spawnFOBlock);
    };
};
