/*
    Function:       FRL_Rolesspawns_fnc_spawnFOdestroyAction
    Author:         Adanteh
    Description:    Handles activating FO FO
*/
#include "macros.hpp"


params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {
    //
    //
    // Condition to grey out the interact menu option
    case "condition": {
        _args params ["_caller", "_target", "_menuTarget"];
        private _spawnpoint = _target getVariable ["spawnpoint", objNull];

        if (_spawnpoint getVariable ["destroyed", false]) exitWith {
            ["showNotification", ["FO is destroyed", "nope"]] call CFUNC(localEvent);
            _return = false;
        };

        if !((_spawnpoint getVariable ["availableFor", sideUnknown]) isEqualTo playerSide) exitWith {
            ["showNotification", ["Can't activate enemy FOs", "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };


    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_target", ["_targetRealtime", {}], ["_quiet", false]];
        private _supplyRequired = [QGVAR(FO_activateSupplies), 500] call MFUNC(cfgSetting);
        private _spawnpoint = _target getVariable ["spawnpoint", objNull];
        private _activateAt = _spawnpoint getVariable ["activateAt", -2];

        if (serverTime >= _activateAt) exitWith {
            if !(_quiet) then {
                ["showNotification", ["FO is already activated", "ok"]] call CFUNC(localEvent);
            };
            _return = false;
        };

        private _supplies = _spawnpoint getVariable ["supplies", 0];
        if (_supplies < _supplyRequired) then {
            if !(_quiet) then {
                private _message = format ["Not enough supply to activate.<br />Need %1 supplies", _supplyRequired];
                ["showNotification", [_message, "nope"]] call CFUNC(localEvent);
            };
            _return = false;
        } else {
            //_spawnpoint setVariable ["supplies", _supplies - _supplyRequired, true];
            // -- Don't subtract supplies for now, gameplay decision, don't want people to not activate if there's a little bit of time left to save supply
            _spawnpoint setVariable ["activateAt", -2, true];
            if !(_quiet) then {
                ["showNotification", ["FO succesfully activated", "ok"]] call CFUNC(localEvent);
            };
            _return = true;
        };
    };
};

_return;
