/*
    Function:       FRL_Rolesspawns_fnc_placeFO
    Author:         Adanteh
    Description:    Handles placing an FO, called from player
*/
#include "macros.hpp"

params ["_mode"];

switch (toLower _mode) do {
    // -- Go into placement mode  -- //
	case "preview": {
		GVAR(validPlacement) = false;
		GVAR(validPlacementReason) = "";
		GVAR(previewObject) = objNull;
		if (!(call FUNC(canPlaceFO))) exitWith { };
		[QMVAR(showControlsHUD), [
			["To place the FO", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa"], { ["place"] call FUNC(placeFO) }, true, [0], { GVAR(validPlacement) }, { GVAR(validPlacementReason) }],
			["To cancel placement", ["\a3\3DEN\Data\Displays\Display3DEN\Hint\rmb_ca.paa"], { ["cancel"] call FUNC(placeFO) }, true, [1]]
		]] call CFUNC(localEvent);

		// -- Having sights up automatically raises weapon again, so if someone has his sights up, make sure we put those down first -- //
		[true] call MFUNC(disableMouse);
		call FUNC(previewFO);
	};

	case "place": {
		[{
		    if (!(call FUNC(canPlaceFO))) exitWith {};
		    if !(GVAR(validPlacement)) exitWith {};

			private _objectPosition = getPosATLVisual GVAR(previewObject);
			private _canPlace = ["placeFOonCall", [CLib_Player, _objectPosition], true] call MFUNC(checkConditions);
			if !(_canPlace) exitWith { false };

		    // -- Change to single obj only and use createSimpleObject instead -- //
		    GVAR(placementMode) = false;
		    private _dir = getDirVisual GVAR(previewObject);
		    private _position = GVAR(previewObject) getPos [3, _dir - 180];
		    _position set [2, (_objectPosition select 2)];
		    private _vectorUp = vectorUpVisual GVAR(previewObject);

            [_objectPosition, _position, _dir, _vectorUp, side group Clib_Player, Clib_Player] call FUNC(createFO);
            deleteVehicle GVAR(previewObject);

		    ["cancel"] call FUNC(placeFO);

		}, [], "respawn"] call CFUNC(mutex);
	};

	case "cancel": {
		if !(isNull GVAR(previewObject)) then {
			deleteVehicle GVAR(previewObject);
			GVAR(previewObject) = objNull;
		};


		GVAR(placementMode) = false;
		[QMVAR(hideControlsHUD)] call CFUNC(localEvent);
		["forceWalk","construction", false] call CFUNC(setStatusEffect);
		[false] call MFUNC(disableMouse);
	};
};
