/*
    Function:       FRL_Rolesspawns_fnc_spawnFOhandleControl
    Author:         Adanteh
    Description:    Handles FO control range (Fortification) for friendly units
*/
#include "macros.hpp"

[GVAR(spawnpoints), {

    if !((_value getVariable ["type", ""]) isEqualTo "fo") exitWith { };

    private _friendlyInside = _value getVariable ["friendlyInside", []];
    private _availableFor = _value getVariable ["availableFor", sideUnknown];

    // -- Call leave event for everyone in this FO range
    if (_value getVariable ["deleted", false]) exitWith {
        if !(_friendlyInside isEqualTo []) then {
            [QGVAR(foLeave), _friendlyInside, [_value, _availableFor]] call CFUNC(targetEvent);
            _value setVariable ["friendlyInside", []];
        };
    };

    if (_value getVariable ["destroyed", false]) exitWith {
        if !(_friendlyInside isEqualTo []) then {
            [QGVAR(foLeave), _friendlyInside, [_value, _availableFor]] call CFUNC(targetEvent);
            _value setVariable ["friendlyInside", []];
        };
    };

    private _position = _value getVariable ["position", [0, 0, 0]];
    private _friendlyCurrent = [];
    private _enemyCurrent = [];
    {
        if (!([_x] call MFUNC(isUnconscious)) && { alive _x }) then {
            if ((side group _x) getFriend _availableFor >= 0.6) then {
                _friendlyCurrent pushBack _x;
            } else {
                if (_position distance _x < GVAR(blockRadius)) then {
                    _enemyCurrent pushBack _x;
                };
            };
        };
        nil;
    } count ([_position, GVAR(buildRadius)] call MFUNC(getNearUnits));

    private _friendlyLeft = _friendlyInside - _friendlyCurrent - [objNull];
    private _friendlyEntered = _friendlyCurrent - _friendlyInside - [objNull];

    if !(_friendlyEntered isEqualTo []) then {
        [QGVAR(foEnter), _friendlyEntered, [_value, _availableFor, _friendlyEntered]] call CFUNC(targetEvent);
    };

    if !(_friendlyLeft isEqualTo []) then {
        [QGVAR(foLeave), _friendlyLeft, [_value, _availableFor, _friendlyLeft]] call CFUNC(targetEvent);
    };

    _value setVariable ["friendlyInside", _friendlyCurrent, false];
    _value setVariable ["enemyInside", _enemyCurrent, false];

}] call MFUNC(forEachVariable);
