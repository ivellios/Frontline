/*
    Function:       FRL_Rolesspawns_fnc_spawnFOpackAction
    Author:         Adanteh
    Description:    Handles packing up FO (Doesn't block area, allows replacement)
*/
#include "macros.hpp"


params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {
    //
    //
    // Called every frame while the hold action button is still pressed, return should be true to keep action going, false to exit it
    case "callback": {
        _args params ["_caller", "_target", "_menuTarget", "_spawnpoint"];
        if !(_target isEqualTo (call _menuTarget)) exitWith { // -- Not aiming at vehicle anymore
            _return = false;
        };

        if (_spawnpoint getVariable ["destroyed", false]) exitWith {
            ["showNotification", ["FO is destroyed", "nope"]] call CFUNC(localEvent);
            _return = false;
        };

        _return = (alive _caller && { !([_caller] call MFUNC(isUnconscious)) })
    };



    //
    //
    // Condition to grey out the interact menu option
    case "condition": {
        _args params ["_caller", "_target", "_menuTarget", "_spawnpoint"];

        if !((_spawnpoint getVariable ["availableFor", sideUnknown]) isEqualTo playerSide) exitWith {
            ["showNotification", ["Can't move enemy FOs", "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };


    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_target", "_targetRealtime"];
        private _duration = [QGVAR(FO_packupDuration), 30] call MFUNC(cfgSetting);
        private _spawnpoint = _target getVariable ["spawnpoint", objNull];
        #ifdef DEBUGFULL
            _duration = 2;
        #endif

        [
            "Packing",
            format ["%1\code_rolesspawns\data\pack-up.paa", QUOTE(BASEPATH)],
            _duration,
            [_caller, _target, _targetRealtime, _spawnpoint],
            { ["condition", _this] call FUNC(spawnFOpackAction) },
            { ["callback", _this] call FUNC(spawnFOpackAction) },
            { ["end", _this] call FUNC(spawnFOpackAction) },
            true,
            true,
            10,
            _dikButton // -- This is passed from menuUseActionKey
        ] call MFUNC(holdActionStart);
    };


    //
    //
    // Called when we press the button from the interact menu
    case "end": {
        _args params ["_data", "_exitCode"];
        if (_exitCode isEqualTo 0) then {
            _data params ["_caller", "_target", "_menuTarget", "_spawnpoint"];
            [_spawnpoint, true] call FUNC(spawnRemove);
        };
    };
};

_return;
