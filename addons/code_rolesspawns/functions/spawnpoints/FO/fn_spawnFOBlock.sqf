/*
    Function:       FRL_Rolesspawns_fnc_spawnFOblock
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_spawnpoint", ["_blocked", false], ["_blockDuration", -1]];

if (_spawnpoint getVariable ["destroyed", false]) exitWith { };
if !(_blocked) then {

    // -- Check if there's more enemies than friendly nearby (Detected by spawnFOhandleControl)
    private _availableFor = _spawnpoint getVariable ["availableFor", grpNull];
    private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
    private _blockCount = [QGVAR(FO_blockCount), 2] call MFUNC(cfgSetting);
    private _nearUnitDifference = count (_spawnpoint getVariable ["enemyInside", []]) - (count (_spawnpoint getVariable ["friendlyInside", []]));

    _blocked = (_nearUnitDifference >= _blockCount);
    _spawnblock_return = "Enemies nearby";
};

if (_blocked) then {
    if (_blockDuration isEqualTo -1) then {
        _blockDuration = [QGVAR(FO_blockDuration), 60] call MFUNC(cfgSetting);
    };

    [_spawnpoint, _blockDuration] call FUNC(spawnDisable);
    if !(_spawnpoint getVariable ["blocked_reason", ""] isEqualTo _spawnblock_return) then {
        _spawnpoint setVariable ["blocked_reason", _spawnblock_return, true];
    };
};
