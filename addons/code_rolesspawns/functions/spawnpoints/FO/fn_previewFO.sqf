#include "macros.hpp"
/*
 *	File: fn_previewFO.sqf
 *	Author: Adanteh
 *	Handles the previewing of construction / FO placement
 *
 *	Example:
 *	[player] call FUNC(previewFO);
 */


// -- Having sights up automatically raises weapon again, so if someone has his sights up, make sure we put those down first -- //
["forceWalk","construction", true] call CFUNC(setStatusEffect);
if (!weaponLowered CLib_Player) then {
	if (local CLib_Player) then {
		if (cameraView == "GUNNER") then { CLib_Player switchCamera "INTERNAL" };
	};
	CLib_Player action ["WeaponOnBack", CLib_Player];
};


private _pointObjects = [playerSide, "fo"] call MFUNC(getSideData) select 0;
_pointObjects params ["_objectType", "_objectOffset"];
private _objPosition = CLib_Player modelToWorldVisual [0, 3, 0];
if ((_objPosition select 2) < 0) then { _objPosition set [2, 0]; };
private _previewObject = _objectType createVehicleLocal [0, 0, 0];

GVAR(previewObject) = _previewObject;
GVAR(placementMode) = true;


[{
	(_this select 0) params ["_previewObject"];

	if !(GVAR(placementMode)) exitWith {
		[_this select 1] call MFUNC(removePerFrameHandler);
	};

	private _objPosition = CLib_Player modelToWorldVisual [0, 3, 0.3];
	_ins = lineIntersectsSurfaces [
		AGLtoASL (_objPosition),
		AGLtoASL (_objPosition vectorAdd [0, 0, -0.8]),
		_previewObject,
		CLib_Player,
		true
	];


	_previewObject setDir (getDirVisual CLib_Player);

	private _validPlacement = true;

	private ["_vectorUp"];
	if (count _ins > 0) then {
		_objPosition = ASLToATL (_ins select 0 select 0);
		if (_objPosition select 2 < 0) then { _objPosition set [2, 0]; };
		_previewObject setPosATL _objPosition;
		_vectorUp = (_ins select 0 select 1);
	} else {
		_objPosition set [2, 0];
		_previewObject setPosATL _objPosition;
		_vectorUp = (surfaceNormal _objPosition);
	};

	if (_vectorUp select 2 <= 0.85) then {
		GVAR(validPlacementReason) = "Too steep";
		_validPlacement = false;
	} else {
		_previewObject setVectorUp _vectorUp;
	};

	// -- Validation starts here -- //
	if (true) then {
		if (count (lineIntersectsSurfaces [
			AGLtoASL (_previewObject modelToWorldVisual [0, 0, 0.5]),
			eyePos CLib_Player,
			_previewObject,
			CLib_Player,
			true
		]) > 0) exitWith {
			_validPlacement = false;
			GVAR(validPlacementReason) = "Need LOS";
		};

		// -- Back top -- //
		if (count (lineIntersectsSurfaces [
			AGLtoASL (_previewObject modelToWorldVisual [-0.4, 0.3, 0.5]),
			AGLtoASL (_previewObject modelToWorldVisual [0.4, 0.3, 0.5]),
			_previewObject,
			objNull,
			true
		]) > 0) exitWith {
			_validPlacement = false;
			GVAR(validPlacementReason) = "Invalid Location";
		};

		// -- Front top -- //
		if (count (lineIntersectsSurfaces [
			AGLtoASL (_previewObject modelToWorldVisual [-0.4, -0.3, 0.5]),
			AGLtoASL (_previewObject modelToWorldVisual [0.4, -0.3, 0.5]),
			_previewObject,
			objNull,
			true
		]) > 0) exitWith {
			_validPlacement = false;
			GVAR(validPlacementReason) = "Invalid Location";
		};
	};

	GVAR(validPlacement) = _validPlacement;
}, 0, [_previewObject]] call MFUNC(addPerFramehandler);
