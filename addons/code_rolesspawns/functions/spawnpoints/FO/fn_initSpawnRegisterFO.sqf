/*
    Function:       FRL_Rolesspawns_fnc_initSpawnRegisterFO
    Author:         Adanteh
    Description:    Registers FO spawn mechanics
*/
#include "macros.hpp"
#define __KEY(var1) [_respawnType, var1] joinString "#"


[QGVAR(FO), configFile >> "FRL" >> "CfgFOB"] call MFUNC(cfgSettingLoad);
[QGVAR(FO), missionConfigFile >> "FRL" >> "CfgFOB"] call MFUNC(cfgSettingLoad);


private _respawnType = "fo";
GVAR(spawnpointTypes) setVariable [__KEY("color"), [0.57, 0, 0.58, 1]];
GVAR(spawnpointTypes) setVariable [__KEY("Marker_mouseover"), {
    params ["_spawnpoint"];

    if (_spawnpoint getVariable ["destroyed", false]) then {
        private _blocked = _spawnpoint getVariable ["blocked", -1];
        private _timeleft = [(_blocked - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
        _text = format ["%1 [Destroyed] (%2 Till Removed)", _text, _timeleft];
    } else {

        private _activatedAt = _spawnpoint getVariable ["activateAt", -1];
        if (_activatedAt > serverTime) then {
            private _blockTime = [(_activatedAt - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
            _text = format ["%1 [Active in %2]", _text, _blockTime];
        };
    };
}];

GVAR(spawnpointTypes) setVariable [__KEY("list_entry"), {
    private _blocked = _spawnpoint getVariable ["blocked", -1];
    if !(_spawnpoint getVariable ["destroyed", false]) then {

        private _activatedAt = _spawnpoint getVariable ["activateAt", -1];
        if (_activatedAt > serverTime) then {
            private _blockTime = [(_activatedAt - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
            _name = format ["%1 [Active in %2]", _name, _blockTime];

        } else {
            if (_blocked > serverTime) then {
                private _blockTime = [(_blocked - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
                private _blockReason = _spawnpoint getVariable ["blocked_reason", ""];
                _name = format ["%1 [Blocked %2] %3", _name, _blockTime, _blockReason];
            };
        };
    } else {
        _name = format ["%1 [Destroyed]", _name];
    };
    _name
}];


["fo_canSpawn", {
    params ["_spawnpoint", "_position"];

    private _destroyed = _spawnpoint getVariable ["destroyed", false];
    private _deleted = _spawnpoint getVariable ["deleted", false];
    if (_destroyed || _deleted) exitWith {
        _returnMessage = "This spawn location is destroyed!";
        false
    };

    private _activatedAt = _spawnpoint getVariable ["activateAt", -1];
    if (_activatedAt > serverTime) exitWith {
        _returnMessage = "This spawn location is not active yet!";
        false
    };

    private _blocked = (_spawnpoint getVariable ["blocked", -1]) > serverTime;
    if (_blocked) exitWith {
        _returnMessage = _spawnpoint getVariable ["blocked_reason", ""];
        if (_returnMessage isEqualTo "") then {
            _returnMessage = "This spawn location is blocked!";
        } else {
            _returnMessage = format ["Blocked: %1", _returnMessage];
        };
        false
    };

    true;
}] call MFUNC(addCondition);

GVAR(buildRadius) = [QGVAR(fo_buildRadius), 100] call MFUNC(cfgSetting);
GVAR(blockRadius) = [QGVAR(fo_blockRadius), 40] call MFUNC(cfgSetting);
