/*
    Function:       FRL_Rolesspawns_fnc_spawnFOdestroyAction
    Author:         Adanteh
    Description:    Handles destroyign FO
*/
#include "macros.hpp"


params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {
    //
    //
    // Called every frame while the hold action button is still pressed, return should be true to keep action going, false to exit it
    case "callback": {
        _args params ["_caller", "_target", "_menuTarget", "_spawnpoint"];
        if !(_target isEqualTo (call _menuTarget)) exitWith { // -- Not aiming at vehicle anymore
            _return = false;
        };

        if (_spawnpoint getVariable ["destroyed", false]) exitWith {
            ["showNotification", ["FO is already destroyed", "nope"]] call CFUNC(localEvent);
            _return = false;
        };

        _return = (alive _caller && { !([_caller] call MFUNC(isUnconscious)) })
    };



    //
    //
    // Condition to grey out the interact menu option
    case "condition": {
        _args params ["_caller", "_target", "_menuTarget", "_spawnpoint"];

        if ((_spawnpoint getVariable ["availableFor", sideUnknown]) isEqualTo playerSide) exitWith {
            ["showNotification", ["Can't destroy friendly FOs", "nope"]] call CFUNC(localEvent);
            _return = false;
        };

        private _canDestroyMessage = "Can't destroy the FO";
        private _canDestroy = ["destroyFOonCall", [side group _caller, _spawnpoint], true] call MFUNC(checkConditions);
        if !(_canDestroy) exitWith {
            ["showNotification", [_canDestroyMessage, "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };


    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_target", "_targetRealtime"];
        private _duration = [QGVAR(FO_destroyDuration), 30] call MFUNC(cfgSetting);
        private _spawnpoint = _target getVariable ["spawnpoint", objNull];
        #ifdef DEBUGFULL
            _duration = 2;
        #endif

        [
            "Destroy",
            format ["%1\code_rolesspawns\data\demolish.paa", QUOTE(BASEPATH)],
            _duration,
            [_caller, _target, _targetRealtime, _spawnpoint],
            { ["condition", _this] call FUNC(spawnFODestroyAction) },
            { ["callback", _this] call FUNC(spawnFODestroyAction) },
            { ["end", _this] call FUNC(spawnFODestroyAction) },
            true,
            true,
            10,
            _dikButton // -- This is passed from menuUseActionKey
        ] call MFUNC(holdActionStart);
    };


    //
    //
    // Called when we press the button from the interact menu
    case "end": {
        _args params ["_data", "_exitCode"];
        if (_exitCode isEqualTo 0) then {
            _data params ["_caller", "_target", "_menuTarget", "_spawnpoint"];
            [_spawnpoint, _caller] call FUNC(spawnFOdestroy);
        };
    };
};

_return;
