/*
    Function:       FRL_RolesSpawns_fnc_spawnFOblockArea
    Author:         Adanteh
    Description:    This will control an FO as 'destroyed' and block the area for a couple minutes after being destroyed
    Locality:       Server only
*/
#include "macros.hpp"

params ["_spawnpoint"];

private _waitTime =  [QGVAR(FO_destroyBlockTime), 300] call MFUNC(cfgSetting);

_spawnpoint setVariable ["icon", (MEDIAPATH + "icons\fob_destroy_ca.paa"), true];
_spawnpoint setVariable ["mapicon", (MEDIAPATH + "icons\fob_destroy_ca.paa"), true];
_spawnpoint setVariable ["blocked", (_waitTime + serverTime), true];

[{
    params ["_spawnpoint"];
    [_spawnpoint] call FUNC(spawnRemove);
}, _waitTime, [_spawnpoint]] call CFUNC(wait);
