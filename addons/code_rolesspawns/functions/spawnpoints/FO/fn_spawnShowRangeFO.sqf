/*
    Function:       FRL_Rolesspawn_fnc_spawnShowRangeFO
    Author:         Adanteh
    Description:    Shows the FO ranges (Markers that indicate where you can place an FO)
*/
#include "macros.hpp"

private _minDistance = [QGVAR(FO_minDistanceEnemyZone), 200] call MFUNC(cfgSetting);
private _maxDistance = [QGVAR(FO_maxDistanceFriendlyZone), 400] call MFUNC(cfgSetting);

// -- this is only bases?
[missionNamespace getVariable [QMVAR(allSectors), objNull], {
    private _sector = _value;
    private _sectorMarker = _sector getVariable ["marker", ""];
    private _sectorSize = selectMax (getMarkerSize _sectorMarker);

    if ((_sector getVariable ["side", sideUnknown]) != playerSide) then {
        _markerSize = _minDistance + _sectorSize;
        private _marker = createMarkerLocal [_sectorMarker + "_placeCondition", getPosATL _sector];
        _marker setMarkerShapeLocal "ELLIPSE";
        _marker setMarkerColorLocal "ColorRed";
        _marker setMarkerAlphaLocal 0.4;
        _marker setMarkerBrushLocal "SolidBorder";
        _marker setMarkerSizeLocal [_markerSize, _markerSize];
        _markers pushBack _marker;
    } else {
        _markerSize = _maxDistance + _sectorSize;
        private _marker = createMarkerLocal [_sectorMarker + "_placeCondition", getPosATL _sector];
        _marker setMarkerShapeLocal "ELLIPSE";
        _marker setMarkerColorLocal QSVAR(ColorGreen);
        _marker setMarkerAlphaLocal 0.5;
        _marker setMarkerBrushLocal QMVAR(DShaded2);
        _marker setMarkerSizeLocal [_markerSize, _markerSize];
        _markers pushBack _marker;
    };
}] call MFUNC(forEachVariable);

private _spacingNormal = [QGVAR(FO_spacingDistance), 300] call MFUNC(cfgSetting);
private _spacingDestroyed = [QGVAR(FO_destroyBlockRange), 200] call MFUNC(cfgSetting);
private _maxChainDistance = [QGVAR(FO_maxChainDistance), 400] call MFUNC(cfgSetting);
private _targetPosition = getPosATL CLib_Player;
private _withinSpacing = false;

// -- FO Chaining
/*
[GVAR(spawnpoints), {
    private _type = _value getVariable ["type", ""];

    if (_value getVariable ["deleted", false]) exitWith { };
    if (_type isEqualTo "fo") then {
        if ((_value getVariable ["availableFor", sideUnknown]) isEqualTo playerSide) then {
            private _position = _value getVariable ["position", [0, 0, 0]];

            private _destroyed = _value getVariable ["destroyed", false];
            if (_destroyed) exitWith { };

            private _marker = createMarkerLocal [(_value getVariable ["id", ""]) + "_placeCondition2", _position];
            _marker setMarkerShapeLocal "ELLIPSE";
            _marker setMarkerColorLocal QSVAR(ColorGreen);
            _marker setMarkerAlphaLocal 0.4;
            _marker setMarkerBrushLocal QMVAR(DShaded2);
            _marker setMarkerSizeLocal [_maxChainDistance, _maxChainDistance];
            _markers pushBack _marker;

        };
    };
}] call MFUNC(forEachVariable);
*/

// -- This is in a separate loop so it goes on top
[GVAR(spawnpoints), {
    private _type = _value getVariable ["type", ""];

    if (_value getVariable ["deleted", false]) exitWith { };
    if (_type isEqualTo "fo") then {
        if ((_value getVariable ["availableFor", sideUnknown]) isEqualTo playerSide) then {
            private _position = _value getVariable ["position", [0, 0, 0]];

            private _destroyed = _value getVariable ["destroyed", false];
            private _spacing = [_spacingNormal, _spacingDestroyed] select _destroyed;

            private _marker = createMarkerLocal [(_value getVariable ["id", ""]) + "_placeCondition", _position];
            _marker setMarkerShapeLocal "ELLIPSE";
            _marker setMarkerColorLocal "ColorRed";
            _marker setMarkerAlphaLocal 0.5;
            _marker setMarkerBrushLocal "SolidBorder";
            _marker setMarkerSizeLocal [_spacing, _spacing];
            _markers pushBack _marker;

            private _marker = createMarkerLocal [(_value getVariable ["id", ""]) + "_buildCondition", _position];
            _marker setMarkerShapeLocal "ELLIPSE";
            _marker setMarkerColorLocal "ColorBlue";
            _marker setMarkerAlphaLocal 0.3;
            _marker setMarkerBrushLocal "SolidBorder";
            _marker setMarkerSizeLocal [GVAR(buildRadius), GVAR(buildRadius)];
            _markers pushBack _marker;

        };
    };
}] call MFUNC(forEachVariable);

private _txt = "<br/><t color='#FF0000' align='center'>FOB spacing</t><br/><br/>" + "<t color='#0000FF' align='center'>Fortifcation build range</t>";
private _icon = ICON(device,gps_fixed);
private _color = "white";

[_txt, _icon, _color] call MFUNC(tooltipShow);

[QGVAR(ShowRangeFO)] call CFUNC(localEvent);
