/*
    Function:       FRL_Rolesspawns_fnc_serverInitFO
    Author:         Adanteh
    Description:    Inits server-side FO mechanics
*/
#include "macros.hpp"

// -- Play ambient radio sound at the FO -- //
{
    [_x, {
        (_this select 0) params ["_spawnpoint", "_position", "_caller"];
        [{ _this call FUNC(spawnFOPlayNoise) }, random 2, [_spawnpoint]] call CFUNC(wait);
    }] call CFUNC(addEventHandler);
} forEach [QGVAR(foPlaced), QGVAR(foRebuild)];

// -- Activation timer (Circumvent by delivering supply to it)
[QGVAR(foPlaced), {
    (_this select 0) params ["_spawnpoint", "_position", "_caller", ["_timeToActivate", -1], ["_supplies", -1]];
    if (_supplies <= 0) then {
        _supplies = _spawnpoint getVariable ["supplies", 0];
    };
    if (_timeToActivate < 0) then {
        _timeToActivate = _spawnpoint getVariable ["activationTime", "noVal"];
        if (_timeToActivate isEqualTo "noVal") then {
            _timeToActivate = [QGVAR(FO_activateTime), 300] call MFUNC(cfgSetting);
        };
    };
    if (_timeToActivate > -1) then {
        _spawnpoint setVariable ["activateAt", serverTime + _timeToActivate, true];
    };

    _spawnpoint setVariable ["supplies", _supplies, true];
    #ifdef DEBUGFULL
        _spawnpoint setVariable ["supplies", 1000 + _supplies, true];
    #endif
}] call CFUNC(addEventHandler);

// -- Blocks the area of the destroyed FO for a while
[QGVAR(foDestroyed), { _this select 0 call FUNC(spawnFOblockArea) }] call CFUNC(addEventHandler);

// -- Block FO when a friendly goes incapped
if (([QGVAR(FO_incapBlock), -1] call MFUNC(cfgSetting)) > 0) then {
    ["incapPlayer", { (_this select 0) call FUNC(spawnFOblockOnIncap) }] call CFUNC(addEventHandler);
};

// -- Handles people entering and leaving FO range
[{ _this call FUNC(spawnFOHandleControl) }, 0.5] call MFUNC(addPerFramehandler);

// -- Autroactivate the FO if we add enough supplies for it
["suppliesUnload", {
    (_this select 0) params ["_spawnpoint", "_forwardOutpost", "_supplies"];
    ["start", [objNull, _forwardOutpost, nil, true]] call FUNC(spawnFOActivateAction);
}] call CFUNC(addEventHandler);


// -- Give ticket gain for FO destruction
[QGVAR(foDestroyed), {
    (_this select 0) params ["_spawnpoint", "", "_destroyedBy"];
    private _side = _spawnpoint getVariable ["availableFor", sideUnknown];
    if !(_side isEqualTo sideUnknown) then {
        private _ticketGain = [QGVAR(FO_ticketGain), 0] call MFUNC(cfgSetting);
        if (_ticketGain > 0) then {
            ["adjustTicket", [_ticketGain, side group _destroyedBy, false]] call CFUNC(localEvent);
        };
    };
}] call CFUNC(addEventHandler);

// for some reason this needs a delay, the setVariable stuff just takes a bit, also lets DFL load
[{
    {
        private _obj = _x;
        private _side = _obj getVariable ["side", west];
        private _activationTime = _obj getVariable ["timeToActivate", -1];
        private _supplies = _obj getVariable ["supplies", 0];

        private _pos = getPos _obj;
        private _dir = getDir _obj;
        private _vectorUp = vectorUpVisual _obj;
        deleteVehicle _obj;
        [_pos, _position, _dir, _vectorUp, _side, ObjNull, _activationTime, _supplies] call FUNC(createFO);
    } foreach allMissionObjects "FRL_FO_Box1";
}, 15] call CFUNC(wait);
