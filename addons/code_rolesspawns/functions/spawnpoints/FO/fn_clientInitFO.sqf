/*
    Function:       FRL_Rolesspawns_fnc_clientInitFO
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

[QGVAR(postInit), {
    ["Create FO", CLib_Player, 0, {
        [QGVAR(isFOPlaceable), FUNC(canPlaceFO), [], 5, QGVAR(clearFOPlaceable)] call CFUNC(cachedCall);
    }, {
        QGVAR(clearFOPlaceable) call CFUNC(localEvent);
        ["preview"] call FUNC(placeFO);
    }, ["showWindow", false]] call CFUNC(addAction);
}] call CFUNC(addEventHandler);


// -- PLACEMENT CONDITIONS
// -- Nearby players
["placeFOonCall", {
    params ["_caller", "_pos", ["_rebuild", false]];
    if (_rebuild) exitWith { true }; // -- Allow rebuilding on yhour own
    private _nearPlayerToBuild = [QGVAR(FO_nearPlayerToBuild), -1] call MFUNC(cfgSetting);

    if ({side group _caller == side group _x} count allPlayers <= 3) then {
        _nearPlayerToBuild = -1;
        ["debugMessage", ["Bypassing required players for placement due to low numbers", "orange"]] call CFUNC(localEvent);
    };
    #ifdef DEBUGFULL
        _nearPlayerToBuild = -1;
    #endif

    private _canPlace = (_nearPlayerToBuild <= 1 || {
        private _nearPlayerToBuildRadius = [QGVAR(FO_nearPlayerToBuildRadius), -1] call MFUNC(cfgSetting);
        private _nearUnits = [ASLtoAGL (getPosASL _caller), _nearPlayerToBuildRadius] call MFUNC(getNearUnits);
        private _count = {((side group _x) == playerSide && !([_x] call MFUNC(isUnconscious))) } count _nearUnits;  // -- This excludes player, so do +1
        (_count >= _nearPlayerToBuild)
    });
    if !(_canPlace) then {
        ["showNotification", [format ["You need %1 friendlies nearby to build", _nearPlayerToBuild - 1], "nope"]] call CFUNC(localEvent);
    };
    _canPlace
}] call MFUNC(addCondition);


// -- Spacing to other FOs
["placeFOonCall", {
    params ["_caller", "_pos", ["_rebuild", false], ["_targetRebuild", objNull]];
    private _spacingNormal = [QGVAR(FO_spacingDistance), 300] call MFUNC(cfgSetting);
    private _spacingDestroyed = [QGVAR(FO_destroyBlockRange), 200] call MFUNC(cfgSetting);
    private _withinSpacing = false;
    [GVAR(spawnpoints), {
        if (_value getVariable ["deleted", false]) exitWith { };

        private _type = _value getVariable ["type", ""];
        if !(_type isEqualTo "fo") exitWith { };

        private _destroyed = _value getVariable ["destroyed", false];
        if (_destroyed && { _targetRebuild isEqualTo _value }) exitWith { };

        private _spacing = [_spacingNormal, _spacingDestroyed] select _destroyed;
        private _availableFor = _value getVariable ["availableFor", sideUnknown];
        private _position = _value getVariable ["position", [0, 0, 0]];
        if ((_availableFor isEqualTo playerSide) && {(_pos distance2D _position) <= _spacing}) exitWith {
            _withinSpacing = true;
        };

        if (_withinSpacing) exitWith { nil };
    }] call MFUNC(forEachVariable);

    if (_withinSpacing) then {
        ["showNotification", [format ["You can't build within %1m of another FO (%2m for destroyed FOs)", _spacingNormal, _spacingDestroyed], "nope"]] call CFUNC(localEvent);
    };

    !_withinSpacing
}] call MFUNC(addCondition);

{
    [QSVAR(fo), _x] call EFUNC(interactthreed,menuOptionAdd);
} forEach [
    ["", "Rearm", { ["rearm"] call FUNC(spawnRearm) }, { ["condition", _this] call FUNC(spawnRearm) }],
    ["", "Demolish", { ["start", _this] call FUNC(spawnFOdestroyAction) }],
    ["", "Pack up", { ["start", _this] call FUNC(spawnFOpackAction) }],
    ["", "Activate", { ["start", _this] call FUNC(spawnFOActivateAction) }, { ["condition", _this] call FUNC(spawnFOActivateAction) }]
];
