#include "macros.hpp"
/*
 *	File: fn_canPlaceFO.sqf
 *	Author: Adanteh
 *	Describe your function
 *
 *	Example:
 *	[player] call FUNC(canPlaceFO);
 */

// -- Check leader
if (leader CLib_Player != CLib_Player) exitWith {false};

// -- Check vehicle
if (vehicle CLib_Player != CLib_Player) exitWith {false};

// -- Max distance checks -- //
private _position = getPosATL Clib_Player;
private _canPlace = ["placeFORealtime", [CLib_Player, _position], true] call MFUNC(checkConditions);
_canPlace;
