/*
    Function:       FRL_Rolesspawns_fnc_spawnShowRanges
    Author:         Adanteh
    Description:    Creates markers indicating where you can place a given spawn type
*/
#include "macros.hpp"

params ["_type", ["_autoHide", -1], ["_update", false]];

private _markers = [];
private _markersCurrent = +(missionNamespace getVariable [QGVAR(conditionMarkers), []]);
if (count _markersCurrent > 0) then {
    {
        deleteMarkerLocal _x;
        nil;
    } count _markersCurrent;
};

switch (toLower _type) do {
	case "fo": {
        [] call FUNC(spawnShowRangeFO);
    };

	case "rally": {
        [] call FUNC(spawnShowRangeRP);
    };
};

missionNamespace setVariable [QGVAR(conditionMarkers), _markers];

if !(_update) then {
    GVAR(conditionMarkerTimeout) = [-1, serverTime + _autoHide] select (_autoHide != -1);

	// -- Update the markers and autoremove after a while -- //
	if ((missionNamespace getVariable [QGVAR(conditionMarkerHandle), -1]) != -1) then {
		[GVAR(conditionMarkerHandle)] call MFUNC(removePerFrameHandler);
	};

	GVAR(conditionMarkerHandle) = [{
		(_this select 0) params ["_type"];
		if ((GVAR(conditionMarkerTimeout) != -1) && { serverTime >= GVAR(conditionMarkerTimeout)}) then {
			{ deleteMarkerLocal _x, nil; } count (missionNamespace getVariable [QGVAR(conditionMarkers), []]);
			missionNamespace setVariable [QGVAR(conditionMarkers), nil];
			[_this select 1] call MFUNC(removePerFrameHandler);
			GVAR(conditionMarkerHandle) = -1;
		} else {
			[_type, nil, true] call FUNC(showRanges);
		};
	}, 1, [_type]] call MFUNC(addPerFramehandler);
};

_markers;
