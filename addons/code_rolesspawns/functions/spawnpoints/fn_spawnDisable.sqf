 /*
    Function:       FRL_Roelsspawns_fnc_spawnDisable
    Author:         Adanteh
    Description:    Disables a spawnpoint for given time
*/
#include "macros.hpp"

params ["_spawnpoint", ["_disabledFor", -1]];

private _currentDuration = _spawnpoint getVariable ["blocked", -1];

// -- Don't overwrite higher disabled times
//if (_currentDuration > (_disabledFor + serverTime)) exitWith { };

// -- Don't update too often -- //
if (abs(_currentDuration - (_disabledFor + serverTime)) > 1) then {
	_spawnpoint setVariable ["blocked", _disabledFor + serverTime, true];
};
