/*
    Function:       FRL_Rolesspawns_fnc_spawnTypeData
    Author:         Adanteh
    Description:    Gets data for a registered spawn type
    Example:        ['fo', 'color'] call FRL_Rolesspawns_fnc_spawnTypeData
*/
#include "macros.hpp"

params ["_type", "_value"];

private _return = GVAR(spawnPointTypes) getVariable ([_type, _value] joinString "#");

// -- Default return
if (isNil "_return") then {
    _return = switch (toLower _value) do {
        case "color": { [0, 0.87, 0, 1] };
        case "marker_mouseover": { {} };
        case "spawnpos": { { nil } };
        case "afterspawn": { { } };
        case "list_entry": { { _name } };
        default { "" };
    };
};


_return;
