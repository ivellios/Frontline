class Spawnpoints {
    class clientInitRearm;
    class clientInitSpawnMarkers;
    class getNearUnitsBlocking;
    class serverInitBlocking;
    class serverInitSpawns;

    class spawnAdd;
    class spawnDeploy;
    class spawnDisable;
    class spawnGetAvailable;
    class spawnMarkerUpdate;
    class spawnRandomize;
    class spawnRearm;
    class spawnRemove;
    class spawnShowRanges;
    class spawnTypeData;

    class FO {
        class canDestroyFO;
        class canPlaceFO;
        class clientInitFO;
        class clientInitSpawnFOUI;
        class createFO;
        class foPlacedTooltip;
        class foRebuild;
        class initSpawnRegisterFO;
        class placeFO;
        class previewFO;
        class serverInitFO;
        class spawnFOActivateAction;
        class spawnFOBlock;
        class spawnFOblockArea;
        class spawnFODestroy;
        class spawnFODestroyAction;
        class spawnFOblockOnIncap;
        class spawnFOhandleControl;
        class spawnFOpackAction;
        class spawnFOPlayNoise;
        class spawnShowRangeFO;
    };

    class Forward {
        class initSpawnRegisterForward;
        class serverInitSpawnForward;
        class spawnForwardBlock;
        class spawnForwardCreate;
    };

    class Rally {
        class canPlaceRally;
        class clientInitSpawnRally;
        class initSpawnRegisterRally;
        class placeRally;
        class spawnRallyBlock;
        class spawnRallyDestroy;
        class spawnShowRangeRP;
    };

    class Temporary {
        class initSpawnRegisterTemporary;
        class serverInitSpawnTemporary;
    };

    class Vehicle {
        class clientInitSpawnVehicle;
        class initSpawnRegisterVehicle;
        class serverInitSpawnVehicle;
        class spawnVehicleBlock;
        class spawnVehicleGetTickets;
        class spawnVehicleHUD;
    };

    class Zone {
        class initSpawnRegisterZone;
        class serverInitSpawnZone;
        class spawnUpdateZone;
    };
};
