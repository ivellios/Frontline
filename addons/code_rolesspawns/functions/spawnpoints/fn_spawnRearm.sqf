/*
    Function:       FRL_Rolesspawns_fnc_spawnRearm
    Author:         Adanteh
    Description:    Rearms your current kit
*/
#include "macros.hpp"

params [["_mode", "rearm"], ["_args", []]];

private _return = true;
switch (toLower _mode) do {
    case "rearm": {

        if (isNil QGVAR(lastRearm)) then {
            GVAR(lastRearm) = -1e5;
        };

        private _rearmCooldown = [QGVAR(Rally_rearmCooldown), 300] call MFUNC(cfgSetting);
        if ((GVAR(lastRearm) + _rearmCooldown) >= serverTime) exitWith {
        	private _timeCooldown = [((GVAR(lastRearm) + _rearmCooldown) - serverTime), "MM:SS", false] call bis_fnc_secondsToString;
        	private _rearmCooldownFormat = [_rearmCooldown, "MM:SS", false] call bis_fnc_secondsToString;
        	[format ["You can only rearm every %1 (%2 Left)", _rearmCooldownFormat, _timeCooldown], MEDIAPATH + "icons\roles.paa", "red"] call MFUNC(notificationShow);
        };

        private _role = CLib_Player getVariable [QSVAR(rKit), ""];
        if (_role == "") exitWith {
        	["You lost your kit and can't rearm", MEDIAPATH + "icons\roles.paa", "red"] call MFUNC(notificationShow);
        };

        if !([Clib_Player] call FUNC(roleCanUse)) exitWith {
        	["The requirements for this role are no longer met", MEDIAPATH + "icons\roles.paa", "red"] call MFUNC(notificationShow);
        };


        CLib_Player playActionNow "PutDown";
        [CLib_Player, _role] call FUNC(roleRearm);
        GVAR(lastRearm) = serverTime;

        ["Finished rearming", MEDIAPATH + "icons\roles.paa", "ok"] call MFUNC(notificationShow);
    };

    case "condition": {
        _args params ["_caller", "_target"];
        private _spawnpoint = _target getVariable ["spawnpoint", objNull];
        if (_spawnpoint getVariable ["destroyed", false]) exitWith {
            ["showNotification", ["This FO is destroyed", MEDIAPATH + "icons\roles.paa", "nope"]] call CFUNC(localEvent);
            _return = false;
        };

        if ((_spawnpoint getVariable ["availableFor", sideUnknown]) != (side group _caller)) exitWith {
            ["showNotification", ["Can't rearm at enemy FOs", MEDIAPATH + "icons\roles.paa", "nope"]] call CFUNC(localEvent);
            _return = false;
        };
    };
};

_return;
