/*
    Function:       FRL_RolesSpawns_fnc_spawnRandomize
    Author:         Adanteh
    Description:    Temporarily adjusts a spawn point position value when it's a zone spawn, so we don't spawn in the middle. Look for building positions or dense vegetation
*/
#include "macros.hpp"
#define __EXCEPTIONLIST ["land_scf_01_heap_bagasse_f", "land_scf_01_crystallizertowers_f", "land_scf_01_storagebin_small_f", "land_scf_01_storagebin_big_f", "land_scf_01_chimney_f"]

params ["_spawnpoint"];

// -- Check if this is a zone spawn first -- //
private _spawnPos = +(_spawnpoint getVariable ["position", [0, 0, 0]]);
private _spawnmarker = _spawnpoint getVariable ["marker", ""];

if (_spawnMarker isEqualTo "") exitWith { nil };

private _spawnmarkerSize = selectMax (getMarkerSize _spawnmarker);
private _spawnPositions = [];
private _buildingList = _spawnPos nearObjects ["House_F", _spawnmarkerSize];
{
	if (_x inArea _spawnmarker) then {
		if !((toLower typeOf _x) in __EXCEPTIONLIST) then {
			private _buildingPosArray = _x buildingPos -1;
			if (count _buildingPosArray > 2) then {
				_buildingPosArray = _buildingPosArray call bis_fnc_arrayShuffle;
				_buildingPosArray resize 2; // -- Only add a couple positions within the same buildling
			};
			{
				_spawnPositions pushBack _x;
				nil;
			} count _buildingPosArray;
		};
	};
	nil;
} count _buildingList;

// -- If not enough spawns, add places with lotsa vegetation in one place  -- //
if (count _spawnPositions < 10) then {
	private _vegetationList = nearestTerrainObjects [_spawnPos, ["TREE"], _spawnmarkerSize, false];
	private _vegetationSpawn = [];
	{
		if (_x inArea _spawnmarker) then {
			if (count (nearestTerrainObjects [_x, ["TREE", "BUSH", "SMALL TREE"], 15, false]) > 8) then {
				private _treePos = getPosATL _x;
				_treePos = _treePos apply { (_x + 2 - (random 4)); };
				_treePos set [2, 0];
				_vegetationSpawn pushBack _treePos;
			};
		};
		nil;
	} forEach _vegetationList;
	if (count _vegetationSpawn > 10) then {
		_vegetationSpawn = _vegetationSpawn call bis_fnc_arrayShuffle;
		_vegetationSpawn resize 10;
	};
	_spawnPositions append _vegetationSpawn;
};


// -- Only take a new spawnposition if there are actually some found. If none found spawn in the middle -- //
if (count _spawnPositions > 0) then {
	_spawnPos = selectRandom _spawnPositions;
};

#ifdef ISDEV
    if !(isNil QGVAR(test_array)) then {
    	{ deleteMarkerLocal _x; nil } count GVAR(test_array);
    };
    GVAR(test_array) = [];

    {
        _marker = format ["spawn_%1", _x];
        if (getMarkerPos _marker isEqualTo [0,0,0]) then {
            _marker = createMarkerLocal [_marker, _x];
        };
        _marker setMarkerShapeLocal "ICON";
        _marker setMarkerTypeLocal "mil_dot";
        _marker setMarkerColorLocal "ColorYellow";
        GVAR(test_array) pushBack _marker;
        nil;
    } count _spawnPositions;
    (format ["spawn_%1", _spawnPos]) setMarkerColorLocal "ColorGreen";
	GVAR(test_spawn) = _spawnPos;
#endif

_spawnPos;
