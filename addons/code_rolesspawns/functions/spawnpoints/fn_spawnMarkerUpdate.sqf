/*
    Function:       FRL_Rolesspawns_fnc_spawnMarkerUpdate
    Author:         Adanteh
    Description:    Updates the spawn markers
*/
#include "macros.hpp"

private _availablePoints = [true] call FUNC(spawnGetAvailable);
private _existingMapIconPoints = GVAR(spawnMarkers) arrayIntersect _availablePoints;
private _sideColor = +([playerSide, "color"] call MFUNC(getSideData));

{
    [_x getVariable ["id", "##"]] call CFUNC(removeMapGraphicsGroup);
    nil
} count (GVAR(spawnMarkers) - _existingMapIconPoints);

{
    private _spawnpoint = _x;
    private _mapIcon = _spawnpoint getVariable ["mapicon", ""];

    if (_mapIcon != "") then {

        private _name = _spawnpoint getVariable ["name", ""];
        private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
        private _type = _spawnpoint getVariable ["type", ""];
        private _icon = _spawnpoint getVariable ["icon", ""];
        private _id = _spawnpoint getVariable ["id", "##"];

        private _color = [_type, "color"] call FUNC(spawnTypeData);
        private _icon = ["ICON", _mapIcon, _color, _position, 25, 25, 0, "", 1];
        private _normalText = ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _position, 25, 25, 0, _name, 2, 0.09];
        [_id, [_icon, _normalText], "normal"] call CFUNC(addMapGraphicsGroup);

         // -- Add registered mouseover code (groupID is given by mapGraphics framework)
        private _hovercode = {
            private _spawnpoint = GVAR(spawnpoints) getVariable [_groupId, objNull];
            private _code = [_spawnpoint getVariable ["type", ""], "Marker_mouseover"] call FUNC(spawnTypeData);
            [_spawnpoint] call _code;
        };

        private _onHoverText = ["ICON","a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _position, 25,25, 0, _name, 2, 0.09, nil, nil, _hovercode];
        [_id, [_icon, _onHoverText], "hover"] call CFUNC(addMapGraphicsGroup);
    };

    nil
} count (_availablePoints - _existingMapIconPoints);

GVAR(spawnMarkers) = _availablePoints;
