/*
    Function:       FRL_RolesSpawns_fnc_spawnDeploy
    Author:         Adanteh
    Description:    Deploy at a given spawnpoint
*/
#include "macros.hpp"

params ["_spawnpoint", ["_redeploy", false]];

private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
private _type = _spawnpoint getVariable ["type", ""];


// -- Forward and zone spawns will spawn you on a random pint within the area.
// -- Possible locations are in/on buildings, and between dense vegetation
private _posAdjust = [_type, "spawnPos"] call FUNC(spawnTypeData);
private _positionAdjust = call _posAdjust; // -- Add registered mouseover code
if !(isNil "_positionAdjust") then {
    _position = _positionAdjust;
};

// -- If there are ticket on this spawn, remove 1
private _tickets = _spawnpoint getVariable ["tickets", 1e5];
if (_tickets != 1e5) then {
    _spawnpoint setVariable ["tickets", _tickets - 1, true];
};

// -- If we spawn quite far above terrain, findEmptyPosition doesn't work at all, so instead we just do a random direction. Not great, but blame BIS
private _height = (ASLtoATL _position) select 2;
if (_height > 10) then {
    private _positionOffset = (_position getPos [2 + (random 5), random 360]);
    _positionOffset set [2, (_position select 2)];
    [AGLToASL _positionOffset] call MFUNC(Respawn);
} else {
    [AGLToASL ([_position, 5, 0, typeOf CLib_Player] call CFUNC(findSavePosition))] call MFUNC(Respawn);
};


[QSVAR(afterSpawn), [_spawnpointSelected, Clib_Player, _redeploy]] call CFUNC(localEvent);

if (isNull objectParent player) then {
    player switchMove "";
};

// -- Execute after we spawned, for example in vehicles we want to move in a seat
private _afterSpawnCode = [_type, "afterSpawn"] call FUNC(spawnTypeData);
[_spawnpoint, Clib_Player] call _afterSpawnCode;


// -- Small delay so we can see waddaup
[{
    [QSVAR(AfterSpawnDelay), _this] call CFUNC(localEvent);
}, 0.5, [_spawnpointSelected, Clib_Player, _redeploy]] call CFUNC(wait);
