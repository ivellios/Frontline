/*
    Function:       FRL_Rolesspawns_fnc_serverInitSpawnVehicle
    Author:         Adanteh
    Description:    Inits forward spawn. This is a randomized area spawn, placed by mission maker, will block if enemies come close
*/
#include "macros.hpp"

GVAR(spawnVehicles) = [];
GVAR(spawnVehicleNames) = [];

["vehicleInit", {
    (_this select 0) params ["_vehicle", "_side"];
    ([_vehicle] call FUNC(spawnVehicleGetTickets)) params ["_ticketsMax", "_ticketCooldown"];

    if (_ticketsMax > 0) then {
        GVAR(spawnVehicles) pushBack [_vehicle, _side];


        private _vehicleCfg = (configFile >> "CfgVehicles" >> (typeOf _vehicle));
        private _icon = getText (_vehicleCfg >> "icon");
        if (isText (configFile >> "cfgVehicleIcons" >> _icon)) then {
            _icon = getText (configFile >> "cfgVehicleIcons" >> _icon);
        };

        // -- Make sure the name isn't too long
        private _vehicleName = getText (_vehicleCfg >> "displayName");
        if (count _vehicleName > 17) then {
            _vehicleName = (_vehicleName select [0, 16]) + "..";
        };

        // -- Add # index, to differentiate between vehicles with the same name
        private _vehicleTypeIndex = ({ _x == _vehicleName } count GVAR(spawnVehicleNames)) + 1;
        private _vehicleSpawnName = format ["%1 #%2", _vehicleName, _vehicleTypeIndex];
        private _spawnpoint = [_vehicleSpawnName, [0, 0, 0], _side, "vehicle", _icon] call FUNC(spawnAdd);

        _vehicle setVariable ["spawnpoint", _spawnpoint, true];
        _spawnpoint setVariable ["tickets", _ticketsMax, true];
        _spawnpoint setVariable ["ticketsMax", _ticketsMax, true];
        _spawnpoint setVariable ["ticketCooldown", _ticketCooldown, false];
        _spawnpoint setVariable ["vehicle", _vehicle, true];

        GVAR(spawnVehicleNames) pushBack _vehicleName;
    };
}] call CFUNC(addEventHandler);
