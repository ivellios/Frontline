/*
    Function:       FRL_Rolesspawns_fnc_spawnVehicleHUD
    Author:         Adanteh
    Description:    Shows bar for the vehicle crew in the top left indicating how many spawns you have left
*/
#include "macros.hpp"

params ["_vehicle"];
([_vehicle] call FUNC(spawnVehicleGetTickets)) params ["_ticketsMax", "_ticketCooldown"];
if (_ticketsMax == 0) exitWith { };

private _spawnpoint = _vehicle getVariable ["spawnpoint", objNull];
["progressHudShow", [QGVAR(hud),
{
    (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\respawn.paa");
    (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.222,0.7,0.222,0.6];
},
{
    params ["_spawnpoint", "_ticketsMax", "_ticketCooldown"];
    private _ticketRefreshAt = _spawnpoint getVariable ["ticketRefresh", 0];
    private _timeLeft = (_ticketRefreshAt - serverTime) max 0;
    (_controlsGroup controlsGroupCtrl 5) progressSetPosition ((0 max (1 - (_timeLeft / _ticketCooldown))) min 1);

    private _blockedTime = _spawnpoint getVariable ["blocked", -1];
    private _blockText = "";
    if (_blockedTime > serverTime) then {
        private _blockReason = _spawnpoint getVariable ["blocked_reason", ""];
        _blockText = format [" [Blocked %1] %2", [ceil (_blockedTime - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString, _blockedTime];
    };
    (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Tickets %1/%2%3", (_spawnpoint getVariable ["tickets", 0]), _ticketsMax, _blockText]);
},
[_spawnpoint, _ticketsMax, _ticketCooldown]]] call CFUNC(localEvent);
