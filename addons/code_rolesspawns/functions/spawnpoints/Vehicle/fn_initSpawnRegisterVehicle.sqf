/*
    Function:       FRL_Rolesspawns_fnc_initSpawnRegisterVehicle
    Author:         Adanteh
    Description:    Registers forward spawn mechanics
*/
#include "macros.hpp"
#define __KEY(var1) [_spawnType, var1] joinString "#"

[QGVAR(vehicle), configFile >> "FRL" >> "cfgVehicleSpawn"] call MFUNC(cfgSettingLoad);
[QGVAR(vehicle), missionConfigFile >> "FRL" >> "cfgVehicleSpawn"] call MFUNC(cfgSettingLoad);

private _spawnType = "vehicle";
GVAR(spawnpointTypes) setVariable [__KEY("color"), [1, 1, 1, 1]];
GVAR(spawnpointTypes) setVariable [__KEY("marker_mouseover"), {}];
GVAR(spawnpointTypes) setVariable [__KEY("afterspawn"), {
    params ["_spawnpoint", "_unit"];
    private _vehicle = _spawnpoint getVariable ["vehicle", objNull];
    // -- Try this 5 times till it's a success
    for "_i" from 1 to 5 do {
        private _succes = [_unit, _vehicle, true] call EFUNC(interactthreed,getInAvailable);
        if (_succes) exitWith { };
    };

    if (vehicle Clib_Player != _vehicle) then {
        Clib_Player moveInAny _vehicle;
    };
    true;
}];

GVAR(spawnpointTypes) setVariable [__KEY("list_entry"), {
    private _tickets = _spawnpoint getVariable ["tickets", 0];
    private _ticketsMax = _spawnpoint getVariable ["ticketsMax", 1];

    _name = format ["%1 (%2/%3)", _name, _tickets, _ticketsMax];
    private _blocked = _spawnpoint getVariable ["blocked", -1];
    if (_blocked > serverTime) then {
        private _blockTime = [(_blocked - serverTime) max 0, "MM:SS"] call bis_fnc_secondsToString;
        private _blockReason = _spawnpoint getVariable ["blocked_reason", ""];
        _name = format ["%1 [Blocked %2] %3", _name, _blockTime, _blockReason];
    };
    _name;
}];


["vehicle_canSpawn", {
    params ["_spawnpoint", "_position"];
    #ifdef DEBUGFULL
        if (true) exitWith { true };
    #endif

    private _blocked = (_spawnpoint getVariable ["blocked", -1]) > serverTime;
    if (_blocked) exitWith {
        _returnMessage = _spawnpoint getVariable ["blocked_reason", ""];
        if (_returnMessage isEqualTo "") then {
            _returnMessage = "This spawn location is blocked!";
        } else {
            _returnMessage = format ["Blocked: %1", _returnMessage];
        };
        false
    };

    private _vehicle = _spawnpoint getVariable ["vehicle", objNull];
    private _crewSeats = (fullCrew [_vehicle, "cargo", true]) + (fullCrew [_vehicle, "Turret", true]);
    private _emptySeats = { isNull (_x select 0) } count _crewSeats;
    if (_emptySeats == 0) exitWith {
        _returnMessage = "Vehicle has no empty passenger seats";
        false;
    };

    true;
}] call MFUNC(addCondition);
