/*
    Function:       FRL_Vehiclespawn_fnc_serverInitTickets
    Author:         Adanteh
    Description:    Handles adding / refreshing tickets and blocking the spawnpoints when close to non-friendly zone
*/
#include "macros.hpp"

params ["_spawnpoint", ["_blocked", false]];
private _availableFor = _spawnpoint getVariable ["availableFor", grpNull];
private _vehicle = _spawnpoint getVariable ["vehicle", objNull];


if !(alive _vehicle) exitWith {
    [_spawnpoint] call FUNC(spawnRemove);
};


// -- Unblock override if we are in base
private _inBase = [_vehicle] call MFUNC(sectorInBase);

// -- Handle tickets (Restore after cooldown period has passed)
private _tickets = _spawnpoint getVariable ["tickets", 0];
private _ticketsMax = _spawnpoint getVariable ["ticketsMax", 1];
if (_tickets < _ticketsMax) then {
    // -- Instant ticket refresh in Main Base
    if (_inBase) then {
        _spawnpoint setVariable ["tickets", _ticketsMax, true];
    } else {
        private _refreshAt = _spawnpoint getVariable ["ticketRefresh", -1];
        private _ticketCooldown = _spawnpoint getVariable ["ticketCooldown", 60];
        if (_refreshAt == -1) then {
            _spawnpoint setVariable ["ticketRefresh", serverTime + _ticketCooldown, true];
        } else {
            if (_refreshAt < serverTime) then {
                _spawnpoint setVariable ["ticketRefresh", serverTime + _ticketCooldown, true];
                _spawnpoint setVariable ["tickets", _ticketsMax, true];
            };
        };
    };
};


if (!_inBase && !_blocked) then {
    private _driver = driver _vehicle;
    private _hasDriver = alive _driver && { !([_driver] call MFUNC(isUnconscious)) };
    if !(_hasDriver) exitWith {
        _blocked = true;
        _spawnblock_return = "Needs a driver";
    };

    private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
    private _blockCount = [QGVAR(vehicle_blockCount), 2] call MFUNC(cfgSetting);
    private _blockRadius = [QGVAR(vehicle_blockRadius), 50] call MFUNC(cfgSetting);
    private _nearUnits = [getPos _vehicle, _blockRadius, _availableFor] call FUNC(getNearUnitsBlocking);

    _blocked = (_nearUnits >= _blockCount);
    _spawnblock_return = "Enemies nearby";
};

if (!_inBase && _blocked) then {
    private _blockDuration = [QGVAR(vehicle_blockDuration), 60] call MFUNC(cfgSetting);
    [_spawnPoint, _blockDuration] call FUNC(spawnDisable);

    if !(_spawnpoint getVariable ["blocked_reason", ""] isEqualTo _spawnblock_return) then {
        _spawnpoint setVariable ["blocked_reason", _spawnblock_return, true];
    };
};
