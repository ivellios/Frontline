/*
    Function:       FRL_Rolesspawns_fnc_clientInit
    Author:         Adanteh
    Description:    Inits the client actions and UI stuff for spawning on vehicles
*/
#include "macros.hpp"

GVAR(vehicleHudShown) = false;

["crewSeatEntered", {
    (_this select 0) params ["_vehicle", "_crewSeat"];
    if !(GVAR(vehicleHudShown)) then {
        private _spawnpoint = _vehicle getVariable ["spawnpoint", objNull];
        if !(isNull _spawnpoint) then {
            GVAR(vehicleHudShown) = true;
            [_vehicle] call FUNC(spawnVehicleHUD);
        };
    };
}] call CFUNC(addEventHandler);

["crewSeatLeft", {
    if (GVAR(vehicleHudShown)) then {
        GVAR(vehicleHudShown) = false;
        ["progressHudHide", [QGVAR(hud)]] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);

/*
["crewSeatLeft", {
    (_this select 0) params ["_vehicle", "_crewSeat"];
    private _spawnpoint = _vehicle getVariable ["spawnpoint", objNull];
    if ((_spawnpoint getVariable ["tickets", -1]) != -1) then {
        if (_crewSeat == "driver") then {
            if (isNull (objectParent Clib_Player)) then {
                if !(isEngineOn _vehicle) then {
                    ["engineOn", _vehicle, [_vehicle]] call CFUNC(targetEvent);
                };
            };
        };
    };
}] call CFUNC(addEventHandler);
*/
