/*
    Function:       FRL_Rolesspawns_fnc_spawnVehicleGetTickets
    Author:         Adanteh
    Description:    Gets vehicle spawning details for vehicle category, both max tickets and time to refresh (in seconds)
    Example:        [cursorTarget] call FRL_Rolesspawns_fnc_spawnVehicleGetTickets;
*/
#include "macros.hpp"

params [["_vehicleType", objNull, ["", objNull]], "_vehicleCategory"];

if (_vehicleType isEqualType objNull) then {
    _vehicleType = typeOf _vehicleType;
};

if (isNil "_vehicleCategory") then {
    _vehicleCategory = ([_vehicleType] call EFUNC(vehicles,getVehicleType) select 1);
};

private _spawnDetails = switch (toLower _vehicleCategory) do {
    case "tank";
    case "mbt";
    case "armour";
    case "wheeledifv";
    case "trackedapc";
    case "armored_car"; // -- IFA 222
    case "apc": { [0, 0] }; // IFA halftrack

    case "ifv": { [5, 10*60] };
    case "halftrack": { [8, 15*60] }; // IFA halftrack
    case "halftrack_small": { [4, 15*60] }; // IFA halftrack

    case "vehicleautonomous";
    case "jet";
    case "plane";
    case "plane_recon": { [0, 0] };
    case "plane_heavytransport": { [8, 30*60] };

    case "helicopter";
    case "attackhelicopter":{ [0, 0] };

    case "heli_heavytransport": { [10, 5*60] };
    case "heli_lighttransport": { [ 5, 5*60] };

    case "truck";
    case "mrap";
    case "armedmrap";
    case "armed_car"; // Humvees, Tigr, Technicals
    case "mrap";
    case "motorcycle";
    case "quadbike";
    case "car";
    case "ship": { [0, 0] };
    case "boat": { [5, 180*60] };

    case "submarine": { [4, 10*60] };

    case "lcm": { [10, 180*60] };
    case "lcvp": { [10, 180*60] };

    case "staticweapon";//
    case "static": { [0, 0] };
    case "cargobox": { [0, 0] };
    default {
        diag_log format ["[FRL Warning] Vehicle type: '%1' doesn't have a preset respawn time (Category: '%2')", _vehicleType, _vehicleCategory];
        [0, 0]
    };
};

private _vehicleCfg = (configFile >> "CfgVehicles" >> _vehicleType);
if (isNumber (_vehicleCfg >> "FRL" >> "vehicleSpawnTickets")) then {
    private _configEntry = getNumber (_vehicleCfg >> "FRL" >> "vehicleSpawnTickets");
    if (_configEntry != -1) then {
        _spawnDetails set [0, _configEntry];
    };
};

if (isNumber (_vehicleCfg >> "FRL" >> "vehicleSpawnCooldown")) then {
    private _configEntry = getNumber (_vehicleCfg >> "FRL" >> "vehicleSpawnRefreshTime");
    if (_configEntry != -1) then {
        _spawnDetails set [1, _configEntry];
    };
};

// _spawnDetails = [0.1, 25, 0.25];
// [Tickets, ticket refresh]
_spawnDetails;
