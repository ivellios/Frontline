/*
    Function:       FRL_Rolesspawns_fnc_clientInit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

// -- Assign a side when the mission starts
["missionStarted", { _this call FUNC(sideAutoAssign)} ] call CFUNC(addEventHandler);

['add', ["gui", 'Frontline Settings', 'Debug', [
    'Fix team assign',
    "Use when you're standing around as locked unit, without a team assigned",
    QMVAR(CtrlButton),
    true,
    { true; },
    {
        if !(playerSide in ([] call MFUNC(getSides))) then {
            [] call FUNC(sideAutoAssign);
        };
    },
    ["FIX TEAM ASSIGN"]
]]] call MFUNC(settingsWindow);
