/*
    Function:       FRL_RolesSpawn_fnc_canSpawn
    Author:         Adanteh
    Description:    Checks if spawn conditions are matched
*/
#include "macros.hpp"

params [["_quiet", true]];

if !((groupId group CLib_Player) in MVAR(squadIds)) exitWith {
    _returnMessage = "You have to join a squad!";
    false
};

private _currentRoleSelection = CLib_Player getVariable [QSVAR(rkit), ""];
if (_currentRoleSelection == "") exitWith {
    _returnMessage = "You have to select a role!";
    false
};

private _deploymentDisplay = uiNamespace getVariable [QGVAR(deploymentDisplay), displayNull];
private _controlDeploymentList = _deploymentDisplay displayCtrl 403;
private _selectedPointID = lnbCurSelRow _controlDeploymentList;
if (_selectedPointID < 0) exitWith {
    _returnMessage = "You have to select a spawnpoint!";
    false
};

private _spawnpoint = [_controlDeploymentList, [_selectedPointID, 0]] call CFUNC(lnbLoad);
private _type = _spawnpoint getVariable ["type", ""];
private _position = _spawnpoint getVariable ["position", [0, 0, 0]];

// -- Check the spawntype, and check some extra conditions, this should set _returnMessage if it fails
private _canSpawn = ([_type + "_canSpawn", [_spawnpoint, _position, side group Clib_Player], true] call MFUNC(checkConditions));
if !(_canSpawn) exitWith {
    false
};

if (_spawnpoint getVariable ["aiSpawnOnly", false]) exitWith {
    _returnMessage = "This spawn is only for AI";
    false;
};

private _tickets = _spawnpoint getVariable ["tickets", 1e5];
if (_tickets != 1e5 && (_tickets <= 0)) exitWith {
    _returnMessage = "No tickets left on this spawn";
    false
};

_spawnpointSelected = _spawnpoint; // IGNORE PRIVATE

true;
