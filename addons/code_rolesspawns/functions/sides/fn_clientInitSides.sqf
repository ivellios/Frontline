/*
    Function:       FRL_Rolesspawns_fnc_clientInitSides
    Author:         Adanteh
    Description:    Inits some side stuff
*/
#include "macros.hpp"

GVAR(lastTimeSideChanged) = -1e5;

// -- This shuffles the list of players and assigns them to a different team 1 by 1 (if not already on that team) -- //
[QGVAR(scrambleReceive), {
    (_this select 0) params ["_targetTeam"];
    [[-1000, -1000, 10], _targetTeam] call FUNC(sideSwitch);
}] call CFUNC(addEventHandler);

// -- Do a random team assign -- //
[QGVAR(scrambleStart), {
    private _allPlayers = +allPlayers;
    private _shuffle = _allPlayers call BIS_fnc_arrayShuffle;
    private _teams = +(call MFUNC(getSides));
    private _teamCount = count _teams;
    {
        private _newTeam = _teams select [_forEachIndex % _teamCount];
        if (_newTeam != (side group _x)) then {
            [QGVAR(scrambleReceive), _x, [_newTeam]] call CFUNC(targetEvent);
        };
    } forEach _allPlayers;
}] call CFUNC(addEventHandler);
