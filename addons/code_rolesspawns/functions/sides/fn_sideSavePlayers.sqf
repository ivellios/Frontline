/*
    Function:       FRL_Rolesspawn_fnc_sideSavePlayers
    Author:         Adanteh
    Description:    Used to keep a list of who was on which side, called in a loop (In case mission is abruptly ended) and on scripted mission ends
*/
#include "macros.hpp"

private _playerList = [[], []];
{
    private _side = (side group _x);
    if ([_side] call MFUNC(getSideIndex) != -1) then {
        (_playerList select 0) pushBack (getPlayerUID _x);
        (_playerList select 1) pushBack _side;
    };
    nil;
} count allPlayers;
uiNamespace setVariable [QGVAR(oldList), _playerList];

_playerList;
