 /*---------------------------------------------------------------------------
     This is a modified version of AAW_Squad_fnc_squadJoin, July 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

 /*
     Function:       FRL_Rolesspawn_fnc_sideSwitch
     Author:         Adanteh
     Description:    Switches you to the opposite side
 */
 #include "macros.hpp"

[{
    private _newSide = [playerSide] call MFUNC(getSideOpposite);
    private _oldSide = playerSide;

    if !([_newSide, _oldSide] call FUNC(sideCanSwitch)) exitWith {};

    // Leave old squad first
    []  call MFUNC(leaveSquad);

    // Respawn as new unit
    GVAR(lastTimeSideChanged) = serverTime;
    [[-1000, -1000, 10], _newSide] call FUNC(sideAssign);
    uiNamespace setVariable [QGVAR(playerSide), _newSide];
}, [], "respawn"] call CFUNC(mutex);
