/*
    Function:       FRL_Rolesspawns_fnc_sideAssign
    Author:         Netfusion
    Description:    Move the player to new unit of another side
*/
#include "macros.hpp"

/*---------------------------------------------------------------------------
    This is a modified version of AAW, July 11th 2016 version
    Released under APL license by the AAW Team.

    Current project can be found here: https://drakelinglabs.github.io/projectrealityarma3/
    Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
---------------------------------------------------------------------------*/

params ["_targetPosition", "_side"];

// -- Create a new body with the proper player class, but make sure it actually exists
private _className = [_side, "playerClass"] call MFUNC(getSideData);
if (_className == "") then {
    _className = "O_Soldier_F";
} else {
    if !(isClass (configFile >> "CfgVehicles" >> _className)) then {
        diag_log ["[FRL {WARNING}]", format ["Mission has reference to non-existing player class '%1' for side '%2'.", _className, _side]];
        diag_log ["[FRL {WARNING}]", format ["This will need to be fixed in cfgSides.hpp, within the mission files"]];
        private _sideID = [_side] call BIS_fnc_sideID;
        _className = ["O_Soldier_F", "B_Soldier_F", "I_soldier_F"] param [_sideID, "B_Soldier_F"];
    };
};

// We need to create a new group otherwise the new unit may not be local (looks like its sometimes local to the group owner).
private _tempGroup = createGroup [_side, true];
private _newUnit = _tempGroup createUnit [_className, [-10000, -10000, 50], [], 0, "NONE"];


// This unit is temporary (will be removed if we call this function again)
_newUnit setVariable [QSVAR(tempUnit), true];
["enableSimulation", [_newUnit, false]] call CFUNC(serverEvent);
["hideObject", [_newUnit, true]] call CFUNC(serverEvent);

private _oldUnit = CLib_Player;

// Move the player to the unit before changing anything
selectPlayer _newUnit;

// Handle the vehicleVarName
private _oldVarName = vehicleVarName _oldUnit;
_oldUnit setVehicleVarName "";
_newUnit setVehicleVarName _oldVarName;

// Copy event handlers
// This should be done by our awesome event system in core on playerChanged event

// Handle position
CLib_Player setDir (random 360);
CLib_Player setPosASL ([_targetPosition, 5, 0,_className] call CFUNC(findSavePosition));

// Broadcast the change after everything is changed
["playerChanged", [_newUnit, _oldUnit]] call CFUNC(localEvent);
CLib_Player = _newUnit;

// Trigger respawn event
["Respawn", [CLib_Player, _oldUnit]] call CFUNC(localEvent);
["MPRespawn", [CLib_Player, _oldUnit]] call CFUNC(globalEvent);

// Remove the old unit if it was a temp unit
if (_oldUnit getVariable [QSVAR(tempUnit), false]) then {
    _tempGroup = group _oldUnit;
    deleteVehicle _oldUnit;
    ["deleteGroup", _tempGroup] call CFUNC(serverEvent);
} else {
    _oldUnit setDamage 1;
};
