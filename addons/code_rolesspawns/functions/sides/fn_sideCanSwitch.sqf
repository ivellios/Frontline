/*
    Function:       FRL_Rolesspawn_fnc_sideCanSwitch
    Author:         Adanteh
    Description:    Checks if we can switch sides
*/
#include "macros.hpp"
#ifdef DEBUGFULL
    if (true) exitWith { true };
#endif

params ["_newSide", "_oldSide"];

private _allowSwitchEvery = getNumber (configFile >> "FRL" >> "CfgTeamBalance" >> "allowSwitchEvery");
if !(_allowSwitchEvery isEqualType 0) then {
	_allowSwitchEvery = call compile _allowSwitchEvery;
};
if ((GVAR(lastTimeSideChanged) + _allowSwitchEvery) >= serverTime) exitWith {
	private _cooldown = [_allowSwitchEvery, "MM:SS"] call bis_fnc_secondsToString;
	private _cooldownLeft = [(GVAR(lastTimeSideChanged) + _allowSwitchEvery) - serverTime, "MM:SS"] call bis_fnc_secondsToString;
    [format ["Can only switch teams every %1 (%2 Left)", _cooldown, _cooldownLeft]] call MFUNC(notificationShow);
    false
};

private _fnc = {
    params ["_side"];
    {_side == side group _x} count allPlayers;
};

private _newSideCount = _newSide call _fnc;
private _oldSideCount = _oldSide call _fnc;
private _allowSwitchDif = 2;
if (isNumber (configFile >> "FRL" >> "CfgTeamBalance" >> "allowSwitchDif")) then {
	_allowSwitchDif = getNumber (configFile >> "FRL" >> "CfgTeamBalance" >> "allowSwitchDif");
} else {
	_allowSwitchDif = getText (configFile >> "FRL" >> "CfgTeamBalance" >> "allowSwitchDif");
	if (_allowSwitchDif != "") then {
		_allowSwitchDif = call compile _allowSwitchDif;
	} else {
		_allowSwitchDif = 2;
	};
};
if (((_newSideCount + 1) - _allowSwitchDif) > (_oldSideCount - 1)) exitWith {
    [format ["Can't switch if difference bigger than %1", _allowSwitchDif]] call MFUNC(notificationShow);
    false
};

true
