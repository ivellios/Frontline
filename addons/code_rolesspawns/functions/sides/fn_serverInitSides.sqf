/*
    Function:       FRL_Rolesspawns_fnc_serverInitSides
    Author:         Adanteh
    Description:    This handles the server side of our team system
*/
#include "macros.hpp"

// -- Check if we have an old player list, if so broadcast -- //
["missionStarted", {
	private _oldPlayerList = uiNamespace getVariable [QGVAR(oldList), [[], []]];
	if !(_oldPlayerList isEqualTo [[], []]) then {
		GVAR(oldPlayers) = _oldPlayerList;
		publicVariable QGVAR(oldPlayers);
	};
}] call CFUNC(addEventHandler);

[{ _this call FUNC(sideSavePlayers) }, 30] call MFUNC(addPerFramehandler);
["endMission", {_this call FUNC(savePlayerList) }] call CFUNC(addEventHandler);
