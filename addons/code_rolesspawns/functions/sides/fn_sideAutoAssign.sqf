/*
    Function:       FRL_Rolessspawns_fnc_sideAutoAssign
    Author:         Adanteh
    Description:    Handles the assigning of a side to the player (At start of game)
*/
#include "macros.hpp"

// Choose the initial side for the player before showing the respawn screen.
[QGVAR(SideSelection)] call MFUNC(startLoadingScreen);
if !(isMultiplayer) exitWith {
    player enableSimulation true;
    player hideObject false;
    private _pos = getPos player;
    _pos set [2, 0];
    player setPos _pos;
};

[{
    // -- This is unassigned players only!  -- //
    private _newPlayers = 0;
    private _allPlayers = +allPlayers;
    private _sizesAssigned = [0, 0, 0, 0];
    private _sizesUnassigned = [0, 0, 0, 0];
    private _sides = +(call MFUNC(getSides));
    _sizesAssigned resize (count _sides);
    _sizesUnassigned resize (count _sides);


    if (isNil QGVAR(oldPlayers)) then { GVAR(oldPlayers) = [[], []]; };

    private _oldSidePlayer = uiNamespace getVariable [QGVAR(playerSide), sideUnknown];
    // -- Comment -- //
    /*
        A simple explanation on the team swapa:
        For the current client it combines the people already assigned to a team, with the people that were on the OPPOSITE team the previous round.
        It then checks the differences between each team and tries to bridge that gap with new players.
        If the number of new players is not high enough to bridge this gap, it will switch the current client over to the smaller team, even if that means playing on the same side twice.

        A more technical explanation:
        This differentiates between 3 groups of players:
        1) People already assigned to a team (Blufor or Opfor) (Processed before this client)
        2) People that don't appear in the list of clients that played previous round (New connects)
        3) People that do appear in the list of clients that played previous round (Blufor or Opfor)

        We count everyone in group 2 as people we can assign to whichever team is smaller (ALREADY ASSIGNED, PLUS FUTURE ASSIGNEES)
        Whenever there are not enough people in group 2 to compensate for player number differences on the sides (ALREADY ASSIGNED, PLUS FUTURE ASSIGNEES COMBINED!)
            we will make the current client (If already played on the prevoius round) be assigned to the smaller team too.
            This procress will be repeated for returning players untill the difference in player numbers is small enough to be bridged by the new players

    */

    // -- This will make a list counting up people already assigned to a team, new unassigned players (No team on previous round) and players who are not yet assigned, but do have a  -- //
    {
        private _unitSide = side group _x;
        private _unitSideID = _sides find _unitSide;
        if (_unitSideID == -1) then { // -- Unassigned player
            private _oldPlayerID = (GVAR(oldPlayers) select 0) find (getPlayerUID _x);
            if (_oldPlayerID == -1) then {
                _newPlayers = _newPlayers + 1;
            } else {
                // -- Check the player opposite side and put him on the new side -- //
                private _oldSide = (GVAR(oldPlayers) select 0) select _oldPlayerID;
                private _oppositeSideID = abs (abs (_sides find _oldSide) - 1);
                _sizesUnassigned set [_oppositeSideID, (_sizesUnassigned select _oppositeSideID) + 1];
            };
        } else {
            _sizesAssigned set [_unitSideID, (_sizesAssigned select _unitSideID) + 1]; // -- Assigned player
        };
        nil;
    } count _allPlayers;

    // -- Figure out the difference in players when the current players assigned to a team + the one we know will be assigned to this
    private _sideOne = (_sizesAssigned select 0) + (_sizesUnassigned select 0); // 5 blufor
    private _sideTwo = (_sizesAssigned select 1) + (_sizesUnassigned select 1); // 12 opfor

    // -- If a player already player before this round, by default put him on the opposite team, unless the difference in player numbers would be too big -- //
    private _targetTeam = sideUnknown;
    if !(_oldSidePlayer isEqualTo sideUnknown) then {
        #define __TOLERANCE 3 // Player difference allowed after compensations
        private _sideDifference = abs (_sideOne - _sideTwo); // 7 players difference
        private _needsCompensation = (_sideDifference - _newPlayers) > __TOLERANCE; // True
        // -- If there are not enough new (unassigned players) to make up for the difference in player numbers, move over to the opposite team instead
        if (_needsCompensation) then {
            if (_sideOne < _sideTwo) then {
                _targetTeam = _sides select 0;
            } else {
                _targetTeam = _sides select 1;
            };
        } else {
            // -- If the team the other person played on is not present in this scenario (OPFOR > GUERILLA) assign him to enemy team.
            private _oppositeSideID = abs (abs (_sides find _oldSidePlayer) - 1);
            _targetTeam = _sides select _oppositeSideID;
        };
    } else {
        if (_sideOne < _sideTwo) then {
            _targetTeam = _sides select 0;
        } else {
            if (_sideOne > _sideTwo) then {
                _targetTeam = _sides select 1;
            } else {
                _targetTeam = _sides select (round random 1);
            };
        };
    };


    // Move the player to the side
    [[-1000, -1000, 10], _targetTeam] call FUNC(sideAssign);
    uiNamespace setVariable [QGVAR(playerSide), _targetTeam];

    // Open the respawn UI
    [{
        [QGVAR(SideSelection)] call MFUNC(endLoadingScreen);
        (findDisplay 46) createDisplay QSVAR(RespawnScreen);
    }, 0.5] call CFUNC(wait);
}, [], "respawn"] call CFUNC(mutex);
