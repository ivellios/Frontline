#include "macros.hpp"
/*
 *	File: fn_loadMenu.sqf
 *	Author: Adanteh
 *	(Un)loads the spotting menu and adds all EHs. Called by onLoad/onUnload dialog entries.
 *
 *	0: Whether to load or unload the dialog <STRING>
 *
 *	Example:
 *	[player] call FUNC(loadMenu);
 */

#define CTRL(x) ((uiNamespace getVariable QGVAR(spotDialog)) displayCtrl x)

params ["_mode"];
if (_mode == "onLoad") then {
	private _clickedPos = uiNamespace getVariable ["RMB_pos", [0, 0]];
	private _worldPos = (uiNamespace getVariable "_map") ctrlMapScreenToWorld _clickedPos;
	private _tempMarker = createMarkerLocal [QGVAR(tempMarker), _worldPos];
	_tempMarker setMarkerShapeLocal "ICON";
	_tempMarker setMarkerTypeLocal "mil_dot";

	CTRL(4) ctrlAddEventHandler ["lbSelChanged", {
		if ((_this select 1) == -1) exitWith { };
		private _spotIndex = (_this select 0) lbData (_this select 1);
		if (_spotIndex != "") then {
			_spotIndex = parseNumber _spotIndex;
			private _category = uiNamespace getVariable [QGVAR(curCategory), 0];
			private _pos = getMarkerPos QGVAR(tempMarker);
			_pos deleteAt 2; // -- This will always be 0, useless to send in broadcast
			private _caller = [Clib_Player] call CFUNC(name);
			[QGVAR(add), playerSide, [_spotIndex, _category, _pos, _caller]] call CFUNC(targetEvent);
			(uiNamespace getVariable QGVAR(spotDialog)) closeDisplay 2;
		};
		true;
	}];

	private _categoryList = [];
	private _categories = +(GVAR(spottingOptions) select 0);
	{
		private _lbIndex = CTRL(3) lbAdd ((GVAR(spottingOptions) select 1) select _forEachIndex);
		CTRL(3) lbSetData [_lbIndex, _x];
		CTRL(3) lbSetValue [_lbIndex, _forEachIndex];
		_categoryList set [_lbIndex, _forEachIndex];
	} forEach _categories;
	uiNamespace setVariable [QGVAR(categoryList), _categoryList];

	CTRL(3) lbSetCurSel 0;
	CTRL(3) ctrlAddEventHandler ["ToolboxSelChanged", {
		private _category = (uiNamespace getVariable [QGVAR(categoryList), []]) param [(_this select 1), 0];
		private _categoryCurrent = uiNamespace getVariable [QGVAR(curCategory), 0];
		if (_category != _categoryCurrent) then {
			uiNamespace setVariable [QGVAR(curCategory), _category];
			[_category] call FUNC(populateMenu);
		};
		true
	}];

	[0] call FUNC(populateMenu);

	private _position = ctrlPosition CTRL(100);
	_position set [0, (_clickedPos select 0) + 0.015];
	_position set [1, (_clickedPos select 1) - 0.15];

	// -- Prevent the window from going below the screen (Move it up if that's the case) -- //
	if (((_position select 1) + (_position select 3)) > (safeZoneY + safeZoneH)) then {
		_position set [1, (safeZoneY + safeZoneH) - (_position select 3)];
	};

	// -- Prevent the window from going to the right-side of screen (Move to left if that's the case)  -- //
	if (((_position select 0) + (_position select 2)) > (safeZoneX + safeZoneW)) then {
		_position set [0, (safeZoneX + safeZoneW) - (_position select 2)];
	};

	CTRL(100) ctrlSetPosition _position;
	CTRL(100) ctrlCommit 0;

	// -- Unload the menu if right-clicked anywhere or if clicked outside the spotting menu -- //
	(uiNamespace getVariable QGVAR(spotDialog)) displayAddEventHandler ["mouseButtonDown", {
		private _groupPos = ctrlPosition CTRL(100);
		params ["", "_button", "_posX", "_posY"];

		if ((_button == 1) || {
			(_posX < (_groupPos select 0)) } || {
			(_posY < (_groupPos select 1)) } || {
			(_posX > ((_groupPos select 0) + (_groupPos select 2)))  } || {
			(_posY > ((_groupPos select 1) + (_groupPos select 3)))  }) then {
			(uiNamespace getVariable QGVAR(spotDialog)) closeDisplay 2;
		};
		true
	}];
} else {
	uiNamespace setVariable [QGVAR(curCategory), nil];
	uiNamespace setVariable [QGVAR(categoryList), nil];
	deleteMarkerLocal QGVAR(tempMarker);
};
