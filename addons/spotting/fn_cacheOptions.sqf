#include "macros.hpp"
/*
 *	File: fn_cacheOptions.sqf
 *	Author: Adanteh
 *	Retrieves the available spotting options and parses them, so we only need to send ID across the network and have faster loading of menus
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(cacheOptions);
 */

private _allEntries = [[], [], []];

{
	private _categoryEntries = [];
	{
		if ((getNumber (_x >> "scope")) >= 2) then {
			// -- Parse some values -- //
			private _color = [1, 1, 1, 1];
			if (isArray (_x >> "markerColor")) then {
				_color = (getArray (_x >> "markerColor")) apply { if (_x isEqualType "") then { call compile _x } else { _x } };
			} else {
				if (isText (_x >> "markerColor")) then {
					_color = toLower (getText (_x >> "markerColor"));
				};
			};

			private _displayNameShort = getText (_x >> "displayNameShort");
			if (_displayNameShort == "") then { _displayNameShort = getText (_x >> "displayName"); };


			_categoryEntries pushBack [
				configName _x,
				getText (_x >> "displayName"),
				getText (_x >> "listPath"),
				_color,
				_displayNameShort,
				getText (_x >> "markerPath"),
				getNumber (_x >> "uptime")
			];
		};
		nil;
	} count ("true" configClasses (configFile >> "FRL" >> "CfgSpotting" >> (configName _x)));
	if (count _categoryEntries > 0) then {
		(_allEntries select 0) pushBack (configName _x);
		(_allEntries select 1) pushBack (getText (_x >> "displayName"));
		(_allEntries select 2) pushBack _categoryEntries;
	};
	nil;
} count (configProperties [(configFile >> "FRL" >> "CfgSpotting"), "getNumber (_x >> 'category') >= 1", true]);
GVAR(spottingOptions) = _allEntries;
