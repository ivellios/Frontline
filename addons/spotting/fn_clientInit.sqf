/*
    Function:       FRL_Spotting_fnc_clientInit
    Author:         Adanteh
    Description:    Inits spotting system
*/
#define __INCLUDE_DIK
#include "macros.hpp"

#include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Inits spotting system
 *
 *	Example:
 *	[player] call FUNC(clientInit);
 */

call FUNC(cacheOptions);

GVAR(spotIndex) = 0;
GVAR(spotList) = [];
GVAR(handle) = [{
	private _delete = false;
    {
        if (_x select 0 >= time) exitWith {};
        [(_x select 1)] call CFUNC(removeMapGraphicsGroup);
        _delete = true;
        GVAR(spotList) set [_forEachIndex, objNull];
    } forEach GVAR(spotList);
    if (_delete) then {
       GVAR(spotList) = GVAR(spotList) - [objNull];
    };
}, 1] call MFUNC(addPerFramehandler);

[QGVAR(add), {
	(_this select 0) params ["_spotId", "_categoryId", "_pos", "_caller"];

	private _entry = ((GVAR(spottingOptions) select 2) select _categoryId) select _spotId;
	_entry params ["_name", "_displayName", "_listPath", "_color", "_displayNameShort", "_markerPath", "_uptime"];
	_pos set [2, 0];

	private _markerID = format ["%1_%2", _name, GVAR(spotIndex)];
    GVAR(spotList) pushBack [(time + _uptime), _markerID];
	GVAR(spotIndex) = GVAR(spotIndex) + 1;

	// -- Get the color of the opposite team -- //
	if (_color isEqualType "") then {
		switch (_color) do {
			case "friendly": {
				_color = +([playerSide, "color"] call MFUNC(getSideData));
			};
			case "enemy": {
				private _enemySide = [playerSide] call MFUNC(getSideOpposite);
				_color = +([_enemySide, "color"] call MFUNC(getSideData));
			};
		};
	};
	private _icon = ["ICON", _markerPath, _color, _pos, 25, 25, 0, "", 0];
    private _normalText = ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _pos, 25, 25, 0, _displayNameShort, 2, 0.075];
    [_markerID, [_icon, _normalText], "normal"] call CFUNC(addMapGraphicsGroup);

    private _code = compile format ["
    	private _timeSince = [ceil (time - %1), 'MM:SS'] call bis_fnc_secondsToString;
    	_text = '%2 [' + _timeSince + ' ago] (%3)';
    ", time, _displayName, _caller];
    private _iconHover = ["ICON", _markerPath, _color, _pos, 25, 25, 0, "", 0];
    private _normalTextHover = ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _pos, 25, 25, 0, _displayName, 2, 0.075, nil, nil, _code];
    [_markerID, [_iconHover, _normalTextHover], "hover"] call CFUNC(addMapGraphicsGroup);

}] call CFUNC(addEventHandler);

["map.RightClick.short", {
    if ([DIK_LCONTROL] call MFUNC(keyPressed)) exitWith { };
    if ([DIK_LSHIFT] call MFUNC(keyPressed)) exitWith { };
    if ([DIK_LALT] call MFUNC(keyPressed)) exitWith { };

    createDialog QGVAR(spotDialog);
}] call CFUNC(addEventHandler);
