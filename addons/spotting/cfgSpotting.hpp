class cfgSpotting {
	class DefaultSpot {
		scope = 2;
		markerColor[] = {1, 1, 1, 0.8};
		displayName = "Unknown";
		displayNameShort = "INF";
		markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_unknown.paa";
		listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_unknown.paa";
		uptime = 60;
	};

	class Spotting {
		category = 1;
		displayName = "SPOT";

		class Infantry: DefaultSpot {
			markerColor = "enemy"; // -- blue for opfor, red for blufor
			displayName = "Infantry";
			displayNameShort = "INF";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf.paa";
		};

		class AntiTank: Infantry {
			displayName = "Anti-Tank";
			displayNameShort = "AT";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_atk.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_atk.paa";
		};

		class AntiAir: Infantry {
			displayName = "Anti-Air";
			displayNameShort = "AA";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_aa.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_aa.paa";
		};

		class Sniper: Infantry {
			displayName = "Sniper";
			displayNameShort = "SNP";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_snpr.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_snpr.paa";
		};

		class Machinegunner: Infantry {
			displayName = "Machinegunner";
			displayNameShort = "MG";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_mg.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_mg.paa";
		};

		class UnarmedVehicle: Infantry {
			displayName = "Unarmed Vehicle";
			displayNameShort = "VHC";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_motorised.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_motorised.paa";
		};

		class ArmedVehicle: Infantry {
			displayName = "Armed Vehicle";
			displayNameShort = "Armd. VHC";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_motorised_wpn.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_inf_motorised_wpn.paa";
		};

		class ArmoredVehicle: Infantry {
			displayName = "Armored Vehicle";
			displayNameShort = "AMR";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_armd.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_armd.paa";
		};

		class Helicopter: Infantry {
			displayName = "Helicopter";
			displayNameShort = "HELI";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_avn_rotary.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_avn_rotary.paa";
			uptime = 30;
		};

		class Air: Helicopter {
			displayName = "Aircraft";
			displayNameShort = "JET";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_avn_fixed.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_avn_fixed.paa";
		};

		class StaticWeapon: Infantry {
			displayName = "Static Weapon";
			displayNameShort = "HMG";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_hmg.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_hmg.paa";
			uptime = 240;
		};

		class Outpost: Infantry {
			displayName = "Forward Outpost";
			displayNameShort = "FO";
			markerPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_hq.paa";
			listPath = "\pr\frl\addons\spotting\markers\opfor\frl_o_hq.paa";
			uptime = 240;
		};
	};

	class Request {
		category = 1;
		displayName = "REQUEST";

		class Medic: DefaultSpot {
			displayName = "Medic";
			markerColor= "friendly";  // -- red for opfor, blue for blufor
			displayNameShort = "MED";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_med.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_med.paa";
		};

		class Outpost: Medic {
			displayName = "Deploy FO";
			displayNameShort = "FO";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_hq.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_hq.paa";
			uptime = 120;
		};

		class Defend: Medic {
			displayName = "Defend";
			displayNameShort = "DEF";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_labor.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_labor.paa";
			uptime = 60;
		};

		class Attack: Medic {
			displayName = "Attack";
			displayNameShort = "ATK";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_recce_snpr.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_recce_snpr.paa";
			uptime = 60;
		};

		class Defenses: Medic {
			displayName = "Build Defenses";
			displayNameShort = "BLD";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_labor.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_labor.paa";
			uptime = 120;
		};

		class ATSupport: Medic {
			displayName = "AT Support";
			displayNameShort = "AT";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_atk.paa";
		};

		class AASupport: Medic {
			displayName = "AA Support";
			displayNameShort = "AA";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_aa.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_aa.paa";
		};

		class Transport: Medic {
			displayName = "Transport";
			displayNameShort = "TRANS";
			markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_motorised.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_inf_motorised.paa";
		};

        class Commander_Arty: Medic {
            displayName = "Artillery";
            displayNameShort = "ART";
            markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_fdarty.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_fdarty.paa";
            uptime = 30;
        };
        class Commander_Mortar: Commander_Arty {
            displayName = "Mortar";
            displayNameShort = "MTR";
            markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_mor.paa";
			listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_mor.paa";
        };
        class Commander_MortarSmoke: Commander_Mortar {
            displayName = "Mortar Smoke";
            displayNameShort = "SMK";
        };
        class Commander_CAS: Commander_Arty {
            displayName = "CAS";
            displayNameShort = "CAS";
            markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_avn_fixed.paa";
            listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_avn_fixed.paa";
        };
        class Commander_supply: Commander_Arty {
            displayName = "Supplies";
            displayNameShort = "SPL";
            markerPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_sup.paa";
            listPath = "\pr\frl\addons\spotting\markers\blufor\frl_b_sup.paa";
        };
	};

};
