#include "macros.hpp"
/*
 *	File: fn_populateMenu.sqf
 *	Author: Adanteh
 *	Adds all entries into spotting from config
 *
 *	0: Which config subclass to populate the menu with <STRING>
 *
 *	Example:
 *	[player] call FUNC(populateMenu);
 */

#define CTRL(x) ((uiNamespace getVariable QGVAR(spotDialog)) displayCtrl x)

params ["_categoryIndex"];
private _categoryEntries = +((GVAR(spottingOptions) select 2) select _categoryIndex);
lbClear CTRL(4);


{
	_x params ["", "_displayName", "_listPath", "_color"];
	private _lbIndex = CTRL(4) lbAdd _displayName;
	CTRL(4) lbSetPicture [_lbIndex, _listPath];
	CTRL(4) lbSetTooltip [_lbIndex, _displayName];
	CTRL(4) lbSetData [_lbIndex, str _forEachIndex];


	if (_color isEqualType "") then {
		switch (_color) do {
			case "friendly": {
				_color = +([playerSide, "color"] call MFUNC(getSideData));
			};
			case "enemy": {
				private _enemySide = [playerSide] call MFUNC(getSideOpposite);
				_color = +([_enemySide, "color"] call MFUNC(getSideData));
			};
		};
	};
	CTRL(4) lbSetPictureColor [_lbIndex, _color];
} forEach _categoryEntries;
