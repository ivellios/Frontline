#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            dependencies[] = {"Main"};
            path = "\pr\FRL\addons\earplugs";

            class clientInit;
            class clientInitAutoEarplug;
            class toggleEarplugs;
        };
    };
};
