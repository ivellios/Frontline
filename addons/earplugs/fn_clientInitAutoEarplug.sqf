/*
    Function:       FRL_Earplugs_fnc_clientInitAutoEarplug
    Author:         Adanteh
    Description:    Does the events for auto earplug detection
*/
#include "macros.hpp"

["vehicleChanged", {
    if !(GVAR(autoEarplugs)) exitWith { false };
	(_this select 0) params ["_vehicle"];
    if (_vehicle isKindOf "CaManBase") exitWith {
        if (GVAR(earplugsEnabled)) then {
            [true] call FUNC(toggleEarplugs);
        };
    };
    private _isVehicle = call {
        if (_vehicle isKindOf "Car") exitWith { true };
        if (_vehicle isKindOf "Tank") exitWith { true };
        if (_vehicle isKindOf "Air") exitWith { true };
        if (_vehicle isKindOf "Motorcycle") exitWith { true };
        if (_vehicle isKindOf "Ship") exitWith { true };
        false;
    };
    if (_isVehicle) then {
        if !(GVAR(earplugsEnabled)) then {
            [true] call FUNC(toggleEarplugs);
        };
    };
}] call CFUNC(addEventHandler);
