/*
    Function:       FRL_Earplugs_fnc_clientInit
    Author:         Adanteh
    Description:    Adds settings, vars and keybind for earplugs
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(earplugsEnabled) = false;
GVAR(autoEarplugs) = profileNamespace getVariable [QGVAR(autoEarplugs), true];
GVAR(dampenLevel) = profileNamespace getVariable [QGVAR(dampenLevel), 0.8];
["missionStarted", {

	['add', ["gui", 'Frontline Settings', 'Earplugs', [
		'Auto Earplugs',
		'Automatically enable earplugs when entering a vehicle',
		QMVAR(CtrlCheckbox),
		{ GVAR(autoEarplugs); },
		true,
		{
			GVAR(autoEarplugs) = !GVAR(autoEarplugs);
			profileNamespace setVariable [QGVAR(autoEarplugs), GVAR(autoEarplugs)];
		},
		[]
	]]] call MFUNC(settingsWindow);

	['add', ["gui", 'Frontline Settings', 'Earplugs', [
		'Dampen Level',
		'How much the sound level should be dampened',
		QMVAR(CtrlSlider),
		{ GVAR(dampenLevel); },
		true,
		{
			GVAR(dampenLevel) = (_this select 1);
			profileNamespace setVariable [QGVAR(dampenLevel), GVAR(dampenLevel)];
		},
		[[0, 1], [0.05,0.5]]
	]]] call MFUNC(settingsWindow);

	["General","earplug_toggle", ["Earplugs","Toggles the earplugs"],
		{ call FUNC(toggleEarplugs); true },
		{ },
		[DIK_F1, [false, false, false]], false, false] call MFUNC(addKeybind);
}] call CFUNC(addEventHandler);
