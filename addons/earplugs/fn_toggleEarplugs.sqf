/*
    Function:       FRL_Earplugs_fnc_toggleEarplugs
    Author:         Adanteh
    Description:    Toggles the earplug
*/
#include "macros.hpp"

params [["_autoAdd", false]];
if (GVAR(earplugsEnabled)) then {
    ["Took out earplugs", "ok"] call MFUNC(notificationShow);
    0.3 fadeSound 1;
    GVAR(earplugsEnabled) = false;
} else {
    ["Put in Earplugs", "ok"] call MFUNC(notificationShow);
    0.3 fadeSound (1 - GVAR(dampenLevel));
    GVAR(earplugsEnabled) = true;
};
