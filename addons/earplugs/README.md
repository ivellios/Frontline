## Contents

This PBO includes the earplugs module.
It lowers volume when pressing F1.

Additionally it has an auto-earplug setting, which will automatically lower the sound level when you enter vehicles.
Settings for auto-earplug and a slider for the actual fading volume are available in Esc > FRL-Settings menu
