#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"FRL_Main"};
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
    };
};

class FRL {
    class Modules {
        class MODULE {
            defaultLoad = 0;
            dependencies[] = {"Main"};

            class init;
            class initCaptureWarning;
            class serverInitBleed;
            class serverInitDeployment;
            class serverInitVictory;
            class serverInit;
            class serverInitDebug;
            class clientInit;
            class createSectorLogic;
            class dependencyLine;
            class drawSector;
            class endMission;
            class getSector;
            class isCaptureable;
            class sectorUpdatePFH;
            class showCaptureStatus;
            class updateDependencies;
        };
    };
};
