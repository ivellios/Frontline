 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector functions, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"
params ["_sector"];
private _side = _sector getVariable ["side",sideUnknown];
private _activeSides = [];
private _currentCount = {
    private _currentSectorSide = ([_x] call FUNC(getSector)) getVariable ["side",sideUnknown];
    if (_currentSectorSide != sideUnknown) then {
        _activeSides pushBackUnique _currentSectorSide;
    };
    !(_currentSectorSide in [sideUnknown,_side])
} count (_sector getVariable ["dependency",[]]);

if (isServer) then {
    _sector setVariable ["activeSides", _activeSides];
};

(_currentCount > 0);
