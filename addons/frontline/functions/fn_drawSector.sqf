 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector_fnc_drawSector, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"
/*
 *    File: fn_drawSector.sqf
 *    Author: Adanteh
 *    This prepares the sector markers, positions and spawn icons
 *
 *    Example:
 *    call FUNC(drawSector);
 *
*/

params ["_sector"];

private _marker = _sector getVariable ["marker",""];
private _side = _sector getVariable ["side", sideUnknown];

if (isServer) then {
    if (_marker != "") then {
        _marker setMarkerColor format["Color%1", [_side, "Black"] select (_side isEqualTo sideUnknown)];
        _marker setMarkerBrush "SolidBorder";
        _marker setMarkerAlpha 0.8;
    };
};

if (hasInterface) then {
    private _color = [];
    private _icon = "";
    private _id = format [QGVAR(ID_%1), _marker];
    private _background = "";
    private _activeSides = [];
    private _attack = false;
    private _defend = false;
    private _fullname = _sector getVariable ["fullname", ""];

    {
        _activeSides pushBackUnique (([_x] call FUNC(getSector)) getVariable ["side",sideUnknown]);
        nil
    } count (_sector getVariable ["dependency",[]]);

    if (count _activeSides > 1 && playerSide in _activeSides) then {
        if (playerSide == _side) then {
            if (_sector call FUNC(isCaptureable)) then {
                _defend = true;
                _icon = "\A3\ui_f\data\igui\cfg\simpleTasks\types\defend_ca.paa";
                _color = [0.01, 0.67, 0.92, 1];
                _background = "\A3\ui_f\data\igui\cfg\simpleTasks\background3_over_ca.paa";
                GVAR(defendZonePos) = getPos _sector;
            } else {
                if (_x getvariable ["isSpawn", false]) then {
                    _icon = "\A3\ui_f\data\igui\cfg\simpleTasks\types\walk_ca.paa";
                    _color = [1, 1, 1, 1];
                };
            };
        } else {
            _color = [0.99, 0.26, 0, 1];
            GVAR(attackZonePos) = getPos _sector;
            if (_sector getVariable ["isActive", false]) then {
                _attack = true;
                _icon = "\A3\ui_f\data\igui\cfg\simpleTasks\types\attack_ca.paa";
                _background = ["\A3\ui_f\data\igui\cfg\simpleTasks\background1_ca.paa", "\A3\ui_f\data\igui\cfg\simpleTasks\background3_over_ca.paa"] select (_sector getVariable ["isActive", false]);
            } else {
                _icon = "\A3\ui_f\data\igui\cfg\simpleTasks\types\walk_ca.paa";
                _background = "\A3\ui_f\data\igui\cfg\simpleTasks\background1_ca.paa";
            };
        };
    } else {
        if !(_side isEqualTo sideUnknown) then {
            if (_x getVariable ["isSpawn", false]) then {
                _icon = "\A3\ui_f\data\igui\cfg\simpleTasks\types\walk_ca.paa";
                _color = [[0.99, 0.26, 0, 1], [1, 1, 1, 1]] select (_side isEqualTo playerSide);
            };
        };
    };

    private _zoneGraphics = [];
    if (_icon != "") then {
        if (_background != "") then {
            _zoneGraphics pushBack ["ICON", _background, [1,1,1,1], getMarkerPos _marker, 30, 30];
        };
        _zoneGraphics pushBack ["ICON", _icon, _color, getMarkerPos _marker, 20, 20];
    };

    private _designator = _sector getVariable ["designator", ""];
    [_id, (_zoneGraphics + [["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], getMarkerPos _marker, 25, 25, 0, _designator, 2]])] call CFUNC(addMapGraphicsGroup);
    [_id, (_zoneGraphics + [["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], getMarkerPos _marker, 25, 25, 0, _fullname, 2]]), "hover"] call CFUNC(addMapGraphicsGroup);
};
