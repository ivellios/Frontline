#include "macros.hpp"
/*
 *	File: fn_serverInitDebug.sqf
 *	Author: Adanteh
 *	Debug functions for A&S mode. Only use define this in dev mode
 *
 */

#ifdef ISDEV
// -- Allows setting zone owner -- //
DFUNC(debugSetOwner) = { // ["C", playerSide] call frl_frontline_fnc_debugSetOwner;
	params [["_sector", "A", ["", objNull]], ["_side", playerSide]];
	if (_sector isEqualType "") then {
		{
			if ((_x getVariable ["designator", "-"]) == _sector) exitWith {
				_sector = _x;
				nil;
			};
			nil;
		} count GVAR(allSectorsArray);
	};

	_lastSide = _sector getVariable ["side", sideUnknown];

	_sector setVariable ["captureRate", 0, true];
	_sector setVariable ["captureProgress", 1, true];
	_sector setVariable ["lastCaptureTick", serverTime, true];
	_sector setVariable ["side", _side, true];
	_sector setVariable ["attackerSide", _side, true];

	["sectorSideChanged", [_sector, _lastSide, _side]] call CFUNC(globalEvent);
	_sector setVariable ["firstCaptureDone", true, true];
};

DFUNC(showSectorNames) = { // call frl_frontline_showSectorNames;
	{
		private _sectorName = vehicleVarName _x;
		private _sectorPos = getMarkerPos (_x getVariable ["marker", ""]);
		private _id = format ["dbg_%1", _sectorName];
		_sectorPos set [1, (_sectorPos select 1) - 25];
		[_id, [["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], _sectorPos, 25, 25, 0, _sectorName, 2]]] call CFUNC(addMapGraphicsGroup);
		nil;
	} count GVAR(allSectorsArray);
};
#endif
