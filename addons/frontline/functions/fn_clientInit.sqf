 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector functions, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

 #include "macros.hpp"
/*
 *	File: fn_clientInit.sqf
 *	Author: Adanteh
 *	Handles sector related client tasks
 *
 *	Example:
 *	[player] call FUNC(clientInit);
 */

GVAR(currentSector) = objNull;
GVAR(attackZonePos) = [0, 0, 0];
GVAR(defendZonePos) = [0, 0, 0];
GVAR(linkMarkers) = [];

// -- Distance to enemy zone and max distance from friendly zone
["placeFOonCall", {
    params ["_caller", "_pos"];
    private _minDistance = [QEGVAR(rolesspawns,FO_minDistanceEnemyZone), 200] call MFUNC(cfgSetting);
    private _maxDistance = [QEGVAR(rolesspawns,FO_maxDistanceFriendlyZone), 400] call MFUNC(cfgSetting);
    private _withinFriendlyRange = false;
    private _withinEnemyRange = false;
    private _zoneMarkers = +(missionNamespace getVariable [QGVAR(allSectorsArray), []]);
    {
    	private _sector = _x;
    	private _sectorMarker = _sector getVariable ["marker", ""];
    	private _sectorSize = selectMax (getMarkerSize _sectorMarker);

    	if ((_sector getVariable ["side", sideUnknown]) != playerSide) then {
    		if ((CLib_Player distance2D _sector) < (_minDistance + _sectorSize)) exitWith {
    			_withinEnemyRange = true;
    		};
    	} else {
    		if ((CLib_Player distance2D _sector) < (_maxDistance + _sectorSize)) then {
    			_withinFriendlyRange = true;
    		};
    	};

    	if (_withinEnemyRange) exitWith { nil; };
        nil
    } count _zoneMarkers;
    private _canPlace = (!_withinEnemyRange && _withinFriendlyRange);
    if !(_canPlace) then {
        if (_withinEnemyRange) exitWith {
            ["showNotification", [format ["Can't place FOs within %1m of enemy zone", _minDistance]]] call CFUNC(localEvent);
        };
        if !(_withinFriendlyRange) exitWith {
            ["showNotification", [format ["FO needs to be within %1m of friendly zone", _maxDistance]]] call CFUNC(localEvent);
        };
    };
    _canPlace
}] call MFUNC(addCondition);


["missionStarted", {
    [QGVAR(LoadingScreen)] call MFUNC(startLoadingScreen);

    ["fadeHud", {
        (_this select 0) params ["_fade", "_commit"];
        if (isNull (uiNamespace getVariable [QSVAR(CaptureBar), displayNull])) exitWith { };
        ((uiNamespace getVariable QSVAR(CaptureBar)) displayCtrl 1000) ctrlSetFade _fade;
        ((uiNamespace getVariable QSVAR(CaptureBar)) displayCtrl 1000) ctrlCommit _commit;
    }] call CFUNC(addEventHandler);

	[{
	    // -- Draw lines showing the dependencies -- //
		{
			private _sector = _x;
			private _sectorMarker = _x getVariable ["marker", ""];
			{
				private _marker = ([_x] call FUNC(getSector)) getVariable ["marker", ""];
				GVAR(linkMarkers) pushBack ([_sectorMarker, _marker, "ColorBlack", "link"] call FUNC(dependencyLine));

				nil;
			} count (_sector getVariable ["dependency", []]);
			nil;
		} count GVAR(allSectorsArray);

		// -- Rally point sector conditions -- //
		["placeRPonCall", {
			params ["_caller", "_pos"];
			private _closeZone = objNull;
			private _minDistance = [QEGVAR(rolesspawns,Rally_minDistanceEnemyZone), 100] call MFUNC(cfgSetting);
			private _conditionPassed = true;
			{
				private _sector = _x;
				if !((_sector getVariable ["side", sideUnknown]) in [sideUnknown, (side group _caller)]) then {
					private _sectorMarker = _sector getVariable ["marker", ""];
					private _sectorSize = selectMax (getMarkerSize _sectorMarker);
					if ((_caller distance _sector) < (_minDistance + _sectorSize)) exitWith {
						_closeZone = _sector;
						_conditionPassed = false;
						[(format ["You can not place a RP within %1m of enemy zone", _minDistance])] call MFUNC(notificationShow);
						nil;
					};
				};

				if !(_conditionPassed) exitWith { nil; };
			    nil
			} count GVAR(allSectorsArray);
			_conditionPassed
		}] call MFUNC(addCondition);


		// -- Raise event after load to force update all markers -- //
		[{
			["frontlineShift"] call CFUNC(localEvent);
        	[QGVAR(LoadingScreen)] call MFUNC(endLoadingScreen);
		}, 2.5] call CFUNC(wait);
    }, { !isNil QGVAR(ServerInitDone) && {GVAR(ServerInitDone)} }] call CFUNC(waitUntil);
}] call CFUNC(addEventhandler);

["sectorEntered", {
    (_this select 0) params ["_unit", "_sector"];
    [true, _sector] call FUNC(showCaptureStatus);
}] call CFUNC(addEventHandler);

["sectorLeaved", {
    [false] call FUNC(showCaptureStatus);
    GVAR(currentSector) = objNull;
}] call CFUNC(addEventHandler);

["sectorSideChanged", {
    (_this select 0) params ["_sector", "_oldSide", "_newSide"];

    private _sectorName = _sector getVariable ["fullName", ""];

    if ((side group Clib_Player) isEqualTo _newSide) exitWith {
        if (GVAR(currentSector) isEqualTo _sector) then {
            [format["You captured sector %1", _sectorName], [_newSide, 'color'] call MFUNC(getSideData)] call MFUNC(notificationShow);
        } else {
            [format["Your team captured sector %1", _sectorName], [_newSide, 'color'] call MFUNC(getSideData)] call MFUNC(notificationShow);
        };
    };

    if ((side group Clib_Player) isEqualTo _oldSide) exitWith {
        [format["You lost sector %1", _sectorName], [_oldSide, 'color'] call MFUNC(getSideData)] call MFUNC(notificationShow);
    };

    if (sideUnknown isEqualTo _newSide && !((side group Clib_Player) isEqualTo _oldSide)) exitWith {
        private _attackerSide = _sector getVariable ["attackerSide", sideUnknown];
        if (GVAR(currentSector) isEqualTo _sector) then {
            [format["You neutralized sector %1", _sectorName], [_attackerSide, 'color'] call MFUNC(getSideData)] call MFUNC(notificationShow);
        } else {
            [format["Your team neutralized sector %1", _sectorName], [_attackerSide, 'color'] call MFUNC(getSideData)] call MFUNC(notificationShow);
        };
    };
}] call CFUNC(addEventHandler);


// -- Find the main bases -- //
["mainbaseReached", {
	(_this select 0) params ["_winnerside", "_base"];
	_winner = (_winnerSide isEqualTo playerSide);
	["All zones were captured", _winner] call FUNC(endMission);
}] call CFUNC(addEventHandler);

["durationEnd", { ["Mission time ran out"] call FUNC(endMission)}] call CFUNC(addEventHandler);

// -- Show frontline shift timer in top right on map
/*
[{
	private _text = "";
	private _timeLeft = (missionNamespace getVariable [QGVAR(frontlineShift), -1]) - serverTime;
	private _durationLeft = if (_timeLeft <= 0) then { "--:--" } else { ([ceil _timeLeft,"MM:SS"] call bis_fnc_secondsToString) };
	private _durationText = format ["<t align='left'>SPAWNPOINT SHIFT<t size='1' font='EtelkaMonospacePro' align='right' color='#ffffff'> %1</t></t>", _durationLeft];
	_durationText
}, 2] call MFUNC(addMapHUD);
*/;
// -- Detection of entering and leaving zones
[{
    if (isNil QGVAR(allSectorsArray)) exitWith {};
    scopeName "MAIN";
    if (alive Clib_Player) then {
        {
            private _marker = _x getVariable ["marker",""];
            if (_marker != "") then {
                if (Clib_Player inArea _marker) then {
                    if (GVAR(currentSector) != _x) then {
                        if (!isNull GVAR(currentSector)) then {
                            if (!isServer) then {
                                ["sectorLeaved", [Clib_Player, GVAR(currentSector)]] call CFUNC(serverEvent);
                            };
                            ["sectorLeaved", [Clib_Player, GVAR(currentSector)]] call CFUNC(localEvent);
                        };

                        GVAR(currentSector) = _x;

                        if (!isServer) then {
                            ["sectorEntered", [Clib_Player, GVAR(currentSector)]] call CFUNC(serverEvent);
                        };
                        ["sectorEntered", [Clib_Player, GVAR(currentSector)]] call CFUNC(localEvent);

                    };
                    breakOut "MAIN";
                };
            };
            nil;
        } count GVAR(allSectorsArray);
    };

    if (!isNull GVAR(currentSector)) then {
        if (!isServer) then {
            ["sectorLeaved", [Clib_Player, GVAR(currentSector)]] call CFUNC(serverEvent);
        };
        ["sectorLeaved", [Clib_Player, GVAR(currentSector)]] call CFUNC(localEvent);
    };

}, 0.1, []] call MFUNC(addPerFramehandler);
