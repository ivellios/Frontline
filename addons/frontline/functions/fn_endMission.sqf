/*
    Function:       FRL_Frontline_fnc_endMission
    Author:         Adanteh
    Description:    Ends the mission, establishing winner based on tickets
*/
#include "macros.hpp"

params [["_reason", ""], "_winner", ["_description", ""]];

if (isNil "_winner") then {
    private _playerSideTickets = [playerSide] call EFUNC(tickets,getSideTickets);
    private _otherSideTickets = [[playerSide] call MFUNC(getSideOpposite)] call EFUNC(tickets,getSideTickets);
    _winner = (_playerSideTickets >= _otherSideTickets);
};

[(["FRL_LOOSER", "FRL_WINNER"] select _winner), _winner, _reason, _description] call MFUNC(endMission);

EGVAR(Tickets,deactivateTicketSystem) = true;
