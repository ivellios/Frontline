 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector functions, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

 #include "macros.hpp"
/*
    Project Reality ArmA 3

    Author: BadGuy, joko // Jonas

    Description:
    Toggle Status Layer of current Sector

    Parameter(s):
    0: Show or Hide Layer <Bool>
    1: Sector <String>

    Returns:
    None
*/

#define CTRL(var1) ((uiNamespace getVariable QSVAR(CaptureBar)) displayCtrl var1)
params ["_show", ["_sectorObject", objNull]];

if (_show) then {
    if !(isNull (uiNamespace getVariable [QSVAR(CaptureBar), displayNull])) exitWith {
        [{
            if (GVAR(currentSector) != (_this select 1)) exitWith {};
            [true, _this select 1] call FUNC(showCaptureStatus);
        }, {
            (isNull (uiNamespace getVariable [QSVAR(CaptureBar), displayNull]))
        }, _this] call CFUNC(waitUntil);
    };

    QSVAR(CaptureBar) cutRsc [QSVAR(CaptureBar),"PLAIN", 0.3];
    if (GVAR(captureStatusPFH) != -1) then {
        [GVAR(captureStatusPFH)] call MFUNC(removePerFrameHandler);
    };
    GVAR(captureStatusPFH) = [{
        (_this select 0) params ["_sector"];

        private _background = +([_sector getVariable ["side",sideUnknown], 'color'] call MFUNC(getSideData));
        _background set [3, 0.2];
        CTRL(1002) ctrlSetStructuredText parseText format ["<t font='%3'>%1</t>  %2", _sector getVariable ["designator",""], _sector getVariable ["fullName",""], FONTBOLD];
        CTRL(1003) ctrlSetBackgroundColor _background;
        CTRL(1004) ctrlSetTextColor ([_sector getVariable ["attackerSide",sideUnknown], 'color'] call MFUNC(getSideData));
        CTRL(1004) progressSetPosition ((_sector getVariable ["captureProgress",0]) + (serverTime - (_sector getVariable ["lastCaptureTick", serverTime])) * (_sector getVariable ["captureRate",0]));
    }, 0, [_sectorObject]] call MFUNC(addPerFramehandler);
} else {
    [GVAR(captureStatusPFH)] call MFUNC(removePerFrameHandler);
    GVAR(captureStatusPFH) = -1;

    QSVAR(CaptureBar) cutFadeOut 0.3;
};
