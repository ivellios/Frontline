 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector_fnc_drawSector, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"
/*
    Project Reality ArmA 3

    Author: BadGuy, joko // Jonas

    Description:
    Get current Sector from DataBase

    Parameter(s):
    0: Sector Name <String>

    Returns:
    Sector Object <Object>
*/

params ["_sector"];

GVAR(allSectors) getVariable [_sector,objNull];
