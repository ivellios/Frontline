/*
    Function:       FRL_Frontline_fnc_initCaptureWarning
    Author:         Adanteh
    Description:    System for showing a warning when a zone is dropping below certain threshold
*/
#include "macros.hpp"

#define __WARNINGTHRESHOLD 0.75
if (isServer) then {
    [QGVAR(capLevelChanged), {
        (_this select 0) params ["_sector", "_captureProgress"];
        if (_captureProgress <= __WARNINGTHRESHOLD) then {
            if !(_sector getVariable ["warningshown", false]) then {
                _sector setVariable ["warningshown", true];
                private _side = (_sector getVariable ["side", false]);
                if !(_side isEqualTo sideUnknown) then {
                    [QGVAR(captureWarning), _side, [_sector]] call CFUNC(targetEvent);
                };
            };
        } else {
            // -- Reset the warning when above 0.75 again
            if (_sector getVariable ["warningshown", false]) then {
                _sector setVariable ["warningshown", false];
            };
        };
    }] call CFUNC(addEventHandler);
};

if (hasInterface) then {
    [QGVAR(captureWarning), {
        (_this select 0) params ["_sector"];
        private _sectorName = _sector getVariable ["fullname", ""];
        private _designator = _sector getVariable ["designator", ""];
        [(format ["Zone [%2] %1 is under attack!", _sectorName, _designator])] call MFUNC(notificationShow);
    }] call CFUNC(addEventHandler);
};
