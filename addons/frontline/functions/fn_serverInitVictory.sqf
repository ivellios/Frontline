 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector functions, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

 #include "macros.hpp"
/*
 *	File: fn_serverInitVictory.sqf
 *	Author: Adanteh
 *	Inits the victory conditions on the server
 *
 *	Example:
 *	[player] call FUNC(serverInitVictory);
 */

// -- Check if main base is attackaable -- //
["frontlineShift", {
	private _baseAttack = false;
	private _winnerSide = sideUnknown;
	private _base = locationNull;
    {
        private _sectorSide = _x getVariable ["side", sideUnknown];
        if (_sectorSide != sideUnknown) then {
            {
                private _sector = [_x] call FUNC(getSector);
                if ((_sector getVariable ["isBase", false]) && {((_sector getVariable ["side", sideUnknown]) != _sectorSide)}) exitWith {
                    _baseAttack = true;
                    _winnerSide = _sectorSide;
                    _base = _sector;
                    nil;
                };
                nil;
            } count (_x getVariable ["dependency", []])
        };

        if (_baseAttack) exitWith { nil };
        nil;
    } count GVAR(allSectorsArray);
    if (_baseAttack) then {
    	["mainbaseReached", [_winnerSide, _base]] call CFUNC(globalEvent);
    };
}] call CFUNC(addEventhandler);

// -- End on server -- //
if !(hasInterface) then {
    ["mainbaseReached", {
        ["All zones were captured", false] call FUNC(endMission);
    }] call CFUNC(addEventHandler);

    ["durationEnd", {
        ["Mission time ran out", false] call FUNC(endMission);
    }] call CFUNC(addEventHandler);
};
