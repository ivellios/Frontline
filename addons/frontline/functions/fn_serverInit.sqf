 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector functions, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

 #include "macros.hpp"
/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Defines debug functions on server only
 *
 *	Example:
 *	[player] call FUNC(serverInit);
 */


GVAR(allSectors)  = objNull;
GVAR(allSectorsArray) = [];
GVAR(ServerInitDone) = false;
GVAR(FrontlineShift) = -1;
GVAR(FrontlinePath) = "";

["sectorEntered", {
    (_this select 0) params ["_unit", "_sector"];

    private _varStr = format ["units%1", side _unit];
    private _unitArray = _sector getVariable [_varStr, []];
    _unitArray pushBackUnique _unit;
    _sector setVariable [_varStr, _unitArray];

}] call CFUNC(addEventhandler);

["sectorLeaved", {
    (_this select 0) params ["_unit", "_sector"];

    private _varStr = format ["units%1", side _unit];
    private _unitArray = _sector getVariable [_varStr, []];
    private _ind = _unitArray find _unit;
    if (_ind >= 0) then {
        _unitArray deleteAt _ind;
        _sector setVariable [_varStr, _unitArray];
    };
}] call CFUNC(addEventhandler);

["sectorSideChanged", FUNC(updateDependencies)] call CFUNC(addEventhandler);

 // -- Find the main bases -- //
["createSector", {
	(_this select 0) params ["_sectorconfig", "_sector"];
	private _sectortype = getText (_sectorconfig >> "sectortype");
	if (_sectortype == "mainbase") then {
		_sector setVariable ["isBase", true, true];
	};
}] call CFUNC(addEventHandler);

["missionStarted", {
    [{
        GVAR(allSectors) = (call CFUNC(getLogicGroup)) createUnit ["Logic", [0,0,0], [], 0, "NONE"];

        GVAR(allSectorsArray) = [];
        private _sectors = "true" configClasses (missionConfigFile >> QUOTE(PREFIX) >> "CfgSectors");


        {
            if ((configName _x find "base") >= 0) then {
                [configName _x, _x, getArray(_x >> "dependency"), getNumber(_x >> "ticketValue"), getNumber(_x >> "minUnits"), getArray(_x >> "captureTime"), getArray(_x >> "firstCaptureTime"), getText(_x >> "designator")] call FUNC(createSectorLogic);
            };
            nil;
        } count _sectors;

        private _path = selectRandom ("true" configClasses (missionConfigFile >> QUOTE(PREFIX) >> "CfgSectors" >> "CfgSectorPath"));
        GVAR(FrontlinePath) = configName _path;

        {
            [configName _x, _x, getArray (_x >> "dependency"),getNumber (_x >> "ticketValue"),getNumber (_x >> "minUnits"),getArray (_x >> "captureTime"), getArray(_x >> "firstCaptureTime"), getText (_x >> "designator")] call FUNC(createSectorLogic);
            nil;
        } count ("true" configClasses _path);

        call FUNC(updateDependencies);

		{
	    	if (_x getVariable ["isActive", false]) then {
	    		_x setVariable ["isActive", true, true];
	    	};
	    	nil;
    	} count GVAR(allSectorsArray);
        ["ticketRecalc"] call CFUNC(localEvent);


        GVAR(ServerInitDone) = true;
        publicVariable QGVAR(allSectors);
        publicVariable QGVAR(allSectorsArray);
        publicVariable QGVAR(ServerInitDone);



    }, 2.5,[]] call CFUNC(wait);
}] call CFUNC(addEventhandler);
