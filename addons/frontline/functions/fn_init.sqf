#include "macros.hpp"
/*
 *	File: fn_init.sqf
 *	Author: Adanteh
 *	Unloads the AAW Sector stuff and adds in our own mechanics

 *	Example:
 *	[player] call FUNC(init);
 */


{
   [_x, {
        {
            [_x] call FUNC(drawSector);
            nil;
        } count GVAR(allSectorsArray);
    }] call CFUNC(addEventhandler);
    nil;
} count ["playerSideChanged", "sectorsUpdated", "frontlineShift"];

[{
	[{
		["sectorsUpdated"] call CFUNC(localEvent);
    }, 2.5] call CFUNC(wait);
}, { !isNil QGVAR(ServerInitDone) && {GVAR(ServerInitDone)}}] call CFUNC(waitUntil);

["endVote", {
    (_this select 0) params ["_reason", "_losingTeam"];
    private _winner = if (isNil "_losingTeam") then {
        nil;
    } else {
        !(playerSide isEqualTo _losingTeam)
    };
    [_reason, _winner] call FUNC(endMission);
}] call CFUNC(addEventHandler);

[QSVAR(endMissionAdmin), {
    (_this select 0) params ["_caller"];
    ["Mission ended by admin", "warning"] call MFUNC(notificationShow);
    [{ ["Mission ended by admin"] call FUNC(endMission)}, 5] call CFUNC(wait);
}] call CFUNC(addEventHandler);


["durationEnd", {
    ["Mission time has passed", nil, "In this case the winner is decided by who has the most tickets. You lose a ticket when dying and holding less than 50% of the map will cost your team tickets"] call FUNC(endMission);
}] call CFUNC(addEventHandler);

["victoryPointsReached", {
    ["Victory points reached", nil] call FUNC(endMission);
}] call CFUNC(addEventHandler);
