 /*---------------------------------------------------------------------------
     This is a modified version of AAW Sector_fnc_updateDependencies, June 11th 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"
/*
 *    File: fn_updateDependencies.sqf
 *    Author: Adanteh
 *    This updates the zones after capture, figure out next attack/defend zone and use Frontline shift mechanic
 *
 *    Example:
 *    call FUNC(updateDependencies);
 *
 */

if (isNil QGVAR(allSectorsArray)) exitWith {};

// -- If a zone was captureable and isn't anymore, disable it instantly -- //
{
    private _isActive = _x getVariable ["isActive", false];
    if !([_x] call FUNC(isCaptureable)) then {
        if (_isActive) then {
            _x setVariable ["isActive", false, true];
        };
        // -- If a zone wasn't a spawn yet, only activate it as a spawn when connected to friendly zones only (Not neutral!) -- //
        if !(_x getVariable ["isSpawn", false]) then {
            private _sectorSide = _x getVariable ["side", sideUnknown];
            private _nonFriendlyDeps = { _sectorSide != (([_x] call FUNC(getSector)) getVariable ["side",sideUnknown]) } count (_x getVariable ["dependency",[]]);
            if (_nonFriendlyDeps == 0 || !GVAR(ServerInitDone)) then {
                _x setVariable ["isSpawn", true, true];
            };
        };
    } else {
        if !(_isActive) then {
            _x setVariable ["isActive", true, true];
            _x setVariable ["lastCaptureTick", nil]; // Fix instant capture bug
            [FUNC(sectorUpdatePFH), 0, [_x]] call MFUNC(addPerFramehandler);
        };
        if (_x getVariable ["isSpawn", false]) then {
            _x setVariable ["isSpawn", false, false];
        };
    };
    nil;
} count GVAR(allSectorsArray);

["frontlineShift"] call CFUNC(globalEvent);
["sectorsUpdated"] call CFUNC(globalEvent);
