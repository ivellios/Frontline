#include "macros.hpp"
/*
 *	File: fn_dependencyLine.sqf
 *	Author: Adanteh
 *	Draws a line from one zone to another zone
 *
 *	0: The marker <OBJECT>
 *
 *	Returns:
 *	The marker ID <STRING>
 *
 *	Example:
 *	[_sectorMarker, _marker] call FUNC(dependencyLine);
 */


// -- Fix this so it draws from closest point to other closest point (Figure out which point of the shaped marker is closest to the other zone) -- //

private ["_fromSize","_toSize","_fromPos","_toPos","_fromDir","_toDir","_fromDirMarker","_toDirMarker","_vector","_vector2","_center","_distance","_marker","_lineTag","_lineColor"];

params ["_fromMarker", "_toMarker", "_lineColor", "_lineTag"];

_fromSize = getMarkerSize _fromMarker;
_toSize = getMarkerSize _toMarker;

_fromPos = (getmarkerpos _fromMarker);
_toPos = (getmarkerpos _toMarker);
_fromDir = _fromPos getdir _toPos;
_toDir = _toPos getDir _fromPos;
_fromDirMarker = markerDir _fromMarker;
_toDirMarker = markerDir _toMarker;

_fromPos set [0, (_fromPos select 0) + ((_fromSize select 0) * sin(_fromDir))];
_fromPos set [1, (_fromPos select 1) + ((_fromSize select 1) * cos(_fromDir))];

_toPos = [(_toPos select 0) + ((_toSize select 0) * sin(_toDir)), (_toPos select 1) + ((_toSize select 1) * cos(_toDir)), 0];

_vector =  _fromPos vectordiff _toPos;
_vector2 = _vector vectorMultiply 0.5;
_center = _toPos vectorAdd _vector2;

_distance  =_fromPos distance _toPos;

_marker = createMarkerLocal [format ["frl_%3_%1>%2", _fromMarker, _toMarker, _lineTag], _center];
_marker setMarkerShapeLocal "RECTANGLE";
_marker setMarkerColorLocal _lineColor;
_marker setMarkerSizeLocal [2,_distance/2];
_marker setMarkerDirLocal _fromDir;
_marker setmarkerposLocal _center;
_marker;