/*
    Function:       FRL_Frontline_fnc_serverInitDeployment
    Author:         Adanteh
    Description:    Adds server-side deployment mechanics for AS gamemode
*/
#include "macros.hpp"

// -- Max distance checks -- //
["vehicleSpawnBlocking", {
    params ["_availableFor", "_vehicle"];
    private _minDistance = [QEGVAR(rolesspawns,setting_minDistanceEnemyZone), 200] call MFUNC(cfgSetting);
    private _withinEnemyRange = false;
    private _zoneMarkers = +(missionNamespace getVariable [QGVAR(allSectorsArray), []]);
    {
        private _sector = _x;
        if ((_sector getVariable ["side", sideUnknown]) != _availableFor) then {
            private _sectorMarker = _sector getVariable ["marker", ""];
            private _sectorSize = selectMax (getMarkerSize _sectorMarker);
            
            if ((_vehicle distance2D _sector) < (_minDistance + _sectorSize)) exitWith {
                _withinEnemyRange = true;
            };
        };

        if (_withinEnemyRange) exitWith { nil; };
        nil
    } count _zoneMarkers;
    !_withinEnemyRange
}] call MFUNC(addCondition);
