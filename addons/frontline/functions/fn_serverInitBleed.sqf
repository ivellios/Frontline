#include "macros.hpp"
/*
 *	File: fn_serverInitBleed.sqf
 *	Author: Adanteh
 *	Runs the bleed mechanic for A&S gamemode
 *
 *	Example:
 *	[] call FUNC(initBleed);
 */

#define _LOOPTIME (0.5*60) // -- Ticket update speed -- //
#define _BLEEDPER (12.5*60) // -- This means bleed PER time frame (So 30 tickets on a zone is 30 tickets per 10 minutes)

GVAR(bleed) = [];

["createSector", {
    (_this select 0) params ["_sectorConfig", "_sector"];
    private _bleed = [20];
    if (isArray (_sectorconfig >> "bleedValue")) then {
        _bleed = getArray (_sectorconfig >> "bleedValue");
    } else {
        if (isNumber (_sectorconfig >> "bleedValue")) then {
            _bleed = [getNumber (_sectorconfig >> "bleedValue")];
        };
    };

    if (count _bleed == 1) then {
        _bleed = [(_bleed select 0), (_bleed select 0)];
    };

    if !(_bleed isEqualTo {}) then {
        _sector setVariable ["bleedValue", _bleed];
    };
}] call CFUNC(addEventhandler);

["sectorsUpdated", { ["ticketRecalc"] call CFUNC(localEvent); }] call CFUNC(addEventhandler);
["ticketRecalc", {
    /*---------------------------------------------------------------------------
        Bleed explanation:
            Each zone has a value. The value of all zones owned by a team gets combined.
            The friendly zone value gets subtracted from the total enemy zone value and the remainder is the bleed.
            The team with more value in zones does not get any bleed.
    ---------------------------------------------------------------------------*/
    private _team1Value = 0;
    private _team2Value = 0;
    {
        private _zoneOwner = _x getVariable ["side", sideUnknown];
        private _zoneBleed = _x getVariable ["bleedValue", [0,0]];
        switch (_zoneOwner) do {
            case (call MFUNC(getSides) select 0): { _team1Value = _team1Value + (_zoneBleed param [0, 0, [0]]) };
            case (call MFUNC(getSides) select 1): { _team2Value = _team2Value + (_zoneBleed param [1, 0, [0]]) };
            default { };
        };
        nil;
    } count GVAR(allSectorsArray);

    private _team0Bleed = ((_team2Value - _team1Value) max 0);
    private _team1Bleed = ((_team1Value - _team2Value) max 0);

    GVAR(bleed) = [[_team0Bleed, _team1Bleed], _BLEEDPER/60];
    publicVariable QGVAR(bleed);
}] call CFUNC(addEventHandler);

[{
    private _bleed = GVAR(bleed) select 0;
    private _team0Bleed = (_bleed param [0, 0]) * (_LOOPTIME / _BLEEDPER);
    private _team1Bleed = (_bleed param [1, 0]) * (_LOOPTIME / _BLEEDPER);
    if (_team0Bleed != 0) then { [_team0Bleed, 1, true] call EFUNC(tickets,adjustTicket); };
    if (_team1Bleed != 0) then { [_team1Bleed, 0, true] call EFUNC(tickets,adjustTicket); };
}, _LOOPTIME] call MFUNC(addPerFramehandler);
