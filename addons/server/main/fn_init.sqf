/*
    Function:       FRL_Main_fnc_init
    Author:         Adanteh
    Description:    Base init for Frontline, handles common events and so on. Always run
*/
#include "macros.hpp"

GVAR(ignoreVariables) = [toLower QSVAR(tempUnit)];

if (hasInterface) then {
    ["ratingChanged", { // -- This is toe restore your side, when you make a couple TKs, preventing you from entering friendly vehicles
        (_this select 0) params ["_newRating", "_oldRating"];
        if (_newRating < 0) then {
            player addRating 1000;
        };
    }] call CFUNC(addEventhandler);
};

// -- Prepare mission-side data
private _sides = [];
{
    private _side = [configName _x] call MFUNC(getSideFromString);
    _sides pushBack _side;
    private _factionClass = getText (missionConfigFile >> "FRL" >> "cfgFactions" >> str _side >> "factionClass");
    private _prefix = format [QSVAR(side_%1), _side];
    [_prefix, (configFile >> "FRL" >> "CfgFactions" >> _factionClass), missionNamespace] call MFUNC(cfgSettingLoadSpecial);
    [_prefix, (missionConfigFile >> "FRL" >> "cfgFactions" >> str _side), missionNamespace] call MFUNC(cfgSettingLoadSpecial);

} forEach ("true" configClasses (missionConfigFile >> "FRL" >> "cfgFactions"));
GVAR(sides) = _sides;

// -- More effecient than checking if entity is CaManBase in 50 places
["entityCreated", {
    params ["_unit"];
    if (_unit isKindOf "CaManBase") then {
        ["unitCreated", _unit] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);

["setDir", {
    (_this select 0) params ["_object", "_value"];
    _object setDir _value;
}] call CFUNC(addEventhandler);

["setVelocity", {
    (_this select 0) params ["_object", "_velocity", ["_stop", false]];
    _object setVelocity _velocity;

    [{
        params ["_object"];
        _object setVelocity [0, 0, 0];
    }, 2, [_object]] call CFUNC(wait);
}] call CFUNC(addEventHandler);

["setVectorUp", {
    (_this select 0) params ["_object", "_vectorUp"];
    _object setVectorUp _vectorUp;
}] call CFUNC(addEventHandler);
