#include "macros.hpp"

/*
 *	File: fn_isAlive.sqf
 *	Author: Adanteh
 *	Does stacked condition check for unconscious/incapacitated states
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call MFUNC(isAlive);
 */

params ["_unit"];
["isAlive", _unit, alive _unit] call MFUNC(checkConditions);
