#include "macros.hpp"

/*
 *	File: fn_isUnconscious.sqf
 *	Author: Adanteh
 *	Does stacked condition check for unconscious/incapacitated states
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call MFUNC(isUnconscious);
 */

params ["_unit"];
(["isUnconscious", _unit, false] call MFUNC(checkConditions));
