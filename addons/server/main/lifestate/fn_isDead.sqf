#include "macros.hpp"

/*
 *	File: fn_isDead.sqf
 *	Author: Adanteh
 *	Does stacked condition check for death states
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call MFUNC(isDead);
 */

params ["_unit"];
["isDead", _unit, false] call MFUNC(checkConditions);
