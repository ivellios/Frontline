/*
    Function:       FRL_Main_fnc_getSides
    Author:         Adanteh
    Description:    Gets sides in the current mission
*/
#include "macros.hpp"

missionNamespace getVariable [QMVAR(sides), []]
