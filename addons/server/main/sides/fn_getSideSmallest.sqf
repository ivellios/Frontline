/*
    Function:       FRL_Main_fnc_getSideSmallest
    Author:         Adanteh
    Description:    Gets smallest player number
*/
#include "macros.hpp"
#define __AICOUNT 0.5

params [["_includeAI", false]];
private _players = selectMin ((missionNamespace getVariable [QMVAR(sides), []]) apply { private _side = _x; ({ side group _x == _side } count allPlayers); });
if (_includeAI) then {
    _players = _players + (ceil ((missionNamespace getVariable [QSVAR(aiPerSide), 0]) * __AICOUNT));
};
_players;
