/*
    Function:       FRL_Main_fnc_getSideIndex
    Author:         Adanteh
    Description:    Gets index of side in current mission
*/
#include "macros.hpp"

params ["_side"];
if (_side isEqualType 0) exitWith { _side };
(missionNamespace getVariable [QMVAR(sides), []] find _side)
