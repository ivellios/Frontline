/*
    Function:       FRL_Main_fnc_getSideOpposite
    Author:         Adanteh
    Description:    Gets opposite side
*/
#include "macros.hpp"

params ["_side"];
private _oppositeSideID = abs (abs ([_side] call MFUNC(getSideIndex)) - 1);
(call MFUNC(getSides)) select _oppositeSideID;
