/*
    Function:       FRL_Main_fnc_getSideData
    Author:         Adanteh
    Description:    Gets variable for a given side
    Example:        ([_side, "flag"] call FRL_Main_fnc_getSideData)
*/
#include "macros.hpp"

params ["_side", "_type", ["_defaultReturn", ""]];
private _return = (missionNamespace getVariable [format [QSVAR(side_%1_%2), _side, _type], "###"]);
if (_return isEqualTo "###") then {
    _return = switch (toLower _type) do {
        case "name": { str _side };
        case "flag": { "#(argb,8,8,3)color(0.5,0.5,0.5,1)" };
        case "color": { [1, 0.5, 0, 1] };
        case "fo": { [["FRL_FO_Box1", [0,0,0], 0], ["Land_SatelliteAntenna_01_F", [0,0.22,0], 177]] };
        case "playerclass": { "" };
        case "rally": { "FRL_Backpacks_West" };
        case "squads": { "" };
        case "aisquad": { "" };
        case "unitnames": { "BritishMen"; };
        default { _defaultReturn };
    };
};

_return
