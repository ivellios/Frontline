/*
    Function:       FRL_Main_fnc_getSideFromString
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_sideString"];

private _side = switch (toLower _sideString) do {
    case "blufor";
    case "colorwest";
    case "west": { west };

    case "opfor";
    case "coloreast";
    case "east": { east };

    case "resistance";
    case "colorguer";
    case "guer": { independent };

    case "civilian";
    case "civ": { civilian };
    default { sideUnknown };
};

_side;
