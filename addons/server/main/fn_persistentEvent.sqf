#include "macros.hpp"
/*
 *	File: fn_persistentEvent.sqf
 *	Author: Adanteh
 *	Handles persistent events
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(persistentEvent);
 */

params [["_eventName", "", [""]], ["_args", []], ["_sender", "Local Called"]];

if (isNil QGVAR(persistentEvents)) then {
	GVAR(persistentEvents) = [];
	["eventAdded", {
	    params ["_arguments", "_data"];
	    _arguments params ["_event", "_function", "_args"];
	    if (toLower _event in GVAR(persistentEvents)) then {
	        if (_function isEqualType "") then {
	            _function = parsingNamespace getVariable [_function, {}];
	        };
	        [nil, _args] call _function;
	    };
	}] call CFUNC(addEventHandler);
};

if ((GVAR(persistentEvents) pushBackUnique toLower _eventName) == -1) then {
	// -- Already executed this event. Probably don't want a second call
};

_this call CFUNC(localEvent);
