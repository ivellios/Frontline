/*
    Function:       FRL_Main_fnc_useAdminOption
    Author:         Adanteh
    Description:    Admin option usage
    Example:        ["kick", [], player] call frl_main_fnc_useAdminOption;
*/
#include "macros.hpp"

params ["_mode", ["_args", []], ["_target", objNull], ["_reason", ""]];
if (isNull _target) then { _target = GVAR(playerSelected) };
switch (toLower _mode) do {
    case "message": {
        [QGVAR(adminPopup), _target, [_args, Clib_Player, 5]] call CFUNC(targetEvent);
    };
    case "kick": {
        ["adminOption", [_mode, _target, Clib_Player, _reason]] call CFUNC(serverEvent);
    };
    case "ban": {
        ["adminOption", [_mode, _target, Clib_Player, _reason]] call CFUNC(serverEvent);
    };
};
