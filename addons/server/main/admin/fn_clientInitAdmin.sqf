/*
    Function:       FRL_Main_fnc_clientInitAdmihn
    Author:         Adanteh
    Description:    Adds some common admin options, like sending a message, kicking banning and so on
*/
#include "macros.hpp"

['add', ["admin", 'Admin Settings', 'Admin', [
    'Admin Message',
    'Shows a big admin message on all clients',
    QGVAR(CtrlTextBoxButton),
    "",
    { [Clib_Player] call FUNC(isAdmin); },
    (format ["['%1', [_value, Clib_Player]] call %2; false;", QGVAR(adminPopup), QCFUNC(globalEvent)]),
    "SEND"
]]] call FUNC(settingsWindow);

[QGVAR(adminPopup), {
    (_this select 0) params ["_message", "_caller", ["_length", 10]];
    private _messageStyle = "BLACK";
    private _messageStyle = ["BLACK", "PLAIN"] select (Clib_Player isEqualTo _caller); // -- No background for admin, so he can still do stuff
    private _fullMessage = format ["ADMIN MESSAGE\n%1", _message];
    titleText [_fullMessage, _messageStyle, 2];
    [{
        titleText ["", "PLAIN"];
    }, _length] call CFUNC(wait);
}] call CFUNC(addEventHandler);

// -----------------------
// Player specific options
// -----------------------
['add', ["admin", 'Admin Settings', 'Admin', [
    'Select Player',
    'Select a player for the options below',
    QGVAR(CtrlPlayerSelector),
    "",
    { true },
    { true },
    ""
]]] call FUNC(settingsWindow);

['add', ["admin", 'Admin Settings', 'Admin', [
    'Message',
    'Shows a big admin message to the selected player',
    QGVAR(CtrlTextBoxButton),
    "",
    { [Clib_Player] call FUNC(isAdmin); },
    { ["message", _value] call FUNC(useAdminOption) },
    "SEND"
]]] call FUNC(settingsWindow);

['add', ["admin", 'Admin Settings', 'Admin', [
    'Kick',
    'Kicks the selected player',
    QGVAR(CtrlButton),
    true,
    { [Clib_Player] call FUNC(isAdmin); },
    { ["kick"] call FUNC(useAdminOption) },
    ["KICK"]
]]] call FUNC(settingsWindow);

['add', ["admin", 'Admin Settings', 'Admin', [
    'Ban',
    'Bans the selected player',
    QGVAR(CtrlButton),
    true,
    { [Clib_Player] call FUNC(isAdmin); },
    { ["ban"] call FUNC(useAdminOption) },
    ["BAN"]
]]] call FUNC(settingsWindow);

// -- Add an admin option to properly end the mission as amin
['add', ["admin", 'Admin Settings', 'Misc', [
    'End Mission',
    'Ends the mission with regular victory check',
    QMVAR(CtrlButton),
    true,
    { [Clib_Player] call FUNC(isAdmin); },
    (format ["['%1', [Clib_Player]] call %2; false;", QSVAR(endMissionAdmin), QCFUNC(globalEvent)]),
    ["END THE MISSION"]
]]] call FUNC(settingsWindow);
