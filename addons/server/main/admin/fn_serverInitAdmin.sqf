/*
    Function:       FRL_Main_fnc_serverInitAdmin
    Author:         Adanteh
    Description:    Inits admin functions on server (Keep these in here, we don't want them defined on client)
*/
#include "macros.hpp"

// -- This already shouldn't run on non-server, but lets put it here just anyway
if !(isServer) exitWith { };

// -- Retrieve the password from external config
DFUNC(adminGetPassword) = {
    private _password = ["admin.password", "invalidpassword"] call FUNC(getExternalConfig);
    _password;
};

// -- Run command on server
DFUNC(adminCommand) = {
    params ["_command", "_args", ["_caller", "Server", ["", objNull]]];
    if (_caller isEqualType objNull) then {
        _caller = _caller call CFUNC(name);
    };
    (format ["<ADMIN> Server command '%1' executed by '%2' at %3", _command, _caller, serverTime]) call FUNC(diagLog);
    private _succeeded = false;
    if (isDedicated) then {
        _succeeded = (call FUNC(adminGetPassword)) serverCommand (format ["%1 %2", _command, _args]);
    } else {
        _succeeded = serverCommand (format ["%1 %2", _command, _args]);
    };
    if (!_succeeded) then {
        (format ["<ADMIN> Server command '%1' failed. Wrong password!", _command]) call FUNC(diagLog);
    };
};


// -- Change the mission (But only if it exists)]\
// Example: ["DFL_Chilly_War.frl_wandashan"] call frl_main_fnc_adminChangeMission
DFUNC(adminChangeMission) = {
    params ["_missionName", ["_caller", "Server", ["", objNull]]];
    private _missionExists = [_missionName] call { true };
    if (_missionExists) then {
        ["#mission", _missionName, _caller] call FUNC(adminCommand);
    };
};


// -- Kick someone, based on name, UID or player object
// Example: ["76561198004602049", "dundy", "AFK Kick"] call frl_main_fnc_adminKick;
DFUNC(adminKick) = {
    params [["_target", "", ["", 0, objNull]], ["_caller", "Server", ["", objNull]], ["_reason", ""]];
    if (_target isEqualType objNull) then {
        _target = getPlayeruID _target;
    };
    if (_reason != "") then {
        (format ["<ADMIN> Player '%1' kicked by '%2'. Reason: %3", _target, _caller, _reason]) call FUNC(diagLog);
    };
    ["#exec kick", str _target, _caller] call FUNC(adminCommand);
};


// -- Ban someone, based on name UID or player object
DFUNC(adminBan) = {
    params [["_target", "", ["", 0, objNull]], ["_caller", "Server", ["", objNull]], ["_reason", ""]];
    if (_target isEqualType objNull) then {
        _target = getPlayeruID _objNull;
    };
    if (_reason != "") then {
        (format ["<ADMIN> Player '%1' banned by '%2'. Reason: %3", _target, _caller, _reason]) call FUNC(diagLog);
    };
    ["#exec ban", str _target, _caller] call FUNC(adminCommand);
};

["adminOption", {
    (_this select 0) params ["_mode", "_args", "_caller", ["_reason", ""]];
    switch (toLower _mode) do {
        case "kick": {
            [_args, _caller, _reason] call FUNC(adminKick);
        };
        case "ban": {
            [_args, _caller, _reason] call FUNC(adminBan);
        };
        default {
            [_mode, _args, _caller] call FUNC(adminCommand);
        };
    };
}] call CFUNC(addEventHandler);
