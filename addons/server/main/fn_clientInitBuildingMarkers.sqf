/*
    Function:       FRL_Main_fnc_clientInitBuildingMarkers
    Author:         Adanteh
    Description:    Adds grey map markers indicating mission-placed buildlings
*/

#include "macros.hpp"

["missionStarted", {
    // -- This is some slow-ass code, so spawn it
    [] spawn {
        private _allObjects = (
            (allMissionObjects "House") +
            (allMissionObjects "HouseBase") +
            (allMissionObjects "Ruins") +
            (allMissionObjects "HBarrier_base_F")
        );

        {
            [_x] call MFUNC(createObjectMarker);
            nil;
        } count (_allObjects arrayIntersect _allObjects);
    };
}] call CFUNC(addEventHandler);
