class init;


class initLocalModules;
class clientInitCoreEvents;
class clientInitBuildingMarkers;
class clientInitDisableConversations;
class persistentEvent;
class persistentEventGlobal;
class serverInitBase;
class toggleSpectatorMode;

MODULE(ADMIN) {
	class isAdmin;
	class clientInitAdmin;
	class serverInitAdmin;
	class useAdminOption;
};

MODULE(AAW) {
    class respawn;
};

MODULE(Group) {
    class initGroups;
    class isLeader;
    class leaveSquad;
    class selectLeader;
};

MODULE(CONFIGS) {
	class getExternalPythiaConfig;
	class getExternalConfig;
	class serverInitJson;
};

MODULE(sides) {
    class getSideData;
    class getSideFromString;
    class getSideIndex;
    class getSideLargest;
    class getSideOpposite;
    class getSides;
    class getSideSmallest;
};

MODULE(lifeState) {
	class isAlive;
	class isDead;
	class isUnconscious;
};

MODULE(sector) {
    class clientInitSectors;
    class sectorCreate;
    class sectorInBase;
};

MODULE(virtualItem) {
	class clientInitItems;
	class createItem;
	class addVirtualItems;
	class restoreHandgunItem;
	class switchHandgunItem;
};

MODULE(ui) {
	class showMapHUD;
	class addMapHUD;
	class setPPBlur;
	class clientInitProgressHUD;
	class clientInitControlHUD;
};

MODULE(interact) {
	class addInteractType;
	class removeInteractType;
	class interactionDetect;
};
