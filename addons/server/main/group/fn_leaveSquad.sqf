/*
    Function:       FRL_Main_fnc_leaveSquad
    Author:         Adanteh
    Description:    Wrapper to leave squad
*/
#include "macros.hpp"

params [["_unit", Clib_Player]];
[_unit] join grpNull;
