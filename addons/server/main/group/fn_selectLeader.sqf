/*
    Function:       FRL_Main_fnc_selectLeader
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

[{
    params ["_unit"];

    if (CLib_Player != leader _unit) exitWith {};

    ["selectLeader", [group CLib_Player, _unit]] call CFUNC(serverEvent);
}, _this, "respawn"] call CFUNC(mutex);
