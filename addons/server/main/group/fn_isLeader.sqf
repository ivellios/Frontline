/*
    Function:       FRL_Main_fnc_isLeader
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_unit"];
leader _unit isEqualTo _unit;
