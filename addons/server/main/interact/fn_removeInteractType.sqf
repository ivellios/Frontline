#include "macros.hpp"
/*
 *	File: fn_removeInteractType.sqf
 *	Author: Adanteh
 *	Adds an object or class type that will be counted towards interaction detection
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(removeInteractType);
 */

params ["_type"];

if (_type isEqualType "") then {
	GVAR(interactionTypes) deleteAt (GVAR(interactionTypes) find (toLower _type));
} else {
	GVAR(interactionObjects) deleteAt (GVAR(interactionTypes) find (toLower _type));
};

if (GVAR(interactHandle) != -1) then {
	if ((GVAR(interactionTypes) isEqualTo []) && {GVAR(interactionObjects) isEqualTo []}) then {
		[GVAR(interactHandle)] call MFUNC(removePerFrameHandler);
		GVAR(interactTarget) = objNull;
        GVAR(interactHandle) = -1;
	};
};
