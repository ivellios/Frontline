#include "macros.hpp"
/*
 *	File: fn_addInteractType.sqf
 *	Author: Adanteh
 *	Adds an object or class type that will be counted towards interaction detection
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(addInteractType);
 */

params ["_type"];

if (isNil QGVAR(interactionTypes)) then {
	GVAR(interactionTypes) = [];
	GVAR(interactionObjects) = [];
	GVAR(interactHandle) = -1;
	GVAR(interactTarget) = objNull;
};

if (GVAR(interactHandle) == -1) then {
	GVAR(interactHandle) = [{ _this call FUNC(interactionDetect) }] call MFUNC(addPerFrameHandler);
};

if (_type isEqualType "") then {
	GVAR(interactionTypes) pushBackUnique (toLower _type);
} else {
	GVAR(interactionObjects) pushBackUnique _type;
};
