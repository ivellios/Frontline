#include "macros.hpp"
/*
 *	File: fn_addInteractType.sqf
 *	Author: Adanteh
 *	Detects nearby objects from list. This is started once
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(addInteractType);
 */

#define __CACHE_TIME 2
#define __RANGE_DETECT 10
#define __MARGIN 1.8
#define __RANGE_INTERACT 2.5

if !(alive CLib_Player) exitWith { GVAR(interactTarget) = objNull; };
if (isNull (findDisplay 46)) exitWith { GVAR(interactTarget) = objNull; };

// -- Considering using 'nearSupplies'
private _withinViewPort = [];
private _unitPos = getPosATLVisual CLib_Player;
{
	private _bodyPos = (_x modelToWorldVisual (_x selectionPosition "spine2"));
	private _screenPos = worldToScreen (_x modelToWorldVisual (_x selectionPosition "spine2"));
	if (count _screenPos > 0) then {
		private _realDistance = (getPosATLVisual _x distance _unitPos) max 0.4;
		if (_realDistance <= __RANGE_INTERACT) then {
			if ((_screenPos distance2D [0.5, 0.5]) <= (__MARGIN / _realDistance)) then {
				_withinViewPort pushBack [_realDistance, _x];
			};
		};
	};

	nil;
} count ([QGVAR(interactTargetCache), {
	private _nearestObjects = (nearestObjects [(positionCameraToWorld [0, 0, 0.5]), GVAR(interactionTypes), __RANGE_DETECT]) - [player];
	_nearestObjects
}, [], __CACHE_TIME] call CFUNC(cachedCall));
_withinViewPort sort true;
private _newTarget = (_withinViewPort param [0, [-1, objNull]]) select 1;
if !(_newTarget isEqualTo GVAR(interactTarget)) then {
	private _oldTarget = GVAR(interactTarget);
	GVAR(interactTarget) = _newTarget;
	["interactTargetChanged", [_newTarget, _newTarget]] call CFUNC(localEvent);
};
