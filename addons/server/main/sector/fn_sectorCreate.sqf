/*
    Function:       FRL_Main_fnc_sectorCreate
    Author:         Adanteh
    Description:    Creates a sector
*/
#include "macros.hpp"

params ["_serverInit", "_args"];
if (isServer && _serverInit) then {
    _args params ["_marker", "_cfg", "_designator"];

    private _logic = (call CFUNC(getLogicGroup)) createUnit ["Logic", getMarkerPos _marker, [], 0, "NONE"];
    _logic setVehicleVarName _marker;

    if (isNil QGVAR(allSectors)) then {
        GVAR(allSectors) = true call CFUNC(createnamespace);
        publicVariable QGVAR(allSectors);
    };
    GVAR(allSectors) setVariable [_marker, _logic, true];

    private _side = switch (markerColor _marker) do {
        case "ColorWEST": {west};
        case "ColorEAST": {east};
        case "ColorGUER": {independent};
        default {sideUnknown};
    };

    _logic setVariable ["fullName", markerText _marker, true];
    _logic setVariable ["designator", _designator, true];
    _logic setVariable ["marker", _marker, true];
    _logic setVariable ["side", _side, true];

    _marker setMarkerColor format["Color%1", [_side, "Black"] select (_side isEqualTo sideUnknown)];
    _marker setMarkerBrush "SolidBorder";
    _marker setMarkerAlpha 0.8;

    ["createSector", [_cfg, _logic]] call CFUNC(localEvent);
};

if (hasInterface && !_serverInit) then {
    _args params ["_sector"];
    private _marker = _sector getVariable ["marker", ""];
    private _fullname = _sector getVariable ["fullName", ""];
    private _designator = _sector getVariable ["designator", ""];
    private _id = format [QGVAR(ID_%1), _marker];

    [_id, ([["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], getMarkerPos _marker, 25, 25, 0, _designator, 2]])] call CFUNC(addMapGraphicsGroup);
    [_id, ([["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1,1,1,1], getMarkerPos _marker, 25, 25, 0, _fullname, 2]]), "hover"] call CFUNC(addMapGraphicsGroup);
};
