/*
    Function:       FRL_Main_fnc_sectorInBase
    Author:         Adanteh
    Description:    Checks if player is in a base sector, also works for positions (2D)
*/
#include "macros.hpp"

params [["_object", Clib_Player]];
private _inBase = false;
[missionNamespace getVariable [QGVAR(allSectors), objNull], {
    if (_value getVariable ["isBase", false]) then {
        if (_object inArea _key) then {
            _inBase = true;
        };
    };
}] call MFUNC(forEachVariable);

_inBase;
