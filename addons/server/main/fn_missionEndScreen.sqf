/*
    Function:       FRL_Main_fnc_missionEndScreen
    Author:         Adanteh
    Description:    Ends the mission
*/
#include "macros.hpp"
#define CTRL(var1) ((uiNamespace getVariable [QGVAR(endMissionScreen), displayNull]) displayCtrl var1)

params [["_mainText", ""], ["_subtitle", ""], ["_music", ""]];

CTRL(1) ctrlSetText _mainText;
if (_music != "") then {
    playMusic [_music, 0];
};

// -- Main Title
if (_mainText != "") then {
    CTRL(1) ctrlsettext toupper _mainText;
} else {
    CTRL(1) ctrlsetfade 1;
    CTRL(1) ctrlcommit 0;
};
//--- Subtitle
if (_subtitle != "") then {
    CTRL(2) ctrlsettext toupper _subtitle;
} else {
    CTRL(2) ctrlsetfade 1;
    CTRL(2) ctrlcommit 0;
};

QGVAR(endNoise) cutrsc ["RscNoise","black"];
QGVAR(endStatic) cutrsc ["RscStatic","plain"];

[{

}, 2] call CFUNC(wait);
