#include "macros.hpp"
/*
 *	File: fn_serverInitBase.sqf
 *	Author: Adanteh
 *	Sets some core events or variables
 *
 */

["missionStarted", {
	GVAR(missionStartTime) = serverTime;
	publicVariable QGVAR(missionStartTime);
}] call CFUNC(addEventHandler);