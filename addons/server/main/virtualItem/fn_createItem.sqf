 /*---------------------------------------------------------------------------
     This is a modified version of AAW_Revive_fnc_clientInitTreatments, July 2nd 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"
/*
    Function:       FRL_Main_fnc_createItem
    Author:         Adanteh
    Description:    Switches to a fake weapon item, created with simpleObject
*/
params ["_unit", "_itemCfg"];

// Create a simple object
VAR(_modelName) = getText (_itemCfg >> "model");
VAR(_attachPoint) = getText (_itemCfg >> "attachPoint");
VAR(_attachOffset) = getArray (_itemCfg >> "attachOffset");
VAR(_vectorDir) = getArray (_itemCfg >> "vectorDir");

if (_attachOffset isEqualTo []) then { _attachOffset = [0, 0, 0]; };

// -- Remove leading slash if present -- //
if ((_modelName select [0, 1]) == "\") then {
    _modelName = _modelName select [1, count _modelName - 1];
};

// -- Add .p3d extension if not present -- //
if ((_modelName select [count _modelName - 4]) != ".p3d") then {
    _modelName = _modelName + ".p3d";
};

VAR(_fakeItem) = createSimpleObject [_modelName, [0, 0, 0]];
_fakeItem attachTo [_unit, _attachOffset, _attachPoint];
if !(_vectorDir isEqualTo []) then {
    ["setVectorDirAndUp", [_fakeItem, _vectorDir]] call CFUNC(globalEvent);
};

_fakeItem;
