/*
    Function:       FRL_Main_fnc_addVirtualItem
    Author:         Adanteh
    Description:    Adds a single virtual item addActions, combining action conditions and code
*/
#include "macros.hpp"

private _items = "(isClass _x) && {getNumber (_x >> 'scope') >= 2}" configClasses (configFile >> "FRL" >> "VirtualItems");
private _fnc_addCode = {
	if (_code != "") then {
		_code = _code + " && ";
	};
	_code = _code + _this;
	_code
};

{
	private _itemCfg = _x;
	private _itemClass = configName _x;
	private _code = "";

	if !(getNumber (_itemCfg >> "allowInVehicle") > 0) then {
		_code = "(CLib_Player == vehicle CLib_Player)" call _fnc_addCode;
	};

	if (isArray (_itemCfg >> "itemRequired")) then {
		private _itemRequired = (getArray (_itemCfg >> "itemRequired")) apply { toLower _x };
		if !(_itemRequired isEqualTo []) then {
			_code = (format ["(({ toLower _x in %1 } count (items CLib_Player)) > 0)", _itemRequired]) call _fnc_addCode;
		};
	} else {
		if (isText (_itemCfg >> "itemRequired")) then {
			private _itemRequired = getText (_itemCfg >> "itemRequired");
			if (_itemRequired != "") then {
				_code = (format ["('%1' in (items CLib_Player))", _itemRequired]) call _fnc_addCode;
			};
		};
	};

	_code = (format ["{(CLib_Player getVariable ['%1', '']) != '%2'}", QMVAR(fakeWeaponName), _itemClass]) call _fnc_addCode;

	[
		(getText (_itemCfg >> "actionName")), CLib_Player, 0,
		compile _code,
		compile format ["['%1', '%2'] call %3", QMVAR(SwitchWeapon), _itemClass, QCFUNC(localEvent)],
		["showWindow", false]
	] call CFUNC(addAction);

	nil;
} count _items;
