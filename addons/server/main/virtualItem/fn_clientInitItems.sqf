 /*---------------------------------------------------------------------------
     This is a modified version of AAW_Revive_fnc_clientInitTreatments, July 2nd 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"

// -- Temporary weapon changed bypass while fake animation is running -- //
GVAR(fakeWeaponBypass) = false;
GVAR(fakeWeaponReady) = false;

// To restore default behaviour if the weapon is changed use currentWeaponChanged EH.
["currentWeaponChanged", {
    (_this select 0) params ["_currentWeapon", "_oldWeapon"];

    if (_currentWeapon != "FRL_FakeHandgunWeapon") then {
        if !GVAR(fakeWeaponBypass) then {
            [QGVAR(RestoreWeapon)] call CFUNC(localEvent);
        } else {
            GVAR(fakeWeaponBypass) = false;
        };
    };
}] call CFUNC(addEventHandler);

// Reset values on death
["Killed", {
    (_this select 0) params ["_unit"];
    _unit setVariable [QGVAR(handgunSaved), nil];
    _unit setVariable [QGVAR(fakeHandgunWeapon), nil];
    _unit setVariable [QGVAR(fakeWeapon), nil];
    _unit setVariable [QGVAR(fakeWeaponName), nil];
    _unit setVariable [QGVAR(restoreWeaponAction), nil];
}] call CFUNC(addEventHandler);

[QGVAR(RestoreWeapon), {
    if (CLib_Player getVariable [QGVAR(fakeWeaponName), ""] != "") then {
        CLib_Player setVariable [QGVAR(fakeWeaponName), ""];

        // Get the weapon holder and delete it.
        private _fakeWeapon = CLib_Player getVariable [QGVAR(fakeWeapon), objNull];
        deleteVehicle _fakeWeapon;

        // Remove the exit action if it exists.
        private _restoreWeaponActions = CLib_Player getVariable [QGVAR(restoreWeaponAction), []];
        {
            if (_x > -1) then {
                CLib_Player removeAction _x;
            };
            nil;
        } count _restoreWeaponActions;
        [QGVAR(SwitchWeapon), ""] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);

[QGVAR(RestoreWeapon), { [false] call FUNC(restoreHandgunItem) }] call CFUNC(addEventHandler);
[QGVAR(SwitchWeapon), FUNC(switchHandgunItem)] call CFUNC(addEventHandler);

["missionStarted", { // -- Allow pressing 2 to switch back to pistol
    (findDisplay 46) displayAddEventHandler ["KeyDown", {
        params ["", "_button"];

        private _return = false;
        if (_button == 0x03) then {
            if (CLib_Player getVariable [QGVAR(fakeWeaponName), ""] != "") then {
                if (alive Clib_Player && (lifeState Clib_Player != "INCAPACITATED")) then {
                    if !((Clib_Player getVariable [QGVAR(handGunSaved), []]) isEqualTo []) then {
                        [true] call FUNC(restoreHandgunItem);
                        _return = true;
                    };
                };
            };
        };
        _return
    }];
}] call CFUNC(addEventHandler);

call FUNC(addVirtualItems);
