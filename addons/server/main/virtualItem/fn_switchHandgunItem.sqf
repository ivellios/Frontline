 /*---------------------------------------------------------------------------
     This is a modified version of AAW_Revive_fnc_clientInitTreatments, July 2nd 2016 version
     Released under APL license by the AAW Team.

     Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
     Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
 ---------------------------------------------------------------------------*/

#include "macros.hpp"
/*
 *	File: fn_switchHandgunItem.sqf
 *	Author: Adanteh
 *	Does virtual weapon switching
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(switchHandgunItem);
 */


params ["_item"];

if (_item == "") exitWith { };

private _itemCfg = (configFile >> "FRL" >> "VirtualItems" >> _item);
private _itemRequiredCondition = { true };
if (isArray (_itemCfg >> "itemRequired")) then {
    private _itemRequired = (getArray (_itemCfg >> "itemRequired")) apply { toLower _x };
    if !(_itemRequired isEqualTo []) then {
        _itemRequiredCondition = compile (format ["(({ toLower _x in %1 } count (items CLib_Player)) > 0)", _itemRequired]);
    };
} else {
    if (isText (_itemCfg >> "itemRequired")) then {
        private _itemRequired = getText (_itemCfg >> "itemRequired");
        if (_itemRequired != "") then {
            _itemRequiredCondition = compile (format ["('%1' in (items CLib_Player))", _itemRequired]);
        };
    };
};


if (handgunWeapon CLib_Player != "FRL_FakeHandgunWeapon") then {
    [QGVAR(RestoreWeapon)] call CFUNC(localEvent);
} else {
    // -- Remove existing attached item -- //
    private _fakeWeapon = CLib_Player getVariable [QGVAR(fakeWeapon), objNull];
    deleteVehicle _fakeWeapon;
};

// Move the weapon on back
// -- Save current pistol
private _handgun = handgunWeapon CLib_Player;
private _handgunAnim = false;
private _switchFakeWeapon = false;
private _actionConfig = configFile >> "CfgActions" >> "SwitchWeapon";

if (_handgun != "") then {
    if (_handgun != "FRL_FakeHandgunWeapon") then {
        private _bullets = CLib_Player ammo _handgun;
        private _mag = handgunMagazine CLib_Player;
        private _items = handgunItems CLib_Player;
        CLib_Player setVariable [QGVAR(handgunSaved), [_handgun, _mag, _items, _bullets]];
    };

    if (currentWeapon CLib_Player == _handgun) then {
        _handgunAnim = true;

        // -- To properly animate switching between virtual items we need to add/remove in a different order -- //
        if (_handgun == "FRL_FakeHandgunWeapon") then {
            //_switchFakeWeapon = true;
        };
    };

    CLib_Player removeWeapon _handgun;
};

if (_handgun != "FRL_FakeHandgunWeapon") then {
    if (getNumber (_actionConfig >> "show") == 1) then {
        // Add the action and store the id to remove it on grenade mode exit.
        private _priority = getNumber (_actionConfig >> "priority");
        private _showWindow = getNumber (_actionConfig >> "showWindow") == 1;
        private _hideOnUse = getNumber (_actionConfig >> "hideOnUse") == 1;
        private _shortCut = getText (_actionConfig >> "shortcut");

        _restoreWeaponID = [];
        if (_handgun != "") then {
           _restoreWeaponID pushBack (CLib_Player addAction [format [getText (_actionConfig >> "text"), getText (configFile >> "CfgWeapons" >> _handGun >> "displayName")], { [true] call FUNC(restoreHandgunItem) }, nil, _priority, _showWindow, _hideOnUse, _shortCut])
        };

        private _primary = primaryWeapon CLib_Player;
        if (_primary != "") then {
           _restoreWeaponID pushBack (CLib_Player addAction [format [getText (_actionConfig >> "text"), getText (configFile >> "CfgWeapons" >> _primary >> "displayName")], { [false] call FUNC(restoreHandgunItem) }, nil, _priority, _showWindow, _hideOnUse, _shortCut])
        };

        CLib_Player setVariable [QGVAR(restoreWeaponAction), _restoreWeaponID];
    };
};

// -- Add action menu items to switch back. You can switch with number keys as well, but it makes sense that if you switch with scroll menu to an item, you can also switch back with it -- //
if (_handgunAnim) then {
    CLib_Player setVariable [QGVAR(fakeWeaponPreType), "handgun"];
    GVAR(fakeWeaponBypass) = true;
    [{
        CLib_Player addWeapon "FRL_FakeHandgunWeapon";
        CLib_Player selectWeapon "FRL_FakeHandgunWeapon";
    }, 0.3] call CFUNC(wait);
} else {
    CLib_Player setVariable [QGVAR(fakeWeaponPreType), ""];
    CLib_Player addWeapon "FRL_FakeHandgunWeapon";
    CLib_Player selectWeapon "FRL_FakeHandgunWeapon";
};

// -- Add a small delay so we can't use actions of thiss items till properly equiiped yet -- //
GVAR(fakeWeaponReady) = false;
[{
    GVAR(fakeWeaponReady) = true;
}, 1.25] call CFUNC(wait);

private _fakeWeapon = [CLib_Player, _itemCfg] call FUNC(createItem);
CLib_Player setVariable [QGVAR(fakeWeapon), _fakeWeapon];

// Store the weapon holder to remove it on restoring real weapon.
CLib_Player setVariable [QGVAR(fakeHandgunWeapon), "FRL_FakeHandgunWeapon"];
CLib_Player setVariable [QGVAR(fakeWeaponName), _item];

// If player lose the item by scripts
[{
    params ["_itemRequiredCondition", "_id"];

    if (CLib_Player getVariable [QGVAR(fakeWeaponName), ""] == "") exitWith {
        _id call MFUNC(removePerFrameHandler);
    };
    if (!(call _itemRequiredCondition)) then {
        private _pistolSwitch = CLib_Player getVariable [QGVAR(fakeWeaponPreType), ""] == "handgun";
        [_pistolSwitch] call FUNC(restoreHandgunItem);
    };
}, 0, _itemRequiredCondition] call MFUNC(addPerFramehandler);
