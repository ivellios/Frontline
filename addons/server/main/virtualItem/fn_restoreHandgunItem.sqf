#include "macros.hpp"
/*
 *	File: fn_restoreHandgunItem.sqf
 *	Author: Adanteh
 *	Restores back to the actual pistol item
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(restoreHandgunItem);
 */


params [["_selectHandgun", false]];

if (handgunWeapon CLib_Player == (CLib_Player getVariable [QGVAR(fakeHandgunWeapon), "FRL_FakeHandgunWeapon"])) then {
    CLib_Player removeWeapon (handgunWeapon CLib_Player);
};

VAR(_handgun) = CLib_Player getVariable [QGVAR(handgunSaved), []];
if (count _handgun > 0) then {
    CLib_Player setVariable [QGVAR(handgunSaved), nil];

	if (count (_handgun select 1) > 0) then {
	    CLib_Player addMagazine [(_handgun select 1) select 0, (_handgun select 3)];
	};


    if (_selectHandgun) then {
        [{
        	params ["_handgun"];
		    CLib_Player addWeapon (_handgun select 0);
		    { if (_x != "") then { CLib_Player addHandgunItem _x; }; nil; } count (_handgun select 2);
        	CLib_Player selectWeapon (handgunWeapon CLib_Player);
        }, 0.3, [_handgun]] call CFUNC(wait);
    } else {
  		CLib_Player addWeapon (_handgun select 0);
		{ if (_x != "") then { CLib_Player addHandgunItem _x; }; nil; } count (_handgun select 2);
    };
};