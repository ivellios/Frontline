#include "macros.hpp"
/*
 *    File: fn_clientInitCoreEvents.sqf
 *    Author: Adanteh
 *    Adds core events, detected by a frame handler
 *
 *    0: The unit <OBJECT>
 *
 *    Returns:
 *    true <BOOL>
 *
 *    Example:
 *    [player] call FUNC(clientInitCoreEvents);
 */

GVAR(menuIsOpened) = false;
GVAR(mapZeusOpened) = false;
GVAR(mapArtyOpened) = false;
GVAR(playerRating) = 0;
GVAR(isAdmin) = false;

[{
    private ["_data", "_map"];
    // -- Esc Menu opened -- //
    _data = !(isNull (findDisplay 49));
    if !(_data isEqualTo GVAR(menuIsOpened)) then {
        GVAR(menuIsOpened) = _data;
        ["escMenu", [(["onunload", "onload"] select _data), [(findDisplay 49)]]] call CFUNC(localEvent);
    };

	// -- Zeus map opened -- //
    private _map = (findDisplay 312) displayCtrl 50;
	_data = !(isNull _map);
	if !(_data isEqualTo GVAR(mapZeusOpened)) then {
		GVAR(mapZeusOpened) = _data;
		["mapZeusToggled", [(["onunload", "onload"] select _data)]] call CFUNC(localEvent);
        if (_data) then {
            [_map] call CFUNC(registerMapControl);
            ["registerMapClick", [_map]] call CFUNC(localEvent);
        };
	};

    // -- Artillery computer opened
    _data = shownArtilleryComputer;
    if !(_data isEqualTo GVAR(mapArtyOpened)) then {
        GVAR(mapArtyOpened) = _data;
        ["mapArtilleryToggled", [(["onunload", "onload"] select _data)]] call CFUNC(localEvent);
        if (_data) then {
            private _artyDisplay = [allDisplays - [findDisplay 12]] call MFUNC(arrayPeek);
            [_artyDisplay displayCtrl 500] call CFUNC(registerMapControl);
            ["registerMapClick", [_artyDisplay displayCtrl 500]] call CFUNC(localEvent);
        };
    };

    // -- Rating changed
    _data = rating player;
    if !(_data isEqualTo GVAR(playerRating)) then {
        ["ratingChanged", [_data, GVAR(playerRating)]] call CFUNC(localEvent);
        GVAR(playerRating) = _data;
    };

    // -- Entered / left admin
    _data = serverCommandAvailable "#kick";
    if !(_data isEqualTo GVAR(isAdmin)) then {
        ["adminChanged", [_data, GVAR(isAdmin)]] call CFUNC(localEvent);
        GVAR(isAdmin) = _data;
    };
}, 0, []] call MFUNC(addPerFramehandler);

["say3D", {
    (_this select 0) params ["_args", "_sound"];
    _args say3D _sound;
}] call CFUNC(addEventHandler);
