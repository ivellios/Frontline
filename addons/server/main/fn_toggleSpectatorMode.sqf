/*
    Function:       FRL_Main_fnc_toggleSpectatorMode
    Author:         Adanteh
    Description:    Turns off most UI things
    Example:        `[true, "nameofperson"] call FRL_Main_fnc_toggleSpectatorMode`
                    "nameofperson" is only if you do a global exec and want to give a specific person spectator mode.
                    If just running locally for yourself remove the string
*/
#include "macros.hpp"

params ["_mode", ["_name", ""]];

// -- ezpz global exec with name filter
if ((_name != "") && {(profileName != _name)}) exitWith { };

if (isNil "_mode") then {
    _mode = isNil { missionNamespace getVariable QGVAR(spectatorStore) };
};

if (_mode) then {
    GVAR(spectatorStore) = [EGVAR(nametags,maxRange), GVAR(dbgLogLevel)];

    ["debugMessage", ["Hiding hud in 10, close esc menu before that"]] call CFUNC(localEvent);
    ["showNotification", ["Hiding hud in 10, close esc menu before that"]] call CFUNC(localEvent);
    [{
        ["fadeHUD", [1, 0]] call CFUNC(localEvent);
    }, 5] call CFUNC(wait);

    EGVAR(nametags,maxRange) = 0;
    GVAR(dbgLogLevel) = -5;
    GVAR(notificationDisabled) = true;

} else {

    private _store = missionNamespace getVariable [QGVAR(spectatorStore), []];
    if (_store isEqualTo []) exitWith { };

    EGVAR(nametags,maxRange) = _store param [0, 100];
    GVAR(dbgLogLevel) = _store param [1, 25];
    GVAR(notificationDisabled) = false;
    GVAR(spectatorStore) = nil;
};
