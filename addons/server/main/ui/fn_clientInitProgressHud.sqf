/*
    Function:       FRL_Main_fnc_clientInitProgressHUD
    Author:         Adanteh
    Description:    Function that supports showing of multiple progress bar in the top left, with animations and repeated usage
    Example:        [cursorTarget, false] call FRL_Main_fnc_clientInitProgressHUD;
*/
#include "macros.hpp"
#define __GROUPIDC 100
#define __DISPLAY uiNamespace getVariable QSVAR(progressHUD)



GVAR(progressNamespace) = call CFUNC(createNamespace);
GVAR(currentProgressHuds) = [];
GVAR(forcedHuds) = [];
GVAR(progressIdcIndex) = 0;

// -- Fade out if HUD is shown
["fadeHud", {
	(_this select 0) params ["_fade", "_commit"];
	if (isNull (uiNamespace getVariable [QSVAR(progressHUD), displayNull])) exitWith { };
	((uiNamespace getVariable QSVAR(progressHUD)) displayCtrl 1) ctrlSetFade _fade;
	((uiNamespace getVariable QSVAR(progressHUD)) displayCtrl 1) ctrlCommit _commit;
}] call CFUNC(addEventHandler);

["progressHudShow", {
	// ["progressHudShow", ["Oogabooga", {(_controlsGroup controlsGroupCtrl 3) ctrlSetText "Doing stuff"}, { (_controlsGroup controlsGroupCtrl 5) progressSetPosition (random 1); }, []]] call Clib_fnc_localEvent;
	(_this select 0) params [["_hudID", "-1", [""]], ["_codeInit", {}, [{}]], ["_codeLoop", {}, [{}]], ["_args", []], ["_speed", 0.5], ["_forcepos", []] ];
	private _currentID = GVAR(progressNamespace) getVariable _hudID;
    if (isNil "_currentID") then { _currentID = -1 };
    if (_currentID != -1) then {
        [_currentID] call MFUNC(removePerFrameHandler);
    };

    if (isNull (uiNamespace getVariable [QSVAR(progressHUD), displayNull])) then {
        QSVAR(progressHUD) cutRsc [QSVAR(progressHUD), "PLAIN"];
    };

    // -- If this is a new HUD, create a controls group for it and saved the ID for further usage
	disableSerialization;
    private _newHUD = (GVAR(currentProgressHuds) pushBackUnique _hudID) != -1;
    private ["_controlsGroupIDC"];
    if (_newHUD) then {
        _controlsGroupIDC = GVAR(progressIdcIndex) + (1 * __GROUPIDC);
        GVAR(progressNamespace) setVariable [(_hudID + "_id"), _controlsGroupIDC];
        GVAR(progressIdcIndex) = _controlsGroupIDC;

        // -- Animate open the element
        __DISPLAY ctrlCreate ["RscFrlProgressGroup", _controlsGroupIDC, (__DISPLAY displayCtrl 1)];
		if (_forcepos isEqualTo []) then {
			(__DISPLAY displayCtrl _controlsGroupIDC) ctrlSetPosition [0, ((count GVAR(currentProgressHuds) - 1 - count GVAR(forcedHuds)) * GRIDY(1.4)), SIZE_WIDTH_PROGRESS, GRIDY(1.4)];
		} else {
			_forcepos params ["_xPos", "_yPos"];
			(__DISPLAY displayCtrl _controlsGroupIDC) ctrlSetPosition [0, _xPos, SIZE_WIDTH_PROGRESS, _yPos];
			GVAR(forcedHuds) pushBackUnique _hudID;
		};
		(__DISPLAY displayCtrl _controlsGroupIDC) ctrlCommit 0.2;
    } else {
        _controlsGroupIDC = GVAR(progressNamespace) getVariable (_hudID + "_id");
    };

	private _controlsGroup = __DISPLAY displayCtrl _controlsGroupIDC;
    // -- Set a var with the controls group, so the codeInit has it availble
    _args call _codeInit;

    // -- Create a perframe handle that executes _codeLoop every so often with the old arguments.
	private _currentID = [{
        (_this select 0) params ["_args", "_codeLoop", "_controlsGroupIDC"];
        private _controlsGroup = (__DISPLAY) displayCtrl _controlsGroupIDC;
        _args call _codeLoop;
    }, _speed, [_args, _codeLoop, _controlsGroupIDC]] call MFUNC(addPerFramehandler);
    GVAR(progressNamespace) setVariable [_hudID, _currentID];
}] call CFUNC(addEventHandler);

// -- Status HIDE -- //
["progressHudHide", {
	(_this select 0) params ["_hudID"];
    private _currentID = GVAR(progressNamespace) getVariable _hudID;
    if (!(isNil "_currentID") && {(_currentID != -1)}) then {
        [_currentID] call MFUNC(removePerFrameHandler);
        GVAR(progressNamespace) setVariable [_hudID, -1];

        // -- Animate the HUD that we are closing upwards
        private _controlsGroup = (__DISPLAY) displayCtrl (GVAR(progressNamespace) getVariable (_hudID + "_id"));
        private _groupPositionMain = ctrlPosition _controlsGroup;
        _groupPositionMain set [3, 0];
        _controlsGroup ctrlSetPosition _groupPositionMain;
        _controlsGroup ctrlCommit 0.15;

        // -- Shift all progress elements below this one upward
        private _positionIndex = 0;
        private _hudIndex = (GVAR(currentProgressHuds) find _hudID);
		GVAR(forcedHuds) = GVAR(forcedHuds) - [_hudID];

		GVAR(currentProgressHuds) deleteAt _hudIndex;
		if (GVAR(currentProgressHuds) isEqualTo []) exitWith {
			GVAR(progressIdcIndex) = 0;
			QSVAR(progressHUD) cutFadeOut 0.3;
		};

        for "_i" from _hudIndex to (count GVAR(currentProgressHuds) - 1) do {
            private _hudElementID = (GVAR(currentProgressHuds) select _i);
			// no touchy forced elements
			if !(_hudElementID in GVAR(forcedHuds)) then {
				private _controlsGroup = (__DISPLAY) displayCtrl (GVAR(progressNamespace) getVariable (_hudElementID + "_id"));
	            private _groupPosition = ctrlPosition _controlsGroup;
	            _groupPosition set [1, _i * GRIDY(1.4)];
	            _controlsGroup ctrlSetPosition _groupPosition;
	            _controlsGroup ctrlCommit 0.15;
			};
        };

    };

}] call CFUNC(addEventHandler);
