#include "macros.hpp"
/*
 *	File: fn_clientInitControlHUD.sqf
 *	Author: Adanteh
 *	Inits the events for controls HUD (Button hold/press actions) with the control shown on screen
 *
 *	Example:
 *	FUNC(clientInitControlHUD);
 */

#define CTRL(x) (_display displayCtrl x)
GVAR(controlsKeyActions) = [[], []];
GVAR(controlsMouseActions) = [[], []];
GVAR(controlsKeyActionRunning) = -1;
GVAR(controlsMouseActionRunning) = -1;
GVAR(controlsHudHandle) = -1;

[QGVAR(hideControlsHUD), {
	GVAR(controlsKeyActions) = [[], []];
	GVAR(controlsMouseActions) = [[], []];

    { inGameUISetEventHandler [_x, ""]; nil} count ["PrevAction", "Action", "NextAction"];
    QSVAR(controlsHUD) cutFadeOut 0;
    [GVAR(controlsHudHandle)] call MFUNC(removePerFrameHandler);
}] call CFUNC(addEventHandler);


[QGVAR(showControlsHUD), {
	params ["_actions"];
	// if no functions and keys are defined we scrap the input handling, allowing the usage of regular inputs
	// that way we dont have override of exiting or entering isVehicle
	// one lazy way to keep this UI with external input handling
	private _takeInputs = ({count _x > 2} count _actions > 0);

	if (_takeInputs) then {
		{ inGameUISetEventHandler [_x, "true"]; nil} count ["PrevAction", "Action", "NextAction"];
		// -- Add the actions -- //
		{
			_x params ["_controlText", "_controlImage", "_actionFnc", "_mouse", "_keybind"];
			_keybind params ["_key", ["_modifiers", [false, false, false]]];
			if (_mouse) then {
	            //(GVAR(controlsKeyActions) select 0) pushBack [_key, _modifiers];
				(GVAR(controlsMouseActions) select 0) pushBack _key;
				(GVAR(controlsMouseActions) select 1) pushBack _actionFnc;
			} else {
				(GVAR(controlsKeyActions) select 0) pushBack [_key, _modifiers];
				(GVAR(controlsKeyActions) select 1) pushBack _actionFnc;
			};
	        nil;
		} count _actions;
	};

	// -- Show the controls -- //
    GVAR(controlsHudHandle) = [{
        params ["_params", "_id"];
        _params params ["_actions"];
        disableSerialization;

        private _display = uiNamespace getVariable [QSVAR(controlsHUD), displayNull];
        if (isNull _display) then {
            QSVAR(controlsHUD) cutRsc [QSVAR(controlsHUD), "PLAIN", 0];
            _display = uiNamespace getVariable [QSVAR(controlsHUD), displayNull];
            CTRL(2) ctrlSetFade 0;
            CTRL(2) ctrlCommit 0;
        };

        private _text = "";
        {
            _x params ["_controlText", "_controlImage"];
            private _icons = _controlImage apply {format ["<img size='1.5' image='%1'/>", _x]} joinString " + ";
            private _condition = _x param [5, { true }];
            if (call _condition) then {
                _text = _text + format ["%1%2<br />", _icons, _controlText];
            } else {
                private _conditionMsg = _x param [6, { "" }];
                _text = _text + format ["%1<t color='#AFAFAFFF'>%2 (%3)</t><br />", _icons, _controlText, call _conditionMsg];
            };
            nil;
        } count _actions;

        CTRL(2) ctrlSetStructuredText parseText _text;
        CTRL(2) ctrlCommit 0;
    }, 0, [_actions]] call MFUNC(addPerFramehandler);
}] call CFUNC(addEventHandler);

[QGVAR(addControlsKeyEH), {
    (findDisplay 46) displayAddEventHandler ["KeyDown", {
        params ["_display", "_button"];

        private _keyIndex = (GVAR(controlsKeyActions) select 0) find _button;
        if (_keyIndex == -1) exitWith { false };
        call (GVAR(controlsKeyActions select 1) select _keyIndex);
        false
    }];

    (findDisplay 46) displayAddEventHandler ["KeyUp", {
        params ["_display", "_button"];
        false
    }];
}] call CFUNC(addEventHandler);

[QGVAR(addControlsMouseEH), {
    (findDisplay 46) displayAddEventHandler ["MouseButtonDown", {
        params ["_display", "_button"];

		if (visibleMap) exitWith { };
        private _keyIndex = (GVAR(controlsMouseActions) select 0) find _button;
        if (_keyIndex == -1) exitWith { false };
        call (GVAR(controlsMouseActions select 1) select _keyIndex);
        false
    }];

    (findDisplay 46) displayAddEventHandler ["MouseButtonUp", {
        params ["_display", "_button"];
        false
    }];
}] call CFUNC(addEventHandler);

["missionStarted", {
    [QGVAR(addControlsKeyEH)] call CFUNC(localEvent);
    [QGVAR(addControlsMouseEH)] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

/*
[QMVAR(showControlsHUD), [
	["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa", "to place rally", _actionFunc1, true, [1]],
	["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa", "to place rally", _actionFunc2, true, [1]]
	["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa", "to dance", _actionFunc1, false, [DIK_Q]],
	["\a3\3DEN\Data\Displays\Display3DEN\Hint\lmb_ca.paa", "to kill yourself", _actionFunc2, false, [DIK_E]]
]] call CFUNC(localEvent);*/
