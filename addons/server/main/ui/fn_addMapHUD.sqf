#include "macros.hpp"
/*
 *	File: fn_addMapHUD.sqf
 *	Author: Adanteh
 *	Adds code with priority. All code should return structured text and not need any arguments.
 *
 *	Example:
 *	[visibleMap] call FUNC(addMapHUD);
 */

params ["_code", ["_priority", 5]];

if (isNil QGVAR(mapInfoHUD)) then { GVAR(mapInfoHUD) = []; };
if (isNil QGVAR(mapDrawEvent)) then {
	GVAR(mapDrawEvent) = ["visibleMapChanged", FUNC(showMapHUD)] call CFUNC(addEventHandler);
};

GVAR(mapInfoHUD) pushBack [_priority, _code];
GVAR(mapInfoHUD) sort true;

