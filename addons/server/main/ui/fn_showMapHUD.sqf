#include "macros.hpp"

/*
 *	File: fn_showMapHUD.sqf
 *	Author: Adanteh
 *	Shows a small HUD element on the map and populate it with stacked details
 *
 *	Example:
 *	[visibleMap] call FUNC(showMapHUD);
 */

(_this select 0) params ["_visibleMap"];

#define ctrlbase ((uiNamespace getVariable QGVAR(mapInfoHUD)))
#define ctrl(x) ((uiNamespace getVariable QGVAR(mapInfoHUD)) controlsGroupCtrl x)

if (isNil QGVAR(mapInfoHUD)) then { GVAR(mapInfoHUD) = []; };

if (_visibleMap) then {

	[{
	    if (visibleMap) then {
	    	// -- Format all code -- //
	    	private _text = [QGVAR(mapInfoCache), {
	    		private _text = "<t align='center'>";
		    	{
		    		_x params ["_priority", "_code"];
		    		private _elementText = call _code;
		    		if (_forEachIndex < (count GVAR(mapInfoHUD) - 1)) then {
		    			_elementText = _elementText + "<br/><br/>";
		    		};
		    		_text = _text + _elementText;

		    	} forEach GVAR(mapInfoHUD);
		    	_text = _text + "</t>";
		    	_text;
		    }, [], 1, "visibleMapChanged"] call CFUNC(cachedCall);

	    	ctrl(51007) ctrlSetStructuredText (parseText _text);
	    	private _height = ctrlTextHeight ctrl(51007);
	    	private _backgroundPos = ctrlPosition ctrl(51006);
	    	private _textPos = ctrlPosition ctrl(51007);
	    	private _groupPos = ctrlPosition ctrl(51005);
	    	private _mainPos = ctrlPosition ctrlbase;

	    	_textPos set [3, _height];
	    	_backgroundPos set [3, (_height + (2 * PX(1.5)) + (2 * __H_SETTING) + PY(__PADDING))];
	    	_groupPos set [3, (_height + (2 * PX(1.5)) + (2 * __H_SETTING) + PY(__PADDING))];
	    	_mainPos set [3, (_backgroundPos select 3) + __H_HEADER];
	    	ctrl(51005) ctrlSetPosition _groupPos;
	    	ctrl(51006) ctrlSetPosition _backgroundPos;
	    	ctrl(51007) ctrlSetPosition _textPos;
	    	ctrlbase ctrlSetPosition _mainPos;
	    	ctrl(51005) ctrlCommit 0;
	    	ctrl(51006) ctrlCommit 0;
	    	ctrl(51007) ctrlCommit 0;
	    	ctrlbase ctrlCommit 0;
	    	{
	    		ctrlSetFocus (_x controlsGroupCtrl 50132);
	    		(_x controlsGroupCtrl 50132) ctrlSetPosition (ctrlPosition (_x controlsGroupCtrl 50132));
	    		(_x controlsGroupCtrl 50132) ctrlCommit 0;
	    		nil;
	    	} count [ctrl(51022), ctrl(51023)];
	    } else {
	    	[_this select 1] call MFUNC(removePerFrameHandler);
	    };
	}, 0, []] call MFUNC(addPerFramehandler);
};
