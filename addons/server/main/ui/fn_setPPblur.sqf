#include "macros.hpp"
/*
 *	File: fnc_setPPBlur.sqf
 *	Author: Adanteh
 *	This functions allows multiple 'effects' to use the same pp layer effectively. When disabling an effect on a certain priority it will automatically fall back to a lower priority.
 *	Priority is descending, this means 0 is low priority and 10 is high priority.
 *
 *	Locality: Client
 *
 *	#0 SCALAR: _priority - How important this effect is. The highest priority gets used for the actual effect
 *	#1 ARRAY: _value - Which value to apply to this PP. [0] is used to delete this priority
 *	#2 SCALAR: _speed - Which speed to commit the effect with
 *	#3 BOOLEAN: _reset - Allows you to remove all saved values and disable the effect completely
 *
 *
 *	Returns:
 *	Effect adjusted <BOOL>
 *
 *	Example:
 *	[6, [1.2], 0.1, 4] call FUNC(setPPBlur);
 */


params [
	["_priority", 0],
	["_value", [1]],
	["_speed", 1.0],
	["_autoFade", 0],
	["_reset", false]
];

// -- Only show effects for players -- //
if !(hasInterface) exitWith { };

if (isNil QGVAR(ppBlur)) then {
	GVAR(ppBlur) = ["DynamicBlur", 199+6, [0]] call CFUNC(createPPEffect); // -- Dynamic blur: Bleeding / Pain pulses -- //
	GVAR(ppBlur_saved) = [];
};

if (_reset) exitWith {
	GVAR(ppBlur) ppEffectAdjust [0];
	GVAR(ppBlur) ppEffectCommit _speed;
	GVAR(ppBlur) ppEffectEnable false;
	GVAR(ppBlur_saved) = [];
	true
};

// -- If this is a pulse effect, automatically remove the effect on this priority after given auto fade time. -- //
if (_autoFade != 0) then {
	[{
		params ["_priority", "_value", "_autoFade"];
		// -- Only hide this automatically if the value is still the same as it was when started -- //
		_valueUnchanged = false;
		{
			if (_priority == (_x param [0, 0])) exitWith {
				_valueUnchanged = (_value isEqualTo (_x param [1, [0]]));
			};
		} forEach GVAR(ppBlur_saved);
		if (_valueUnchanged) then {
			[_priority, [0], _autoFade] call FUNC(setPPBlur);
		};
	}, _autoFade, [_priority, _value, _autoFade]] call CFUNC(wait);
};

// -- If we are adjusting an effect, make sure there are no existing effects with a higher priority -- //
_adjustValue = true;
_existingIndex = -1;
{
	_prioritySaved = _x param [0, 0];
	if (_prioritySaved > _priority) then {
		_adjustValue = false;
	};
	if (_prioritySaved == _priority) exitWith {
		_existingIndex = _forEachIndex;
	};
} forEach GVAR(ppBlur_saved);

// -- If value was set to 0, remove it from saved values at this priority and figure out if there are any other values we can set it to -- //
_valueRemoved = false;
if (_value isEqualTo [0]) then {
	_valueRemoved = true;
	if (_existingIndex != -1) then {
		GVAR(ppBlur_saved) deleteAt _existingIndex;
	};

	// -- If we are removing one effect, but there are still others left use the most important remaining value -- //
	if !(GVAR(ppBlur_saved) isEqualTo []) then {
		_adjustValue = true;
		_value = (GVAR(ppBlur_saved) select 0) param [1, [0]];
	};
};

// -- Apply new most important value to the pp layer and commit with given speed -- //
if (_adjustValue) then {
	GVAR(ppBlur) ppEffectEnable true;
	GVAR(ppBlur) ppEffectAdjust _value;
	GVAR(ppBlur) ppEffectCommit _speed;
};

// -- Sort by priority after adding a new item -- //
if !(_valueRemoved) then {
	if (_existingIndex == -1) then {
		GVAR(ppBlur_saved) pushBack [_priority, _value];
		GVAR(ppBlur_saved) sort false;
	} else {
		GVAR(ppBlur_saved) set [_existingIndex, [_priority, _value]];
	};
} else {
	// -- When there are no effects left using this pp, disable it -- //
	if (GVAR(ppBlur_saved) isEqualTo []) then {
		[{
			// -- Double check if no new values were added meanwhile -- //
			if (GVAR(ppBlur_saved) isEqualTo []) then {
				GVAR(ppBlur) ppEffectEnable false;
			};
		}, _speed + 0.01, []] call CFUNC(wait);
	};
};

_adjustValue