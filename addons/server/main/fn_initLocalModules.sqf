 /*
     Function:       FRL_Main_fnc_initLocalModules
     Author:         Adanteh
     Description:    Passthrough from Clib inits to our local module system
 */
 #include "macros.hpp"

["preinit"] call FUNC(moduleQueueExecute);
[] spawn {
    ["postinit"] call FUNC(moduleQueueExecute);
};
