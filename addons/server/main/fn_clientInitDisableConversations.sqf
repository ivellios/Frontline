/*
    Function:       FRL_Main_fnc_clientInitDisableConversations
    Author:         Adanteh
    Description:    Disables radio/text messages (This is in combination with cfgVoice overrides)
*/
#include "macros.hpp"

enableSentences false;
enableRadio false;

["playerChanged", {
    (_this select 0) params ["_newPlayer"];
    _newPlayer disableConversation true;
    _newPlayer setVariable ["BIS_noCoreConversations", true];
}] call CFUNC(addEventhandler);

["unitCreated", {
    params ["_unit"];
    _unit disableConversation true;
    _unit setVariable ["BIS_noCoreConversations", true];
}] call CFUNC(addEventhandler);
