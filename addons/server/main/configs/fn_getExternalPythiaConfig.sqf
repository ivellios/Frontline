/*
    Function:       FRL_Main_fnc_getExternalPythiaConfig
    Author:         Adanteh
    Description:    This returns an external value
    Example:        ["radio_channel", ""] call FRL_Main_fnc_getExternalPythiaConfig;
*/
#include "macros.hpp"

params [["_value", "", [""]], "_defaultValue"];

if (isNil "py3_fnc_callExtension") exitWith {
    _defaultValue;
};

private _return = [format ["%1.getValue", "Frontline"], [_value]] call py3_fnc_callExtension;
_return;
