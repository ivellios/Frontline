/*
    Function:       FRL_Main_fnc_getExternalConfig
    Author:         Adanteh
    Description:    This returns an external value
    Example:        ["radio_channel", ""] call FRL_Main_fnc_getExternalConfig;
*/
#include "macros.hpp"

params [["_value", "", [""]], "_defaultValue"];

#define __FILENAME "\pr\frl\addons\server\configs\serverconfiginclude.sqf"

if !(isServer) exitWith { _defaultValue };

private _processedSettings = missionNamespace getVariable QGVAR(externalSettings);

if (isNil "_processedSettings") then {
    private _fnc_fileExists = {
    /*    private ["_ctrl", "_fileExists"];
        disableSerialization;
        _ctrl = findDisplay 0 ctrlCreate ["RscHTML", -1];
        _ctrl htmlLoad __FILENAME;
        _fileExists = ctrlHTMLLoaded _ctrl;
        ctrlDelete _ctrl;*/
        true;
    };

    private ["_code"];
    if (hasInterface) then {
        // -- Check if file exists -- http://killzonekid.com/arma-scripting-tutorials-kk_fnc_fileexists/ -- //
        private _fileExists = [] call _fnc_fileExists;
        if (_fileExists) then {
            _code = preprocessFile __FILENAME;
        } else {
            _code = "";
        };
    } else {
        _code = preprocessFile __FILENAME;
    };

    if (_code != "") then {
        // -- Split up all the settings -- //
        _processedSettings = _code splitString ";";
        private _deleteIndex = 0;
        {
            private _splitSetting = (_x splitString "=");
            if (count _splitSetting != 2) then {
                _processedSettings deleteAt (_forEachIndex - _deleteIndex);
                _deleteIndex = _deleteIndex + 1;
                //[format ["ERROR in serverconfig file. Don't use '=' or ';' in setting values"]] call IFUNC(logInfo);
            } else {
                // -- Remove trailing space for setting name -- //

                _splitSetting set [0, ([_splitSetting select 0] call FUNC(removeSpaces))];
                _splitSetting set [1, ([_splitSetting select 1] call FUNC(removeSpaces))];
                _processedSettings set [_forEachIndex, _splitSetting];
            };
        } forEach _processedSettings;
    } else {
        _processedSettings = [];
    };

    // -- Process these settings once only, save use for the rest of the mission -- //
    missionNamespace setVariable [QGVAR(externalSettings), _processedSettings];
};

private _returnValue = "FRL_<###>";
{
    _x params ["_settingName", "_settingValue"];
    if (_settingName == _value) exitWith {
        _returnValue = call compile _settingValue;
    };
} forEach _processedSettings;

_returnValue;
