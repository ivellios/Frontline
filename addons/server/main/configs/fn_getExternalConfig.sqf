/*
    Function:       FRL_Main_fnc_getExternalConfig
    Author:         Adanteh
    Description:    This returns an external value
    Example:        ["radio.channelName", ""] call FRL_Main_fnc_getExternalConfig;
*/
#include "macros.hpp"

params [["_value", "", [""]], "_defaultValue"];

#define __FILENAME "\pr\frl\addons\server\main\configs\serverconfiginclude.sqf"

if !(isServer) exitWith { _defaultValue };
if (isNil QFUNC(JsonGET)) then { call FUNC(serverInitJson) };

private _processedSettings = missionNamespace getVariable QGVAR(externalSettings);

if (isNil "_processedSettings") then {
    private _fnc_fileExists = {
    /*    private ["_ctrl", "_fileExists"];
        disableSerialization;
        _ctrl = findDisplay 0 ctrlCreate ["RscHTML", -1];
        _ctrl htmlLoad __FILENAME;
        _fileExists = ctrlHTMLLoaded _ctrl;
        ctrlDelete _ctrl;*/
        true;
    };

    private ["_code"];
    if (hasInterface) then {
        // -- Check if file exists -- http://killzonekid.com/arma-scripting-tutorials-kk_fnc_fileexists/ -- //
        private _fileExists = [] call _fnc_fileExists;
        if (_fileExists) then {
            _code = preprocessFile __FILENAME;
        } else {
            _code = "";
        };
    } else {
        _code = preprocessFile __FILENAME;
    };

    if (_code != "") then {
        _processedSettings = _code call FUNC(JsonParse);
    } else {
        _processedSettings = [];
    };

    // -- Process these settings once only, save use for the rest of the mission -- //
    missionNamespace setVariable [QGVAR(externalSettings), _processedSettings];
};

private _returnValue = [_processedSettings, _value] call FUNC(JsonGet);
if (_returnValue isEqualTo objNull) then { _returnValue = _defaultValue };
_returnValue;
