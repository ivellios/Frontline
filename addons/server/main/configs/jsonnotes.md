/***
JSON Parser by Muzzleflash
v0.987

--- To parse json
	_json = "JSON_STRING" call MF_Json_Parse

	_json now contains the JSON structure in SQF as described below.
	If some error occurs during parsing _json would be set to a string instead,
	so you to check for errors you should check whether the return is of type string.

--- To ease use, MF_Json_Get can be used to read keys separated by period (.)
	It currently does not handle indexing into arrays

--- Example Usage:

FILE: mydata.json:
{
	"name": "John Doe",
	"age": 34,
	"active": true,
	"qualifications": ["rifleman","grenadier"],
	"position": {
		"rank": "Private",
		"squad": "Bravo"
	}
}

Script:
//Load library
[] call compile preProcessFile "json.sqf";
_json = (loadFile "mydata.json") call MF_Json_Parse;
//Find rank
_rank = [_json, "position.rank"] call MF_Json_Get;

--- How JSON data is mapped to SQF

	JSON objects map to:  ["object", [[key_1, val_1], [key_2, val_2] ...]]
	JSON arrays map to: ["array", [val_1, val_2, val_3, ...]]
	JSON null maps to objNull
	The other JSON types maps to what you would expect, eg.: false, true, 123.456, "and strings"

	The above example would map to:

	["object", [
		["name", "John Doe"],
		["age", 34],
		["active", true],
		["qualifications", ["array", ["rifleman", "grenadier"]]],
		["position",
			["object", [
				["rank", "Private"],
				["squad", "Bravo"]
			]]
		]
	]]

--- The only unsupported feature is  \uXXXX inside strings.

--- Performance notes

	Scheduled environment. (Do not parse in non-scheduled environment since the parsing is aborted halway by the engine)

	The second number in parantheses is the amount of time spent on lexing out of the total time taken.

	254 lines (8577 chars) takes 0.81 (0.78) seconds
	1010 lines (34693 chars) takes 3.29 (3.06) seconds
	10082 lines (349246 chars) takes 34.19 (31.94) seconds

	On my budget non-server machine with 500 AI all engaged in battle simultaneous, the time to parse doubled.

	So expected performance is around 5000-10000 characters or 150-300 lines per second.
***/
