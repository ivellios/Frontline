#include "macros.hpp"
/*
 *	File: fn_persistentEventGlobal.sqf
 *	Author: Adanteh
 *	Handles persistent events
 *
 *	0: The unit <OBJECT>
 *
 *	Returns:
 *	true <BOOL>
 *
 *	Example:
 *	[player] call FUNC(persistentEventGlobal);
 */

params [["_eventName", "", [""]], ["_args", []]];

if (isNil QGVAR(persistentEventGlobal)) then {
	GVAR(persistentEventGlobal) = [];
	["eventAdded", {
	    params ["_arguments", "_data"];
	    _arguments params ["_event", "_function", "_args"];
	    if (toLower _event in GVAR(persistentEventGlobal)) then {
	        if (_function isEqualType "") then {
	            _function = parsingNamespace getVariable [_function, {}];
	        };
	        [nil, _args] call _function;
	    };
	}] call CFUNC(addEventHandler);
};

if ((GVAR(persistentEventGlobal) pushBackUnique toLower _eventName) == -1) then {
	// -- Already executed this event. Probably don't want a second call
} else {
	publicVariable QGVAR(persistentEventGlobal);
};

_this call CFUNC(globalEvent);
