/*
    Function:       FRL_Main_fnc_respawn
    Author:         NetFusion
    Description:    Respawns a unit
*/
#include "macros.hpp"

/*---------------------------------------------------------------------------
    This is a modified version of AAW functions, June 11th 2016 version
    Released under APL license by the AAW Team.

    Current project can be found here: https://github.com/TaktiCool/ArmaAtWar
    Full license code here: https://www.bistudio.com/community/licenses/arma-public-license
---------------------------------------------------------------------------*/

params ["_targetPosition", ["_tempUnit", false]];

// Remove tempUnit status
if (CLib_Player getVariable [QSVAR(tempUnit), false]) then {
    CLib_Player setVariable [QSVAR(tempUnit), false];
    ["enableSimulation", [CLib_Player, true]] call CFUNC(serverEvent);
    ["hideObject", [CLib_Player, false]] call CFUNC(serverEvent);
    [{
        CLib_Player allowDamage true;
    }, 1] call CFUNC(wait);
};


if !(alive CLib_Player) then {
    // This will cause one frame delay until new unit is available
    setPlayerRespawnTime 0;

    [{
        params ["_targetPosition", "_tempUnit", "_oldPlayer"];
        [['execNextFrame', _this], "cyan"] call MFUNC(debugMessage);

        setPlayerRespawnTime 10e10;

        if (_tempUnit) then {
            CLib_Player setVariable [QSVAR(tempUnit), true];
            CLib_Player allowDamage false;
            Clib_PLayer setVelocity [0, 0, 0]; // Needed because otherwise person will reenable simulation with very fast falling
            Clib_Player enableSimulation false;
            ["enableSimulation", [CLib_Player, false]] call CFUNC(serverEvent);
            ["hideObject", [CLib_Player, true]] call CFUNC(serverEvent);
        };

        CLib_Player setDir (random 360);
        CLib_Player setPosASL _targetPosition;

        // Respawn event is triggered by engine
        ["MPRespawn", [CLib_Player, _oldPlayer]] call CFUNC(globalEvent);
    }, [_targetPosition, _tempUnit, CLib_Player]] call CFUNC(execNextFrame);
} else {
    CLib_Player setDir (random 360);
    CLib_Player setPosASL _targetPosition;

    // This is instant cause we reuse the old unit
    ["Respawn", [CLib_Player, CLib_Player]] call CFUNC(localEvent);
    ["MPRespawn", [CLib_Player, CLib_Player]] call CFUNC(globalEvent);
};
