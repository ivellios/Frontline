#define DFNC(f) class f
#define FNC(f) DFNC(f)
#define APIFNC(f) DFNC(f) {api = 1;}
#define MODULE(m) class m

class CfgCLibModules {
    class FRL {
        path = "\pr\FRL\addons\server";
        dependency[] = {"CLib"};

        MODULE(Default) {
            dependency[] = {
                "FRL/Main",
                "FRL/Misc"
                //"FRL/Afkkick"
            };
        };

        MODULE(Singleplayer) {
          dependency[] = {
              "FRL/Main",
              "FRL/Misc"
          };
        };

        MODULE(Main) {
            dependency[] = {"Clib"};
            #include "main\clibModule.hpp"
        };

        MODULE(misc) {
            dependency[] = {"FRL/Main"};
            #include "misc\clibModule.hpp"
        };

        #include "dynamicFrontline\clibModule.hpp"
        #include "searchdestroy\clibModule.hpp"
    };
};
