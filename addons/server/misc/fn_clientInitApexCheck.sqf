/*
    Function:       FRL_Misc_fnc_clientInitApexCheck
    Author:         Adanteh
    Description:    Checks if the current loaded world needs apex, and if so show a warning if you don't have it.
*/

#include "macros.hpp"

if (true) exitWith { };

["MissionStarted", {
    [{
        private _worldName = worldName;
        private _requiresApex = (getNumber (configFile >> "CfgWorlds" >> _worldName >> "NeedsApex")) >= 1;
        if !(_requiresApex) then {
            _requiresApex = (getText (configFile >> "CfgWorlds" >> _worldName >> "dlc")) == "Expansion";
        };
        if (_requiresApex) then {
            private _hasApex = !(395180 in (getDLCs 2));
            if !(_hasApex) then {
                ["apexcheck", true, "<t align='center'>You need Apex to play on this terrain.<br />Make sure you pick it up on the next sale!<br/><br />Disconnecting in 30s<br />;_;</t>"] call MFUNC(blockingMessage);
                if !(isNull (uiNamespace getVariable [QEGVAR(RespawnUI,respawnDisplay), displayNull])) then {
                    (uiNamespace getVariable [QEGVAR(RespawnUI,respawnDisplay), displayNull]) closeDisplay 1;
                };
                [{ ([] call bis_fnc_displayMission) closeDisplay 2; }, 30] call CFUNC(wait);
            };
        };
    }, 5] call CFUNC(wait);
}] call CFUNC(addEventHandler);
