/*
    Function:       FRL_Misc_fnc_serverInitROE
    Author:         Adanteh
    Description:    Rules of engagement management for server. Logs TKs and so
*/
#include "macros.hpp"

["IncapPlayer", {
    (_this select 0) params ["_victim", "_killers"];

    if ([_victim] isEqualTo _killers) exitWith { }; // -- Suicide

    private _killerFirst = _killers param [0, objNull];
    private _killerSide = side group _killerFirst;

    if ((side group _victim) getFriend _killerSide >= 0.6) then {
        private _message = format ["%1 Teamkilled %2 <automatic message>", _killerFirst call CFUNC(name), _victim call CFUNC(name)];
        ["systemChat", [_message]] call CFUNC(globalEvent);
    };
}] call CFUNC(addEventhandler);
