#include "macros.hpp"
/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Runs random small systems on server, not connected to other things whatsoe ver
 *
 *	Example:
 *	[player] call FUNC(serverInit);
 */

[true, true, false] call FUNC(remoteSensorToggle);

["missionStarted", {
	enableEnvironment false;
}] call CFUNC(addEventHandler);

// -- Disable chat spam
{
    _x call CFUNC(addIgnoredEventLog);
    nil
} count [
    ["drawmapgraphics", 0],
    ["eventadded", 0],
    ["cursortargetchanged", 0],
    ["cursorobjectchanged", 0],
    ["playerinventorychanged", 0],
	["animstatechanged", 0],
	["visiblemapchanged", 0]
];
