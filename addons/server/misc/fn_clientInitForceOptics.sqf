#include "macros.hpp"
/*
 *	File: fn_clientInitForceOptics.sqf
 *	Author: Adanteh
 *	This forces having 3D optics on RHS. Otherwise they get switched out with way more effective 2D Optics
 *
 *	[player] call FUNC(clientInitForceOptics);
 */

if (isClass (configFile >> "CfgPatches" >> "rhs_weapons") && { (!isNil "rhsusf_fnc_findPlayer") }) then {
	[{
		if ((profileNamespace getVariable ["rhs_preferedOptic", 1]) != 1) then {
			profileNamespace setVariable ["rhs_preferedOptic", 1];
			saveProfileNamespace;

			// -- Restore 3D sight
			private _p = call rhsusf_fnc_findPlayer;
			private _base = getText (configFile >> "cfgWeapons" >> ((primaryWeaponItems _p) select 2) >> "rhs_optic_base");
			if (_base isEqualTo "") exitWith { };
			_p addPrimaryWeaponItem (format["%1_3d", _base]);
		};
	}, 5] call MFUNC(addPerFramehandler);
};
