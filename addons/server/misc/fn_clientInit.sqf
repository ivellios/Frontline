/*
    Function:       FRL_Misc_fnc_clientInit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#define __INCLUDE_DIK
#include "macros.hpp"

 // -- Improves performance, by disabling raycasting (Used for AI) -- //
[true, true, false] call FUNC(remoteSensorToggle);

#ifdef ISDEV
	[QGVAR(teleport), "onMapSingleClick", {
	    if (_alt) exitWith {
            if (cameraOn isEqualTo (getConnectedUAV player)) then {
                private _posATL = getPosATL getConnectedUAV player;
                _pos set [2, _posATL select 2];
                (getConnectedUAV player) setPosATL _posATL;
            } else {
                (vehicle player) setPos _pos;
            };
	        true
	    };
	}] call BIS_fnc_addStackedEventHandler;

    // -- Add recompile modules keybind
    [
        "Frontline", QSVAR(recompile),
        ["Recompile", "Recompiles all module functions"],
        { true },
        {
            ["recompile"] call MFUNC(moduleLoad);
            ["Recompiled all functions", "red", 5, 1] call MFUNC(debugMessage);
            true
        },
        [DIK_GRAVE, [true, false, false]], false, true
    ] call MFUNC(addKeybind);
#endif

// -- Readd the Frontline instructions when you change sides (New unit gets created, which removes all custom briefing entries, thanks BIS)
["playerSideChanged", {
	[nil, false] call compile preprocessFileLineNumbers '\pr\frl\addons\interface\scripts\fn_loadingScreen.sqf'
}] call CFUNC(addEventHandler);

["missionStarted", {
	// -- Disable the birds and bees -- //
	enableEnvironment [false, true];


    private _centerPos = [0, 0, 0];
    private _sides = call MFUNC(getSides);
    {
        _centerPos = _centerPos vectorAdd (getMarkerPos format ["base_%1", _x]);
    } forEach _sides;

    // -- Center map on main base
    GVAR(firstMapOpen) = true;
    GVAR(baseCenterPos) = _centerPos vectorMultiply ( 1 / (count _sides));
    ["visibleMapChanged", {
        if (GVAR(firstMapOpen)) then {
            (_this select 1) params ["_centerPos"];
            private _controlMap = ((findDisplay 12) displayCtrl 51);
            private _mapScale = (ctrlMapScale _controlMap) min 0.15;
            _controlMap ctrlMapAnimAdd [0, _mapScale, _centerPos]; // Dialog syntax can not be used
            ctrlMapAnimCommit _controlMap;
            GVAR(firstMapOpen) = false;
        };

    }, [GVAR(baseCenterPos)]] call CFUNC(addEventHandler);

}] call CFUNC(addEventHandler);

// -- Show player numbers on top right corner of map
[{
	private _playersNumber = "";
	{
		private _side = _x;
		private _count = { side group _x == _side } count allPlayers;
		private _flag = [_side, 'flag'] call MFUNC(getSideData);
        private _text = format ["%1 Player%2", _count, ["s", ""] select (_count == 1)];
        private _aiCount = missionNamespace getVariable [QSVAR(aiPerSide), 0];
        if (_aiCount > 0) then {
            _text = format ["%1 (+ %2 AI)", _text, _aiCount];
        };

		_playersNumber = _playersNumber + format ["<t align='%3'><img image='%2' size='1.2'/>    %1</t>", _text, _flag, ["left", "right"] select (_forEachIndex % 2)];
	} forEach (call MFUNC(getSides));
	_playersNumber;
}, 10] call MFUNC(addMapHUD);

// -- Set the font for the menu. For some super weird reason this didn't work in the script related to the menu AND setting it in the configs (WTF) so we have to do it like this instead
["escMenu", {
	((findDisplay 49) displayctrl 2) ctrlSetFont FONTBOLD_ALT;
	((findDisplay 49) displayctrl 1010) ctrlSetFont FONTLIGHT_ALT;
	((findDisplay 49) displayctrl 101) ctrlSetFont FONTLIGHT_ALT;
	((findDisplay 49) displayctrl 301) ctrlSetFont FONTLIGHT_ALT;
	((findDisplay 49) displayctrl 302) ctrlSetFont FONTLIGHT_ALT;
	((findDisplay 49) displayctrl 303) ctrlSetFont FONTLIGHT_ALT;
	((findDisplay 49) displayctrl 307) ctrlSetFont FONTLIGHT_ALT;
	((findDisplay 49) displayctrl 122) ctrlSetFont FONTLIGHT_ALT;
	((findDisplay 49) displayctrl 104) ctrlSetFont FONTLIGHT_ALT;
}] call CFUNC(addEventHandler);


['add', ["gui", 'Frontline Settings', 'Debug', [
    'Fix Camera',
    "Use when it seems like you're spectacting another unit",
    QMVAR(CtrlButton),
    true,
    { true; },
    { switchCamera player },
    ["FIX CAMERA"]
]]] call MFUNC(settingsWindow);
