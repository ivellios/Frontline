#include "macros.hpp"

/*
 *	File: fn_remoteSensorToggle.sqf
 *	Author: Adanteh
 *	Easy mode function for using disableRemoteSensors command. Handles locality
 *
 *	Returns:
 *	None
 *
 *	Example:
 *	[] call FUNC(remoteSensorToggle);
 */

params [["_init", false], ["_toggle", true], ["_broadcast", true]];

// -- Make sure we don't broadcast when init param is used. In that case it's already ran on everyone  -- //
if (!_broadcast && !_init) exitWith {
	[false, _toggle] remoteExec [_fnc_scriptName, 0, false];
};

// -- If this is run through the init check if the server wants us to skp this -- //
if (_init && _toggle && {!(missionNamespace getVariable [QGVAR(remoteSensors), true])}) exitWith {  };


if (getRemoteSensorsDisabled isEqualTo !_toggle) then {
	disableRemoteSensors _toggle;
};

if (isServer) then {
	GVAR(remoteSensors) = _toggle;
	publicVariable QGVAR(remoteSensors);
};