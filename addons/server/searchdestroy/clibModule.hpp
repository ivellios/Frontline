MODULE(searchDestroy) {
	dependency[] = {"CLib", "FRL/Main"};

    class initEnd;
    class initAdmin;
	class clientInit;
	class clientInitIntel;
	class clientInitDeployment;
    class serverInit;
	class serverInitBleed;
	class serverInitIntel;

    class canTakeKitFromCache;
    class changeIntel;
	class isDefender;
    class isAttacker;
	class endMission;
    class getAIWaypointPosition;


    class objectives {
        class bombArmAction;
        class bombCookoffHandle;
        class bombSirenToggle;
        class cacheDestroy;
        class createCache;
        class createDefenderSpawn;
        class createObjective;
        class createObjectiveMarkers;
        class dbgBuildingPos;
        class getNewPosition;
        class getPosInBuilding;
        class processBuildingList;
        class updateCacheMarker;
    };

};
