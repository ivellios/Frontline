/*
    Function:       FRL_searchdestroy_fnc_clientInitDeployment
    Author:         Adanteh
    Description:    Inits the deployment mechanics for SD
*/
#include "macros.hpp"

/*
["placeFOonCall", {
    params ["_caller", "_targetPosition"];

    // -- No distance limit for defenders
    if ([_caller] call FUNC(isDefender)) exitWith {
        true
    };

    // -- Distance from main base / zone / whatever, allow spawning pls
    private _maxDistanceZone = [QEGVAR(rolesspawns,FO_maxDistanceFriendlyZone), 400] call MFUNC(cfgSetting);
    private _withinFriendlyRange = false;
    [missionNamespace getVariable [QMVAR(allSectors), objNull], {
        private _sector = _value;
        if (_value getVariable ["isBase", false]) then {
            if ((_value getVariable ["side", sideUnknown]) isEqualTo (side group _caller)) then {
                private _sectorMarker = _value getVariable ["marker", ""];
                private _sectorSize = selectMax (getMarkerSize _sectorMarker);
                if (_targetPosition distance2D (getMarkerPos _sectorMarker) < (_sectorSize + _maxDistanceZone)) then {
                    _withinFriendlyRange = true;
                };
            };
        };
    }] call MFUNC(forEachVariable);


    // -- FO Chain linking, allows using a deployed FO to place another FO
    private _maxDistance = [QEGVAR(rolesspawns,FO_maxChainDistance), 400] call MFUNC(cfgSetting);
    [EGVAR(rolesspawns,spawnpoints), {
        if ((_value getVariable ["type", ""]) isEqualTo "fo") then {
            if (_value getVariable ["destroyed", false]) exitWith { };
            if ((_value getVariable ["availableFor", sideUnknown]) isEqualTo (side group _caller)) then {
                if (_targetPosition distance2D (_value getVariable ["position", [0, 0, 0]]) < _maxDistance) then {
                    _withinFriendlyRange = true;
                };
            };
        };
    }] call MFUNC(forEachVariable);

    if !(_withinFriendlyRange) exitWith {
        ["showNotification", [format ["You can only place FOs within %1m of Main Base, Forward Base or another FO", _maxDistanceZone, _maxDistance]]] call CFUNC(localEvent);
        #ifdef DEBUGFULL
            ["debugMessage", ["Bypassing distance check for FOs due to debug mode (Wasnt passed)", "orange"]] call CFUNC(localEvent);
            true
        #else
            false
        #endif
    };

    true;

}] call MFUNC(addCondition);
*/
