/*
    Function:       FRL_SearchDestroy_fnc_serverInitIntel
    Author:         Adanteh
    Description:    This inits the intel system.
*/
#include "macros.hpp"

GVAR(intelValue) = 0;
GVAR(intelStage) = 0;
GVAR(intelStages) = [QGVAR(Settings_intelStages), [20, 40, 60, 80]] call MFUNC(cfgSetting);
GVAR(markerSize) = [QGVAR(Settings_markerSizes), [120, 250, 400, 600, 1000]] call MFUNC(cfgSetting);
GVAR(markerArray) = [];

publicVariable QGVAR(intelStages);

// -- Add intel on defender death
["KilledPlayer", {
    (_this select 0) params ["_victim", "_killers"];
    if ([_victim] call FUNC(isDefender)) then {
        private _intelFromDeath = [QGVAR(Settings_deathIntel), 1] call MFUNC(cfgSetting);
        [QGVAR(changeIntel), [_intelFromDeath]] call CFUNC(localEvent);
    };
}] call CFUNC(addEventhandler);


// -- Add intel on defender death
[QGVAR(changeIntel), { (_this select 0) call FUNC(changeIntel); }] call CFUNC(addEventHandler);


// -- Change intel marker size
[QGVAR(intelStageChanged), {
    private _position = GVAR(markerArray) select GVAR(intelStage);
    private _markerSize = GVAR(markerSize) select ((count GVAR(markerSize) - 1) - GVAR(intelStage));
    GVAR(intelMarker) = [_position, _markerSize];
    publicVariable QGVAR(intelMarker);
    QGVAR(updateIntelMarker) call CFUNC(globalEvent);
}] call CFUNC(addEventHandler);


// -- Prepare the marker set for this objective
[QGVAR(objectiveCreated), { _this call FUNC(createObjectiveMarkers) }] call CFUNC(addEventHandler);


#define _LOOPTIME (0.5*60)
#define _BLEEDPER (10*60)
["gamemodeStarted", {
    [{

        if (["isGameStarted", [], true] call MFUNC(checkConditions)) then {

            private _intelAmount = [QGVAR(Settings_passiveIntel), 10] call MFUNC(cfgSetting);
            [QGVAR(changeIntel), [_intelAmount * (_LOOPTIME/_BLEEDPER)]] call CFUNC(localEvent);
            
        };
    }, _LOOPTIME] call MFUNC(addPerFrameHandler);
}] call CFUNC(addEventHandler);
