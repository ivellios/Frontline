/*
    Function:       FRL_SearchDestroy_fnc_isAttacker
    Author:         Adanteh
    Description:    Function to check if you're on attacking team
*/
#include "macros.hpp"

params [["_unit", Clib_Player]];
(side group _unit) isEqualTo GVAR(attackerSide);
