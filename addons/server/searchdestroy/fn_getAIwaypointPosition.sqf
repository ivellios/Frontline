/*
    Function:       FRL_SearchDestroy_fnc_getAIWaypointPosition
    Author:         Adanteh
    Description:    Gets position for AI to move to
*/
#include "macros.hpp"

params ["_side", "_groupPosition", "_group"];

// maybe a different method for attacker/defender should be used, but i think this is fine for now
private _position = [];
private _attacker = _side isEqualTo GVAR(attackerSide);
private _defender = _side isEqualTo GVAR(defenderSide);
private _waypointupdateSpeed = [QGVAR(Settings_aiUpdateSpeedAttacker), 120] call MFUNC(cfgSetting);
#ifdef DEBUGFULL
    _waypointupdateSpeed = 10;
#endif

if (_attacker) then {
    // Get a position within the current intel marker, with minimum 1/3th offset
    private _lastUpdate = _group getVariable [QGVAR(waypointupdateAt), -1];
    if (serverTime > _lastUpdate) then {
        GVAR(intelMarker) params ["_markerPos", "_markerSize"];

        while { true } do { // -- Get non-water waypoint position
            private _random = random (_markerSize - _markerSize / 3);
            private _offset = RANDOMOFFSET(_markerSize / 3, _random);
            _position = _markerPos getPos [_offset, random 360];
            if !(surfaceIsWater _position) exitWith { };
        };
        _group setVariable [QGVAR(waypointupdateAt), serverTime + _waypointupdateSpeed];
    } else {
        _position = [-1]; // -- Dont update
    };
};

if (_defender) then {
    // Get a position within the current intel marker, with minimum 1/5th offset
    private _lastUpdate = _group getVariable [QGVAR(waypointupdateAt), -1];
    if (serverTime > _lastUpdate) then {
        GVAR(intelMarker) params ["_markerPos", "_markerSize"];

        while { true } do { // -- Get non-water waypoint position
            private _random = random (_markerSize - _markerSize / 3);
            private _offset = RANDOMOFFSET(_markerSize / 3, _random);
            _position = _markerPos getPos [_offset, random 360];
            if !(surfaceIsWater _position) exitWith { };
        };
        _group setVariable [QGVAR(waypointupdateAt), serverTime + _waypointupdateSpeed];
    } else {
        _position = [-1]; // -- Dont update
    };
};

if !(_position isEqualTo []) then {
    _AI_waypointPosition = _position; // NO PRIVATE
};

true;
