/*
    Function:       FRL_SearchDestroy_fnc_canTakeKitFromCache
    Author:         Adanteh
    Description:    Checks if unit can take kit from cache
*/
#include "macros.hpp"

params ["_caller", "_target"];

if (_caller getVariable [QGVAR(tookKitThisLife), false]) exitWith {
    if (_caller isEqualTo Clib_Player) then {
        ["showNotification", ["You can only grab your gear once per life"]] call CFUNC(localEvent);
    };
    false;
};
if (_target getVariable ["destroyed", false]) exitWith {
    if (_caller isEqualTo Clib_Player) then {
        ["showNotification", ["This cache is destroyed, you can't get gear here"]] call CFUNC(localEvent);
    };
    false;
};

private _kit = _caller getVariable [QSVAR(rKit), ""];
if (_kit isEqualTo "") exitWith {
    if (_caller isEqualTo Clib_Player) then {
        ["showNotification", ["You lost your assigned kit, select a new one on respawn"]] call CFUNC(localEvent);
    };
    false;
};

_caller setVariable [QGVAR(tookKitThisLife), true];
_caller playActionNow "PutDown";

true;
