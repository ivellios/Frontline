/*
    Function:       FRL_SearchDestroy_fnc_changeIntel
    Author:         Adanteh
    Description:    Changes the intel
    Example:        [10] call FRL_SearchDestroy_fnc_changeIntel
*/
#include "macros.hpp"

params ["_intelChanged"];
GVAR(intelValue) = GVAR(intelValue) + _intelChanged;
private _intelRequired = GVAR(intelStages) param [GVAR(intelStage), 1e9];
if (GVAR(intelValue) >= _intelRequired) then {
    GVAR(intelStage) = GVAR(intelStage) + 1;
    publicVariable QGVAR(intelStage);
    [QGVAR(intelStageChanged), [GVAR(intelStage)]] call CFUNC(globalEvent);
};

publicVariable QGVAR(intelValue);
