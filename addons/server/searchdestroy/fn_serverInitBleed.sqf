/*
    Function:       FRL_searchdestroy_fnc_serverInitBleed;
    Author:         Adanteh
    Description:    Handles bleed

    For SD this means that defending team gets a steady tick of victory points
*/
#include "macros.hpp"

#define _LOOPTIME (0.5*60) // -- Ticket update speed -- //
#define _BLEEDPER (10*60) // -- This means bleed PER time frame (So 30 tickets on a zone is 30 tickets per 10 minutes)

[{

    // -- Default victory point tick for defenders
    private _bleedAmount = [QGVAR(Settings_bleedStrength), 10] call MFUNC(cfgSetting);
    ["adjustTicket", [_bleedAmount * (_LOOPTIME / _BLEEDPER), GVAR(defenderSide)]] call CFUNC(localEvent);
}, _LOOPTIME] call MFUNC(addPerFrameHandler);


// -- Victory points for attackers when they destroy cache
[QGVAR(cacheDestroyed), {
    private _bleedAmount = [QGVAR(Settings_bleedPerCache), 25] call MFUNC(cfgSetting);
    ["adjustTicket", [_bleedAmount, GVAR(attackerSide)]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

["handlePlayerTicket", {
    params ["_unit"];
    ((side group _unit) isEqualTo GVAR(sideAttacker))
}] call MFUNC(addCondition);
