/*
    Function:       FRL_searchdestroy_fnc_endMission
    Author:         Adanteh
    Description:    Ends mission for client and established if player is winning or losing
*/
#include "macros.hpp"

params [["_reason", ""], "_winner", ["_description", ""]];

if (isNil "_winner") then {
    private _playerSideTickets = [playerSide] call EFUNC(tickets,getSideTickets);
    private _otherSideTickets = [[playerSide] call MFUNC(getSideOpposite)] call EFUNC(tickets,getSideTickets);
    _winner = (_playerSideTickets >= _otherSideTickets);
};

[(["FRL_LOOSER", "FRL_WINNER"] select _winner), _winner, _reason, _description] call MFUNC(endMission);

EGVAR(Tickets,deactivateTicketSystem) = true;
