/*
    Function:       FRL_SearchDestroy_fnc_clientInit
    Author:         Adanteh
    Description:    basic init for SD
*/
#include "macros.hpp"

[QGVAR(Settings), configFile >> "FRL" >> "CfgSearchDestroy"] call MFUNC(cfgSettingLoad);
[QGVAR(Settings), missionConfigFile >> "FRL" >> "CfgSearchDestroy"] call MFUNC(cfgSettingLoad);

// -- Make some base markers
[QGVAR(initAO), {
    [missionNamespace getVariable [QMVAR(allSectors), objNull], {
        [false, [_value]] call MFUNC(sectorCreate);
    }] call MFUNC(forEachVariable);

}] call CFUNC(addEventHandler);

// -- Waits till server is done preparing the gamemode
["missionStarted", {
    [{
        [{
            [QGVAR(initAO)] call CFUNC(localEvent);
            [QGVAR(updateIntelMarker)] call CFUNC(localEvent);
            [QGVAR(updateCacheMarker)] call CFUNC(localEvent);

        }, 2.5] call CFUNC(wait);
    }, { missionNamespace getVariable [QGVAR(ServerInitDone), false] }] call CFUNC(waitUntil);
}] call CFUNC(addEventHandler);

["durationEnd", {["Mission time ran out"] call FUNC(endMission)}] call CFUNC(addEventHandler);

// -- Give notification when a cache is destroyed
[QGVAR(cacheDestroyed), {
    (_this select 0) params ["_cache"];

    private _text = ["Your team destroyed an enemy cache", "One of your caches was destroyed!"] select ([] call FUNC(isDefender));
    ["showNotification", [_text]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

[QGVAR(objectiveCreated), {
    ["showNotification", ["A new set of caches was created"]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);


// -- Updates the markers when a cache is created/destroyed
[QGVAR(updateCacheMarker), { _this call FUNC(updateCacheMarker) }] call CFUNC(addEventHandler);
["playerSideChanged", {
    [QGVAR(updateCacheMarker)] call CFUNC(localEvent);

    // -- Spawn players with very basic gear, let them equip their actual gear from the cache
    if ([] call FUNC(isDefender)) then {
        private _basekit = [playerSide, "baserole"] call MFUNC(getSideData);
        MVAR(roleSpawnOverride) = _baseKit;
    } else {
        MVAR(roleSpawnOverride) = nil;
    };
}] call CFUNC(addEventHandler);


[QGVAR(Cache), ["",
    { ["Plant bomb", "Defuse bomb"] select ([] call FUNC(isDefender)) },
    { ["start", _this] call FUNC(bombArmAction) }
]] call EFUNC(interactthreed,menuOptionAdd);

[QGVAR(Cache), ["",
    "Take gear",
    { [Clib_Player] call EFUNC(rolesspawns,roleEquip), },
    { _this call FUNC(canTakeKitFromCache) },
    { [] call FUNC(isDefender) }
]] call EFUNC(interactthreed,menuOptionAdd);


// TODO ADD EXCLUDE FOR BATTLEPREP LEAVING BASE IF YOU ARE DEFENDERS
["battleprep_canLeaveBase", {
    params [["_side", side group Clib_Player]];

    _canLeaveBaseText = "Attackers only"; // NOPRIVATE
    (_side isEqualTo GVAR(defenderSide));
}] call MFUNC(addCondition);

["Respawn", {
    (_this select 0) params ["_newUnit"];
    _newUnit setVariable [QGVAR(tookKitThisLife), nil];
}] call CFUNC(addEventHandler);


[QSVAR(AfterSpawnDelay), {
    (_this select 0) params ["_spawnpoint", "_unit", "_redeploy"];
    if !(_spawnpoint getVariable ["type", ""] isEqualTo "base") then {
        if ([] call FUNC(isDefender)) then {
            ["showNotification", ["Remember to pick up your gear from a cache"]] call CFUNC(localEvent);
        };
    };
}] call CFUNC(addEventHandler);
