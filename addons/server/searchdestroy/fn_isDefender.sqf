/*
    Function:       FRL_SearchDestroy_fnc_isDefender
    Author:         Adanteh
    Description:    Function to check if you're on defending team
*/
#include "macros.hpp"

params [["_unit", Clib_Player]];
(side group _unit) isEqualTo GVAR(defenderSide);
