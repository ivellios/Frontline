/*
    Function:       FRL_SearchDestroy_fnc_serverInit
    Author:         Adanteh
    Description:    SD base server nit
*/
#include "macros.hpp"

GVAR(sdNamespace) = true call MFUNC(createNamespace);
GVAR(cacheNamespace) = false call MFUNC(createNamespace);

GVAR(ServerInitDone) = false;
GVAR(cacheIndex) = 0; // Number for total caches
GVAR(setIndex) = 0; // Number for current active objective set
GVAR(setPositionCached) = []; // Prepared cache positions
GVAR(lastPosition) = [-500, -500, 0]; // Position of previous created cache position

[QGVAR(Settings), configFile >> "FRL" >> "CfgSearchDestroy"] call MFUNC(cfgSettingLoad);
[QGVAR(Settings), missionConfigFile >> "FRL" >> "CfgSearchDestroy"] call MFUNC(cfgSettingLoad);

GVAR(attackerSide) = [[QGVAR(Settings_attackerSide), "west"] call MFUNC(cfgSetting)] call MFUNC(getSideFromString); // Global variable for which team is attacking
GVAR(defenderSide) = [[QGVAR(Settings_defenderSide), "east"] call MFUNC(cfgSetting)] call MFUNC(getSideFromString); // Global variable for which team is defending

publicVariable QGVAR(sdNamespace);
publicVariable QGVAR(defenderSide);
publicVariable QGVAR(attackerSide);

// -- Description:   Creates a objective, with multiple destroyable objectives in a random position
[QGVAR(createObjective), { _this call FUNC(createObjective) }] call CFUNC(addEventHandler);

// -- Description:   Single cache is destroyed, update marker, check if all objects for it are destroyed
[QGVAR(cacheDestroyed), {
    (_this select 0) params ["_cache"];
    private _set = _cache getVariable ["set", -1];
    if (_set isEqualTo -1) exitWith { };

    private _objects = GVAR(sdNamespace) getVariable [format ["objects_%1", _set], []];
    private _allDestroyed = ({ !(_x getVariable ["destroyed", false]) } count _objects) == 0;
    if (_allDestroyed) then {
        [QGVAR(objectiveSetDestroyed), [_set]] call CFUNC(localEvent);
        [QGVAR(createObjective)] call CFUNC(localEvent);
    };
    [QGVAR(updateCacheMarker)] call CFUNC(globalEvent);
}] call CFUNC(addEventHandler);

// -- Create a temporary spawn location on the new objective set for the defender team
[QGVAR(objectiveCreated), { _this call FUNC(createDefenderSpawn) }] call CFUNC(addEventHandler);

// -- Remove the spawnpoint
[QGVAR(objectiveSetDestroyed), {
    (_this select 0) params ["_set"];
    private _spawnpoint = GVAR(sdNamespace) getVariable [format ["spawnpoint_%1", _set], objNull];
    [_spawnpoint] call EFUNC(rolesspawns,spawnRemove);

}] call CFUNC(addEventHandler);

["missionStarted", {
    [{
        // # Create base sectors
        private _sectors = "true" configClasses (missionConfigFile >> QUOTE(PREFIX) >> "CfgSectors");
        {
            private _sector = _x;
            [true, [configName _sector, _x, getText (_x >> "designator")]] call MFUNC(sectorCreate);
        } forEach _sectors;

        "frl_ao" setMarkerAlpha 0;


        [QGVAR(createObjective)] call CFUNC(localEvent);
        GVAR(ServerInitDone) = true;
        publicVariable QGVAR(ServerInitDone);

        ["gamemodeStarted"] call CFUNC(localEvent);
        [{ _this call FUNC(bombCookoffHandle) }] call MFUNC(addPerFrameHandler);

    }, 2.5, []] call CFUNC(wait);
}] call CFUNC(addEventhandler);


// -- Prevents spawning of AI if too far from the frontline
["ai_canSpawn", {
    params ["_spawnpoint", "_position", "_distanceFilter", "_type"];

    if ((_spawnpoint getVariable ["availableFor", sideUnknown]) isEqualTo GVAR(attackerSide)) then {
        // Attacker spawn near attack marker
        GVAR(intelMarker) params ["_markerPos", "_markerSize"];
        ((_position distance2D _markerPos) < (_markerSize + _distanceFilter))
    } else {
        // Defenders spawn near cache
        (_position distance2D GVAR(lastPosition) < _distanceFilter)
    };
}] call MFUNC(addCondition);


// -- automagic AI pathfinding, send them to a position within the current  intel marker
["AI_waypointPosition", { _this call FUNC(getAIWaypointPosition); }] call MFUNC(addCondition);
