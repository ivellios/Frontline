/*
    Function:       FRL_SearchDestroy_fnc_initAdmin
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

if (hasInterface) then {
    ['add', ["admin", 'Admin Settings', QUOTE(MODULE), [
        'Destroy caches',
        'Destroys current set of caches',
        QMVAR(CtrlButton),
        true,
        { [Clib_Player] call MFUNC(isAdmin); },
        {
            private _caches = GVAR(sdNamespace) getVariable [format ["objects_%1", GVAR(setIndex)], []];
            { [_x] call FUNC(cacheDestroy) } forEach _caches;
        },
        ["DESTROY"]
    ]]] call MFUNC(settingsWindow);


    ['add', ["admin", 'Admin Settings', QUOTE(MODULE), [
        'Give 20 Intel',
        'Gives 20 intel for free',
        QMVAR(CtrlButton),
        true,
        { [Clib_Player] call MFUNC(isAdmin); },
        { [20] call FUNC(changeIntel) },
        ["GIVE INTEL"]
    ]]] call MFUNC(settingsWindow);

    ['add', ["admin", 'Admin Settings', QUOTE(MODULE), [
        'New Objectives',
        'Creates new set of caches',
        QMVAR(CtrlButton),
        true,
        { [Clib_Player] call MFUNC(isAdmin); },
        { [] call FUNC(createObjective) },
        ["CREATE"]
    ]]] call MFUNC(settingsWindow);
};
