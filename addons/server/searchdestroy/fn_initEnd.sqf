/*
    Function:       FRL_SearchDestroy_fnc_initEnd
    Author:         Adanteh
    Description:    End the mission
*/
#include "macros.hpp"


["endVote", {
    (_this select 0) params ["_reason", "_losingTeam"];
    private _winner = if (isNil "_losingTeam") then {
        nil;
    } else {
        !(playerSide isEqualTo _losingTeam)
    };
    [_reason, _winner] call FUNC(endMission);
}] call CFUNC(addEventHandler);

[QSVAR(endMissionAdmin), {
    (_this select 0) params ["_caller"];
    ["Mission ended by admin", "warning"] call MFUNC(notificationShow);
    [{ ["Mission ended by admin", nil, "In this case the winner is decided by who has the most tickets. You lose a ticket when dying and holding less than 50% of the map will cost your team tickets"] call FUNC(endMission)}, 5] call CFUNC(wait);
}] call CFUNC(addEventHandler);

["durationEnd", {
    ["Mission time has passed", nil, "In this case the winner is decided by who has the most tickets. You lose a ticket when dying and holding less than 50% of the map will cost your team tickets"] call FUNC(endMission);
}] call CFUNC(addEventHandler);

["victoryPointsReached", {
    ["Victory points reached", nil] call FUNC(endMission);
}] call CFUNC(addEventHandler);
