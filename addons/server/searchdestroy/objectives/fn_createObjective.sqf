/*
    Function:       FRL_SearchDestroy_fnc_createObjective
    Author:         Adanteh
    Description:    Creates a set of objectives, and prepares the markers
    Locality:       Server only
*/
#include "macros.hpp"

if !(isServer) exitWith {
    [[], QFUNC(createObjective), 2] call CFUNC(remoteExec);
};

private _newObjectiveSet = call FUNC(getNewPosition);
if (isNil "_newObjectiveSet") exitWith { false };
_newObjectiveSet params ["_buildingSet", "_centerPosition"];

GVAR(setIndex) = GVAR(setIndex) + 1;
publicVariable QGVAR(setIndex);

private _caches = [];
{
    private _cache = [_x, _forEachIndex] call FUNC(createCache);
    _caches pushBack _cache;
} forEach _buildingSet;

GVAR(sdNamespace) setVariable [(format ["objects_%1", GVAR(setIndex)]), _caches, true];
[QGVAR(objectiveCreated), _newObjectiveSet] call CFUNC(globalEvent);
[QGVAR(updateCacheMarker)] call CFUNC(globalEvent);
