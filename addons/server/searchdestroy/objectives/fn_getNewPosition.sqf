/*
    Function:       FRL_SearchDestroy_fnc_getNewPosition
    Author:         Adanteh
    Description:    Gets a random building, not too close to a recent set of objectives and make sure there are 3 suitable houses to place a crate in
    Locality:       Server only
*/
#include "macros.hpp"

/*
    This will look for a list of building types, that we know we can fit a cache in properly.
    Then it checks if the position has two extra suitable buildings nearby, because a 'cache' consist of 3 seperately placed boxes with some spacing in between
    Then it makes sure to select one of the possible locations, which is not too close to the previous set
*/


if (GVAR(setPositionCached) isEqualTo []) then {
    private _maxSpacing = [QGVAR(Settings_cacheSpreadMax), 60] call MFUNC(cfgSetting);
    private _minSpacing = [QGVAR(Settings_cacheSpreadMin), 10] call MFUNC(cfgSetting);

    private _suitableBuildingTypes = (call FUNC(processBuildingList) apply { _x select 0 });
    private _suitableBuildingTypes = _suitableBuildingTypes arrayIntersect _suitableBuildingTypes;
    private _area = "frl_ao";
    private _suitableHouses = (nearestObjects [(markerPos _area), _suitableBuildingTypes, selectMax (markerSize _area), true]) select { _x inArea _area };


    // -- BIS RNG leans way heavy on the front, so shuffle and split it a bunch to get a better result.
    private _cnt = count _suitableHouses;
    for "_i" from 1 to 3 do {
        for "_j" from 1 to _cnt do {
        	_suitableHouses pushBack (_suitableHouses deleteAt floor random _cnt);
        };

        // -- Split it somewhere in the first half and put it back at the end
        private _randomSelect = floor random (40 min _cnt);
        private _firstHalf = [];
        for "_h" from 1 to _randomSelect do {
            _firstHalf pushBack (_suitableHouses deleteAt 0);
        };
        _suitableHouses append _firstHalf;
    };

    /*#ifdef DEBUGMODE
    {
        if (_forEachIndex < 10) then {
            private _marker = createMarkerLocal ["uwot" + str _forEachIndex, getPosATL _x];
            _marker setMarkerShapeLocal "ICON";
            _marker setMarkerTypeLocal "mil_dot";
            _marker setMarkerSizeLocal [0.5, 0.5];
            _marker setMarkerTextLocal format ["#%1", _forEachIndex];
            _marker setMarkerColorLocal "ColorRed";
        };
    } forEach _suitableHouses;
    #endif*/

    private _suitableLocations = 0;
    private _buildingSets = [];
    private _cacheAmount = ([QGVAR(Settings_cacheAmount), 3] call MFUNC(cfgSetting)) max 1;
    // -- Filter for locations that have 2 extra suitable locations nearby, inside _maxSpacing and outside _minSpacing
    while { (_suitableLocations < 20) && (count _suitableHouses != 0) } do {
        private _locationToCheck = _suitableHouses deleteAt 0;
        private _nearbyHouses = ((nearestObjects [_locationToCheck, _suitableBuildingTypes, _maxSpacing, true]) - [_locationToCheck]);
        private _filteredNearby = _nearbyHouses select { _x distance2D _locationToCheck > _minSpacing };

        if (count _filteredNearby > (_cacheAmount - 1)) then { // Look for this many extra caches
            _nearbyHouses = _filteredNearby;
        };

        if (count _nearbyHouses > (_cacheAmount - 1)) then {
            private _set = [_locationToCheck];
            if (_cacheAmount >= 2) then {
                for "_i" from 1 to (_cacheAmount - 1) do {
                    _set pushBack (selectRandom (_nearbyHouses - _set));
                };
            };
            _suitableHouses = _suitableHouses - _set;
            _buildingSets pushBack _set;
            _suitableLocations = _suitableLocations + 1;
        };
    };

    // -- No suitable location found
    if (_suitableLocations == 0) exitWith { nil };
    GVAR(setPositionCached) = _buildingSets call BIS_fnc_arrayShuffle;
};

if (GVAR(setPositionCached) isEqualTo []) exitWith { nil };

private _minDistance = [QGVAR(Settings_objectiveSpacing), 300] call MFUNC(cfgSetting);
private _filteredSets = GVAR(setPositionCached) select { ((_x select 0) distance2D GVAR(lastPosition)) >= _minDistance };
private _buildingSet = _filteredSets param [0, GVAR(setPositionCached) select 0];
GVAR(setPositionCached) deleteAt (GVAR(setPositionCached) find _buildingSet);

private _centerPosition = [0, 0, 0];
{ _centerPosition = _centerPosition vectorAdd (getPos _x); nil } count _buildingSet;
_centerPosition = _centerPosition vectorMultiply (1 / (count _buildingSet));

GVAR(lastPosition) = _centerPosition;

/*
#ifdef DEBUGMODE
private _marker = createMarker [QGVAR(set_) + str GVAR(setIndex), _centerPosition];
_marker setMarkerShape "ICON";
_marker setMarkerType "Mil_dot";
_marker setMarkerColor "ColorBlue";
_marker setMarkerText format ["#%1", GVAR(setIndex)];
#endif
*/


[_buildingSet, _centerPosition];
