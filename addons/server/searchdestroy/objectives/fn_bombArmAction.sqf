/*
    Function:       FRL_SearchDestroy_fnc_bombArmAction
    Author:         Adanteh
    Description:    Handles destroyign FO
*/
#include "macros.hpp"


params ["_mode", "_args"];

private _return = true;
switch (toLower _mode) do {
    //
    //
    // Called every frame while the hold action button is still pressed, return should be true to keep action going, false to exit it
    case "callback": {
        _args params ["_caller", "_target", "_menuTarget", "_arming"];

        _return = (alive _caller && { !([_caller] call MFUNC(isUnconscious)) });

        if !(_target isEqualTo (call _menuTarget)) exitWith { // -- Not aiming at vehicle anymore
            _return = false;
        };

        if (_target getVariable ["destroyed", false]) exitWith {
            ["showNotification", ["Cache is already destroyed", "nope"]] call CFUNC(localEvent);
            _return = false;
        };


        if (_arming) then {
            if (_target getVariable ["bombIsArmed", false]) then {
                ["showNotification", ["Bomb is already armed", "nope"]] call CFUNC(localEvent);
                _return = false;
            };
        } else {
            if !(_target getVariable ["bombIsArmed", false]) then {
                ["showNotification", ["No bomb to defuse", "nope"]] call CFUNC(localEvent);
                _return = false;
            };
        };

    };


    //
    //
    // Condition run once at the start
    case "condition": {
        _args params ["_caller", "_target", "_menuTarget", "_arming"];

    };


    //
    //
    // Called when we press the button from the interact menu
    case "start": {
        _args params ["_caller", "_target", "_targetRealtime"];

        private _arming = [] call FUNC(isAttacker);
        private _duration = if (_arming) then {
            [QGVAR(Settings_bombArmTime), 10] call MFUNC(cfgSetting);
        } else {
            [QGVAR(Settings_bombDefuseTime), 10] call MFUNC(cfgSetting);
        };

        [
            "Destroy",
            format ["%1\code_rolesspawns\data\demolish.paa", QUOTE(BASEPATH)],
            _duration,
            [_caller, _target, _targetRealtime, _arming],
            { ["condition", _this] call FUNC(bombArmAction) },
            { ["callback", _this] call FUNC(bombArmAction) },
            { ["end", _this] call FUNC(bombArmAction) },
            true,
            true,
            10,
            _dikButton // -- This is passed from menuUseActionKey
        ] call MFUNC(holdActionStart);
    };


    //
    //
    // Called when we press the button from the interact menu
    // ['end', [[player, cursorobject, cursorobject, true], 0]] call frl_searchdestroy_fnc_bombArmAction
    case "end": {
        _args params ["_data", "_exitCode"];
        if (_exitCode isEqualTo 0) then {
            _data params ["_caller", "_target", "_menuTarget", "_arming"];
            if (_arming) then {
                private _bombCookoffTime = [QGVAR(Settings_bombCookofftime), 45] call MFUNC(cfgSetting);
                private _oldArmedTime = _target getvariable ["bombCookoffAt", 0];
                _target setVariable ["BombIsArmed", true, true];
                _target setVariable ["bombCookoffAt", serverTime + (_bombCookoffTime - _oldArmedTime), true];

                ["showNotification", ["Bomb planted", "ok"]] call CFUNC(localEvent);
            } else { // When defusing, we save the time the bomb was armed, for a shorter cookoff time next arm
                _target setVariable ["BombIsArmed", false, true];
                private _timeArmed = (_target getVariable ["bombCookoffAt", serverTime]) - serverTime;
                private _oldArmedTime = _target setVariable ["bombCookoffAt", _timeArmed, true];

                ["showNotification", ["Bomb defused", "ok"]] call CFUNC(localEvent);
            };

            [_target, _arming] call FUNC(bombSirenToggle);
        };
    };
};

_return;
