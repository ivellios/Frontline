/*
    Function:       FRL_SearchDestroy_fnc_bombCookoffHandle
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

[GVAR(cacheNamespace), {
    if !(_value getVariable ["destroyed", false]) then {
        if (_value getVariable ["BombIsArmed", false]) then {
            if (serverTime >= (_value getVariable ["bombCookoffAt", serverTime])) then {
                [_value] call FUNC(cacheDestroy);
            };
        };
    };
}] call MFUNC(forEachVariable);
