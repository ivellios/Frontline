/*
    Function:       FRL_SearchDestroy_fnc_updateCacheMarker
    Author:         Adanteh
    Description:    Handles the cache markers
*/
#include "macros.hpp"

private _graphics = [];
private _defending = [] call FUNC(isDefender);

for "_i" from 1 to GVAR(setIndex) do {
    private _objects = GVAR(sdNamespace) getVariable [format ["objects_%1", _i], []];
    {
        private _destroyed = _x getVariable ["destroyed", false];
        private _disabled = _x getVariable ["disabled", false];
        if (_destroyed || _defending) then {
            private _icon = [(MEDIAPATH + "icons\fob_ca.paa"), (MEDIAPATH + "icons\fob_destroy_ca.paa")] select _destroyed;
            private _indexInSet = _x getVariable ["index", 1];
            private _text = format [["CACHE #%1", "DESTROYED #%1"] select _destroyed, _indexInSet + 1];
            private _color = [[1, 1, 1, 1], [0.5, 0, 0, 0.7]] select _destroyed;
            _graphics pushBack ["ICON", _icon, _color, getPosASL _x, 25, 25, 0, _text, 1, 0.06];
        };
    } forEach _objects;
};

if !(_graphics isEqualTo []) then {
    [QGVAR(cacheMarkers), _graphics] call CFUNC(addMapGraphicsGroup);
} else {
    [QGVAR(cacheMarkers)] call CFUNC(removeMapGraphicsGroup);
};
