/*
    Function:       FRL_SearchDedstroy_fnc_createCache
    Author:         Adanteh
    Description:    Create the cache object and set some needed variables
    Locality:       Server only
*/
#include "macros.hpp"

params ["_building", ["_indexInSet", 0]];
private _buildingPos = getPosATL _building;
private _posAGL = [_building] call FUNC(getPosInBuilding);
private _cacheType = [GVAR(defenderSide), "cachetype", "Box_CSAT_Uniforms_F"] call MFUNC(getSideData);
private _cache = createVehicle [_cacheType, _posAGL, [], 0, "CAN_COLLIDE"];

_cache setVariable ["index", _indexInSet, true];
_cache setVariable ["set", GVAR(setIndex)];
_cache setVariable ["building", _building];

_cache setPosASL (AGLtoASL _posAGL);
_cache setDir (getDir _building);

clearMagazineCargoGlobal _cache;
clearWeaponCargoGlobal _cache;
clearItemCargoGlobal _cache;
clearBackpackCargoGLobal _cache;
_cache lock true;
_cache enableSimulationGlobal false;
_cache allowDamage false;

if ([QGVAR(Settings_preventBuildingDamage), 1] call MFUNC(cfgSetting) >= 1) then {
    _building allowDamage false;
};

GVAR(cacheIndex) = GVAR(cacheIndex) + 1;
GVAR(cacheNamespace) setVariable [str GVAR(cacheIndex), _cache];
_cache setVariable [QSVAR(interactMenuCategory), QGVAR(cache), true]; // -- Interact options

#ifdef DEBUGFULL
[["FRL_SearchDedstroy_fnc_createCache", format ["cache created at %1", getPosATL _cache]], "red"] call MFUNC(debugMessage);
#endif

_cache;
