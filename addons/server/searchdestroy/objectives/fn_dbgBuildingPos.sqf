/*
    Function:       FRL_SearchDestroy_fnc_dbgBuildingPos
    Author:         Adanteh
    Description:    Standalone function to indicate building positions in 3D, for adding new building types to possible cache list
    Example         [] call FRL_SearchDestroy_fnc_dbgBuildingPos
*/
#include "macros.hpp"

params [["_object", cursorTarget]];
_currentPositions = missionNameSpace getVariable [QGVAR(dbgPosShown), []];
{
    removeMissionEventHandler ["Draw3D", (_x select 0)];
    deleteVehicle (_x select 1);
} forEach _currentPositions;
_currentPositions = [];

if (isNull _object) then {
    _object = nearestObjects [(getPos player), ["building", "StaticShip"], 25, true] param [0, objNull];
};

_buildingPositions = _object buildingPos -1;
_total = count _buildingPositions;

{
    _code = format ["drawIcon3D ['', [0, 0, 1, 1], %1, 0, 0.25, 0, '%2', 1, 0.05, 'PuristaMedium'];", _x, _forEachIndex];
    _id = addMissionEventHandler ["Draw3D", _code];
    _circle = "Sign_Sphere10cm_F" createVehicle [0, 0, 0];
    _circle setPos _x;
    _currentPositions pushback [_id, _circle];
} forEach _buildingPositions;

GVAR(dbgPosShown) = _currentPositions;

// If no building positions like this, you can save directg position like so
// cursorobject worldToModel (getPosATL player) apply { round (_x * 100) / 100 }

_currentPositions
