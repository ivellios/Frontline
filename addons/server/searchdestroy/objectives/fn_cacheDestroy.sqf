/*
    Function:       FRL_SearchDestroy_fnc_cacheDestroy
    Author:         Adanteh
    Description:    Destroys given cache
    Example:        [cursorobject] call FRL_SearchDestroy_fnc_cacheDestroy
*/
#include "macros.hpp"

params ["_cache"];

if (isNull _cache) exitWith { };

_cache setVariable ["BombIsArmed", false, true];
[_cache, false] call FUNC(bombSirenToggle);

if (_cache getVariable ["destroyed", false]) exitWith { };

_cache setVariable ["destroyed", true, true];

// -- Create some thermite effect
_pos = getPosATL _cache;
_pos set [2, (_pos select 2) + 0.4];
_destructEffect = QSVAR(FO_Explosion) createVehicle _pos;
_destructEffect setPosATL _pos;

[QGVAR(cacheDestroyed), [_cache]] call CFUNC(globalEvent);
