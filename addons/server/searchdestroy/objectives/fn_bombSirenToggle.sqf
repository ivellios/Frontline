/*
    Function:       FRL_SearchDestroy_fnc_bombSirenToggle
    Author:         Adanteh
    Description:    Handles destroyign FO
*/
#include "macros.hpp"

params ["_target", "_arming"];

if (_arming) then {
    private _pos = ASLtoAGL (getPosASL _target);
    private _source = createSoundSource ["Sound_Alarm", _pos, [], 0];
    _target setVariable ["soundSource", _source, true];
} else {
    private _source = _target getVariable ["soundSource", objNull];
    if !(isNull _source) then {
        deleteVehicle _source;
    };
};
