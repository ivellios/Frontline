/*
    Function:       FRL_SearchDestroy_fnc_createObjectiveMarkers
    Author:         Adanteh
    Description:    Creates a set of objectives, and prepares the markers
*/
#include "macros.hpp"
#define LOOPLIMIT 100

(_this select 0) params ["_buildingSet", "_centerPosition"];
private _markerCount = count GVAR(intelStages);
private _markerArray = [];
_centerPosition params ["_xPos", "_yPos"];

private _distance = 0;
for "_i" from 0 to (count GVAR(markerSize) - 1) do {
    private _thisRadius = GVAR(markerSize) select _i;
    private _previousRadius = GVAR(markerSize) param [(_i - 1), _thisRadius];
    private _offsetBase = (_thisRadius - _previousRadius);

    private _i = 0;
    while { true } do { // -- Check for new position, make sure the center position is within the map though
        private _offset = (_offsetBase * 0.75) + (random (_offsetBase * 0.25)); // -- Because BIS RNGesus has a habit of coming out low, we always add half
        private _angle = ((random 120) + (random 120) + (random 120));
        private _pos = [_xPos + (_offset * sin _angle), _yPos + (_offset * cos _angle), 0];
        if ([_pos, _thisRadius * 0.5] call MFUNC(posIsInTerrain) || (_i > LOOPLIMIT)) exitWith {
            _markerArray pushBack _pos;  // -- Order backwards
            _xPos = _pos select 0;
            _yPos = _pos select 1;
        };
        _i = _i + 1;
    };

};

reverse _markerArray;

GVAR(markerArray) = _markerArray;
GVAR(intelStage) = 0;
GVAR(intelValue) = 0;
{ publicVariable _x } forEach [QGVAR(intelStage), QGVAR(intelValue)];

[QGVAR(intelStageChanged), [0]] call CFUNC(globalEvent);
