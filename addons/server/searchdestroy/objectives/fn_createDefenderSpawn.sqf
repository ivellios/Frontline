/*
    Function:       FRL_SearchDestroy_fnc_createDefender spawn
    Author:         Adanteh
    Description:    This creates a randomzied spawn area for the defending team, for a new objective
*/
#include "macros.hpp"

(_this select 0) params ["_buildingSet", "_centerPosition"];

private _tunnelTime = [QGVAR(Settings_tunnelTime), -1] call MFUNC(cfgSetting);
private _tunnelSize = [QGVAR(Settings_tunnelSize), 50] call MFUNC(cfgSetting);

private _markerName = format [QGVAR(defenderSpawn_%1), GVAR(setIndex)];
private _marker = createMarker [_markerName, _centerPosition];
_marker setMarkerShape "ELLIPSE";
_marker setMarkerSize [_tunnelSize, _tunnelSize];

private _spawnName = format ["Tunnel #%1", GVAR(setIndex)];
private _spawnpoint = [_marker, GVAR(defenderSide), _spawnName, _tunnelTime, ""] call EFUNC(rolesspawns,spawnForwardCreate);
