/*
    Function:       FRL_SearchDestroy_fnc_getPosInBuilding
    Author:         Adanteh
    Description:    Gets the actual position within a buildling
*/
#include "macros.hpp"

params ["_building"];
private _buildingType = typeOf _building;
private _posAGL = getPosASL _building;
private _buildingData = call FUNC(processBuildingList);

{
    _x params ["_type", "_index"];
    if (_buildingType == _type) exitWith {
        if (_index isEqualType []) then {
            _posAGL = _building modelToWorld _index;
        } else {
            _posAGL = _building buildingPos _index;
        };
    };
    nil;
} count _buildingData;
_posAGL;
