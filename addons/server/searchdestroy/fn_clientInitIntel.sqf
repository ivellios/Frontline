/*
    Function:       FRL_SearchDestroy_fnc_clientInitIntel
    Author:         Adanteh
    Description:    Does client-side intel stuff
*/
#include "macros.hpp"

if (isNil QGVAR(intelMarker)) then { GVAR(intelMarker) = []; };

// -- Shows the intel bar
[QGVAR(initAO), {
    ["progressHudShow", [
        QGVAR(intelBar),
        {
            (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\request_base_ca.paa");
            (_controlsGroup controlsGroupCtrl 3) ctrlSetText "Intel";
            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.995, 0.714, 0.208, 1];
        },
        {
            private _intelProgress = missionNamespace getVariable [QGVAR(intelValue), 0];
            private _text = if (GVAR(intelStage) == (count GVAR(intelStages))) then {
                "Max intel";
            } else {
                private _intelForNextStage = GVAR(intelStages) select GVAR(intelStage);
                format ["Intel %1/%2", round _intelProgress, _intelForNextStage];
            };
            private _intelForLastStage = [GVAR(intelStages)] call MFUNC(arrayPeek);

            (_controlsGroup controlsGroupCtrl 3) ctrlSetText _text;
            (_controlsGroup controlsGroupCtrl 5) progressSetPosition ((_intelProgress / _intelForLastStage) min 1);
        },
    []]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

// -- Gives a notifcation when the AO marker shrinks down
[QGVAR(intelStageChanged), {
    (_this select 0) params ["_stage"];
    if (_stage != 0) then {
        if !([] call FUNC(isDefender)) then {
            ["showNotification", ["Cache position was narrowed down!"]] call CFUNC(localEvent);
        };
    };
}] call CFUNC(addEventHandler);

// -- This is a circle that indicates the known area which shrinks when intel groes
[QGVAR(updateIntelMarker), {
    private _data = +(GVAR(intelMarker));

    if (_data isEqualTo []) exitWith {
        [QGVAR(intelMarker)] call CFUNC(removeMapGraphicsGroup);
    };

    _data params ["_position", "_size"];
    private _graphics = [];
    if ([] call FUNC(isDefender)) then {
        _graphics pushBack ["ELLIPSE", _position, _size, _size, 0, [0.5, 0, 0, 1], "", {}]; // -- No fill for defending team
    } else {
        _graphics pushBack ["ELLIPSE", _position, _size, _size, 0, [1, 1, 1, 1], "#(rgb,8,8,3)color(0.5,0,0,0.15)", {}]; // -- fill for attacking team
        _graphics pushBack ["ELLIPSE", _position, _size + 1.0, _size + 1.0, 0, [0.5, 0, 0, 1], "", {}];
        _graphics pushBack ["ELLIPSE", _position, _size + 1.5, _size + 1.5, 0, [0.5, 0, 0, 1], "", {}];
        _graphics pushBack ["ELLIPSE", _position, _size + 2.0, _size + 2.0, 0, [0.5, 0, 0, 1], "", {}];
    };
    [QGVAR(intelMarker), _graphics] call CFUNC(addMapGraphicsGroup);
}] call CFUNC(addEventHandler);

["playerSideChanged", { [QGVAR(updateIntelMarker)] call CFUNC(localEvent); }] call CFUNC(addEventHandler);
