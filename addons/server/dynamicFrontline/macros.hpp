#define MODULE dynamicFrontline

#include "..\macros.hpp"

#define DEBUGMODE true

#define pixelScale  0.25
#define GRID_W (pixelW * pixelGridNoUIScale * pixelScale)
#define GRID_H (pixelH * pixelGridNoUIScale * pixelScale)
#define CENTER_X  ((getResolution select 2) * 0.5 * pixelW)
#define CENTER_Y  ((getResolution select 3) * 0.5 * pixelH)
#define pxW(x) (x * GRID_W)
#define pxH(x) (x * GRID_H)
