/*
    Function:       FRL_DynamicFrontline_fnc_serverInitDeployment
    Author:         Adanteh
    Description:    Inits the deployment mechanics for DF
*/
#include "macros.hpp"

// -- Blocks FOs when they get cut off (on an island) or end up in enemy territory (With exception of enemy islands)
["fo_SpawnBlock", {
    params ["_spawnpoint", "_availableFor", "_pos"];
    _blocked = false;
    if ([_pos, _availableFor] call FUNC(posInFriendly)) then {
        _blocked = [_pos] call FUNC(posIsIsland);
        if (_blocked) then {
            _spawnblock_return = "On cut off territory";
        };
    } else {
        _blocked = !([_pos] call FUNC(posIsIsland));
        if (_blocked) then {
            _spawnblock_return = "In enemy territory";
        };
    };
    _blocked
}] call MFUNC(addCondition);

["forward_SpawnBlock", {
    params ["_spawnpoint", "_availableFor", "_pos"];
    private _blocked = !([_pos, _availableFor] call FUNC(posInFriendly));
    if (_blocked) then {
        _spawnblock_return = "In enemy territory";
    };
    _blocked
}] call MFUNC(addCondition);

["vehicle_SpawnBlock", {
    params ["_spawnpoint", "_availableFor"];
    private _vehicle = _spawnpoint getVariable ["vehicle", objNull];
    private _bypassSpawnLimitLevel = _vehicle getVariable [QSVAR(aoBypass), 0];
    private _blocked = !([getPosASL _vehicle, _availableFor] call FUNC(posInFriendly));
    if (_blocked) then { // -- aoBypass 2 allows ALWAYS spawning, 1 allows spawning every but enemy
        _spawnblock_return = "In enemy territory";
        if (_bypassSpawnLimitLevel >= 2) exitWith { _blocked = false };
        if (_bypassSpawnLimitLevel == 1) then { //  -- Check if it's neutral territory
            _blocked = !([getPosASL _vehicle, sideLogic] call FUNC(posInFriendly));
        };
    };
    _blocked
}] call MFUNC(addCondition);


// -- Prevents spawning of AI if too far from the frontline
["ai_canSpawn", {
    params ["_spawnpoint", "_position", "_distanceFilter", "_type"];
    if (count allPlayers <= 8) exitWith { true };
    private _inArea = [];
    {
        _inArea append (_x inAreaArray [_position, _distanceFilter, _distanceFilter, 0, false, 0]);
    } forEach GVAR(frontlines);
    (count _inArea > 0);
}] call MFUNC(addCondition);


// -- Prevent destruction of FO if further than 100m into friendly territory. Will check on each frontline update if FO is behind lines
if (([QGVAR(preventDestroyRangeFO), 200] call MFUNC(cfgSetting)) != -1) then {

    [QGVAR(drawFrontline), {
        (_this select 0) params ["_frontlines"];

        private _destroyRange = [QGVAR(preventDestroyRangeFO), 200] call MFUNC(cfgSetting);
        private _namespace = missionNamespace getVariable [QEGVAR(rolesspawns,spawnpoints), locationNull];

        [_namespace, {
            params ["_varName", "_spawnpoint"];

            if (_spawnpoint getVariable ["deleted", false]) exitWith { };
            private _type = _spawnpoint getVariable ["type", ""];
            if !(_type isEqualTo "fo") exitWith { };

            private _position = _spawnpoint getVariable ["position", [0, 0, 0]];
            private _availableFor = _spawnpoint getVariable ["availableFor", sideUnknown];
            private _behindLines = call {

                // -- If in enemy territory, always allow decap
                if !([_position, _availableFor] call FUNC(posInFriendly)) exitWith {
                    false
                };

                private _inArea = [];
                {
                    _inArea append (_x inAreaArray [_position, _destroyRange, _destroyRange, 0, false, -1]);
                } forEach (_frontlines select 0);

                (count _inArea == 0)
            };

            if !((_spawnpoint getVariable ["behindLines", false]) isEqualTo _behindLines) then {
                _spawnpoint setVariable ["behindLines", _behindLines, true];
            };

        }] call MFUNC(forEachVariable);


    }] call CFUNC(addEventHandler);
};
