/*
    Function:       FRL_DynamicFrontline_fnc_drawLine
    Author:         Adanteh
    Description:    Draws a thick line from one point to another to indicate Frontline
*/
#include "macros.hpp"

params ["_posFrom", "_posTo", ["_color", [0, 0, 0, 1]]];
_posFrom set [2, 0];
_posTo set [2, 0];

private _dirFrom = _posFrom getdir _posTo;
private _vector =  _posFrom vectordiff _posTo;
private _vector2 = _vector vectorMultiply 0.5;
private _center = _posTo vectorAdd _vector2;
private _distance = _posFrom distance _posTo;

["RECTANGLE", _center, 2.5, _distance/2, _dirFrom, _color, _color];

/*
_marker = createMarkerLocal [format [QGVAR(line_%1), (_posFrom apply { floor _x })], _center];
_marker setMarkerShapeLocal "RECTANGLE";
_marker setMarkerColorLocal "ColorBlack";
_marker setMarkerSizeLocal [2.5, _distance/2];
_marker setMarkerDirLocal _dirFrom;
_marker setmarkerposLocal _center;
_marker setMarkerAlphaLocal 1;
_marker setMarkerBrushLocal "SolidFull";
_marker;
*/
