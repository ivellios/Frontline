/*
    Author:         Adanteh
    Function:       FRL_DynamicFrontline_fnc_createReplay
    Description:    Run a Dynamic Frontline replay from a replay file located in `replays` directory.
                    Arguments:
                    - Filename - the name of the file that is going to be used for replay purposes
                    - FPS - the number of FPS that the replay will be played at
    Example:        ["my_replay_file", 120] call FRL_DynamicFrontline_fnc_createReplay
*/
#include "macros.hpp"

// -- THIS IS FOR REPLAY PURPOSES ONLY AND SHOULD BE MANUALLY CALLED
params ["_filename", ["_fps", 30]];

// -- Do AI Shit
[QEGVAR(ai,settings_enabled), 0] call MFUNC(cfgSettingSet);
call EFUNC(ai,resetAI);

// -- Increase the update speed for a faster timelapse
[QGVAR(positionsUpdateSpeed), 1/_fps] call MFUNC(cfgSettingSet);
[QGVAR(frontlineUpdateSpeed), 1/_fps] call MFUNC(cfgSettingSet);

{
    private _handle = _x;
    private _array = MVAR(perFrameHandleArray) findIf { (_x select 5) == _handle };
    if (_array != -1) then {
        private _pfh = MVAR(perFrameHandleArray) select _array;
        _pfh set [1, 1 / _fps];
    };
} forEach [GVAR(pfh1), GVAR(pfh2)];

[format ["%1.run_replay", GVAR(pythonDir)], [_filename]] call py3_fnc_callExtension;
