/*
    Function:       FRL_DynamicFrontline_fnc_posIsIsland
    Author:         Adanteh
    Description:    Checks if given position is on an island
    Example:        [getPos player] call FRL_DynamicFrontline_fnc_posIsIsland;
*/
#include "macros.hpp"

params ["_pos", ["_returnCode", { GVAR(posIsIsland) = _this }], ["_varName", QGVAR(posIsIsland)]];
_pos params ["_xPos", "_yPos"];
if (isServer) then {
    private _value = !([format ['%1.is_mainland', GVAR(pythonDir)], [floor _xPos, floor _yPos]] call py3_fnc_callExtension);
    _value call _returnCode;
    _value;
} else {
    ["requestRemoteValue", [_varName,
        compile format ["
            private _value = ([format ['%1.is_mainland', %4], [%2, %3]] call py3_fnc_callExtension);
            if (_value isEqualType []) then { false } else { !_value };
        ", "%1", floor _xPos, floor _yPos, QGVAR(pythonDir)],
    _returnCode]] call CFUNC(localEvent);
};
