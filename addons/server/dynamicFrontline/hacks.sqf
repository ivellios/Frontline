private _oldHtml = (uinamespace getVariable ["frl_df", controlNull]);
if !(isNull _oldHtml) then {
    ctrlDelete _oldHtml;
    [oogabooga] call clib_fnc_removePerFrameHandler;
};

private _url = "http://frontline-mod.com/ctrlhtml/df_01.php?x=100&y=100";
private _html = (findDisplay 46) ctrlCreate ["RscHTML", -1];
_html ctrlSetBackgroundColor [0,0,0,0];
_html ctrlSetPosition [safeZoneX, safeZoneY, safeZoneW, safeZoneH];
_html ctrlCommit 0;
_html htmlLoad _url;
testvar = ctrlHTMLLoaded _html;
oldmapscale = 0;

_resolution = getResolution;
xResolution = _resolution select 0;
yResolution = _resolution select 1;
uiScale = _resolution select 4;

if (testVar) then {
    uinamespace setVariable ["frl_df", _html];
    oogabooga = [{
        private _html = (uinamespace getVariable ["frl_df", controlNull]);
        if (visibleMap) then {
            private _originPos = frl_dynamicfrontline_aoOrigin;
            private _uiPos = (((findDisplay 12) displayCtrl 51) ctrlMapWorldToScreen _originPos) params ["_xPos", "_yPos"];
            private _mapScale = ctrlMapScale ((findDisplay 12) displayCtrl 51);
            if (_mapScale != oldmapscale) then {
                oldMapScale = _mapScale;

                private _mapScaleCoef = (10^(abs log _mapScale));
                private _xSize = round (499 * (_mapScaleCoef / 9.04));
                private _ySize = round (352 * (_mapScaleCoef / 9.04));
                private _url = format ["http://frontline-mod.com/ctrlhtml/df_01.php?x=%1&y=%2", _xSize, _ySize];
                _html htmlLoad _url;
                hint format ["%1\n%2\n%3", _mapScale, _mapScaleCoef, ctrlHTMLLoaded _html];
            };
            _html ctrlSetPosition [_xPos, _yPos];
            _html ctrlSetFade 0.90;
        } else {
            _html ctrlSetFade 1;
        };
        _html ctrlCommit 0;

    }, 0, []] call Clib_fnc_addPerFrameHandler;
};
