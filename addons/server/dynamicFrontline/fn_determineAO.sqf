/*
    Function:       FRL_DynamicFrontline_fnc_determineAO
    Author:         Adanteh
    Description:    Figures out the coordinates for the AO
*/
#include "macros.hpp"


// -- See if we have markers to indicate a polygon

// POLYGON AO. This will loop through markers and make a polygon out of them. It'll also allow skipping up to 5 deleted markers so you don't need to rename everything if you delete a single one
private _corners = [];
private _markerIndex = 0;
private _markersSkipped = 0;
while { _markersSkipped < 10 } do {
    _polyMarker = format ["frl_aoPoly_%1", _markerIndex];
    private _markerPos = (markerPos _polyMarker);
    if (_markerPos isEqualTo [0, 0, 0]) then {
        _markersSkipped = _markersSkipped + 1;
    } else {
        _corners pushBack _markerPos;
        _polyMarker setMarkerAlpha 0;
        _markersSkipped = 0;
    };
    _markerIndex = _markerIndex + 1;
};

// RECTANGLE AO
// -- If the polygon markers are invalid, use a square AO instead
if (count _corners < 3) then {
    private _aoMarker = "frl_ao";
    private _aoPos = getMarkerPos _aoMarker;
    if (_aoPos isEqualTo [0, 0, 0]) exitWith { }; // -- AO marker doesn't exist


    private _aoPos = markerPos _aoMarker;
    private _aoDir = markerDir _aoMarker;

    // -- Get coordinates of all corners
    _aoPos params ["_xPos", "_yPos"];
    (markerSize _aoMarker) params ["_aoWidth", "_aoHeight"];

    private _corners = ([
        [-_aoWidth * cos(_aoDir) - _aoHeight * sin(_aoDir), _aoWidth*sin(_aoDir) - _aoHeight*cos(_aoDir)],
        [-_aoWidth * cos(_aoDir) - -_aoHeight * sin(_aoDir), _aoWidth*sin(_aoDir) - -_aoHeight*cos(_aoDir)],
        [_aoWidth * cos(_aoDir) - -_aoHeight * sin(_aoDir), -_aoWidth*sin(_aoDir) - -_aoHeight*cos(_aoDir)],
        [_aoWidth * cos(_aoDir) - _aoHeight * sin(_aoDir), -_aoWidth*sin(_aoDir) - _aoHeight*cos(_aoDir)]
    ]);

    _corners = _corners apply { [((_x select 0) + _xPos), (_x select 1) + _yPos] };

    _aoMarker setMarkerBrush "Border";
    _aoMarker setMarkerColor "ColorBlack";
};

// -- No valid AO
if (count _corners < 3) exitWith { false };

private _originBottomLeft = [999999, 999999, 0]; // Marks the min X and min Y coordinate (Can be outside the AO!)
private _originTopRight = [0, 0, 0]; // Marks the min X and min Y coordinate (Can be outside the AO!)
{
    _x params ["_xPosCorner", "_yPosCorner"];
    if (_xPosCorner < (_originBottomLeft select 0)) then {
        _originBottomLeft set [0, _xPosCorner];
    };
    if (_yPosCorner < (_originBottomLeft select 1)) then {
        _originBottomLeft set [1, _yPosCorner];
    };
    if (_xPosCorner > (_originTopRight select 0)) then {
        _originTopRight set [0, _xPosCorner];
    };
    if (_yPosCorner > (_originTopRight select 1)) then {
        _originTopRight set [1, _yPosCorner];
    };
} forEach _corners;

private _fullWidth = (_originTopRight select 0) - (_originBottomLeft select 0);
private _fullHeight = (_originTopRight select 1) - (_originBottomLeft select 1);
GVAR(aoOrigin) = _originBottomLeft apply { floor _x };
GVAR(aoSize) = [_fullWidth, _fullHeight];
GVAR(aoCorners) = _corners;
publicVariable QGVAR(aoCorners);
publicVariable QGVAR(aoOrigin);

true;
