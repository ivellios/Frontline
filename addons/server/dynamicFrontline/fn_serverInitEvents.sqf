#include "macros.hpp"

// -- Description:    Creates the square markers to visually backend capture areas
[QGVAR(initAOServer), {

    private _pointSize = GVAR(aoPointsize);

    if !(call FUNC(pythiaTest)) exitWith { }; // -- Error in pythia mod

    if !([] call FUNC(determineAO)) exitWith { }; // No proper AO marker
    private _priorityZones = call FUNC(determineAoPriorityZones);

    GVAR(settings) pushBack ["mission", (missionname + "." + worldName)];

    private _sides = call MFUNC(getSides);
    private _basePosition1 = getMarkerPos (format ["base_%1", _sides select 0]);
    private _basePosition2 = getMarkerPos (format ["base_%1", _sides select 1]);
    _basePosition1 resize 2;
    _basePosition2 resize 2;
    private _corners = +GVAR(aoCorners);
    { _x resize 2; nil } count _corners; // Pythia uses 2D positions, seeing height doesn't matter
    private _squares = [format ["%1.init", GVAR(pythonDir)], [
        _corners,
        _basePosition1, // Blufor base location
        _basePosition2,  // Opfor base location
        _priorityZones,  // zones of higher priority
        GVAR(settings)
    ]] call py3_fnc_callExtension;

    //diag_log ["[FRL DF] - Squares count", count _squares];
    for "_i" from 0 to (count _squares - 1) do {
        (_squares select _i) params ["_iX", "_iY", "_importance"];
        private _pos = [
            (GVAR(aoOrigin) select 0) + (_iX * _pointSize) + (_pointSize / 2),
            (GVAR(aoOrigin) select 1) + (_iY * _pointSize) + (_pointSize / 2)
        ];

        private _squareMarkName = format ["frl_dynamicfrontline_%1_%2", _iX, _iY];
        private _squareMrk = createMarker [_squareMarkName, _pos];
        _squareMrk setMarkerShape "RECTANGLE";
        _squareMrk setMarkerBrush "Solid";
        _squareMrk setMarkerAlpha _importance;
        _squareMrk setMarkerSize [_pointSize / 2, _pointSize / 2];
    };

    GVAR(pythiaLogicStarted) = true;

}] call CFUNC(addEventHandler);


// -- Description:    Updates player positions to pass to the server addon
[QGVAR(updateUnitPositions), {
    private _unitPos = [];
    private _allPlayers = +(allUnits);
    private _sidesInMission = call MFUNC(getSides);

    {
        private _side = (side group _x);
        if (_side in _sidesInMission) then {
            if (["canCapture", _x, true] call MFUNC(checkConditions)) then {
                (getPosASL _x) params ["_xPos", "_yPos"];
                _unitPos pushBack [
                    -1, // Unit ID
                    ([east, west, resistance, civilian] find _side),
                    round _xPos,
                    round _yPos
                ];
            };
        };
        nil;
    } count _allPlayers;
    private _return = [format ["%1.units_location", GVAR(pythonDir)], _unitPos] call py3_fnc_callExtension;
    _return
}] call CFUNC(addEventHandler);


// -- Description:    Retrieves the current frontlines from python system and sends
[QGVAR(updateFrontline), {
    private _frontlines = [format ["%1.get_frontlines", GVAR(pythonDir)]] call py3_fnc_callExtension;
    [QGVAR(drawFrontline), [_frontlines]] call CFUNC(globalEvent);
    GVAR(frontlines) = _frontlines;
}] call CFUNC(addEventHandler);


// -- Description:    Updates the percentage of AO controlled in a side organized array
[QGVAR(updateAOControlled), {
    GVAR(aoPercentages) = [format ["%1.get_ao_controlled", GVAR(pythonDir)]] call py3_fnc_callExtension;
    publicVariable QGVAR(aoPercentages);
}] call CFUNC(addEventHandler);

/*  Description:    Updates a square with new side info
    Locality:       Server          */
[QGVAR(markerUpdate), {
    private _squareStatus = _this select 0;
    {
        _x params ["_iX", "_iY", "_capture"];
        _squareMark = format [QGVAR(%1_%2), _iX, _iY];

        if (abs(_capture) < 2) then {
            _squareMark setMarkerColor "ColorBlack";
        } else {
            if (_capture < 0) then {
                if ((getMarkerColor _squareMark) != "colorEAST") then {
                    _squareMark setMarkerColor "colorEAST";
                };
            } else {
                if (_capture > 0) then {
                    if ((getMarkerColor _squareMark) != "colorWEST") then {
                        _squareMark setMarkerColor "colorWEST";
                    };
                };
            };
        };

        if (abs _capture == 255) then {
            if (markerBrush _squareMark != "FRL_Main_IslandShade2") then {
                _squareMark setMarkerBrush "FRL_Main_IslandShade2";
            };
        } else {
            if (markerBrush _squareMark != "Solid") then {
                _squareMark setMarkerBrush "Solid";
            };
        };
    } count _squareStatus;
}] call CFUNC(addEventHandler);


/*  Description:    Updates squares
    Locality:       Server          */
GVAR(squares) = [];
[QGVAR(squaresUpdate), {
    if (GVAR(squaresUpdating)) exitWith { };
    GVAR(squaresUpdating) = true;
    GVAR(squares) = [format ["%1.get_debug_squares", GVAR(pythonDir)]] call py3_fnc_callExtension;
    [QGVAR(markerUpdate), GVAR(squares)] call CFUNC(localEvent);
    GVAR(squaresUpdating) = false;
}] call CFUNC(addEventHandler);


// -- Description:    Ends calculations, calls mission end and deinits python module
["endMission", {
    GVAR(endMission) = true;
    publicVariable QGVAR(endMission);
    [format ["%1.deinit", GVAR(pythonDir)]] call py3_fnc_callExtension;
}] call CFUNC(addEventHandler);
