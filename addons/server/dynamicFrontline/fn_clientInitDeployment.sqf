/*
    Function:       FRL_DynamicFrontline_fnc_clientInitDeployment
    Author:         Adanteh
    Description:    Inits the deployment mechanics for DF
*/
#include "macros.hpp"


// -- This will get called, then a request to get the proper value from server is made, when value is returned the function will be called again but with the proper data available
GVAR(receivedIslandValue) = false;

["placeFOonCall", {
    params ["_caller", "_pos"];
    private _canPlace = ([_pos, side group _caller] call FUNC(posInFriendly));
    if !(_canPlace) then {
        ["showNotification", ["You can only place FOs on friendly territory"]] call CFUNC(localEvent);
    };
    _canPlace
}] call MFUNC(addCondition);

["placeRPonCall", {
    params ["_caller", "_pos"];
    private _canPlace = ([_pos, side group _caller] call FUNC(posInFriendly));
    if !(_canPlace) then {
        ["showNotification", ["You can only place RPs on friendly territory"]] call CFUNC(localEvent);
    };
    _canPlace
}] call MFUNC(addCondition);


["placeRPonCall", {
    params ["_caller", "_pos"];
    // -- Check friendly position first
    if !(GVAR(receivedIslandValue)) then {
        [_pos,
        {
            GVAR(posIsIsland) = _this;
            GVAR(receivedIslandValue) = true;
            ["place"] call EFUNC(rolesspawns,placeRally);
        }] call FUNC(posIsIsland);
        false;
    } else {
        GVAR(receivedIslandValue) = false;
        if (GVAR(posIsIsland)) exitWith {
            ["You can not place RPs on islands"] call MFUNC(notificationShow);
            false
        };
        true;
    };

}] call MFUNC(addCondition);

["placeFOonCall", {
    params ["_caller", "_pos"];
    // -- Check friendly position first
    if !(GVAR(receivedIslandValue)) then {
        [_pos,
        {
            GVAR(posIsIsland) = _this;
            GVAR(receivedIslandValue) = true;
            ["place"] call EFUNC(rolesspawns,placeFO);
        }] call FUNC(posIsIsland);
        false;
    } else {
        GVAR(receivedIslandValue) = false;
        if (GVAR(posIsIsland)) exitWith {
            ["You can not place FOs on islands"] call MFUNC(notificationShow);
            false
        };
        true;
    };

}] call MFUNC(addCondition);


// -- Prevent destruction of FO if further than 100m into friendly territory
["destroyFOonCall", {
   params ["_side", "_spawnpoint"];
   if (_spawnpoint getVariable ["behindLines", false]) exitWith {
       _canDestroyMessage = "FO is too far behind enemy lines"; //NOPRIVATE
       false;
   };
   true;
}] call MFUNC(addCondition);
