/*
    Function:       FRL_dynamicFrontline_fnc_serverInitBleed;
    Author:         Adanteh
    Description:    Handles bleed
*/
#include "macros.hpp"

GVAR(ticketBleed) = 0;
GVAR(aoPercentages) = [0.5, 0.5];
GVAR(factionOrder) = [east, west];
publicVariable QGVAR(factionOrder);

#define _LOOPTIME (0.5*60) // -- Ticket update speed -- //
#define _BLEEDPER (10*60) // -- This means bleed PER time frame (So 30 tickets on a zone is 30 tickets per 10 minutes)
#define _BLEEDSTRENGTH 5

// -- Do the bleed thing :)
[{
    if (GVAR(endMission)) exitWith { [_this select 1] call MFUNC(removePerFrameHandler); };

    private _controlDifference = 50 - ((GVAR(aoPercentages) select 0) * 100);
    if (_controlDifference != 0) then {
        // -- This means 60% capture, will bleed 2 tickets per minute
        private _aboveFifty = abs _controlDifference + 50;
        private _bleedAmount = switch (true) do {
            //case (_aboveFifty >= 80): { 20 };
            //case (_aboveFifty >= 75): { 18 };
            case (_aboveFifty >= 70): { 17 };
            case (_aboveFifty >= 65): { 15 };
            case (_aboveFifty >= 60): { 14 };
            case (_aboveFifty >= 55): { 11 };
            case (_aboveFifty > 51):  { 8 };
            default { 0 };
        };

        if (_controlDifference > 0) then {
            ["adjustTicket", [_bleedAmount * (_LOOPTIME / _BLEEDPER), 0, true]] call CFUNC(localEvent);
        } else { // -- OPFOR Bleed
            ["adjustTicket", [_bleedAmount * (_LOOPTIME / _BLEEDPER), 1, true]] call CFUNC(localEvent);
        };

        GVAR(ticketBleed) = _bleedAmount;
        publicVariable QGVAR(ticketBleed);
    };
}, _LOOPTIME] call MFUNC(addPerFramehandler);

// -- End the game if (PercentageRequired) percentage is captured for _timeRequired time
GVAR(percentageRequired) = [QGVAR(Settings_percentageToEnd), 0.75] call MFUNC(cfgSetting);
if (GVAR(percentageRequired) > 0) then {
    GVAR(percentageReachedAt) = -1;
    GVAR(winningSide) = sideUnknown;
    [{
        private _timeRequired = [QGVAR(Settings_timeRequired), 90*60] call MFUNC(cfgSetting);
        private _highestControl = selectMax GVAR(aoPercentages);
        if (_highestControl > GVAR(percentageRequired)) then {

            private _winnerSide = GVAR(factionOrder) select (GVAR(aoPercentages) find _highestControl);
            if (_winnerSide == GVAR(winningSide)) then {
                private _timeRequired = [QGVAR(Settings_timeRequired), 10*60] call MFUNC(cfgSetting);
                if (serverTime > (GVAR(percentageReachedAt) + _timeRequired)) then {
                    [(_this select 1)] call MFUNC(removePerFrameHandler);
                    [QGVAR(endMissionPercentage), [_winnerSide]] call CFUNC(globalEvent);
                };
            } else {
                GVAR(winningSide) = _winnerSide;
                GVAR(percentageReachedAt) = serverTime;
                publicVariable QGVAR(percentageReachedAt);
                publicVariable QGVAR(winningSide);
                [QGVAR(percentageVictoryUpdate)] call CFUNC(globalEvent);

            };
        } else {
            if (GVAR(winningSide) != sideUnknown) then {
                GVAR(winningSide) = sideUnknown;
                GVAR(percentageReachedAt) = -1;
                publicVariable QGVAR(percentageReachedAt);
                [QGVAR(percentageVictoryUpdate)] call CFUNC(globalEvent);
            };
        };

    }, 1, []] call MFUNC(addPerFramehandler);
};
