/*
    Function:       FRL_DynamicFrontline_fnc_posInFriendly
    Author:         Adanteh
    Description:    Checks if given position is under control of given side
    Example:        [getPos player, playerSide] call FRL_DynamicFrontline_fnc_posInFriendly;
*/
#include "macros.hpp"

params ["_pos", ["_side", playerside]];

(_pos vectorDiff GVAR(aoOrigin)) params ["_xPos", "_yPos"];
private _markerColor = getMarkerColor (format [QGVAR(%1_%2),
    floor (_xPos / GVAR(aoPointsize)),
    floor (_yPos / GVAR(aoPointsize))
]);

private _colorToCheck = [(format ["color%1", _side]), ""] select (_side isEqualTo sideLogic);
(_markerColor == _colorToCheck);
