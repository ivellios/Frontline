MODULE(dynamicFrontline) {
	dependency[] = {"CLib", "FRL/Main"};

	class drawLine;
	class clientInit;
	class clientInitDeployment;
	class clientInitLimitAO;
    class createReplay;
	class determineAO;
	class determineAoPriorityZones;
	class endMission;
	class initDebug;
	class initEnd;
	class posInFriendly;
	class posIsIsland;
	class pythiaTest;
	class serverInit;
	class serverInitBleed;
	class serverInitEvents;
	class serverInitDeployment;

    class freeze {
        class clientInitFreeze;
        class showFreezeTime;
    };
};
