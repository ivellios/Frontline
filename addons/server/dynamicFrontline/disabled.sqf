// -- Polygon drawing of frontline, pointless till BIS adds filling of polygons
/*
[QGVAR(drawFrontline), {
    (_this select 0) params ["_frontlines"];
    private _cornersOriginal = +(missionNamespace getVariable [QGVAR(aoCorners), []]);
    _cornersOriginal = _cornersOriginal apply { if (count _x == 2) then { _x + [0] } else { _x }};
    {
        private _cornersSide = +_cornersOriginal;
        _cornersSide = [_cornersOriginal select 0];
        _cornersSide append _x;
        _cornersSide pushBack (_cornersOriginal select 1);
        [QGVAR(drawPolygon), [_cornersSide]] call CFUNC(localEvent);
    } forEach _frontlines;
}] call CFUNC(addEventHandler);

[QGVAR(drawPolygon), {
    (_this select 0) params ["_polygon"];
    private _side = west;
    private _color = (getArray (configFile >> "CfgMarkerColors" >> (format ["color%1", _side]) >> "Color")) apply { call compile _x };\
    [(format ["%1_poly", _side]), [["POLYGON", _polygon, _color]], "normal"] call CFUNC(addMapGraphicsGroup);
}] call CFUNC(addEventHandler);
*/



/*
[QGVAR(initAO), {
    _aoMarker = "frl_ao";
    (markerPos _aoMarker) params ["_xPos", "_yPos"];
    (markerSize _aoMarker) params ["_xSize", "_ySize"];
    private _aoDir = markerDir _aoMarker;
	private _sizeOut = 100000;
    private _offset = 500;

	for "_i" from 0 to 270 step 90 do {
		private _size1 = [_xSize,_ySize] select (abs cos _i);
		private _size2 = [_xSize,_ySize] select (abs sin _i);
		private _sizeMarker = [_size2 + (_offset), _sizeOut] select (abs sin _i);
		private _aoDirTemp = _aoDir + _i;
		private _markerPos = [
			_xPos + (sin _aoDirTemp * (_sizeOut + _offset)),
			_yPos + (cos _aoDirTemp * (_sizeOut + _offset))
		];

		private _marker = format [QGVAR(mapCover_%1),_i];
		createmarker [_marker,_markerPos];
		_marker setmarkerpos _markerPos;
		_marker setmarkersize [_sizeMarker,_sizeOut - _size1];
		_marker setmarkerdir _aoDirTemp;
		_marker setmarkershape "rectangle";
		_marker setmarkerbrush "solid";
		_marker setmarkercolor "colorBlack";
	};
}] call CFUNC(AddEventHandler);
*/


[QGVAR(initAO), {
    private _aoMarker = "frl_ao";
    private _aoPos = markerPos _aoMarker;
    _aoPos params ["_xPos", "_yPos"];
    (markerSize _aoMarker) params ["_aoWidth", "_aoHeight"];
    private _originPos = [(_xPos - _aoWidth), (_yPos - _aoHeight)];
    private _pointSize = 50;

    _originPos params ["_xOrigin", "_yOrigin"];

    private _fnc_createSquare = {

        private _squareMrk = createMarkerLocal [format [QGVAR(dbgSquare_%1_%2), _iX, _iY], [
            _xOrigin + (_iX * _pointSize) + (_pointSize / 2),
            _yOrigin + (_iY * _pointSize) + (_pointSize / 2)
        ]];
        private _grid = ((_iX + _iY) mod 2 == 0);
        _squareMrk setMarkerShapeLocal "RECTANGLE";
        _squareMrk setMarkerColorLocal "ColorBLACK";
        _squareMrk setMarkerBrushLocal (["SolidBorder", "Solid"] select _grid);
        _squareMrk setMarkerAlphaLocal ([0.9, 0.7] select _grid);
        _squareMrk setMarkerSizeLocal [_pointSize / 2, _pointSize / 2];
    };

    private _pointsX = ceil ((_aoWidth * 2) / (_pointSize));
    private _pointsY = ceil ((_aoHeight * 2) / (_pointSize));

    for "_iY" from 0 to (_pointsY - 1) do {
        for "_iX" from 0 to (_pointsX - 1) do {
            call _fnc_createSquare;
        };
    };
}] call CFUNC(addEventHandler);


/*  Description:    Updates a square with new side info
    Locality:       Client          */
[QGVAR(dbgUpdateSquaresClient), {
    private _squareStatus = _this select 0;
    {
        _x params ["_iX", "_iY", "_capture"];
        _marker = format [QGVAR(dbgSquare_%1_%2), _iX, _iY];

        if (abs(_capture) < 5) then
        {
            _marker setMarkerAlphaLocal 0.2;
            _marker setMarkerColorLocal "ColorBlack"
        } else {
            _marker setMarkerAlphaLocal (((abs _capture) / 127) + 0.2);
            _marker setMarkerColorLocal (["colorWEST", "colorEAST"] select (_capture < 0));
        };

    } count _squareStatus;
}] call CFUNC(addEventHandler);




// -- Polygon drawing of frontline, pointless till BIS adds filling of polygons
/*
[QGVAR(drawFrontline), {
    (_this select 0) params ["_frontlines"];
    private _cornersOriginal = +(missionNamespace getVariable [QGVAR(aoCorners), []]);
    _cornersOriginal = _cornersOriginal apply { if (count _x == 2) then { _x + [0] } else { _x }};
    {
        private _cornersSide = +_cornersOriginal;
        _cornersSide = [_cornersOriginal select 0];
        _cornersSide append _x;
        _cornersSide pushBack (_cornersOriginal select 1);
        [QGVAR(drawPolygon), [_cornersSide]] call CFUNC(localEvent);
    } forEach _frontlines;
}] call CFUNC(addEventHandler);

[QGVAR(drawPolygon), {
    (_this select 0) params ["_polygon"];
    private _side = west;
    private _color = (getArray (configFile >> "CfgMarkerColors" >> (format ["color%1", _side]) >> "Color")) apply { call compile _x };\
    [(format ["%1_poly", _side]), [["POLYGON", _polygon, _color]], "normal"] call CFUNC(addMapGraphicsGroup);
}] call CFUNC(addEventHandler);
*/
