/*
    Function:       FRL_DynamicFrontline_fnc_clientInitLimitAO
    Author:         Adanteh
    Description:    Keeps keep within the ao
*/
#include "macros.hpp"

// -- Check additional markers that allow you to pass through them

GVAR(aoBypassMarkers) = [];
[QGVAR(initAO), {
    private _markers = [];
    private _markerIndex = 0;
    private _markersSkipped = 0;
    while { _markersSkipped < 10 } do {
        private _bypassMarker = format ["frl_aoBypass_%1", _markerIndex];
        private _markerPos = (markerPos _bypassMarker);
        if (_markerPos isEqualTo [0, 0, 0]) then {
            _markersSkipped = _markersSkipped + 1;
        } else {
            _markers pushBack _bypassMarker;
            _bypassMarker setMarkerBrushLocal "FRL_Main_FShaded2";
            _bypassMarker setMarkerAlphaLocal 0.5;
            _bypassMarker setMarkerColorLocal "ColorBlack";
            _markersSkipped = 0;
        };
        _markerIndex = _markerIndex + 1;
    };
    GVAR(aoBypassMarkers) = _markers;
}] call CFUNC(addEventHandler);


[QGVAR(initAO), {
    GVAR(aoLeft) = -1;
    [{
        private _timeout = [QGVAR(Settings_outsideAOtime), 20] call MFUNC(cfgSetting);
        [{
            (_this select 0) params ["_timeout"];

            private _poly = +GVAR(aoCorners);
            private _canGoOutside = call {
                if (Clib_Player getVariable [QSVAR(tempUnit), false]) exitWith { true };
                if !(alive Clib_Player) exitWith { true; };
                if (vehicle Clib_Player != Clib_Player) then {
                    if ((vehicle Clib_Player) isKindOf "Air") then {
                        true;
                    } else {
                        false
                    };
                };
            };

            if (_canGoOutside) exitWith { };
            if !(getPosASL Clib_Player inPolygon _poly) then {
                if ([] call MFUNC(sectorInBase)) exitWith { GVAR(aoLeft) = -1 }; // -- Not in a base
                if ({ Clib_Player inArea _x } count GVAR(aoBypassMarkers) > 0) exitWith { GVAR(aoLeft) = -1 }; // -- In an additional marker
                if (GVAR(aoLeft) == -1) exitWith { GVAR(aoLeft) = diag_tickTime + _timeout };
                private _timeLeft = GVAR(aoLeft) - diag_tickTime;
                if (_timeLeft > 0) then {
                    ["showNotification", [format ["You have %1 seconds to return to the AO", ceil _timeLeft], "nope", "red"]] call CFUNC(localEvent);
                } else {
                    Clib_Player setDamage 1;
                    GVAR(aoLeft) = -1;
                };
            } else {
                GVAR(aoLeft) = -1;
            };

        }, 2, [_timeout]] call MFUNC(addPerFramehandler);
    }, 10] call CFUNC(wait);

}] call CFUNC(addEventHandler);
