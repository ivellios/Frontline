/*
    Function:       FRL_dynamicFrontline_fnc_initDebug
    Author:         Adanteh
    Description:    Inits all the visual debugging / system checks
*/
#include "macros.hpp"

#ifdef DEBUGMODE
//#define DRAWSQUARES

#define pixelScale  0.25
#define GRID_W (pixelW * pixelGridNoUIScale * pixelScale)
#define GRID_H (pixelH * pixelGridNoUIScale * pixelScale)
#define CENTER_X  ((getResolution select 2) * 0.5 * pixelW)
#define CENTER_Y  ((getResolution select 3) * 0.5 * pixelH)
#define pxW(x) (x * GRID_W)
#define pxH(x) (x * GRID_H)

if (hasInterface) then {
    /*  Description:    Creates a debug interface with the important stuff for this gamemode
        Locality:       Client           */
    GVAR(debugContents) = [];
    GVAR(debugShown) = profileNamespace getVariable [QGVAR(debugShown), false];
    GVAR(debugPFH) = -1;

    GVAR(debugContents) pushBack {
        private _percentages = missionNamespace getVariable [QGVAR(aoPercentages), [0, 0]];
        private _text = format ["Capture: RED %1%3 - BLUE %2%3",
            (_percentages select 0) * 100,
            (_percentages select 1) * 100,
            "%"];
        _text
    };
    GVAR(debugContents) pushBack {
        private _text = format ["Bleed: %1 /minute",
            (missionNamespace getVariable [QGVAR(ticketBleed), 0]) / 10];
        _text
    };


    GVAR(debugContents) pushBack {
        private _inFriendly = [QGVAR(dbgFriendlyPos), {
            [getPosASL Clib_Player, side group Clib_Player] call FUNC(posInFriendly);
        }, true, 1] call CFUNC(cachedCall);
        private _text = format ["Friendly Area: %1", _inFriendly];
        _text
    };

    GVAR(debugContents) pushBack {
        private _isIsland = [QGVAR(dbgIslandPos), {
            [getPos Clib_Player, nil, QGVAR(dbgIslandPos)] call FUNC(posIsIsland);
            (missionNamespace getVariable [QGVAR(posIsIsland), false])
        }, true, 1] call CFUNC(cachedCall);
        private _text = format ["Is Island: %1", _isIsland];
        _text
    };


    [QGVAR(dbgDebugInterfaceShow), {
        if !(isNull (uiNamespace getVariable [QGVAR(uiDbgInterface), controlNull])) then {
            ctrlDelete (uiNamespace getVariable [QGVAR(uiDbgInterface), controlNull]);
            ctrlDelete (uiNamespace getVariable [QGVAR(uiDbgInterface2), controlNull]);
        };

        disableSerialization;

        private _ctrl1 = (findDisplay 46) ctrlCreate ["RscBackground", -1];
        _ctrl1 ctrlSetPosition [safeZoneX + pxW(10), CENTER_Y - pxH(30), pxW(70), pxH(60)];
        _ctrl1 ctrlSetBackgroundColor [0, 0, 0, 0.5];
        _ctrl1 ctrlCommit 0;

        private _baseText = format ["<t align='left' shadow='1' size='%1'>DynamicFrontline Debug:<br />", pxH(12) / 0.08];
        private _ctrl = (findDisplay 46) ctrlCreate ["RscStructuredText", -1];
        _ctrl ctrlSetPosition [safeZoneX + pxW(10), CENTER_Y - pxH(30), pxW(70), pxH(60)];
        _ctrl ctrlSetStructuredText parseText _baseText;
        _ctrl ctrlCommit 0;

        uiNamespace setVariable [QGVAR(uiDbgInterface), _ctrl];
        uiNamespace setVariable [QGVAR(uiDbgInterface2), _ctrl1];

        GVAR(debugPFH) = [{
            if (missionNamespace getVariable [QGVAR(endMission), false]) exitWith { [_this select 1] call MFUNC(removePerFrameHandler); };
            private _newText = (_this select 0);
            {
                _newText = format ["%1%2<br />", _newText, (call _x)];
                nil;
            } count GVAR(debugContents);
            _newText = _newText + "</t>";
            (uiNamespace getVariable [QGVAR(uiDbgInterface), controlNull]) ctrlSetStructuredText parseText _newText;
        }, 1, _baseText] call MFUNC(addPerFramehandler)
    }] call CFUNC(addEventHandler);


    [QGVAR(dbgDebugInterfaceHide), {
        if !(isNull (uiNamespace getVariable [QGVAR(uiDbgInterface), controlNull])) then {
            ctrlDelete (uiNamespace getVariable [QGVAR(uiDbgInterface), controlNull]);
            ctrlDelete (uiNamespace getVariable [QGVAR(uiDbgInterface2), controlNull]);
        };
        if (GVAR(debugPFH) != -1) then {
            [GVAR(debugPFH)] call MFUNC(removePerFrameHandler);
        };
    }] call CFUNC(addEventHandler);

    /*  Description:    Adds given message to the debug interface
        Locality:       Client           */
    [QGVAR(dbgInterfaceAdd), {
        params ["_message", ["_color", "#FFFFFF"]];
        GVAR(debugContents) pushBack (compile str format ["<t color='%2'>%1</t>", _message, _color]);
    }] call CFUNC(addEventHandler);

    [QGVAR(initAO), {
        ['add', ["gui", 'Frontline Settings', 'DF Debug', [
        	'Debug Monitor',
        	'Shows the DF debug monitor',
        	QMVAR(CtrlCheckbox),
        	{ GVAR(debugShown); },
        	true,
        	{
        		GVAR(debugShown) = (_this select 1 == 1);
        		profileNamespace setVariable [QGVAR(debugShown), GVAR(debugShown)];
        		if (GVAR(debugShown)) then {
                    [QGVAR(dbgDebugInterfaceShow)] call CFUNC(localEvent);
                } else {
                    [QGVAR(dbgDebugInterfaceHide)] call CFUNC(localEvent);
                };
        	},
        	[]
        ]]] call MFUNC(settingsWindow);

        if (GVAR(debugShown)) then {
            [QGVAR(dbgDebugInterfaceShow)] call CFUNC(localEvent);
        };
    }] call CFUNC(addEventHandler);

    // -- AO Capture painting. Left mouse to capture for OPFOR, Middle mouse to do BLUFOR capture
    [{
        GVAR(buttonHeld) = -1;
        GVAR(nextDraw) = -1;
        GVAR(allowPainting) = profileNamespace getVariable [QGVAR(allowPainting), false];
        ['add', ["gui", 'Frontline Settings', 'DF Debug', [
        	'Enable painting',
        	'Left mouse for side one, middle mouse for the other one',
        	QMVAR(CtrlCheckbox),
        	{ GVAR(allowPainting); },
        	true,
        	{
        		GVAR(allowPainting) = (_this select 1 == 1);
        		profileNamespace setVariable [QGVAR(allowPainting), GVAR(allowPainting)];
        	},
        	[]
        ]]] call MFUNC(settingsWindow);


        private _map = ((findDisplay 12) displayCtrl 51);
        {
            _map ctrlAddEventHandler [_x, {
                if (GVAR(buttonHeld) != -1) then {
                    params ["_map", "_xPos", "_yPos"];
                    if (diag_tickTime > GVAR(nextDraw)) then { // -- Only send a position every 0.5 second
                        GVAR(nextDraw) = diag_tickTime + 0.05;
                        private _sideID = [0, 2] find GVAR(buttonHeld);
                        if (_sideID == -1) exitWith { };

                        (((findDisplay 12) displayCtrl 51) ctrlMapScreenToWorld [_xPos, _yPos]) params ["_xPosWorld", "_yPosWorld"];
                        [QGVAR(dbgPaint), [_sideID, _xPosWorld, _yPosWorld]] call CFUNC(serverEvent);
                    };
                };
            }];
        } forEach ["MouseMoving", "MouseHolding"];

        // -- We dont use the map ctrl, because BIS doesn't accept middle mouse on map event handlers, because why would they do that.
        // -- That would actually require some sort of consistency between UI events and elements and we can't have that in Arma
        (findDisplay 46) displayAddEventHandler ["MouseButtonDown", {
            if !(visibleMap) exitWith { false };
            if !(call MFUNC(isAdmin)) exitWith { false };
            if !(GVAR(allowPainting)) exitWith { false };
            params ["", "_button", "_xPos", "_yPos", "_shift", "_ctrl", "_alt"];
            if ({_x } count [_shift, _ctrl, _alt] == 0) then {
                if (GVAR(buttonHeld) != _button) then {
                    GVAR(buttonHeld) = _button;
                };
            };
        }];

        (findDisplay 46) displayAddEventHandler ["MouseButtonUp", {
            if !(visibleMap) exitWith { false };
            params ["", "_button", "_xPos", "_yPos", "_shift", "_ctrl", "_alt"];
            if ({_x } count [_shift, _ctrl, _alt] == 0) then {
                if (GVAR(buttonHeld != -1)) then {
                    GVAR(buttonHeld) = -1;
                    [QGVAR(updateFrontline)] call CFUNC(serverEvent);
                    [QGVAR(updateAOControlled)] call CFUNC(serverEvent);
                    [QGVAR(squaresUpdate)] call CFUNC(serverEvent);
                };

            };
        }];
    }, {!(isNull ((findDisplay 12) displayCtrl 51))}] call CFUNC(waitUntil);
};

if (isServer) then {
    [QGVAR(dbgPaint), {
        (_this select 0) params ["_sideID", "_xPos", "_yPos"];
        // -- Make it update 5 times 5 units (5units is max capture), so have faster result
        for "_i" from 1 to 20 do {
            private _unitPos = [];
            {
                _unitPos pushBack [-1, _sideID, _xPos, _yPos];
                nil;
            } count [1, 2, 3, 4, 5];
            private _return = [format ["%1.units_location", GVAR(pythonDir)], _unitPos] call py3_fnc_callExtension;
        };
    }] call CFUNC(addEventHandler);
};



#endif
