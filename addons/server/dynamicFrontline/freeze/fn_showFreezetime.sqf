/*
    Function:       FRL_freeze_fnc_showFreezetime
    Author:         N3croo
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

if (isNil QGVAR(freezeEndAt)) exitWith { };

// -- Already ended
GVAR(freezeEndAt) params [["_freezeTime", 0], ["_endAt", 0]];
if (serverTime > _endAt) exitWith { };

private _wtfIsThisServertime = serverTime - GVAR(endTimeAt);
private _freezeFor = _freezeTime - _wtfIsThisServertime;

["progressHudShow",
    [QGVAR(FreezetimeHUD),
        {
            (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\respawn.paa");
            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor [0.222, 0.7, 0.222, 0.6];
        },
        {
            params [["_startTime", 1], ["_endAt", 1], ["_freezeFor", 10]];
            private _timeLeft = _endAt - serverTime;
            private _timeElapsed = serverTime - _startTime;
            private _percentage = _timeElapsed / _freezeFor;
            if (_percentage >= 1) exitWith { };

            (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
            (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["Frontline active in %1", ([_timeLeft, "MM:SS", false] call BIS_fnc_secondsToString)]);
        },
        [serverTime, _endAt, serverTime - _endAt]
    ]
] call CFUNC(localEvent);
