/*
    Function:       FRL_DynamicFrontline_fnc_clientInitFreeze
    Author:         Adanteh
    Description:    Show hud for frozen frontline at game start (Shows after battleprep_
*/
#include "macros.hpp"

["freezeTimeStart", {
    [] call FUNC(showFreezetime)
}] call CFUNC(addEventHandler);

["freezeTimeEnd", {
    ["shownotification", ["Frontline is now active!"]] call CFUNC(localEvent);
    ["progressHudHide", [QGVAR(FreezetimeHUD)]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

["missionStarted", {
    // -- Call it for JIP
    [] call FUNC(showFreezetime);
}] call CFUNC(addEventHandler);


['add', ["admin", 'Admin Settings', 'Battleprep', [
    'End freeze',
    'End the freezetime',
    QMVAR(CtrlButton),
    { true },
    { true },
    {
        GVAR(freezeEndAt) = [0, 0];
        publicVariable QGVAR(freezeEndAt);
    },
    ["END"]
]]] call MFUNC(settingsWindow);
