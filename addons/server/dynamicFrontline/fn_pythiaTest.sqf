#include "macros.hpp"

/*  Function:       FRL_DynamicFrontline_fnc_pythiaTest
    Description:    Test if pythia is enabled and working correctly
    Locality:       Server          */

private _error = call {
    if !(isClass (configFile >> "CfgPatches" >> "PY3_Pythia")) exitWith { "@Pythia mod not loaded" };
    (call py3_fnc_extensionTest) params ["_extensionTest", "_extensionReturn"];
    if !(_extensionTest) exitWith { _extensionReturn };
    "";
};

if (_error != "") then {
    [QGVAR(dbgInterfaceAdd), [format ["ERROR: %1", _error], "#FF0000"]] call CFUNC(globalEvent);
    false;
} else {
    true;
};
