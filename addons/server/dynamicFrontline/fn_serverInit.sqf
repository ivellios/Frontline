 #include "macros.hpp"
/*
 *	File: fn_serverInit.sqf
 *	Author: Adanteh
 *	Defines debug functions on server only
 *
 *	Example:
 *	[player] call FUNC(serverInit);
 */

GVAR(pythonDir) = "DynamicFrontline"; // Adanteh: Kinda need a way to figure out mod path here
GVAR(ServerInitDone) = false;
GVAR(pythiaLogicStarted) = false;
GVAR(aoCorners) = [];
GVAR(frontlines) = [];
GVAR(aoOrigin) = [0, 0, 0];
GVAR(endMission) = false;


[QGVAR(settings), configFile >> "FRL" >> "CfgDynamicFrontline"] call MFUNC(cfgSettingLoad);
[QGVAR(settings), missionConfigFile >> "FRL" >> "CfgDynamicFrontline"] call MFUNC(cfgSettingLoad);

// -- Make a dictionary of the settings
GVAR(settings) = [[QUOTE(PREFIX), "CfgDynamicFrontline", "Settings"], nil, true] call MFUNC(configToDictionary);
GVAR(aoPointsize) = [GVAR(settings), "square_size", 50] call MFUNC(dictionaryGet);

publicVariable QGVAR(aoPointsize);

GVAR(pfh1) = -1;
GVAR(pfh2) = -1;

["missionStarted", {
    [{

        private _frontlineUpdateSpeed = [QGVAR(frontlineUpdateSpeed), 10] call MFUNC(cfgSetting);

        [QGVAR(initAOServer)] call CFUNC(localEvent);

        if !(GVAR(pythiaLogicStarted)) exitWith {
            if (hasInterface) then { // -- If local host, don't get your game stuck when init fails
                [QGVAR(LoadingScreen)] call MFUNC(endLoadingScreen);
            };
        };

        GVAR(ServerInitDone) = true;
        publicVariable QGVAR(ServerInitDone);

        // -- Time the Frontline is frozen after battleprep
        // Skip this entirely for singleplayer.
        ["freezeTimeEnd", {
            private _positionsUpdateSpeed = [QGVAR(positionsUpdateSpeed), 2.5] call MFUNC(cfgSetting);
            GVAR(pfh1) = [{
                if (GVAR(endMission)) exitWith { [_this select 1] call MFUNC(removePerFrameHandler); };
                [QGVAR(updateUnitPositions)] call CFUNC(localEvent);
            }, _positionsUpdateSpeed] call MFUNC(addPerFramehandler);
        }] call CFUNC(addEventHandler);

        if !(isMultiplayer) then {
            ["freezeTimeEnd"] call CFUNC(localEvent);
        } else {
            if (([QGVAR(frontlineFreezetime), 300] call MFUNC(cfgSetting)) > 0) then {

                // Start the freeze at the end of battleprep
                ["gameStart", {
                    private _freezeTime = [QGVAR(frontlineFreezetime), 300] call MFUNC(cfgSetting);
                    private _endAt = _freezeTime + serverTime;
                    GVAR(freezeEndAt) = [_freezeTime, _endAt];
                    publicVariable QGVAR(freezeEndAt);
                    ["freezeTimeStart", [_endAt]] call CFUNC(globalEvent);
                    [{
                        ["freezeTimeEnd"] call CFUNC(globalEvent);
                    }, { serverTime > (GVAR(freezeEndAt) select 1) }] call CFUNC(waitUntil);
                }] call CFUNC(addEventHandler);
            };
        };

        // -- Visual feedback updating
        GVAR(pfh2) = [{
            if (GVAR(endMission)) exitWith { [_this select 1] call MFUNC(removePerFrameHandler); };
            [QGVAR(updateFrontline)] call CFUNC(localEvent);
            [QGVAR(updateAOControlled)] call CFUNC(localEvent);
            [QGVAR(squaresUpdate)] call CFUNC(localEvent);
        }, _frontlineUpdateSpeed] call MFUNC(addPerFramehandler);
    }, 2.5,[]] call CFUNC(wait);
}] call CFUNC(addEventhandler);



// -- automagic AI pathfinding, send them to closest place
["AI_waypointPosition", {
    params ["_side", "_groupPosition", "_group"];

    private _position = [];
    private _closest = 1e9;
    {
        // -- If this is a big frontline, exclude the corner points so we don't send AI along the sides too much
        private _points = _x;
        // -- Ignore very small islands
        if (count _points > 10) then {
            _points = _points select [5, count _points - 10];
            private _distances = (_points apply { _x distance2D _groupPosition });
            private _distanceMin = selectMin _distances;
            if (_distanceMin < _closest) then {
                _position = _points select (_distances find _distanceMin);
                _closest = _distanceMin;
            };
        };
    } forEach (GVAR(frontlines) select 0);
    if !(_position isEqualTo []) then {
        _AI_waypointPosition = _position; // NO PRIVATE
    };
    true;
}] call MFUNC(addCondition);


["missionStarted", {
    [{
        private _sectors = "true" configClasses (missionConfigFile >> QUOTE(PREFIX) >> "CfgSectors");
        {
            private _sector = _x;
            [true, [configName _sector, _x, getText (_x >> "designator")]] call MFUNC(sectorCreate);
        } forEach _sectors;

    }, 2.5,[]] call CFUNC(wait);
}] call CFUNC(addEventhandler);
