/*
    Function:       FRL_DynamicFrontline_fnc_determineAoPriorityZones
    Author:         N3croo
    Description:    Returns the priority Zones placed in the editor and deletes their static markers
*/
#include "macros.hpp"

private _zones = [];

private _markerIndex = 0;
private _markersSkipped = 0;

private _markerName = "";

while { _markersSkipped < 10 } do {

    _markerName = format ["frl_AoPrio_%1", _markerIndex];
    private _markerShape = markerShape _markerName;

    if (_markerShape != "") then {
        private _size = getMarkerSize _markerName;
        private _dir = markerDir _markerName;
        private _pos = getMarkerPos _markerName;
        private _alpha = markerAlpha _markerName;
        _pos resize 2;
        _zones pushBack [_markerShape, _pos, _size, _dir, _alpha];

        _markerName setMarkerAlpha 0;
    } else {
        _markersSkipped = _markersSkipped + 1;
    };

    _markerIndex = _markerIndex + 1;
};

_zones;
