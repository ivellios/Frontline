#include "macros.hpp"

if (isNil QGVAR(aoOrigin)) then { GVAR(aoOrigin) = [0, 0, 0]; };
if (isNil QGVAR(aoPercentages)) then { GVAR(aoPercentages) = [0.5, 0.5]; };
[QGVAR(settings), configFile >> "FRL" >> "CfgDynamicFrontline"] call MFUNC(cfgSettingLoad);
[QGVAR(settings), missionConfigFile >> "FRL" >> "CfgDynamicFrontline"] call MFUNC(cfgSettingLoad);


// -- Updates markers for main base and adds name on map
[QGVAR(initAO), {
    [missionNamespace getVariable [QMVAR(allSectors), objNull], {
        [false, [_value]] call MFUNC(sectorCreate);
    }] call MFUNC(forEachVariable);

}] call CFUNC(addEventHandler);

// -- Waits till server is done preparing the gamemode
["missionStarted", {
    if (canSuspend) then {
        diag_log ["[FRL LOG] - DF, SCHEDULED WARNING"];
    };
    diag_log ["[FRL LOG] - DF, Client mission started"];
    if !(isServer) then {
        [QGVAR(LoadingScreen)] call MFUNC(startLoadingScreen);
    };

    [{
        diag_log ["[FRL LOG] - DF, Client initAO"];
        [{
            [QGVAR(initAO)] call CFUNC(localEvent);
            [QGVAR(aoPoly), [["POLYGON", GVAR(aoCorners), [0, 0, 0, 1]]], "normal"] call CFUNC(addMapGraphicsGroup);
            [QGVAR(LoadingScreen)] call MFUNC(endLoadingScreen);
        }, 2.5] call CFUNC(wait);
    }, { missionNamespace getVariable [QGVAR(ServerInitDone), false] }] call CFUNC(waitUntil);

}] call CFUNC(addEventHandler);

// -- Draws the black frontline
[QGVAR(drawFrontline), {
    (_this select 0) params ["_frontlines"];
    _frontlines params ["_frontlines_black", "_frontlines_west", "_frontlines_east"];

    // Draw the black frontline
    private _frontlineBlocks = [];
    {
        private _points = _x;
        for "_i" from 0 to (count _points - 2) do {
            private _posFrom = _points select _i;
            private _posTo = _points select (_i + 1);
            private _line = [_posFrom, _posTo, [0, 0, 0, 1]] call FUNC(drawLine);
            _frontlineBlocks pushBack _line;
        };
        nil;
    } count _frontlines_black;

    // Draw the blue frontline. This is only used in replays
    {
        private _points = _x;
        for "_i" from 0 to (count _points - 2) do {
            private _posFrom = _points select _i;
            private _posTo = _points select (_i + 1);
            private _line = [_posFrom, _posTo, [0,0.3,0.6,1]] call FUNC(drawLine);
            _frontlineBlocks pushBack _line;
        };
        nil;
    } count _frontlines_west;

    // Draw the red frontline. This is only used in replays
    {
        private _points = _x;
        for "_i" from 0 to (count _points - 2) do {
            private _posFrom = _points select _i;
            private _posTo = _points select (_i + 1);
            private _line = [_posFrom, _posTo, [0.6,0,0,1]] call FUNC(drawLine);
            _frontlineBlocks pushBack _line;
        };
        nil;
     } count _frontlines_east;

     [QGVAR(frontline), _frontlineBlocks, "normal"] call CFUNC(addMapGraphicsGroup);
}] call CFUNC(addEventHandler);


// -- Show the progress to end of mission by capturing a certain percentage
[QGVAR(percentageVictoryUpdate), {
    private _timeStart = missionNamespace getVariable [QGVAR(percentageReachedAt), -1];
    if (_timeStart == -1) then {
        ["progressHudHide", QGVAR(percentageVictoryUpdate)] call CFUNC(localEvent);
    } else {
        private _timeRequired = [QGVAR(Settings_timeRequired), 10*60] call MFUNC(cfgSetting);
        private _winningSide = missionNamespace getVariable [QGVAR(winningSide), sideUnknown];
        private _baseText = ["Defeat in", "Victory in"] select (_winningSide isEqualTo playerSide);
        ["progressHudShow", [QGVAR(percentageVictoryUpdate),
        {
            params ["_timeStart", "_timeRequired", "_winningSide"];
            (_controlsGroup controlsGroupCtrl 2) ctrlSetText (MEDIAPATH + "icons\respawn.paa");
            (_controlsGroup controlsGroupCtrl 5) ctrlSetTextColor ([_winningSide, 'color'] call MFUNC(getSideData));
        },
        {
            params ["_timeStart", "_timeRequired", "", "_baseText"];
            private _timeLeft = ((_timeStart + _timeRequired) - serverTime) max 0;
            private _percentage = (0 max ((serverTime - _timeStart) / _timeRequired)) min 1;
            (_controlsGroup controlsGroupCtrl 5) progressSetPosition _percentage;
            (_controlsGroup controlsGroupCtrl 3) ctrlSetText (format ["%2: %1", ([_timeLeft, "MM:SS", false] call BIS_fnc_secondsToString), _baseText]);
        },
        [_timeStart, _timeRequired, _winningSide, _baseText]]] call CFUNC(localEvent);
    };
}] call CFUNC(addEventHandler);

{
    [_x, {
        [QGVAR(percentageVictoryUpdate)] call CFUNC(localEvent);
    }] call CFUNC(addEventHandler);
} forEach ["MissionStarted", "PlayerSideChanged"];


// -- Show bleed * capture percentages
[{
	private _controlIndicator = "";
	{
        private _sideID = abs (GVAR(factionOrder) find _x);
		private _control = (GVAR(aoPercentages) param [_sideID, 0]) * 100;
		_controlIndicator = _controlIndicator + format ["<t align='%2'>%3%4 Control</t>",
            "",
            ["left", "right"] select (_forEachIndex % 2),
            _control,
            "%"
        ];
	} forEach (call MFUNC(getSides));
	_controlIndicator;
}, 11] call MFUNC(addMapHUD);
