## TODO

* [x] Need to do spawnpoint deployment requirements
* [x] Allow AO rotation
  * [x] Fix the black blocks on the outside (stack)
* [x] Create an SP testing mission  
* [x] Allow polygonal AO
* [x] Block FOs if they end up on an island or behind the frontline

* [x] remote Value fetch is broken (isIsland no worky)
* [x] AO polygon drawing no worky
