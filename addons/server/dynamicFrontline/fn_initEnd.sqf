/*
    Function:       FRL_DynamicFrontline_fnc_intEnd
    Author:         Adanteh
    Description:    Ends the mission
*/
#include "macros.hpp"

["endVote", {
    (_this select 0) params ["_reason", "_losingTeam"];
    private _winner = if (isNil "_losingTeam") then {
        nil;
    } else {
        !(playerSide isEqualTo _losingTeam)
    };
    [_reason, _winner] call FUNC(endMission);
}] call CFUNC(addEventHandler);

[QSVAR(endMissionAdmin), {
    (_this select 0) params ["_caller"];
    ["Mission ended by admin", "warning"] call MFUNC(notificationShow);
    [{ ["Mission ended by admin", nil, "In this case the winner is decided by who has the most tickets."] call FUNC(endMission)}, 5] call CFUNC(wait);
}] call CFUNC(addEventHandler);

[QGVAR(endMissionPercentage), {
    (_this select 0) params ["_winnerSide"];
    private _winner = _winnerSide isEqualTo playerSide;
    ["A large enough area has come under total control", _winner, "Holding almost the entire AO for a while will ensure a quick victory. You'll see a progress bar in the top left whenever this is about to happen"] call FUNC(endMission);
}] call CFUNC(addEventHandler);

["durationEnd", {
    ["Mission time has passed", nil, "In this case the winner is decided by who has the most tickets."] call FUNC(endMission);
}] call CFUNC(addEventHandler);

["victoryPointsReached", {
    ["Victory points reached", nil] call FUNC(endMission);
}] call CFUNC(addEventHandler);
