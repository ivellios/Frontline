#include "..\macros.hpp"

class CfgPatches {
    class DOUBLE(ADDON,_gestures) {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        requiredAddons[] = {
            "FRL_Main"
        };
    };
};


class cfgGesturesMale {
    class Default;
    class States {

        // This is the class name of the gesture. Use this name when invoking the gesture with the "switchGesture" or "playGesture" command
        class GestureMountMuzzle;
        class SVAR(baseGesture): GestureMountMuzzle {
            mask = "handsWeapon";
            canReload = 0;
            headBobStrength = -1;
            looped = 0;
            disableWeapons = 1;
            disableWeaponsLong = 1;
            showWeaponAim = 0;
            canPullTrigger = 0;
            enableOptics = 0;
        };

        #define __MODIFIER 0.76
        class SVAR(bandageGesture) : SVAR(baseGesture) {
            //rightHandIKCurve[] = {0.107, 1, 0.12, 0, 0.842, 0, 0.8625, 1};
            //leftHandIKCurve[] = {0.066, 1, 0.089, 0, 0.82, 0, 0.846, 1};
            file = PATHTO(data\HEALING\AinvPknlMstpSnonWnonDnon_medic1);
            speed = 0.26 * __MODIFIER;  // (Source Frame Rate divided by Number of frames) = 30 / 51 = .588
        };

        // INJECTOR
        class SVAR(bandageGesture1): SVAR(bandageGesture) {
            file = PATHTO(data\HEALING\AinvPknlMstpSnonWnonDnon_medic1);
            speed = 0.26 * __MODIFIER;
        };

        // SALT-SHAKER
        class SVAR(bandageGesture2): SVAR(bandageGesture) {
            file = PATHTO(data\HEALING\AinvPknlMstpSnonWnonDnon_medic2);
            speed = 0.19 * __MODIFIER;
        };

        // BANDAGE CHEST
        class SVAR(bandageGesture3): SVAR(bandageGesture) {
            file = PATHTO(data\HEALING\AinvPknlMstpSnonWnonDnon_medic3);
            speed = 0.2 * __MODIFIER;
        };

        class SVAR(repairGesture) : SVAR(baseGesture) {
            file = "\A3\anims_f\Data\Anim\Sdr\inv\pne\stp\lay\rfl\AinvPpneMstpSlayWrflDnon_medic";
            speed = -8 * __MODIFIER;  // (Source Frame Rate divided by Number of frames) = 30 / 51 = .588
        };
        class SVAR(repairGesture1) : SVAR(baseGesture) {
            file = PATHTO(data\cargo\Acts_carFixingWheel);
            speed = 0.05;  // (Source Frame Rate divided by Number of frames) = 30 / 51 = .588
        };
        class SVAR(repairGesture2) : SVAR(baseGesture) {
            file = "\a3\Anims_F_EPA\data\Anim\sdr\cts\Custom\A_m01\Acts_TreatingWounded01";
            speed = 0.1887;  // (Source Frame Rate divided by Number of frames) = 30 / 51 = .588
        };
    };
};

class CfgMovesBasic {
    class ManActions {
        SVAR(bandageGesture) = QSVAR(bandageGesture);
        SVAR(bandageGesture1) = QSVAR(bandageGesture1);
        SVAR(bandageGesture2) = QSVAR(bandageGesture2);
        SVAR(bandageGesture3) = QSVAR(bandageGesture3);
        SVAR(repairGesture) = QSVAR(repairGesture);
        SVAR(repairGesture1) = QSVAR(repairGesture1);
        SVAR(repairGesture2) = QSVAR(repairGesture2);
    };
    class Actions {
        class NoActions: ManActions {
            SVAR(bandageGesture)[] = {QSVAR(bandageGesture), "Gesture"};
            SVAR(bandageGesture1)[] = {QSVAR(bandageGesture1), "Gesture"};
            SVAR(bandageGesture2)[] = {QSVAR(bandageGesture2), "Gesture"};
            SVAR(bandageGesture3)[] = {QSVAR(bandageGesture3), "Gesture"};
            SVAR(repairGesture)[] = {QSVAR(repairGesture), "Gesture"};
            SVAR(repairGesture1)[] = {QSVAR(repairGesture1), "Gesture"};
            SVAR(repairGesture2)[] = {QSVAR(repairGesture2), "Gesture"};
        };
    };
};
