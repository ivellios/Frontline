#include "macros.hpp"

class CfgPatches {
    class ADDON {
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        // author = AUTHOR_STR;
        // authors[] = AUTHOR_ARR;
        // authorUrl = AUTHOR_URL;
        version = VERSION;
        versionStr = QUOTE(VERSION);
        versionAr[] = {VERSION_AR};
        requiredAddons[] = {
            "FRL_Main"
        };
    };
};


class CfgMovesFatigue {
    staminaDuration = 60;
    staminaCooldown = 10;
    staminaRestoration = 60;
    aimPrecisionSpeedCoef = 7;
    terrainDrainSprint = -0.9;
    terrainDrainRun = -0.5;
    terrainSpeedCoef = 0.9;
};


class CfgGesturesMale {
    class Default;
    class States {
        // -- THROWING SPEED
        class GestureReloadBase;
        class GestureThrowGrenade: GestureReloadBase {
            speed = -0.9;
            headBobStrength = 2;
            headBobMode = 3;
        };

        class GestureThrowGrenadeUna: GestureReloadBase {
            speed = -0.9;
        };

        class GestureThrowGrenadePistol: GestureReloadBase {
            headBobStrength = 2;
            headBobMode = 3;
            speed = -0.9;
        };

        class GestureThrowGrenadeLauncher: GestureReloadBase {
            headBobStrength = 2;
            headBobMode = 3;
            speed = -0.9;
        };

        // -- Launcher reload speed
        class GestureReloadRPG7: GestureReloadBase {
            mask = "launcher";
            file = "\A3\Anims_F_Exp\Data\Anim\sdr\gst\gestureReloadRPG7";
            weaponIK = 1;
            speed = 0.178;
            LeftHandIKCurve[] = {0.038, 1, 0.076, 0, 0.851, 0, 0.958, 1};
            RightHandIKCurve[] = {0.038, 1, 0.076, 0, 0.851, 0, 0.958, 1};
        };
        
        class GestureReloadRPG7Kneel: GestureReloadRPG7 {
            file = "\A3\Anims_F_Exp\Data\Anim\sdr\gst\gestureReloadRPG7knl";
            weaponIK = 1;
            speed = 0.18;
        };

    };
};

class CfgMovesBasic {
    class Actions {
        // -- JUMP
        class RifleStandActionsNoAdjust;
        class RifleStandActionsRunF: RifleStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };
        
        class RifleStandActionsRunFL: RifleStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };
        
        class RifleStandActionsRunFR: RifleStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };

        class RifleStandEvasiveActionsF: RifleStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };
        
        class RifleStandEvasiveActionsFL: RifleStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };
        
        class RifleStandEvasiveActionsFR: RifleStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };

        class RifleLowStandActionsNoAdjust;
        class RifleLowStandActionsRunF: RifleLowStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };
        
        class RifleLowStandActionsRunFL: RifleLowStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };
        
        class RifleLowStandActionsRunFR: RifleLowStandActionsNoAdjust {
            getOver = "AovrPercMrunSrasWrflDf";
        };
    };
};

class CfgMovesMaleSdr: CfgMovesBasic {
    class StandBase;
    class Default;
    class States {

/*
        class SprintBaseDf;
        class SprintCivilBaseDf;
        class AmovPercMwlkSlowWrflDf;
        class AmovPercMrunSlowWrflDfl;
        class AmovPercMrunSrasWrflDfl;
        class AmovPercMrunSnonWnonDfl;


        class AmovPknlMrunSrasWpstDf;
        class AidlPknlMstpSrasWlnrDnon_G0S;
        class AmovPknlMrunSnonWnonDf;

        class AmovPknlMwlkSoptWbinDf;
        class AmovPercMevaSnonWbinDf_rfl;
        class AmovPercMevaSnonWbinDf_pst;
        class AmovPknlMrunSnonWbinDf_pst;

        class AmovPercMwlkSlowWrflDf_ver2;
        class AmovPercMwlkSlowWrflDfl_ver2;
        class AmovPknlMtacSrasWlnrDf;
        class AmovPknlMrunSlowWrflDfl;

        class AmovPknlMrunSlowWrflDl;
        class AmovPknlMrunSlowWrflDr;
        class AmovPercMtacSlowWrflDfr_ver2;

        class AmovPercMrunSrasWpstDf;
        class AmovPknlMevaSrasWrflDf;
        class AmovPercMwlkSoptWbinDf;
        class AmovPercMstpSoptWbinDnon;

        class AmovPercMtacSrasWrflDf;
*/


        class TransAnimBase;
        class SprintBaseDf;
        class SprintCivilBaseDf;
        class AmovPercMwlkSlowWrflDf;
        class AmovPercMrunSlowWrflDfl;
        class AmovPercMrunSrasWrflDfl;
        class AmovPercMrunSnonWnonDfl;
        //class AmovPknlMrunSrasWpstDf;
        class AidlPknlMstpSrasWlnrDnon_G0S;
        class AmovPknlMwlkSoptWbinDf;
        class AmovPercMevaSnonWbinDf_rfl;
        class AmovPercMevaSnonWbinDf_pst;
        class AmovPknlMrunSnonWbinDf_pst;
        class AmovPercMwlkSlowWrflDfl_ver2;
        class AmovPknlMtacSrasWlnrDf;
        //class AmovPknlMrunSlowWrflDfl;
        //class AmovPknlMrunSlowWrflDl;
        //class AmovPknlMrunSlowWrflDr;
        class AmovPercMtacSlowWrflDfr_ver2;
        class AmovPercMrunSrasWpstDf;
        class AmovPknlMevaSrasWrflDf;
        class AmovPercMwlkSoptWbinDf;
        class AmovPercMstpSoptWbinDnon;
        class AswmPercMstpSnonWnonDnon;
        class AadjPpneMstpSrasWrflDleft;
        class AadjPpneMstpSrasWrflDright;
        class AmovPknlMstpSnonWnonDnon;
        class AmovPercMstpSnonWnonDnon;
        class AidlPknlMstpSlowWrflDnon_G0S;
        //class AmovPknlMrunSlowWrflDbl;
        //class AmovPknlMrunSlowWrflDbr;
        //class AmovPknlMrunSnonWnonDf;
        class AidlPknlMstpSrasWpstDnon_G0S;
        //class AmovPknlMtacSrasWpstDf;
        class AmovPercMwlkSrasWpstDf;
        class AmovPknlMwlkSrasWpstDf;
        class AmovPercMrunSlowWlnrDb;
        class AmovPercMrunSlowWlnrDfr;

        class AsswPercMstpSnonWnonDnon;


        // -- JUMP
        class AovrPercMstpSrasWrflDf;
        class AovrPercMrunSrasWrflDf: AovrPercMstpSrasWrflDf {
            forceAim = 0;
        };

         class AmovPercMstpSlowWrflDnon: StandBase{
            stamina = 1.7;
        };
        
        class AmovPercMstpSrasWrflDnon: AmovPercMstpSlowWrflDnon{	//standing weapon up
            stamina = 1.5;
        };
        
        class AmovPercMrunSrasWrflDf: AmovPercMstpSrasWrflDnon { 	// run weapon up
            InterpolateTo[] = {"AovrPercMrunSrasWrflDf",0.22,"AmovPercMrunSlowWrflDf",0.025,"AmovPercMwlkSrasWrflDf",0.025,"AmovPknlMrunSrasWrflDf",0.03,"AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon",0.02,"AmovPercMevaSrasWrflDf",0.025,"Unconscious",0.01,"AmovPercMtacSrasWrflDf",0.02,"AmovPercMrunSrasWrflDfl",0.02,"AmovPercMrunSrasWrflDfl_ldst",0.02,"AmovPercMrunSrasWrflDfr",0.02,"AmovPercMrunSrasWrflDfr_ldst",0.02,"AmovPercMstpSrasWrflDnon",0.02,"AmovPercMrunSrasWrflDl",0.02,"AmovPercMrunSrasWrflDbl",0.02,"AmovPercMrunSrasWrflDb",0.02,"AmovPercMrunSrasWrflDbr",0.02,"AmovPercMrunSrasWrflDr",0.02,"AmovPknlMstpSlowWrflDnon_relax",0.1,"AmovPercMrunSrasWrflDf_ldst",0.02,"AmovPercMrunSrasWrflDf",0.02};
			speed = 0.62; //	0.684541;
            stamina = -0.025;
        };
        
        class AmovPercMwlkSlowWrflDf_ver2: AmovPercMstpSlowWrflDnon{
            speed = 0.32; //0.27536
            stamina = 1;
        };
        
        class AmovPknlMstpSlowWrflDnon: AmovPercMstpSlowWrflDnon{   // crouched weapon lowered
            stamina = 2.2;
        };
        
        class AmovPpneMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon{   // prone weapon up
            stamina = 2;
        };
        
        class AmovPknlMrunSlowWrflDf: AidlPknlMstpSlowWrflDnon_G0S{
            speed = 0.65;
        };
        
        class AmovPknlMrunSlowWrflDfl: AmovPknlMrunSlowWrflDf{
            speed = 0.65;
        };
        
        class AmovPknlMrunSlowWrflDfr: AmovPknlMrunSlowWrflDfl{
            speed = 0.65;
        };
        
        class AmovPknlMrunSlowWrflDl: AmovPknlMrunSlowWrflDfl{		//crouched lowered combat
            speed = 0.4;
        };
        
        class AmovPknlMrunSlowWrflDr: AmovPknlMrunSlowWrflDfl{
            speed = 0.5;
        };
        
        class AmovPknlMtacSlowWrflDl: AmovPknlMrunSlowWrflDl{
            speed = 0.8;
        };
        
        class AmovPknlMrunSlowWrflDb: AmovPknlMrunSlowWrflDfl{
            speed = 0.44;
        };
        
        class AmovPknlMrunSlowWrflDbl: AmovPknlMrunSlowWrflDfl{
            speed = 0.44;
        };
        
        class AmovPknlMrunSlowWrflDbr: AmovPknlMrunSlowWrflDfl{
            speed = 0.44;
        };
        
        class AmovPknlMtacSlowWrflDr: AmovPknlMrunSlowWrflDr{
            speed = 0.95;
        };
        
        class AmovPknlMtacSlowWrflDb: AmovPknlMrunSlowWrflDb{
            speed = 1.1;
        };
        
        class AmovPknlMtacSlowWrflDbl: AmovPknlMrunSlowWrflDbl{
            speed = 1.1;
        };
        
        class AmovPknlMtacSlowWrflDbr: AmovPknlMrunSlowWrflDbr{
            speed = 1.1sa;
        };
        
        class AmovPercMtacSlowWrflDf_ver2: AmovPercMwlkSlowWrflDf_ver2{			//erect lowered combat
            speed = 0.84865;
            stamina = 0;
        };
        
        class AmovPercMtacSlowWrflDfl_ver2: AmovPercMwlkSlowWrflDfl_ver2{
            speed = 0.84865;
            stamina = 0;
        };
        
        class AmovPercMtacSlowWrflDl_ver2: AmovPercMtacSlowWrflDf_ver2{
            speed = 0.8;
        };
        
        class AmovPercMtacSlowWrflDr_ver2: AmovPercMtacSlowWrflDf_ver2{
            speed = 0.45;
        };
        
        class AmovPercMtacSlowWrflDb_ver2: AmovPercMtacSlowWrflDf_ver2{		//
            speed = 0.3;
        };
        
        class AmovPercMtacSlowWrflDbl_ver2: AmovPercMtacSlowWrflDb_ver2{
            speed = 0.3;
        };
        
        class AmovPercMtacSlowWrflDbr_ver2: AmovPercMtacSlowWrflDb_ver2{
            speed = 0.3;
        };
        
        class AmovPknlMtacSrasWlnrDl: AmovPknlMtacSrasWlnrDf{
            speed = 0.977778;
        };
        
        class AmovPknlMtacSrasWlnrDr: AmovPknlMtacSrasWlnrDf{
            speed = 0.902811;
        };
        
        class AmovPknlMtacSrasWlnrDb: AmovPknlMtacSrasWlnrDf{
            speed = 0.9;
        };
        
        class AmovPknlMtacSlowWrflDfr: AmovPercMtacSlowWrflDfr_ver2{
            speed = 1.45;
        };
        
        class AmovPknlMstpSrasWrflDnon: AmovPknlMstpSlowWrflDnon{
            stamina = 2;
        };
        
        class AmovPknlMrunSrasWrflDf: AmovPknlMstpSrasWrflDnon{	//run crouched
            stamina = -0.2;
        };
        
        class AmovPercMwlkSlowWrflDl: AmovPercMwlkSlowWrflDf{
            speed = 0.206897;
            stamina = 1;
        };
        
        class AmovPercMwlkSlowWrflDr: AmovPercMwlkSlowWrflDf{
            speed = 0.406897;
            stamina = 1;
        };
        
        class AmovPknlMrunSrasWrflDfl: AmovPknlMrunSrasWrflDf{
            stamina = -0.3;
        };
        
        class AmovPercMrunSlowWrflDf: AmovPercMstpSlowWrflDnon{   // run weapon lowered
            speed = 0.77;
            stamina = 0;
        };
        
        class AmovPercMrunSlowWrflDl: AmovPercMrunSlowWrflDfl{	/// strafe and back lowered
            speed = 0.45;
            stamina = -0.05;
        };
        
        class AmovPercMrunSlowWrflDb: AmovPercMrunSlowWrflDfl{
            speed = 0.45;
            stamina = -0.05;
        };
        
        class AmovPercMrunSlowWrflDbl: AmovPercMrunSlowWrflDfl{
            speed = 0.45;
            stamina = -0.05;
        };
        
        class AmovPercMrunSlowWrflDbr: AmovPercMrunSlowWrflDfl{
            speed = 0.45;
            stamina = -0.05;
        };
        
        class AmovPknlMrunSrasWrflDl: AmovPknlMrunSrasWrflDfl{		///
            speed = 0.4;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDr: AmovPknlMrunSrasWrflDfl{
            speed = 0.37;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDb: AmovPknlMrunSrasWrflDfl{
            speed = 0.45;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDb_ldst: AmovPknlMrunSrasWrflDb{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDbl: AmovPknlMrunSrasWrflDb{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDbl_ldst: AmovPknlMrunSrasWrflDb{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDbr: AmovPknlMrunSrasWrflDb{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDbr_ldst: AmovPknlMrunSrasWrflDbr{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDr_ldst: AmovPknlMrunSrasWrflDr{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSrasWrflDfr: AmovPknlMrunSrasWrflDfl{
            speed = 0.55;
            stamina = -0.1;
        };
        
        class AmovPknlMrunSrasWrflDfr_ldst: AmovPknlMrunSrasWrflDfr{
            stamina = -0.1;
        };
        
        class AmovPknlMtacSrasWrflDf: AmovPknlMrunSrasWrflDf{		// tac crouched
            speed = 0.45;
            stamina = -0.1;
        };
        /*
        class AmovPknlMtacSrasWrflDfl: AmovPknlMtacSrasWrflDf{
            stamina = -0.3;
        };
        */
        class AmovPknlMtacSrasWrflDfr: AmovPknlMtacSrasWrflDf{
            speed = 0.48;
            stamina = -0.1;
        };
        
        class AmovPknlMtacSrasWrflDl: AmovPknlMtacSrasWrflDf{		// strafe left crouched combat
            speed = 0.36;
            stamina = -0.3;
        };
        
        class AmovPknlMtacSrasWrflDb: AmovPknlMtacSrasWrflDf{
            speed = 0.38;
            stamina = -0.3;
        };
        
        class AmovPknlMtacSrasWrflDr: AmovPknlMtacSrasWrflDf{		// strafe right crouched combat
            speed = 0.38;
            stamina = -0.3;
        };
        
        class AmovPercMrunSrasWpstDl: AmovPercMrunSrasWpstDf{	//stafe pistol
            speed = 0.74;
            stamina = -0.1;
        };
        
        class AmovPercMrunSrasWpstDr: AmovPercMrunSrasWpstDf{
            speed = 0.76;
            stamina = -0.1;
        };
        
        class AmovPercMrunSrasWpstDb: AmovPercMrunSrasWpstDf{
            speed = 0.5;
        };
        
        class AmovPercMrunSlowWrflDr: AmovPercMrunSlowWrflDfl{
            speed = 0.45;
            stamina = -0.05;
        };
        
        class AmovPercMrunSlowWrflDfr: AmovPercMrunSlowWrflDfl{  /// end
            stamina = -0.05;
        };

        /// walk
        class AmovPercMwlkSrasWrflDf: AmovPercMstpSrasWrflDnon{
            speed = 1.25;
            stamina = 1;
        };
        
        class AmovPercMwlkSrasWrflDl: AmovPercMwlkSrasWrflDf{
            speed = 0.51;
            stamina = 1;
        };
        
        class AmovPercMwlkSrasWrflDr: AmovPercMwlkSrasWrflDf{
            speed = 0.46;
            stamina = 1;
        };
        
        class AmovPercMwlkSrasWrflDb: AmovPercMwlkSrasWrflDf{
            stamina = 1;
        };


        //combat pace
        class AmovPercMtacSrasWrflDf: AmovPercMwlkSrasWrflDf{
            speed = 0.65;
            stamina = -0.02;
        };
        
        class AmovPercMtacSrasWrflDl: AmovPercMtacSrasWrflDf{
            speed = 0.46;
        };
        
        class AmovPercMtacSrasWrflDr: AmovPercMtacSrasWrflDf{
            speed = 0.412;
        };
        
        class AmovPercMtacSrasWrflDb: AmovPercMtacSrasWrflDf{
            speed = 0.5;
        };


        class AmovPercMrunSrasWrflDl: AmovPercMrunSrasWrflDfl{		// strafe left
            speed = 0.45;
            stamina = -0.1;
        };
        
        class AmovPercMrunSrasWrflDb: AmovPercMrunSrasWrflDfl{
            speed = 0.45;
            stamina = -0.1;
        };
        
        class AmovPercMrunSrasWrflDr: AmovPercMrunSrasWrflDfl{		// strafe right
            speed = 0.47;
            stamina = -0.1;
        };
        
        class AmovPercMrunSrasWrflDl_ldst: AmovPercMrunSrasWrflDl{	//strafing left?
            speed = 0.8;
            stamina = -0.1;
        };
        
        class AmovPercMrunSrasWrflDr_ldst: AmovPercMrunSrasWrflDr{	//strafing right?
            speed = 0.8;
            stamina = -0.1;
        };
        
        class AmovPercMrunSnonWnonDl: AmovPercMrunSnonWnonDfl{
            speed = 0.50946;
            stamina = -0.1;
        };
        
        class AmovPercMrunSnonWnonDb: AmovPercMrunSnonWnonDl{
            speed = 0.5;
            stamina = -0.1;
        };
        
        class AmovPercMrunSnonWnonDr: AmovPercMrunSnonWnonDl{
            speed = 0.572059;
            stamina = -0.1;
        };
        
        class AmovPknlMrunSrasWrflDf_ldst: AmovPknlMrunSrasWrflDf{
            stamina = -0.1;
        };
        
        class AmovPknlMrunSrasWrflDfl_ldst: AmovPknlMrunSrasWrflDfl{
            stamina = -0.1;
        };
        
        class AmovPknlMrunSrasWrflDl_ldst: AmovPknlMrunSrasWrflDl{
            stamina = -0.1;
        };
        
        class AmovPercMrunSlowWpstDl: AmovPercMrunSrasWpstDl{
            speed = 0.8;
            stamina = -0.1;
        };
        
        class AmovPercMrunSlowWpstDr: AmovPercMrunSrasWpstDr{
            speed = 0.8;
            stamina = -0.1;
        };
        
        class AmovPknlMrunSrasWpstDf: AidlPknlMstpSrasWpstDnon_G0S{
            speed = 0.6;
        };
        
        class AmovPknlMrunSrasWpstDl: AmovPknlMrunSrasWpstDf{
            speed = 0.64;
            stamina = -0.1;
        };
        
        class AmovPknlMrunSrasWpstDb: AmovPknlMrunSrasWpstDf{
            speed = 0.5;
            stamina = -0.1;
        };
        
        class AmovPercMtacSrasWpstDf: AmovPercMwlkSrasWpstDf{		//pistol combat
            speed = 0.65;
        };
        
        class AmovPercMtacSrasWpstDl: AmovPercMtacSrasWpstDf{
            speed = 1.2;
        };
        
        class AmovPercMtacSrasWpstDr: AmovPercMtacSrasWpstDf{
            speed = 1;
        };
        
        class AmovPercMtacSrasWpstDb: AmovPercMtacSrasWpstDf{		//
            speed = 0.6;
        };
        
        class AmovPknlMrunSrasWpstDr: AmovPknlMrunSrasWpstDf{
            speed = 0.6;
            stamina = -0.1;
        };
        
        class AmovPknlMtacSrasWpstDf: AmovPknlMwlkSrasWpstDf{
            speed = 0.6;
        };
        
        class AmovPknlMtacSrasWpstDl: AmovPknlMtacSrasWpstDf{
            speed = 1;
        };
        
        class AmovPknlMtacSrasWpstDr: AmovPknlMtacSrasWpstDf{
            speed = 0.9;
        };
        
        class AmovPknlMtacSrasWpstDb: AmovPknlMtacSrasWpstDf{
        speed = 0.5;
        };
        
        class AmovPercMrunSlowWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S{		//launcher
            speed = 0.67;
            stamina = -0.3;
        };
        
        class AmovPercMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf{
            speed = 0.85;
            stamina = -1.4;
        };
        
        class AmovPercMevaSlowWlnrDfr: AmovPercMevaSlowWlnrDf{
            speed = 0.85;
            stamina = -1.4;
        };
        
        class AmovPercMevaSlowWlnrDfl: AmovPercMevaSlowWlnrDf{
            speed = 0.85;
            stamina = -1.4;
        };
        
        class AmovPknlMstpSrasWlnrDnon: Default{
        };
        
        class AmovPercMstpSrasWlnrDnon: AmovPknlMstpSrasWlnrDnon{
        };
        
        class AmovPknlMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf{
        };
        
        class AmovPknlMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf{		///////////////
        };
        
        class AmovPknlMrunSrasWlnrDfr: AmovPercMrunSlowWlnrDfr {
            speed = 0.6;
        };
        
        class AmovPknlMrunSrasWlnrDl: AmovPknlMrunSrasWlnrDf{
            speed = 1.12;
        };
        
        class AmovPknlMrunSrasWlnrDr: AmovPknlMrunSrasWlnrDf{
            speed = 1;
        };

        class AmovPercMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf{
            speed = 0.8;
            stamina = -0.3;
        };
        
        class AmovPercMrunSrasWlnrDl: AmovPercMrunSrasWlnrDf{
            speed = 1.3;
        };
        
        class AmovPercMrunSrasWlnrDr: AmovPercMrunSrasWlnrDf{
            speed = 1.16;
        };
        
        class AmovPercMrunSrasWlnrDb: AmovPercMrunSlowWlnrDb{
            speed = 0.6;
        };
        
        class AmovPercMrunSrasWlnrDfl: AmovPercMrunSrasWlnrDf{
            speed = 0.75;
        };

        class AmovPknlMrunSnonWnonDf: AmovPknlMstpSnonWnonDnon{
            stamina = 1.7;
        };
        
        class AmovPknlMwlkSnonWnonDf: AmovPknlMrunSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMwlkSnonWnonDbr: AmovPknlMwlkSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMwlkSnonWnonDbl: AmovPknlMwlkSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMwlkSnonWnonDb: AmovPknlMwlkSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMwlkSnonWnonDl: AmovPknlMwlkSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMwlkSnonWnonDr: AmovPknlMwlkSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMwlkSnonWnonDfr: AmovPknlMwlkSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMwlkSnonWnonDfl: AmovPknlMwlkSnonWnonDf{
            stamina = -0.05;
        };
        
        class AmovPknlMstpSnonWnonDnon_AmovPknlMrunSnonWnonDf: AmovPknlMrunSnonWnonDf{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWnonDfl: AmovPknlMrunSnonWnonDf{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWnonDl: AmovPknlMrunSnonWnonDf{
            speed = 0.525;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWnonDr: AmovPknlMrunSnonWnonDf{
            speed = 0.525;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWnonDbl: AmovPknlMrunSnonWnonDf{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWnonDb: AmovPknlMrunSnonWnonDf{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWnonDbr: AmovPknlMrunSnonWnonDf{
            stamina = -0.2;
        };
        
        class AmovPpneMrunSlowWrflDf: AmovPpneMstpSrasWrflDnon{          		//run prone
            speed = 0.9;
            stamina = 0;
        };
        
        class AmovPpneMevaSlowWrflDf: AmovPpneMrunSlowWrflDf{		//prone fast
            stamina = -0.5;
        };
        
        class AmovPercMevaSrasWrflDf: SprintBaseDf{  // sprint weapon up
            speed = 1.8;
            stamina = -2;
        };
        
        class AmovPercMevaSlowWrflDf: AmovPercMevaSrasWrflDf{  // sprint weapon down
        };
        
        class AmovPknlMevaSrasWrflDfl: AmovPknlMevaSrasWrflDf{	//
            stamina = -1;
        };
        
        class AmovPknlMevaSrasWrflDfr: AmovPknlMevaSrasWrflDf{
            stamina = -1;
        };
        
        class AmovPercMevaSnonWnonDf: SprintCivilBaseDf{
            speed = 1.5;
            stamina = -0.6;
        };
        
        class AmovPercMevaSnonWnonDfl: AmovPercMevaSnonWnonDf{
            speed = 1.4;
            stamina = -0.6;
        };
        
        class AmovPercMevaSnonWnonDfr: AmovPercMevaSnonWnonDf{
            speed = 1.4;
            stamina = -0.6;
        };
        
        class AmovPercMrunSnonWbinDf: AmovPercMwlkSoptWbinDf{	/// Run with binos
            stamina = -0.025;
        };
        
        class AmovPercMrunSnonWbinDfr: AmovPercMrunSnonWbinDf{
            stamina = -0.025;
        };
        
        class AmovPercMrunSnonWbinDr: AmovPercMrunSnonWbinDf{
            speed = 0.47;
            stamina = -0.025;
        };
        
        class AmovPercMrunSnonWbinDb: AmovPercMrunSnonWbinDf{
            speed = 0.55;
            stamina = -0.025;
        };
        
        class AmovPercMrunSnonWbinDbr: AmovPercMrunSnonWbinDb{
            stamina = -0.025;
        };
        
        class AmovPercMrunSnonWbinDbl: AmovPercMrunSnonWbinDb{
            stamina = -0.025;
        };
        
        class AmovPercMrunSnonWbinDl: AmovPercMrunSnonWbinDf{
            speed = 0.5;
            stamina = -0.025;
        };
        
        class AmovPercMrunSnonWbinDfl: AmovPercMrunSnonWbinDf{	/// end run with bino
            stamina = -0.025;
        };
        
        class AmovPercMevaSnonWbinDf: AmovPercMrunSnonWbinDf{
            speed = 1.28;
            stamina = -1.1;
        };
        
        class AmovPercMevaSnonWbinDfl: AmovPercMevaSnonWbinDf{
            speed = 1.28;
            stamina = -0.6;
        };
        
        class AmovPercMevaSnonWbinDfr: AmovPercMevaSnonWbinDf{
            speed = 1.28;
            stamina = -0.6;
        };
        
        class AovrPercMstpSoptWbinDf: AmovPercMstpSoptWbinDnon{
            stamina = -2;
        };
        
        class AmovPknlMstpSoptWbinDnon: AmovPknlMstpSrasWrflDnon{
            stamina = 2;
        };
        
        class AmovPknlMrunSnonWbinDf: AmovPknlMwlkSoptWbinDf{
            speed = 0.64;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWbinDr: AmovPknlMrunSnonWbinDf{
            speed = 0.5;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWbinDl: AmovPknlMrunSnonWbinDf{
            speed = 0.5;
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWbinDb: AmovPknlMrunSnonWbinDf{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWbinDbr: AmovPknlMrunSnonWbinDb{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWbinDbl: AmovPknlMrunSnonWbinDb{
            stamina = -0.2;
        };
        
        class AmovPknlMrunSnonWbinDfl: AmovPknlMrunSnonWbinDf{
            stamina = -0.2;
        };
        
        class AmovPknlMevaSnonWbinDf: AmovPknlMrunSnonWbinDf{
            speed = 1.4;
            stamina = -1;
        };
        
        class AmovPknlMevaSnonWbinDfl: AmovPknlMevaSnonWbinDf{
            speed = 1.4;
            stamina = -1;
        };
        
        class AmovPknlMevaSnonWbinDfr: AmovPknlMevaSnonWbinDf{
            speed = 1.4;
            stamina = -1;
        };
        
        class AmovPercMevaSnonWbinDfl_rfl: AmovPercMevaSnonWbinDf_rfl{
            speed = 1.2;
        };
        
        class AmovPercMevaSnonWbinDfr_pst: AmovPercMevaSnonWbinDf_pst{
            speed = 1.2;
        };
        
        class AmovPknlMevaSnonWbinDf_pst: AmovPknlMrunSnonWbinDf_pst{
            speed = 1.2;
        };



        //weapon lowered
        class AmovPercMstpSlowWrflDnon_AmovPercMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon{
            speed = 4.05539; //3.05539
        };
        
        class AmovPercMstpSrasWrflDnon_AmovPercMstpSlowWrflDnon: AmovPercMstpSlowWrflDnon{
            speed = -0.3; //-0.5
        };
        
        class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon: TransAnimBase{
            speed = 1.4875; // 1.1875
        };
        
        class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSnonWnonDnon: AmovPknlMstpSnonWnonDnon{
            speed = 0.641176; //0.441176
        };
        
        class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSlowWrflDnon: AmovPknlMstpSlowWrflDnon{
            speed = -0.45; //-0.65
        };
        
        class AmovPknlMstpSlowWrflDnon_AmovPknlMstpSrasWrflDnon: AmovPknlMstpSrasWrflDnon{
            speed = 3.29714; //2.29714
        };

        //fall sideways
        class AmovPercMstpSrasWrflDnon_AadjPpneMstpSrasWrflDleft: AadjPpneMstpSrasWrflDleft{
            speed = -1.2; //-1.6
        };
        
        class AmovPercMstpSrasWrflDnon_AadjPpneMstpSrasWrflDright: AadjPpneMstpSrasWrflDright{
            speed = -1.2; //-1.6
        };
        
        class AmovPknlMstpSrasWrflDnon_AadjPpneMstpSrasWrflDright: AadjPpneMstpSrasWrflDright{
            speed = -1.3; //-1.8
        };
        
        class AmovPknlMstpSrasWrflDnon_AadjPpneMstpSrasWrflDleft: AadjPpneMstpSrasWrflDleft{
            speed = -1.3; //-1.6
        };


        //ladder climbing
        class LadderCivilStatic: StandBase{
            speed = 2; //1e+010
        };
        
        class LadderCivilOn_Top: LadderCivilStatic{
            speed = 3;
        };
        
        class LadderCivilOn_Bottom: LadderCivilStatic{
            speed = 3;
        };
        
        class LadderCivilUpLoop: LadderCivilStatic{
            speed = 2.1;
        };
        
        class LadderCivilTopOff: LadderCivilUpLoop{
            speed = 3;
        };


        //swimming
        class AswmPercMstpSnonWnonDnon_AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon{
            speed = 0.8; //0.7;
        };
        
        class AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon{
            stamina = -0.2;
            speed = 0.45; //0.35
        };
        
        class AswmPercMsprSnonWnonDf: AswmPercMrunSnonWnonDf{
            stamina = -0.5;
            speed = 0.55; //0.46;
        };
        
        class AmovPercMstpSrasWrflDnon_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon{
            speed = 0.8; //0.441176; //0.7;
        };

        // Surface swim
        class AsswPercMrunSnonWnonDf: AsswPercMstpSnonWnonDnon{
            stamina = -0.2;
            speed = 0.45;
        };
        
        class AsswPercMsprSnonWnonDf: AsswPercMrunSnonWnonDf{
            stamina = -0.5;
            speed = 0.55;
        };
    };
};
