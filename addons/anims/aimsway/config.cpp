#include "..\macros.hpp"

class CfgPatches {
    class DOUBLE(ADDON,AIMSWAY) {
        addonRootClass = QUOTE(ADDON);
        requiredAddons[] = {QUOTE(ADDON)};
        units[] = {};
        weapons[] = {};
    };
};

class CfgMovesBasic { };
class CfgMovesMaleSdr: CfgMovesBasic {
    class StandBase;
    class Default;
    class States {

        class AmovPercMstpSlowWrflDnon: StandBase {
            aimPrecision  = 0.4;
        };

        class AmovPercMstpSrasWrflDnon: AmovPercMstpSlowWrflDnon {	//standing weapon raised
            aimPrecision  = 1;
        };

        class AmovPercMrunSrasWrflDf: AmovPercMstpSrasWrflDnon { 	// run weapon up
            aimPrecision  = 4;
        };

        class AmovPknlMstpSlowWrflDnon: AmovPercMstpSlowWrflDnon {
            aimPrecision  = 0.5;
        };

        class AmovPknlMstpSrasWrflDnon: AmovPknlMstpSlowWrflDnon {
            aimPrecision = 1;
        };

        class AmovPknlMrunSrasWrflDf: AmovPknlMstpSrasWrflDnon {	//run crouched
            aimPrecision = 5;
        };

        class AmovPercMrunSlowWrflDf: AmovPercMstpSlowWrflDnon {   // run weapon lowered
            aimPrecision  = 3;
        };

        class AmovPknlMrunSrasWrflDfl;
        class AmovPknlMrunSrasWrflDl: AmovPknlMrunSrasWrflDfl {		///
            aimPrecision  = 6;
        };

        class AmovPknlMrunSrasWrflDr: AmovPknlMrunSrasWrflDfl {
            aimPrecision  = 6;
        };

        class AmovPknlMtacSrasWrflDf: AmovPknlMrunSrasWrflDf {		// tac crouched
            aimPrecision = 6;
        };

        class AmovPknlMtacSrasWrflDl: AmovPknlMtacSrasWrflDf {		// strafe left crouched combat
            aimPrecision  = 6;
        };

        class AmovPknlMtacSrasWrflDb: AmovPknlMtacSrasWrflDf {
            aimPrecision  = 6;
        };

        class AmovPknlMtacSrasWrflDr: AmovPknlMtacSrasWrflDf {		// strafe right crouched combat
            aimPrecision  = 6;
        };
        /// walk
        class AmovPercMwlkSrasWrflDf: AmovPercMstpSrasWrflDnon {
            aimPrecision  = 1;
        };

        class AmovPercMtacSrasWrflDf;
        class AmovPercMtacSrasWrflDl: AmovPercMtacSrasWrflDf {
            aimPrecision  = 6;
        };

        class AmovPercMtacSrasWrflDr: AmovPercMtacSrasWrflDf {
            aimPrecision  = 6;
        };

        class AmovPercMtacSrasWrflDb: AmovPercMtacSrasWrflDf {
            aimPrecision  = 6;
        };

        class AmovPercMrunSrasWrflDfl;
        class AmovPercMrunSrasWrflDl: AmovPercMrunSrasWrflDfl {		// strafe left
            aimPrecision = 6;
        };

        class AmovPercMrunSrasWrflDb: AmovPercMrunSrasWrflDfl {
            aimPrecision = 6;
        };

        class AmovPercMrunSrasWrflDr: AmovPercMrunSrasWrflDfl {		// strafe right
            aimPrecision  = 6;
        };

        class AmovPercMrunSlowWlnrDf;
        class AmovPercMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf {
            aimPrecision = 5;
        };

        class AmovPknlMstpSrasWlnrDnon;
        class AmovPercMstpSrasWlnrDnon: AmovPknlMstpSrasWlnrDnon {
            aimPrecision = 1.75;
        };

        class AmovPknlMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf {
            aimPrecision = 6;
        };

        class AmovPercMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf {
            aimPrecision = 4;
        };

        class SprintBaseDf;
        class AmovPercMevaSrasWrflDf: SprintBaseDf {  // sprint weapon up
            aimPrecision = 14; //4.5
        };
        class AmovPercMevaSlowWrflDf: AmovPercMevaSrasWrflDf {  // sprint weapon down
            aimPrecision = 14; //3.5
        };


        // -- STANCES
        class AidlPknlMstpSrasWrflDnon_G0S;
        class AadjPknlMstpSrasWrflDup: AidlPknlMstpSrasWrflDnon_G0S {
            aimPrecision = 1; // 0.5;
        };
    };
};
