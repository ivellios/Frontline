/*
    Function:       FRL_MissionSelection_fnc_serverInit
    Author:         Adanteh
    Description:    Adds events for automatic mission rotation, getting mission list from python
*/
#include "macros.hpp"


// -- Keep a list of the last 3 played  mission (Do on start, so we also use it when admin switches mission directly)''
["MissionStarted", {
    private _lastMissionsPlayed = uiNamespace getVariable [QGVAR(lastMissions), []];
    _lastMissionsPlayed pushBack toLower (format ["%1.%2", missionName, worldName]);
    if (count _lastMissionsPlayed > 4) then {
        _lastMissionsPlayed deleteAt 0;
    };
    uiNamespace setVariable [QGVAR(lastMissions), _lastMissionsPlayed];
}] call CFUNC(addEventHandler);

if (isDedicated) then {
    ["EndMission", {
        [{
            if (GVAR(nextMissionSelected) != "") exitWith {
                [GVAR(nextMissionSelected), "Admin-selected"] call MFUNC(adminChangeMission);
            };

            private _playerCount = count allPlayers;
            private _missionList = call FUNC(getMissionList);
            if (_missionList isEqualTo []) exitWith { };

            private _lastMissionsPlayed = uiNamespace getVariable [QGVAR(lastMissions), []];
            private _missionListFiltered = _missionList select {
                _x params ["_gamemode", "_filename", "_minPlayers", "_maxPlayers"];
                ((_gamemode == "DFL") && {_playerCount >= _minPlayers} && { _playerCount < _maxPlayers }) && { !((toLower _filename) in _lastMissionsPlayed) }
            };

            if (_missionListFiltered isEqualTo []) then {
                (format ["<ADMIN> ERROR in autorotation filter, fallback to full mission (Might bug out)! %1", serverTime]) call MFUNC(diagLog);
                _missionListFiltered = _missionList;
            };

            // -- Shuffle this list a bunch of times (SelectRandom has a habit of picking entries at the start)
            private _cnt = count _missionListFiltered;
            for "_i" from 1 to 3 do {
                for "_j" from 1 to _cnt do {
                    _missionListFiltered pushBack (_missionListFiltered deleteAt floor random _cnt);
                };

                // -- Split it somewhere in the first half and put it back at the end
                private _randomSelect = floor random (40 min _cnt);
                private _firstHalf = [];
                for "_h" from 1 to _randomSelect do {
                    _firstHalf pushBack (_missionListFiltered deleteAt 0);
                };
                _missionListFiltered append _firstHalf;
            };

            private _missionToPlay = selectRandom _missionListFiltered;
            _missionToPlay params ["_displayName", "_filename"];
            [_filename, "Autorotation"] call MFUNC(adminChangeMission);
        }, 9.5] call CFUNC(wait); // 11.5 seconds for endMission function
    }] call CFUNC(addEventHandler);
};

// -- Sets mission as next one
GVAR(nextMissionSelected) = "";
[QGVAR(setNextMission), {
    (_this select 0) params ["_mission", "_caller"];
    (format ["<ADMIN> Next mission set to '%1' by '%2' at %3", _mission, _caller, serverTime]) call MFUNC(diagLog);
    if (_mission == "<Autorotation>") then {
        _mission = "";
    };
    GVAR(nextMissionSelected) = _mission;
}] call CFUNC(addEventhandler);

// -- Changes mission through admin panel right now
[QGVAR(setMissionNow), {
    (_this select 0) params ["_mission", "_caller"];
    (format ["<ADMIN> Instant mission switch to '%1' by '%2' at %3", _mission, _caller, serverTime]) call MFUNC(diagLog);
    if (_mission == "<Autorotation>") then {
        _mission = "";
    };

    GVAR(nextMissionSelected) = _mission;
    // -- End whatever gamemode is running
    [QSVAR(endMissionAdmin), [Clib_Player]] call CFUNC(globalEvent);

}] call CFUNC(addEventhandler);
