/*
    Function:       FRL_MissionSelection_fnc_loadList
    Author:         Adanteh
    Description:    Does stuff
*/
#include "macros.hpp"

["requestRemoteValue", [QGVAR(missionList), { call FUNC(getMissionList) }]] call CFUNC(localEvent);
private _data = [["-", "<Autorotation>", -1, -1, "Autorotate", "-"]];
if (GVAR(missionList) isEqualTo []) then {
    _data pushBack ["-", "-", -1, -1, "Reopen this settings window to show mission list", "-", "info"];
} else {
    _data append GVAR(missionList);
};

private _select = -1;
private _currentPlayers = count allPlayers;
_dropdown setVariable ["data", _data];
{
    _x params ["_gamemode", "_filename", "_minPlayers", "_maxPlayers", "_missionName", "_terrainClass", ["_meta", "##"]];

    // -- Get a somewhat readable name for the gggiven terain class
    private _terrainName = GVAR(namespace) getVariable [_terrainClass, "-"];
    if (_terrainName isEqualTo "-") then {
        if !(_terrainClass isEqualTo "-") then {
            _terrainName = getText (configFile >> "CfgWorlds" >> _terrainClass >> "Description");
            if (_terrainName find "WW2: " == 0) then { // -- Remove WW2 prefix, makes it easier to read
                _terrainName = _terrainName select [5, count _terrainName - 5];
            };
            if (count _terrainName > 10) then {
                _terrainName = _terrainName select [0, 10];
            };
            GVAR(namespace) setVariable [_terrainClass, _terrainName];
            _missionName = [_missionName, _terrainName] joinString ", ";
        };
    };
    if !(_gamemode isEqualTo "-") then {
        _missionName = format ["%1 (%2)", _missionName, _gamemode];
    };

    private _inRange = (_currentPlayers >= _minPlayers) && (_currentPlayers < _maxPlayers);
    if !([_minPlayers, _maxPlayers] isEqualTo [-1, -1]) then {
        _missionName = format ["%1 [%2-%3]", _missionName, _minPlayers, _maxPlayers];
    };

    private _lbIndex = _dropdown lbAdd _missionName;
    _dropdown lbSetColor [_lbIndex, [[1, 0.5, 0.5, 1], [1, 1, 1, 1]] select _inRange];
    _dropdown lbSetData [_lbIndex, _meta];

    if (_filename == GVAR(currentSelected)) then { // -- Reselect previously selected
        _select = _forEachIndex;
    };
} forEach _data;
if (_select != -1) then {
    _dropdown lbSetCurSel _select;
};
