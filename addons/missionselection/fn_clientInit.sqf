/*
    Function:       FRL_MissionSelection_fnc_clientInit
    Author:         Adanteh
    Description:    Allows selecting next mission easily if you are an admin
*/
#include "macros.hpp"

GVAR(missionList) = [];
GVAR(currentSelected) = "<Autorotation>";
GVAR(namespace) = false call CFUNC(createNamespace);

// -- When you become admin, retrieve the mission list
["adminChanged", {
    ["requestRemoteValue", [QGVAR(missionList), { call FUNC(getMissionList) }]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

// -- Also get it at start anyway (Because the other event doesn't seem to work propelry when you were already logged in)
[{
    if (serverCommandAvailable "#kick") then {
        ["requestRemoteValue", [QGVAR(missionList), { call FUNC(getMissionList) }]] call CFUNC(localEvent);
    };
}, 20] call CFUNC(wait);

// -- Gets the mission list from the server
['add', ["admin", 'Admin Settings', 'Mission', [
    'Select mission',
    'Select a mission',
    QMVAR(CtrlCombo),
	{ true },
    { true },
    { // Selection changed
        params ['_control', '_index'];
        private _data = _control getVariable ["data", []];
        private _selected = _data param [_index, ["", ""]];
        if (_selected param [6, "##"] isEqualTo "info") exitWith { };
        GVAR(currentSelected) = _selected select 1;
    },
    { // Onload
        _this call FUNC(loadList);
    }
]]] call MFUNC(settingsWindow);

['add', ["admin", 'Admin Settings', 'Mission', [
    'Set Next',
    'Sets the selection mission as next one',
    QMVAR(CtrlButton),
    true,
    { [Clib_Player] call MFUNC(isAdmin); },
    { [QGVAR(setNextMission), [GVAR(currentSelected), Clib_Player]] call CFUNC(serverEvent) },
    ["SET NEXT"]
]]] call MFUNC(settingsWindow);


['add', ["admin", 'Admin Settings', 'Mission', [
    'Switch',
    'Switch to selected mission now',
    QMVAR(CtrlButton),
    true,
    { [Clib_Player] call MFUNC(isAdmin); },
    { [QGVAR(setMissionNow), [GVAR(currentSelected), Clib_Player]] call CFUNC(serverEvent) },
    ["SWITCH NOW"]
]]] call MFUNC(settingsWindow);
