/*
    Function:       FRL_MissionSelection_fnc_getMissionList
    Author:         Adanteh
    Description:    Gets list of valid mission (Contain metadata.hpp) in MPMissions folder of current Arma3.exe
*/
#include "macros.hpp"

if (isNil "py3_fnc_callExtension") exitWith { [["-", "ERROR Retrieving missions. Missionfinder broken?", 0, 99, "info"]] };
private _missionList = [format ["python.MissionFinder.get_missions"]] call py3_fnc_callExtension;
if (isNil "_missionList") exitWith { [["-", "ERROR Retrieving missions. Missionfinder broken?", 0, 99, "info"]] };

// -- Process the thing
private _missionListProcessed = [];
{
    _x params ["_missionName", "_data"];
    private _missionData = ["", "", 0, 999, _missionName];

    {
        _x params ["_key", "_value"];
        switch (toLower _key) do {
            case "gamemode": {
                _missionData set [0, _value];
            };
            case "filename": {
                 // Remove extension
                _missionData set [1, (_value select [0, count _value - 4])];
            };
            case "minplayers": {
                _missionData set [2, _value];
            };
            case "maxplayers": {
                _missionData set [3, _value];
            };
        };
    } forEach _data;

    // -- Only include terrains loded in on server
    private _filename = _missionData param [1, ""];
    private _terrainName = _filename splitString ".";
    _terrainName = _terrainName param [count _terrainName - 1, ""];
    if (isClass (configFile >> "CfgWorlds" >> _terrainName)) then {
        _missionData set [5, _terrainName];
        _missionListProcessed pushBack _missionData;
    };

    nil;
} count _missionList;
_missionListProcessed;
