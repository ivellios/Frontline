# HOW TO CONTRIBUTE

Give Gunther dank server donations. Buy beer and cheese for Adanteh.
You could also check the list below to see if there's anything on it that you can help with.

Look at the Third Party issues and see if there's anything in there you can help with.
Do you know modelling and/or texturing and/or got experience working on terrains? Then you can help us for sure.


## List

### Terrain Making
A little warning that this is a task that needs some persistence. It's not a thing that you're going to figure out in one day,
and to actually see it in-game all tasks below need to be done.

  * [ ] Creating objects (Buildings, vegetation, walls and so on) (Modelling, texturing)
  * [ ] Placing objects (Using XCAM) - Very easy to learn, can just randomly do little bits and can even do it in MP
  * [ ] Editing heightmaps (Using L3DT, or finding and importing real world data) (No specific skill required, just some googlefu)
  * [ ] Editing satellite pictures (Photoshop, importing real world data)


### Coding
Most of this is SQF specific, which first requires you to have a pretty good knowledge of SQF.
You should primarily be thinking of a separate module here that we can introduce at any time, this means it's quite an independent task.

  * [ ] Persistent player profiles (Keep player scores)
  * [ ] Additional gamemodes (A&D, S&D, something new?)
  * [ ] Mission voting (Allow voting on a mission at the end of a round)
  * [ ] Vehicle mechanics (Better damage effects, logistics, )
  * [ ] Mortar/artillery system
  * [ ] Commander mode
  * [ ] Tutorial/training mode (SP)
  * [ ] In-game manual

### Modelling / Texturing / Other 3D
  * [ ] Creating fortifications (Bunkers, little sandbag walls and so on)
  * [ ] Enterable/non-enterable buildings for terrains (Either existing or new)
  * [ ] Creating animations for medical treatments
  * [ ] Making medical item models and setting them up as equipable 'weapon' and other similar items


### Animating
 * [ ] Making gestures for using items, medical options and more.
 * [ ] Different sitting animations for vehicles (Lower profile)

### Sound design
 * [ ] Better radio sounds for FOs (Static, maybe WW2 songs for IFA)




## A little disclaimer
We don't mind helping you to get going, but generally speaking for us to provide full support we need some indication that you're serious with helping.
This saves us hours from explaining everything and discussing details with someone who is gone again the next day, which has happened many times in the past.
