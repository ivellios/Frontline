name = "FRL - Frontline";
picture = "\pr\frl\addons\main\ui\frontline_logo_256.paa";
actionName = "Website";
action = "http://www.frontline-mod.com";
description = "Frontline v0.1";
logo = "\pr\frl\addons\main\ui\frontline_logo_64.paa";
logoOver = "\pr\frl\addons\main\ui\frontline_logo_64.paa";
tooltip = "Frontline";
tooltipOwned = "Frontline Owned";
overview = "Frontline, the coolest gamemode ever created.";
author = "Frontline Team";
overviewPicture = "\pr\frl\addons\main\ui\frontline_logo_64.paa";
