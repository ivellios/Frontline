## 1.0.1.0
* Fix Shotgun and CQC SL kit for USMC
* Increased hit sound level
* Removed grenades from LAT, HAT and AR
* Adjusted AA damage and helo flare count
* Removed explosion damage on vehicles
* Adjusted vehicle smoke particles
* Remove morphine / CPR for now (#445)
* Remove block timer on vehicpe spawning (#446)
* Fix broken capture warning (#429)
* Fix (attempt) for unit in underwear (#451 - Will need mission update)
* FIx (attempt) for broken squad invite (#465)
* Fix (attempt) for stuck on radio broadcast (#350)
* Rebalance vehicle rearm times
* Add earplugs with F1 keybind, plus auto earplug setting under FRL Settings (#457)
* Fix vehicles not getting repaired when only turret is damaged (#466)
* Make HEAT and HEDP presets for LAT
* Change scope for US Marksman
* Reduced Toolkit mass so it's easier to put in vest (#459)
* Update Insurgency gear configs
* Fix unlimited STG44 in Sturmtrooper squads
* Fix bradley having wrong TOWs (#462)
* Remove pointless backpack with extra ammo for LAT (#452)
* Fix spawn in vehicle for MRAP (#430)
* Add better helo type detection for respawn times and spawn in vehicle
* Fix spawning in corner of map when trying to spawn on full vehicle (#463)

## 1.0.0.3
* Update Squad Radar background
* Fix MRAP not coming with spawnable seats (#430)
* Rebalance FO placing distances
* Fix tickets not going down when spawning on vehicle (#431)
* Fix being able to spawn on a blocked vehicle (#427)
* Add a warning when friendly zone capture drops below 75% (#429)
* Fix broken F door opening on A3 building (#430)
* Add Request Join option to for locked squads (#395)
* Add 'CLAIM SL' button when old SL left group (#433)
* Add gear configs for Final Front WW2 Weapons made by Diabolical
* Update to latest Clib version
* Fix incorrect apostrophe in hints text (#380)
* Fix (hopefully) long spawn timer when connecting to an ongoing mission (#428)
* Fix WW2 Soldiers spamming chat with AI messages (#221)

## 1.0.0.2
* Added spawning on vehicles (IFVs, APCs, trucks)
* Enable environment sound with new command (#332)
* Add IFA winter gear
* Add empty soldier classes for better Performance
* Add extra options under admin menu (#419)
* Add better vehicle category detection for WW2 stuff
* Fix for resetting squad type selection (#408)
* Fix instant bandaging finish when pressing Q or E (#404)
* Fix broken FO and RP blocking
* Fix a missing pixel on respawn screen hints (#407)
* Balanced WW2 Anti-air
* Reduce IFV ammo (RHS)
* Reduce bradley zeroing
* Reduce length of clutter on WW2 terrains
* Reduce magagazine count slightly for RHS gear
* Made Humvee (RHS) more sluggish

## Frontline to 1.0.0.0
* Added GUER RHS Faction
* Added Iron Front gear factions
* Add rebalanced recoil for IFA
* Rebalance FO placement distances
* Reduce FO Sound range
* Fix for BIS recompile on event handlers
* Fix ammo count text overlap on machine mugs (#381)
* Add highlighting of your on role in respawn screen (#382)
* Fix progress bar and voting HUD disappearing
* Fix broken prevention of switching to smaller team (#359)
* Add loading screen images for IFA terrains
* Add audiocustoms Wehrmacht rallypoint
* Fix backpack coming with items that belong to role on spawn
* Add better way to unlock custom squad types (Player threshold per squad)
* Reduce strafing speed
* Added extra icons for IFA roles
* Add automatic map markers for mission-placed buildings
* Fix a bunch of RPT errors
* Reduced Bradley ammo count
* Fix ctrl+click waypoint for squad leaders
* Fix error in scoring code
* Fix errors in particle (pebomb, minedust)
* Fix broken FO / RP Blocking (#368)
* Increase synced spawn timer at the start of the round, for battleprep (#399)
* Fix lack of ammo or too few AT rockets (#393)
* Add hints in the respawn screen (#383)
* Fix (Attempt) at broken keybinds/radio (#350)
* Update mission lobby selection pictures
* Add time multiplier (Longer dusk) in mission weather settings
* Replace M16s by M4s for maximum balance

## 0.9.0.5
* Fix RHS leftover vehicle hud (#337)
* Fix vehicle spawn conditions (#375)
* Made body cleanup stricter (#369)
* Made 'switch team' button more obvious (#372)
* Preload terrain for selected spawn location (#370)
* Fix incorrect highlighting for role icon (#363)
* Add check if player has Apex DLC (#371)
* Fix CPR/Drag buttons staying on screen (#361)
* Fix progress bar shown when dragging (#361)
* Fix CPR/Drag buttons overlapping with bandage buttons (#361)
* Fix centering of 'You are being bandaged' message
* Fix incorrect unassigned unit number (#362)
* Fix the loading screen hint changing too much (#366)
* Fix scoreboard scrollbar overlapping with text (#365)
* Added mutex to RP deployment (Might fix some issues
* Added function for common vehicle spawn conditions
* Add vehicle side override if vehicle is placed at main base in editor

## 0.9.0.4
* Fix wrong time in RP cooldown bar (#354)
* Fix RP Cooldown showing below 0 seconds
* Fix rally points being destroyed by dead / incapped units (#353)
* Fix FO not getting blocked by units in vehicle (#356)
* Fix BTR smoke launchers not showing on correct place on HUD (#337)
* Fix overlapped zeroing/ammotype HUD text (#347)
* Fix being able to see enemy vehicles on map (#233)
* Fix (attempt) at removing 'Regroup' chat messages (#221)
* Fix players sometimes getting way more effective 2D scopes
* Fix RP placement giving lots of errors in log file (#256)
* Fix first RP placement freezing the game for a second (#300)
* Fix not being able to vote/accept squad invite on respawn screen (#169)
* Fix respawn screen not closing when mission ends
* Fix 'You are being bandaged' stuck on screen (#77)
* Fix incapped timer not resetting after respawn
* Fix kills from respawning incap players not showing on scoreboard
* Fix broken dragging/CPR (#352)
* Fix CQB preset for SL (Without scope)
* Add Forced Radio usage (#331)
* Add padding to deploy button on spawn screen (#349)
* Add attack/defend request marker on map click menu
* Add Highlight important vacant kits with color (#235)
* Add highlight of your own name in respawn screen (#236)
* Add a delay to open respawn screen when you die from consecutive incaps (#288)
* Add more loading screen hints (#335)
* Add HAT squadtype unlocking at higher player counts
* Changed 50cal impact effect and sound
* Improve BTR smoke launcher effectiveness
* Increase RP/FO rearm cooldown to 7:30
* Increase F-interact cone (easier door opening)
* Reduce weight on static weapon backpacks
* Allow opening squad screen when incapped/in vehicle
* Disable skiplobby on all missions
* Disable vote to increase tickets
* Make progress bar in top left a bit smaller

## 0.9.0.2
* UI Changes
  * Changed Arma loading screen
  * Changed Arma 3 Esc menu
  * Changed Arma 3 HUD for infantry and vehicles to lower profile
  * Reduced data on Command bar (Bottom left for SL) removing pointless info    
  * Change Frontline Settings UI (Left top on Esc menu)
  * Added custom font for big buttons/headers
  * Fix Question HUD (Voting/F5-F6 thing)  not showing more than two options
  * Add shared 'progress bar' HUD in top Left
  * Added RP uptime and cooldown for squad leads to progress bar HUD
  * Changed capture bar to new UI
  * Changed left/right click hold to new UI
  * Changed tickets UI to smaller element in top Left
  * Added fancier loading screen backgrounds
  * Update some UI elements on scoreboard
  * Highlight player name on the scoreboard in orange
  * Redid parts of Respawn menu visual style
  * Remove tabs on map hud in top right (#340)

* Bornholm Changes
  * Redid forest clutter
  * Removed a lot of trees from forest areas
  * Added heightmap noise to forest areas
  * Added more wheat fields
  * Painted the yellow wheat fields on the satmap
  * Made wheat fields less dense
  * Added more rolling hills to heightmap
  * Fixed the coastal heightmap
  * Switched to CUP Objects where possible
  * Redid some Tree textures/rvmats
  * Fixed some pine trees being impossible to shoot through
  * Missing .p3d factory

* Add Vehicle Rearming (Maintenance pad on map)
* Add Vehicle Repairs (Maintenance pad on map)
* Add Mobility Repairs (Use toolkit from vehicle inventory)
* Fix incapped units blocking FO capturing (#324)
* Fix being able to set off breaching charage after respawn (#320)
* Add blocking of FO area after one has been destroyed (#190)
* Increase effectiveness of vehicles a little
* Improve teamswitch cooldown notification (#333)
* Add role presets (#259)
* Add map screenshots to MP Lobby for easier mission selection
* Add better suppression to IFV autocannons
* Reduced the effectiveness of partial cover when getting suppressed (#325)
* Vehicles will now have a delayed spawned at start (No more instant vehicle rush #200)
* Fix vehicle spawn conditions (Missions can add required players #200)
* Fix not being able to repair certain parts of vehicle if low damage (#345)
* Fix Tanoa missions not using APEX Nato gear
* Make BTR slightly easier to disable
* Change all missions to 5 zones instead of 7
* Reduce standard ticket amount to 200 instead of 250
* Replace AAW Server mod by AAW
* Update to latest Clib version
* Update to TS 3.1 compatible TFAR Plugin
* Recode parts of the Radio mod to work standalone with CBA

## 0.8.3.4
  * Fix breaching charges not working on Kunduz (#314)
  * Fix enemy vehicle markers shown after switching side (#233)
  * Fix wrong color of attack defend map icons (#296)
  * Fix revive not working (#315)

## 0.8.3.3
  * Allow breachign charges to destroy Hescos (#303)
  * Fix incaps being shown as kills on scoreboard
  * Update configs for RHS IFVs
  * Fix being unable to destroy FO while inside vehicle (#309)
  * Fix being unable to block FO while inside vehicle (#309)
  * Enable the FPS meter in the top right corner (#306)
  * Disable the FPS barmeter on right side of screen
  * Fix only the BLUFOR team showing on scoreboard
  * Fix showing of enemy roles/squad types on scoreboard
  * Fix the lack of medical 3D icons for medic kit (#289)
  * Tweak stamina a little
  * Attempt to fix being able to hold the gas while incapped in vehicle (#310)
  * Fix not showing as incapped when you go incapped in a vehicle (#311)
  * Disable ability to treat enemy players (#302)
  * Fix not being able to access inventory of dead units (#286)
  * Make the MRAP maximum speed a little higher

## 0.8.3.2
  * Made infantry Vests a little stronger again
  * Fix only the BLUFOR team showing on scoreboard
  * Fix showing of enemy roles/squad types on scoreboard
  * Added tooltips to the columns on scoreboard
  * Added a darker background to the scoreboard
  * Can now move while scoreboard is open
  * Fix scrolling to an empty line within groups on the scoreboard  
  * Reduce the points you get for setting up FOs slightly
  * Increased required FOB spacing
  * Increased maximum range you can place FO
  * Increased minimum range from enemy zone for FO
  * Add switching to Pistol with Number key when bandage equipped #292
  * Fix bleedout timer not stopping when bandaged #294
  * Reduce clutter / grass sizes on bornholm and Bozcaada
  * Fix dragging not showing correctly all the time
  * Fix missing file 'any' when hovering over group marker #220

## 0.8.3.1
### Systems
  * Rewrote medical system / incapped state #107
    * Faster, less broadcast, more reliable
    * Fixes some bugs with dragging and messages getting stuck on screen
  * Added ePeen Comparison Chart #263
    * Shows Kills/Incaps/Deaths and 'Total' score
    * Sorted by average score per squad
    * Will update in 1minute cycle only (Prevent using as kill-confirm)
    * More will be added to this in future

#### Fixes
  * Fix 'You are being bandaged' or similar message on screen #77
  * Fix MG role not spawning with a backpack #267
  * Fix game not ending on 0 tickets #270
  * Fix mission not ending on succesful vote
  * Fix player count not updating while respawn screen is open #278
  * Fix the AI 'Regroup' announcer not shutting the hell up #221
  * Fix squad invite not working #275 and #258
  * Fix losing weapon on slow revive/after dragging #266
  * Fix Rearm option giving more items than you should be getting #277
  * Fix actual map being open behind respawn screen #250
  * Fix mission ending repeatedly
  * Fix constant blurry screen that could happen when connecting
  * Fix error that could happen when leaving squad

#### Changes
  * Standardized vest armor
  * Increased shotgun recoil
  * Increased LMG intertia (Sway when quickly turning)

## 0.8.2.4
  * Fix Vehicle respawn
  * Fix Vehicle abandonment

## 0.8.2.3
#### Fixes
  * Fix not being able to select multiple rifleman kits #240
  * Fix for losing role when someone else with same role joins squad #202
  * Fix gear duplicating on revive #213
  * Fix losing rockets/equipment on revive #234
  * Fix vehicles not despawning properly #238
  * Fix vehicles spawning in with extra weapons/ammo #227
  * Fix missing uniforms for USMC faction
  * Fix ticket getting subtracted on incap instead of death #102
  * Fix a debug chat message showing whenever you treat someone/rearm
  * Fix squads with 0 players being shown on respawn UI #248

#### Additions
  * [+ Disposable launchers can now be rearmed at FOs / RPs #239 +]
  * [+ Squad radar now enabled inside vehicles #212 +]
  * [+ Added autoassign to Squad Lead role when you create a new group #224 +]
  * [+ Prevent switching teams with big player count difference +]
  * [+ Player count is now shown on respawn screen #249 +]
  * [+ Players inside vehicles are now detected properly when attempting to deploy an FO #251 +]
  * [+ Players are now automatically assigned to the opposite team on new round start +]
  * [+ Can now lock squads #258 +]
  * [+ Can now turn in in MRAPs, giving you less visibility but more cover +]


#### Changes
  * Increased FO destroy range from 20 to 30
  * Spotted markers for OPFOR are now Blue
  * LAT explosion effects are redone
  * LAT explosion now applies suppression
  * Change default mission duration to 2 hours
