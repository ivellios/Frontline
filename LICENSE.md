Copyright 2017. Frontline Team

No license (https://www.gnu.org/licenses/license-list.html#NoLicense)
This includes no commerial use allowed (Any form of monetization).
EXCEPTION IS MADE TO ALL .PBOs that have a seperate LICENSE file.

Some functions are modifications of old AAW, which was released under APL License. All files used under the license will have it mentioned so separately. Full APL License can be found here: https://www.bistudio.com/community/licenses/arma-public-license
The current AAW project can be found here: https://github.com/TaktiCool/ArmaAtWar

--

If you ask nicely we might just grant you an exception to this,
so if there's something you like and you have a clear case of what you want to use it for, go ahead and ask in Discord
